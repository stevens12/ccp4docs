DISTANG (CCP4: Supported Program)
=================================

NAME
----

**distang** - Distances and angles calculation

SYNOPSIS
--------

| **distang xyzin** *foo\_in.brk*
| [`Keyworded input <#keywords>`__]

 DESCRIPTION
------------

DISTANG is a general purpose program for calculating atomic distances
within a protein molecule, and between symmetry equivalent molecules
within a crystal lattice. It is based on Sam Motherwell's program
DJANGO, developed for the Cambridge Data Base (and that goes back to
John Rollett's original algorithm!). It will be almost completely
superseded by Eugene Krissenel's new ncont program, but there are still
some functions which are useful.

 PROGRAM FUNCTION
-----------------

The functions of the program DISTANG may be summarised as follows:

#. To calculate bond distances for a subset of the coordinates stored in
   a Brookhaven format file. The atomic radii can be chosen by the user.
   This is very useful when examining the distribution of heavy atoms or
   anomalous scatterers and checking for non-crystallographic symmetry.
#. To calculate bond angles for a subset of the coordinates stored in a
   Brookhaven format file.
#. To calculate possible packing restrictions for a coordinate set.
#. To generate information in a suitable format for
   `WATERTIDY <watertidy.html>`__ from a set of coordinates stored in
   Brookhaven format.
   **Important:** *post CCP4 V4.2, WATERTIDY cannot read the logfile
   from DISTANG directly. Instead the `OUTPUT DISTOUT <#output>`__
   option of DISTANG must be used - the resulting output file assigned
   to DISTOUT will be in the appropriate format to be read in by
   WATERTIDY.*
#. To generate information in a suitable format for the PROTIN
   dictionary from a set of coordinates stored in Brookhaven format.

The type of distances listed is controlled by the keyword
`DISTANCE <#distance>`__ using combinations of the subsidiary keywords
ALL or VDW and INTER or INTRA.

Another useful option is to do a packing search. If the orientation of a
molecule is known, various translational parameters can be tested to
find those which give impossible packing. See `GRID <#grid>`__ for
details about this.

 KEYWORDED INPUT
----------------

All control input is in free format with keywords specifying the options
required. The possibilities are:

    `**ADDRADIUS** <#addradius>`__, `**ANGLE** <#angle>`__,
    `**DISTANCE** <#distance>`__, `**DMIN** <#dmin>`__,
    `**END** <#end>`__, `**FROM** <#from>`__, `**GRID** <#grid>`__,
    `**LIST** <#list>`__, `**OUTPUT** <#output>`__,
    `**RADII** <#radii>`__, `**SYMMETRY** <#symmetry>`__,
    `**TITLE** <#title>`__, `**TO** <#to>`__, `**TORSION** <#torsion>`__

 DISTANCE [ VDW \| ALL ] [ INTER \| INTRA ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 VDW
    (Default. ) Only contacts between atoms not in the same, or
    adjoining, residues are printed.
 ALL
    all contacts to be output, even those within one residue.
 INTER
    Lists intermolecular distances only.
 INTRA
    (Default.) Calculates intra and inter-molecular distances.

 SYMMETRY <SG>
~~~~~~~~~~~~~~

Followed by a spacegroup name or number or explicit symmetry operator in
the standard form. If the Spacegroup is given in the coordinate header
this is used by default.

 RADII <atomid> <radii> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~

| 
| Resets search radii for different atom types. A list of atom types and
  radii follows on the same line.

| Default values are: CA 1.5 C 1.5 N 1.5 O 1.5 S 1.5 OW 1.5 P 1.5 SE 1.5
| *e.g.*: RADII CA 2.2 OW 1.5 ZN 2.0 C 1.7 N 1.7 O 1.7

The program tries to match the atom name to the 2 character atom types
first, and if none are found to the 1 character atom types. This allows
you to set different radii for CA and other Carbon atoms say, but there
is an unsolved problem for CAlcium. All atom types are upper case.

If the atom name does not match it is ignored

 FROM ATOM <inat0> [TO] <inat1>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

FROM RESIDUE <ires0> [TO] <ires1> [ CHAIN <chainID> ] [ ALL \| ONS \| CA ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 TO ATOM <jnat0> [TO] <jnat1>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TO RESIDUE <jres0> [TO] <jres1> [ CHAIN <chainID> ] [ ALL \| ONS \| CA ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sets atom limits. (Default all atoms to all.)

If ATOM is specified it is followed by <inat0> and <inat1>, respectively
the first and last target atoms checked. *I.e.* FROM atoms <inat0> to
<inat1> are checked against TO atoms <jnat0> to <jnat1>. (Atoms are
numbered 1 to NAT, in the order read, but the residue order can be
varied without restriction. Beware: atoms with occ=0.0 are not counted.)

If RESIDUE is specified it is followed by <jres0> and <jres1>,
respectively the first and last target residues checked, and optionally
subsidiary keywords:

 CHAIN <chainID>
    Chain ID for the specified residue range.
 ALL \| ONS \| CA
    ALL (default) will select all atoms in the requested residues. ONs
    will select just the oxygen and nitrogen atoms. CA will select just
    the CA atoms.

*i.e.* FROM residues <ires0> to <ires1> are checked against TO residues
<jres0> to <jres1> for the appropriate class of atoms.

 ANGLE
~~~~~~

Calculate bond angles between bonded atom triples.

 ADDRADIUS <addrii>
~~~~~~~~~~~~~~~~~~~

Value added to the value given by the RADIUS keyword. Only applies if
the RADIUS keyword is also given.

 DMIN <dmin>
~~~~~~~~~~~~

The minimum distance printed is dmin

 LIST <natoms>
~~~~~~~~~~~~~~

List fractional and orthogonal coordinates for first <natoms> atoms.

 OUTPUT [ LINKS \| LPOUT \| DISTOUT \| KHCONN \| PDBCONN ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 LINKS/LPOUT
    writes the coordinate pairs for each link to a file assigned to
    XYZOUT. (These keywords are synonyms for each other.)
 DISTOUT
    The "old-style" DISTANG output is written to a file assigned to
    DISTOUT. This is required for the program WATERTIDY.
 KHCONN
    list pseudo-PROTIN distance dictionary output to a file assigned to
    KHCONN. Only 250 atoms are allowed for this option. (See
    `MAKEDICT <makedict.html>`__ to prepare residue type entry for
    dictionary.)
 PDBCONN
    list PDB type file containing CONECT lines to a file assigned to
    PDBCONN. Only 250 atoms allowed for this option.

 GRID <xmin> <xmax> <dtx> <ymin> <ymax> <dty> <zmin> <zmax> <dtz>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All in fractions of unit cell.

Define search grid for a correctly oriented molecule through asymmetric
unit. The coordinates are translated over the grid and a table of the
number of bad contacts is generated; *i.e.* it is a sort of packing
search. It sets all RADII except that for CA equal 0 to save time. The
default RADII for CA is 2.0Å but you can reset this.

 TITLE <title>
~~~~~~~~~~~~~~

Title to print on output.

 TORSION
~~~~~~~~

Calculate torsion angles between bonded atom quadruples. Probably will
not work but there is residual code for any eager programmer to look at.

 END
~~~~

triggers calculation.

 NOTES
------

#. Size limitation: a maximum of 60000 atoms is handled.
#. It is ESSENTIAL to give CRYSTAL and SCALE lines in the input
   coordinate file. These are needed for generating symmetry
   equivalents. If the spacegroup is given the SYMM keyword is not
   required.

 EXAMPLES
---------

::

    distang XYZIN $CTEST/toxd/toxd.pdb << END-distang
    SYMM 19
    dist vdw INTRA
    RADI   C 1.5 CA 1.5 N 1.5 O  1.5 OW 1.6
    FROM ATOM 1 TO 1000
    TO ATOM 1 to 200000
    END  
    END-distang

    distang XYZIN $CTEST/toxd/toxd.pdb << END-distang
    SYMM 19
    dist vdw INTRA
    RADI   C 1.5 CA 1.5 N 1.5 O  1.5 OW 1.6
    FROM RES ALL CHAIN A 1 to 125
    TO   RES ALL CHAIN W 1 to 256
    END  
    END-distang

    #   Calculate distance angles for symmetry contacts.
    #
    distang XYZIN $CTEST/toxd/toxd.pdb << END-distang 
    SYMM P212121
    dist inter 
    angle
    RADI    C 0.8 N 1.5 O  1.5 OW 1.6
    FROM ATOM 1 TO 561
    TO ATOM 1 TO 561
    END  
    END-distang

    #   Search a set of coordinate fragments to see if any should be linked
    #  Useful after Arp/Warp scripts
    distang XYZIN taka_2065115.pdb << EOF
    SYMM 170
    DIST VDW 
    RADI  CA 3.8
    FROM ATOM 1 TO 10000
    TO ATOM 1 TO 10000
    END
    EOF  


    # Search to se which SE might be associated with which molecule.
    distang XYZIN chmi_s1.pdb XYZOUT chmi_s1-contacts.cds << EOF
    outp link
    symmetry P41212
    radii  SE 10.0
    end
    EOF

    #   A grid search:  check your rotation function
    distang XYZIN taka_2065115.pdb << EOF
    SYMM 170
    DIST VDW INTER 
    RADI  CA 1.8
    GRID 0 1 0.05   0 1 0.05   0 0 1
    FROM ATOM 1 TO 10000
    TO ATOM 1 TO 10000
    END
    EOF  


    #   Output list of contacts in form suitable for WATERTIDY
    distang DISTOUT $CCP4_SCR/distang.list XYZIN $CEXAM/toxd/toxd.pdb << END-distang 
    SYMM 19
    output distout
    dist vdw 
    #  Use default radii and search from protein atoms to solvent atoms
    from atom 1 to 502
    to atom 503 to 600
    END  
    END-distang

    #   Output distance pairs to XYZOUT.
    distang XYZOUT $CCP4_SCR/links.coords XYZIN $CEXAM/toxd/toxd.pdb << END-distang 
    SYMM 19
    output link
    dist vdw 
    #  Use default radii and search all atoms
    END  
    END-distang

    #   Calculate distance angles  and output PROTIN type distance list.
    #  for a new substrate (Prepare other parts of dictionary with MAKEDICT)
    distang KHCONN $CCP4_SCR/junk.kh XYZIN substrate.pdb << END-distang 
    SYMM 19
    output khconn
    dist all 
    angle
    RADI    C 0.8 N 0.8 O  0.8  P 1.3
    FROM ATOM 1320 TO 1350     !   Atoms in a substrate perhaps.
    TO ATOM 1320 to 1350
    END  
    END-distang

SEE ALSO
--------

Alternative contact analysis:

-  `act <act.html>`__
-  `contact <contact.html>`__

 REFERENCES
-----------

#. IUPAC-IUB conventions, *J. Mol. Biol.*, **50**, 1 (1972).
