OVERLAPMAP (CCP4: Supported Program)
====================================

NAME
----

**overlapmap** - calculates the average of two maps

SYNOPSIS
--------

| **overlapmap mapin1** *foo.map* **mapin2** *bar.map* [ **mapin3**
  *baz.map* ] **mapout** *quux.map*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

**overlapmap has basically three modes of operation, combining two
maps** to produce an output map, correlating two maps or generating a
real-space R-factor between two maps. Here are the modes in more detail:

#. Average MAPIN1 and MAPIN2 with output to MAPOUT (`MAP
   AVERAGE <#map_average>`__).
#. Sum the density from MAPIN1 and MAPIN2, output to MAPOUT (`MAP
   ADD <#map_add>`__).
#. Set density to zero in MAPOUT if present in MAPIN1 (*i.e.* non zero
   MAPIN1 density). Otherwise grid points in MAPOUT are set to MAPIN2
   (`MAP EXCLUDE <#map_exclude>`__). This requires MAPIN1 has some grid
   points which are EXACTLY zero; you are unlikely to have this unless
   you have generated a map from coordinates.
#. Set density in MAPOUT to MAPIN2 if the grid point in MAPIN1 > 0.
   Otherwise set to zero (`MAP INCLUDE <#map_include>`__).
#. Correlate the density between MAPIN1 and MAPIN2 over a given volume,
   which may be either the whole map, the volume occupied by a given
   residue or atom, or section by section (there is no MAPOUT in this
   mode). There are three correlation modes (`CORRELATE
   ... <#correlate>`__), two of which require a third map array (see
   `MAPIN3 <#notes_mapin3>`__). This is used to flag which grid points
   are associated with each residue or atom and are therefore to be
   included in their correlation coefficient calculation.

   Remember the correlation transcends any need to scale the 2 maps.
   Below is the definition of the correlation coefficient:

   ::


       corr. coeff.  =    (<x y> - <x><y>) / 
                     ( sqrt(<x^2> - <x>^2)  *  sqrt(<y^2> - <y>^2) )

   | where x represents the density values from one map and y the values
     from the other.
   | < > represents the mean value of the quantities inside the bracket.

#. Calculates a Real Space R factor à la Branden/Jones (`reference
   [1] <#reference1>`__).

   ::


       Real space R factor =
                 Sum(RHO_obs - RHO_calc)/Sum(RHO_obs + RHO_calc)

   summed over all points within a residue. This requires that RHO\_obs
   and RHO\_calc are ON THE SAME SCALE. It is also sensitive to the atom
   radius you chose in `SFALL <sfall.html>`__. Eleanor Dodson believes
   it is not as useful as the correlation coefficient. MAPIN3 will be
   needed for this mode of operation calculated from SFALL in the RESMOD
   mode - see `below <#notes_mapin3>`__.

KEYWORDED INPUT
---------------

The available keywords are:

    `**CHAIN** <#chain>`__, `**CORRELATE** <#correlate>`__,
    `**MAP** <#map>`__, `**PATTERSON** <#patterson>`__, `**REAL SPACE
    R** <#real_space_r>`__

MAP [ ADD \| AVERAGE \| EXCLUDE \| INCLUDE ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 ADD <scale1> <scale2>
    add <scale1>\*map1 + <scale2>\*map2 and output new map. (Default
    <scale1> = <scale2> =1.)
 AVERAGE
    average two maps and output new map.
 EXCLUDE
    exclude points from output map if they exist in MAP1. Any grid point
    that is is not zero in MAP1 will be set to zero in MAPOUT. Grid
    points in MAPOUT will be set to the value in MAP2 if the grid point
    in MAP1 is zero.
 INCLUDE
    include points in output map from MAP2 if they exist in MAP1. The
    value of the grid point in MAPOUT will be that of MAP2 if the grid
    point in MAP1 > 0. Otherwise the point in MAPOUT will be set to
    zero.

CORRELATE [ SECTION \| RESIDUE \| ATOM ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 SECTION
    correlation is section by section.
 RESIDUE
    correlation is residue by residue.
 ATOM
    correlation is atom by atom.

REAL SPACE R
~~~~~~~~~~~~

tabulated residue by residue.

CHAIN <chain\_ID> <1st\_residue\_number> <last\_residue\_number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Repeat residue numbering by chain, as in `SFALL <sfall.html>`__.

PATTERSON <sigmin> sigmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculate a correlation coefficient as in GROPAT
(`[2] <#reference2>`__). <sigmin> and <sigmax> are the minimum and
maximum grid value/sigma ratio, typically 3. and 10. It is recommended
that origin removed difference Pattersons should be calculated.

NOTES
-----

MAPIN3 may be assigned to the modified map output of SFALL. This map
flags each grid point according to certain criteria; the most useful is
the residue flag created by MODE ATMMAP RESMOD. This flags each point in
the map space to indicate which atom of a residue it is near (using the
Atom Radii specified in SFALL), and whether the atom involved is main
chain, or side chain. This is required if you want to use CORRELATE
RESIDUE or REALSPACE Rfactor.

The program must have both maps calculated in exactly the same way.
Since any Fc map generated from coordinates has a fixed asymmetric unit
and AXIS ORDER do them first and make sure your FFT calculation matches
that.

EXCLUDE is useful for flattening the protein density when you want to
inspect the residual density only. Used in solvent searches and also in
the `PHASED TRANSLATION <fft.html#phtranslation>`__ function (part of
the FFT package). Generate the protein density in SFALL outputting an Fc
map.

CORRELATE can be done section by section or by RESIDUE or ATOM.
CORRELATE RESIDUE is only possible if MAPIN3 is generated in SFALL
outputting an Fc map with mode = `mode atmmap
resmod <sfall.html#mode_atmmap_resmod>`__).

CORRELATE ATOM is only possible if MAPIN3 is generated in SFALL
outputting an Fc map with mode = `mode atmmap
atmmod <sfall.html#mode_atmmap_atmmod>`__).

SEE ALSO
--------

| `**fft** <fft.html>`__\ (CCP4),
| `**sfall** <sfall.html>`__\ (CCP4),
| `**maprot** <maprot.html>`__\ (CCP4)

REFERENCES
----------

#.  Branden C. and Jones A., Nature 343 687-689 (1990)
#.  Jones Y. and Stuart D, Proc. of CCP4 Study Weekend on Isomorphous
   Replacement And Anomalous Scattering, 1991 39-48.

EXAMPLES
--------

Correlation by residue
~~~~~~~~~~~~~~~~~~~~~~

`overlapmap.exam <../examples/unix/runnable/overlapmap.exam>`__

Averaging 2 maps
~~~~~~~~~~~~~~~~

::

    overlapmap  MAPIN1 chmi_phase_extend.ext  MAPIN2 wang_cycle3.ext 
      MAPOUT chmi_wang_av.map << END 
    MAP AVERAGE
    END

See also $CEXAM/unix/runnable/mapcorrelation\_procedures and
$CEXAM/unix/non-runnable/mapcorrelation.exam.
