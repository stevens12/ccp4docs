OTHERCELL (CCP4: Supported Program)
===================================

NAME
----

**othercell**

SYNOPSIS
--------

**othercell [-tolerance <angular\_tolerance>] [xmlout <output.xml>]**\ 

| \ `Examples <#examples>`__
| `Release Notes <#release_notes>`__

DESCRIPTION
-----------

| This program takes an input unit cell and lattice centering type, and
  generates all possible equivalent alternative unit cells with the
  tolerance (default 3°: note that this number affects both cell lengths
  and cell angles). A "target" cell may be given, in which case only
  alternatives which match this (within the tolerance) are listed. If
  run interactively from the command line, the user is prompted for
  input.
| The program shares code with Pointless.
| Input line 1:   unit cell dimensions, lattice type
|     cell  is  a b c  alpha  beta  gamma
|     lattice type:  P, A, B, C, I, F, R
| Input line 2: [optional]  target cell
|     a b c  alpha  beta  gamma

Examples
--------

| Just list alternatives (in this case hexagonal, trigonal,
  orthorhombic, monoclinic and triclinic)
| othercell << EOF
| 74.72 129.22 184.25  90.00  90.00  90.00 C
| EOF
| Choose matching alternatives (in this case hexagonal, trigonal,  and
  triclinic)
| othercell << EOF
| 74.72 129.22 184.25  90.00  90.00  90.00 C
| 74.72 129.22 184.25  90.00  90.00  90.00
| EOF

Release notes
-------------

1.2
~~~

| Tidied output slightly, added this documentation
