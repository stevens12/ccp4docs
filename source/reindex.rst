REINDEX (CCP4: Supported Program)
=================================

NAME
----

**reindex** - produces an MTZ file with h k l reindexed. There are
options to change the symmetry, and to reduce the reflection to the
(new) asymmetric unit.

SYNOPSIS
--------

| **reindex hklin** *foo\_in.mtz* **hklout** *foo.mtz*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

Reindex reflections and/or change symmetry and/or reduce to asymmetric
unit again. The input file may be:

a. an unmerged (multirecord) file, in which the operations are carried
   out on the original indices (reconstructed using the M/ISYM column of
   HKLIN), or
b. a merged file (one record per reflection), in which only reindexing
   is allowed. The file should not contain phase or phase coefficient
   columns: these will be left unchanged, and will probably be wrong. If
   the reflection indices are reduced to the asymmetric unit (default
   unless `NOREDUCE <#noreduce>`__ is specified), then any anomalous
   difference columns will be negated, and F+/- I+/- columns swapped, if
   the hand of the index is changed by the reduction process.

This program is useful, for example when:

#. you have unmerged integrated data, which will need to be reduced to
   the asymmetric unit after reindexing, *e.g.* **mosflm**.
#. you have unmerged data, and you want to change to a different point
   group. This will have a different definition of the asymmetric unit.
#. you have merged data which needs to be reindexed, *e.g.* you might
   want to change h,k,l to k,h,-l in a tetragonal system. Beware, not
   all reindexing is appropriate, and the program does no check (*e.g.*
   reindexing l,h,k is invalid for a tetragonal crystal).

The program operates on each reflection in turn, without sorting them.
Therefore, the output file will normally need to be resorted before
further use (see `CAD <cad.html>`__ or `SORTMTZ <sortmtz.html>`__). For
the reindex option, the new unit cell is determined and written to the
output file. For unmerged data, the orientation data (cell and Umat) are
also updated for all batches.

The following operations are carried out on each reflection:

#. If unmerged data (multirecord file): reconstruct original indices
#. Reindex if requested
#. Reject reflections with non-integral indices
#. (Optionally) reduce reflection to asymmetric unit, using new symmetry
   (if different). Anomalous difference, F+/- and I+/- columns will be
   corrected as necessary, *i.e.* if the hand of the index is changed by
   the reduction process.

KEYWORDED INPUT
---------------

Available keywords are:

    `**END** <#end>`__, `**LEFTHAND** <#lefthand>`__,
    `**MATCH** <#match>`__, `**NOREDUCE** <#noreduce>`__,
    `**REINDEX** <#reindex>`__, `**SYMMETRY** <#symmetry>`__

REINdex
~~~~~~~

Subkeywords HKL or AXIS (default HKL) followed by a transformation.

The transform can be given a similar way to symmetry operations (see
below for examples).

Subkeyword **HKL**

::

    reindex HKL   k, h, -l

implies h\ :sub:`new` = k\ :sub:`old`, k\ :sub:`new` = h\ :sub:`old`,
l\ :sub:`new` = -l:sub:`old` *i.e.* this defines a matrix to reindex the
reflection data, such that

::

         [hn kn ln]          =  [ho ko lo] [M]

::

    reindex HKL   k, h, -l

generates a reindexing matrix:

::

         [M] =  [ 0  1  0]
                [ 1  0  0]
                [ 0  0 -1]

::

    reindex HKL   h, -k, -h/2-l/2

would generate a matrix:

::

         [M] =  [ 1.0  0.0  0.0]
                [ 0.0 -1.0  0.0]
                [-0.5  0.0 -0.5]

::

    reindex HKL   h-1, k, l

would result in all h indices being reduced by 1.

| 

Subkeyword **AXIS**

If the program `AXISSEARCH <axissearch.html>`__ has been used to find
possible alternative reciprocal axes, the reindexing matrix can be
specified in terms of reciprocal axes, for example:

::

    reindex AXIS   a*+c*,c*,-b*

implies a\*\ :sub:`new` = a\*\ :sub:`old`\ +c\*\ :sub:`old`,
b\*\ :sub:`new` = -c\*:sub:`old`, c\*\ :sub:`new` = -b\*:sub:`old`
(which is in fact equivalent to ``reindex HKL h,l-k,-k``)

::

        If     [a*new] = [M]**-1 [a*old]
               [b*new]           [b*old]
               [c*new]           [c*old]

        Since   [hn kn ln] [a*n]    =  [ho ko lo] [a*o]
                           [b*n]                  [b*o]
                           [c*n]                  [c*o]

          [hn kn ln] [M] **-1 [a*o] =     [ho ko lo] [a*o]
                              [b*o]                  [b*o]
                              [c*o]                  [c*o]

        and     [hn kn ln]          = [ho ko lo]  [M] as before

| 

Cell dimensions will be recalculated for the redefined cell. Be careful
that the index transformation preserves the hand of the axes (unless you
want to invert the hand), *i.e.* that the matrix has a positive
determinant. If it does invert the hand, the program will change the
sign of all reindexing elements unless the keyword
`LEFTHAND <#lefthand>`__ is present.

If the transformation leads to fractional indices for some cases (as in
the 2nd example above), these reflections will be rejected. If the
reindexing operations include translations, then the orientation data in
the output file will not be strictly correct.

MATCH
~~~~~

In some complicated cases it is easier to specify how three new
reciprocal vectors should be matched onto three old ones. The program
`AXISSEARCH <axissearch.html>`__ allows you to check reciprocal vector
lengths and angles, and you may wish to convert a P1 cell say to one
which approximately matches a C2 one with angles approximately 90
degrees. The program can deduce the reindexing matrix from this
information.

::

        match   2h'=h +k -l  * 2k'= -h +k + l * 1l'= 1h

SYMMetry <spacegroup\_name \| spacegroup\_number \| symmetry operations>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For unmerged files, set new symmetry for reduction to asymmetric unit
(after optional reindexing). Defaults to input symmetry from HKLIN file.
New symmetry is written to the HKLOUT file. If the output symmetry is a
lower symmetry, then all input reflections will output, but there will
be a lower completeness over the asymmetric unit. If the output symmetry
is a higher symmetry, then all input reflections will output, with more
observations per h k l set.

For merged files, new symmetry is written to HKLOUT but reduction to
asymmetric unit is turned off. This option is therefore not advisable
for merged data, unless you know what you are doing.

LEFTHAND
~~~~~~~~

Allow the reindexing transformation to invert the hand (EXTREMELY unwise
unless you are very sure you know what you are doing!). Anomalous
differences will NOT be altered by a reindexing operation inverting the
hand.

NOREduce
~~~~~~~~

Do not reduce merged reflections to asymmetric unit after reindexing.
Reflections in unmerged files are always reduced to the asymmetric unit
defined by the (new) symmetry.

END
~~~

Terminate control input, also done by end-of-file.

INPUT AND OUTPUT FILES
----------------------

HKLIN
    Input MTZ file. This may contain unmerged or merged reflections,
    depending on what you want to do, see `DESCRIPTION <#description>`__
    section above.
HKLOUT
    Output MTZ file, containing reindexed reflections.

EXAMPLES
--------

unix example scripts found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `reindex.exam <../examples/unix/runnable/reindex.exam>`__

Non-runnable examples of reindexing and sort
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Data processed by MOSFLM in P1 with cell

   ::


          73.6510  126.4434  126.6338  120.6573   89.7704   89.9541

   We want to convert this to P3 for scaling and merging in SCALA. As
   well as changing the point group, we need to reindex such that the
   120 degree angle is gamma.

   ::


       reindex HKLIN processed-p1.mtz HKLOUT junk.mtz <<EOF
       reindex HKL k, l, h
       symm P3
       end
       EOF

       sortmtz hklin junk.mtz HKLOUT processed-p1-to-p3.mtz <<EOF
       H K L M/ISYM BATCH I SIGI
       EOF

   Note that the data needs sorting again after reindex.

#. Converting P1 cell to approximate C2 form:

   ::

        #  Reindex
        reindex hklin $CCP4_SCR/P1.mtz hklout $SCRATCH/C2.mtz <<eof
               match   2h'=h +k -l  * 2k'= -h +k + l * 1l'= 1h
        eof

#. Changing P21 with h+l = 2n +1 absent to standard P21 form:

   ::

        #  Reindex
        reindex hklin $CCP4_SCR/bigcell.mtz hklout $SCRATCH/JUNK.mtz <<eof
               reindex h/2-l/2, k, h/2+l/2
        eof
        #  Same transformation given in terms of reciprocal axes
        #
        reindex hklin $CCP4_SCR/bigcell.mtz hklout $SCRATCH/JUNK.mtz <<eof
               reindex AXIS a*-c*,b*,a*+c*
        eof
        # Sort and reconvert to asymmetric unit again
        cad hklin1 $SCRATCH/JUNK hklout $CCP4_SCR/newcell <<eof
               LABI FILE 1 ALLIN
        eof

SEE ALSO
--------

`axissearch <axissearch.html>`__, `reindexing <reindexing.html>`__
