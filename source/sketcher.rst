SKETCHER (CCP4: Supported Program)
==================================

NAME
----

**sketcher** - monomer library sketcher.

SYNOPSIS
--------

**ccp4i -t sketch**

DESCRIPTION
-----------

Sketcher is an interface to the `LIBCHECK <libcheck.html>`__, which can
be used to create new monomer library descriptions for use in
refinement.

The sketcher is part of the CCP4 graphical user interface
`ccp4i <ccp4i.html>`__ and cannot be run as a standalone application
except in the fashion described in the synopsis above. More normally
Sketcher is accessed from within CCP4i, via the \`\`Monomer Library
Sketcher'' task in the \`\`Refinement'' module.

USAGE
-----

The full Sketcher documentation can be found at
`$CCP4I\_TOP/help/modules/sketcher.html <../ccp4i/help/modules/sketcher.html>`__

SEE ALSO
--------

`ccp4i <ccp4i.html>`__, `LIBCHECK <libcheck.html>`__

AUTHOR
------

Liz Potterton
