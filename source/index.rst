CCP4 v7.0 Program Documentation
===============================

| **Reference:**
| Collaborative Computational Project, Number 4. 1994.
| "The CCP4 Suite: Programs for Protein Crystallography". Acta Cryst.
  D50, 760-763

There is a partial list of `program references <REFERENCES.html>`__
extracted from the individual program documentation.

--------------

| **Changes:**
| List of the `major program changes <CHANGESinV7_0.html>`__ since the
  previous release.

--------------

This list can also be seen in `function grouping. <FUNCTION.html>`__

BASIC
-----

-  `ccp4 <ccp4.html>`__ - introduction to the CCP4 Program Suite
-  `ccp4\_interface\_installation <ccp4i_installation.html>`__ -
   Installation of CCP4 Interface
-  `ccp4i <ccp4i.html>`__ - interface to the CCP4 programs & file
   display
-  `ccp4i2 <https://ccp4i2.gitlab.io/rstdocs/index.html>`__ - Modernised interface for the CCP4 program suite

GENERAL
-------

-  `alternate\_origins <alternate_origins.html>`__ - list of alternate
   origins for enantiomorphic spacegroups
-  `alternate\_origins\_allgroups <alternate_origins_allgroups.html>`__
   - list of alternate origins for different spacegroups
-  `ccp4 roadmaps <../share/ccp4i/help/roadmaps/index.html>`__ -
   roadmaps through the stages of structure determination using the CCP4
   suite
-  `cheshire cells <cheshirecell.html>`__ - background information on
   Cheshire Cells and their uses
-  `dm\_ncs\_averaging <dm_ncs_averaging.html>`__ - dm for ncs averaging
-  `dm\_skeletonisation <dm_skeletonisation.html>`__ - iterative
   skeletonisation using dm
-  `freerunique <freerunique.html>`__ - convert FreeRflags between CCP4
   and other formats
-  `harvesting <harvesting.html>`__ - harvesting data automatically and
   using datasets
-  `non-centro\_origins <non-centro_origins.html>`__ - lists of
   alternate origins for non-centrosymetric spacegroups
-  `pxmaths <pxmaths/index.html>`__ - Basic Maths for Protein
   Crystallographers
-  `reindexing <reindexing.html>`__ - information about changing
   indexing regime
-  `rotationmatrices <rotationmatrices.html>`__ - on Eulerian angles,
   polar angles and direction cosines, and orthogonalisation codes
-  `scalechoose <scalechoose.html>`__ - information about choice of
   scaling program
-  `symmetry <symmetry.html>`__ - overview of symmetry information
-  `tutorial <../examples/tutorial/html/index.html>`__ - tutorial
   material for CCP4 suite including Data Processing, MAD phasing,
   Molecular Replacement and Refinement
-  `twinning <twinning.html>`__ - dealing with data from twinned
   crystals

SUPPORTED
---------

-  `abs <abs.html>`__ - determine the absolute configuration (hand) of
   the heavy atom substructure
-  `acorn <acorn.html>`__ - Molecular Replacement, Sayre Equation and
   Dynamic Density Modification for the determination of a protein
   structure
-  `act <act.html>`__ - analyse coordinates
-  `afro <afro.html>`__ - multivariate substructure factor amplitude
   estimation for SAD/MAD and SIRAS
-  `aimless <aimless.html>`__ - scale together multiple observations of
   reflections
-  `almn <almn.html>`__ - calculates rotation function overlap values
   using FFT techniques
-  `amore <amore.html>`__ - Jorge Navaza's state-of-the-art molecular
   replacement package
-  `anisoanl <anisoanl.html>`__ - analyses of anisotropic displacement
   parameters
-  `areaimol <areaimol.html>`__ - Analyse solvent accessible areas
-  `baverage <baverage.html>`__ - averages B over main and side chain
   atoms
-  `blend <blend.html>`__ - multi-dataset combination and cluster
   analysis
-  `bones2pdb <bones2pdb.html>`__ - Make a PDB pseudo-coordinate file
   from a bones file
-  `bp3 <bp3.html>`__ - multivariate likelihood substructure refinement
   and phasing of S/MIR(AS) and/or S/MAD
-  `buccaneer <cbuccaneer.html>`__ - Statistical Model Building
-  `bulk <bulk.html>`__ - bulk solvent correction for translation search
   and rigid body refinement steps of AMoRe
-  `cad <cad.html>`__ - Collect and sort crystallographic reflection
   data from several files
-  `cavenv <cavenv.html>`__ - Visualise cavities in macromolecular
   structures
-  `ccp4mapwish <ccp4mapwish.html>`__ - custom wish interpreter required
   for MapSlicer
-  `chainsaw <chainsaw.html>`__ - Mutate a pdb file according to an
   input sequence alignment
-  `cif2mtz <cif2mtz.html>`__ - Convert an mmCIF reflection file to MTZ
   format
-  `cif2xml <cif2xml.html>`__ - Conversion of mmCIF files to XML
-  `clustalw interface <clustalw.html>`__ - Graphical Interface to the
   ClustalW Program
-  `cmakereference <cmakereference.html>`__ - Generate reference
   structure for pirate/buccaneer
-  `cmapcut <cmapcut.html>`__ - Prepare MR density search
-  `combat <combat.html>`__ - produces an MTZ file in multirecord form
   suitable for input to SCALA.
-  `comit <comit.html>`__ - Composite omit maps
-  `contact <contact.html>`__ - computes various types of contacts in
   protein structures
-  `convert2mtz <convert2mtz.html>`__ - CNS to MTZ conversion
-  `coord\_format <coord_format.html>`__ - fix PDB format and convert
   to/from other formats
-  `coordconv <coordconv.html>`__ - Interconvert various coordinate
   formats
-  `cphasematch <cphasematch.html>`__ - Calculate agreement between
   phase sets
-  `crank <crank.html>`__ - Automated structure solution for
   experimental phasing
-  `cross\_validate <cross_validate.html>`__ - Validation of harvest
   files for deposition
-  `crossec <crossec.html>`__ - interpolate X-ray cross sections and
   compute anomalous scattering factors
-  `csymmatch <csymmatch.html>`__ - Use symmetry to match chains
-  `ctruncate <ctruncate.html>`__ - Intensity to amplitude conversion
-  `data harvesting manager <dhm_tool.html>`__ - Tool for managing Data
   Harvesting Files
-  `detwin <detwin.html>`__ - detwins merohedrally twinned data
-  `dimple <dimple.html>`__ - automated difference map calculation
-  `distang <distang.html>`__ - Distances and angles calculation
-  `dm <dm.html>`__ - density modification package
-  `dmmulti <dmmulti.html>`__ - multi-xtal density modification package
-  `dtrek2mtz <dtrek2mtz.html>`__ - converts d\*trek scalemerge output
   into MTZ format
-  `dtrek2scala <dtrek2scala.html>`__ - initial processing of intensity
   files from D\*TREK
-  `dyndom <dyndom.html>`__ - determine dynamic domains when two
   conformations are available
-  `ecalc <ecalc.html>`__ - calculate normalised structure amplitudes
-  `edstats <edstats.html>`__ - Calculate electron density statistics
-  `f2mtz <f2mtz.html>`__ - Convert a formatted reflection file to MTZ
   format
-  `fffear <fffear.html>`__ - map interpretation package
-  `ffjoin <ffjoin.html>`__ - joining model fragments from FFFEAR
-  `fft <fft.html>`__ - fast Fourier transform
-  `fhscal <fhscal.html>`__ - Scaling of isomorphous derivative data
   using Kraut's method
-  `findncs <findncs.html>`__ - detect NCS operations automatically from
   heavy atom sites
-  `freerflag <freerflag.html>`__ - tags each reflection in an MTZ file
   with a flag for cross-validation
-  `fsearch <fsearch.html>`__ - 6-d molecular replacement (envelope)
   search
-  `gcx <gcx.html>`__ - support program to generate crank XML and run
   crank via a script
-  `gensym <gensym.html>`__ - generate sites by symmetry
-  `geomcalc <geomcalc.html>`__ - molecular geometry calculations
-  `gesamt <gesamt.html>`__ - structural alignment
-  `getax <getax.html>`__ - real space correlation search
-  `hgen <hgen.html>`__ - generate hydrogen atom positions for proteins
-  `hklplot <hklplot.html>`__ - plots a precession photo from an HKL
   data file
-  `hklview <hklview.html>`__ - displays zones of reciprocal space as
   pseudo-precession images
-  `hltofom <chltofom.html>`__ - Convert to/from Hendrickson-Lattman
   coefficients
-  `icoefl <icoefl.html>`__ - vectorially combined scaling of Fobs
   (Iobs) with partial Fc's
-  `import/edit protein sequence <get_prot.html>`__ - Import and edit
   protein sequences
-  `libcheck <libcheck.html>`__ - monomer library management program
-  `loggraph <loggraph.html>`__ - viewer for CCP4 formatted \`log' files
-  `lsqkab <lsqkab.html>`__ - apply various transformations to
   coordinate files
-  `makedict <makedict.html>`__ - converts PDB file to TNT or PROTIN
   dictionaries and PROTIN to PDB
-  `mama2ccp4 <mama2ccp4.html>`__ - Convert between \`mama' and
   Cambridge/CCP4 map formats
-  `map2fs <map2fs.html>`__ - convert CCP4 map to XtalView fsfour format
-  `mapdump <mapdump.html>`__ - print a dump of sections of a map file
-  `mapmask <mapmask.html>`__ - map/mask extend program
-  `maprot <maprot.html>`__ - map skewing, interpolating, rotating,
   averaging and correlation masking program
-  `mapsig <mapsig.html>`__ - print statistics on signal/noise for
   translation function map
-  `mapslicer <mapslicer.html>`__ - interactive section viewer for CCP4
   map files
-  `maptona4 <maptona4.html>`__ - Convert binary map file to and from
   na4 ascii format
-  `matthews\_coef <matthews_coef.html>`__ - Misha Isupov's Jiffy to
   calculate Matthews coefficient
-  `mlphare <mlphare.html>`__ - maximum likelihood heavy atom refinement
   and phase calculation
-  `molrep <molrep.html>`__ - automated program for molecular
   replacement
-  `mosflm <mosflm.html>`__ - MOSFLM version 7 for processing image
   plate and CCD data
-  `mrbump <mrbump.html>`__ - automated search model generation and
   automated molecular replacement
-  `mtz2cif <mtz2cif.html>`__ - produce mmCIF structure factor file
   suitable for deposition
-  `mtz2various <mtz2various.html>`__ - produces reflexion file for
   MULTAN, SHELX, TNT, X-PLOR/CNS, pseudo-SCALEPACK, XtalView, mmCIF or
   other ascii format
-  `mtzMADmod <mtzMADmod.html>`__ - Generate F+/F- or F/D from other for
   anomalous data
-  `mtzdump <mtzdump.html>`__ - dump data from an MTZ reflection data
   file
-  `mtzmnf <mtzmnf.html>`__ - Identify missing data entries in an MTZ
   file and replace with a Missing Number Flag (MNF)
-  `mtztona4 <mtztona4.html>`__ - interconvert MTZ reflection file and
   ASCII format
-  `mtzutils <mtzutils.html>`__ - Reflection data files utility program
-  `nautilus <cnautilus.html>`__ - Statistical Model Building
-  `ncont <ncont.html>`__ - computes various types of contacts in
   protein structures
-  `ncsmask <ncsmask.html>`__ - averaging mask manipulation program
-  `npo <npo.html>`__ - Molecule and map plotting
-  `oasis <oasis.html>`__ - A program for breaking phase ambiguity in
   OAS or SIR
-  `omit <omit.html>`__ - program to calculate omit-maps according to
   Bhat procedure
-  `othercell <othercell.html>`__ - explore equivalent alternative unit
   cells
-  `overlapmap <overlapmap.html>`__ - calculates the average of two maps
-  `pandda <pandda.html>`__ - Multi-dataset crystallographic analysis
   methods for ligands
-  `parrot <parrot.html>`__ - density modification package
-  `pdb\_extract <pdb_extract.html>`__ - RCSB/PDB Programs for
   extracting harvest information from program log files
-  `pdb\_merge <pdb_merge.html>`__ - merge two coordinate files into one
-  `pdbcur <pdbcur.html>`__ - various useful manipulations on coordinate
   files
-  `pdbset <pdbset.html>`__ - various useful manipulations on coordinate
   files
-  `peakmax <peakmax.html>`__ - search for peaks in the electron density
   map
-  `phaser-2.5.5 <phaser.html>`__ - Molecular Replacement, SAD Phasing,
   Anisotropy Correction, Cell Content Analysis, Translational NCSand
   Twin Analysis
-  `phistats <phistats.html>`__ - Analysis of agreement between phase
   sets, and checking it against weighting factors
-  `pirate <cpirate.html>`__ - Statistical Phase Improvement
-  `pisa <pisa.html>`__ - Protein interfaces, surfaces and assemblies
-  `pltdev <pltdev.html>`__ - convert Plot84 meta-files to PostScript,
   Tektronix or HPGL
-  `pointless <pointless.html>`__ - determine Laue group
-  `polarrfn <polarrfn.html>`__ - fast rotation function that works in
   polar angles
-  `privateer <privateer.html>`__ - Validation of carbohydrate
   structures
-  `procheck <procheck.html>`__ - programs to check the Stereochemical
   Quality of Protein Structures
-  `prodrg <cprodrg.html>`__ - generation of small molecule coordinates
-  `professs <professs.html>`__ - determination of NCS operators from
   heavy atoms
-  `prosmart <prosmart.html>`__ - Comparison of chains, and restraints
   generation
-  `r500 <r500.html>`__ - Checks PDB files for any problems before
   submission
-  `rampage <rampage.html>`__ - Ramachandran plots
-  `rantan <rantan.html>`__ - Direct Method module for the determination
   of heavy atom positions
-  `rapper <rapper.html>`__ - conformer modelling and building
-  `rebatch <rebatch.html>`__ - alter batch numbers in an unmerged MTZ
   file
-  `refmac5 <refmac5.html>`__ - macromolecular refinement program
-  `reindex <reindex.html>`__ - produces an MTZ file with h k l
   reindexed and/or the symmetry changed
-  `revise <revise.html>`__ - estimates optimised value of the
   normalised anomalous scattering using MAD data
-  `rfcorr <rfcorr.html>`__ - Analysis of correlations between cross-
   and self-Rotation functions
-  `rotamer <rotamer.html>`__ - List amino acids whose side chain
   torsion angles deviate from the Penultimate Rotamer Library
-  `rotgen <rotgen.html>`__ - Program to simulate X-ray diffraction
   rotation images
-  `rotmat <rotmat.html>`__ - interconverts CCP4/MERLOT/X-PLOR rotation
   angles
-  `rsps <rsps.html>`__ - heavy atom positions from derivative
   difference Patterson maps
-  `rstats <rstats.html>`__ - scale together two sets of F's
-  `rwcontents <rwcontents.html>`__ - Count atoms by type
-  `sapi <sapi.html>`__ - heavy atom site location
-  `sc <sc.html>`__ - analyse shape complementarity
-  `scala <scala.html>`__ - scale together multiple observations of
   reflections
-  `scaleit <scaleit.html>`__ - derivative to native scaling
-  `scalepack2mtz <scalepack2mtz.html>`__ - converts merged scalepack
   output into MTZ format
-  `sequins <sequins.html>`__ - Statistical Model Building
-  `seqwt <seqwt.html>`__ - Calculate molecular weight from sequence
-  `sfall <sfall.html>`__ - Structure factor calculation and X-ray
   refinement using forward and reverse FFT
-  `sfcheck <sfcheck.html>`__ - program for assessing the agreement
   between the atomic model and X-ray data
-  `sftools <sftools.html>`__ - reflection data file utility program
-  `sigmaa <sigmaa.html>`__ - Improved Fourier coefficients using
   calculated phases
-  `sketcher <sketcher.html>`__ - monomer library sketcher
-  `solomon <solomon.html>`__ - density modification (phase improvement)
   by solvent flipping
-  `sortmtz <sortmtz.html>`__ - Sort a MTZ reflection data file
-  `sortwater <sortwater.html>`__ - sort waters by the protein chain to
   which they "belong"
-  `stereo <stereo.html>`__ - Extract coordinates from stereo diagrams
-  `stgrid <stgrid.html>`__ - Generate plot to measure angular
   coordinates on a stereographic projection from polarrfn
-  `stnet <stnet.html>`__ - Generate plot to measure angles between
   points on a stereographic projection from polarrfn
-  `superpose <superpose.html>`__ - structural alignment based on
   secondary structure matching
-  `surface <surface.html>`__ - surface accessibility program and for
   preparing input file to program volume
-  `symconv <symconv.html>`__ - Fetch and convert symmetry and
   spacegroup information
-  `tffc <tffc.html>`__ - Translation Function Fourier Coefficients
-  `tlsanl <tlsanl.html>`__ - analysis of TLS tensors and derived
   anisotropic U factors
-  `tlsextract <tlsextract.html>`__ - extract TLS group description from
   a PDB file
-  `topdraw <topdraw.html>`__ - Sketchpad for protein topology diagrams
-  `topp <topp.html>`__ - a topological comparison program
-  `tracer <tracer.html>`__ - Lattice TRAnsformation and CEll Reduction
-  `truncate <truncate.html>`__ - obtain structure factor amplitudes
   using Truncate procedure
-  `unique <unique.html>`__ - Generate a unique list of reflections
-  `vecref <vecref.html>`__ - Vector-space refinement of heavy atom
   sites in isomorphous derivatives
-  `vectors <vectors.html>`__ - generates Patterson vectors from atomic
   coordinates
-  `volume <volume.html>`__ - polyhedral volume around selected atoms
-  `watertidy <watertidy.html>`__ - rationalise waters at the end of
   refinement
-  `watncs <watncs.html>`__ - Pick waters which follow NCS and sort out
   to NCS asymmetric unit
-  `watpeak <watpeak.html>`__ - selects peaks from peakmax and puts them
   close to the respective protein atoms
-  `wilson <wilson.html>`__ - Wilson plot, absolute scale and
   temperature factor
-  `xia2 <xia2.html>`__ - Automated data reduction
-  `xplot84driver <xplot84driver.html>`__ - a viewer for Plot84 meta
   files

UNSUPPORTED
-----------

-  `angles <angles.html>`__ - Bond lengths, bond angles and dihedral
   angles from coordinate files
-  `axissearch <axissearch.html>`__ - Check short reciprocal lattice
   vectors- there may be an alternative definition of unit cell
-  `compar <compar.html>`__ - Comparison of coordinates
-  `extends <extends.html>`__ - Extend Fourier maps and compute standard
   uncertainty of electron density
-  `havecs <havecs.html>`__ - Generate heavy atom vectors
-  `helixang <helixang.html>`__ - Program to calculate contacts, angles
   and separations between helices.
-  `mapexchange <mapexchange.html>`__ - Convert map between binary and
   formatted
-  `mapreplace <mapreplace.html>`__ - Combine parts of 2 maps
-  `mtz2sca <mtz2sca.html>`__ - Convert CCP4 mtz-files containing
   anomalous data to Scalepack format
-  `openastexviewer <AstexViewer.html>`__ - Java program for display
   molecular structures and electron density maps
-  `p842asc <p842asc.html>`__ - Read and write plot84 and ascii files
-  `postref <postref.html>`__ - Post-refinement of reflection data
-  `prelyscar <prelyscar_ccp4.html>`__ - Predictor of Lysine
   Carboxylation
-  `refindex <refindex.html>`__ - Reindex dataset maximising correlation
   with reference dataset
-  `reforigin <reforigin.html>`__ - Apply best origin shift to PDB atom
   co-ords according to reference file
-  `vecsum <vecsum.html>`__ - Program to deconvolute a Patterson
   function and solve the structure
-  `zeroed <zeroed.html>`__ - Set parts of map to zero

DEPRECATED
----------

-  `arp\_waters <arp_waters.html>`__ - Automated Refinement Procedure
   for refining protein structures
-  `beast <beast.html>`__ - Likelihood-based molecular replacement
-  `bplot <bplot.html>`__ - Plot temperature factor by residue
-  `ipdisp <ipdisp.html>`__ - displays images from a variety of
   (crystallographic) sources
-  `polypose <polypose.html>`__ - program for superimposing many
   multi-domain structures
-  `rdent <rdent.html>`__ - Create dictionary entries for Restrain from
   PDB file
-  `restrain <restrain.html>`__ - macromolecular refinement program
-  `rotaprep <rotaprep.html>`__ - produces an MTZ file in multirecord
   form suitable for input to SCALA.
-  `rsearch <rsearch.html>`__ - R-factor and correlation coefficient
   between Fcalc and Fobs
-  `xdldataman <xdldataman.html>`__ - manipulation, analysis and
   reformatting of reflection files
-  `xdlmapman <xdlmapman.html>`__ - manipulation, analysis and
   reformatting of electron density maps
-  `xloggraph <xloggraph.html>`__ - a viewer of specially formatted
   \`log' files

LIBRARY
-------

-  `c libraries <C_library/index.html>`__ - CCP4 C/C++ Software Library
-  `cciflib <cciflib.html>`__ - Subroutine Library for handling CCIF
   files
-  `ccplib <ccplib.html>`__ - utility subroutine library
-  `cmap library <C_library/cmap_page.html>`__ - C/C++ Software Library
   for CCP4 map files
-  `cmtz library <C_library/cmtz_page.html>`__ - C/C++ Software Library
   for MTZ files
-  `cparser library <C_library/cparser_page.html>`__ - C Software
   Library for CCP4-style parsing
-  `csym library <C_library/csym_page.html>`__ - C/C++ Software Library
   for symmetry information
-  `diffraction image <DiffractionImage.html>`__ - CCP4 Diffraction
   Image Handling Library
-  `diskio <diskio.html>`__ - low-level input/output subroutine library
-  `dna\_output <dna_output.html>`__ - DNA library for XML output from
   programs
-  `fffear fragment library <fffear_fraglib.html>`__ - map
   interpretation package
-  `harvlib <harvlib.html>`__ - Subroutine library for writing CIF
   harvest files
-  `keyparse <keyparse.html>`__ - high-level keyword parsing subroutines
-  `lib\_list <lib_list.html>`__ - contents of multi-purpose monomer
   dictionary used by REFMAC
-  `libhtml <libhtml.html>`__ - Subroutine Library for inserting html
   tags into log files
-  `library <library.html>`__ - low-level C subroutine library
-  `maplib <maplib.html>`__ - Subroutine Library for CCP4 map file
   handling
-  `modlib <modlib.html>`__ - Subroutine Library for mathematical
   operations
-  `mon\_lib <mon_lib.html>`__ - description of multi-purpose monomer
   dictionary used by REFMAC
-  `mtzlib <mtzlib.html>`__ - Fortran Software Library for Reflection
   data file handling
-  `parser <parser.html>`__ - parser subroutine library
-  `pxxml <pxxml.html>`__ - Subroutine Library for writing XML
-  `rotamer table <rotamer_table.html>`__ - rotamer tables used in the
   \`rotamer' program
-  `rwbrook <rwbrook.html>`__ - Library Subroutines for handling the
   co-ordinate working format
-  `symlib <symlib.html>`__ - Fortran Software Library for handling
   symmetry operations
-  `unix.m4 <unix.m4.html>`__ - library routines for system interactions
-  `xdl\_view <xdl_view.html>`__ - X-windows based toolkit

FILE FORMATS
------------

-  `intro\_mon\_lib <intro_mon_lib.html>`__ - description of monomer
   library used in CCP4
-  `loggraphformat <loggraphformat.html>`__ - Loggraph file format for
   CCP4
-  `mmcifformat <mmcifformat.html>`__ - mmCIF file format as used in
   CCP4
-  `mtzformat <mtzformat.html>`__ - MTZ file format for CCP4
-  `pdbformat <pdbformat.html>`__ - PDB file format for CCP4
