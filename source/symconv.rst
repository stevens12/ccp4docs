SYMCONV (CCP4: Supported Program)
=================================

NAME
----

**symconv** - fetches and converts symmetry operators and spacegroup
information

SYNOPSIS
--------

| **symconv**
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

SYMCONV is a utility program that allows interrogation of the CCP4
symmetry library. It provides a way to look up spacegroup information
(`SPACEGROUP <#spacegroup>`__ keyword), and to acquire data about
symmetry operations expressed in different formats.

INPUT AND OUTPUT FILES
----------------------

There are no input or output files associated with SYMCONV; all input is
via keywords and all output is to standard out.

KEYWORDED INPUT
---------------

The program is designed to be able to operate in an interactive manner,
so the order that keywords are supplied are important - the output of
later commands e.g. `OP <#op>`__ will be affected by commands entered
previously in that run.

The available keywords are:

    `**OP** <#op>`__, `**XDS** <#xds>`__, `**NOXDS** <#noxds>`__,
    `**CELL** <#cell>`__, `**NOCELL** <#nocell>`__,
    `**RCSB** <#rcsb>`__, `**NORCSB** <#norcsb>`__,
    `**SPACEGROUP** <#spacegroup>`__,
    `**NOSPACEGROUP** <#nospacegroup>`__, `**END** <#end>`__

OP <symop\_string>
~~~~~~~~~~~~~~~~~~

The OP keyword must be followed by a symmetry operation expressed in
string format, e.g. x,y,z or x-y,x,z+1/2 (real space) or h,-l,k
(reciprocal space).

The program will print the equivalent symmetry matrix, and for
reciprocal space operations it will also print the XDS REIDX command. If
a cell has previously been supplied via the `CELL <#cell>`__ keyword
then the symmetry operation matrix to be applied to real space atom
coordinates will also be printed. If a spacegroup has previously been
supplied via the `spacegroup <#spacegroup>`__ command then the program
will identify the sym.id for the operation, plus any additional lattice
translations that may have been applied (for real space operations
only). If the RCSB symmetry data is available then it will also print
the RCSB sym.id (see the `RCSB <#rcsb>`__ keyword).

XDS
~~~

Specifying the XDS keyword means that an XDS REIDX line will also be
written out as part of the output of an `OP <#op>`__ keyword, when a
reciprocal space symmetry operation is supplied. This is the default,
and can be turned off using the `NOXDS <#noxds>`__ keyword.

NOXDS
~~~~~

Turn off the output of an XDS REIDX line by the `OP <#op>`__ keyword.
Turn back on using the `XDS <#xds>`__ keyword.

CELL <a> <b> <c> [<alpha> <beta> <gamma>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Specify a unit cell to be used in conjunction with `OP <#op>`__
keywords. Cell lengths are in Angstroms and cell angles are in degrees;
if the angles are not specified then they default to 90 degrees.

After a CELL keyword is given the program will generate and print the
fractionalising and orthogonalising matrices. It will also store the
cell parameters and use them for subsequent `OP <#OP>`__ keywords, to
generate the real space symmetry matrix. Use the `NOCELL <#nocell>`__
keyword to clear the cell parameters.

NOCELL <a> <b> <c> [<alpha> <beta> <gamma>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clear the unit cell parameters specified by a previous `CELL <#cell>`__
keyword.

RCSB
~~~~

Turn on the printing of the RCSB sym.ids for
`SPACEGROUP <#spacegroup>`__ and `OP <#op>`__ keywords (this is the
default).

Use the `NORCSB <#norcsb>`__ keyword to turn off this behaviour.

NORCSB
~~~~~~

Turn off printing of the RCSB sym.ids (see the `RCSB <#rcsb>`__
keyword).

SPACEGROUP <sg name>
~~~~~~~~~~~~~~~~~~~~

Look up a spacegroup in the syminfo.lib file and return information such
as number and a list of symmetry operations and sym.ids (sym.id is the
position of the operator in the list).

After a SPACEGROUP keyword is given the program will also store the
spacegroup information (if the spacegroup was found in the library), and
will attempt to look up symmetry operations given by subsequent
`OP <#op>`__ keywords. If the symmetry operation is found in the list of
operators for the current spacegroup then its sym.id is reported. Use
the `NOSPACEGROUP <#nospacegroup>`__ keyword to clear the spacegroup
information.

NOSPACEGROUP
~~~~~~~~~~~~

Clear the spacegroup data specified by a previous
`SPACEGROUP <#spacegroup>`__ keyword.

END
~~~

End of input, also end-of-file will do.

KNOWN PROBLEMS
--------------

Due to a bug in the CCP4 parser library in releases 6.0.1 and earlier,
the RCSB lookup functions may not work correctly. This has been fixed
for subsequent CCP4 versions (6.0.2 and later).

EXAMPLES
--------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `symconv.exam <../examples/unix/runnable/symconv.exam>`__

AUTHOR
------

| Peter Briggs, CCLRC Daresbury Laboratory
| August 2006

The program also includes spacegroup data kindly provided by the
RCSB-PDB.

SEE ALSO
--------
