|contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

1. Introduction
===============

Aims
----

These Operating Instructions describe how to run the **PROCHECK** suite
of programs (`Laskowski et al., 1993 <manrefs.html#PROCHECK>`__) for
assessing the "stereochemical quality" of a given protein structure. The
instructions assume that the programs have already been installed on
your computer system. If this is not the case, please refer to the
separate **Installation Guide** which deals with the installation
procedures for your particular system.

The aim of **PROCHECK** is to assess how **normal**, or conversely how
**unusual**, the geometry of the residues in a given protein structure
is, as compared with stereochemical parameters derived from
well-refined, high-resolution structures.

Unusual regions highlighted by **PROCHECK** are not necessarily
**errors** as such, but **may** be unusual features for which there is a
reasonable explanation (*eg* distortions due to ligand-binding in the
protein's active site). Nevertheless they are regions that should be
checked carefully.

Stereochemical parameters
-------------------------

The stereochemical parameters used are those described in detail in
`Morris et al. (1992) <manrefs.html#MORRIS>`__. These parameters, which
are for the most part not included in standard refinement procedures
(and so are less likely to be biased by them), are listed in `Table 1 of
Appendix A <manappa.html#TABLE_A.1>`__.

The checks also make use of "ideal" bond lengths and bond angles, as
derived from a recent and comprehensive analysis `(Engh & Huber,
1991) <manrefs.html#ENGH>`__ of small molecule structures in the
Cambridge Structural Database, CSD `(Allen et al.,
1979) <manrefs.html#CSD>`__ - now numbering over 100,000 structures.
These "ideal" values are listed in `Table 2 of Appendix
A <Appendix_A.html#TABLE_A.2>`__.

Input
-----

The input to **PROCHECK** is a single file containing the coordinates of
your protein structure. This must be in Brookhaven file format (see
`Appendix B <manappb.html>`__). One of the by-products of running
**PROCHECK** is that your coordinates file will be "cleaned up" by the
first of the programs. The cleaning-up process corrects any mislabelled
atoms and creates a new coordinates file which has a file-extension of
**.new**. The **.new** file will have the atoms labelled in accordance
with the **IUPAC** naming conventions `(IUPAC-IUB Commission of
Biochemical Nomenclature, 1970) <manrefs.html#IUPAC>`__.

**Note.** At present, hydrogen atoms are ignored. Atoms with zero
occupancy are omitted from the analyses and only the highest occupancy
conformations are retained wherever there are alternate conformations.

Outputs
-------

The outputs comprise a number of plots, together with a detailed
residue-by-residue listing. The plots are output in PostScript format
`(Adobe Systems Inc., 1985) <manrefs.html#ADOBE>`__, and so can be
printed off on a PostScript laser printer, or displayed on a graphics
screen using suitable software (*eg* **GHOSTSCRIPT** or **GHOSTVIEW** on
Sun or Silicon Graphics workstations, or **PSVIEW** on Silicon Graphics
IRIS-4D systems). The plots can be in colour or black-and-white and are
described in `Sample plots <examples/index.html.html>`__. The
residue-by-residue listing is described in `Appendix
D <manappd.html>`__.

Programs
--------

The suite comprises a number of programs which are described in
`Appendix C <manappc.html>`__.

--------------

|contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |contents page| image:: uupb.gif
   :target: index.html
.. | | image:: 12p.gif
.. |next section| image:: rightb.gif
   :target: man2.html
