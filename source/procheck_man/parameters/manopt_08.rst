|previous page| | | |plot parameters| | | |next page| | |\ *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

8. Main-chain bond angle distributions
======================================

There are **7** parameters defining the `**Main-chain bond angle
distributions** <../examples/plot_08.html>`__, as follows:-

--------------

::

    8. Main-chain bond angle distributions
    --------------------------------------
    Y     <- Background shading on graphs (Y/N)?
    2.0   <- Number of standard deviations for highlighting
    N     <- Produce a COLOUR PostScript file (Y/N)?
    WHITE        <- Background colour of page
    CREAM        <- Background shading on graphs
    PURPLE       <- Colour of histogram bars
    RED          <- Colour of highlighted histogram bars

--------------

Description of options:-
------------------------

-  **Background shading** - This option determines whether the graph
   background of each plot is to be lightly shaded when the plot is in
   black-and-white.
-  **Number of standard deviations for highlighting** - This option
   determines which histogram bars in the plot are to be highlighted.
   Residues whose plotted parameter deviate by more than the number of
   standard deviations specified here will be highlighted.
-  **Produce a COLOUR PostScript file** - This option defines whether a
   colour or black-and-white plot is required. Note that if the `Colour
   all plots <manopt_0a.html>`__ option is set to **Y**, a colour
   PostScript file will be produced irrespective of the setting here.

   The colour definitions on the following lines use the \`names' of
   each colour as defined in the colour table at the bottom of the
   parameter file (see `Colours <manopt_cols.html>`__). If the name of a
   colour is mis-spelt, or does not appear in the colour table, then
   **white** will be taken instead. Each colour can be altered to suit
   your taste and aesthetic judgement, as described in the
   `Colours <manopt_cols.html>`__ section.

-  **Background colour** - This option defines the background colour of
   the page on which the plots are drawn.
-  **Colours** - The various additional colours on the lines that follow
   define the colours for the highlighted and unhighlighted histogram
   bars, and the background colour of each graph.

--------------

|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_07.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_09.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_09.html
