|plot parameters| | | |next page| | |\ *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Colour all plots?
=================

This is a single parameter which determines whether all the plots are to
be produced as **colour** or **black-and-white** PostScript files.

--------------

::

    Colour all plots?
    -----------------
    Y    <- Produce all plots in colour (Y/N)?

--------------

Description
-----------

If the option is set to **Y**, then colour files will be generated for
**all** plots.

If it is set to **N**, each plot will be generated in colour or
black-and-white depending on the colour option for that particular plot
(see parameters for individual plots).

--------------

|plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. | | image:: ../12p.gif
.. |next page| image:: ../rightr.gif
   :target: manopt_0b.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_0b.html
