|previous listing| | | |plots listing| | | |next plot| | |\ *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Plot 6. Residue properties
==========================

|image4|

Description
-----------

The various graphs and diagrams on this plot show how the protein's
geometrical properties vary along its sequence. This gives a
visualization of which regions appear to have consistently poor or
unusual geometry (perhaps because they are poorly defined) and which
have more normal geometry.

The properties plotted are:-

-  **Graphs a-c: Optional properties**

   The first three graphs at the top of the page, can be selected from
   **14** possibles by the user. The three default graphs, which are
   plotted when you first run **PROCHECK**, are the first three of:-

   For each graph, **unusual** values (usually those more than **2.0**
   standard deviations away from the "ideal" mean value) are shown
   **highlighted**.

-  **Graph d: Secondary structure & average estimated accessibility**

   The **secondary structure plot** shows a **schematic** representation
   of the **`Kabsch & Sander (1983) <../manrefs.html#KABSCH>`__**
   secondary structure assignments. The key just below the picture shows
   which structure is which. Beta strands are taken to include all
   residues with a Kabsch & Sander assignment of **E**, helices
   corresponds to both **H** and **G** assignments, while everything
   else is taken to be random coil.

   The shading behind the schematic picture gives an **approximation**
   to the **residue accessibilities**. The approximation is a fairly
   crude one, being based on each residue's **Ooi** number `(Nishikawa &
   Ooi, 1986) <../manrefs.html#OOI>`__. An **Ooi** number is a count of
   the number of other **C\ *alpha*** atoms within a radius of, in this
   case, **14Å** of the given residue's own **C\ *alpha***. Although
   crude, this does give a good impression of which parts of the
   structure are buried and which are exposed on the surface. Future
   versions of **PROCHECK** will include an accurate calculation of
   residue accessibility.

-  **Graph e: Sequence & Ramachandran regions**

   The next section shows the **sequence** of the structure (using the
   20 standard one-letter amino-acid codes) and a set of markers that
   identify the **region** of the **`Ramachandran
   plot <plot_01.html>`__** in which each residue is located. There are
   **four** marker types, one for each of the four different types of
   region: **core** (*ie* most favoured), **allowed**, **generous** and
   **disallowed**.

-  **Graph f: Max. deviation**

   The small histogram of **asterisks** and **plus-signs** shows each
   residue's **maximum deviation** from one of the ideal values given on
   the **`residue-by-residue listing <../manappd.html>`__** in the
   **.out** file. Refer to the final column of the **.out** file to see
   which is the parameter that deviates by the amount shown here.

-  **Graph g: *G*-factors**

   The shaded squares give a schematic representation of each residue's
   `***G*-factor** <../manappe.html>`__ values. (Note that the **chi-1**
   *G*-factors are shown only for those residues that do not have a
   **chi-2**, and hence no **chi1-chi2** *G*-factor).

   Regions with many dark squares correspond to regions where the
   properties are "**unusual**", as defined by a low (or negative)
   *G*-factor. These may correspond to highly mobile or poorly defined
   regions such as loops, or may need further investigation.

Options
-------

The main options for the plot are:-

-  Which 3 of 14 plots to be printed in the 3 main graphs at the top of
   the page.
-  Number of standard deviations for highlighting outliers (default is
   **2.0**).
-  The plot can be in colour or black-and-white.

These options can be altered by editing the parameter file,
**procheck.prm**, as described `here <../parameters/manopt_06.html>`__.

--------------

|previous listing| | | |plots listing| | | |next plot| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous listing| image:: ../leftr.gif
   :target: plot_05.html
.. | | image:: ../12p.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_07.html
.. |image4| image:: plot_06.gif
   :width: 306px
   :height: 434px
   :target: plot_06.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_07.html
