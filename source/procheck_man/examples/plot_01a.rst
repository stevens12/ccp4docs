| | |plots listing|

--------------

Ramachandran plot regions
=========================

The different regions on the **Ramachandran plot** are as described in
`Morris et al. (1992) <../manrefs.html#MORRIS>`__.

The regions are labelled as follows:

::

        A   - Core alpha            L   - Core left-handed alpha   
        a   - Allowed alpha         l   - Allowed left-handed alpha
       ~a   - Generous alpha       ~l   - Generous left-handed alpha
        B   - Core beta             p   - Allowed epsilon          
        b   - Allowed beta         ~p   - Generous epsilon         
       ~b   - Generous beta

The different regions were taken from the observed **phi-psi**
distribution for **121,870** residues from **463** known X-ray protein
structures. The two most favoured regions are the **"core"** and
**"allowed"** regions which correspond to **10°** x **10°** pixels
having more than **100** and **8** residues in them, respectively. The
**"generous"** regions were defined by `Morris et al.
(1992) <../manrefs.html#MORRIS>`__ by extending out by **20°** (two
pixels) all round the **"allowed"** regions. In fact, the authors found
very few residues in these **"generous"** regions, so they can probably
be treated much like the **"disallowed"** region and any residues in
them investigated more closely.

Ideally, one would hope to have over **90%** of the residues in the
**"core"** regions. The percentage of residues in the **"core"** regions
is one of the better guides to the stereochemical quality of a protein
structure.

--------------

|plots listing| | |

.. | | image:: ../12p.gif
.. |plots listing| image:: ../uupr.gif
   :target: plot_01.html
.. |plots listing| image:: ../uupr.gif
   :target: plot_01.html
