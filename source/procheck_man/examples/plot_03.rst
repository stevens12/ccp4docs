|previous listing| | | |plots listing| | | |next plot| | |\ *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Plot 3. Chi1-Chi2 plots
=======================

|image4|

Description
-----------

The Chi1-Chi2 plots show the **chi1-chi2** sidechain torsion angle
combinations for all residue types whose sidechains are long enough to
have both these angles.

The **shading** on each plot indicates how **favourable** each region on
the plot is; the darker the shade the more favourable the region. The
data on which the shading is based has come from a data set of **163**
non-homologous, high-resolution protein chains chosen from structures
solved by X-ray crystallography to a resolution of **2.0Å** or better
and an *R*-factor no greater than **20%**.

The **numbers in brackets**, following each residue name, show the total
number of **data points** on that graph. The **red numbers** above the
data points are the **reside-numbers** of the residues in question (*ie*
showing those residues lying in unfavourable regions of the plot).

Options
-------

The main options for the plot are:-

-  The cut-off value for the *G*-factor defining which points are to be
   labelled.
-  The plot can be in colour or black-and-white.

These options can be altered by editing the parameter file,
**procheck.prm**, as described `here <../parameters/manopt_03.html>`__.

--------------

|previous listing| | | |plots listing| | | |next plot| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous listing| image:: ../leftr.gif
   :target: plot_02.html
.. | | image:: ../12p.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_04.html
.. |image4| image:: plot_03.gif
   :width: 306px
   :height: 434px
   :target: plot_03.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_04.html
