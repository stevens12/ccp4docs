|plots listing| | | |next plot| | |\ *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Plot 1. Ramachandran plot
=========================

|image3|

Description
-----------

The **Ramachandran plot** shows the **phi-psi** torsion angles for all
residues in the structure (except those at the chain termini). Glycine
residues are separately identified by triangles as these are not
restricted to the regions of the plot appropriate to the other sidechain
types.

The colouring/shading on the plot represents the different
`regions <plot_01a.html>`__ described in `Morris et al.
(1992) <../manrefs.html#MORRIS>`__: the **darkest** areas (here shown in
*red*) correspond to the **"core"** regions representing the most
**favourable** combinations of **phi-psi** values.

Ideally, one would hope to have over **90%** of the residues in these
**"core"** regions. The percentage of residues in the **"core"** regions
is one of the better guides to stereochemical quality.

**Note that** additional **Ramachandran plots** can also be generated,
as follows:-

-  Separate plots for each of the **20** different **amino acid types**
   (see `Plot 2. Ramachandran plots by residue type <plot_02.html>`__).
-  Separate plots for just the **Gly & Pro** residues (available as an
   option in `Plot 2. Ramachandran plots by residue
   type <plot_02.html>`__).

Options
-------

The main options for the Ramachandran plot are:-

-  Labelling of residues in disallowed regions can be switched off, or
   alternatively can be extended into the other regions.
-  Shading/colouring of the different regions can be switched off.
-  The plot can be in colour or black-and-white.
-  A "publication version" of the plot (without the outer border and
   statistics) can be generated.

These options can be altered by editing the parameter file,
**procheck.prm**, as described `here <../parameters/manopt_01.html>`__.

--------------

|plots listing| | | |next plot| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. | | image:: ../12p.gif
.. |next plot| image:: ../rightr.gif
   :target: plot_02.html
.. |image3| image:: plot_01.gif
   :width: 306px
   :height: 434px
   :target: plot_01.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_02.html
