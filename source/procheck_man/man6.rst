|previous section| | | |contents page| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

6. Viewing G-factors in 3D
==========================

A simple utility program called **gfac2pdb** is provided with
**PROCHECK** to allow you to visualize a given structure's **dihedral
angle `*G-*\ factors <manappe.html>`__** in **3D** using any standard
molecular graphics software package.

The program generates an output file in **PDB** format with the
following atoms' temperature factors replaced by values representing the
*G-*\ factors of the corresponding torsion angles:-

::

        G-factor              Atoms
        --------              -----
        phi-psi               N and CA
        omega                 Backbone C
        chi1-chi2             CB and CG
        chi3                  CD
        chi4                  CE

All other atoms' temperature factors are set to **0**.

You can view the output **PDB** file using any molecular graphics
program (such as **QUANTA** or **RasMol**), and colour the atoms
according to *B-value. The **red** regions of the structure will be
those with **unusual** torsion angles, corresponding to negative
*G-*\ factors.*

Any large regions of redness might indicate parts of the structure that
need further attention. However, please note that some programs (*eg*
**RasMol**) scale the colours according to the maximum and minimum
*B*-values they encounter in the file. So, there will always be red
regions, corresponding to the maximum *B*-values, irrespective of how
large or small the absolute values of those B-values are.

You can overcome this problem by, say, inserting a **dummy atom** into
your displayed **PDB** file which has a *B*-value of **80.00** (which is
the maximum value that **gfac2pdb** gives). The atom might, for example,
be:

::

    HETATM    1  N   XXX     1       0.000   0.000   0.000  1.00 80.00

In **RasMol** you can "hide" this atom by using the command "**select
not xxx**".

Examples
~~~~~~~~

| | |image3| `All atoms <man6a.html>`__ | | |image4| `Mainchain
atoms <man6b.html>`__

--------------

Running gfac2pdb
----------------

Before running **gfac2pdb** you must first run **PROCHECK** on your
structure so that all the *G-*\ factors are calculated.

Then, to run **gfac2pdb**, enter:

where:

-  ***coordinate\_file*** is the name of the **PDB** coordinates file.

For example,

The output file will be called **1abc\_gfact.pdb**.

--------------

|previous section| | | |contents page| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous section| image:: leftb.gif
   :target: man5.html
.. | | image:: 12p.gif
.. |contents page| image:: uupb.gif
   :target: index.html
.. |image3| image:: man6a.gif
   :width: 125px
   :height: 80px
   :target: man6a.html
.. |image4| image:: man6b.gif
   :width: 125px
   :height: 80px
   :target: man6b.html
