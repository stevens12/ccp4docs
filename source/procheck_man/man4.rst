|previous section| | | |contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

4. PROCHECK outputs
===================

**PROCHECK** generates a number of output files in the default directory
which have the same name as the original **PDB** file, but with
different extensions.

-  .. rubric:: PostScript files (see `Sample
      plots <examples/index.html>`__)
      :name: postscript-files-see-sample-plots

   The output **PostScript** files all have an extension of **.ps**,
   their names being:

   ::

             filename_01.ps  }
             filename_02.ps  }
             filename_03.ps  } One PostScript file per plot
               ...
             filename_10.ps  }

   where *filename* is the root name of the original **PDB** file.

   You can make these filenames more self-descriptive by opting to add
   `**File-handles** <parameters/manopt_file.html>`__ to the names. The
   output filename become:-

   ::

             filename_01_ramachand.ps - Ramchandran plot
             filename_02_ramglypro.ps - Gly & Pro Ramchandran plot
               ...  etc.

   The full list of descriptors is given in
   `**File-handles** <parameters/manopt_file.html>`__.

   The plots show each of the different **PROCHECK** analyses generated
   by the run. They can be sent directly to a **PostScript** printer or
   viewed on a graphics or X Windows terminal.

   Examples of all the plots produced by **PROCHECK** are given in
   `Sample plots <examples/index.html>`__.

-  .. rubric:: Residue-by-residue listing (see `Appendix
      D <manappd.html>`__)
      :name: residue-by-residue-listing-see-appendix-d

   The residue-by-residue listing has a **.out** extension and lists all
   the computed stereochemical properties, by residue, in a printable
   ASCII text file. It is described in detail in `Appendix
   D <manappd.html>`__.

-  .. rubric:: Other output files
      :name: other-output-files

   The other files output by **PROCHECK** have the following
   extensions:-

   ::

             .lan    Main-chain bond lengths and bond angles used by the plotting programs
             .nb     List of atom-pairs making near-neighbour contacts
             .new    "Cleaned-up" version of the original coordinates file
             .pln    Coordinates of atoms in planar groups
             .rin    Residue information used by the plotting programs
             .sco    Main-chain and side-chain properties
             .sdh    Residue-by-residue G-factors
             .sum    PROCHECK summary results

   Most of these files are used internally by the **PROCHECK** suite and
   are unlikely to be of much use to you. The more informative ones are
   the following:-

   -  **.new file** The .new file holds the \`cleaned-up' version of the
      original **PDB** file, with any wrong atom-labels corrected in
      accordance with the **IUPAC** naming conventions `(IUPAC-IUB
      Commission of Biochemical Nomenclature,
      1970) <manrefs.html#IUPAC>`__.
   -  **.sum file** The .sum file gives a short summary of the overall
      **PROCHECK** RESULTS.

-  .. rubric:: Log files
      :name: log-files

   Each program in the suite also produces its own log file. Should the
   **PROCHECK** suite crash, or give strange-looking results, these log
   files should be the first place you look for a reason for the
   problem. The **7** files are:

   ::

            anglen.log   clean.log    pplot.log    tplot.log
            bplot.log    nb.log       secstr.log

--------------

|previous section| | | |contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous section| image:: leftb.gif
   :target: man3.html
.. | | image:: 12p.gif
.. |contents page| image:: uupb.gif
   :target: index.html
.. |next section| image:: rightb.gif
   :target: man5.html
