|contents page| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Acknowledgements
================

We would like to thank **Eleanor Dodson, Andrej Sali, Keith Wilson,
Victor Lamzin, Mike Sutcliffe, George Sheldrick, Paula Kuser, Helen
Stirk, V Dhanaraj** and **Geoff Barton** for immensely helpful comments,
criticisms and suggestions.

The preparation of **PROCHECK v.1.0** was partly funded by Oxford
Molecular Ltd. Funding for subsequent versions have come from the
**BIOTECH** program of **DGXII** of the **Commission of the European
Union**, under contract number **BIO2CT-920524**. Roman Laskowski would
also like to gratefully acknowledge **Parke-Davis Pharmaceutical
Research** and the **BBSRC** for financial support.

| *Original HTML version of this manual prepared by `Michael
  Scharf <http://www.embl-heidelberg.de/~scharf/>`__ (March 1994).*
| Current version prepared by `Roman A
  Laskowski <http://www.biochem.ucl.ac.uk/~roman/>`__.

--------------

|contents page| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |contents page| image:: uupb.gif
   :target: index.html
.. | | image:: 12p.gif

