COMBAT (CCP4: Supported Program)
================================

NAME
----

**combat** - produces an MTZ file in multirecord form suitable for input
to SCALA.

SYNOPSIS
--------

| **combat hklin** *foo.mtz* **hklout** *foo.mtz*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

COnvert to Multi BATch MTZ files. This program converts output from
assorted data processing programs into a `multirecord MTZ
file <#files_output>`__ suitable for input to the CCP4 scaling and
merging programs (`SCALA <scale.html>`__ and
`TRUNCATE <truncate.html>`__). Only **mosflm** writes such an MTZ file.
The program is a replacement program for ROTAPREP which was replaced as
it was very difficult to support.

Input types accepted at present are:
`**DATRED\_RIGAKU** <#datred_rigaku>`__, `**DENZO** <#denzo>`__,
`**JIMS** <#jims>`__, `**MTZF** <#mtzf>`__, `**MTZI** <#mtzi>`__,
`**MUF** <#muf>`__, `**MUI** <#mui>`__, `**RAXISDUMP** <#raxisdump>`__,
`**SAINT** <#saint>`__, `various **SCALEPACK** options <#scal_nm2>`__ ,
`**TEXHKL** <#texhkl>`__, `**TEXSAN** <#texsan>`__,
`**USER** <#user>`__, `**WEISS** <#weiss>`__,
`**XDSASCII** <#xdsascii>`__, `**XDSUNIQUE** <#xdsunique>`__.

Some of these are quite rare, but it seems sensible to keep them. It
would be trivial to extend the program for any format.

Warning: It is quite likely that the authors of processing programs will
change their formats without telling CCP4. If you have any queries or
knowledge of such changes please contact us.

Datasets and Harvesting
^^^^^^^^^^^^^^^^^^^^^^^

Datasets should be assigned project, crystal and dataset names, using
the `NAME <#name>`__ keyword. These are written to the output MTZ file
and are used in subsequent processing to categorise the data. The
project and dataset names are also utilised for `Data
Harvesting <harvesting.html>`__.

INPUT FORMATS
-------------

The following arguments to the `INPUT <#input>`__ keyword are supported.
The most commonly used arguments are those used to convert existing MTZ
files, or options for DENZO/SCALEPACK outputs (keywords
`**SCALEPACK** <#scalepack>`__, `**SCAL\_NM** <#scal_nm>`__ and
`**SCAL\_NM2** <#scal_nm2>`__). These are described first. The items in
the descriptions of the format have the following interpretations:

h k l
    Miller indices (possibly unique ones);
F sigF
    Measured structure factor amplitude and its standard deviation;
I sigI
    Measured intensity and its standard deviation;
delI sigdelI
    Anomalous intensity difference and its standard deviation;
ident / ibatch
    Batch number or equivalent;
mpart / fpart
    Partiality flag (in some convention).

MTZI
~~~~

Standard MTZ file containing h k l I sigI or h k l I+ sigI+ I- sigI-. It
is possible to read in a standard MTZ format file and produce a
\`multirecord' MTZ file. You should **only** use merged data as a
`reference \`batch' for SCALA <scala.html#run_reference>`__.

MTZF
~~~~

Standard MTZ file containing either h k l F sigF or h k l F(+) sigF(+)
F(-) sigF(-). It is possible to read such an MTZ format file and produce
a \`multirecord' MTZ file. You should **only** use merged data as a
`reference \`batch' for SCALA <scala.html#run_reference>`__. This is
probably most useful for producing test data from Fcalc

SCALEPACK
~~~~~~~~~

Output from **SCALEPACK**
[``h k l  I SigI  or h k l I+ SigI+ I- SigI-``] in format (3i4,4f8.0).

*N.B.* Run `**SCALEPACK2MTZ** <scalepack2mtz.html>`__ to generate an MTZ
file containing h k l I sigI or h k l I+ sigI+ I- sigI- then input this
as MTZI.

SCAL\_NM2
~~~~~~~~~

Output from **SCALEPACK** option **NOMERGE original indices**
[``h k l h0 k0 l0 ibatch I SigI``] in format (6I4,i6,7x,2f8.0). Scales
have been applied and partials summed. The Friedel pairs are separate in
this case.

SCAL\_NM
~~~~~~~~

Output from **SCALEPACK** option **NOMERGE no partials**
[``h k l ibatch isym I SigI (Fpart)``] in format (4i4,i3,2f8.0,f7.2).
Scales have been applied and partials summed. The Friedel pairs are
separate in this case.

DATRED\_RIGAKU
~~~~~~~~~~~~~~

[``h k l F sigF``] in format (3i4,4f8.1).

JIMS
~~~~

| **MADNES for050** files.
| [``h k l I sigI``] in format (3i5,2f10.2).

MUI
~~~

| For *intensities* output from the **XENTRONICS** and **XENGEN** data
  processing packages.
| [format (3i4, 1x, F6.4, 6(1x,f8.2), 1x, i2).]

MUF
~~~

| For *amplitudes* output from the **XENTRONICS** and **XENGEN** data
  processing packages.
| [format (3i4, 1x, F6.4, 6(1x,f8.2), 1x, i2).]

RAXISDUMP
~~~~~~~~~

| Output from Raxis READBF of mergefile.
| [``mpart h k l I sigI ibatch``] in format (21x, I4, 4x, 3i4, 8x,
  2f10.3, i4).

Note: if you use the \`log' file produced under unix rather than the
\`output' file you may need to change the format to eat an extra space,
*i.e.* start it with \`22x'. Note also that you may find alternative
processing packages to be better than the Raxis software.

DENZO
~~~~~

*This option may cause serious trouble further down the line. Do not use
unless you know exactly what you are doing.*

Concatenated output files from **DENZO**. These contain HEADER blocks
and reflection data. [``h k l fsqp I sigI fpart phi``] in format (3I4,
2X, 2F8.0, 7X, F6.0, 6X, 2F7.0, 14X, 2F6.0)

Care is needed when doing this, since no post-refinement has been done.
We now prefer to run **SCALEPACK** to refine the cell and orientation,
then *either* run DENZO again to re-integrate the images without
re-refining the cell *etc.* (however there may still be problems with
partiality flags), *or* output an unmerged file from scalepack (see
keywords `SCAL\_NM <#scal_nm>`__ and `SCAL\_NM2 <#scal_nm2>`__).

*N.B.* If using the commercialised Denzo identified like:

::

      HKL data processing system Version 1.0 FT1.31 Rev1.4

you need to use the Denzo command

::

      film york output file <filename>

to produce a suitable format for \`combat'. The \`york' output can also
be read into \`scalepack' with

::

      format denzo_york1

SAINT
~~~~~

Output from SAINT, one batch only (not fully tested).

TEXSAN
~~~~~~

| Output from **TEXSAN** data processing package for Rigaku
  diffractometer.
| [``h k l ident I sigI``] in format (3i4, i3, 5x, f8.1, f7.1).

TEXHKL
~~~~~~

| Output from **TEXSAN** data processing package for Rigaku
  diffractometer.
| [``h k l I sigI scale``] in format (3x, 3i4, 2F10.2, 29x, F7.4).

USER
~~~~

ASCII data file in a user-defined format, see `LABEL <#label>`__ and
`FORMAT <#format>`__ keywords.

WEISS
~~~~~

Output from the **WEISS** data processing package for Japanese
Weissenberg data. [``h k l mpart I sigI ident``] in format (5x, 4i4,
2f8.2, i10).

XDSASCII
~~~~~~~~

Output from Kabsch's **XDS** data processing package in the standard
text file **XDS\_ASCII.HKL** from the **CORRECT** stage.

In the XDS file, a negative sign is attached to SIGMA(IOBS) to indicate
a MISFIT (IOBS is incompatible with symmetry equivalent reflection
intensities). These reflections are kept in the MTZ output on the
assumption that SCALA will deal appropriately with them.

This option may work with output from **XSCALE** where the reflections
have been scaled but not merged ( MERGE=FALSE ). However, the resulting
MTZ file will likely be missing information that `SCALA <scala.html>`__
needs (depending on the scaling options chosen). You might get this to
work by simplifying the scaling model in SCALA, or by adding records to
the header of the XDS\_ASCII.HKL file, such as OSCILLATION\_RANGE.

This option will not work with merged output from **XSCALE** (
MERGE=TRUE ). The program `F2MTZ <f2mtz.html>`__ should be used.

XDSUNIQUE
~~~~~~~~~

| Output from older versions of Kabsch's **XDS** data processing package
  (or the MAR Research version) in the standard text file **UNIQUE.HKL**
  from the CORRECT stage. [The output of the **MARSCALE** program (and
  possibly the original **XSCALE**) can also be put in this form,
  although there is probably no reason to prefer these to
  `**scala** <scala.html>`__]. This file contains symmetry averaged
  reflection intensities and anomalous differences after scaling.
| [``h k l I sigI delI sigdelI``] in format (3I5, 4E12.4)

Since UNIQUE output is scaled and merged, it is sensible to use
`f2mtz <f2mtz.html>`__ to produce an MTZ file for direct input to
`truncate <truncate.html>`__.

DESCRIPTION OF THE OUTPUT MTZ FILE
----------------------------------

The output file has the column labels:

H K L
    Miller indices.
M/ISYM
    256\*M + ISYM, where the partiality flag M=0 for fully recorded
    reflexions and M=1 for partials, and ISYM is the symmetry number
    defined as follows. For I+ of a Friedel pair, ISYM = 2\*isymop - 1;
    for I- of a Friedel pair, ISYM = 2\*isymop, where "isymop" is the
    number of the symmetry operator used to place the reflection in the
    asymmetric unit.
BATCH
    Batch (serial) number for this film/image.
I, sigI
    Corrected integrated intensity and its standard deviation.
IPR, sigIPR
    is meant to be corrected profile-fitted intensity and SD. Here IPR
    and sigIPR are set equal to I and sigI if there is no appropriate
    information input.
FRACTIONCALC
    This is the calculated partial fraction of a spot (>0.5 if partial
    more than half present). For most inputs FRACTIONCALC is a dummy set
    to 3.0 or 0.5 for a partial.
XDET, YDET, ROT
    Detector position and PHI angle. This information is available for
    `**XDSASCII** <#xdsascii>`__, `DENZO <#denzo>`__ &
    `SAINT <#saint>`__ only.

KEYWORDED INPUT
---------------

Available keywords are:

    `**ADDBATCH** <#addbatch>`__, `**BATCH** <#batch>`__,
    `**CELL** <#cell>`__, `**DETECTOR** <#detector>`__,
    `**WAVELENGTH** <#wavelength>`__,\ `**END** <#end>`__,
    `**FORMAT** <#format>`__, `**INPUT** <#input>`__,
    `**LABEL** <#label>`__, `**LABIN** <#labin>`__,
    `**MONITOR** <#monitor>`__, `**NAME** <#name>`__,
    `**PHIRANGE** <#phirange>`__, `**REINDEX** <#reindex>`__,
    `**REJECT** <#reject>`__, `**RESOLUTION** <#resolution>`__,
    `**SCALE** <#scale>`__, `**SYMMETRY** <#symmetry>`__,
    `**TITLE** <#title>`__

Compulsory input keywords for some options:
-------------------------------------------

INPUT <type>
~~~~~~~~~~~~

The allowed <type>s are those listed `above <#files_input>`__.

BATCH <batch number>
~~~~~~~~~~~~~~~~~~~~

This specifies the \`batch' number for data in the multirecord MTZ
output. (BATCH is not required for DENZO, SCAL\_NM, SCAL\_NM2, SAINT or
USER input if a batch column is present in the input file. It is not
required for WEISS or TEXSAN inputs but if given it will override the
IDENT value.)

CELL <a> <b> <c> [ <alpha> <beta> <gamma> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cell dimensions in Angstroems and angles in degrees; the angles default
to 90 degrees. This keyword is not compulsory for `MTZF <#mtzf>`__,
`MTZI <#mtzi>`__, `SCAL\_NM <#scal_nm>`__ or `DENZO <#denzo>`__ input,
but if given it will override the cell dimensions from the file headers.
This keyword is compulsory for other input types.

LABIN <program label> = <file label> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Only compulsory for `MTZF <#mtzf>`__ or `MTZI <#mtzi>`__ input.
| Column assignments for conventional labels F and SIGF are required for
  MTZF input, or for I and SIGI for MTZI input.

LABEL <file label 1> <file label 2> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Column assignments for user-defined input format (INPUT USER). <file
label n> must be chosen from H, K, L, M, BATCH, I, SIGI (or F, SIGF, or
F(+) SIGF(+) F(-) SIGF(-)), IPR, SIGIPR, FRACTION and given in the order
in which they occur in the input file. The indices H, K, L are
compulsory. M is optional and defaults to 0, *i.e.* fully recorded
reflexions. The batch number should either be read in from a BATCH
column or taken from the BATCH keyword (the former takes precedence if
both are present). Either I, SIGI or F, SIGF must be given; if the
latter, then F is squared and I/SIGI output. The remaining columns IPR,
SIGIPR, FRACTION are optional.

SYMMETRY <SG number> \| <SG symbol>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Specifies the spacegroup for the data. Compulsory if not
`MTZF <#mtzf>`__ or `MTZI <#mtzi>`__ input.

Optional input keywords:
------------------------

For input types which contain batch number information the following two
keywords determine the transformation of the input numbers. Note that
the "MISBATCH" command is no longer needed to list missing batches and
has been removed.

ADDBATCH <offset>
~~~~~~~~~~~~~~~~~

<offset> is added to all input batch numbers (default 0).

PHIRANGE <divisions>
~~~~~~~~~~~~~~~~~~~~

| (DENZO only, default 1.) Each batch is split into a number <divisions>
  of divisions for scaling. The output batch number will be:
| <divisions>\*(batch - 1) + phi\_step\_counter + <offset>

FORMAT <format string>
~~~~~~~~~~~~~~~~~~~~~~

Supply a format specifier for user-defined input (see `INPUT
USER <#user>`__). The argument must be a valid FORTRAN fixed format
string, such as might be given in a FORMAT statement, including the
brackets and quoted. *E.g.*:

::

       FORMAT '(6(6X,F8.3))'

will read records comprising six numbers each preceded by a
six-character-wide field which will be skipped. It is not possible to
read more than one reflexion from each input line.

The format should be reading *real* numbers (F), and not integers (I),
so if your input file contains integers, then they should be read as
reals with *e.g.* F6.0 rather than I6. If the supplied FORMAT statement
contains I descriptors then COMBAT will automatically convert them to
the correct F format for you.

Make use of the X descriptor to skip unwanted columns. Under Unix, the
cut (1) command may be useful for reformatting the input columns if, for
instance, the relevant fields aren't in fixed positions. If the FORMAT
keyword is not given for user-defined input, then the default format
specifier '\*' is used.

NAME PROJECT <pname> CRYSTAL <xname> DATASET <dname>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[Note that the keywords PNAME <pname>, XNAME <xname> and DNAME <dname>
are also available, but the NAME keyword is preferred.]

Specify the project, crystal and dataset names for the output MTZ file.
For input options `MTZF <#mtzf>`__ and `MTZI <#mtzi>`__, this
information will be inherited from the input file, but can be overridden
with this keyword. For all other input types, it is strongly recommended
that this information is given. Otherwise, the default project, crystal
and dataset names are "unknown", "unknown" and "unknownddmmyy"
respectively.

The project-name specifies a particular structure solution project, the
crystal name specifies a physical crystal contributing to that project,
and the dataset-name specifies a particular dataset obtained from that
crystal. All three should be given.

WAVELENGTH <wavelength>
~~~~~~~~~~~~~~~~~~~~~~~

Assign wavelength to dataset.

MONITOR <nmon>
~~~~~~~~~~~~~~

Every <nmon>-th reflection is monitored (printed out). Default: no
monitoring.

REJECT <hrej> <krej> <lrej>
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The arguments specify indices of reflections to be excluded from the
output file (these indices should lie in the spacegroup's chosen
asymmetric unit). All reflections equivalent to this set of indices will
be rejected. This is a desperate measure to weed out bad reflections
(usually overloads) without repeating the data processing. This line can
be repeated up to 200 times.

REINDEX <transformation>
~~~~~~~~~~~~~~~~~~~~~~~~

<transformation> specifies how to transform the original indices for the
output. It is specified in the form k,h,-l; h,-k,-h/2-1/2; etc. The
default is no transformation.

RESOLUTION <resmin> [ <resmax> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rejects reflections outside the given resolution range in Ås or
4s\*\*2/lambda\*\*2.

SCALE <scale>
~~~~~~~~~~~~~

The input intensities are multiplied by <scale> (default 1.0).

TITLE <title>
~~~~~~~~~~~~~

<title> is written to the output file.

DETECTOR <xmin> <xmax> <ymin> <ymax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

limits of detector coordinates XDET, YDET. These are needed for the
detector scaling options in `Scala <scala.html>`__.

END
~~~

to finish.

SEE ALSO
--------

`scala <scala.html>`__, `f2mtz <f2mtz.html>`__, `Data
Harvesting <harvesting.html>`__, `CAD <cad.html>`__

`XDS web site <http://www.mpimf-heidelberg.mpg.de/~kabsch/xds/>`__

EXAMPLES
--------

::

    combat HKLIN raxis.dump HKLOUT junk.mtz << +
    CELL 63 63 264 90 90 90
    MONITOR 1000 
    BATCH 1                # first batch number
    INPUT RAXISDUMP
    RESOLUTION 0 0.137
    SYMMETRY 89
    TITLE   Test combat
    NAME PROJECT myproject CRYSTAL mycrystal DATASET native
    END 
    +

::

    combat                       \
    HKLIN ESRF/pcrb/pcrb5_e1.sca \
    HKLOUT $SCRATCH/pcrb5_e1.mtz    \
    << 'END-mtzrwrot' 
    MONI 10000 
    INPUT SCAL_NM2
    ADDBATCH 0
    CELL 92.004  92.004 175.229  90.000  90.000  90.000
    SYMM P41212
    TITLE Test on Scalepack - NoMerge- Original Indices L1
    NAME PROJECT myproject CRYSTAL mycrystal DATASET native
    END 
    'END-mtzrwrot'
    #

AUTHOR
------

Alun Ashton, CCP4 Daresbury Laboratory.

Original program was ROTAPREP by Eleanor Dodson, University of York
