FSEARCH (CCP4: Supported Program)
=================================

NAME
----

**fsearch** -  A program to perform up to 6 dimensional molecular
(envelope) replacement search.

SYNOPSIS
--------

| **fsearch hklin** *foo\_in.mtz* [**flmin** *foo\_in.txt*]
  [**xyzin**\ *foo\_in.pdb*] [**mapin** *foo\_in.map*]
| [`Key-worded input <#keywords>`__]

DESCRIPTION
-----------

FSEARCH can be used for a molecular-replacement (up to 6 dimensional)
solution  given a predetermined envelope from any source, provided that
the envelope can be converted to the standard CCP4 map format or
expressed in terms of spherical harmonics or generated from a pdb file.

INPUT AND OUTPUT FILES
----------------------

Input
~~~~~

 HKLIN
    (compulsory)
    Input MTZ file. This should contain the conventional (CCP4)
    asymmetric unit of data (must be \*sorted\* on H K L). See the LABIN
    keyword for columns used.
 (Envelope input mode; ONLY one from the following three)
FLMIN
    Spherical Harmonics File. Free format: L  M  Flm(re)  Flm(im).
 XYZIN
    Coordinates File (Brookhaven format).
 MAPIN
    Map File - one asymmetric unit only (standard CCP4 map file format).

Output
~~~~~~

Results (up to 200 solutions sorted by R-factor) are shown in the log
file.

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords. Only the
first 4 characters of a keyword are significant. The cards can be in any
order. Numbers in [ ] are optional and can be left out. The only
compulsory command is `LABIN <#LABIN>`__. The available keywords are:

    `**TITLE** <#TITLE>`__, `**LABIN** <#LABIN>`__,
    `**GRID** <#GRID>`__, `**RESOLUTION** <#RESOLUTION>`__,
    `**SIGCUT** <#SIGCUT>`__, `**RFILTER** <#RFILTER>`__,
    `**ORTH** <#ORTH>`__, `**CHKOVERLAP** <#CHKOVERLAP>`__,
    `**ALPHA** <#ALPHA>`__, `**BETA** <#BETA>`__,
    `**GAMMA** <#GAMMA>`__, `**XRANGE** <#XRANGE>`__,
    `**YRANGE** <#YRANGE>`__, `**ZRANGE** <#ZRANGE>`__

TITLE <title string>
~~~~~~~~~~~~~~~~~~~~

Up to 80 character title.

LABIN FP=...  SIGFP=...
~~~~~~~~~~~~~~~~~~~~~~~

| (Compulsory.)
| E.g.
| FP=F  SIGFP=SIGF.

GRID <nx> <ny> <nz>
~~~~~~~~~~~~~~~~~~~

Number of sampling divisions along whole cell edge. For all
space-groups, NX,NY,NZ must be even and must have no prime factors
greater than 19. Default values: nx = a, ny = b and nz = c (nearest
integer in Angstrom).

RESOLUTION <rmin>
~~~~~~~~~~~~~~~~~

| High resolution cutoff in Angstrom (default value = 10).
|  

SIGCUT <Nsig>
~~~~~~~~~~~~~

Reflections will be excluded if F < Nsig\*SIGF. Default: no reflections
are excluded.

RFILTER <rf>
~~~~~~~~~~~~

Solutions with R-factor < rf will be written to the log file. Default:
rf = 0.60.

ORTH <code>
~~~~~~~~~~~

| Orthonormalization code.
|       = 1 orthogonal x y z along a, c\*xa, c\* (Brookhaven, default)
|       = 2 b, a\*xb, a\*
|       = 3 c, b\*xc, b\*
|       = 4 a+b, c\*x(a+b), c\*
|       = 5 a\*, cxa\*, c   (Rollett)

CHKOVERLAP
~~~~~~~~~~

Check packing clashes (% of the whole unit cell). Note: this can be
\*very time consuming\*, therefore not recommended for the initial
search. Default: no checking.

ALPHA <min> <max> <step>
~~~~~~~~~~~~~~~~~~~~~~~~

Search range and step on Eulerian angle alpha (in degrees). Default: 0 
180  3.

BETA <min> <max> <step>
~~~~~~~~~~~~~~~~~~~~~~~

Search range and step on Eulerian angle beta (in degrees). Default: 0 
180  3.

GAMMA <min> <max> <step>
~~~~~~~~~~~~~~~~~~~~~~~~

Search range and step on Eulerian angle gamma (in degrees). Default: 0 
180  3.

XRANGE <min> <max> <step>
~~~~~~~~~~~~~~~~~~~~~~~~~

Search range and step on X (in Angstroms). Default: 0  a/2  1.

YRANGE <min> <max> <step>
~~~~~~~~~~~~~~~~~~~~~~~~~

Search range and step on Y (in Angstroms). Default: 0  b/2  1.

ZRANGE <min> <max> <step>
~~~~~~~~~~~~~~~~~~~~~~~~~

Search range and step on Z (in Angstroms). Default: 0  c/2  1.

PRINTER OUTPUT
--------------

| Results (up to 200 solutions sorted by R-factor) are shown in the log
  file.
|  

EXAMPLES
--------

Unix examples script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`fsearch.exam <../examples/unix/runnable/fsearch.exam>`__

Non-runnable example script
~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    fsearch hklin pfsodb.mtz xyzin sod3.pdb <<eof
      TITL SOD
     # GRID 48 52 150
      LABI FP=FP SIGFP=SIGFP
      ALPHA 0 0 1
      BETA 0 0 1
      GAMMA 0 30 3
      XRANGE 0 18 3
      YRANGE 0 25 1
      ZRANGE 0 58 1
      RESO 6
    eof

PARALLELISED VERSION
--------------------

Code for an MPI-parallelised version of FSEARCH is distributed as
fsearch\_mpi.f This is not integrated into the CCP4 installation, and
needs to be built separately. You will need:

-  An MPI compiler, such as
   `MPICH <http://www-unix.mcs.anl.gov/mpi/mpich/>`__ or
   `LAM <http://www.lam-mpi.org>`__.
-  The link line will need to include to the MPI library as well as the
   usual CCP4 libraries (possible names libmpich.a, libmpif.a,
   libfmpi.a)
-  The source code fsearch\_mpi.f includes the system file "mpif.h".
   This may need to be adjusted.

The program distributes the search over Eulerian angles alpha and beta
over the available processors (with one processor reserved as a master
node). By default, 3 processors are used for the beta search for each
trial alpha angle. The search over alpha is then split over how ever
many sets of 3 processors there are. If the total number of processors
(not including the master node) is not a multiple of 3, then the last 1
or 2 processors are not used. The number of processors for the beta
search can be changed from the default value of 3 by the keyword PBET
(e.g. "PBET 5").

REFERENCES
----------

#. Hao, Q. (2001), *Acta Cryst.* **D57** 1410-1414. "Phasing from an
   Envelope".
#. Q. Liu, A. J. Weaver, T. Xiang, D. J. Thiel and Q. Hao, (2003) *Acta
   Cryst.* **D59** 1016-1019. "Low-resolution molecular replacement
   using a six-dimensional search"

AUTHOR
------

Quan Hao, Cornell University.

Email: qh22@cornell.edu.
