|CCP4 web logo|

Basic Maths for Protein Crystallographers

Phasing

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

An X-ray experiment allows us to measure all I(\ **h**) to some
resolution limit. If we knew both \|F(\ **h**)\| and |phi|\ (**h**) then
we could generate a map of the unit cell having peaks at and only at
each atom position using the Fourier summation

    |expression for electron density|

This (the presence of peaks) is the fundamental property of crystal
diffraction which underpins all structure solution methods.

But the phases cannot be measured directly and have to be inferred from
differences between sets of intensity measurements. The experimental
techniques to find them are loosely labelled as MIR, MIRAS, SIR, SIRAS,
MAD, SAD:

SIR
    single isomorphous replacement. Measurements are taken from a
    "native" protein and one "derivative" where some additional atoms
    have been incorporated into the lattice.
MIR
    multiple isomorphous replacement. Measurements are taken from a
    "native" protein and several derivatives.
SIRAS
    single isomorphous replacement plus anomalous differences. As above
    but the anomalous measurements for F(\ **h**) and F(-\ **h**) for
    the derivative are used.
MIRAS
    multiple isomorphous replacement plus anomalous differences.
SAD
    single anomalous dispersion. The "native" crystal contains some
    atoms which scatter anomalously, and these differences are used in a
    similar way to the SIR treatment.
MAD
    multiple anomalous dispersion.

We need to consider the structure factor equation in more detail before
discussing these.

    |structure factor equation 3|

In fact the scattering factor f(i,\ **S**) is:

    f(i,\ **S**) + f'(i) + i f"(i)

where f' and f" describe the scattering from inner electron shells,
which varies as a function of the wavelength, but is more or less
constant at all resolutions (*i.e.* f"(i,\ **S**) = f"(i)). For many
elements ( C, N, O in particular) f' and f" are very small at all
accessible wavelengths. Others, such as S and Cl have a small but
detectable component at CuK\ |alpha| (f" ~ 0.5). In general transition
elements such as Se, Br have observable f" (f" ~ 3-4) at short
wavelengths. Metals and other heavy elements such as Hg, Pt, I *etc.*
have quite large f" and f' contributions at most accessible wavelengths
(at CuK\ |alpha| f"\ :sub:`Hg` ~ 8).

It helps to re-write the F\ :sub:`H`\ (**h**) or F\ :sub:`A`\ (**h**)
component like this:

+---------------------------------+-----------------------------------------------------------------------+
| |structure factor equation 4|   | |pictorial representation of structure factor for h with anomalous|   |
+---------------------------------+-----------------------------------------------------------------------+

The anomalous contribution is always 90 degrees in ADVANCE of the real
contribution. The ratio of all \|F"\ :sub:`H`\ \|/ \|F\ :sub:`H`\ \| =
f"(j,h)/{f(j,h) -f'(j,h)}.

Now

+---------------------------------+------------------------------------------------------------------------+
| |structure factor equation 5|   | |pictorial representation of structure factor for -h with anomalous|   |
+---------------------------------+------------------------------------------------------------------------+

which means that, although the magnitudes of F\ :sub:`H`\ (**h**) and
F\ :sub:`H`\ (-**h**) are equal, their phases are different, and
F\ :sub:`H`\ (-**h**) is no longer the complex conjugate of
F\ :sub:`H`\ (**h**).

And since F\ :sub:`PH`\ (**h**) = F\ :sub:`H`\ (**h**) +
F\ :sub:`P`\ (**h**), and F\ :sub:`PH`\ (-**h**) = F\ :sub:`H`\ (-**h**)
+ F\ :sub:`P`\ (-**h**) it follows that neither the magnitudes of
\|F\ :sub:`PH`\ (**h**)\| and \|F\ :sub:`PH`\ (-**h**)\| are equal, nor
the phase |phi|\ :sub:`PH`\ (**h**) equal to
-|phi|\ :sub:`PH`\ (-**h**).

How does this help phasing?
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Answer: In no way unless we can position the heavy (or anomalous) atoms.

If they are known, the vector F\ :sub:`H`\ (**h**) can be calculated and
from the knowledge of the three magnitudes \|F\ :sub:`H`\ (**h**)\|,
\|F\ :sub:`P`\ (**h**)\| and \|F\ :sub:`PH`\ (**h**)\| plus the phase of
F\ :sub:`H`\ (**h**), it is easy to show from a phase triangle that
|phi|\ :sub:`P` will have to equal
|phi|\ :sub:`H`\ ±\ |phi|\ :sub:`diff`.

+--------------------------------------------------------------------------------------------------------------+---------------------+
| This is often represented with "phase circles" (or `phasing diagrams <bmg9.html>`__) or "phase triangles":   | |phase triangles|   |
+--------------------------------------------------------------------------------------------------------------+---------------------+

Deviation into Patterson theory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Since there are usually only a few heavy atoms associated with many
protein atoms, they can usually be positioned using Pattersons or direct
methods. Both these techniques require only an estimate of the
**magnitude** of the F\ :sub:`H`\ (**h**).

It maybe is worth summarising here the theory behind difference
Pattersons.

| F\ :sub:`PH`\ (**h**) = F\ :sub:`H`\ (**h**) + F\ :sub:`P`\ (**h**).
| The cos rule gives: \|F\ :sub:`PH`\ (**h**)\|² =
  \|F\ :sub:`H`\ (**h**)\|² + \|F\ :sub:`P`\ (**h**)\|² + 2
  \|F\ :sub:`H`\ (**h**)\| \|F\ :sub:`P`\ (**h**)\|
  cos\ |phi|\ :sub:`diff`
| where |phi|\ :sub:`diff` is the phase between vector
  F\ :sub:`H`\ (**h**) and vector F\ :sub:`P`\ (**h**). From this we can
  approximate:

    \|F\ :sub:`PH`\ (**h**)\| = {\|F:sub:`H`\ (**h**\ \|² +
    \|F\ :sub:`P`\ (**h**)\|² + 2 \|F\ :sub:`H`\ (**h**)\|
    \|F\ :sub:`P`\ (**h**)\| cos\ |phi|\ :sub:`diff`}\ :sup:`½` =
    \|F\ :sub:`P`\ (**h**)\| {1 + 2
    \|F\ :sub:`H`\ (**h**)\|/ \|F\ :sub:`P`\ (**h**)\|
    cos\ |phi|\ :sub:`diff` +
    (\|F:sub:`H`\ (**h**)\|/ \|F\ :sub:`P`\ (**h**)\|)²}\ :sup:`½`

The binomial theorem gives (1+x):sup:`½` ~ 1 + x/2 when x is small, so

    \|F\ :sub:`PH`\ (**h**)\| ~ \|F\ :sub:`P`\ (**h**)\| {1 +
    \|F\ :sub:`H`\ (**h**)\|/ \|F\ :sub:`P`\ (**h**)\|
    cos\ |phi|\ :sub:`diff` +
    ½(\|F\ :sub:`H`\ (**h**)\|/ \|F\ :sub:`P`\ (**h**)\|)²}
    = \|F\ :sub:`P`\ (**h**)\| + \|F\ :sub:`H`\ (**h**)\|
    cos\ |phi|\ :sub:`diff` +
    ½\|F\ :sub:`H`\ (**h**)\|²/ \|F\ :sub:`P`\ (**h**)\|

| So \|F\ :sub:`PH`\ (**h**)\| - \|F\ :sub:`P`\ (**h**)\| ~
  \|F\ :sub:`H`\ (**h**)\| cos\ |phi|\ :sub:`diff` + an even smaller
  term, providing \|F\ :sub:`H`\ (**h**)\| is small compared to
  \|F\ :sub:`P`\ (**h**)\|.
| and a Patterson with coefficients (\|F:sub:`PH`\ (**h**)\| -
  \|F\ :sub:`P`\ (**h**)\|)² is approximately equivalent to one with
  coefficients (\|F:sub:`H`\ (**h**)\| cos\ |phi|\ :sub:`diff`)² =
  ½\|F\ :sub:`H`\ (**h**)\|² (1 + cos 2\ |phi|\ :sub:`diff`) (remember:
  cos²(x) = (1+cos(2x))/2)
| The summation of ½\|F\ :sub:`H`\ (**h**)\|² will give the normal
  Patterson distribution of vectors between related atoms while the
  summation of ½\|F\ :sub:`H`\ (**h**)\|² cos 2\ |phi|\ :sub:`diff` will
  generate only noise.

Deviation into Difference Fourier Theory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Similar equations explain why a Fourier summation gives full weight
peaks at the atomic positions which have been included in the phasing,
and peaks at about half the expected height for atoms excluded from the
phasing. Say F\ :sub:`PH`\ (**h**) = F\ :sub:`P`\ (**h**) +
F\ :sub:`H`\ (**h**) where F\ :sub:`H` is much smaller than F\ :sub:`P`;
*i.e.* only a few atoms are excluded from the phasing. Then as above

    \|F\ :sub:`PH`\ \| ~ \|F\ :sub:`P`\ \| + \|F\ :sub:`H`\ \|
    cos(\ |phi|\ :sub:`P`-|phi|\ :sub:`H`) + small terms

The Fourier summation

    |large capital Sigma|\ \|F\ :sub:`PH`\ \|
    e\ :sup:`i\ |phi|\ :sub:`P`` = |large capital
    Sigma|\ \|F\ :sub:`P`\ \| e\ :sup:`i\ |phi|\ :sub:`P`` + |large
    capital Sigma|\ \|F\ :sub:`H`\ \|
    cos(\ |phi|\ :sub:`P`-|phi|\ :sub:`H`) e\ :sup:`i\ |phi|\ :sub:`P``

Since cos(x) = (e:sup:`ix` + e :sup:`-ix`)/2

    cos(\ |phi|\ :sub:`P`-|phi|\ :sub:`H`) e\ :sup:`i\ |phi|\ :sub:`P``
    = ½ (e:sup:`i\ |phi|\ :sub:`H`` +
    e\ :sup:`i(2\ |phi|\ :sub:`P`-|phi|\ :sub:`H`)`)

and the second term becomes

    |large capital Sigma|\ F\ :sub:`H` ½(e\ :sup:`i\ |phi|\ :sub:`H`` +
    e\ :sup:`i(2\ |phi|\ :sub:`P`-|phi|\ :sub:`H`)`)

giving the Fourier map for the atoms contributing to F\ :sub:`H` at half
weight, plus noise, since the phase 2\ |phi|\ :sub:`P`-|phi|\ :sub:`H`
is not related to these atoms at all.

| 

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg9.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg7.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
.. |expression for electron density| image:: ../images/densitytr.gif
   :width: 288px
   :height: 53px
.. |structure factor equation 3| image:: ../images/strfac3tr.gif
   :width: 330px
   :height: 129px
.. |alpha| image:: ../images/alphatr.gif
   :width: 12px
   :height: 11px
.. |structure factor equation 4| image:: ../images/strfac4tr.gif
   :width: 282px
   :height: 145px
.. |pictorial representation of structure factor for h with anomalous| image:: ../images/Fhtr.gif
   :width: 253px
   :height: 161px
.. |structure factor equation 5| image:: ../images/strfac5tr.gif
   :width: 294px
   :height: 139px
.. |pictorial representation of structure factor for -h with anomalous| image:: ../images/F-htr.gif
   :width: 253px
   :height: 130px
.. |phase triangles| image:: ../images/phasetrianglestr.gif
   :width: 237px
   :height: 223px
.. |large capital Sigma| image:: ../images/Sigma2tr.gif
   :width: 14px
   :height: 19px
.. |large capital Sigma| image:: ../images/Sigma2tr.gif
   :width: 14px
   :height: 19px
