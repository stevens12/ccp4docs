|CCP4 web logo|

Basic Maths for Protein Crystallographers

Lattice geometry

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

Diffraction from a crystal lattice is visible at discrete points
conveniently indexed as an integer triple (h,k,l), or in vector notation
as h\ **a\ :sup:`\*`**\ +k\ **b\ :sup:`\*`**\ +l\ **c\ :sup:`\*`**,
relative to the reciprocal lattice vectors
**a\ :sup:`\*`**,\ **b\ :sup:`\*`**,\ **c\ :sup:`\*`**. These are
defined to have the properties

    +-----------------------------------------------------------------------------------------+
    | **a\ :sup:`\*`\ ·** **a** = **b\ :sup:`\*`\ ·** **b** = **c\ :sup:`\*`\ ·** **c** = 1   |
    +-----------------------------------------------------------------------------------------+
    | **a\ :sup:`\*`\ ·** **b** = **a\ :sup:`\*`\ ·** **c** = **b\ :sup:`\*`\ ·** **c** = 0   |
    +-----------------------------------------------------------------------------------------+

*i.e.* **a\ :sup:`\*`** is perpendicular to **b** and **c**, and
therefore parallel to **b×c**. *E.g.* in P3\ :sub:`1`:

|P31 geometry|

where **c** and **c\ :sup:`\*`** lie vertically in the plane of the
drawing, and **a\ :sup:`\*`** comes perpendicularly out of the plane of
the drawing.

| 

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg4.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg2.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |P31 geometry| image:: ../images/p3geometrytr.gif
   :width: 398px
   :height: 223px
