|CCP4 web logo|

Basic Maths for Protein Crystallographers

Atom notation

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

An atom in a crystal can be referenced as follows:

-  as (x:sub:`i` )
-  as a "coordinate triple": x\ :sub:`i`,y\ :sub:`i`,z\ :sub:`i`
-  in vector notation as
   x\ :sub:`i`\ **a**\ +y\ :sub:`i`\ **b**\ +z\ :sub:`i`\ **c**

Its position in another unit cell is (x:sub:`i`\ +n\ :sub:`x`)\ **a** +
(y:sub:`i`\ +n\ :sub:`y`)\ **b** + (z:sub:`i`\ +n\ :sub:`z`)\ **c**. The
atom's position is given **relative to some origin**, which is
conveniently chosen relative to the symmetry axes.

| 

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg3.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg1.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
