|CCP4 web logo|

Basic Maths for Protein Crystallographers

Form factors

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

A single atom in a lattice will diffract with an amplitude proportional
to its form factor f(i,\ **S**).

+----------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |scattering factor; borrowed from Crystallography 101|   | Scattering curve for O (oxygen): electrons v d\ :sup:`\*`. For a description/further explanation of this graph, see `Crystallography 101 - Calculation of Atomic Scattering Factors <http://www.ruppweb.dyndns.org/Xray/comp/scatfac.htm>`__   |
+----------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

The atom's diffracting power is further reduced at higher resolution by
any atomic vibration (*i.e.* temperature factor), and the combined
scattering is often labelled g(i,\ **S**). If the oscillation is
isotropic

    g(i,\ **S**) = f(i,\ **S**) e\ :sup:`-B:sub:`i`\ **S**\ ²`

The diffraction "wave" from each atom for any given reflection will have
a phase dependent on its position within the lattice. By convention the
origin is chosen relative to the symmetry operators.

An atom's contribution can be expressed in various ways, often used
concurrently in text books:

    |phase expressions|

| 

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg7.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg5.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |scattering factor; borrowed from Crystallography 101| image:: ../images/scatfactr.gif
   :width: 175px
   :height: 138px
.. |phase expressions| image:: ../images/phaseexpressionstr.gif
   :width: 237px
   :height: 184px
