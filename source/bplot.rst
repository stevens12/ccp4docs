BPLOT (CCP4: Deprecated Program)
================================

NAME
----

**bplot** - Plot temperature factor by residue

SYNOPSIS
--------

| **bplot XYZIN1** *foo\_in1.pdb* **XYZIN2** *foo\_in2.pdb* **plot**
  *foo.plt*
| [`Keyworded input <#keywords>`__]

 DESCRIPTION
------------

This program is used to plot the temperature factors for residues in the
PDB input file using plot84 subroutine library. Average B-factors for
main chain atoms are plotted against residue number as a line graph.
Average B-factors for side chain atoms are plotted against residue
number as a histogram, unless the `CROSSES <#crosses>`__ keyword is
specified.

The current version calculates the maximum B-value for the chain and
adjusts the y-axis scale accordingly.

The output meta-file PLOT can then be viewed with
`XPLOT84DRIVER <xplot84driver.html>`__, or converted to PostScript,
Tektronix or HPGL with `PLTDEV <pltdev.html>`__.

KEYWORDED INPUT
---------------

The available keywords are:

    `BMAX <#bmax>`__, `CBMAIN <#cbmain>`__, `CBSIDE <#cbside>`__,
    `COMPARE <#compare>`__, `CROSSES <#crosses>`__, `MAIN <#main>`__,
    `PLTY <#plty>`__, `RES1 <#res1>`__, `RES2 <#res2>`__,
    `SIDE <#side>`__, `TITLE <#title>`__, `XSCALE <#xscale>`__,
    `YSCALE <#yscale>`__

TITLE <title>
~~~~~~~~~~~~~

Title to appear on the plot

PLTY
~~~~

Plot with the x-axis along the length of the paper (landscape).

By default the picture is drawn with the x-axis along the width of the
paper (portrait).

CROSSES
~~~~~~~

Points for side chains marked with crosses.

By default the points are plotted as a histogram.

COMPARE
~~~~~~~

Tells the program there are two files to read.

For this option can only plot either main chains or side chains

MAIN
~~~~

Plot the B values for the main chain only

(this card or SIDE is compulsory if two files input)

SIDE
~~~~

Plot the B values for the side chains only

(this card or MAIN is compulsory if two files input)

XSCALE <xscale>
~~~~~~~~~~~~~~~

Set the scale along the x-axis explicitly

Default = 0.85

YSCALE <yscale>
~~~~~~~~~~~~~~~

Set the scale along the y-axis explicitly

Default = 0.85

BMAX <bmax>
~~~~~~~~~~~

The maximum y-axis value on the plot.

If the value you supplied is smaller then the calculated maximum B value
for this chain, the supplied <bmax> will not be used.

By default the program scales the plot according to the maximum B value
found in the input pdb file.

RES1 <res\_beg> <res\_end>
~~~~~~~~~~~~~~~~~~~~~~~~~~

followed by the chain labels and numbers of the first and last residues
to be plotted from xyzin1

eg. RES1 A1 A224

RES2 <res\_beg> <res\_end>
~~~~~~~~~~~~~~~~~~~~~~~~~~

followed by the chain labels and numbers of the first and last residues
to be plotted from xyzin2

eg. RES2 A1 A224

CBMAIN
~~~~~~

Treat CB's as main chain atoms.

CBSIDE
~~~~~~

Treat CB's as side chain atoms (default).

INPUT AND OUTPUT FILES
----------------------

XYZIN1 Input coordinates

XYZIN2 Input second coordinate file (optional)

PLOT Plot meta-file for `PLTDEV <pltdev.html>`__

AUTHORS
-------

Originator : Miri Hirshberg

EXAMPLES
--------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `bplot.exam <../examples/unix/runnable/bplot.exam>`__

SEE ALSO
--------

`baverage <baverage.html>`__
