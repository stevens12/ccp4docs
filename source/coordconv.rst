COORDCONV (CCP4: Supported Program)
===================================

NAME
----

**coordconv** - Interconvert various coordinate formats

SYNOPSIS
--------

| **coordconv xyzin** *foo\_in.xyz* **xyzout** *foo\_out.brk*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

This program converts various coordinate formats. It reads cell
dimensions and orthogonalises the coordinates.

KEYWORDED INPUT
---------------

The data control lines are identified by the following keywords. Only
the first 4 letters of each keyword are necessary.

    `**CELL** <#cell>`__, `**END** <#end>`__, `**INPUT** <#input>`__,
    `**OUTPUT** <#output>`__

INPUT [ FRAC \| HA \| PDB \| SHELX-F \| SHELX-S \| CSD \| XPLOR/CNS \| SNB ] [ ORTH <ncode> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Defines the input coordinate format. The following options are
supported:

FRAC
    FRACTIONAL - York traditional fractional, a precursor to PDB format,
    *e.g.*
HA
    HA - the format used for CCP4 Interface input and output from
    MLPHARE. There is no fixed format except that any CELL information
    must be on a line beginning CELL, and coordinates on lines beginning
    ATOM. If the CELL line is given, this is used for coordinate
    conversion. All other lines are ignored. *e.g.*
PDB
    BROOKHAVEN with or without CRYST1 and SCALEx cards
SHELX-F
    SHELX \`full' (2 lines per atom)
SHELX-S
    SHELX \`short' (1 line per atom) e.g. a shelx.res or shelx.ins file.
    The format used is ``FORMAT(A4,I3,3F8.4,F10.6,F6.2)`` which
    corresponds to SHELXS-90. This definitely won't work with SHELXD
    output, but you should be able to get PDB files directly in that
    case.
CSD
    Cambridge structural data base
XPLOR
    Brookhaven - X-PLOR version. You will have to split chains later and
    correct some atom names. See also \`OUTPUT XPLOR' option in PDBSET.
SNB
    ShakeNBake peaks file in Fortran format:

    ::

        (3F10.0,F10.2,I5,I2)

    The peak height is assigned to the B-factor column, and the flag
    indicating whether the peak should be used is converted to an
    occupancy. The choice of input format corresponds to the format
    written by BnP, \`The Buffalo and Pittsburgh Interface combining SnB
    and Phases' - see http://www.hwi.buffalo.edu/BnP/ for more
    information.

The ORTH keyword must be followed by <ncode> - the orthogonalisation
code. The default is orthogonalisation code 1. If the INPUT is a PDB
file with CRYST1 and SCALEx cards, then the SCALEx matrices override the
orthogonalisation code.

::

          ncode = 1 -  orthogonal axes are defined to have
                       A parallel to XO   CSTAR parallel to ZO
          ncode = 2 -  orthogonal axes are defined to have
                          B parallel to XO   ASTAR parallel to ZO
          ncode = 3 -  orthogonal axes are defined to have
                          C parallel to XO   BSTAR parallel to ZO
          ncode = 4 -  orthogonal axes are defined to have
                        HEX A+B parallel to XO   CSTAR parallel to ZO
          ncode = 5 -  orthogonal axes are defined to have
                        ASTAR parallel to XO   C     parallel to ZO
          ncode = 6 -  orthogonal axes are defined to have
                          A parallel to XO   BSTAR parallel to YO

OUTPUT [ FRAC \| HA \| PDB \| XPLOR ] [ ORTH <ncode> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Defines output coordinate format as `above <#input>`__.

CELL <a> <b> <c> [ <alpha> <beta> <gamma> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cell dimensions in Angstroms and degrees. Angles default to 90.
Compulsory unless the input file is a PDB file with CRYST1 card or an HA
file with CELL specified.

END
~~~

Ends input.

INPUT AND OUTPUT FILES
----------------------

XYZIN Input coordinates

XYZOUT Output coordinates

AUTHORS
-------

Originator : Eleanor Dodson, University of York

EXAMPLES
--------

Sample Unix input file:
~~~~~~~~~~~~~~~~~~~~~~~

`coordconv.exam <../examples/unix/runnable/coordconv.exam>`__ (This is a
simple example which just converts the toxd.pdb file to different
formats)

SEE ALSO
--------

`SHELXPRO <http://shelx.uni-ac.gwdg.de/SHELX>`__ - can be used to
produce a ``.ins`` file for input to SHELX from a PDB format file.

`pdbset <pdbset.html>`__ - various manipulations of a PDB coordinate
file.
