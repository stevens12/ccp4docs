SUPERPOSE (CCP4: Supported Program)
===================================

NAME
----

**superpose** - structural alignment based on secondary structure
matching

SYNOPSIS
--------

**superpose** *foo\_1st.pdb* [-s CID1] *foo\_2nd.pdb* [-s CID2] ...
*foo\_Nth.pdb* [-s CIDN] [-o *foo\_out.pdb*]

where [-s CID1/2/.../N] are optional selection strings in `MMDB
convention <pdbcur.html#atom_selection>`__, and [foo\_out.pdb] is an
optional output file.

DESCRIPTION
-----------

``superpose`` aligns and superposes two or more protein structures by
matching graphs built on the protein's secondary-structure elements,
followed by an iterative three-dimensional alignment of protein backbone
C-alpha atoms.

For this method to work, aligned structures should always include at
least three secondary structural elements.

INPUT AND OUTPUT FILES
----------------------

foo\_1st.pdb / foo\_2nd.pdb / ... / foo\_Nth.pdb
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input coordinate files; at least two files must be given. Although
typically a PDB file, an input file can also be in mmCIF or MMDB binary
formats. The input format is detected automatically and independently
for each input file. In case of pairwise aligbment, foo\_1st.pdb is
considered to be the Query structure to which the transformation matrix
will be applied. In case of multiple (N>2) structure alignment,
transformation matrices will be calculated and output separately for
each structure.

foo\_out.pdb
~~~~~~~~~~~~

If specified, this file will contain a sequence of input structures with
the calculated transformation matrices applied.

Command line options
--------------------

The optional selection strings ``[-s CID1/2]`` are in the format
described in the `pdbcur <pdbcur.html#atom_selection>`__ documentation.
For example, ``-s A/23-55`` means use only residues 23 to 55 of chain A
in calculating the alignment.

PROGRAM OUTPUT
--------------

The program reports the Transformation Matrix calculated for
superimposing the input structures and the RMSD from the superposition.

The program then gives a residue-by-residue listing of the alignment(a).
Strands and helices in the aligned structures are identified in the
listing. The quality of the match for each residue is also given.

EXAMPLES
--------

Runnable example
~~~~~~~~~~~~~~~~

`superpose.exam <../examples/unix/runnable/superpose.exam>`__

SEE ALSO
--------

| `lsqkab <lsqkab.html>`__
| `Gesamt <gesamt.html>`__

AUTHOR
------

Eugene Krissinel, CCP4 (formerly at European Bioinformatics Institute,
Cambridge, UK).

REFERENCE
---------

-  E.Krissinel and K.Henrick (2004), Acta Cryst. D60, 2256-2268
   Secondary-structure matching (SSM), a new tool for fast protein
   structure alignment in three dimensions
