.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: SAD using CCP4i
   :name: sad-using-ccp4i
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

Data for this tutorial are found
`here <http://www.phaser.cimr.cam.ac.uk/index.php/Tutorials>`__

::

    Reflection data: iod_scala-unique.mtz
    Heavy atom sites: iod_hyss_consensus_model.pdb
    Sequence file: hewl.pir

This tutorial demonstrates SAD phasing using the Phaser pipeline.

Lysozyme will readily crystallise from a wide range of conditions and
yield several crystal forms. One of those, tetragonal lysozyme, is
particularly well suited for halide soaking, since it grows from a high
concentration of sodium chloride. A dataset has been collected from a
lysozyme crystal soaked in 0.5 M potassium iodide. A strong anomalous
signal has been detected, and locations of anomalous scatterers have
already been found, but you can choose optionally to look for them as
part of this run.

#. Start the ccp4 GUI by typing ccp4i at the command line.
#. Make a new project called "phaser\_tute" using the
   Directories&ProjectDir button on the RHS of the GUI. Set the
   "Project" to "phaser\_tute" and "uses directory" to the directory
   where the files for this tutorial are located, and make this the
   "Project for this session of the CCP4Interface". You will then be
   able to go directly to this directory in the GUI using the pull-down
   menu that appears before every file selection.
#. Go to the Experimental Phasing module, in the yellow pull-down on the
   LHS of the GUI
#. Open the Automated Search & Phasing tab, then bring up the GUI for
   the Phaser SAD pipeline
#. You can choose (near the top) whether to use SHELXD (if you have the
   SHELX package installed) or HYSS (if you have the Phenix package
   installed) to find the substructure before running Phaser. If not,
   you can use a predetermined substructure, such as the PDB file
   included with this tutorial.
#. You can also choose whether or not to run Parrot to carry out density
   modification and Buccaneer to carry out a very preliminary model
   building, in the pipeline after Phaser.
#. All the yellow boxes need to be filled in.

   -  Make sure that "Mode for experimental phasing" is set to
      Single-wavelength anomalous dispersion (SAD). This is the default.
   -  Note that Phaser requires F(+), SIGF(+), F(-) and SIGF(-) and not
      the FP and DANO used by some other programs.
   -  If you are using SHELXD or HYSS to determine the initial
      substructure, a good number of I atoms to ask for would be 8.
   -  It is also a good idea to fill in the TITLE.

#. When you have entered all the information, run Phaser.
#. Look at the "Final phasing statistics" table at the end of the
   logfile.

   -  There is some explanation on important logfile sections in the
      Phaser Wiki

#. Look through the log file and identify the workflow. How many cycles
   did Phaser need to reach convergence? What are the convergence
   criteria? How many iodides were in the initial and final
   substructures? How many were added? Were any deleted?
#. If you ran the whole pipeline, look at whether one hand gave better
   statistics than the other in the Parrot and Buccaneer steps. Look at
   the better Buccaneer model in the corresponding Parrot map, using
   coot.
#. Only a quick build is carried out with Buccaneer. If the result is
   substantially better with one hand than the other, set up a job to
   carry on building from that Buccaneer model. You could use Buccaneer
   again, in which case you should open the Model Building module,
   choose "Buccaneer - autobuild/refine", start from experimental
   phases, provide the Buccaneer model from the Phaser pipeline, the
   corresponding Parrot MTZ file and the sequence. Alternatively, you
   could use ARP/wARP Classic, in which case you should choose the same
   model but use the MTZ file produced for the correct hand by Phaser,
   set Refmac to use HL phase restraints (in the Refmac tab) and select
   the HLA/B/C/D coefficients. Five cycles of model-building (25 cycles
   in total) should be enough.

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks">

.. raw:: html

   <div id="mw-normal-catlinks">

`Category <../../../../articles/c/a/t/Special%7ECategories_101d.html>`__:
`Tutorial <../../../../articles/t/u/t/Category%7ETutorial_c3b5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
