.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Combined MR-SAD using CCP4i
   :name: combined-mr-sad-using-ccp4i
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

All files for this tutorial are distributed from the Phaser web page

::

    Reflection data: lyso2001_scala1.mtz
    Lactalbumin model: 1fkq_prot.pdb
    Sequence file: hewl.pir

This tutorial illustrates a common molecular replacement/experimental
phasing scenario, when refinement is hindered by very strong model bias,
but there is some experimental phasing signal available.

Goat α-lactalbumin is 45% identical to hen egg-white lysozyme. Although
it is possible to solve lysozyme using α-lactalbumin as a model, it is
very difficult to refine the structure, partly because of model bias.
Unfortunately, low solvent content of this crystal form limits the
ability of density modification to remove the bias. However, one can use
anomalous scattering from intrinsic sulfur atoms to improve phases
dramatically. It is noteworthy that the anomalous signal from the sulfur
atoms is not sufficient for ab initio phasing (it is not possible to
locate the anomalous scatterers from the data alone).

#. Solve the structure with the α-lactalbumin model. Follow the
   "Molecular replacement tutorial" if necessary.
#. For a fairer comparison of phase quality, we will treat the molecular
   replacement solution as a source of experimental phase information.
   (If you use the "automated model building starting from PDB file"
   mode, the current version of ARP/wARP will be able to build the
   structure, but older versions coupled with older versions of Refmac5
   failed.) Do a quick solvent flattening with Parrot (choose "Use
   PHI/FOM instead of HL coefficients" because the MTZ file produced
   after MR doesn't include Hendrickson-Lattman coefficients; set
   PHI=PHIC and FOM=FOM; select "Use map coefficients", then set F=FWT
   and PHI=PHWT).
#. Start up ARP/wARP Classic in "automated model building starting from
   experimental phases" mode. Select the MTZ file from Parrot. To start
   from the Parrot map, select "Use a different (pre-weighted) Fobs for
   initial map calculation" under the ARP/wARP flow parameters folder,
   then set FBEST=parrot.F\_phi.F, PHIB=parrot.F\_phi.phi and
   FOM=Unassigned. Select the sequence file, and note there are 129
   residues in lysozyme. To save time, do 3 cycles of autobuilding
   instead of 10.
#. Now add the S-SAD phase information. Bring up the GUI for Phaser SAD
   pipeline in the Automated Search & Phasing section of the
   Experimental Phasing module
#. All the yellow boxes need to be filled in.

   -  Set "Mode for experimental phasing" to SAD with molecular
      replacement partial structure.
   -  Uncheck the Parrot and Buccaneer steps of the pipeline (to allow
      control for better comparison with the MR model alone).
   -  Set LLG-map calculation atom type to S.
   -  Under the "Define atoms" heading, set "Partial structure" to the
      molecular replacement solution (output PDB-file) you have obtained
      in step 1.

#. Run Phaser after you entered all the information.
#. Solvent flatten with Parrot using a similar protocol to step 2.
   However, you should \*not\* choose "Use PHI/FOM instead of HL
   coefficients". Choose the HLanomA/B/C/D coefficients because these
   describe the phase information obtained only from the anomalous
   scattering information and not from the molecular replacement model;
   using HLA/B/C/D would include the model phase information, which
   would bias maps from subsequent cycles of phase improvement to look
   like the model.
#. Run ARP/wARP using a similar protocol as in step 3, except you should
   open the Refmac parameters folder and choose the option to include HL
   phase restraints. In the MTZ data section, choose the HLanomA/B/C/D
   coefficients.
#. How many anomalous scatterers has Phaser found? Check them against
   the model and guess what they may be! Why is it not important to
   specify the exact element type in this case?
#. If you did not know the correct space group (from the MR step), would
   you have to run Phaser SAD-phasing twice?
#. Compare the two ARP/wARP runs! Which one has built more residues?

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks">

.. raw:: html

   <div id="mw-normal-catlinks">

`Category <../../../../articles/c/a/t/Special%7ECategories_101d.html>`__:
`Tutorial <../../../../articles/t/u/t/Category%7ETutorial_c3b5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
