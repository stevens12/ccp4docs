.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Top Ten Tips
   :name: top-ten-tips
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

Here are our Top Ten Tips for MR, SAD and MR-SAD phasing in Phaser

#. **Use the automated search**
   Don't have a quick read of the instructions and feel the need to
   change the defaults. If Phaser can solve your problem, it can
   probably solve it with the automated search using default inputs.
#. **Search for components concurrently**
   You should search for all components of the asymmetric unit at the
   same time, and not in independent jobs. Phaser is not the same as
   other MR programs in this respect. Prior knowledge is used to build
   up a tree search. The correct solution for the first component may
   not be obvious until the last component is placed.
#. **Search separately for domains of flexible proteins**
   If there are domain movements, the individual domains will be better
   models giving a clearer signal than the whole protein.
#. **Use sculptor**
   For low sequence identity (<30%) or other difficult cases for which a
   default Phaser run fails, use sculptor to trim off loops that don't
   align and to prune back non-identical side chains. Test models
   derived with all the protocols. The best results are obtained with
   accurate sequence alignments, which can be obtained for instance from
   HHpred, PROMALS3D or FFAS.
#. **Use ensembler**
   Molecular replacement can be difficult when the sequence identity is
   low, but then you probably have several choices of model with
   similarly low sequence identity. Construct an ensemble by using the
   multiple structure alignment in ensembler, and try the option to trim
   poorly-conserved regions to produce a conserved core.
#. **Homology models do not have 100% identity**
   If you mutate the side chains of your model so that the sequence is
   the same as the target structure, do not enter an identity of 100%
   into Phaser. The sequence identity is used to calculate the rms
   deviation in atomic positions between the search and target
   structure, and this will only be improved slightly by the most
   sophisticated modelling algorithms. The identity you enter should
   thus be the same as that of the original sequence of your model.
#. **You may need to change the packing criteria**
   Sometimes valid solutions are excluded because the number clashes is
   higher than the allowed default number. Look in the log and summary
   files to see how many high Z-score solutions Phaser is rejecting
   because of clashes, and how many clashes there are in each case. In
   recent versions of Phaser this is less of a problem than it used to
   be, but it is still worth being aware of.
#. **Search for different anomalous scatterers concurrently**
   If there are multiple types of anomalous scatterer present in your
   crystal (such as Fe and S), Phaser will get better results by
   distinguishing between them. If there is a good signal, the correct
   hand can be identified as the one with the highest LLG score.
#. **Use the MR-SAD mode**
   After molecular replacement, if there is significant anomalous signal
   in your data, use the MR-SAD mode to identify the anomalous
   scatterers and improve the phase information.
#. **Install the latest version of Phaser**
   We work hard so you don't have to!

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Top_Ten_Tips&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Top_Ten_Tips>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 15:07, 19 January 2015 by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__. Based on work
   by `Randy Read <../../../../articles/r/a/n/User:Randy.html>`__ and
   `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
