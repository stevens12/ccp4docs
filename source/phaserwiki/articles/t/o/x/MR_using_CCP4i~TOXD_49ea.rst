.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: MR using CCP4i:TOXD
   :name: mr-using-ccp4itoxd
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

Data for this tutorial are found
`here <http://www.phaser.cimr.cam.ac.uk/index.php/Tutorials>`__

::

    Reflection data: toxd.mtz
    Structure files: 1BIK.pdb, 1D0D_B.pdb
    Sequence file: toxd.seq

This tutorial demonstrates the ensembling procedure in Phaser.

α-Dendrotoxin (TOXD, 7139Da) is a small neurotoxin from green mamba
venom. You have two models for the structure. One is in the file
1BIK.pdb, which contains the protein chain from PDB entry 1BIK, and the
other is in the file 1D0D\_B.pdb, which contains chain B from PDB entry
1D0D. 1BIK is the structure of Bikunin, a serine protease inhibitor from
the human inter-α-inhibitor complex, with sequence identity 37.7% to
TOXD. 1DOD is the complex between tick anticoagulant protein (chain A)
and bovine pancreatic trypsin inhibitor (BPTI, chain B). BPTI has a
sequence identity of 36.4% to TOXD. Note that models making up an
ensemble must be superimposed on each other, which has not yet been done
with these two structures.

#. Use the SSM superpose option in coot to superimpose 1BIK on 1D0D\_B,
   saving the resulting coordinates in 1BIK\_on\_1D0D.pdb.
#. Start the ccp4 GUI by typing ccp4i at the command line.
#. Make a new project called "phaser\_tute" using the
   Directories&ProjectDir button on the RHS of the GUI. Set the
   "Project" to phaser\_tute and "uses directory" to the directory where
   the files for this tutorial are located, and make this the "Project
   for this session of the CCP4Interface". You will then be able to go
   directly to this directory in the GUI using the pull-down menu that
   appears before every file selection.
#. Go to the Molecular Replacement module, in the yellow pull-down on
   the LHS of the GUI
#. Bring up the GUI for Phaser
#. All the yellow boxes need to be filled in.

   -  It is a good idea to change the Ensemble id from the default.
   -  It is also a good idea to fill in the TITLE.

#. When you have entered all the information, run Phaser.
#. Has Phaser solved the structure? What was the LLG of the best
   solution? What was the Z‑score of the best translation function
   solution?

   -  The meaning of the Z‑score is given in the
      `documentation <../../../../articles/m/o/l/Molecular_Replacement_a23a.html#Has_Phaser_Solved_It.3F>`__

#. Look though the log file and identify the anisotropy correction,
   rotation function, translation function, packing, and refinement
   modes. Draw a flow diagram of the search strategy.
#. How many potential solutions did Phaser find or reject at each stage?
   What were the selection criteria for carrying potential solutions
   forward to the next step in the rotation and translation functions?
   How many other selection criteria could have been used, and what are
   they?

   -  Use the documentation

#. Run Phaser again without using ensembling i.e. run two jobs, one
   using 1BIK only and the other using 1D0D only as models. What are the
   LLGs of the final solutions? What are the Z‑scores of the translation
   functions? Was ensembling a good idea?

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks">

.. raw:: html

   <div id="mw-normal-catlinks">

`Category <../../../../articles/c/a/t/Special%7ECategories_101d.html>`__:
`Tutorial <../../../../articles/t/u/t/Category%7ETutorial_c3b5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
