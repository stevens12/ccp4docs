.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Keywords
   :name: keywords
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   <div id="toc" class="toc">

.. raw:: html

   <div id="toctitle">

.. rubric:: Contents
   :name: contents

.. raw:: html

   </div>

-  `1 Phaser Executable <#Phaser_Executable>`__
-  `2 Python Interface <#Python_Interface>`__
-  `3 Basic Keywords <#Basic_Keywords>`__

   -  `3.1 ATOM <#ATOM>`__
   -  `3.2 CLUSTER <#CLUSTER>`__
   -  `3.3 COMPOSITION <#COMPOSITION>`__
   -  `3.4 CRYSTAL <#CRYSTAL>`__
   -  `3.5 ENSEMBLE <#ENSEMBLE>`__
   -  `3.6 HKLIN <#HKLIN>`__
   -  `3.7 JOBS <#JOBS>`__
   -  `3.8 LABIN <#LABIN>`__
   -  `3.9 MODE <#MODE>`__
   -  `3.10 PARTIAL <#PARTIAL>`__
   -  `3.11 SEARCH <#SEARCH>`__
   -  `3.12 SGALTERNATIVE <#SGALTERNATIVE>`__
   -  `3.13 SOLUTION <#SOLUTION>`__
   -  `3.14 SPACEGROUP <#SPACEGROUP>`__
   -  `3.15 WAVELENGTH <#WAVELENGTH>`__

-  `4 Output Control Keywords <#Output_Control_Keywords>`__

   -  `4.1 DEBUG <#DEBUG>`__
   -  `4.2 EIGEN <#EIGEN>`__
   -  `4.3 HKLOUT <#HKLOUT>`__
   -  `4.4 KEYWORDS <#KEYWORDS>`__
   -  `4.5 LLGMAPS <#LLGMAPS>`__
   -  `4.6 MUTE <#MUTE>`__
   -  `4.7 KILL <#KILL>`__
   -  `4.8 OUTPUT <#OUTPUT>`__
   -  `4.9 TITLE <#TITLE>`__
   -  `4.10 TOPFILES <#TOPFILES>`__
   -  `4.11 ROOT <#ROOT>`__
   -  `4.12 VERBOSE <#VERBOSE>`__
   -  `4.13 XYZOUT <#XYZOUT>`__

-  `5 Advanced Keywords <#Advanced_Keywords>`__

   -  `5.1 ELLG <#ELLG>`__
   -  `5.2 FORMFACTORS <#FORMFACTORS>`__
   -  `5.3 HAND <#HAND>`__
   -  `5.4 LLGCOMPLETE <#LLGCOMPLETE>`__
   -  `5.5 NMA <#NMA>`__
   -  `5.6 PACK <#PACK>`__
   -  `5.7 PEAKS <#PEAKS>`__
   -  `5.8 PERMUTATIONS <#PERMUTATIONS>`__
   -  `5.9 PERTURB <#PERTURB>`__
   -  `5.10 PURGE <#PURGE>`__
   -  `5.11 RESOLUTION <#RESOLUTION>`__
   -  `5.12 ROTATE <#ROTATE>`__
   -  `5.13 SCATTERING <#SCATTERING>`__
   -  `5.14 SCEDS <#SCEDS>`__
   -  `5.15 TARGET <#TARGET>`__
   -  `5.16 TNCS <#TNCS>`__
   -  `5.17 TRANSLATE <#TRANSLATE>`__
   -  `5.18 ZSCORE <#ZSCORE>`__

-  `6 Expert Keywords <#Expert_Keywords>`__

   -  `6.1 MACANO <#MACANO>`__
   -  `6.2 MACMR <#MACMR>`__
   -  `6.3 MACOCC <#MACOCC>`__
   -  `6.4 MACSAD <#MACSAD>`__
   -  `6.5 MACTNCS <#MACTNCS>`__
   -  `6.6 OCCUPANCY <#OCCUPANCY>`__
   -  `6.7 RESCORE <#RESCORE>`__
   -  `6.8 RFACTOR <#RFACTOR>`__
   -  `6.9 SAMPLING <#SAMPLING>`__
   -  `6.10 TNCS <#TNCS_2>`__

-  `7 Developer Keywords <#Developer_Keywords>`__

   -  `7.1 BINS <#BINS>`__
   -  `7.2 BFACTOR <#BFACTOR>`__
   -  `7.3 BOXSCALE <#BOXSCALE>`__
   -  `7.4 CELL <#CELL>`__
   -  `7.5 DDM <#DDM>`__
   -  `7.6 ENM <#ENM>`__
   -  `7.7 NORMALIZATION <#NORMALIZATION>`__
   -  `7.8 OUTLIER <#OUTLIER>`__
   -  `7.9 PTGROUP <#PTGROUP>`__
   -  `7.10 RESHARPEN <#RESHARPEN>`__
   -  `7.11 SOLPARAMETERS <#SOLPARAMETERS>`__
   -  `7.12 TARGET <#TARGET_2>`__
   -  `7.13 TNCS <#TNCS_3>`__
   -  `7.14 TRANSLATE <#TRANSLATE_2>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. rubric:: Phaser Executable
   :name: phaser-executable

The Phaser executable runs in different modes, which perform Phaser's
different functionalities. The mode is selected with the
`MODE <#MODE>`__ keyword. The different modes and the keywords relevant
to each mode are described in
`Modes <../../../../articles/m/o/d/Modes.html>`__.

Most keywords only refer to a single parameter, and if used multiple
times, the parameter will take the last value input. However, some
keywords are meaningful when entered multiple times. The order may or
may not be important.

-  |User1.gif| `Basic Keywords <#Basic_Keywords>`__

-  |Output.png| `Output Control Keywords <#Output_Control_Keywords>`__

-  |User2.gif| `Advanced Keywords <#Advanced_Keywords>`__

-  |Expert.gif| `Expert Keywords <#Expert_Keywords>`__

-  |Developer.gif| `Developer Keywords <#Developer_Keywords>`__

| 

.. rubric:: Python Interface
   :name: python-interface

Phaser can be compiled as a python library. The mode is selected by
calling the appropriate run-job. Input to the run-job is via
input-objects, which are passed to the run-job. Setter function on the
input objects are equivalent to the keywords for input to the phaser
executable. The different modes and the keywords relevant to each mode
are described in `Modes <../../../../articles/m/o/d/Modes.html>`__. See
`Python Interface <../../../../articles/p/y/t/Python_Interface.html>`__
for details.

The python interface uses standard python and cctbx/scitbx variable
types.

::

    str          string
    float        double precision floating point
    Miller       cctbx::miller::index<int> 
    dvect3       scitbx::vec3<float> 
    dmat33       scitbx::mat3<float> 
    type_array   scitbx::af::shared<type> arrays

.. rubric:: Basic Keywords
   :name: basic-keywords

.. rubric:: |User1.gif|\ ATOM
   :name: user1.gifatom

 ATOM CRYSTAL <XTALID> PDB <FILENAME>
    Definition of atom positions using a pdb file.
 ATOM CRYSTAL <XTALID> HA <FILENAME>
    Definition of atom positions using a ha file (from RANTAN, MLPHARE
    etc.).
 ATOM CRYSTAL <XTALID> [ELEMENT\|CLUSTER] <TYPE> [ORTH\|FRAC] <X Y Z>
OCC <OCC>
    Minimal definition of atom position. B-factor defaults to isotropic
    and Wilson B-factor. Use <TYPE>=TX for Ta6Br12 cluster and <TYPE>=XX
    for all other clusters. Scattering for cluster is spherically
    averaged. Coordinates of cluster compounds other than Ta6Br12 must
    be entered with CLUSTER keyword. Ta6Br12 coordinates are in phaser
    code and do not need to be given with CLUSTER keyword.
 ATOM CRYSTAL <XTALID> [ELEMENT\|CLUSTER] <TYPE> [ORTH\|FRAC] <X Y Z>
OCC <OCC> [ ISOB <ISOB> \| ANOU <HH KK LL HK HL KL> \| USTAR <HH KK LL
HK HL KL>] FIXX [ON\|OFF] FIXO [ON\|OFF] FIXB [ON\|OFF] BSWAP [ON\|OFF]
LABEL <SITE\_NAME>
    Full definition of atom position including B-factor.
ATOM CHANGE BFACTOR WILSON [ON\|OFF]
    Reset all atomic B-factors to the Wilson B-factor.
 ATOM CHANGE SCATTERER <SCATTERER>
    Reset all atomic scatterers to element (or cluster) type.

::

    setATOM_PDB(str <XTALID>,str <PDB FILENAME>)
    setATOM_IOTBX(str <XTALID>,iotbx::pdb::hierarchy::root <iotbx object>)
    setATOM_STR(str <XTALID>,str <pdb format string>)
    setATOM_HA(str <XTALID>,str <FILENAME>)
    addATOM(str <XTALID>,str <TYPE>,
      float <X>,float <Y>,float <Z>,float <OCC>)
    addATOM_FULL(str <XTALID>,str <TYPE>,bool <ORTH>,
      dvect3 <X Y Z>,float <OCC>,bool <ISO>,float <ISOB>,
      bool <ANOU>,dmat6 <HH KK LL HK HL KL>,
      bool <FIXX>,bool <FIXO>,bool <FIXB>,bool <SWAPB>,
      str <SITE_NAME>)
    setATOM_CHAN_BFAC_WILS(bool)
    setATOM_CHAN_SCAT(bool)
    setATOM_CHAN_SCAT_TYPE(str <TYPE>)

.. rubric:: |User1.gif|\ CLUSTER
   :name: user1.gifcluster

 CLUSTER PDB <PDBFILE>
    Sample coordinates for a cluster compound for experimental phasing.
    Clusters are specified with type XX. Ta6Br12 clusters do not need to
    have coordinates specified as the coordinates are in the phaser
    code. To use Ta6Br12 clusters, specify atomtypes/clusters as TX.

::

    setCLUS_PDB(str <PDBFILE>)
    addCLUS_PDB(str <ID>, str <PDBFILE>)

.. rubric:: |User1.gif|\ COMPOSITION
   :name: user1.gifcomposition

 COMPOSITION BY [ :sup:`1`\ AVERAGE\| :sup:`2`\ SOLVENT\| :sup:`3`\ ASU
]
    Alternative ways of defining composition
    :sup:`1` AVERAGE solvent fraction for crystals (50%)
    :sup:`2` Composition entered by solvent content.
    :sup:`3` Explicit description of composition of ASU by sequence or
    molecular weight
 :sup:`2`\ COMPOSITION PERCENTAGE <SOLVENT>
    Specified SOLVENT content
 :sup:`3`\ COMPOSITION PROTEIN [ MW <MW> \|SEQUENCE <FILE> \| NRES
<NRES> \| STR <STR> ] NUMBER <NUM>
    Contribution to composition of the ASU. The number of copies NUM of
    molecular weight MW or SEQ given in fasta format (in a file FILE) or
    number of residues <NRES> or a sequence string (no spaces) of
    protein in the asymmetric unit.
 :sup:`3`\ COMPOSITION NUCLEIC [ MW <MW> \|SEQUENCE <FILE> \| NRES
<NRES> \| STR <STR> ] NUMBER <NUM>
    Contribution to composition of the ASU. The number of copies NUM of
    molecular weight MW or SEQ given in fasta format (in a file FILE) or
    number of residues <NRES> or a sequence string (no spaces) of
    nucleic acid in the asymmetric unit.
 :sup:`3`\ COMPOSITION ATOM <TYPE> NUMBER <NUM>
    Add NUM copies of an atom (usually a heavy atom) to the composition

-  Default: COMPOSITION BY ASU

::

    setCOMP_BY(str ["AVERAGE" | "SOLVENT" | "ASU" ])
    setCOMP_PERC(float <SOLVENT>)
    addCOMP_PROT_MW_NUM(float <MW>,float <NUM>)
    addCOMP_PROT_STR_NUM(str <SEQ>,float <NUM>)
    addCOMP_PROT_NRES_NUM(float <NRES>,float <NUM>)
    addCOMP_PROT_SEQ_NUM(str <FILE>,float <NUM>)
    addCOMP_NUCL_MW_NUM(float <MW>,float <NUM>)
    addCOMP_NUCL_STR_NUM(str <SEQ>,float <NUM>)
    addCOMP_NUCL_NRES_NUM(float <NRES>,float <NUM>)
    addCOMP_NUCL_SEQ_NUM(str <FILE>,float <NUM>)
    addCOMP_ATOM(str <TYPE>,float <NUM>)

.. rubric:: |User1.gif|\ CRYSTAL
   :name: user1.gifcrystal

 CRYSTAL <XTALID> DATASET <WAVEID> LABIN Fpos =<F+> SIGFpos=<SIG+>
Fneg=<F-> SIGFneg=<SIG->
    Columns of MTZ file to read for this (anomalous) dataset
 CRYSTAL <XTALID> DATASET <WAVEID> LABIN F =<F> SIGF=<SIGF>
    Columns of MTZ file to read for this (non-anomalous) dataset. Used
    for LLG completion in SAD phasing when there is no anomalous signal
    (single atom MR protocol). Use LABIN for MR.

::

    setCRYS_ANOM_LABI(str <F+>,str <SIGF+>,str <F->,str <SIGF->)  
    setCRYS_MEAN_LABI(str <F>,str <SIGF>)

.. rubric:: |User1.gif|\ ENSEMBLE
   :name: user1.gifensemble

 ENSEMBLE <MODLID> [PDB\|CIF] <PDBFILE> [RMS <RMS>:sup:`1` \| ID
<ID>:sup:`2` \| CARD ON\ :sup:`3`] *CHAIN "<CHAIN>":sup:`4` MODEL
<NUM>:sup:`5`*
    The names of the PDB/CIF files used to build the ENSEMBLE, and
    either
    :sup:`1` The expected RMS deviation of the coordinates to the "real"
    structure
    :sup:`2` The percent sequence identity with the real sequence, which
    is converted to an RMS deviation.
    :sup:`3` The RMS deviation or sequence IDENTITY is parsed from
    special REMARK cards of the pdb file (e.g. "REMARK PHASER ENSEMBLE
    MODEL 1 ID 31.2") containing the superimposed models concatenated in
    the one file. This syntax enables simple automation of the use of
    ensembles. The pdb file can be non-standard because the atom list
    for the different models need not be the same.
    :sup:`4` CHAIN is selected from the pdb file.
    :sup:`5` MODEL number is selected from the pdb file.

 ENSEMBLE <MODLID> HKLIN <MTZFILE> F=<F> PHI=<PHI> EXTENT <EX> <EY> <EZ>
RMS <RMS> CENTRE <CX> <CY> <CZ> PROTEIN MW <PMW> NUCLEIC MW <NMW> *CELL
SCALE <SCALE>*
    An ENSEMBLE defined from a map (via an mtz file). The molecular
    weight of the object the map represents is required for scaling. The
    effective RMS coordinate error is needed to judge how the map
    accuracy falls off with resolution. For density obtained from an EM
    image reconstruction, a good first guess would be to take the
    resolution where the FSC curve drops below 0.5 and divide by 3. The
    extent (difference between maximum and minimum x,y,z coordinates of
    region containing model density) is needed to determine reasonable
    rotation steps, and the centre is needed to carry out a proper
    interpolation of the molecular transform. The extent and the centre
    are both given in Ångstroms. The cell scale factor defaults to 1 and
    can be refined (for example, if the map is from electron microscopy
    when the cell scale may be unknown to within a few percent).
 ENSEMBLE <MODLID> ATOM <TYPE> RMS <RMS>
    Define an ensemble as a single atom for single atom MR
 ENSEMBLE <MODLID> HELIX <NUM>
    Define an ensemble as a helix with NUM residues
 ENSEMBLE <MODLID> HETATM [ON\|OFF]
    Use scattering from HETATM records in pdb file. See `Coordinate
    Editing <../../../../articles/m/o/l/Molecular_Replacement.html#Coordinate_Editing>`__
 ENSEMBLE <MODLID> DISABLE CHECK [ON\|OFF]
    Toggle to disable checking of deviation between models in an
    ensemble. **Use with extreme caution**. Results of computations are
    not guaranteed to be sensible.
 ENSEMBLE <MODLID> ESTIMATOR [OEFFNER \| OEFFNERHI \| OEFFNERLO \|
CHOTHIALESK ]
    Define the estimator function for converting ID to RMS
 ENSEMBLE <MODLID> PTGRP [COVERAGE \| IDENTITY \| RMSD \| TOLANG \|
TOLSPC \| EULER \| SYMM ]
    Define the pointgroup parameters
 ENSEMBLE <MODLID> BINS [MIN <N>\| MAX <M>\| WIDTH <W> ]
    Define the Fcalc reflection binning parameters
 ENSEMBLE <MODLID> TRACE [PDB\|CIF] <PDBFILE>
    Define the coordinates used for packing independent of the
    coordinates used for structure factor calculation
    :sup:`1` The expected RMS deviation of the coordinates to the "real"
    structure
    :sup:`2` The percent sequence identity with the real sequence, which
    is converted to an RMS deviation.
    :sup:`3` The RMS deviation or sequence IDENTITY is parsed from
    special REMARK cards of the pdb file

 ENSEMBLE <MODLID> TRACE SAMPLING MIN <DIST>
    Set the minimum distance for the sampling of packing grid
 ENSEMBLE <MODLID> TRACE SAMPLING TARGET <NUM>
    Set the target for the number of points to sample the smallest
    molecule to be packed
 ENSEMBLE <MODLID> TRACE SAMPLING RANGE <NUM>
    Target range (TARGET+/-RANGE) for search for hexgrid points in
    protein volume
 ENSEMBLE <MODLID> TRACE SAMPLING USE [AUTO \| ALL \| CALPHA \| HEXGRID
]
    Sample trace coordinates using all atoms, C-alpha atoms, a hexagonal
    grid in the atomic volume or use an automatically determined choice
 ENSEMBLE <MODLID> TRACE SAMPLING DISTANCE <DIST>
    Set the distance for the sampling of the hexagonal grid explicitly
    and do not use TARGET to find default sampling distance
 ENSEMBLE <MODLID> TRACE SAMPLING WANG <WANG>
    Scale factor for the size of the Wang mask generated from an
    ensemble map
 ENSEMBLE <MODLID> TRACE PDB <PDBFILE>
    Use the given set of coordinates for packing rather than using the
    coordinates for structure factor calculation

-  Default: ENSEMBLE <MODLID> DISABLE CHECK OFF
-  Default: ENSEMBLE <MODLID> TRACE SAMPLING MIN 1.0
-  Default: ENSEMBLE <MODLID> TRACE SAMPLING TARGET 1000
-  Default: ENSEMBLE <MODLID> TRACE SAMPLING RANGE 100

::

    addENSE_PDB_ID(str <MODLID>,str <FILE>,float <ID>) 
    addENSE_PDB_RMS(str <MODLID>,st setENSE_TRAC_SAMP_MIN(MODLID,float <MIN>)
    setENSE_TRAC_SAMP_TARG(MODLID,int <TARGET>)
    setENSE_TRAC_SAMP_RANG(MODLID,int <RANGE>)
    setENSE_TRAC_SAMP_USE(MODLID,str)
    setENSE_TRAC_SAMP_DIST(MODLID,float <DIST>)
    setENSE_TRAC_SAMP_WANG(MODLID,float <WANG>)r <FILE>,float <RMS>)
    addENSE_CARD(str <MODLID>,str <FILE>,bool)
    addENSE_MAP(str <MODLID>,str <MTZFILE>,str <F>,str <PHI>,dvect3 <EX EY EZ>,
      float <RMS>,dvect3 <CX CY CZ>,float <PMW>,float <NMW>,float <CELL>)
    setENSE_DISA_CHEC(bool)

.. rubric:: |User1.gif|\ HKLIN
   :name: user1.gifhklin

 HKLIN <FILENAME>
    The mtz file containing the data

::

    setHKLI(str <FILENAME>)

.. rubric:: |User1.gif|\ JOBS
   :name: user1.gifjobs

 JOBS <NUM>
    Number of processors to use in parallelized sections of code

-  Default: JOBS 2

::

    setJOBS(int <NUM>)

.. rubric:: |User1.gif|\ LABIN
   :name: user1.giflabin

 LABIN F = <F> SIGF = <SIGF>
    Columns in mtz file. F must be given. SIGF should be given but is
    optional
 LABIN I = <I> SIGI = <SIGI>
    Columns in mtz file. I must be given. SIGI should be given but is
    optional
 LABIN FMAP = <PH> PHMAP = <PHMAP>
    Columns in mtz file. FMAP/PHMAP are weighted coefficients for use in
    the Phased Translation Function.
 LABIN FMAP = <PH> PHMAP = <PHMAP> FOM = <FOM>
    Columns in mtz file. FMAP/PHMAP are unweighted coefficients for use
    in the Phased Translation Function and FOM the figure of merit for
    weighting

::

    setLABI_F_SIGF(str <F>,str <SIGF>)
    setLABI_I_SIGI(str <I>,str <SIGI>)
    setLABI_PTF(str <FMAP>,str <PHMAP>,str <FOM>)

::

    Data should be input to python run-jobs using one data_refl parameter
    This is extracted from the ResultMR_DAT object after a call to runMR_DAT
    setREFL_DATA(data_ref)
    Example:
      i = InputMR_DAT()
      i.setHKLI("*.mtz")
      i.setLABI("F","SIGF")
      r = runMR_DAT(i)
      if r.Success():
        i = InputMR_LLG()
        i.setSPAC_HALL(r.getSpaceGroupHall())
        i.setCELL6(r.getUnitCell())
        i.setREFL_DATA(r.getDATA())
     See also:  Python Example Scripts

::

    Alternatively, reflection arrays can be set using cctbx::af::shared<double>
    setREFL_F_SIGF(float_array <F>,float_array <SIGF>)
    setREFL_I_SIGI(float_array <I>,float_array <SIGI>)
    setREFL_PTF(float_array <FMAP>,float_array <PHMAP>,float_array <FOM>)

.. rubric:: |User1.gif|\ MODE
   :name: user1.gifmode

 MODE [ ANO \| CCA \| SCEDS \| NMAXYZ \| NCS \| MR\_ELLG \| MR\_AUTO \|
MR\_GYRE \| MR\_ATOM \| MR\_ROT \| MR\_TRA \| MR\_RNP \| \| MR\_OCC \|
MR\_PAK \| EP\_AUTO \| EP\_SAD]
    The mode of operation of Phaser. The different modes are described
    in a separate page on `Keyword
    Modes <../../../../articles/k/e/y/Keyword_Modes.html>`__

::

    ResultANO    r = runANO(InputANO)
    ResultCCA    r = runCCA(InputCCA)
    ResultNMA    r = runSCEDS(InputNMA)
    ResultNMA    r = runNMAXYZ(InputNMA)
    ResultNCS    r = runNCS(InputNCS)
    ResultELLG   r = runMR_ELLG(InputMR_ELLG)
    ResultMR     r = runMR_AUTO(InputMR_AUTO)
    ResultEP     r = runMR_ATOM(InputMR_ATOM)
    ResulrMR_RF  r = runMR_FRF(InputMR_FRF)
    ResultMR_TF  r = runMR_FTF(InputMR_FTF)
    ResultMR     r = runMR_RNP(InputMR_RNP)
    ResultGYRE   r = runMR_GYRE(InputMR_RNP)
    ResultMR     r = runMR_OCC(InputMR_OCC)
    ResultMR     r = runMR_PAK(InputMR_PAK)
    ResultEP     r = runEP_AUTO(InputEP_AUTO)
    ResultEP_SAD r = runEP_SAD(InputEP_SAD)

.. rubric:: |User1.gif|\ PARTIAL
   :name: user1.gifpartial

 PARTIAL PDB <PDBFILE> [RMSIDENTITY] <RMS\_ID>
    The partial structure for MR-SAD substructure completion. Note that
    this must already be correctly placed, as the experimental phasing
    module will not carry out molecular replacement.
 PARTIAL HKLIN <MTZFILE> [RMS\|IDENTITY] <RMS\_ID>
    The partial electron density for MR-SAD substructure completion.
 PARTIAL LABIN FC=<FC> PHIC=<PHIC>
    Column labels for partial electron density for MR-SAD substructure
    completion.

::

    setPART_PDB(str <PDBFILE>)
    setPART_HKLI(str <MTZFILE>) 
    setPART_LABI_FC(str <FC>)
    setPART_LABI_PHIC(str <PHIC>)  
    setPART_VARI(str ["ID"|"RMS"])
    setPART_DEVI(float <RMS_ID>)

.. rubric:: |User1.gif|\ SEARCH
   :name: user1.gifsearch

 SEARCH ENSEMBLE <MODLID> *{OR ENSEMBLE <MODLID>}… NUMBER <NUM>*
    The ENSEMBLE to be searched for in a rotation search or an automatic
    search. When multiple ensembles are given using the OR keyword, the
    search is performed for each ENSEMBLE in turn. When the keyword is
    entered multiple times, each SEARCH keyword refers to a new
    component of the structure. If the component is present multiple
    times the sub-keyword NUMber can be used (rather than entering the
    same SEARCH keyword NUMber times).
 SEARCH ORDER AUTO [ON\|OFF]
    Search in the "best" order as estimated using estimated rms
    deviation and completeness of models.
 SEARCH METHOD [FULL\|FAST]
    Search using the `full
    search <../../../../articles/m/o/d/Modes.html#Modes>`__ or `fast
    search <../../../../articles/m/o/d/Modes.html#Modes>`__ algorithms.
 SEARCH PRUNE [ON\|OFF]
    For high TFZ solutions that fail the packing test, carry out a
    sliding-window occupancy refinement and prune residues that refine
    to low occupancy, in an effort to resolve packing clashes. If this
    flag is set to true, the flag for keeping high tfz score solutions
    (see `PACK <#PACK>`__) that don't pack is also set to true (PACK
    KEEP HIGH TFZ ON).
 SEARCH AMALGAMATE [ON\|OFF]
    For multiple high TFZ solutions, enable amalgamation into a single
    solution
 SEARCH DOWN PERCENT <PERC>
    `SEARCH METHOD FAST <#SEARCH>`__ only. Percentage to reduce rotation
    function cutoff if there is no TFZ over the zscore cutoff that
    determines a true solution (see `ZSCORE <#ZSCORE>`__) in first
    search.
 SEARCH BFACTOR <BFAC>
    B-factor applied to search molecule (or atom).
 SEARCH OFACTOR <OFAC>
    Occupancy factor applied to search molecule (or atom).

-  Default: SEARCH METHOD FAST
-  Default: SEARCH ORDER AUTO ON
-  Default: SEARCH DEEP ON
-  Default: SEARCH DOWN PERCENT 25
-  Default: SEARCH BFACTOR 0
-  Default: SEARCH OFACTOR 1
-  Default: SEARCH PRUNE ON

::

    addSEAR_ENSE_NUMB(str <MODLID>,int <NUM>) 
    addSEAR_ENSE_OR_ENSE_NUMB(string_array <MODLIDS>,int <NUM>) 
    setSEAR_ORDE_AUTO(bool])
    setSEAR_METH(str [ "FULL" | "FAST" ])
    setSEAR_PRUN(bool <PRUNE>)
    setSEAR_DOWN_PERC(float <PERC>)
    setSEAR_BFAC(float <BFAC>)
    setSEAR_OFAC(float <OFAC>)

.. rubric:: |User1.gif|\ SGALTERNATIVE
   :name: user1.gifsgalternative

 SGALTERNATIVE SELECT [:sup:`1`\ ALL\| :sup:`2`\ HAND\| :sup:`3`\ LIST\|
:sup:`4`\ NONE]
    Selection of alternative space groups to test in translation
    functions i.e. those that are in same laue group as that given in
    `SPACEGROUP <#SPACEGROUP>`__
    :sup:`1` Test all possible space groups,
    :sup:`2` Test the given space group and its enantiomorph.
    :sup:`3` Test the space groups listed with SGALTERNATIVE TEST <SG>.
    :sup:`4` Do not test alternative space groups.
 SGALTERNATIVE TEST <SG>
    Alternative space groups to test. Multiple test space groups can be
    entered.

-  Default: SGALTERNATIVE SELECT HAND

::

    setSGAL_SELE(str [ "ALL" | "HAND" | "LIST" | "NONE" ]) 
    addSGAL_TEST(str <SG>)

.. rubric:: |User1.gif|\ SOLUTION
   :name: user1.gifsolution

 SOLUTION SET <ANNOTATION>
    Start new set of solutions
 SOLUTION TEMPLATE <ANNOTATION>
    Specifies a template solution against which other solutions in this
    run will be compared. Given in place of SOLUTION SET. Template
    rotation and translations given by subsequent SOLUTION 6DIM cards as
    per SOLUTION SETS.
 SOLUTION 6DIM ENSEMBLE <MODLID> EULER <A> <B> <C> [ORTH\|FRAC] <X> <Y>
<Z> *FIXR [ON\|OFF] FIXT [ON\|OFF] FIXB [ON\|OFF] BFAC <BFAC> MULT
<MULT>*
    This keyword is repeated for each known position and orientation of
    an ENSEMBLE MODLID. A B G are the Euler angles (z-y-z convention)
    and X Y Z are the translation elements, expressed either in
    orthogonal Angstroms (ORTH) or fractions of a cell edge (FRAC). The
    input ensemble is transformed by a rotation around the origin of the
    coordinate system, followed by a translation. BFAC default to 0,
    MULT (for multiplicity) defaults to 1.
 SOLUTION ENSEMBLE <MODLID> VRMS DELTA <DELTA> RMSD <RMSD>
    Refined RMS variance terms for pdb files (or map) in ensemble
    MODLID. RMSD is the input RMSD of the job that produced the sol
    file, DELTA is the shift with respect to this RMSD. If given as part
    of a solution, these values overwrite the values used for input in
    the ENSEMBLE keyword (if refined).
 SOLUTION ENSEMBLE <MODLID> CELL SCALE <SCALE>
    Refined cell scale factor. Only applicable to ensembles that are
    maps
 SOLUTION TRIAL ENSEMBLE <MODLID> EULER <A> <B> <C> RFZ <RFZ>
    Rotation List for translation function
 SOLUTION ORIGIN ENSEMBLE <MODLID>
    Create solution for ensemble MODLID at the origin
 SOLUTION SPACEGROUP <SG>
    Space Group of the solution (if alternative spacegroups searched)
 SOLUTION RESOLUTION <HIRES>
    High resolution limit of data used to find/refine this solution
 SOLUTION PACKS <PACKS>
    Flag for whether solution has been retained despite failing packing
    test, due to having high TFZ

::

    setSOLU(mr_solution <SOL>) 
    addSOLU_SET(str <ANNOTATION>) 
    addSOLU_TEMPLATE(str <ANNOTATION>) 
    addSOLU_6DIM_ENSE(str <MODLID>,dvect3 <A B C>,bool <FRAC>,dvect3 <X Y Z>,
      float <BFAC>,bool <FIXR>,bool <FIXT>,bool <FIXB>,int <MULT>) 
    addSOLU_ENSE_DRMS(str <MODLID>, float <DRMS>) 
    addSOLU_ENSE_CELL(str <MODLID>, float <SCALE>)
    addSOLU_TRIAL_ENSE(string <MODLID>,dvect3 <A B C>,float <RFZ>)
    addSOLU_ORIG_ENSE(string <MODLID>)
    setSOLU_SPAC(str <SG>)
    setSOLU_RESO(float <HIRES>)
    setSOLU_PACK(bool <PACKS>)

.. rubric:: |User1.gif|\ SPACEGROUP
   :name: user1.gifspacegroup

 SPACEGROUP <SG>
    Space group may be altered from the one on the MTZ file to a space
    group in the same point group. The space group can be entered in one
    of three ways

#. The Hermann-Mauguin symbol e.g. P212121 or P 21 21 21 (with or
   without spaces)
#. The international tables number, which gives standard setting e.g. 19
#. The Hall symbols e.g. P 2ac 2ab

**The space group can also be a subgroup of the merged space group**.
For example, P1 is always allowed. The reflections will be expanded to
the symmetry of the given subgroup. This is only a valid approach when
the true symmetry is the symmetry of the subgroup and perfect twinning
causes the data to merge "perfectly" in the higher symmetry.
**A list of the allowed space groups in the same point group as the
given space group (or space group read from MTZ file) and allowed
subgroups of these is given in the Cell Content Analysis logfile.**

-  Default: Read from MTZ file

::

    setSPAC_NUM(int <NUM>)
    setSPAC_NAME(string <HM>)
    setSPAC_HALL(string <HALL>)

.. rubric:: |User1.gif|\ WAVELENGTH
   :name: user1.gifwavelength

 WAVELENGTH <LAMBDA>
    The wavelengh at which the SAD dataset was collected

::

    setWAVE(float <LAMBDA>)

| 

.. rubric:: Output Control Keywords
   :name: output-control-keywords

.. rubric:: |Output.png|\ DEBUG
   :name: output.pngdebug

 DEBUG [ON\|OFF]
    Extra verbose output for debugging

-  Default: DEBUG OFF

::

    setDEBU(bool) 

.. rubric:: |Output.png|\ EIGEN
   :name: output.pngeigen

 EIGEN WRITE [ON\|OFF]
EIGEN READ <EIGENFILE>
    Read or write a file containing the eigenvectors and eigenvalues. If
    reading, the eigenvalues and eigenvectors of the Hessian are read
    from the file generated by a previous run, rather than calculated.
    This option must be used with the job that generated the eigenfile
    and the job reading the eigenfile must have identical input for the
    ENM parameters. Use WRITE to control whether or not the eigenfile is
    written when not using the READ mode.

-  Default: EIGEN WRITE ON

::

    setEIGE_WRIT(bool)
    setEIGE_READ(str <EIGENFILE>)

.. rubric:: |Output.png|\ HKLOUT
   :name: output.pnghklout

 HKLOUT [ON\|OFF]
    Flags for output of an mtz file containing the phasing information

-  Default: HKLOUT ON

::

    setHKLO(bool) 

.. rubric:: |Output.png|\ KEYWORDS
   :name: output.pngkeywords

 KEYWORDS [ON\|OFF]
    Write output Phaser .sol file (.rlist file for rotation function)

-  Default: KEYWORDS ON

::

    setKEYW(bool)

.. rubric:: |Output.gif|\ LLGMAPS
   :name: output.gifllgmaps

 LLGMAPS [ON\|OFF]
    Write log-likelihood gradient map coefficients to MTZ file

-  Default: LLGMAPS OFF

::

    setLLGM(bool <True|False>)

.. rubric:: |Output.png|\ MUTE
   :name: output.pngmute

 MUTE [ON\|OFF]
    Toggle for running in silent/mute mode, where no logfile is written
    to *standard output*.

-  Default: MUTE OFF

::

    setMUTE(bool) 

.. rubric:: |Output.png|\ KILL
   :name: output.pngkill

 KILL TIME [MINS]
    Kill Phaser after MINS minutes of CPU have elapsed, provided
    parallel section is complete
 KILL FILE [FILENAME]
    Kill Phaser if file FILENAME is present, provided parallel section
    is complete

-  Default: KILL TIME 0 *Phaser runs to completion*
-  Detaulf: KILL FILE "" *Phaser runs to completion*

::

    setKILL_TIME(float) 
    setKILL_FILE(string)

.. rubric:: |Output.png|\ OUTPUT
   :name: output.pngoutput

 OUTPUT LEVEL [SILENT\|CONCISE\|SUMMARY\|LOGFILE\|VERBOSE\|DEBUG]
    Output level for logfile
 OUTPUT LEVEL [0\|1\|2\|3\|4\|5]
    Output level for logfile (equivalent to keyword level setting)

-  Default: OUTPUT LEVEL LOGFILE

::

    setOUTP_LEVE(string)
    setOUTP(enum)

.. rubric:: |Output.png|\ TITLE
   :name: output.pngtitle

 TITLE <TITLE>
    Title for job

-  Default: TITLE [no title given]

::

    setTITL(str <TITLE>)

.. rubric:: |Output.png|\ TOPFILES
   :name: output.pngtopfiles

 TOPFILES <NUM>
    Number of top pdbfiles or mtzfiles to write to output.

-  Default: TOPFILES 1

::

    setTOPF(int <NUM>) 

.. rubric:: |Output.png|\ ROOT
   :name: output.pngroot

 ROOT <FILEROOT>
    Root filename for output files (e.g. FILEROOT.log)

-  Default: ROOT PHASER

::

    setROOT(string <FILEROOT>)

.. rubric:: |Output.png|\ VERBOSE
   :name: output.pngverbose

 VERBOSE [ON\|OFF]
    Toggle to send verbose output to log file.

-  Default: VERBOSE OFF

::

    setVERB(bool) 

.. rubric:: |Output.png|\ XYZOUT
   :name: output.pngxyzout

 XYZOUT [ON\|OFF] *ENSEMBLE [ON\|OFF]:sup:`1` PACKING [ON\|OFF]:sup:`2`*
    Toggle for output coordinate files.
    :sup:`1` If the optional ENSEMBLE keyword is ON, then each placed
    ensemble is written to its own pdb file. The files are named
    FILEROOT.#.#.pdb with the first # being the solution number and the
    second # being the number of the placed ensemble (representing a
    SOLU 6DIM entry in the .sol file).
    :sup:`2` If the optional PACKING keyword is ON, then the hexagonal
    grid used for the packing analysis is output to its own pdb file
    FILEROOT.pak.pdb

-  Default: XYZOUT OFF (Rotation functions)
-  Default: XYZOUT ON ENSEMBLE OFF (all other relevant modes)
-  Default: XYZOUT ON PACKING OFF (all other relevant modes)

::

    setXYZO(bool) 
    setXYZO_ENSE(bool)
    setXYZO_PACK(bool)

| 

.. rubric:: Advanced Keywords
   :name: advanced-keywords

.. rubric:: |User2.gif|\ ELLG
   :name: user2.gifellg

(`explained
here <../../../../articles/m/o/l/Molecular_Replacement.html#Should_Phaser_Solve_It.3F>`__)

 ELLG TARGET <TARGET>
    Target value for expected LLG for determining resolution limits and
    search order

-  Default: ELLG TARGET 120

::

    setELLG_TARG(float <TARGET>)

.. rubric:: |User2.gif|\ FORMFACTORS
   :name: user2.gifformfactors

 FORMFACTORS [XRAY \| ELECTRON \| NEUTRON]
    Use scattering factors from x-ray, electron or neutrons

-  Default: FORMFACTORS XRAY

::

    setFORM(string XRAY)

.. rubric:: |User2.gif|\ HAND
   :name: user2.gifhand

 HAND [ :sup:`1`\ ON\| :sup:`2`\ OFF\| :sup:`3`\ BOTH]
    Hand of heavy atoms for experimental phasing
    :sup:`1`\ Phase using the other hand of heavy atoms
    :sup:`2`\ Phase using given hand of heavy atoms
    :sup:`3`\ Phase using both hands of heavy atoms

-  Default: HAND BOTH

::

    setHAND(str [ "OFF" | "ON" | "BOTH" ])

.. rubric:: |User2.gif|\ LLGCOMPLETE
   :name: user2.gifllgcomplete

 LLGCOMPLETE COMPLETE [ON\|OFF]
    Toggle for structure completion by log-likelihood gradient maps
 LLGCOMPLETE SCATTERER <TYPE>
    Atom/Cluster type(s) to be used for log-likelihood gradient
    completion. If more than one element is entered for log-likelihood
    gradient completion, the atom type that gives the highest Z-score
    for each peak is selected. Type = "RX" is a purely real scatterer
    and type="AX" is purely anomalous scatterer
 LLGCOMPLETE REAL ON
    Use a purely real scatterer for log-likelihood gradient completion
    (equivalent to LLGCOMPLETE SCATTERER RX)
 LLGCOMPLETE ANOMALOUS ON
    Use a purely anomalous scatterer for log-likelihood gradient
    completion (equivalent to LLGCOMPLETE SCATTERER AX)
 LLGCOMPLETE CLASH <CLASH>
    Minimum distance between atoms in log-likelihood gradient maps and
    also the distance used for determining anisotropy of atoms (default
    determined by resolution, flagged by CLASH=0)
 LLGCOMPLETE SIGMA <Z>
    Z-score (sigma) for accepting peaks as new atoms in log-likelihood
    gradient maps
 LLGCOMPLETE NCYC <NMAX>
    Maximum number of cycles of log-likelihood gradient structure
    completion. By default, NMAX is 50, but this limit should never be
    reached, because all features in the log-likelihood gradient maps
    should be assigned well before 50 cycles are finished. This keyword
    should be used to reduce the number of cycles to 1 or 2.
 LLGCOMPLETE METHOD [IMAGINARY\|ATOMTYPE]
    Pick peaks from the imaginary map only or from all the completion
    atomtype maps.

-  Default: LLGCOMPLETE COMPLETE OFF
-  Default: LLGCOMPLETE CLASH 0
-  Default: LLGCOMPLETE SIGMA 6
-  Default: LLGComplete NCYC 50
-  Default: LLGComplete METHOD ATOMTYPE

::

    setLLGC_COMP(bool <True|False>) 
    setLLGC_CLAS(float <CLASH>) 
    setLLGC_SIGM(float <Z>)  
    setLLGC_NCYC(int <NMAX>)
    setLLGC_METH(str ["IMAGINARY"|"ATOMTYPE"])

.. rubric:: |User2.gif|\ NMA
   :name: user2.gifnma

 NMA MODE <M1> *{MODE <M2>…}*
    The MODE keyword gives the mode along which to perturb the
    structure. If multiple modes are entered, the structure is perturbed
    along all the modes AND combinations of the modes given. There is no
    limit on the number of modes that can be entered, but the number of
    pdb files explodes combinatorially. The first 6 modes are the pure
    rotation and translation modes and do not lead to perturbation of
    the structure. If the number M is less than 7 then M is interpreted
    as the first M modes starting at 7, so for example, MODE 5 would
    give modes 7 8 9 10 11.
 NMA COMBINATION <NMAX>
    Controls how many modes are present in any combination.

-  Default: NMA MODE 5
-  Default: NMA COMBINATION 2

::

    addNMA_MODE(int <MODE>) 
    setNMA_COMB(int <NMAX>)

.. rubric:: |User2.gif|\ PACK
   :name: user2.gifpack

 PACK SELECT [ :sup:`1`\ PERCENT \| :sup:`2`\ ALL ]
    :sup:`1`\ Allow up to the PERCENT of sampling points to clash,
    considering only pairwise clashes
    :sup:`2`\ Allow all solutions (no packing test)
 PACK CUTOFF <PERCENT>
    Limit on total number (or percent) of clashes
 PACK QUICK [ON\|OFF]
    Packing check stops when ALLOWED\_CLASHES or MAX\_CLASHES is
    reached. However, all clashes are found when the solution has a high
    Z-score (see `ZSCORE <#ZSCORE>`__).
 PACK COMPACT [ON\|OFF]
    Pack ensembles into a compact association (minimize distances
    between centres of mass for the addition of each component in a
    solution).
 PACK KEEP HIGH TFZ [ON\|OFF]
    Solutions with high tfz (see `ZSCORE <#ZSCORE>`__) but that fail to
    pack are retained in the solution list. Set to true if SEARCH PRUNE
    ON (see `SEARCH <#SEARCH>`__)

-  Default: PACK SELECT PERCENT
-  Default: PACK CUTOFF 5
-  Default: PACK COMPACT ON
-  Default: PACK QUICK ON
-  Default: PACK KEEP HIGH TFZ OFF
-  Default: PACK CONSERVATION DISTANCE 1,5

::

    setPACK_SELE(str ["PERCENT"|"ALL"]) 
    setPACK_CUTO(float <ALLOWED_CLASHES>)
    setPACK_QUIC(bool)
    setPACK_KEEP_HIGH_TFZ(bool)
    setPACK_COMP(bool)

.. rubric:: |User2.gif|\ PEAKS
   :name: user2.gifpeaks

 PEAKS TRA SELECT [ :sup:`1`\ PERCENT \| :sup:`2`\ SIGMA \|
:sup:`3`\ NUMBER \| :sup:`4`\ ALL]
PEAKS ROT SELECT [ :sup:`1`\ PERCENT \| :sup:`2`\ SIGMA \|
:sup:`3`\ NUMBER \| :sup:`4`\ ALL]
    Peaks for individual rotation functions (ROT) or individual
    translation functions (TRA) satisfying selection criteria are saved.
    To be used for subsequent steps, peaks must also satisfy the overall
    `PURGE <#PURGE>`__ selection criteria, so it will usually be
    appropriate to set only the PURGE criteria (which over-ride the
    defaults for the PEAKS criteria if not explicitly set). See `How to
    Select
    Peaks <../../../../articles/m/o/l/Molecular_Replacement.html#How_to_Select_Peaks>`__
    :sup:`1` Select peaks by taking all peaks over CUTOFF percent of the
    difference between the top peak and the mean value.
    :sup:`2` Select peaks by taking all peaks with a Z-score greater
    than CUTOFF.
    :sup:`3` Select peaks by taking top CUTOFF.
    :sup:`4` Select all peaks.
 PEAKS ROT CUTOFF <CUTOFF>
PEAKS TRA CUTOFF <CUTOFF>
    Cutoff value for the rotation function (ROT) or translation function
    (TRA) peak selection criteria.
    If selection is by percent and `PURGE PERCENT <#PURGE>`__ is changed
    from the default, then the PEAKS percent value is set to the lower
    PURGE percent value.
 PEAKS ROT CLUSTER [ON\|OFF]
PEAKS TRA CLUSTER [ON\|OFF]
    Toggle selects clustered or unclustered peaks for rotation function
    (ROT) or translation function (TRA).
 PEAKS ROT DOWN [PERCENT]
    `SEARCH METHOD FAST <#SEARCH>`__ only. Percentage to reduce rotation
    function cutoff if there is no TFZ over the zscore cutoff that
    determines a true solution (see `ZSCORE <#ZSCORE>`__) in first
    search.

-  Default: PEAKS ROT SELECT PERCENT
-  Default: PEAKS TRA SELECT PERCENT
-  Default: PEAKS ROT CUTOFF 75
-  Default: PEAKS TRA CUTOFF 75
-  Default: PEAKS ROT CLUSTER ON
-  Default: PEAKS TRA CLUSTER ON

::

    setPEAK_ROTA_SELE(str ["SIGMA"|"PERCENT"|"NUMBER"|"ALL"]) 
    setPEAK_TRAN_SELE(str ["SIGMA"|"PERCENT"|"NUMBER"|"ALL"])
    setPEAK_ROTA_CUTO(float <CUTOFF>)
    setPEAK_TRAN_CUTO(float <CUTOFF>) 
    setPEAK_ROTA_CLUS(bool <CLUSTER>) 
    setPEAK_TRAN_CLUS(bool <CLUSTER>)
    setPEAK_ROTA_DOWN(float <PERCENT>)

.. rubric:: |User2.gif|\ PERMUTATIONS
   :name: user2.gifpermutations

 PERMUTATIONS [ON\|OFF]
    Only relevant to `SEARCH METHOD FULL <#SEARCH>`__. Toggle for
    whether the order of the search set is to be permuted.

-  Default: PERMUTATIONS OFF

::

    setPERM(bool <PERMUTATIONS>)

.. rubric:: |User2.gif|\ PERTURB
   :name: user2.gifperturb

 PERTURB RMS STEP <RMS>
    Increment in rms Ångstroms between pdb files to be written.
 PERTURB RMS MAX <MAXRMS>
    The structure will be perturbed along each mode until the MAXRMS
    deviation has been reached.
 PERTURB RMS DIRECTION [FORWARD\|BACKWARD\|TOFRO]
    The structure is perturbed either forwards or backwards or
    to-and-fro (FORWARD\|BACKWARD\|TOFRO) along the eigenvectors of the
    modes specified.
 PERTURB INCREMENT [RMS\| DQ]
    Perturb the structure by rms devitations along the modes, or by set
    dq increments
 PERTURB DQ <DQ1> *{DQ <DQ2>…}*
    Alternatively, the DQ factors (as used by the Elnemo server (K.
    Suhre & Y-H. Sanejouand, NAR 2004 vol 32) ) by which to perturb the
    atoms along the eigenvectors can be entered directly.

-  Default: PERTURB INCREMENT RMS
-  Default: PERTURB RMS STEP 0.2
-  Default: PERTURB RMS MAXRMS 0.3
-  Default: PERTURB RMS DIRECTION TOFRO

::

    setPERT_INCR(str [ "RMS" | "DQ" ])
    setPERT_RMS_MAXI(float <MAX>)
    setPERT_RMS_DIRE(str [ "FORWARDS" | "BACKWARDS" | "TOFRO" ]) 
    addPERT_DQ(float <DQ>)

.. rubric:: |User2.gif|\ PURGE
   :name: user2.gifpurge

 PURGE ROT ENABLE [ON\|OFF]:sup:`1`
PURGE ROT PERCENT <PERC>:sup:`2`
PURGE ROT NUMBER <NUM>:sup:`3`
    Purging criteria for rotation function (RF), where PERCENT and
    NUMBER are alternative selection criteria (*OR* criteria)
    :sup:`1` Toggle for whether to purge the solution list from the RF
    according to the top solution found. If there are a number of RF
    searches with different partial solution backgrounds, the PURGE is
    applied to the total list of peaks. If there is one clearly
    significant RF solution from one of the backgrounds it acts to purge
    all the less significant solutions. If there is only one RF (a
    single partial solution background) the PURGE gives no additional
    selection over and above the PEAKS command.
    :sup:`2` `Selection by
    percent <../../../../articles/m/o/l/Molecular_Replacement.html#How_to_Select_Peaks>`__.
    PERC is percentage of the top peak, where the value of the top peak
    is defined as 100% and the value of the mean is defined as 0%. Note
    that this criterion is applied subsequent to any selection criteria
    applied through the `PEAKS <#PEAKS>`__ command. If the
    `PEAKS <#PEAKS>`__ selection is by percent, then the percent cutoff
    given in the PURGE command over-rides that for `PEAKS <#PEAKS>`__,
    so as to rationalize results in the case of only a single RF (single
    partial solution background.)
    :sup:`3` NUM is the number of solutions to retain in purging. If
    NUMBER is given (non-zero) it overrides the PERCENT option. NUMBER
    given as zero is the flag for not applying this criterion.

 PURGE TRA ENABLE [ON\|OFF]
PURGE TRA PERCENT <PERC>
PURGE TRA NUMBER <NUM>
    As above, but for translation function
 PURGE RNP ENABLE [ON\|OFF]
PURGE RNP PERCENT <PERC>
PURGE RNP NUMBER <NUM>
    As above but for refinement, but with the important distinction that
    PERCENT and NUMBER are concurrent selection criteria (*AND*
    criteria)

-  Default: PURGE ROT ENABLE ON
-  Default: PURGE ROT PERC 75
-  Default: PURGE ROT NUM 0
-  Default: PURGE TRA ENABLE ON
-  Default: PURGE TRA PERC 75
-  Default: PURGE TRA NUM 0
-  Default: PURGE RNP ENABLE ON
-  Default: PURGE RNP PERC 75
-  Default: PURGE RNP NUM 0

::

    setPURG_ROTA_ENAB(bool <ENABLE>)
    setPURG_TRAN_ENAB(bool <ENABLE>) 
    setPURG_RNP_ENAB(bool <ENABLE>)
    setPURG_ROTA_PERC(float <PERC>) 
    setPURG_TRAN_PERC(float <PERC>) 
    setPURG_RNP_PERC(float <PERC>)
    setPURG_ROTA_NUMB(float <NUM>) 
    setPURG_TRAN_NUMB(float <NUM>)
    setPURG_TRAN_NUMB(float <NUM>)
    setPURG_RNP_NUMB(float <NUM>)

.. rubric:: |User2.gif|\ RESOLUTION
   :name: user2.gifresolution

 RESOLUTION HIGH <HIRES>
    High resolution limit in Ångstroms.
 RESOLUTION LOW <LORES>
    Low resolution limit in Ångstroms.
 RESOLUTION AUTO HIGH <HIRES>
    High resolution limit in Ångstroms for final high resolution
    refinement in MR\_AUTO mode.

-  Default for molecular replacement: Set by `ELLG TARGET <#ELLG>`__ for
   structure solution, final refinement uses all data
-  Default for experimental phasing: All data used

::

    setRESO_HIGH(float <HIRES>) 
    setRESO_LOW(float <LORES>)
    setRESO_AUTO_HIGH(float <HIRES>) 
    setRESO_AUTO_LOW(float <LORES>)
    setRESO(float <HIRES>,float <LORES>)

.. rubric:: |User2.gif|\ ROTATE
   :name: user2.gifrotate

 ROTATE VOLUME FULL
    Sample all unique angles
 ROTATE VOLUME AROUND EULER <A> <B> <C> RANGE <RANGE>
    Restrict the search to the region of +/- RANGE degrees around
    orientation given by EULER

::

    setROTA_VOLU(string ["FULL"|"AROUND"|) 
    setROTA_EULE(dvect3 <A B C>) 
    setROTA_RANG(float <RANGE>)

.. rubric:: |User2.gif|\ SCATTERING
   :name: user2.gifscattering

 SCATTERING TYPE <TYPE> FP=<FP> FDP=<FDP> FIX [ON\|OFF\|EDGE]
    Measured scattering factors for a given atom type, from a
    fluorescence scan. FIX EDGE (default) fixes the fdp value if it is
    away from an edge, but refines it if it is close to an edge, while
    FIX ON or FIX OFF does not depend on proximity of edge.
 SCATTERING RESTRAINT [ON\|OFF]
    use Fdp restraints
 SCATTERING SIGMA <SIGMA>
    Fdp restraint sigma used is SIGMA multiplied by initial fdp value

-  Default: SCATTERING SIGMA 0.2
-  Default: SCATTERING RESTRAINT ON

::

    addSCAT(str <TYPE>,float <FP>,float <FDP, string <FIXFDP>) 
    setSCAT_REST(bool) 
    setSCAT_SIGM(float <SIGMA>)

.. rubric:: |User2.gif|\ SCEDS
   :name: user2.gifsceds

 SCEDS NDOM <NDOM>
    Number of domains into which to split the protein
 SCEDS WEIGHT SPHERICITY <WS>
    Weight factor for the the Density Test in the SCED Score. The
    Sphericity Test scores boundaries that divide the protein into more
    spherical domains more highly.
 SCEDS WEIGHT CONTINUITY <WC>
    Weight factor for the the Continuity Test in the SCED Score. The
    Continuity Test scores boundaries that divide the protein into
    domains contigous in sequence more highly.
 SCEDS WEIGHT EQUALITY <WE>
    Weight factor for the the Equality Test in the SCED Score. The
    Equality Test scores boundaries that divide the protein more equally
    more highly.
 SCEDS WEIGHT DENSITY <WD>
    Weight factor for the the Equality Test in the SCED Score. The
    Density Test scores boundaries that divide the protein into domains
    more densely packed with atoms more highly.

-  Default: SCEDS NDOM 2
-  Default: SCEDS WEIGHT EQUALITY 1
-  Default: SCEDS WEIGHT SPHERICITY 4
-  Default: SCEDS WEIGHT DENSITY 1
-  Default: SCEDS WEIGHT CONTINUITY 0

::

    setSCED_NDOM(int <NDOM>) 
    setSCED_WEIG_SPHE(float <WS>) 
    setSCED_WEIG_CONT(float <WC>)
    setSCED_WEIG_EQUA(float <WE>) 
    setSCED_WEIG_DENS(float <WD>)

.. rubric:: |User2.gif|\ TARGET
   :name: user2.giftarget

 TARGET ROT [FAST \| BRUTE]
    Target function for fast rotation searches (2). BRUTE searches all
    angles on a grid with the full rotation likelihood target. FAST uses
    a fast fft approximation to the likelihood target and only rescores
    the highest scoring of these rotations with the full rotation
    likelihood target.
 TARGET TRA [FAST \| BRUTE \| PHASED]
    Target function for fast translation searches (3). BRUTE searches
    all positions on a grid with the full translation likelihood target.
    FAST uses a fast fft approximation to the likelihood target and only
    rescores the highest scoring of these positions with the full
    translation likelihood target. PHASED selects the "phased
    translation function", for which a LABIN command should also be
    given, specifying FMAP, PHMAP and (optionally) FOM.

-  Default: TARGET ROT FAST
-  Default: TARGET TRA FAST

::

    setTARG_ROTA(str ["FAST"|"BRUTE"])
    setTARG_TRAN(str ["FAST"|"BRUTE"|"PHASED"])

.. rubric:: |User2.gif|\ TNCS
   :name: user2.giftncs

 TNCS NMOL <NMOL>
    Number of molecules/molecular assemblies related by single TNCS
    vector (usually only 2). If the TNCS is a pseudo-tripling of the
    cell then NMOL=3, a pseudo-quadrupling then NMOL=4 etc.

-  Default: TNCS NMOL 2

::

    setTNCS_NMOL(int <NMOL>)

| 

.. rubric:: |User2.gif|\ TRANSLATE
   :name: user2.giftranslate

 TRANSLATE VOLUME [ :sup:`1`\ FULL \| :sup:`2`\ REGION \| :sup:`3`\ LINE
\| :sup:`4`\ AROUND ])
    Search volume for brute force translation function.
    :sup:`1` Cheshire cell or Primitive cell volume.
    :sup:`2` Search region.
    :sup:`3` Search along line.
    :sup:`4` Search around a point.
 :sup:`1 2 3`\ TRANSLATE START <X Y Z>
:sup:`1 2 3`\ TRANSLATE END <X Y Z>
    Search within region or line bounded by START and END.
 :sup:`4`\ TRANSLATE POINT <X Y Z>
:sup:`4`\ TRANSLATE RANGE <RANGE>
    Search within +/- RANGE Ångstroms (not fractional coordinates, even
    if the search point is given as fractional coordinates) of a point
    <X Y Z>.
 TRANSLATE [ORTH \| FRAC]
    Coordinates are given in orthogonal or fractional values.
 TRANSLATE PACKING USE [ON \| OFF]
    Top translation function peak will be testes for packing.
 TRANSLATE PACKING CUTOFF <PERC>
    Percent pairwise packing used for packing function test of top TF
    peak. Equivalent to PACK CUTOFF <PERC> for packing function.

-  Default: TRANSLATE VOLUME FULL
-  Default: TRANSLATE PACK CUTOFF 50

::

    setTRAN_VOLU(string ["FULL"|"REGION"|"LINE"|"AROUND"])
    setTRAN_START(dvect <START>)
    setTRAN_END(dvect <END>)
    setTRAN_POINT(dvect <POINT>)
    setTRAN_RANGE(float <RANGE>)
    setTRAN_FRAC(bool <True=FRAC False=ORTH>)
    setTRAN_PACK_USE(bool)
    setTRAN_PACK_CUTO(float)

.. rubric:: |User2.gif|\ ZSCORE
   :name: user2.gifzscore

 ZSCORE USE [ON\|OFF]
    Use the TFZ tests. Only applicable with `SEARCH METHOD
    FAST <#SEARCH>`__. (Note Phaser-2.4.0 and below use "ZSCORE SOLVED
    0" to turn off the TFZ tests)
 ZSCORE SOLVED <ZSCORE\_SOLVED>
    Set the minimum TFZ that indicates a definite solution for
    amalgamating solutions in FAST search method.
ZSCORE STOP [ON\|OFF]
    Stop adding components beyond point where TFZ is below cutoff when
    adding multiple components in FAST mode. However, FAST mode will
    always add one component even if TFZ is below cutoff, or two
    components if starting search with no components input (no input
    solution).
 ZSCORE HALF [ON\|OFF]
    Set the TFZ for amalgamating solutions in the FAST search method to
    the maximum of ZSCORE\_SOLVED and half the maximum TFZ, to
    accommodate cases of partially correct solutions in very high TFZ
    cases (e.g. TFZ > 16)

-  Default: ZSCORE USE ON
-  Default: ZSCORE SOLVED 8
-  Default: ZSCORE HALF ON
-  Default: ZSCORE STOP ON

::

    setZSCO_USE(bool <True=ON False=OFF>)
    setZSCO_SOLV(floatType ZSCORE_SOLVED)
    setZSCO_HALF(bool <True=ON False=OFF>)
    setZSCO_STOP(bool <True=ON False=OFF>)

| 

.. rubric:: Expert Keywords
   :name: expert-keywords

.. rubric:: |Expert.gif|\ MACANO
   :name: expert.gifmacano

 MACANO PROTOCOL [DEFAULT\|CUSTOM\|OFF\|ALL]
    Protocol for the refinement of SigmaN in the anisotropy correction
 MACANO ANISO [ON\|OFF] BINS [ON\|OFF] SOLK [ON\|OFF] SOLB [ON\|OFF]
*{NCYCle <NCYC>} {MINIMIZER [BFGS\|NEWTON\|DESCENT]}*
    Macrocycle for the custom refinement of SigmaN in the anisotropy
    correction. Macrocycles are performed in the order in which they are
    entered.

-  Default: MACANO PROTOCOL DEFAULT

::

    setMACA_PROT(str [ "DEFAULT" | "CUSTOM" | "OFF" | "ALL" ])
    addMACA(bool <ANISO>,bool <BINS>,bool <SOLK>,bool <SOLB>,
      int <NCYC>,str [BFGS"|"NEWTON"|"DESCENT"])

.. rubric:: |Expert.gif|\ MACMR
   :name: expert.gifmacmr

 MACMR PROTOCOL [DEFAULT\|CUSTOM\|OFF\|ALL]
    Protocol for refinement of molecular replacement solutions
 MACMR ROT [ON\|OFF] TRA [ON\|OFF] BFAC [ON\|OFF] VRMS [ON\|OFF] *CELL
[ON\|OFF] LAST [ON\|OFF] NCYCLE <NCYC> MINIMIZER
[BFGS\|NEWTON\|DESCENT]*
    Macrocycle for custom refinement of molecular replacement solutions.
    Macrocycles are performed in the order in which they are entered.
    For description of VRMS see the
    `FAQ <../../../../articles/f/a/q/FAQ.html>`__. CELL is the scale
    factor for the unit cell for maps (EM maps). LAST is a flag that
    refines the parameters for the last component of a solution only,
    fixing all the others.
 MACMR CHAINS [ON\|OFF]
    Split the ensembles into chains for refinement
    Not possible as part of an automated mode

-  Default: MACMR PROTOCOL DEFAULT
-  Default: MACMR CHAINS OFF

::

    setMACM_PROT(str [ "DEFAULT" | "CUSTOM" | "OFF" | "ALL" ])
    addMACM(bool <ROT>,bool <TRA>,bool <BFAC>,bool <VRMS>,bool <CELL>,bool <LAST>,
      int <NCYC>,str [BFGS"|"NEWTON"|"DESCENT"])
    setMACM_CHAI(bool])

.. rubric:: |Expert.gif|\ MACOCC
   :name: expert.gifmacocc

 MACOCC PROTOCOL [DEFAULT\|CUSTOM\|OFF\|ALL]
    Protocol for refinement of molecular replacement solutions
 MACOCC NCYCLE <NCYC> MINIMIZER [BFGS\|NEWTON\|DESCENT]
    Macrocycle for custom refinement of occupancy for solutions.
    Macrocycles are performed in the order in which they are entered.

-  Default: MACOCC PROTOCOL DEFAULT

::

    setMACO_PROT(str [ "DEFAULT" | "CUSTOM" | "OFF" | "ALL" ])
    addMACO(int <NCYC>,str [BFGS"|"NEWTON"|"DESCENT"])

.. rubric:: |Expert.gif|\ MACSAD
   :name: expert.gifmacsad

 MACSAD PROTOCOL [DEFAULT\|CUSTOM\|OFF\|ALL]
    Protocol for SAD refinement.
    *n.b. PROTOCOL ALL will crash phaser and is only useful for
    debugging - see code for details*
 MACSAD XYZ [ON\|OFF] OCC [ON\|OFF] BFAC [ON\|OFF] FDP [ON\|OFF] SA
[ON\|OFF] SB [ON\|OFF] SP [ON\|OFF] SD [ON\|OFF] *{PK [ON\|OFF]} {PB
[ON\|OFF]} {NCYCLE <NCYC>} MINIMIZER [BFGS\|NEWTON\|DESCENT]*
    Macrocycle for SAD refinement. Macrocycles are performed in the
    order in which they are entered.

-  Default: MACSAD PROTOCOL DEFAULT

::

    setMACS_PROT(str [ "DEFAULT" | "CUSTOM" | "OFF" | "ALL" ])
    addMACS(
      bool <XYZ>,bool <OCC>,bool <BFAC>,bool <FDP>
      bool <SA>,bool <SB>,bool <SP>,bool <SD>,
      bool <PK>, bool <PB>,
      int <NCYC>,str [BFGS"|"NEWTON"|"DESCENT"])

.. rubric:: |Expert.gif|\ MACTNCS
   :name: expert.gifmactncs

 MACTNCS PROTOCOL [DEFAULT\|CUSTOM\|OFF\|ALL]
    Protocol for pseudo-translational NCS refinement.
 MACTNCS ROT [ON\|OFF] TRA [ON\|OFF] VRMS [ON\|OFF] *NCYCLE <NCYC>
MINIMIZER [BFGS\|NEWTON\|DESCENT]*
    Macrocycle for pseudo-translational NCS refinement. Macrocycles are
    performed in the order in which they are entered.

-  Default: MACTNCS PROTOCOL DEFAULT

::

    setMACT_PROT(str [ "DEFAULT" | "CUSTOM" | "OFF" | "ALL" ])
    addMACT(bool <ROT>,bool <TRA>,bool <VRMS>,
       int <NCYC>,str [BFGS"|"NEWTON"|"DESCENT"])

.. rubric:: |Expert.gif|\ OCCUPANCY
   :name: expert.gifoccupancy

 OCCUPANCY WINDOW ELLG <ELLG>
    Target eLLG for determining number of residues in window for
    occupancy refinement. The number of residues in a window will be an
    odd number. Occupancy refinement will be done for each offset of the
    refinement window.
 OCCUPANCY WINDOW NRES <NRES>
    As an alternative to using the eLLG to define the number of residues
    in window for occupancy refinement, the number may be input
    directly. NRES must be an odd number.
 OCCUPANCY WINDOW MAX <NRES>
    Maximum number of residues in an occupancy window (determined either
    by ellg or given as NRES) for which occupancy is refined. Prevents
    the refinement windows from spanning non-local regions of space.
 OCCUPANCY MIN <MINOCC> MAX <MAXOCC>
    Minimum and maximum values of occupancy during refinement.
 OCCUPANCY MERGE [ON/OFF]
    Merge refined occupancies from different window offsets to give
    final occupancies per residue. If OFF, occupancies from a single
    window offset will be used, selected using OCCUPANCY OFFSET <N>
 OCCUPANCY FRAC <FRAC>
    Minimum fraction of the protein for which the occupancy may be set
    to zero.
 OCCUPANCY OFFSET <N>
    If OCCUPANCY MERGE OFF, then <N> defines the single window offset
    from which final occupancies will be taken

-  Default: OCCUPANCY WINDOW ELLG 5
-  Default: OCCUPANCY WINDOW MAX 111
-  Default: OCCUPANCY MIN 0.01 MAX 1
-  Default: OCCUPANCY NCYCLES 1
-  Default: OCCUPANCY MERGE ON
-  Default: OCCUPANCY FRAC 0.5
-  Default: OCCUPANCY OFFSET 0

::

    setOCCU_WIND_ELLG(float <ELLG>)
    setOCCU_WIND_NRES(int <NRES>)
    setOCCU_WIND_MAX(int <NRES>)
    setOCCU_MIN(float <MIN>)
    setOCCU_MAX(float <MAX>)
    setOCCU_MERG(float <FRAC>)
    setOCCU_FRAC(bool)
    setOCCU_OFFS(int <N>)

.. rubric:: |Expert.gif|\ RESCORE
   :name: expert.gifrescore

 RESCORE ROT [ON\|OFF]
RESCORE TRA [ON\|OFF]
    Toggle for rescoring of fast rotation function (ROT) or fast
    translation function (TRA) search peaks.

-  Default: RESCORE ROT ON
-  Default: RESCORE TRA ON\|OFF will depend on whether phaser is running
   in the mode `MODE MR\_AUTO <#MODE>`__ with `SEARCH METHOD
   FAST <#SEARCH>`__ or with `SEARCH METHOD FULL <#SEARCH>`__, or
   running the translation function separately `MODE MR\_FTF <#MODE>`__.
   For `SEARCH METHOD FAST <#SEARCH>`__ the default also depends on
   whether or not the expected LLG target `ELLG TARGET
   <TARGET> <#ELLG>`__ value is reached.

::

    setRESC_ROTA(bool)
    setRESC_TRAN(bool)

.. rubric:: |Expert.gif|\ RFACTOR
   :name: expert.gifrfactor

 RFACTOR USE [ON\|OFF]
    For cases of searching for one ensemble when there is one ensemble
    in the asymmetric unit, the R-factor for the ensemble at the
    orientation and position in the input is calculated. If this value
    is low then MR is not performed in the MR\_AUTO job in FAST search
    mode. Instead, rigid body refinement is performed before exiting.
    Note that the same R-factor test and cutoff is used for judging
    success in single-atom molecular replacement (MR\_ATOM mode).
 RFACTOR CUTOFF <VALUE>
    Rfactor in percent used as cutoff for deciding whether or not the
    R-factor indicates that MR is not necessary.

-  Default: RFACTOR USE ON CUTOFF 35

::

    setRFAC_USE(bool)
    setRFAC_CUTO(float <PERCENT>)

.. rubric:: |Expert.gif|\ SAMPLING
   :name: expert.gifsampling

 SAMPLING ROT
SAMPLING TRA
    Sampling of search given in degrees for a rotation search and
    Ångstroms for a translation search. Sampling for rotation search
    depends on the mean radius of the Ensemble and the high resolution
    limit (dmin) of the search.

-  Default: SAMP = 2\*atan(dmin/(4\*meanRadius)) (ROTATION FUNCTION)
-  Default: SAMP = dmin/5; (BRUTE TRANSLATION FUNCTION)
-  Default: SAMP = dmin/4; (FAST TRANSLATION FUNCTION)

::

    setSAMP_ROTA(float )
    setSAMP_TRAN(float )

| 

.. rubric:: |User2.gif|\ TNCS
   :name: user2.giftncs-1

 TNCS USE [ON\|OFF]
    Use TNCS if present: apply TNCS corrections. (Note: was TNCS IGNORE
    [ON\|OFF] in Phaser-2.4.0)
 TNCS RLIST ADD [ON \| OFF]
    Supplement the rotation list used in the translation function with
    rotations already present in the list of known solutions. New
    molecules in the same orientation as those in the known search (as
    occurs with translational ncs) may not have peaks associated with
    them from the rotation function because the known molecules mask the
    presence of the ones not yet found.
 TNCS PATT HIRES <hires>
    High resolution limit for Patterson calculation for TNCS detection
 TNCS PATT LORES <lores>
    Low resolution limit for Patterson calculation for TNCS detection
 TNCS PATT PERCENT <percent>
    Percent of origin Patterson peak that qualifies as a TNCS vector
 TNCS PATT DISTANCE <distance>
    Minimum distance of Patterson peak from origin that qualifies as a
    TNCS vector
 TNCS TRANSLATION PERTURB [ON \| OFF]
    If the TNCS translation vector is on a special position, perturb the
    vector from the special position before refinement
 TNCS ROTATION RANGE <angle>
    Maximum deviation from initial rotation from which to look for
    rotational deviation. Default uses internally determined value based
    on resolution of data and size of G-function effective molecular
    radius. Value of 0 turns rotational refinement off.
 TNCS ROTATION SAMPLING <sampling>
    Sampling for rotation search. Default uses internally determined
    value based on resolution of data and size of G-function effective
    molecular radius. Value of 0 turns rotational refinement off.

-  Default: TNCS USE ON
-  Default: TNCS RLIST ADD ON
-  Default: TNCS PATT HIRES 5
-  Default: TNCS PATT LORES 10
-  Default: TNCS PATT PERCENT 20
-  Default: TNCS PATT DISTANCE 15
-  Default: TNCS TRANSLATION PERTURB ON

::

    setTNCS_USE(bool)
    setTNCS_RLIS_ADD(bool)
    setTNCS_PATT_HIRE(float <HIRES>)
    setTNCS_PATT_LORE(float <LORES>) 
    setTNCS_PATT_PERC(float <PERCENT>) 
    setTNCS_PATT_DIST(float <DISTANCE>)
    setTNCS_TRAN_PERT(bool)
    setTNCS_ROTA_RANG(float <RANGE>) 
    setTNCS_ROTA_SAMP(float <SAMPLING>) 

| 

.. rubric:: Developer Keywords
   :name: developer-keywords

.. rubric:: |Developer.gif|\ BINS
   :name: developer.gifbins

 BINS DATA [ :sup:`1`\ MIN <L> \| :sup:`2`\ MAX <H> \| :sup:`3`\ WIDTH
<W> ]
    The binning of the data.
    :sup:`1` L = minimum number of bins
    :sup:`2` H = maximum number of bins.
    :sup:`3` W = width of the bins in number of reflections
 BINS ENSE [ :sup:`1`\ MIN <L> \| :sup:`2`\ MAX <H> \| :sup:`3`\ WIDTH
<W> ]
    The binning of the calaculated structure factos for the ensembles.
    :sup:`1` L = minimum number of bins
    :sup:`2` H = maximum number of bins.
    :sup:`3` W = width of the bins in number of reflections

-  Default: BINS DATA MIN 6 MAX 50 WIDTH 500
-  Default: BINS ENSE MIN 6 MAX 1000 WIDTH 1000

::

    setBINS_DATA_MINI(float <L>)
    setBINS_DATA_MAXI(float <H>)
    setBINS_DATA_WIDT(float <W>)
    setBINS_ENSE_MINI(float <L>)
    setBINS_ENSE_MAXI(float <H>)
    setBINS_ENSE_WIDT(float <W>)

.. rubric:: |Developer.gif|\ BFACTOR
   :name: developer.gifbfactor

 BFACTOR WILSON RESTRAINT [ON\|OFF]
    Toggle to use the Wilson restraint on the isotropic component of the
    atomic B-factors in SAD phasing.
 BFACTOR SPHERICITY RESTRAINT [ON\|OFF]
    Toggle to use the sphericity restraint on the anisotropic B-factors
    in SAD phasing
 BFACTOR REFINE RESTRAINT [ON\|OFF]
    Toggle to use the restraint to zero for the molecular B-factor in
    molecular replacement.
 BFACTOR WILSON SIGMA <SIGMA>
    The sigma of the Wilson restraint.
 BFACTOR SPHERICITY SIGMA <SIGMA>
    The sigma of the sphericity restraint.
 BFACTOR REFINE SIGMA <SIGMA>
    The sigma of the restraint to zero for the molecular B-factor in
    molecular replacement.

-  Default: BFACTOR WILSON RESTRAINT ON
-  Default: BFACTOR SPHERICITY RESTRAINT ON
-  Default: BFACTOR REFINE RESTRAINT ON
-  Default: BFACTOR WILSON SIGMA 5
-  Default: BFACTOR SPHERICITY SIGMA 5
-  Default: BFACTOR REFINE SIGMA 6

::

    setBFAC_WILS_REST(bool <True|False>)
    setBFAC_SPHE_REST(bool <True|False>)
    setBFAC_REFI_REST(bool <True|False>) 
    setBFAC_WILS_SIGM(float <SIGMA>) 
    setBFAC_SPHE_SIGM(float <SIGMA>) 
    setBFAC_REFI_SIGM(float <SIGMA>)

.. rubric:: |Developer.gif|\ BOXSCALE
   :name: developer.gifboxscale

 BOXSCALE <BOXSCALE>
    Scale for box for calculating structure factors. The ensembles are
    put in a box equal to (extent of molecule)\*BOXSCALE

-  Default: BOXSCALE 4

::

    setBOXS<float <BOXSCALE>)

.. rubric:: |Developer.gif|\ CELL
   :name: developer.gifcell

 Python Only
    Unit cell dimensions

-  Default: Cell read from MTZ file

::

    setCELL(float <A>,float <B>,float <C>,float <ALPHA>,float <BETA>,float <GAMMA>)
    setCELL6(float_array <A B C ALPHA BETA GAMMA>)

.. rubric:: |Developer.gif|\ DDM
   :name: developer.gifddm

 DDM SLIDER <VAL>
    The SLIDER window width is used to smooth the Difference Distance
    Matrix.
 DDM DISTANCE MIN <VAL> MAX <VAL> STEP <VAL>
    The range and step interval, as the fraction of the difference
    between the lowest and highest DDM values used to step.
 DDM SEPARATION MIN <VAL> MAX <VAL>
    Through space separation in Angstroms used.
 DDM SEQUENCE MIN <VAL> MAX <VAL>
    The range of sequence separations between matrix pairs.
 DDM JOIN MIN <VAL> MAX <VAL>
    The range of lengths of the sequences to join if domain segments are
    discontinuous in percentages of the polypeptide chain.

-  Default: DDM SLIDER 0
-  Default: DDM DISTANCE MIN 1 MAX 5 STEP 50
-  Default: DDM SEPARATION MIN 7 MAX 14
-  Default: DDM SEQUENCE MIN 0 MAX 1
-  Default: DDM JOIN MIN 2 MAX 12

::

    setDDM_SLID(int <VAL>) 
    setDDM_DIST_STEP(int <VAL>) 
    setDDM_DIST_MINI(int <VAL>) 
    setDDM_DIST_MAXI(int <VAL>) 
    setDDM_SEPA_MINI(float <VAL>) 
    setDDM_SEPA_MAXI(float <VAL>)
    setDDM_JOIN_MINI(int <VAL>) 
    setDDM_JOIN_MAXI(int <VAL>) 
    setDDM_SEQU_MINI(int <VAL>) 
    setDDM_SEQU_MAXI(int <VAL>)

.. rubric:: |Developer.gif|\ ENM
   :name: developer.gifenm

 ENM OSCILLATORS [ :sup:`1`\ RTB \| :sup:`2`\ ALL ]
    Define the way the atoms are used for the elastic network model.
    :sup:`1`\ Use the rotation-translation block method.
    :sup:`2`\ Use all atoms to determine the modes (only for use on very
    small molecules, less than 250 atoms).
 ENM MAXBLOCKS <MAXBLOCKS>
    MAXBLOCKS is the number of rotation-translation blocks for the RTB
    analysis.
 ENM NRES <NRES>
    For the RTB analysis, by default NRES=0 and then it is calculated so
    that it is as small as it can be without reaching MAXBlocks.
 ENM RADIUS <RADIUS>
    Elastic Network Model interaction radius (Angstroms)
 ENM FORCE <FORCE>
    Elastic Network Model force constant

-  Default: ENM OSCILLATORS RTB MAXBLOCKS 250 NRES 0 RADIUS 5 FORCE 1

::

    setENM_OSCI(str ["RTB"|"CA"|"ALL"])
    setENM_RTB_MAXB(float <MAXB>)
    setENM_RTB_NRES(float <NRES>) 
    setENM_RADI(float <RADIUS>) 
    setENM_FORC(float <FORCE>)

.. rubric:: |Developer.gif|\ NORMALIZATION
   :name: developer.gifnormalization

Scripting Only
NORM EPSFAC WRITE <FILENAME>
The normalization factors that correct for anisotropy in the data are
written to BINARY file FILENAME
NORM EPSFAC READ <FILENAME>
The normalization factors that correct for anisotropy in the data are
read from BINARY file FILENAME. Further refinement is automatically
turned off.
Python Only
The normalization factors that correct for anisotropy in the data,
extracted from ResultANO object with getSigmaN(). Anisotropy should
subsequently be turned off with setMACA\_PROT("OFF").

::

    setNORM_DATA(data_norm <SIGMAN>)

.. rubric:: |Developer.gif|\ OUTLIER
   :name: developer.gifoutlier

 OUTLIER REJECT [ON\|OFF]
    Reject low probability data outliers
 OUTLIER PROB <PROB>
    Cutoff for rejection of low probablity outliers

-  Default: OUTLIER REJECT ON PROB 0.000001

::

    setOUTL_REJE(bool) 
    setOUTL_PROB(float <PROB>)

.. rubric:: |Developer.gif|\ PTGROUP
   :name: developer.gifptgroup

 PTGROUP COVERAGE <COVERAGE>
    Percentage coverage for two sequences to be considered in same
    pointgroup
 PTGROUP IDENTITY <IDENTITY>
    Percentage identity for two sequences to be considered in same
    pointgroup
 PTGROUP RMSD <RMSD>
    Percentage rmsd for two models to be considered in same pointgroup
 PTGROUP TOLERANCE ANGULAR <ANG>
    Angular tolerance for pointgroup
 PTGROUP TOLERANCE SPATIAL <DIST>
    Spatial tolerance for pointgroup

::

    setPTGR_COVE(float <COVERAGE>) 
    setPTGR_IDEN(float <IDENTITY>) 
    setPTGR_RMSD(float <RMSD>) 
    setPTGR_TOLE_ANGU(float <ANG>) 
    setPTGR_TOLE_SPAT(float <DIST>)

.. rubric:: |Developer.gif|\ RESHARPEN
   :name: developer.gifresharpen

 RESHARPEN PERCENTAGE <PERC>
    Perecentage of the B-factor in the direction of lowest fall-off (in
    anisotropic data) to add back into the structure factors F\_ISO and
    FWT and FDELWT so as to sharpen the electron density maps

-  Default: RESHARPEN PERCENT 100

::

    setRESH_PERC(float <PERCENT>)

.. rubric:: |Developer.gif|\ SOLPARAMETERS
   :name: developer.gifsolparameters

 SOLPARAMETERS SIGA FSOL <FSOL> BSOL <BSOL> MIN <MINSIGA>
    Babinet solvent parameters for Sigma(A) curves. MINSIGA is the
    minimum SIGA for Babinet solvent term (low resolution only). The
    solvent term in the Sigma(A) curve is given by
    max(1 - FSOL\*exp(-BSOL/(4d^2)),MINSIGA).
 SOLPARAMETERS BULK USE [ON\|OFF]
    Toggle for use of solvent mask scaling parameters for ensemble
    structure factors, applied to Fmask.
 SOLPARAMETERS BULK FSOL <FSOL> BSOL <BSOL>
    Solvent mask scaling parameters for ensemble structure factors,
    applied to Fmask.

-  Default: SOLPARAMETERS SIGA FSOL 1.05 BSOL 501 MIN 0.1
-  Default: SOLPARAMETERS SIGA RESTRAINT ON
-  Default: SOLPARAMETERS BULK USE OFF
-  Default: SOLPARAMETERS BULK FSOL 0.35 BSOL 45

::

    setSOLP_SIGA_FSOL(float <FSOL>) 
    setSOLP_SIGA_BSOL(float <BSOL>)
    setSOLP_SIGA_MIN(float <MINSIGA>)
    setSOLP_BULK_USE(bool)
    setSOLP_BULK_FSOL(float <FSOL>) 
    setSOLP_BULK_BSOL(float <BSOL>)

.. rubric:: |Developer.gif|\ TARGET
   :name: developer.giftarget

 TARGET ROT FAST TYPE [LERF1 \| CROWTHER]
    Target function type for fast rotation searches
 TARGET TRA FAST TYPE [LETF1 \| LETF2 \| CORRELATION]
    Target function type for fast translation searches

::

    setTARG_ROTA_TYPE(string TYPE)
    setTARG_TRAN_TYPE(string TYPE)

.. rubric:: |Developer.gif|\ TNCS
   :name: developer.giftncs

 TNCS ROTATION ANGLE <A> <B> <C>
    Input rotational difference between molecules related by the
    pseudo-translational symmetry vector, specified as rotations in
    degrees about x, y and z axes. Central value for grid search (RANGE
    > 0).
 TNCS ROTATION RANGE <A>
    Range of angular difference in rotation angles to explore around
    central angle.
 TNCS ROTATION SAMPLING <A>
    Sampling step for rotational grid search.
 TNCS TRA VECTOR <x y z>
    Input pseudo-translational symmetry vector (fractional coordinates).
    By default the translation is determined from the Patterson.
 TNCS VARIANCE RMSD <num>
    Input estimated rms deviation between pseudo-translational symmetry
    vector related molecules.
 TNCS VARIANCE FRAC <num>
    Input estimated fraction of cell content that obeys
    pseudo-translational symmetry.
 TNCS LINK RESTRAINT [ON \| OFF]
    Link the occupancy of atoms related by TNCS in SAD phasing
 TNCS LINK SIGMA <sigma>
    Sigma of link restraint of the occupancy of atoms related by TNCS in
    SAD phasing

-  Default: TNCS ROTATION ANGLE 0 0 0
-  Default: TNCS ROTATION RANGE -999 (determine from structure size and
   resolution)
-  Default: TNCS ROTATION SAMPLING -999 (determine from structure size
   and resolution)
-  Default: TNCS TRANSLATION VECTOR determined from position of
   Patterson peak
-  Default: TNCS VARIANCE RMSD 0.4
-  Default: TNCS VARIANCE FRAC 1
-  Default: TNCS LINK RESTRAINT ON
-  Default: TNCS LINK SIGMA 0.1

::

    setTNCS_ROTA_ANGL(dvect3 <A B C>) 
    setTNCS_TRAN_VECT(dvect3 <X Y Z>) 
    setTNCS_VARI_RMSD(float <RMSD>) 
    setTNCS_VARI_FRAC(float <FRAC>)
    setTNCS_LINK_REST(bool)
    setTNCS_LINK_SIGM(float <SIGMA>)

Scripting Only
TNCS EPSFAC WRITE <FILENAME>
The normalization factors that correct for tNCS in the data are written
to BINARY file FILENAME
TNCS EPSFAC READ <FILENAME>
The normalization factors that correct for tNCS in the data are read
from BINARY file FILENAME. Further refinement is automatically turned
off.
Python Only
The normalization factors that correct for tNCS in the data, extracted
from ResultNCS object with getPtNcs(). tNCS refinement should
subsequently be turned off with setMACT\_PROT("OFF").

::

    setTNCS(data_tncs <PTNCS>)

.. rubric:: |Developer.gif|\ TRANSLATE
   :name: developer.giftranslate

 TRANSLATE MAPS [ON \| OFF]
    Output maps of fast translation function FSS scoring functions
    Maps take the names <ROOT>.<ensemble>.k.e.map where k is the Known
    backgound number and e is the Euler angle number for that known
    background

-  Default: TRANSLATE MAPS OFF

::

    setTRAN_MAPS(bool)

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/k/e/y/Keywords.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Keywords&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Keywords>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 11:56, 15 September 2016 by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__. Based on work
   by `Randy Read <../../../../articles/r/a/n/User:Randy.html>`__, `Rob
   Oeffner <http://localhost/index.php?title=User:Rdo20&action=edit&redlink=1>`__
   and `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |User1.gif| image:: ../../../../images/d/2F/d/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |Output.png| image:: ../../../../images/4/2F/4/4/4a/Output.png
   :width: 48px
   :height: 48px
.. |User2.gif| image:: ../../../../images/7/2F/7/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |Expert.gif| image:: ../../../../images/b/2F/b/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |Developer.gif| image:: ../../../../images/e/2F/e/e/e3/Developer.gif
   :width: 48px
   :height: 48px
.. |Output.gif| image:: ../../../../images/e/2F/e/e/eb/Output.gif
   :width: 48px
   :height: 48px
.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
