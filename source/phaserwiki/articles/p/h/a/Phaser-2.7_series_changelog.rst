.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Phaser-2.7 series changelog
   :name: phaser-2.7-series-changelog
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

Phaser-2.7.14

-  solution coodinates placed nearest to input coordinates if possible
-  logfile width extended
-  default number of processors for parallelization increased to 4

Phaser-2.7.13

-  support for unicode

Phaser-2.7.12

-  GIMBLE refinement, which splits ensembles by chain for rigid body
   refinement
-  automatic tNCS NMOL determination in presence of commensurate
   modulation
-  bugfixes

input solution list checked for pass of intrinic TF packing test
ensembles can refine to zero occupany in pruning
Phaser-2.7.11

-  additional peak clustering selection/annotation functionality PEAK
   ... LEVEL
-  search deep only implemented for auto fast mode, not for FRF mode
-  packing test during FTF run by default with 50% pairwise packing
   cutoff
-  bugfixes

stored solution list does not carry phasing arrays excessive memory
occupancy refinement takes account of VRMS
Phaser-2.7.10

-  option to run packing test during FTF so as to eliminate non-packing
   translations from top
-  packing test during FTF run by default for helices
-  (selected) obsoleted keywords don't throw error reported as warning
   in logfile
-  bugfixes

peak search percent for translation function
Phaser-2.7.9

-  replace unweighted 2nd moments by inverse variance-weighted 2nd
   moments for twinning tests
-  pairwise packing by percent only, other packing modes obsoleted
-  PAK reported in annotiation is maximum percent overlap amongst
   solution set
-  bugfixes

use of input trace molecule
Phaser-2.7.8

-  Speed up packing test to O(N)
-  bugfixes

correctly increase scattering with implausible composition
Phaser-2.7.7

-  write tNCS and anisotropy parameters to binary files (on user
   request)
-  read TNCS/anisotropy binary files to avoid refinement, when running
   through scripts

Phaser-2.7.6

-  account for measurement error in eLLG calculation
-  rigid body refinement can separate chains in ensembles with MACMR
   CHAINS ON

Phaser-2.7.5

-  change Vm probabilities to Weichenberger & Rupp 2013 from
   Kantardjieff & Rupp 2003
-  CCA accounts for PROTEIN/NUCLEIC/MIXED compositions different Vm
   probabilities

Phaser-2.7.4

-  packing trace molecule can be entered independently of coordinates
   and map
-  packing volume for maps calculated using Wang mask algorithm
-  packing test by PAIRWISE only considers pairwise overlaps, not global
   overlap
-  packing sampling can be forced to ALL, CALPHA, HEXGRID or AUTO
   (default AUTO)
-  obsoleted packing test by BEST
-  automatic search number for single search ensemble
-  bugfixes

RMS from ID estimator function parameters (A,B,C) did not match
published parameters
negative variance when trying to amalgamate many solutions with negative
B-factors
chain overlap incorrectly detected for nucleic acid
Phaser-2.7.3

-  improve tests for RMS correlations between members of an ensemble
-  change from average scattering residues to polyalanine for target
   eLLG calculation

Phaser-2.7.2

-  determine number of average scattering residues required to achieve
   target eLLG
-  bugfixes

header for Rotation Gyration Refinement mode corrected
Phaser-2.7.1

-  selection by CHAIN and MODEL for PDB coordinate entry
-  concatenation of PDB files on ENSEMBLE keyword input obsoleted
-  occupancy refinement operates on single model, not ensembles
-  change to format of SOLUTION HISTORY
-  change to SOLUTION variance card, only parse/unparse DRMS
-  bugfixes

only use solutions that pack to determine top LLG for purge after
refinement
occupancy refinement using ensembles failing various asserts in some
circumstances
exclude solutions that fail to pack from duplicate test after refinement
incorrect selection of best model when CARD ON input used sequence
identity
reporting negative contrast (Cntrst) for LLG near purge mean
bugs in FAST mode solution reversion with pruning
Phaser-2.7.0

-  No functional changes from 2.6 series
-  new internal data structures for ensembles

ensemble parameters either input or worked out on input
Indexing of values in ensemble/model arrays consistent

-  introduce command line flag --changelog gives changelog and bugfixes
   from previous version

formal tracking of changes

-  bugfixes

Wilson B-factor not being carried internally or used correctly for
output pdb files
ID being used as RMS is eLLG calculation in MODE MR\_FRF

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/p/h/a/Phaser-2.7_series_changelog.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Phaser-2.7_series_changelog&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest
   revision <http://localhost/index.php/Phaser-2.7_series_changelog>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 12:25, 13 September 2016 by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
