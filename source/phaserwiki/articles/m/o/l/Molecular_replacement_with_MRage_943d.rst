.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Molecular replacement with MRage
   :name: molecular-replacement-with-mrage
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

This tutorial explains how to use to run phaser.MRage from the `Phenix
GUI <http://www.phenix-online.org>`__ (input files to run the same
through the command line examples are also available).

| 

+--------------------------------------------------------------------------+
| .. raw:: html                                                            |
|                                                                          |
|    <div id="toctitle">                                                   |
|                                                                          |
| .. rubric:: Contents                                                     |
|    :name: contents                                                       |
|                                                                          |
| .. raw:: html                                                            |
|                                                                          |
|    </div>                                                                |
|                                                                          |
| -  `1 Beta/blip <#Beta.2Fblip>`__                                        |
|                                                                          |
|    -  `1.1 Input <#Input>`__                                             |
|    -  `1.2 Output <#Output>`__                                           |
|                                                                          |
| -  `2 Lysozyme <#Lysozyme>`__                                            |
|                                                                          |
|    -  `2.1 Input <#Input_2>`__                                           |
|    -  `2.2 Output <#Output_2>`__                                         |
+--------------------------------------------------------------------------+

.. rubric::  Beta/blip
   :name: betablip

This is a complex between beta lactamase (*beta*) and beta lactamase
inhibitor (*blip*). There are good models available, but the space group
is ambiguous. This tutorial shows basic MRage functionality with a
two-component system.

Experimental data for this tutorial is distributed with `Phenix
GUI <http://www.phenix-online.org>`__ and is also available from the
`Tutorials <http://www.phaser.cimr.cam.ac.uk/index.php/Tutorials>`__
page.

.. rubric::  Input
   :name: input

The MRage GUI is available from the main Phenix window under the
**Molecular replacement** tab (select the *Enable alpha features*
options under *File/Preferences* dialog for it to show up). On startup,
the GUI presents the **Basic options** dialog.

The following screenshot shows the this required settings to run the
tutorial. Note, that almost everything is filled in automatically once
the reflection file has been selected, or default. The only additional
change one needs to make is to switch the **space group exploration**
box to *enantiomorph* from its default value *dataset*, as the space
group is enantiomorphic, and it is not possible to know without solving
the structure, which hand is the correct one. In this tutorial, only a
single CPU is used, but feel free to use more if available! Note that
the **Overall count** box has been left blank. This tells the program to
determine the number of copies automatically.

+--------------------------------+
| |image0|                       |
+--------------------------------+
| The **Basic options** dialog   |
+--------------------------------+

Next, we need to tell the program about the composition of the
asymmetric unit and the available search models.

First, the *beta* component is described with its sequence, and the only
search model. Since there is no modification needed to this model (it is
100% identical), it is input through the **Ensemble** tab. This tells
the program that there is one alternative model for the *beta*
component.

+--------------------------------------+--------------------------------------+
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    <div class="center">              |    <div class="center">              |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    <div class="floatnone">           |    <div class="floatnone">           |
|                                      |                                      |
| |image1|                             | |image2|                             |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    </div>                            |    </div>                            |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    </div>                            |    </div>                            |
+--------------------------------------+--------------------------------------+
| The **Basic info** dialog of         | The **Ensembles** dialog of          |
| **Component1** (*beta*)              | **Component1** (*beta*)              |
+--------------------------------------+--------------------------------------+

Then, a new component is created for *blip* (using the **Add component**
button on the **Basic info** tab). This is filled out accordingly.

+--------------------------------------+--------------------------------------+
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    <div class="center">              |    <div class="center">              |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    <div class="floatnone">           |    <div class="floatnone">           |
|                                      |                                      |
| |image3|                             | |image4|                             |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    </div>                            |    </div>                            |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    </div>                            |    </div>                            |
+--------------------------------------+--------------------------------------+
| The **Basic info** dialog of         | The **Ensembles** dialog of          |
| **Component2** (*blip*)              | **Component2** (*blip*)              |
+--------------------------------------+--------------------------------------+

The job is ready to run!

Running from the command line
**Input file** (betablip.eff)

::

    hklin = beta_blip.mtz
    labin = Fobs,Sigma
    symmetry_exploration = enantiomorph

::

    composition
    {

::

      component
      {
        sequence = beta.seq
        ensemble
        {
          coordinates
          {
            pdb = beta.pdb
            identity = 1.00
          }
        }
      }

::

      component
      {
        sequence = blip.seq
        ensemble
        {
          coordinates
          {
            pdb = blip.pdb
            identity = 1.00
          }
        }
      }

::

    }

**Command line**

::

    phaser.MRage --verbosity=VERBOSE betablip.eff

.. rubric::  Output
   :name: output

The GUI displays the program output in the logfile window. First, the
program starts searching in both possible space groups.

| 

+--------------------------+--------------------------+--------------------------+
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div class="center">  |    <div class="center">  |    <div class="center">  |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="floatnone">    |    class="floatnone">    |    class="floatnone">    |
|                          |                          |                          |
| |image5|                 | |image6|                 | |image7|                 |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
+--------------------------+--------------------------+--------------------------+
| Initially, search starts | After finding the first  | ...and only P3₂21 is     |
| in both P3₁21 and P3₂21  | component, the search in | propagated with the      |
|                          | P3₁21 is dropped...      | partial solutions found. |
+--------------------------+--------------------------+--------------------------+

+--------------------------------------+--------------------------------------+
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    <div class="center">              |    <div class="center">              |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    <div class="floatnone">           |    <div class="floatnone">           |
|                                      |                                      |
| |image8|                             | |image9|                             |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    </div>                            |    </div>                            |
|                                      |                                      |
| .. raw:: html                        | .. raw:: html                        |
|                                      |                                      |
|    </div>                            |    </div>                            |
+--------------------------------------+--------------------------------------+
| The final solution: evaluation shows | Final solution shown in the GUI      |
| a high probability for it to be      | **Top solution** tab with possible   |
| correct.                             | further steps.                       |
+--------------------------------------+--------------------------------------+

.. rubric::  Lysozyme
   :name: lysozyme

As a frequently used test protein, there are good models for lysozyme.
This tutorial demonstrates how to solve lysozyme starting from a
homology search.

.. rubric::  Input
   :name: input-1

+--------------------------+--------------------------+--------------------------+
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div class="center">  |    <div class="center">  |    <div class="center">  |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="floatnone">    |    class="floatnone">    |    class="floatnone">    |
|                          |                          |                          |
| |image10|                | |image11|                | |image12|                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
+--------------------------+--------------------------+--------------------------+
| Basic options dialog.    | Setting up composition.  | Specifying the homology  |
| Note that the space      | It would be possible to  | search file. There are   |
| group is enantiomorphic, | tick the **NCBI Blast**  | many hits, and the GUI   |
| but we pretend we know   | option and run, but we   | by default selects the   |
| the correct space group  | want to avoid            | first three. Selecting   |
| P4₃2₁2 to save time      | overloading the NCBI and | any larger number,       |
|                          | provide a saved homology | however, makes no        |
|                          | search file              | difference!              |
+--------------------------+--------------------------+--------------------------+

Running from the command line
**Input file** (lysozyme.eff)

::

    hklin = lyso.mtz
    labin = "F_New,SIGF_New,DANO_New,SIGDANO_New,ISYM_New"

::

    composition
    {
      component
      {
        sequence = hewl.seq
        homology
        {
          file_name = hewl_ebi_wu_blast.xml
          max_hits = 3
        }
      }
    }

**Command line**

::

    phaser.MRage --verbosity=VERBOSE lysozyme.eff

.. rubric::  Output
   :name: output-1

+--------------------------+--------------------------+--------------------------+
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div class="center">  |    <div class="center">  |    <div class="center">  |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="floatnone">    |    class="floatnone">    |    class="floatnone">    |
|                          |                          |                          |
| |image13|                | |image14|                | |image15|                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
+--------------------------+--------------------------+--------------------------+
| The program fetches the  | ...then processes it     | This model gives a clear |
| first homologue...       | with sculptor. Note that | solution, and processing |
|                          | although all 13          | stops. Although 3 hits   |
|                          | protocols in sculptor    | were specified, the      |
|                          | are active, it only      | second and the third was |
|                          | creates 7 models,        | never downloaded,        |
|                          | because remaining        | because the first gave a |
|                          | protocols create models  | clear solution.          |
|                          | that are identical to    |                          |
|                          | the ones used.           |                          |
+--------------------------+--------------------------+--------------------------+

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |image0| image:: ../../../../images/8/84/Bb-basic-options_thumb.png
   :width: 200px
   :height: 175px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Basic-options.png
.. |image1| image:: ../../../../images/f/f0/Bb-beta-composition_thumb.png
   :width: 200px
   :height: 175px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/bb-beta-composition.png
.. |image2| image:: ../../../../images/b/b6/Bb-beta-ensembles_thumb.png
   :width: 200px
   :height: 175px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/bb-beta-ensembles.png
.. |image3| image:: ../../../../images/7/70/Bb-blip-composition_thumb.png
   :width: 200px
   :height: 175px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/bb-blip-composition.png
.. |image4| image:: ../../../../images/f/f6/Bb-blip-ensembles_thumb.png
   :width: 200px
   :height: 175px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/bb-blip-ensembles.png
.. |image5| image:: ../../../../images/7/7a/Bb-log-first-round-start_thumb.png
   :width: 200px
   :height: 218px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Beta-blip-log-first-round-start.png
.. |image6| image:: ../../../../images/2/20/Bb-log-space-group-evaluation_thumb.png
   :width: 200px
   :height: 218px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Beta-blip-log-space-group-evaluation.png
.. |image7| image:: ../../../../images/9/9c/Bb-log-second-round-start_thumb.png
   :width: 200px
   :height: 193px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Beta-blip-log-second-round-start.png
.. |image8| image:: ../../../../images/1/15/Bb-log-final_thumb.png
   :width: 200px
   :height: 185px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Beta-blip-log-finish.png
.. |image9| image:: ../../../../images/0/00/Bb-solutions_thumb.png
   :width: 200px
   :height: 96px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Beta-blip-solutions.png
.. |image10| image:: ../../../../images/8/8c/Lyso-basic-options_thumb.png
   :width: 200px
   :height: 177px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Lysozyme-basic-options.png
.. |image11| image:: ../../../../images/2/23/Lyso-composition_thumb.png
   :width: 200px
   :height: 177px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Lysozyme-composition.png
.. |image12| image:: ../../../../images/b/b9/Lyso-homology-searches_thumb.png
   :width: 200px
   :height: 177px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Lysozyme-homology-searches.png
.. |image13| image:: ../../../../images/3/38/Lyso-fetch-1lsg_thumb.png
   :width: 200px
   :height: 175px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Lysozyme-fetch-1lsg.png
.. |image14| image:: ../../../../images/6/6a/Lyso-sculptor-1lsg_thumb.png
   :width: 200px
   :height: 264px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Lysozyme-sculptor-1lsg.png
.. |image15| image:: ../../../../images/d/d6/Lyso-finish_thumb.png
   :width: 200px
   :height: 250px
   :target: http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/Lysozyme-finish.png
