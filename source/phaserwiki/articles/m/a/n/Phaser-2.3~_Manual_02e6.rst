.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Phaser-2.3: Manual
   :name: phaser-2.3-manual
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div style="margin-left: 25px; float: right;">

+--------------------------------------------------------------------------+
| .. raw:: html                                                            |
|                                                                          |
|    <div id="toctitle">                                                   |
|                                                                          |
| .. rubric:: Contents                                                     |
|    :name: contents                                                       |
|                                                                          |
| .. raw:: html                                                            |
|                                                                          |
|    </div>                                                                |
|                                                                          |
| -  `1 Keywords <#Keywords>`__                                            |
| -  `2 Python <#Python>`__                                                |
| -  `3 XML <#XML>`__                                                      |
| -  `4 CCP4i <#CCP4i>`__                                                  |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

This is the documentation for Phaser–2.3. There are some changes between
this version and previous versions so input scripts may need editing.

.. rubric:: Keywords
   :name: keywords

Phaser can be run using CCP4-style keyword input

 `Keyword
Modes <http://www.phaser.cimr.cam.ac.uk/index.php?oldid=805>`__
    The different functions that Phaser can perform and the keywords
    relevant for each mode
 `Keywords <http://www.phaser.cimr.cam.ac.uk/index.php?oldid=937>`__
    Detailed descriptions of the keywords
 `Keyword Example
Scripts <http://www.phaser.cimr.cam.ac.uk/index.php?oldid=737>`__
    Copy and edit to start using Phaser from keyword input

.. rubric:: Python
   :name: python

Phaser can be scripted using python.

`Python
Run-Jobs <http://www.phaser.cimr.cam.ac.uk/index.php?oldid=713>`__
    How to run Phaser from python and the functions relevant for each
    Run-Job
`Python
Functions <http://www.phaser.cimr.cam.ac.uk/index.php?oldid=979>`__
    Detailed descriptions of the functions
`Python Example
Scripts <http://www.phaser.cimr.cam.ac.uk/index.php?oldid=601>`__
    Copy and edit to start using Phaser from python

.. rubric:: XML
   :name: xml

| There is XML output available for developers writing automation
  scripts.
| Please contact us for more information or XML output requests.

`XML <http://www.phaser.cimr.cam.ac.uk/index.php?oldid=506>`__
.. rubric:: CCP4i
   :name: ccp4i

Download

#. Molecular Replacement
   `Phaser\_MR\_3.tar.gz <http://www-structmed.cimr.cam.ac.uk/phaser/Phaser_MR_3.tar.gz>`__
#. Experimental Phasing
   `Phaser\_EP\_3.tar.gz <http://www-structmed.cimr.cam.ac.uk/phaser/Phaser_EP_3.tar.gz>`__
#. Normal Mode Analysis
   `Phaser\_NMA\_3.tar.gz <http://www-structmed.cimr.cam.ac.uk/phaser/Phaser_NMA_3.tar.gz>`__

To install new Phaser GUI

#. Make sure you have **write permission** for the directory where ccp4
   is installed
#. Start the CCP4 GUI (by entering "ccp4i" at the command line)
#. Select the "System Administration" pull-down on the right-hand side
   of the CCP4 gui
#. Select "Install Tasks"
#. In the new window, in the "Task archive" box, enter
   <path-to>/Phaser\_MR\_3.tar.gz. Note that the file **must** be called
   Phaser\_MR\_3.tar.gz − **Do not rename it**
#. The details of the Phaser gui installation will appear in the boxes
#. Click "Apply" at the bottom of the window
#. Click "Accept" in the prompt box

To uninstall previous Phaser GUI

#. Make sure you have **write permission** for the directory where ccp4
   is installed
#. Start the CCP4 GUI (by entering "ccp4i" at the command line)
#. Select the "System Administration" pull-down on the right-hand side
   of the CCP4 gui
#. Select "Install Tasks"
#. In the top pulldown, Select **Run the Installation Manager to**
   "uninstall an existing task"
#. In the new Choose Task to Uninstall folder, choose "Phaser". *If
   there is no task called Phaser, you have not installed the ccp4i GUI
   for Phaser manually.*
#. The details of the Phaser gui installation will appear in the boxes
#. Click "Apply" at the bottom of the window
#. Click "Proceed" in the prompt box
#. Exit ccp4i

New GUI will not rerun jobs with old GUI

Keyword input has changed. You cannot (reliably) rerun old phaser jobs
in the new GUI (not backwardly compatible).

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
