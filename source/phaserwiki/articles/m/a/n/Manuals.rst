.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Manuals
   :name: manuals
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

The manuals for Phaser are in two sections

Modes
    The different functions that Phaser can perform (e.g. automated MR,
    automated SAD). The number of modes increases as major new
    functionality is added.
Keywords
    Detailed descriptions of the keywords. The keywords change
    significantly between versions. We attempt to keep backwards
    compatibility in the use and meaning of keywords between versions
    but this is not always possible. The ccp4i interface must be
    compatible with the keywords of the phaser version run. The
    ccp4-style keyword and phenix-style python input functions are
    described on the same page (*n.b.* the manual pages for versions
    prior to Phaser-2.5 describe these on separate pages).

The manual pages for versions prior to Phaser-2.5 also have information
on XML output, which has been deprecated

|Bug.png| See also: `Bugs <../../../../articles/b/u/g/Bugs.html>`__

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   </div>

.. rubric:: Currently Supported Releases
   :name: currently-supported-releases

-  **Phaser-2.7.14** →
   `Manual <../../../../articles/m/a/n/Phaser-2.7.14:_Manual.html>`__

   -  `Phaser-2.7 series
      changelog <../../../../articles/p/h/a/Phaser-2.7_series_changelog.html>`__

-  **Phaser-2.6.0** →
   `Manual <../../../../articles/m/a/n/Phaser-2.6.0:_Manual.html>`__

.. rubric:: Obsolete
   :name: obsolete

-  **Phaser-2.5.6** →
   `Manual <../../../../articles/m/a/n/Phaser-2.5.6:_Manual.html>`__
-  **Phaser-2.5.5** →
   `Manual <../../../../articles/m/a/n/Phaser-2.5.5:_Manual.html>`__
-  **Phaser-2.5.4** →
   `Manual <../../../../articles/m/a/n/Phaser-2.5.4:_Manual.html>`__
-  **Phaser-2.5.3** →
   `Manual <../../../../articles/m/a/n/Phaser-2.5.3:_Manual.html>`__
-  **Phaser-2.5.2** →
   `Manual <../../../../articles/m/a/n/Phaser-2.5.2:_Manual.html>`__
-  **Phaser-2.5.1** →
   `Manual <../../../../articles/m/a/n/Phaser-2.5.1:_Manual.html>`__
-  **Phaser-2.5** →
   `Manual <../../../../articles/m/a/n/Phaser-2.5:_Manual.html>`__
-  **Phaser-2.4** →
   `Manual <../../../../articles/m/a/n/Phaser-2.4:_Manual.html>`__
-  **Phaser-2.3** →
   `Manual <../../../../articles/m/a/n/Phaser-2.3:_Manual.html>`__
-  **Phaser-2.2** →
   `Manual <../../../../articles/m/a/n/Phaser-2.2:_Manual.html>`__

.. rubric:: Ancient
   :name: ancient

Versions older than Phaser-2.1 are located at the `**obsolete Phaser
website** <http://www-structmed.cimr.cam.ac.uk/phaser/>`__

-  **Phaser-2.1** →
   `Manual <http://www-structmed.cimr.cam.ac.uk/phaser_obsolete/documentation/phaser-2.1.html>`__
-  **Phaser-2.0** →
   `Manual <http://www-structmed.cimr.cam.ac.uk/phaser_obsolete/documentation/phaser-2.0.html>`__
-  **Phaser-1.3** →
   `Manual <http://www-structmed.cimr.cam.ac.uk/phaser_obsolete/documentation/phaser-1.3.html>`__
-  **Phaser-1.4** →
   `Manual <http://www-structmed.cimr.cam.ac.uk/phaser_obsolete/documentation/phaser-1.2.html>`__

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Manuals&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Manuals>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 12:21, 13 September 2016 by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__. Based on work
   by `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Bug.png| image:: ../../../../images/7/2F/7/7/7d/Bug.png
   :width: 48px
   :height: 48px
.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
