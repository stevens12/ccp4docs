.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Using Electron Density as a Model
   :name: using-electron-density-as-a-model
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

When Phaser uses a PDB file as a model for molecular replacement, it
computes electron density in a large cell from which a finely-sampled
molecular transform is calculated. This finely-sampled molecular
transform can be interpolated to get structure factors for the model in
any arbitrary orientation. Similarly, Phaser can use structure factors
representing electron density. Because of the need for a finely-sampled
molecular transform, the density must be placed in a large cell prior to
structure factor calculation.

The electron density can be obtained from a crystallographic experiment,
or from electron microscopy. Here we give instructions for density
coming from a crystallographic experiment. Note that, even though it
should be possible to do much of this through the ccp4i interface using
the "Map Cutting" task, there is a (documented) bug in the program
maprot, which leads to incorrect results when the extent of the mask in
any direction is greater than the corresponding cell edge. The procedure
below uses a workaround suggested by Kevin Cowtan.

NB: If you are using unpatched versions of CCP4 5.0 through 5.0.2, this
procedure may fail. You can find patches for bugs in the programs
`maprot <http://www.ccp4.ac.uk/problems.php#5.0.2-maprot>`__ and
`mapmask <http://www.ccp4.ac.uk/problems.php#5.0.2-mapmask>`__ on the
Problems pages of the CCP4 website, courtesy of Martyn Winn.

The mask in the following procedure defines two things: the volume
containing the density to be used as a model, and the unit cell into
which that density is to be placed for calculation of a finely-sampled
molecular transform. The cell should extend at least 2.5 times the
extent of the mask in each direction.

If you have non-crystallographic symmetry, you may be able to define a
mask using a local correlation map, for instance in the CCP4 program
maprot. Then it will be necessary to change the cell to the desired one
for the molecular transform, using the programs mapmask (change the mask
to a map and eventually back again) and maprot (change the cell of the
intermediate map).

Often, as in the example below, the mask can be defined as the union of
a set of spheres around positions specified in a PDB file, either a
small number of large spheres, or a large number of small spheres
centred on skeleton atoms. The first step in the procedure sets the
space group in the input PDB file to P1 and the cell to the desired
size. To get the cell dimensions, you can take the limits of xyz
coordinates for atoms in the PDB file, add twice the sphere radius to
get the extents for the mask, then multiply by 2.5. You will need to
remember the mask extents and the coordinates of the centre of the mask,
to be given as input to Phaser when you use the MTZ file.

::

       pdbset XYZIN mask.pdb XYZOUT $CCP4_SCR/mask_targetcell.pdb << end-pdbset > cutout.log
       SPACEGROUP 1
       CELL 108.0 140.0 108.0 90. 90. 90.
       END
       end-pdbset

Now make a mask from the PDB file, which will have the cell and symmetry
that was just set. Choose a grid suitable for eventual
back-transformation of the density, at least as fine as dmin/3. To make
sfall happy, the number of grid points should be divisible by 2 and
should not contain prime factors greater than 19. Set the radius for the
spheres you are using around the atomic positions. Note that there is
also a program called ncsmask in the XtalView suite. By referring to
"$CBIN/ncsmask" you can avoid any possible name conflicts, but if you
are running this from the ccp4i interface you will either want to make
sure that CCP4 programs come before XtalView programs in your path, or
configure the ccp4i interface to give the full path for ncsmask. (How to
do this is discussed in the
`FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__, in the context of
how to make sure ccp4i finds the right version of Phaser.)

::

       $CBIN/ncsmask XYZIN $CCP4_SCR/mask_targetcell.pdb MSKOUT $CCP4_SCR/mask_targetcell.msk << end-ncsmask >> cutout.log
       GRID 120 150 120
       RADIUS 4.0
       PEAK 1
       end-ncsmask
       /bin/rm $CCP4_SCR/mask_targetcell.pdb

Take an input map containing your desired density, covering at least the
asymmetric unit, and extend to cover exactly one unit cell.

::

       mapmask MAPIN input.map MAPOUT $CCP4_SCR/onecell.map << end-mapmask >> cutout.log
       XYZLIM CELL
       end-mapmask

Cut out the density from the original cell, put it into a box matching
the mask and its cell.

::

       maprot MAPIN $CCP4_SCR/onecell.map MSKIN $CCP4_SCR/mask_targetcell.msk \
           WRKOUT $CCP4_SCR/masked_targetcell.map << end-maprot >> cutout.log
       MODE FROM
       SYMMETRY WORK 1
       AVERAGE
       ROTATE EULER 0 0 0
       TRANSLATE 0 0 0
       end-maprot
       /bin/rm $CCP4_SCR/onecell.map $CCP4_SCR/mask_targetcell.msk

Shuffle the masked density into a map covering the full unit cell.

::

       mapmask MAPIN $CCP4_SCR/masked_targetcell.map \
           MAPOUT $CCP4_SCR/masked_fullcell.map << end-mapmask2 >> cutout.log
       XYZLIM CELL
       PAD 0.0
       end-mapmask2
       /bin/rm $CCP4_SCR/masked_targetcell.map

Finally, back-transform the map to get finely-sampled molecular
transform structure factors. Set the resolution equal to that used in
computing the original input map.

::

       sfall HKLOUT masked_density.mtz MAPIN $CCP4_SCR/masked_fullcell.map << end-sfall >> cutout.log
       LABOUT FC=FC_mask PHIC=PHIC_mask
       MODE SFCALC MAPIN
       RESOLUTION 2.8
       END
       end-sfall
       /bin/rm $CCP4_SCR/masked_fullcell.map

That's it! Now you can use the MTZ file as HKLIn in the ENSEMBLE command
of Phaser. Choose a value of RMS to give a falloff in the Sigma(A)
values similar to the falloff of the figures of merit (see the section
of the documentation describing the formula for Sigma(A)).
Alternatively, give a value of 1Å for a map of reasonable quality, or
1.5Å for a poorer map. It is essential to give the correct coordinates
for the centre of the mask used to cut out density, in order for the
molecular transform interpolation to work correctly.

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
