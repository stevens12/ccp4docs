.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Famos
   :name: famos
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   </div>

**phaser.famos** (**phaser.find\_alt\_orig\_sym\_mate**) is a script for
determining the best common origin for different molecular replacement
solutions, in real space. The coordinates do not need to be identical.
The common origin is found via secondary structure matching.

.. rubric:: Author
   :name: author

Robert D. Oeffner

.. rubric:: Purpose
   :name: purpose

**phaser.famos** attempts to find the best superposition of two
different molecular replacement solutions of the same dataset termed
moving.pdb and fixed.pdb with respect to all symmetry operations and
alternate origin shifts permitted by the spacegroup of the crystal. If
either of the pdb files contain more chains each chain will be tested
for their best match against chains in the other pdb file. It does so by
calculating a score value, MLAD (see Algorithm), for all possible
symmetry operations and alternate origin shifts of each chain in
moving.pdb compared with chains in fixed.pdb. The transformation with
the smallest MLAD is retained for that particular pair of chains.

The script returns the best matches between pairs of chains in the two
files. This includes the MLAD values, chain IDs, symmetry
transformations and alternate origins for the two structures. If the
same origin shift is not applied to all pairs of chains a warning will
be printed. If the space group of the crystal has a floating origin this
generally results in an origin offset between chains that is not a
rational number.

The superposed structure and the fixed structure can be visually
inspected in a molecular viewer such as Coot or Pymol. Matches with low
MLAD scores will have correspondingly good superpositions.

.. rubric:: Usage from the command line
   :name: usage-from-the-command-line

**phaser.famos** uses `Phenix PHIL
input <https://www.phenix-online.org/documentation/file_formats.html#phil-files-eff-def-phil>`__
in either a text file or as keywords like:

::

     phaser.famos moving.pdb=pdbfile1 fixed.pdb=pdbfile2

or:

::

     phaser.famos my_phil_input.txt.

The PHIL input scopes, moving and fixed, specifies the MR solutions.
Both scopes are programmatically equivalent and must be non-empty. This
means that a scope should either specify the parameter xyzfname or the
sub-scope mrsolution or the sub-scope pickle.solution.

Examples:

-  Use the pdb file from one of the MR solutions in a scope. This is
   useful for simple cases such as when the solution comes from the
   PHENIX MRage GUI or when it is obtained from another MR program than
   Phaser.
-  Specify a Phaser MR solution by assigning the .sol file name of the
   solution to the mrsolution.solfname parameter of the sub-scope as
   well as assigning the IDs of the ensembles and the corresponding MR
   models to the parameters mrsolution.ensembles.name and
   mrsolution.ensembles.xyzfname. Multiple search components are
   specified as multiple ensembles. This way of specifying solutions is
   useful for solution files produced when Phaser is run from the
   command line and it produces several solutions of which one is the
   correct solution.
-  Use a solution from the Phaser-MR GUI in PHENIX. In that case the
   parameter pickle.solution.pklfname in the sub-scope should be
   assigned to the solution file that is produced by PHENIX after an MR
   calculation. The pickle.solution.philfname should then be assigned to
   the input file for the MR calculation. This way of specifying
   solutions is useful when MR solutions are available from having run
   Phaser through the PHENIX interface.
-  If space group and the unit cell dimensions is not available from any
   of the input files then these need to be specified by assigning the
   parameter, spacegroupfname, either to a PDB file with a CRYST1 record
   or to an MTZ file with that information. This should be the data file
   used for the molecular replacement calculation.

| 

.. rubric:: Examples of PHIL input
   :name: examples-of-phil-input

Unless the input just constitutes of two PDB files a PHIL file is the
easiest way to enter input. A few examples of PHIL for **phaser.famos**
are given below.

Testing a command-line Phaser MR solution file against a solution
specified as a PDB file:

::

     AltOrigSymMates.fixed.mrsolution
     {
       solfname = "testdata/MR_3ECI_A0_2P82_A0.sol"
       ensembles
       {
         name = "MR_2P82_A0"
         xyzfname = "testdata/sculpt_2P82_A0.pdb"
       }
       ensembles
       {
         name = "MR_3ECI_A0"
         xyzfname = "testdata/sculpt_3ECI_A0.pdb"
       }
     }
     
     AltOrigSymMates.moving_pdb="testdata/2z0d.pdb"

Testing two set of solution files from the PHENIX Phaser-MR GUI against
one another:

::

     AltOrigSymMates.fixed.pickle_solution
     {
       philfname = "testdata/phaser_mr_13.eff"
       pklfname = "testdata/phaser_mr_13.pkl"
     }
     AltOrigSymMates.moving.pickle_solution
     {
       philfname = "testdata/phaser_mr_11.eff"
       pklfname = "testdata/phaser_mr_11.pkl"
     }

Testing two solution files from the command-line version of Phaser
against one another:

::

     AltOrigSymMates.fixed.mrsolution
     {
       solfname = "testdata/MR_3ECI_A0_2P82_A0.sol"
       ensembles
       {
         name = "MR_2P82_A0"
         xyzfname = "testdata/sculpt_2P82_A0.pdb"
       }
       ensembles
       {
         name = "MR_3ECI_A0"
         xyzfname = "testdata/sculpt_3ECI_A0.pdb"
       }
     }
     AltOrigSymMates.moving.mrsolution
     {
       solfname = "testdata/MR_2ZPN_A0_2P82_A0.sol"
       ensembles
       {
         name = "MR_2P82_A0"
         xyzfname = "testdata/sculpt_2P82_A0.pdb"
       }
       ensembles
       {
         name = "MR_2ZPN_A0"
         xyzfname = "testdata/sculpt_2ZPN_A0.pdb"
       }
     }
     AltOrigSymMates.spacegroupfname = "testdata/2Z0D.mtz"

For more information on the PHIL input see the bottom of this page.

.. rubric:: Also move HETATM (hetero atoms)
   :name: also-move-hetatm-hetero-atoms

Invoking this flag will move hetero atoms (ligands, waters, metals,
etc.) in conjunction with their associated peptide chain. The program
first invokes phenix.sort\_hetatms as to associate hetero atoms sensibly
with adjacent peptide chains.

After having identified the transformation for a chain yielding the
smallest MLAD score it then subjects the associated hetero atoms to the
same transformation.

.. rubric:: Debug mode
   :name: debug-mode

Invoking the debug flag produces individual pdb files of the C-alpha
atoms used for each SSM alignment of the moving scope for each permitted
symmetry operation and alternative origin. A gold atom is placed at the
centroid of the C-alpha atoms. Similar files are produced for the fixed
scope. These files are stored in a subfolder named
"AltOrigSymMatesFiles".

If a floating origin is present in the space group a table of MLAD
values is produced by sliding a copy of the chains in the moving scope
along the polar axis in the fractional interval [-0.5, 0.5] for each
permitted symmetry operation and alternative origin.

.. rubric:: List of all available keywords
   :name: list-of-all-available-keywords

See `Phenix documentation for
phenix.find\_alt\_orig\_sym\_mate <https://www.phenix-online.org/documentation/reference/find_alt_orig_sym_mate.html#list-of-all-available-keywords>`__

.. rubric:: Output
   :name: output

The closest match between chains in the moving section to chains in the
fixed section is saved with the name of the pdb file in the fixed scope,
concatenated with the name of the pdb file in the moving scope but
prepended with "MinMLAD\_". A log file with the name of the pdb file in
the fixed scope, concatenated with the name of the pdb file in the
moving scope but prepended with "AltOrigSymMLAD\_" is written containing
standard output. All files are saved in the current working directory.
If MR solutions specified in the PHIL contains multiple solutions then
**phaser.famos** will output mulitple log files corresponding to each MR
solution.

.. rubric:: Do the MR solutions match?
   :name: do-the-mr-solutions-match

A good match between two chains usually have a MLAD value below 1.5
whereas a bad match usually have a value above 2.0. This is a rule of
thumb and exceptions do occur. It is advisable to visually inspect that
the structures superpose one another in a molecular graphics viewing
program.

.. rubric:: Algorithm
   :name: algorithm

**phaser.famos** computes configurations by looping over all symmetry
operations and alternative origin shifts. An alignment between C-alpha
atoms from moving.pdb and fixed.pdb is computed using secondary
structure matching (SSM). If that fails or if the MLAD score achieved
with SSM is larger than 2.0 an alignment is computed using MMTBX
alignment functions which is part of the CCTBX. To estimate the best
match a distance measure between the aligned C-alpha atoms is computed
for each configuration. The mean log absolute deviation (MLAD) is
defined as:

MLAD(dR) = Σ( log(dr·dr/(\|dr\| + 0.9) + max(0.9, min(dr·dr,1))) -
log(0.9)),

where dr is the difference vector between a pair of aligned C-alpha
atoms and the sum is taken over all atom pairs in the alignment. The
factor log(0.9) is subtracted to ensure that MLAD(0) = 0.0, i.e. that
two identical structures produces the value zero.

MLAD can loosely be interpreted as a distance measure between
structures. But it is not a metric in a strict mathematical sense since
the triangle inequality is not fulfilled. Unlike a plain root mean
square deviation the logarithm in the MLAD formula will downplay
contributions of atom pairs where the atoms are spatially distant. If an
RMSD were employed such atom pairs would contribute on an equal footing
with those groups of atom pairs that can be superposed perfectly. This
in turn may lead to non-optimal superpositions when the structures
tested against one another consists of multiple domains where one domain
has undergone a domain motion, i.e. where a subset of atoms in one chain
are bound to be spatially distant from the atoms in the other chain they
have been paired with.

**phaser.famos** will for each chain in the fixed scope find the
smallest MLAD with a copy of each chain in the moving scope for a given
symmetry operation and alternative origin. When all chains in the fixed
scope have been tested these copies will be saved to a file. For
spacegroups with floating origin the minimum MLAD is found by doing a
Golden Sectioning minimization along the polar axis for each copy of
chains in moving.pdb.

.. rubric:: Caveats
   :name: caveats

The program tests all solutions present in solution files entered as
fixed scope against all solutions present in solution files entered as
moving scope. Consequently the execution time is proportional to the
number of solutions in fixed scope times the number of solutions in
moving scope.

The execution time is proportional to the number of SSM alignments being
tested; if SSM identifies 12 alignments the program will take 12 times
as long.

.. rubric:: Changes
   :name: changes

The command-line syntax mentioned in the Computational Crystallography
Newsletter 2012 January has been replaced by PHIL syntax.

.. rubric:: Literature
   :name: literature

Algorithms for deriving crystallographic space-group information R.W.
Große-Kunstleve Acta Cryst. A55, 383-395 (1999)

phenix.find\_alt\_orig\_sym\_mate Robert D. Oeffner, Gábor Bunkóczi and
Randy J. Read Computational Crystallography Newsletter 2012 January,
5-10 (2012)

Secondary-structure matching (SSM), a new tool for fast protein
structure alignment in three dimensions. E. Krissinel and K. Henrick
Acta Cryst. D60, 2256-2268 (2004)

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/f/a/m/Famos.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Famos&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Famos>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 17:45, 8 September 2016 by `Rob
   Oeffner <http://localhost/index.php?title=User:Rdo20&action=edit&redlink=1>`__.
   Based on work by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
