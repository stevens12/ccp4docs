.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Bugs
   :name: bugs
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   <div id="toc" class="toc">

.. raw:: html

   <div id="toctitle">

.. rubric:: Contents
   :name: contents

.. raw:: html

   </div>

-  `1 CCP4-6.3 <#CCP4-6.3>`__
-  `2 CCP4-6.2 <#CCP4-6.2>`__
-  `3 CCP4-6.1.13 Windows <#CCP4-6.1.13_Windows>`__
-  `4 Phaser-1.3.1 <#Phaser-1.3.1>`__
-  `5 Phaser-1.2 <#Phaser-1.2>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

We apologise for the bugs.

If you come across a bug, check the known bug list below and if the
problem is new `**please report it to
us** <../../../../articles/c/o/n/Contact.html>`__.

.. rubric:: |Bug.png|\ CCP4-6.3
   :name: bug.pngccp4-6.3

-  If you choose the SGALTERNATIVE LIST command but provide only one
   SGALTERNATIVE TEST space group, this test space group

is ignored and only the space group given in the MTZ header is tested.
To work around this problem, it is best to correct the space group in
the MTZ header (since you obviously know the space group already).
Alternatively, you can provide more than one SGALTERNATIVE LIST command
to test more than one space group.

.. rubric:: |Bug.png|\ CCP4-6.2
   :name: bug.pngccp4-6.2

Released 32 bit binary occasionally aborts on 64 bit Linux

-  *A fixed 32 bit binary is available from the ftp site and replaces
   the distributed binary in automated downloads from 19th October 2011
   onwards.* → `CCP4-6.2.0 program
   bug <http://www.ccp4.ac.uk/problems.php#6.2.0-phaser>`__.

There is a bug in the CCP4 interface for phaser. It is not possible to
change the packing criteria. Please download this file and substitute
for your $CCP4I\_TOP/tasks/phaser\_MR.tcl file.

-  Download CCP4 file for Phaser\_MR
   `phaser\_MR.tcl <http://www-structmed.cimr.cam.ac.uk/phaser/Phaser_MR/tasks/phaser_MR.tcl>`__

.. rubric:: |Bug.png|\ CCP4-6.1.13 Windows
   :name: bug.pngccp4-6.1.13-windows

For a short period of time during the release of CCP4-6.1.13 (sometime
in 2010, although we are not sure exactly when), phaser-2.2.1 was being
released with the gui for phaser-2.1.4. A fix is available. Please email
cimr-phaser@lists.cam.ac.uk if you are affected by this bug.

.. rubric:: |Bug.png|\ Phaser-1.3.1
   :name: bug.pngphaser-1.3.1

-  **Memory usage can go out of control under OSF1 (Tru64 Unix)**
   This problem is specific to OSF1, and we believe it arises from a bug
   in the handling of the standard map library on that platform.

-  **Core dump if maximum score is negative (specific to IRIX
   executable)**
   Fixed for upcoming patch release. Work around by (e.g.) increasing
   RMS to avoid negative scores, or by running under a different
   operating system..

-  **SGALT TEST doesn't work if only one test space group specified**
   Fixed for upcoming patch release. Work around by using SPACEGROUP
   command instead..

-  **Using DQ option of NMAPDB command leads to infinite loop**
   Fixed for upcoming patch release. Work around by using MAXRMS option
   instead.

-  **If COMPosition is specified by SEQuence, the last line of the
   sequence file is used twice**
   Fixed for upcoming patch release. Work around by adding comment line
   (line starting with ">") at end of sequence file.

-  **COMPosition ENSEmble FRACtional keyword doesn't work**
   Work around by using the other methods to specify the composition of
   the asymmetric unit.

-  **ROTATE AROUND option of MR\_BRF has disappeared from the ccp4i
   GUI**
   Fixed for upcoming patch release. Work around by entering correct
   command using the "Run&View Com File" option of ccp4i.

.. rubric:: |Bug.png|\ Phaser-1.2
   :name: bug.pngphaser-1.2

-  **.../phaser/src/Ensemble.cc(663): PROGRAM\_ASSERT(storedV ==
   mapV[b]) failure.**
   This is a numerical instability that has been fixed for the upcoming
   1.3 release. If you have the installer release, you can replace line
   663 of Ensemble.cc with
   PROGRAM\_ASSERT(storedV < mapV[b]+tol && storedV > mapV[b]-tol);
   then recompile.
   Otherwise you can try changing the resolution ever so slightly (e.g.
   from 2.5Å to 2.51Å) as this might prevent the bug from showing up.

-  **Molecular replacement using electron density as a model does not
   work.**
   Bugs were introduced, breaking this feature prior to the release of
   version 1.2. This has now been fixed for the upcoming release. If you
   need to use this feature now, please contact us to obtain a
   pre-release executable.

-  **Default fast rotation search returns no orientations.**
   The default search should return at least one orientation above 75%
   of the maximum. If no orientations are returned, you have encountered
   a numerical stability problem with the fast rotation function that
   shows up on some architectures (e.g. MacOSX, Irix) when the molecule
   is large compared to the resolution of the data. This has been fixed
   for the upcoming 1.3 release. In the meantime, you can work around
   this problem by: 1) running under Linux, where the problem does not
   show up; 2) reducing the resolution, as the default is usually higher
   than needed for large molecules; 3) running with VERBOSE ON to find
   out how large the sphere is for the Patterson decomposition for the
   fast rotation, then running again with the CLMN SPHERE set to a
   smaller number, say 10-20% smaller than the default.

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/b/u/g/Bugs.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Bugs&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Bugs>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 17:13, 15 August 2012 by `Randy
   Read <../../../../articles/r/a/n/User:Randy.html>`__. Based on work
   by `Airlie McCoy <../../../../articles/a/i/r/User:Airlie.html>`__ and
   `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Bug.png| image:: ../../../../images/7/2F/7/7/7d/Bug.png
   :width: 48px
   :height: 48px
.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
