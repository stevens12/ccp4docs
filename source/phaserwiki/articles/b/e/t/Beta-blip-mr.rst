.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Beta-blip-mr
   :name: beta-blip-mr
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

β-Lactamase (BETA, 29kDa) is an enzyme produced by various bacteria, and
is of interest because it is responsible for penicillin resistance,
cleaving penicillin at the β-lactam ring. There are many small molecule
inhibitors of BETA in clinical use, but bacteria can become resistant to
these as well. Streptomyces clavuligerus produces beta-lactamase
inhibitory protein (BLIP, 17.5kDa), which has been investigated as an
alternative to small molecule inhibitors, as it appears more difficult
for bacteria to become resistant to this form of BETA inhibition. The
structures of BETA and BLIP were originally solved separately by
experimental phasing methods. The crystal structure of the complex
between BETA and BLIP has been a test case for molecular replacement
because of the difficulty encountered in the original structure
solution. BETA, which models 62% of the unit cell, is trivial to locate,
but BLIP is more difficult to find. The BLIP component was originally
found by testing a large number of potential orientations with a
translation function search, until one solution stood out from the
noise.

.. rubric:: Tutorial 1
   :name: tutorial-1

Search for blip alone.

(Right click to control movie)

type="html5" style="width:900px;"
|Video is not supported|
.. rubric:: Tutorial 2
   :name: tutorial-2

Search for beta and blip together.

(Right click to control movie)

type="html5" style="width:900px;"
|Video is not supported|

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-site" class="portlet">

.. rubric:: site
   :name: site

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Home">

   .. raw:: html

      </div>

   `Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-News">

   .. raw:: html

      </div>

   `News <../../../../articles/n/e/w/News.html>`__
-  

   .. raw:: html

      <div id="n-Events">

   .. raw:: html

      </div>

   `Events <../../../../articles/e/v/e/Events.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-documentation" class="portlet">

.. rubric:: documentation
   :name: documentation

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__
-  

   .. raw:: html

      <div id="n-Keyword-Examples">

   .. raw:: html

      </div>

   `Keyword
   Examples <../../../../articles/m/r/_/MR_using_keyword_input_9c88.html>`__
-  

   .. raw:: html

      <div id="n-Python-Examples">

   .. raw:: html

      </div>

   `Python
   Examples <../../../../articles/p/y/t/Python_Example_Scripts_7723.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Bugs">

   .. raw:: html

      </div>

   `Bugs <../../../../articles/b/u/g/Bugs.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-View-SVN">

   .. raw:: html

      </div>

   `View SVN <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-other" class="portlet">

.. rubric:: other
   :name: other

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Contact">

   .. raw:: html

      </div>

   `Contact <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developers">

   .. raw:: html

      </div>

   `Developers <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__
-  

   .. raw:: html

      <div id="n-Help">

   .. raw:: html

      </div>

   `Help <../../../../articles/c/o/n/Help%7EContents_22de.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Video is not supported| image:: images/Phaserwiki_logo.png

