.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: External Links
   :name: external-links
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   </div>

Documentation for Phaser is also found on external websites

`CCP4 <http://www.ccp4.ac.uk>`__
    Please use the search box to find Phaser-related talks and
    documentation

`CCP4 Developers' Wiki <http://ccp4wiki.org/~ccp4wiki/wiki>`__
    Please use the search box to find Phaser-related talks and
    documentation
    *Examples include*

    -  `Molecular replacement with Phaser using
       ccp4i <http://ccp4wiki.org/~ccp4wiki/wiki/index.php?title=Molecular_replacement_with_Phaser>`__
    -  `Experimental phasing with Phaser using
       ccp4i <http://ccp4wiki.org/~ccp4wiki/wiki/index.php?title=Experimental_phasing_with_Phaser>`__

`CCP4 Community
Wiki <http://strucbio.biologie.uni-konstanz.de/ccp4wiki/index.php/Main_Page>`__
    Please use the search box to find Phaser-related talks and
    documentation
    *Examples include*

    -  `How to compile Phaser to run on multiple
       CPUs <http://strucbio.biologie.uni-konstanz.de/ccp4wiki/index.php/How_to_compile_PHASER_to_run_on_multiple_CPUs_%28Linux%29>`__
    -  `Phenix Page on CCP4 Community
       Wiki <http://strucbio.biologie.uni-konstanz.de/ccp4wiki/index.php/Phenix>`__

`Phenix <http://www.phenix-online.org>`__
    Please use the search box to find Phaser-related talks and
    documentation
    *Examples include*

    -  `Overview of MR in
       phenix <http://www.phenix-online.org/documentation/reference/mr_overview.html>`__
    -  `Molecular replacement and autobuilding using Phaser, Rosetta,
       and autobuild with
       mr\_rosetta <http://www.phenix-online.org/documentation/reference/mr_rosetta.html>`__
    -  `Experimental
       Phasing <http://www.phenix-online.org/documentation/overviews/xray-experimental-phasing.html>`__
    -  `SAD and MR-SAD phasing with the Phaser
       GUI <http://www.phenix-online.org/documentation/reference/phaser_ep.html>`__
    -  `Experimental phasing in the AutoSol
       GUI <http://www.phenix-online.org/documentation/reference/autosol_gui.html>`__

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
