.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Experimental Phasing
   :name: experimental-phasing
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div style="margin-left: 25px; float: right;">

+--------------------------------------------------------------------------+
| .. raw:: html                                                            |
|                                                                          |
|    <div id="toctitle">                                                   |
|                                                                          |
| .. rubric:: Contents                                                     |
|    :name: contents                                                       |
|                                                                          |
| .. raw:: html                                                            |
|                                                                          |
|    </div>                                                                |
|                                                                          |
| -  `1 Automated Experimental                                             |
|    Phasing <#Automated_Experimental_Phasing>`__                          |
| -  `2 How to Define Data <#How_to_Define_Data>`__                        |
| -  `3 How to Define Atoms <#How_to_Define_Atoms>`__                      |
|                                                                          |
|    -  `3.1 Cluster Compounds <#Cluster_Compounds>`__                     |
|                                                                          |
| -  `4 How to Control Output <#How_to_Control_Output>`__                  |
| -  `5 Basic Modes for Experimental                                       |
|    Phasing <#Basic_Modes_for_Experimental_Phasing>`__                    |
|                                                                          |
|    -  `5.1 SAD Phasing <#SAD_Phasing>`__                                 |
|    -  `5.2 Combined MR and SAD Phasing <#Combined_MR_and_SAD_Phasing>`__ |
|                                                                          |
| -  `6 Density Modification after                                         |
|    Phaser <#Density_Modification_after_Phaser>`__                        |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

Phaser performs SAD phasing in two modes. In the Automated Experimental
Phasing mode, Phaser corrects for anisotropy, puts the data on absolute
scale, does a cell content analysis, refines heavy atom sites to
optimize phasing, and completes the model from log-likelihood gradient
maps. Alternatively, the SAD Phasing mode can be used, which only
refines heavy atom sites to optimize phasing, and completes the model
from log-likelihood gradient maps. For this mode, the data should be
pre-corrected for anisotropy and put on an absolute scale. This mode
should only be used as part of automation pipelines, where the correct
preparation of the data can be guaranteed and it saves cpu time

.. rubric:: Automated Experimental Phasing
   :name: automated-experimental-phasing

MODE EP\_AUTO combines the anisotropy correction, cell content analysis,
and SAD Phasing modes to automatically solve a structure by experimental
phasing. The final solution is output to the files FILEROOT.sol,
FILEROOT.mtz and FILEROOT.pdb. Many structures can be solved by running
an automated experimental phasing job with defaults.

.. rubric:: How to Define Data
   :name: how-to-define-data

You need to tell Phaser the name of the mtz file containing your data
and the columns in the mtz file to be used. For SAD phasing, a single
CRYSTAL and DATASET with anomalous data (F(+), SIGF(+), F(-) and
SIGF(-)) must be given. The columns must have the correct CCP4 column
type: 'G' for F(+) and F(-) and 'L' for SIGF(+) and SIGF(-). If the
columns on your mtz file have somehow acquired the incorrect column
type, you should change the column type with an mtz editing programme
(e.g. sftools).

::

       CRYSTAL insulin DATASET sad LABIN F+ = F(+) SIG+ = SIGF(+) F- = F(-) SIG- = SIGF(-)

.. rubric:: How to Define Atoms
   :name: how-to-define-atoms

Atom sites are defined with the ATOM keyword. Atoms sites may be entered
one at a time specifying fractional or orthogonal coordinates, occupancy
and B-factor, or from a PDB file, or from a mlphare-style HA file. The
crystal to which the atoms correspond must be specified in the input.

A partial structure, for example a partial and poor MR solution (which
may or may not contain anomalous scatterers) can be used to start the
phasing, either alone or in addition to some anomalous scattering sites
entered with the ATOM keyword. The partial structure is entered with the
PART keyword. If you do not have a partial MR structure, anomalous
scatterers must be found using Patterson methods, Direct methods or Dual
Space methods (e.g. phenix.hyss, SHELXD, SnB, Rantan) prior to running
Phaser. Note that SHELXD outputs a pdb file with the scatterers labelled
sulphur (S) regardless of the correct scattering type: you need to
change the pdb so that it contains the correct scattering type prior to
using it in Phaser.

.. rubric:: Cluster Compounds
   :name: cluster-compounds

Cluster compounds can be used as the scattering type. The coordinates
for Ta₆Br₁₂ are stored internally and have the cluster name TX.
Coordinates of other cluster compounds must be supplied (coordinates are
translated to the origin internally before spherical averaging of the
structure factors) and are given the cluster name XX.

::

       CLUSTER PDB <PDBFILE>

.. rubric:: How to Control Output
   :name: how-to-control-output

The output of Phaser can be controlled with the following keywords:

.. rubric:: Basic Modes for Experimental Phasing
   :name: basic-modes-for-experimental-phasing

.. rubric:: SAD Phasing
   :name: sad-phasing

MODE EP\_SAD phases SAD data and completes the structure from
log-likelihood gradient maps. The final solution is output to the files
FILEROOT.sol, FILEROOT.mtz and FILEROOT.pdb .

Do SAD phasing of insulin. This is the minimum input, using all defaults
(except the ROOT filename)

::

      #insulin_sad.com
      phaser << eof
      MODE EP_SAD
      TITLe sad phasing insulin with intrinsic sulphurs
      HKLIn S-insulin.mtz
      CRYStal insulin DATAset sad LABIn F+=F(+) SIG+=SIGF(+) F-=F(-) SIG-=SIGF(-)
      WAVElength 1.5418
      LLGComplete COMPLETE ON SCATterer S
      ATOM CRYStal insulin PDB S-insulin_hyss.pdb
      ROOT insulin_sad
      eof

.. rubric:: Combined MR and SAD Phasing
   :name: combined-mr-and-sad-phasing

MODE EP\_SAD when used with the PART keyword phases SAD data and
completes the structure from log-likelihood gradient maps using a
partial structure as (at least part of) the initial atomic substructure.
Only the real (i.e. non-anomalous) signal from the partial structure is
used. The final solution is output to the files FILEROOT.sol,
FILEROOT.mtz and FILEROOT.pdb. Note that, because the substructure after
log-likelihood-gradient completion includes the partial model, the
phases automatically combine information from the partial model and the
anomalous scatterers.

Do SAD phasing of insulin starting from partial model of one helix only.
This is the minimum input, using all defaults (except the ROOT filename)

::

      #insulin_mr_sad.com
      phaser << eof
      MODE EP_SAD
      TITLe sad phasing insulin with intrinsic sulphurs, starting from a helix
      HKLIn S-insulin.mtz
      CRYStal insulin DATAset sad LABIn F+=F(+) SIG+=SIGF(+) F-=F(-) SIG-=SIGF(-)
      WAVElength 1.5418
      LLGComplete COMPlete ON SCATterer S
      PART PDB helix.pdb ID 100
      ROOT insulin_mr_sad
      eof

.. rubric:: Density Modification after Phaser
   :name: density-modification-after-phaser

Phaser produces map coefficients that reduce the model bias coming from
the real contribution of the anomalous scatterers, and there is a small
but significant improvement by starting density modification from these
FWT/PHWT coefficients rather than FP, PHIB and FOM.

The FWT/PHWT coefficients should be used when running Resolve. If you
run Phaser then Resolve from the AutoSol wizard of phenix, FWT/PHWT are
picked up automatically.

If you're using DM from ccp4i, you have to provide PHIB, FOM and the HL
coefficients in the interface, then use the "Run&View Com File" option
to add "FDM=FWT PHIDM=PHWT" to the LABIN line of the DM input.

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
