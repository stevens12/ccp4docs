.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: EP using Phenix
   :name: ep-using-phenix
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. rubric:: Experimental phasing with Phaser in Phenix
   :name: experimental-phasing-with-phaser-in-phenix

Data for this tutorial are found
`here <http://www.phaser.cimr.cam.ac.uk/index.php/Tutorials>`__.

We will assume that you are using version 1.6 of Phenix or later, as the
graphical interface does not work as smoothly in earlier versions.
Further documentation on Phenix can be found on the `Phenix
webpage <http://www.phenix-online.org/documentation/>`__.

.. rubric:: Tutorial 1: SAD
   :name: tutorial-1-sad

SAD phasing and model-building in Phenix using HySS, Phaser and Resolve.

::

    Reflection data: iod_scala-unique.mtz 
    Sequence file: hewl.seq 

Lysozyme will readily crystallise from a wide range of conditions and
yield several crystal forms. One of those, tetragonal lysozyme, is
particularly well suited for halide soaking, since it grows from a high
concentration of sodium chloride. A dataset has been collected from a
lysozyme crystal soaked in 0.5 M potassium iodide. We will use the
Phenix AutoSol wizard to find the anomalous substructure (HySS),
complete the substructure and compute SAD phases (Phaser) and use a
combination of map improvement and model-building to construct an
initial model (Resolve).

#. Download the data file, un-tar it and cd into the directory.
#. Type the command "phenix" to open the Phenix GUI. Create a new
   project, which you could name "SAD".
#. Open the AutoSol GUI by choosing the AutoSol button under Phasing on
   the right hand side.
#. Use the "Browse" button to open the MTZ file containing the SAD data
   (iod\_scala-unique.mtz) and the sequence file (hewl.seq). Enter the
   wavelength (1.5418 for Cu-Ka radiation), the atom type (I) and a
   guess for the number of sites from the soaking experiment (10). The
   guess will be used by HySS when it determines the substructure, but
   the subsequent refinement and completion in Phaser will add as many
   sites as there is good evidence for in log-likelihood-gradient maps.
   If you like, you can click the button "Guess missing f'/f" values"
   and the values will be filled in by table lookup, but Phaser will
   carry out a table lookup from the wavelength and atom type in any
   case.
#. Click the "Run" button at the top of the window.
#. Once phenix.xtriage has been run to check the data quality, the
   logfile and graphs will be available from the Summary tab. Click on
   "Results and graphs" to see that the data set appears to be of good
   quality. In particular, note that the "Anomalous measurability" graph
   indicates useful anomalous signal to at least 2.2Å.
#. Once phenix.hyss has been run to determine the substructure and
   Phaser has been run to complete the substructure and carry out
   phasing, the number of sites and some quality indicators will be
   available under the "Heavy-atom search and phasing" tab. The "Graphs"
   tab will show the results of phasing at an initial low resolution
   then at the full resolution.
#. After Resolve has been run to carry out a quick model-building job,
   the "Model-building" tab will show the statistics for the results. At
   this point it is probably a good idea to first go into the "Summary"
   tab and click "Open in Coot" to view the initial model in the
   density-modified map.
#. If you have time, go back to the "Model-building" tab and click "Run
   AutoBuild" to launch the AutoBuild GUI for a more extensive round of
   model-building.

.. rubric:: Tutorial 2: MR+SAD
   :name: tutorial-2-mrsad

This tutorial illustrates a common molecular replacement/experimental
phasing scenario, when refinement is hindered by very strong model bias,
but there is some experimental phasing signal available.

::

    Reflection data: lyso2001_scala1.mtz 
    Lactalbumin model: 1fkq_prot.pdb 
    Sequence file: hewl.seq 

Goat α-lactalbumin is 40% identical to hen egg-white lysozyme. Although
it is possible to solve lysozyme using α-lactalbumin as a model, it is
very difficult to refine the structure, partly because of model bias.
Unfortunately, the low solvent content of this crystal form limits the
ability of density modification to remove the bias. However, one can use
anomalous scattering from intrinsic sulfur atoms to improve phases
dramatically. It is noteworthy that the anomalous signal from the sulfur
atoms in this data set is not sufficient for ab initio phasing (it is
not possible to locate the anomalous scatterers from the data alone).

If you have run the pure SAD tutorial above, you shouldn't need to set
up a new project.

#. Solve the structure with the α-lactalbumin model by running Phaser
   with the AutoMR wizard in Phenix. Use the "Browse" button to choose
   the reflection file (lyso2001\_scala1.mtz). Under the "Search models"
   tab, define the MR model by specifying the PDB file (1fkq\_prot.pdb)
   and its sequence identity to the target (0.4). Under the "Asymmetric
   unit contents" tab, specify the content by providing the sequence
   file (hewl.seq). Note that you are only expecting one copy in the
   asymmetric unit. Uncheck the box "Run AutoBuild after MR" because we
   will want to run AutoSol instead, so that the phases can be improved
   by addition of the S-SAD phase information. (If you forget, you will
   get a chance to cancel the AutoBuild run!) Now click "Run".
#. This job results in a single solution with a convincing
   translation-function Z-score, but it would be relatively difficult
   (though not impossible) to rebuild and refine. We can add the S-SAD
   phase information easily by going to the "Summary" tab and clicking
   the "Run MR-SAD" button to launch the AutoSol GUI. Note that all of
   the required input files have already been selected. All you need to
   do is to specify the wavelength (1.5418Å) and the atom type (S)
   before clicking the Run button.
#. You could look through the output as for the iodide test case. In
   particular, note that phenix.xtriage gives a much more cautious
   assessment of the quality of the anomalous differences.
#. Once the "Density modification" section appears under the "Heavy-atom
   search and phasing" tab, you can view the S atom sites within the
   density-modified map by selecting solution 1 then clicking "View
   sites and density-modified map".
#. Once the initial model-building has finished, as before, you can go
   to the "Summary" tab and click "Open in Coot" to view the initial
   model.
#. If you have time, you can open the "Model-building" tab and click
   "Run AutoBuild" to carry out a more extensive model-building.

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
