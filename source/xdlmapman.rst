XDLMAPMAN (CCP4: Deprecated Program)
====================================

NAME
----

**xdlmapman** - X-windows tool; manipulation, analysis and reformatting
of electron density maps.

SYNOPSIS
--------

| **xdlmapman** [-font1 \| font2 \| font3 \| font4 \| font5]
| [menu-driven command selection; interactive parameter input]

DESCRIPTION
-----------

xdlmapman can be used to read, write, analyse and manipulate
electron-density maps from most biomacromolecular refinement packages,
as well as from/for CCP4, PHASES and O.

The program runs in interactive mode only. It uses the XDL\_VIEW toolkit
of J.W. Campbell to provide an easy-to-use interface. Commands are
selected by clicking on the desired menu option. Map formats are
selected with pop-up menus; all other parameters are set in pop-up
dialogue boxes (cut-and-paste is supported). In most cases, default
values are given in [square brackets]. To accept these defaults, hit the
RETURN key. If multiple numbers are to be input (e.g., cell constants),
and if only the first one needs to be changed, for instance, typing the
new value for this first number followed by the RETURN key will preserve
the values for the other five numbers.

There is a command line option (-font?) which will determine the size of
the menu font. These fonts refer to the xdl fonts which are defined from
1 to 5. This can be useful if the window size is too large for the
screen. The default font size is 2. The font definitions can be changed
in the .Xdefaults file or xrdb, however all xdl programs will be then be
effected.

Output from the program is written to a separate area of the main
window. Output can be scrolled and cut and pasted into other documents.

For lengthy operations a progress bar shows how much of the operation
has been completed.

HISTORY
-------

xdlmapman is a CCP4 special version of MAPMAN (part of the Uppsala RAVE
averaging package). This version is entirely interactive and has a
user-friendly interface. However, this version can only handle one map
at a time, and some of the functionality of the parent program is
absent.

MAPMAN was originally written as a simple format-exchange program, to
convert X-PLOR maps to CCP4 format etc. It has grown quite a bit to
handle many types of map, to include MAPPAGE functionality (previously
provided as a stand-alone program from Uppsala), and to perform
skeletonisation (also previously provided as a stand-alone program from
Uppsala) and peak-picking.

Many (parts of) routines were written by other people including Soren
Thirup, Morten Kjeldgaard, Wolfgang Steigemann and Phil Evans.

MENU OPTIONS
------------

Commands are issued by moving the pointer over the desired menu option
and clicking with the left mouse button.

READ A NEW MAPFILE
~~~~~~~~~~~~~~~~~~

Provide the name of the file and then select the appropriate file type
from the pop-up menu. See `FORMATS <#formats>`__.

LIST MAP STATISTICS
~~~~~~~~~~~~~~~~~~~

This prints some information and statistics pertaining to the map that
is currently in memory.

MOMENTS OF THE MAP
~~~~~~~~~~~~~~~~~~

This calculates and lists statistical moments of the map (average,
standard deviation, skewness and curtosis).

MULTIPLY MAP BY A FACTOR
~~~~~~~~~~~~~~~~~~~~~~~~

All density values will be multiplied by the number provided (if this
number is not equal to zero or one).

DIVIDE MAP BY A FACTOR
~~~~~~~~~~~~~~~~~~~~~~

All density values will be divided by the number provided (if this
number is not equal to zero or one).

PLUS SOME VALUE
~~~~~~~~~~~~~~~

The number provided (if unequal zero) will be added to all density
values.

SCALE THE MAP TO A RANGE
~~~~~~~~~~~~~~~~~~~~~~~~

The map is scaled such that all electron density values lie inside the
range of numbers provided (e.g., -100 to +100).

ZERO PARTS OF THE MAP
~~~~~~~~~~~~~~~~~~~~~

All density values which lie outside the range provided are set to zero.

NORMALISE THE MAP
~~~~~~~~~~~~~~~~~

The map will be scaled such that the average density is zero and the
standard deviation ("sigma") is one.

UVW
~~~

The axis-order in which the map is written is changed according to the
input by the user. For example, to write out XY sections with Y varying
fastest, then X and then Z, use UVW = 2 1 3.

SPACEGROUP OF THE MAP
~~~~~~~~~~~~~~~~~~~~~

The spacegroup \*number\* will be set to the number provided. For
example, to change the spacegroup to P212121, the number 19 would be
entered.

CELL CONSTANTS
~~~~~~~~~~~~~~

The cell constants can be edited. This is normally not a good idea. The
only exception is when a map is read with a format that does not
precisely enough reproduce the cell constants (e.g., integer\*2 DSN6
maps). In such cases, angles of 90.0 degrees are sometimes converted to
89.9998.

BONES SKELETONISE
~~~~~~~~~~~~~~~~~

This is the Thirup & Jones implementation of Greer's electron density
skeletonisation algorithm. A base level (e.g., 1.5 \* sigma) and a step
size (e.g., sigma) need to be provided. This command would normally be
followed by BONES WRITE (quod vide). the reason to separate the two is
to enable (1) experimentation with different skeletonisation parameters
without having to write out the BONES file, and (2) experimentation with
parameters of the BONES WRITE command without a need to redo the
skeletonisation.

BONES WRITE
~~~~~~~~~~~

Transform the skeletonised density into a set of BONES suitable for use
in O. Provide the filename, skeleton name and a cut-off length to
partition bones into main-chain and side-chain fragments.

PROD\_PLUS FOR MAPPAGE/BRIX
~~~~~~~~~~~~~~~~~~~~~~~~~~~

With this command the value of Prod and Plus for MAPPAGE and BRIX can be
changed. These values are normally set such that the entire range of
density can be captured by the integer\*2 format of DSN6 maps.

RANGE FOR MAPPAGE/BRIX
~~~~~~~~~~~~~~~~~~~~~~

Instead of supplying Prod and Plus for MAPPAGE and BRIX, the desired
range of density values which should be covered in a DSN6 map can be
supplied. The program will use this range to calculate appropriate
values for Prod and Plus.

MAPPAGE FILE
~~~~~~~~~~~~

This produces a DSN6 map ("mini-map") suitable for use with O. The map
is in binary DSN6 format, using one byte per density value.

BRIX FILE
~~~~~~~~~

This produces a BRIX map ("mini-map") suitable for use with O. The map
is in binary character format, which means that it can head inspected
(using Unix commands such as cat, head, or more). In particular, the
header record can be inspected in this fashion.

SWAPBYTES
~~~~~~~~~

This command takes an existing DSN6 map file which was created on a VAX,
swaps the bytes, and then writes the new records back to the \*same\*
file.

WRITE MAP
~~~~~~~~~

Provide the name of the file and then select the appropriate file type
from the pop-up menu. See `FORMATS <#formats>`__.

PICK PEAKS
~~~~~~~~~~

With this command, peaks above a certain level and in a user-defined
part of the map can be picked. They are written out as a PDB file
containing dummy water molecules (in fact, water oxygen atoms). The
algorithm is the same as that used to pick peaks in 2D and 3D NMR
spectra (see `REFERENCES <#references>`__).

DELETE CURRENT MAP
~~~~~~~~~~~~~~~~~~

Remove the current map from memory.

HELP
~~~~

This prints some brief information. Subsequently, click on \*any\* menu
command to get a brief explanation of what that command does.

QUIT
~~~~

Stop working with the program.

FORMATS
-------

Supported input formats : PROTEIN FFT-Y TENEYCK2 CCP4 X-PLOR EZD MASK
NEWEZD BINXPLOR BRICK DSN6 3DMATRIX TNT PHASES FSMASK

Supported output formats : CCP4 EZD MASK NEWEZD ENVELOPE

Notes on formats :

    CCP4 - seems to work okay (not masks ?)
    PROTEIN/FFT-Y/TENEYCK2 - untested
    X-PLOR - ASCII maps; works okay
    BINXPLOR - binary maps; may work on 32-bit machines
    BRICK/DSN6 - O-type map files (not BRIX !)
    EZD/NEWEZD - O-type ASCII maps
    MASK - any of the Uppsala mask formats (output: COM only)
    3DMATRIX - ASCII files from X-PLOR Rot Fun etc.
    TNT - untested
    PHASES - Furey maps; works except for scale
    FSMASK - Furey masks; not implemented yet
    ENVELOPE - CCP4-type masks; does not work ?

REFERENCES
----------

#. XDLMAPMAN:
   G.J. Kleywegt & T.A. Jones (1996), *Acta Cryst.* **D52**, 826-828.
#. XDL\_VIEW:
   J.W. Campbell (1995). "XDL\_VIEW, an X-windows-based toolkit for
   crystallographic and other applications", *J. Appl. Cryst.* **28**,
   236-242.
#. RAVE:
   G.J. Kleywegt & T.A. Jones (1994). "Halloween ... Masks and Bones",
   in "From First Map to Final Model" (S. Bailey, R. Hubbard & D.
   Waller, Eds.), SERC Daresbury Laboratory, pp. 59-66.
#. MINI-MAPS:
   T.A. Jones (1978). "A graphics model building and refinement system
   for macromolecules", *J. Appl. Cryst.* **11**, 268-272.
#. O/BONES:
   T.A. Jones, J.Y. Zou, S.W. Cowan, & M. Kjeldgaard (1991). "Improved
   methods for building protein models in electron density maps and the
   location of errors in these models", *Acta Cryst.* **A47**, 110-119.
#. SKELETONISATION:
   J. Greer (1974). "Three-dimensional pattern recognition: an approach
   to automated interpretation of electron density maps of proteins",
   *J. Mol. Biol.* **82**, 279-301.
#. PEAK-PICKING:
   G.J. Kleywegt, G.W. Vuister, A. Padilla, R.M.A. Knegtel, R. Boelens,
   & R. Kaptein (1993). "Computer-assisted assignment of homonuclear 3D
   NMR spectra of proteins. Application to pike parvalbumin III", *J.
   Magn. Reson.* **B102**, 166-176.
#. CCP4:
   Collaborative Computational Project Number 4 (1994). "The CCP4 suite:
   programs for protein crystallography", *Acta Cryst.* **D50**,
   760-763.

KNOWN BUGS
----------

| Some of the map formats are untested.
| If you improve the program, please notify GJK of your changes so that
  they can be implemented in future versions and the entire community
  may benefit from them (E-mail a brief description and the relevant
  pieces of code to "gerard@xray.bmc.uu.se").

AUTHORS
-------

Originators: G.J. Kleywegt & T.A. Jones, Uppsala

SEE ALSO
--------

Companion program for reflection files:

-  `xdldataman <xdldataman.html>`__

Other map/mask conversion programs:

-  `mama2ccp4 <mama2ccp4.html>`__
-  `maptona4 <maptona4.html>`__
