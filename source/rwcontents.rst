RWCONTENTS (CCP4: Supported Program)
====================================

NAME
----

**rwcontents** - Count atoms by type, and other analyses

SYNOPSIS
--------

| **rwcontents xyzin** *foo\_in.xyz*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

The program reads and analyses a PDB file. Information given includes
numbers of atoms by type, theoretical number of hydrogens, molecular
weights, Matthews Coefficient the sum of the atomic formfactors, F000,
and the sum of the square of the atomic formfactors, ie I000.

If the NHOH keyword is specified, the number of water molecules expected
to be determined by PX at the given resolution and temperature is
reported, as estimated by the statistical analysis of Carugo & Bordo,
Acta Cryst D, 55, 479 (1999). This estimation is based on an analysis of
structures deposited at the Protein Data Bank. Only models having less
that 3% heteroatom content other than water were considered in this
analysis. The predicted number of crystallographic waters is meant only
as a guide, and is not a substitute for correct evaluation of electron
density maps.

INPUT AND OUTPUT FILES
----------------------

XYZIN
    Input coordinates in CCP4 PDB format.

KEYWORDED INPUT
---------------

 NHOH <reso> [ ROOM \| LOWT ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Evaluate the number of water molecules expected to be determined by PX,
as estimated by the statistical analysis of Carugo & Bordo. This is
determined by the resolution <reso> which must be given. The subkeyword
ROOM or LOWT specifies whether the expression for room temperature or
low temperature results is used (default ROOM).

 END
~~~~

(Optional) Specifies the end of keyworded input and starts RWCONTENTS
running.

AUTHORS
-------

Originator : Eleanor Dodson

Modifications : Martyn Winn

 EXAMPLES
---------

Sample Unix input using toxd:

::


      rwcontents XYZIN $CEXAM/toxd/toxd.pdb << eof 
      nhoh 2.3 lowt
      end
      eof

PRINTER OUTPUT
--------------

The output contains the following information derived from the
coordinate data:

-  a count of the number of amino acid residues, *e.g.*

   ::


        Number of amino-acids residues =     59

-  estimates for the theoretical numbers of hydrogens for the protein
   and for the waters, *e.g.*

   ::


        A rough estimate of the  theoretical number of Hydrogens for the protein is  489.
        (including Haems).

        A rough estimate of the  theoretical number of Hydrogens for the waters is   118.

-  the number of atoms of each type in the PDB file (taking account of
   partial occupancies), *e.g.*

   ::


        Number of atoms of each type in PDB file (except hydrogens).
        (Partial occupancies are taken into account.)

        Atom number and type - number of atoms        6    C    295.00
        Atom number and type - number of atoms        7    N     93.00
        Atom number and type - number of atoms        8    O     95.00
        Atom number and type - number of atoms       16    S      9.00
                             - number of waters                  59.00

-  the total number of atoms including hydrogens, *e.g.*

   ::


        Total number of protein atoms (including hydrogens)    966.00
        Total number of         atoms (including hydrogens)   1158.00

-  the number of water molecules expected to be determined by PX, as
   estimated by the statistical analysis of Carugo & Bordo, *e.g.*

   ::


        Total number of protein atoms (excluding H) =     477.0
        Expected number of crystallographic waters  =      39
        Associated standard error of this estimate  =       4
        Expected range at 99% confidence level:            30  to      48
        Actual number of waters in model            =      59.0

-  cell volume, molecular weight of protein and of all atoms, *e.g.*

   ::


        Cell volume:                                           66488.688

        Molecular Weight of protein:                            7139.000

        Molecular Weight of all atoms:                          8201.000

        I000 - SIGMA - Sum(fi_squared)  Protein only:          23049.000

        I000 - SIGMA - Sum(fi_squared)  All Atoms:             26825.000

        F000 - SIGMA - Sum(fi)          Protein only:          3325.000

        F000 - SIGMA - Sum(fi)          All Atoms:             3797.000

-  the Matthews coefficient, and estimates of the solvent content and of
   the percentage of the unit cell which is empty, *e.g.*

   ::


        The Matthews Coefficient is :                              9.31
        Assuming protein density is 1.34, the solvent % is:       86.69
        If atom volume ~ 10,  % of cell without protein is:       85.47
        If atom volume ~ 10,  % of cell without atoms is:         82.58
