DM (CCP4: Supported Program)
============================

NAME
----

**dm** - density modification package, release 2.1, 04/04/00

SYNOPSIS
--------

| **dm HKLIN** *foo.mtz* **HKLOUT** *bar.mtz* [ **SOLIN** *foo.msk* ] [
  **SOLOUT** *bar.msk* ] [ **NCSIN1** *foo1.msk* [ **NCSIN2 ...** ] ] [
  **NCSOUT** *foobar.msk* ] [ **VUOUT** *foobar.vu* ]
| [`Keyworded input <#keywords>`__]

REFERENCE
---------

-  K. Cowtan (1994), Joint CCP4 and ESF-EACBM Newsletter on Protein
   Crystallography, 31, p34-38.

DESCRIPTION
-----------

\`dm' is a package which applies real space constraints based on known
features of a protein electron density map in order to improve the
approximate phasing obtained from experimental sources. Various
information can be applied, including such diverse elements as the
following (see the `MODE <#mode>`__ keyword):

SOLV
    Solvent flattening (`reference [8] <#reference8>`__)
HIST
    Histogram mapping (`reference [9] <#reference9>`__)
MULT
    Multi-resolution modification
AVER
    NCS averaging (`reference [2] <#reference2>`__, `reference
    [6] <#reference6>`__)
SKEL
    Skeletonisation (`reference [1] <#reference1>`__, `reference
    [7] <#reference7>`__)
SAYR
    Sayre's equation (`reference [5] <#reference5>`__, `reference
    [9] <#reference9>`__)

The program has many phase extension schemes and phase
weighting/combination modes, which are selected by the appropriate
choice of keywords. The combination mode is determined by the
`COMBINE <#combine>`__ keyword, if this keyword is omitted then the
program runs in perturbation-gamma mode.

Calculation of scale and B-factor for the data are automatic. This is
performed by comparison with an empirically derived database of map
variance at different resolutions, and is more reliable than the
conventional Wilson plot.

Non-crystallographic symmetry averaging can be performed for both proper
and improper symmetries, and different NCS averaging operations can be
applied to different parts of the protein. (Thanks to Dave Schuller for
his help with this). Input masks may be on any grid and axis order. In
the case of a single averaging domain, if no averaging mask is input
then a mask can be generated automatically.

Skeletonisation is by the core-tracing algorithm of Swanson (`reference
[7] <#reference7>`__). This is faster than Greer's algorithm and allows
adjustment of the skeletonisation parameters without recalculating the
skeleton. As a result the skeletonisation calculation is rendered
largely automatic.

\`dm' RECIPES
-------------

As a starting point, I used the following recipes. If averaging is
available, use it and run for at least 10 cycles:

::

    SOLC <solc>
    MODE SOLV HIST MULT AVER
    COMBINE PERT
    NCYCLE 20
    AVER REFI
     ...
    LABIN ...
    LABOUT ...

For averaging calculations where there is a great deal of phase
extension to be performed (*e.g.* from 6.0Å to 2.5Å), use more cycles
and specify a phase extension scheme:

::

    SOLC <solc>
    MODE SOLV HIST AVER
    COMBINE PERT
    SCHEME RES FROM 6.0
    NCYCLE 200
    AVER REFI
     ...
    LABIN ...
    LABOUT ...

or

If averaging is not available, you may want to use `NCYCLE
AUTO <#ncycle_auto>`__ to prevent the phase bias and overweighting.
Alternatively, you can run the calculation for more cycles, but be aware
that the FOMs will be badly overestimated:

::

    SOLC <solc>
    MODE SOLV HIST MULT
    COMBINE PERT
    NCYCLE AUTO
    LABIN ...
    LABOUT ...

`HISTogram matching <#mode_hist>`__ should ALWAYS be used.
`MULTi-resolution modification <#mode_mult>`__ is new, but also worth
using.

Free Indicators
~~~~~~~~~~~~~~~

There are two Free indicators that \`dm' can use. The first is the
density modification Free-R (defined in the same way as the refinement
Free-R). This is calculated in the Free-Sim and Omit modes.
Unfortunately, while effective for refinement, it is a poor indicator of
the progress of density modification, however it can be used in many
cases to identify the correct enantiomorph. A better indicator (due to
J. P. Abrahams) is the real-space-free-residual. This is calculated by
omitting two small spheres of protein and solvent from the density
modification. The flatness of the solvent sphere and the histogram fit
in the protein sphere provide a better indication of progress.

INPUT/OUTPUT FILES
------------------

HKLIN
~~~~~

Input mtz file - This should contain the conventional (CCP4) asymmetric
unit of data (see `CAD <cad.html>`__).

HKLOUT
~~~~~~

Output mtz file.

SOLIN
~~~~~

Input solvent mask - This overrides the automatic Wang mask
determination. The input mask can have any grid and axis ordering, and
may have any extent from the protein region of a single asymmetric unit
to the whole cell.

Alternatively, a map may be input on the SOLIN channel. In this map any
grid points set to 1.0 are considered protein, grid points set to 0.0
are solvent, and grid points set to -1.0 are considered to be neither.
By constructing an appropriate input mask it is possible to perform
solvent flattening and histogram matching without suppressing any heavy
atom density.

SOLOUT
~~~~~~

Output solvent mask - This will be on the program grid with default axis
order, and will cover the whole unit cell.

NCSIN<i>
~~~~~~~~

Input NCS averaging masks - These are used with the
`AVER <#mode_aver>`__ option. The input masks can have any grid or axis
ordering, and may cover a single monomer or the whole multimer.

If an NCS averaging mask is not input, the program will compute it with
an automatic procedure, when there is only one domain involved. Auto-NCS
masking depends on knowing how many monomers form a closed symmetry
group. This can be specified with the NCSMASK NMER keywords, or the
program will attempt to estimate it for simple cases. If you do not
supply a value, check the value the program estimates carefully.

NCSOUT
~~~~~~

If the averaging mask is calculated automatically, or is being refined
(`NCSMASK UPDATE <#ncsmask_update>`__ keyword) it may be output in this
file.

VUOUT
~~~~~

When Non-Crystallographic Symmetry is present, its symmetry elements,
i.e. axis and points, can be visualised using XtalView or O. If the
keyword VUOUT is followed by a \`\`.vu'' file, the program writes out a
file that can be used in XtalView to view the NCS elements. If the
keyword is followed by a \`\`.o'' file the output can be visualised
using the program O. Default is to .vu files.

Using ``dm`` after ``SHARP``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``SHARP`` is probably the best source of phasing for density
modification. However, if you wish to run ``dm`` after ``SHARP`` you
should first turn off the ``Solomon`` option.

``Solomon`` produces excellent maps, and so often having run ``SHARP``
and ``Solomon`` you will not want to use ``dm`` at all. However,
``Solomon`` produces badly overestimated FOM's (typically 0.9 - 0.95),
which while they do not damage the maps, effectively cripple any further
density modification (or for that matter any phased maximum-likelihood
refinement calculation).

(Since the FOM is based on an estimate of the error in the phases, a
high FOM implies that the phases are correct and therefore should not be
modified by any subsequent procedure. Thus further density modification
will hardly change the maps, and ML-refinement will be badly biased by
the errors in the starting phases.)

KEYWORDS
--------

Input is keyworded. Available keywords are: `AVERAGE <#average>`__,
`COMBINE <#combine>`__, `GRID <#grid>`__, `LABIN <#labin>`__,
`LABOUT <#labout>`__, `MODE <#mode>`__, `NCSMASK <#ncsmask>`__,
`NCYCLE <#ncycle>`__, `REALFREE <#realfree>`__,
`RESOLUTION <#resolution>`__, `SCALE <#scale>`__, `SCHEME <#scheme>`__,
`SKEL <#skel>`__, `SOLC <#solc>`__, `SOLMASK <#solmask>`__.

In addition, the following optional keywords control the data harvesting
functionality: `**PNAME** <#general_pname>`__,
`**DNAME** <#general_dname>`__, `**PRIVATE** <#general_private>`__,
`**USECWD** <#general_usecwd>`__, `**RSIZE** <#general_rsize>`__,
`**NOHARVEST** <#general_noharvest>`__

BASIC KEYWORDS
--------------

(SOLC and MODE are compulsory)

.. raw:: html

   <div align="center">

+--------------------------------------------------------------------------+
| .. rubric:: MODE [SOLV] [HIST] [MULT] [AVER] [SKEL] [SAYR] [NO????]      |
|    :name: mode-solv-hist-mult-aver-skel-sayr-no                          |
|                                                                          |
| Select the calculation to be performed:                                  |
|                                                                          |
| SOLV                                                                     |
|     Solvent flattening (recommended)                                     |
| HIST                                                                     |
|     Histogram mapping (recommended)                                      |
| MULT                                                                     |
|     Multi-resolution application of solv/hist (recommended)              |
| AVER                                                                     |
|     Non-crystallographic symmetry averaging (recommended when available) |
| SKEL                                                                     |
|     Skeletonisation                                                      |
| SAYR                                                                     |
|     Sayre's equation                                                     |
| NOHIST                                                                   |
|     Histogram matching is enabled by default. However there may exist    |
|     some very exceptional cases where histogram matching is undesirable. |
|     Such cases should first be established by careful trials with        |
|     synthetic data or solved structures. Histogram matching is more      |
|     often disabled through ignorance or superstition. It is disabled by  |
|     adding the NOHIST subkeyword.                                        |
|                                                                          |
| .. rubric:: SOLC <solc> [MEAN <solvval> <protval>]                       |
|    :name: solc-solc-mean-solvval-protval                                 |
|                                                                          |
| <solc>                                                                   |
|     = solvent content for scaling. **Always input the correct solvent    |
|     content here to ensure correct scaling**. To vary the masked volume, |
|     do not alter the SOLC card, rather use the `SOLMASK                  |
|     FRAC <#solmask_frac>`__ keyword. 0.0=all protein, 1.0=all solvent.   |
| MEAN <solvval> <protval>                                                 |
|     - used to set mean density for solvent and protein regions. This     |
|     affects scaling and density modification.                            |
|     <solvval> = mean density in solvent region.                          |
|     <protval> = mean density in protein region.                          |
|     (defaults 0.32, 0.43 electrons per cubic angstrom)                   |
|                                                                          |
| .. rubric:: NCYCLE <ncycle> \| AUTO                                      |
|    :name: ncycle-ncycle-auto                                             |
|                                                                          |
| Number of cycles of phase extension to perform.                          |
|                                                                          |
| <ncycle>                                                                 |
|     = Number of cycles over which to perform phase extension. Use 10     |
|     cycles for a quick result, try more (20-100) but check the free-R    |
|     factor. (Free-Sim mode).                                             |
| AUTO                                                                     |
|     = Run until the real-space-free residual stops decreasing, then      |
|     stop. This is used in the Perturbation/Omit combination modes when   |
|     no averaging is available and running the calculation for too many   |
|     cycles can cause phase bias and overweighting.                       |
|                                                                          |
| (defaults <ncycles>=10)                                                  |
|                                                                          |
| .. rubric:: COMBINE PERT \| OMIT [ NOCOMBINE ] [ RESTORE <restorewt> ]   |
|    :name: combine-pert-omit-nocombine-restore-restorewt                  |
|                                                                          |
| PERT                                                                     |
|     Use perturbation gamma correction for bias reduction. This is the    |
|     recommended mode for all calculations, and is selected by default.   |
| OMIT                                                                     |
|     Use reflection omit combination for bias reduction. This method is   |
|     much slower than perturbation mode and introduces some noise into    |
|     the maps, however it does enable calculation of the density          |
|     modification Free-R. This is a weak indicator (in no way comparable  |
|     to the refinement free-R) which can be helpful in identifying the    |
|     correct enantiomorph, but is inadequate for choosing density         |
|     modification calculations.                                           |
| NOCOMBINE                                                                |
|     Disable phase combination. In cases where there are strong density   |
|     constraints (*e.g.* 4+ fold averaging) this may be useful to avoid   |
|     bias from the initial phases, especially if that phasing is from an  |
|     MR model.                                                            |
| RESTORE <restorewt>                                                      |
|     <restorewt> is the weight which will be given to missing reflections |
|     restored by density modification. The recommended value is 0.0-0.2   |
|     with no averaging, up to 1.0 for high order averaging. The restored  |
|     F's may be accessed by assigning the FCDM output label. If a         |
|     combined set of Fobs and missing Fcalc is required, this should be   |
|     generated by the user with an appropriate weighting scheme.          |
|                                                                          |
| (defaults: PERT, <restorewt>=0)                                          |
| .. rubric:: SCHEME ALL \| AUTO \| RES \| MAG \| FOM [[ FROM <res> ] [    |
|    FRAC <frac> ]]                                                        |
|    :name: scheme-all-auto-res-mag-fom-from-res-frac-frac                 |
|                                                                          |
| ALL                                                                      |
|     - Use all reflections for the whole calculation. Compulsory for      |
|     `NCYCLE AUTO <#ncycle_auto>`__. (recommended for most non-averaging  |
|     calculations).                                                       |
| AUTO                                                                     |
|     - perform phase extension using a combination of resolution,         |
|     magnitude and FOM chosen on the basis of what the data set looks     |
|     like. This option will also pick a reasonable value for <frac>.      |
|     (alternative for non-averaging calculations).                        |
| RES                                                                      |
|     - perform phase extension in resolution steps, starting with the low |
|     resolution data. (recommended for averaging calculations where a     |
|     great deal of phase extension must be performed). Note: any input    |
|     phase information beyond the initial resolution limit will be        |
|     incorporated as the calculation proceeds.                            |
| MAG                                                                      |
|     - perform phase extension in magnitude steps, starting with the      |
|     largest reflections.                                                 |
| FOM                                                                      |
|     - perform phase extension in FOM steps, starting with the best       |
|     phased data.                                                         |
| FRAC <frac>                                                              |
|     - fraction of the input data to use as a starting set.               |
| FROM <res>                                                               |
|     - sets <frac> to the fraction of the data within a resolution sphere |
|     radius <res>.                                                        |
|                                                                          |
| (default: ALL, or AUTO for COMBINE FREE)                                 |
|                                                                          |
| .. rubric:: LABIN FP=.. SIGFP=.. [PHIO=.. FOMO=..] [HLA=.. HLB=.. HLC=.. |
|    HLD=..] [FDM=..] [PHIDM=..] [FOMDM=..] [FREE=..]                      |
|    :name: labin-fp..-sigfp..-phio..-fomo..-hla..-hlb..-hlc..-hld..-fdm.. |
| -phidm..-fomdm..-free..                                                  |
|                                                                          |
| Normally just the first four columns (FP,SIGFP,PHIO,FOMO) are input.     |
| However if you have Hendrickson-Lattman coefficients you may want to     |
| input these to the program as well (the difference is marginal except    |
| for SIR data). If you want to start from the end of a previous density   |
| modification calculation then the PHIDM, FOMDM columns are used.         |
|                                                                          |
| FP                                                                       |
|     = F magnitude                                                        |
| SIGFP                                                                    |
|     = standard deviation, 0 for unmeasured                               |
| PHIO                                                                     |
|     = best initial phase estimate                                        |
| FOMO                                                                     |
|     = weight attached to PHIO                                            |
|                                                                          |
| HLA-HLD                                                                  |
|     = Hendrickson Lattman coefficients                                   |
| FDM,PHIDM,FOMDM                                                          |
|     = map coefficients of the starting map to which density modification |
|     is to be applied, e.g. from a previous density modification          |
|     calculation (phase and weight) or difference map coefficients from   |
|     `SIGMAA <sigmaa.html>`__ (magnitude and phase). FDM must be on the   |
|     same scale as FP.                                                    |
| FREE                                                                     |
|     = free-R flag (only used if <ncross> > 1)                            |
|                                                                          |
| .. rubric:: LABOUT FDM=.. PHIDM=.. FOMDM=.. [FCDM=.. PHICDM=..]          |
|    [HLADM=.. etc]                                                        |
|    :name: labout-fdm..-phidm..-fomdm..-fcdm..-phicdm..-hladm..-etc       |
|                                                                          |
| Three columns are output by default, a magnitude, phase and              |
| figure-of-merit. Normally a map would be calculated using FDM and PHIDM  |
| (do not include the weight FOMDM). Alternatively a weighted map with FP, |
| PHIDM, FOMDM should give the same result, except that restored           |
| magnitudes will be missing.                                              |
|                                                                          |
| FDM                                                                      |
|     weighted (restored) structure factor for map calculation.            |
| PHIDM                                                                    |
|     modified phase                                                       |
| FOMDM                                                                    |
|     weight attached to PHIDM                                             |
| FCDM PHICDM                                                              |
|     magnitude and phase from final modified map before phase             |
|     recombination                                                        |
| HLADM HLBDM HLCDM HLDDM                                                  |
|     modified Hendrickson Lattman coefficients                            |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

ADVANCED/AVERAGING KEYWORDS
---------------------------

.. raw:: html

   <div align="center">

+--------------------------------------------------------------------------+
| .. rubric:: AVERAGE [DOMAIN <idom>] [REFI [STEP <dr> <dphi>] [EVERY      |
|    <nref>]]                                                              |
|    :name: average-domain-idom-refi-step-dr-dphi-every-nref               |
|                                                                          |
| Set a NCS symmetry averaging operator. This card is followed by          |
| rotation/translation matrices on subsequent lines in either CCP4 or      |
| O/RAVE format.                                                           |
|                                                                          |
| CCP4 Formats (see also the program `\`lsqkab' <lsqkab.html>`__)          |
|     ROTA EULER <alpha> <beta> <gamma> (Euler angles)                     |
|     TRAN <t1> <t2> <t3>                                                  |
| or                                                                       |
|     ROTA POLAR <omega> <phi> <kappa> (Polar angles)                      |
|     TRAN <t1> <t2> <t3>                                                  |
| or                                                                       |
|     ROTA MATRIX <r11> <r12> <r13> <r21> <r22> <r23> <r31> <r32> <r33>    |
|     TRAN <t1> <t2> <t3>                                                  |
| O/RAVE Format                                                            |
|     OMAT                                                                 |
|     r11 r21 r31                                                          |
|     r12 r22 r32                                                          |
|     r13 r23 r33                                                          |
|     t1 t2 t3                                                             |
|     (note that the rotation matrix is transposed with respect to CCP4    |
|     matrix format)                                                       |
| where                                                                    |
|     x' = r11 x + r12 y + r13 z + t1                                      |
|     y' = r21 x + r22 y + r23 z + t2                                      |
|     z' = r31 x + r32 y + r33 z + t3                                      |
|                                                                          |
| These are the operations which map the density in the region covered by  |
| the input mask onto the other equivalent regions. The first operator     |
| must be the identity matrix. The mask is input in CCP4 mask format on    |
| the input file label NCSIN1. In the case of improper ncs, the mask must  |
| cover just a monomer, for proper ncs it may cover the monomer or         |
| multimer. The mask grid need not agree with the program grid.            |
|                                                                          |
| If no input mask is assigned then 'dm' will attempt to generate one      |
| automatically from the local density correlation, under the control of   |
| the `NCSMASK <#ncsmask>`__ keyword. Always check the mask afterwards in  |
| an appropriate graphics program (use the NCSOUT channel).                |
|                                                                          |
| If you want to apply different NCS operations to different domains of    |
| the protein, give a set of AVER cards for each DOMAIN, with the DOMAIN   |
| number on each AVER card (or the first for each domain). An input mask   |
| is also required for each domain. The AVER DOMAIN 1 cards corresponds to |
| the mask on NCSIN1, the AVER DOMAIN 2 to NCSIN2, etc. The masks should   |
| be defined in the same multimer in the unit cell, or at least in close   |
| proximity to one another.                                                |
|                                                                          |
| The REF, STEP and EVERY cards will enable refinement of the NCS rotation |
| matrices between averaging cycles. The REF card enables the refinement   |
| of a particular set of NCS parameters. Note that the STEP card allows    |
| different refinement step sizes can be used for different domains,       |
| however all but one EVERY card will be ignored. The refined matrices     |
| will be written out at the end of the log file.                          |
|                                                                          |
| <dr>                                                                     |
|     = step size for refinement of positional parameters in Angstrom.     |
| <dphi>                                                                   |
|     = step size for refinement of rotational parameters in degrees.      |
| <nref>                                                                   |
|     = the number of phase extension cycles between each parameter        |
|     refinement.                                                          |
|     (defaults <dr>=0.5 A, <dphi>=2.5 degrees, <nref>=3) See also the     |
|     document `dm\_ncs\_averaging <dm_ncs_averaging.html>`__,             |
|                                                                          |
| .. rubric:: NCSMASK [OVERLAP] [INVERT] [NMER <nmer>] [UPDATE <cyc>]      |
|    [STEP <step>] [ALIM <u1> <u2>] [BMIN <v1> <v2>] [CLIM <w1> <w2>]      |
|    [SIZE <size>] [BFAC <bfac>]                                           |
|    :name: ncsmask-overlap-invert-nmer-nmer-update-cyc-step-step-alim-u1- |
| u2-bmin-v1-v2-clim-w1-w2-size-size-bfac-bfac                             |
|                                                                          |
| Control ncs-matrix, masking, and auto-masking behaviour. The OVERLAP     |
| card forces overlap removal for all NCS-masks. This was the default mode |
| of operation for old versions of \`dm' which did not support multimer    |
| masks; it must not be used if the NCS-mask covers a more than one        |
| monomer. Note that the ncs-correlation statistics may be less reliable   |
| when using a multimer mask.                                              |
|                                                                          |
| The INVERT card forces all symmetry operators to be inverted (reversed), |
| thus with this card the operator maps from the symmetry related copy     |
| back onto the masked copy.                                               |
|                                                                          |
| The NMER, STEP, ALIM, BLIM, CLIM, SIZE and BFAC cards control            |
| ncs-auto-masking if no averaging mask is input.                          |
|                                                                          |
| NMER <nmer>                                                              |
|     The number of monomers related by proper symmetries. This number is  |
|     automatically computed by the program, but the user has the option   |
|     to introduce his own choice, if needed.                              |
|     For example: in the case of 2-fold improper ncs the <nmer>=1. In the |
|     case of 2-fold proper (*i.e.* rotational) ncs, <nmer>=2. In the case |
|     of a 2-fold ncs axis perpendicular to a 3-fold crystallographic      |
|     axis, 6 copies of the molecule will obey the ncs operator, and so    |
|     <nmer>=6. Also, when the crystal contains a closed group of mixed    |
|     crystallographic and non-crystallographic symmetry (*e.g.* a 222 ncs |
|     with one ncs 2-fold and one crystallographic 2-fold), the automatic  |
|     determination of monomers might go wrong. In such a case the user    |
|     has to check the automatic assignment to be sure the program is      |
|     doing the correct thing.                                             |
| UPDATE <cyc>                                                             |
|     - Update the averaging mask every <cyc> cycles. This allows an input |
|     mask to be refined after the first few cycles, or for an automask to |
|     be refined.                                                          |
| STEP <step>                                                              |
|     The coarseness of the search to find the ncs averaging mask.         |
|     Increasing this value will speed up the mask calculation, decreasing |
|     it will produce a better mask. Values of 2,3, or 4 are realistic.    |
| ALIM <u1> <u2>                                                           |
|     Limits on the search space over which the mask is to be formed.      |
|     These are necessary if an ncs-axis lies perpendicular to a cell      |
|     edge, thus the ncs maps the cell repeat to itself along that axis.   |
|     In this case the mask must be limited to a single repeat along that  |
|     direction, *e.g.* ALIM 0 1 or ALIM -0.5 0.5 sets limits along        |
|     a-axis.                                                              |
| BLIM <v1> <v2>                                                           |
|     See ALIM. Sets limits along B axis.                                  |
| CLIM <w1> <w2>                                                           |
|     See CLIM. Sets limits along B axis.                                  |
| SIZE <size> (usually not needed)                                         |
|     If the ncsautomask procedure gives an out-of-memory error place a    |
|     number greater than 1 on this card.                                  |
| BFAC <bfac> (usually not needed)                                         |
|     Temperature factor for smoothing the map before calculating local    |
|     correlation.                                                         |
|                                                                          |
| Defaults: <nmer>=1, <step>=3, <size>=1, <bfac>=20.                       |
|                                                                          |
| .. rubric::  SOLMASK [ UPDATE <cyc> ] [ FRAC <solvfrac> <protfrac> ] [   |
|    RADIUS <radius> <mode> ] [ LIMITS <rhomin> <rhomax> ]                 |
|    :name: solmask-update-cyc-frac-solvfrac-protfrac-radius-radius-mode-l |
| imits-rhomin-rhomax                                                      |
|                                                                          |
| Set parameters for calculation of the solvent mask.                      |
|                                                                          |
| UPDATE <cyc>                                                             |
|     - Update the solvent mask every <cyc> cycles. This allows an input   |
|     mask to be refined after the first few cycles.                       |
| FRAC <solvfrac> <protfrac>                                               |
|     - used to set different mask volumes to the above for histogram      |
|     matching and solvent flattening.                                     |
|     <solvfrac> = fraction of cell to be masked as solvent.               |
|     <protfrac> = fraction of cell to be masked as protein.               |
|     If <solvfrac>+<protfrac> < 1.0 then there will be a buffer region    |
|     between solvent and protein which is neither histogram matched or    |
|     solvent flattened. This feature allows uncertain regions to be       |
|     neither solvent flattened nor histogram matched.                     |
| RADIUS <radius> <mode>                                                   |
|     <radius> = radius of averaging sphere (Angstroms)                    |
|     <mode> = 0: Use weighting scheme w=constant (Spherical top hat)      |
|     <mode> = 1: Use weighting scheme w=1-(r/R) (Wang's method)           |
|     <mode> = 2: Use weighting scheme w=1-(r/R)\*\*2                      |
|                                                                          |
| Heavy atoms can bias the mask calculation procedure, resulting in a mask |
| of spheres around the heavy atom sites. The LIMITS card can be used to   |
| set the values at which the electron density is truncated before         |
| smoothing. To truncate heavy atoms set <rhomax> to the maximum electron  |
| density due to non-heavy atoms at the appropriate resolution.            |
|                                                                          |
| | If a negative Wang radius is given, then the program will determine a  |
|   suitable radius from the data. This radius will decrease as the        |
|   calculation progresses.                                                |
| | (defaults <radius>=-1.0 <mode>=2 <rhomin>=0.32 <rhomax>=2.0 e/A^3)     |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

OTHER KEYWORDS
--------------

Don't use these unless you really know what you are doing. In which case
you'd better have a better idea of what you are doing than I do.

.. raw:: html

   <div align="center">

+--------------------------------------------------------------------------+
| .. rubric:: COMBINE RPERT \| EMPIRICAL \| GAMMA <gamma> \| FREE <ncross> |
|    [ SETS <numsets> ] [ WEIGHT <cmbwt> ]                                 |
|    :name: combine-rpert-empirical-gamma-gamma-free-ncross-sets-numsets-w |
| eight-cmbwt                                                              |
|                                                                          |
| RPERT                                                                    |
|     Use resolution dependent perturbation gamma correction for bias      |
|     reduction. This is generally unnecessary with current density        |
|     modifications. `COMBINE PERT <#combine_pert>`__ already incorporates |
|     the necessary resolution dependence for `MODE MULTI <#mode_mult>`__. |
| EMPIRICAL                                                                |
|     Use empirical gamma correction (`reference [12] <#reference12>`__)   |
| GAMMA <gamma>                                                            |
|     Use a gamma correction of <gamma>                                    |
| FREE <ncross>                                                            |
|     Use Free-Sim phase combination. <ncross> = number of times each step |
|     is performed to provide statistics for the free-R and phase          |
|     weighting.                                                           |
|     For <ncross>=1 a changing random set of reflections are omitted each |
|     cycle for the free-R factor.                                         |
|     For <ncross>=2 a fixed set is chosen (using the free-R flag if       |
|     available) and omitted for the free-R factor, then the cycle is run  |
|     a second time using all the reflections.                             |
|     For <ncross> > 2 (<ncross>-1) multiple free-R sets are generated,    |
|     then on the <ncross>-th cycle all reflections are included.          |
|     The total time taken is proportional to the product of these two     |
|     values. Use <ncross> = 1 for large structures where the time becomes |
|     a significant factor, otherwise use <ncross> = 2. Only use <ncross>  |
|     > 2 for small structures where the statistics are particularly poor  |
|     (< 5000 reflections).                                                |
| SETS <numsets>                                                           |
|     <numsets> is the number of free sets into which the data will be     |
|     divided. These are used both in Free-Sim and Reflection Omit modes.  |
|     In reflection omit mode the calculation time increases in proportion |
|     to the number of free sets.                                          |
| WEIGHT <cmbwt>                                                           |
|     <cmbwt> is a weight applied to the initial phasing in phase          |
|     combination. Normally <cmbwt>=1, COMBINE NOCOMBINE implies <cmbwt>=0 |
|                                                                          |
| (<ncross>=1, <numsets>=20)                                               |
| .. rubric:: RESOLUTION <rmin> <rmax>                                     |
|    :name: resolution-rmin-rmax                                           |
|                                                                          |
| | Resolution range of reflections to include in the calculation. This    |
|   keyword can be used to exclude part of the input data by resolution    |
|   cutoffs. This is generally highly inadvisable.                         |
| | (default is the whole range of the input mtz file)                     |
|                                                                          |
| .. rubric:: SKEL [ LENGTH <joinlen> <endlen> ] [ BFAC <bfac> ] [ EVERY   |
|    <nskl> ]                                                              |
|    :name: skel-length-joinlen-endlen-bfac-bfac-every-nskl                |
|                                                                          |
| Perform iterative skeletonisation on the map. Cycles of skeletonisation  |
| are interspersed with cycles of conventional density modification.       |
|                                                                          |
| <joinlen>                                                                |
|     = length of skeleton in Angstrom/residue to generate between density |
|     peaks.                                                               |
| <endlen>                                                                 |
|     = length of skeleton in Angstrom/residue to generate in \`trailing   |
|     ends'.                                                               |
| <bfac>                                                                   |
|     = temperature factor to apply to the sharpened map before            |
|     skeletonisation.                                                     |
| <nskl>                                                                   |
|     = apply skeletonisation instead of every <nskl>-th density           |
|     modification cycle.                                                  |
|     (defaults <joinlen>=6.0 <endlen>=6.0 <bfac>=45 <nskl>=3)             |
|     See also the document                                                |
|     `\`dm\_skeletonisation' <dm_skeletonisation.html>`__.                |
|                                                                          |
| .. rubric:: GRID <nx> <ny> <nz>                                          |
|    :name: grid-nx-ny-nz                                                  |
|                                                                          |
| | Set the grid for the calculation. You may want to do this if you want  |
|   to output a map or mask.                                               |
| | (defaults: minimum efficient factors above Nyquist spacing)            |
|                                                                          |
| .. rubric:: SCALE <scale> <bfac>                                         |
|    :name: scale-scale-bfac                                               |
|                                                                          |
| Override internal scaling and scale input data by F^2 = <scale> \* exp   |
| (<bfac> \* s / 2.0) \* F^2. Scaling is critical to histogram mapping and |
| Sayre's equation. In some cases you may want to override the B-factor,   |
| but run without this card first, and consider long and hard before       |
| changing scale.                                                          |
|                                                                          |
| .. rubric:: REALFREE [SOLV <sx> <sy> <sz> <sr>] [PROT <px> <py> <pz>     |
|    <pr>]                                                                 |
|    :name: realfree-solv-sx-sy-sz-sr-prot-px-py-pz-pr                     |
|                                                                          |
| | Enable the real-free residual (implied by `NCYCLE                      |
|   AUTO <#ncycle_auto>`__). Optionally set the coordinates and radii (in  |
|   Angstrom) of the spherical patches of density where the density        |
|   modification constraints will be omitted in order to provide a         |
|   real-space free indicator of progress. If <sr> or <pr> is negative the |
|   Solvent or Protein free indicator will be omitted.                     |
| | (defaults: <sr>=4.0 <pr>=4.0, coordinates chosen from solvent mask).   |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

DATA HARVESTING KEYWORDS
------------------------

.. raw:: html

   <div align="center">

+--------------------------------------------------------------------------+
| Provided a Project Name and a Dataset Name are specified (either         |
| explicitly or from the MTZ file) and provided the                        |
| `NOHARVEST <#general_noharvest>`__ keyword is not given, the program     |
| will automatically produce a data harvesting file. This file will be     |
| written to                                                               |
|                                                                          |
| ``$HARVESTHOME``/``DepositFiles``/*<projectname>*/ *<datasetname>.dm*    |
|                                                                          |
| The environment variable ``$HARVESTHOME`` defaults to the user's home    |
| directory, but could be changed, for example, to a group project         |
| directory. When running the program through the CCP4 interface, the      |
| $HARVESTHOME variable defaults to the 'PROJECT' directory.               |
|                                                                          |
| .. rubric:: PNAME<project\_name>                                         |
|    :name: pnameproject_name                                              |
|                                                                          |
| Project Name. In most cases, this will be inherited from the MTZ file.   |
|                                                                          |
| .. rubric:: DNAME<dataset\_name>                                         |
|    :name: dnamedataset_name                                              |
|                                                                          |
| Dataset Name. In most cases, this will be inherited from the MTZ file.   |
|                                                                          |
| .. rubric:: PRIVATE                                                      |
|    :name: private                                                        |
|                                                                          |
| Set the directory permissions to '700', *i.e.* read/write/execute for    |
| the user only (default '755').                                           |
|                                                                          |
| .. rubric:: USECWD                                                       |
|    :name: usecwd                                                         |
|                                                                          |
| Write the deposit file to the current directory, rather than a           |
| subdirectory of $HARVESTHOME. This can be used to send deposit files     |
| from speculative runs to the local directory rather than the official    |
| project directory, or can be used when the program is being run on a     |
| machine without access to the directory ``$HARVESTHOME``.                |
|                                                                          |
| .. rubric:: RSIZE<row\_length>                                           |
|    :name: rsizerow_length                                                |
|                                                                          |
| Maximum width of a row in the deposit file (default 80). <row\_length>   |
| should be between 80 and 132 characters.                                 |
|                                                                          |
| .. rubric:: NOHARVEST                                                    |
|    :name: noharvest                                                      |
|                                                                          |
| Do not write out a deposit file; default is to do so provided Project    |
| and Dataset names are available.                                         |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

LOOKING AT YOUR OUTPUT
----------------------

Two free indicators may be generated. The density modification Free-R is
calculated in `COMBINE OMIT <#combine_omit>`__ mode. This is a weak
indicator (in no way comparable to the refinement free-R) which can be
helpful in identifying the correct enantiomorph, but is inadequate for
choosing density modification calculations.

The `NCYCLE AUTO <#ncycle_auto>`__ or `REALFREE <#realfree>`__ keywords
enable calculation of the real-space free residual, which provide some
information when used in conjunction with `SCHEME ALL <#scheme_all>`__.

The LogGraph output, as well as showing the free-R factor, gives some
information on the quality and completeness of the input data, and also
a plot of the data fit against a standard protein data set.

For NCS-averaging calculations, correlations are calculated between
related areas of density. These are summarised at the end of the log
file, and error or warning messages will be generated if the initial
values are too low: this is a good indication of errors in the input
matrices or mask.

Also check the statistics of the averaging mask. If using a monomer
mask, the masked fraction or the cell multiplied by the number of
monomers/ASU multiplied by the order of crystallographic symmetry should
give the protein faction. In the case of a multimer mask, this is
reduced by the size of the multimer.

AUTHOR
------

| Kevin D. Cowtan, Department of Chemistry, University of York
| email: cowtan@ysbl.york.ac.uk

REFERENCES
----------

#. Baker D., Bystroff C., Fletterick R., Agard D. (1994) Acta Cryst D49
   429-439
#. Bricogne, G. (1974) Acta Cryst A30 395-405
#. Brunger, A. T. (1992) Nature 355, 472-474.
#. Cowtan K. D., Main, P. (1993) Acta Cryst D49 148-157
#. Sayre, D. (1974) Acta Cryst A30 180-184
#. Schuller D. (1996) Acta Cryst D52 425-434
#. Swanson, S. (1994) Acta Cryst D50 695-708
#. Wang, B. C. (1985) Methods in Enzymology 115, 90-112
#. Zhang, K. Y. J., Main P. (1990) Acta Cryst A46 377-381
#. Abrahams, J. P. (1995) Acta Cryst D51 371-376
#. K. D. Cowtan, P. Main (1996) Acta Cryst D52 43-48
#. K. D. Cowtan (1999) Acta Cryst D55 1555-1567

SEE ALSO
--------

`cad <cad.html>`__, `lsqkab <lsqkab.html>`__,
`xloggraph <xloggraph.html>`__,
`dm\_skeletonisation <dm_skeletonisation.html>`__,
`dm\_ncs\_averaging <dm_ncs_averaging.html>`__

EXAMPLES
--------

`dm <../examples/unix/runnable/dm.exam>`__

::

    #
    #[ a simple solvent/histogram calculation          ]
    #

    dm      hklin gmto.mtz  hklout gmtodm.mtz  << my-data
    SOLC 0.35
    MODE SOLV HIST
    COMBINE PERT
    NCYCLE 10
    LABIN FP=FP SIGFP=SIGFP PHIO=PHIB FOMO=FOM
    LABOUT PHIDM=PHI1 FOMDM=W1
    my-data

    #
    #[ a better solvent/histogram/multires calculation,]
    #[ uses NCYCLE AUTO to terminate before phase bias ]
    #[ sets in, bias reduction using perturbation-gamma]
    #

    dm      hklin gmto.mtz  hklout gmtodm.mtz  << my-data
    SOLC 0.35
    MODE SOLV HIST MULT
    COMBINE PERT
    NCYCLE AUTO
    SCHEME ALL
    LABIN FP=FP SIGFP=SIGFP PHIO=PHIB FOMO=FOM HLA=HLA HLB=HLB HLC=HLC HLD=HLD
    LABOUT PHIDM=PHI1 FOMDM=W1
    my-data

    #
    #[ a molecular replacement type calculation]
    #[ sigmaa is used to generate FOMs for the ]
    #[ Fcalc, PHIcalc                          ]
    #

    sigmaa hklin gmto_sfall.mtz   hklout gmto_sigmaa.mtz << eof
    partial
    labin FP=FP SIGFP=SIGFP FC=FCmolr PHIC=PHICmolr
    eof

    dm     hklin gmto_sigmaa.mtz  hklout gmtodm.mtz  << my-data
    SOLC 0.35
    MODE SOLV HIST
    COMBINE PERT
    NCYCLE 10
    LABIN FP=FP SIGFP=SIGFP PHIO=PHICmolr FOMO=WCMB
    my-data

    #
    # NON-CRYSTALLOGRAPHIC SYMMETRY AVERAGING
    #[ A three fold averaging calculation      ]
    #[ This could also be done in reflection   ]
    #[ omit mode if you have enough time       ]
    #

    dm   hklin chmimir.mtz hklout dmchm.mtz   
         ncsin1 chmi.msk                      
         << MY-DATA
    SOLC 0.52
    NCYC 10
    MODE SOLV HIST AVER
    COMBINE PERT
    AVER REFI
    ROTA POLAR  0.0  0.0  0.0
    TRANS  0.0  0.0  0.0
    AVER REFI
    ROTA POLAR  113.28130 103.41944 120.33858
    TRANS  43.635 38.059 62.726
    AVER REFI
    ROTA POLAR   66.58067 -76.78019 119.69176
    TRANS  82.989 15.401 -8.928
    LABI FP=F SIGFP=SIGF PHIO=PHIB FOMO=FOM HLA=HLA HLB=HLB HLC=HLC HLD=HLD
    LABO PHIDM=PHIDM FOMDM=FOMDM
    END
    MY-DATA

    #
    # NON-CRYSTALLOGRAPHIC SYMMETRY AVERAGING
    #[ A three fold averaging calculation with ]
    #[ extreme phase extension e.g. 8.0 - 3.0Å ]
    #[ must be done more carefully, hence      ]
    #[ NCYC and SCHEME cards                   ]
    #

    dm   hklin chmimir.mtz hklout dmchm.mtz   
         ncsin1 chmi.msk                      
         << MY-DATA
    SOLC 0.52
    MODE SOLV HIST AVER
    COMBINE PERT
    SCHEME RES FROM 8.0
    NCYC 1000
    AVER REFI
    ROTA POLAR  0.0  0.0  0.0
    TRANS  0.0  0.0  0.0
    AVER REFI
    ROTA POLAR  113.28130 103.41944 120.33858
    TRANS  43.635 38.059 62.726
    AVER REFI
    ROTA POLAR   66.58067 -76.78019 119.69176
    TRANS  82.989 15.401 -8.928
    LABI FP=F SIGFP=SIGF PHIO=PHIB FOMO=FOM
    LABO PHIDM=PHIDM FOMDM=FOMDM
    END
    MY-DATA

    #
    # NON-CRYSTALLOGRAPHIC SYMMETRY AVERAGING WITH AUTOMASK
    #[ A 3-fold averaging calculation. No input averaging ]
    #[ mask is required, the the NCSMASK NMER keyword is  ]
    #[ added. Mask is updated occasionally.               ]
    #

    dm   hklin chmimir.mtz hklout dmchm.mtz   
         << MY-DATA
    SOLC 0.52
    NCSMASK NMER 3 UPDATE 4
    NCYC 10
    MODE SOLV HIST AVER
    COMBINE PERT
    AVER REFI
    ROTA POLAR  0.0  0.0  0.0
    TRANS  0.0  0.0  0.0
    AVER REFI
    ROTA POLAR  113.28130 103.41944 120.33858
    TRANS  43.635 38.059 62.726
    AVER REFI
    ROTA POLAR   66.58067 -76.78019 119.69176
    TRANS  82.989 15.401 -8.928
    LABI FP=F SIGFP=SIGF PHIO=PHIB FOMO=FOM
    LABO PHIDM=PHIDM FOMDM=FOMDM
    END
    MY-DATA

    #
    # MULTI-DOMAIN AVERAGING
    #[ a two fold averaging calculation with   ]
    #[ two domains and refinement of the 2nd   ]
    #[ set of averaging matrices.              ]
    #[ WARNING: IF YOU DONT KNOW WHAT MULTI-   ]
    #[ DOMAIN AVERAGING IS, YOU DONT NEED IT   ]
    #

    dm  hklin hpattj.mtz    hklout dm1.mtz       
        ncsin1 cwnads.mask  ncsin2 cwglobs.mask 
        << EOF-dm
    SOLC 0.57
    MODE SOLV HIST AVER
    NCYCLE 40
    AVERAGE DOMAIN 1
    OMAT
     1.0 0.0 0.0
     0.0 1.0 0.0
     0.0 0.0 1.0
     0.0 0.0 0.0
    AVERAGE DOMAIN 1
    OMAT
        -0.71389002    -0.69492584     0.08611962
        -0.69635397     0.69129372    -0.19136506
         0.07357326    -0.19652288    -0.97735721
       115.37364197    54.98566055    67.00005341
    AVERAGE DOMAIN 2 REFINE
    ROTA MATRIX  1.0 0.0 0.0 -
                 0.0 1.0 0.0 -
                 0.0 0.0 1.0
    TRANS 0.0 0.0 0.0
    AVERAGE DOMAIN 2 REFINE
    ROTA MATRIX     0.75830859     0.65183645     0.00883542  -
                    0.65189570    -0.75824565    -0.00975925  -
                    0.00033828     0.01316060    -0.99991322
    TRANS    17.30371666   -47.10081482    68.99727631
    LABIN FP=FP SIGFP=SIGFP PHIO=PHIml FOMO=FOMml HLA=HLA HLB=HLB HLC=HLC HLD=HLD
    LABOUT PHIDM=PHIDM FOMDM=FOMDM
    EOF-dm

    #
    # NOTE: If you don't know what multi-domain averaging is,
    # you don't need it. Use the ncs averaging example, not
    # the multi-domain example.
    #

