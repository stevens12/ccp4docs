CCP4 Suite (CCP4: Basic)
========================

NAME
----

**ccp4** - introduction to the CCP4 Program Suite

SYNOPSIS
--------

**program** [ **-n** ] [ **-v** *level* ] [ **-d** *file* ] [ **-e**
*file* ] [ **-i** ] [ **-nohtml** ] [ **-nosummary** ] [ *<logical name>
<file name> ...* ]

DESCRIPTION
-----------

*program* is the name of the CCP4 program being run. This documentation
outlines the general concepts common to running most CCP4 programs. For
specific details of how to run individual programs you are referred to
the specific documentation for those programs.

The contents of this documentation are:

-  `Specifying input and output files <#input>`__
-  `Keyworded input <#keywords>`__
-  `Command line options <#options>`__

-  `Appendix: details of logical name processing <#details>`__

-  `EXAMPLES <#examples>`__
-  `FILES <#files>`__
-  `SEE ALSO <#see_also>`__
-  `WARNINGS <#warnings>`__

Specifying input and output files
---------------------------------

When running CCP4 programs, input and output data files are normally
specified by associating the file names with the logical names. Logical
names are alphanumeric, and can include underscores.

The usual format of a program invocation is

::

    <program name> [ <logical name> <file name> ] ...

where "< >" indicates placeholders and "[ ]" indicates optional repeated
items i.e. zero or more occurrences (here and elsewhere in the
documentation).

A concrete example is

::

    fft hklin native-Fs.mtz mapout 2Fo-Fc.map

-  Logical names are case-insensitive
-  Each standard logical name has a default extension associated with it

File extensions are optional and if omitted default values will be used.
Some common logical names with default extensions are

+--------------+-------------------------------------+
| **HKLIN**    | input MTZ reflection file (.mtz)    |
+--------------+-------------------------------------+
| **HLKOUT**   | output MTZ reflection file (.mtz)   |
+--------------+-------------------------------------+
| **MAPIN**    | input map file (.map)               |
+--------------+-------------------------------------+
| **MAPOUT**   | output map file (.map)              |
+--------------+-------------------------------------+
| **XYZIN**    | input coordinate file (.pdb)        |
+--------------+-------------------------------------+
| **XYZOUT**   | output coordinate file (.pdb)       |
+--------------+-------------------------------------+

In some cases more than one file corresponding to the same type of entry
in $CINCL/default.def is required e.g., several input reflection files.
In this case they are typically specified as ``HKLIN``\ *n*, for
instance, where *n* is a sequence number. The rules for matching logical
names with the default specifications given above then apply to the
prefix of the name which matches the entries in the files
$CINCL/environ.def and $CINCL/default.def.

(For information on default.def and environ.def, see the section on
`details of logical name processing <#details>`__.)

Input files that do not exist are reported and the program stops
processing. Output files that already exist will also cause the program
to stop unless the environment variable **CCP4\_OPEN** is set to
\`UNKNOWN', when they will be truncated to zero, overwritten and a
warning message displayed.

Some file extensions are also treated as special cases and if you use
them then you may experience unexpected behaviour - see the section on
`warnings <#warnings>`__.

Keyworded input
---------------

Most CCP4 programs take \`keyworded' input to set their parameters.

These have a leading keyword followed, possibly, by arguments which
might be numbers or strings or keyword/value pairs of the form

*keyword*\ **=**\ *value.*

Such arguments are separated by spaces, tabs, commas or \`=' characters.
The details of the input expected is given in the documentation for each
program, however there are some general rules:

-  Only the first four characters of keywords are significant (although
   you are recommended to use complete keywords) and they are
   case-insensitive.
-  Records may be continued across line breaks using **&**, **-** or
   **/** as the last non-blank, non-comment character on the line to be
   continued.
-  Text following a non-quoted **!** or **#** is treated as a comment
   and ignored. A continuation character may precede the comment.
-  Strings may be single- or double-quoted or unquoted if they don't
   contain any delimiter characters mentioned above or if the whole of
   the rest of the record is read as a single string.
-  Leading spaces are ignored.
-  If an input record is of the form ``@``\ *<filename>*, with possible
   surrounding whitespace, the contents of the file *<filename>* will be
   included as input at that point, after which the following records
   will be read.

Command line options
--------------------

 -n
    Do *not* read the global files **default.def** and **environ.def**.
 -v level
    Verbose output; the amount depends on the value of *level*, which
    may have a value between 0 and 9, higher values giving more
    information. This mainly affects output from the CCP4 library
    routines. The *Logical\_name* / *File\_name* assignments are
    displayed as they are processed along with other debug information.
    The default value is 1. Choose 0 if you want to suppress some
    output, or 2 or higher if you want extra output.
 -d file
    Use the next argument to specify an alternative to the
    **default.def** file.
 -e file
    Use the next argument to specify an alternative to the
    **environ.def** file.
 -i
    Print CCP4 library version, program name and program version to
    standard output, and exit.
 -nohtml
    Suppresses the writing of HTML tags in the program output. This is
    an alternative to setting the CCP\_SUPPRESS\_HTML environment
    variable.
 -nosummary
    Suppresses the writing of summary tags in the program output. This
    is an alternative to setting the CCP\_SUPPRESS\_SUMMARY environment
    variable.

Appendix: Details of logical name processing
--------------------------------------------

*[The gory details in this section can be skipped by casual users.]*

The programs normally read two initialisation files (held in the
directory pointed to by environment variable **CINCL**):

-  First the file **environ.def** is read - this specifies the logical
   names that can be expected and some information about the type of
   file associated with each logical name.

-  Then **default.def** is read, which specifies some common logical
   names defined for all CCP4 processes, and the corresponding file
   names (whose path defaults to the value of environment variable
   **CLIBD** ).
   The logical names actually correspond variables in the program's
   environment; the values defined in **default.def** don't override
   existing values of the environment variables corresponding to the
   relevant logical names, so defaults may be set by the shell's
   environment which a program inherits (e.g. with **setenv** in
   **csh**, or the Bourne shell assignment and export mechanism).

-  Finally the command line arguments are parsed so that the user can
   override the defaults on the command line. Thus the value of logical
   name **FOO** will come from the following sources in order of
   precedence:

   #. The command line, e.g.:
      *program* **foo ...** *value ...*
   #. The environment, e.g. (C shell):
      **setenv FOO** *value* **# NOT using set**
      or Bourne shell:
      **FOO=**\ *value*\ **; export FOO**
   #. Values from **default.def**.

   If a logical name hasn't been defined when a program tries to use it,
   e.g. by opening **HKLIN**, the name of the variable will be used as
   its value if it is meant to define a file.

These steps are performed by the CCP4 preprocessor functions ``CCPFYP``
and ``ccp4fyp`` (for Fortran and C programs respectively). For more
information on these, see the documentation for
`CCPLIB <ccplib.html>`__.

EXAMPLES
--------

    ::

        f2mtz hklin xylose.hkl hklout xylose.mtz <<EOF
        SYMMETRY 152
        LABOUT H   K  L   F  SIGF   FreeRflag 
        CTYPE  H   H  H   F  Q      X 
         ...
        EOF

Alternatively, if the keywords are in the file **keywords** then it is
possible to simply use:

    f2mtz hklin xylose.hkl hklout xylose.mtz <keywords

See also the example scripts in the directory ``$CEXAM/unix/runnable``.

FILES
-----

| ``$CINCL/default.def``
| ``$CINCL/environ.def`` 

SEE ALSO
--------

Documentation for the program you want to run, via the `index of
programs <INDEX.html>`__.

WARNINGS
--------

If a file name ends in **.lib**, **.bes** , **.prt** or **.dic** and is
*not* a full path name (i.e. with the full directory specified in front
of the file name, such as ./foo.bar or /foo/bar.baz) then the
environment variable ``$CLIBD`` will be prepended to the file name. This
can cause files to be written in an unexpected directory.

A solution is to write (say) **SYMOP ./my\_symop.lib**, or to use a
different extension.

If a file name ends in **.scr**, and is *not* the full path name then
the environment variable ``$CCP4_SCRATCH`` will be prepended to the file
name. The extension **scr** will *in all cases* be replaced by the
process number to give the file a (reasonably) distinct name.
