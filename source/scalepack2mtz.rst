SCALEPACK2MTZ (CCP4: Supported Program)
=======================================

NAME
----

**scalepack2mtz** - converts merged scalepack output into MTZ format.

SYNOPSIS
--------

| **scalepack2mtz** **HKLIN** *foo.dat* **HKLOUT** *foo.mtz*
| [`Keyworded input <#keywords>`__]

 DESCRIPTION
------------

This program converts merged data from scalepack (which is part of the
DENZO package) into an MTZ format. The scalepack data file may or may
not contain anomalous data. After the conversion use
`TRUNCATE <truncate.html>`__ to convert the Is into Fs. Finally,
`CAD <cad.html>`__ should be run to sort the data and put it into the
correct asymmetric unit for CCP4.

 MTZ datasets and Data Harvesting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Columns in MTZ files are organised into projects, crystals and datasets.
This information is used in subsequent processing to make sensible
decisions based on which dataset a column belongs to. Project, crystal
and dataset names should be assigned using the `NAME <#name>`__ keyword.
These will be written to the output MTZ file, and be inherited by
subsequent programs. `Data harvesting <harvesting.html>`__ also uses the
project and dataset names to write out appropriate harvest files for
deposition.

 INPUT/OUTPUT FILES
-------------------

 HKLIN
    is the input ASCII scalepack file. It is assumed to start with a
    3-line header, the third line containing the cell dimensions. In the
    absence of anomalous data there should be 5 columns H, K, L, I and
    SIGI, and with anomalous data there should be 7 columns H, K, L, I+,
    SIGI+, I- and SIGI-.
 HKLOUT
    is the output MTZ file containing labels H, K, L, IMEAN, SIGIMEAN
    and if anomalous data is present I(+), SIGI(+), I(-), SIGI(-) as
    well.

 KEYWORDED INPUT
----------------

Available keywords are:

    `**ANOMALOUS** <#anomalous>`__, `**CELL** <#cell>`__,
    `**END** <#end>`__, `**MONITOR** <#monitor>`__,
    `**NAME** <#name>`__, `**REJECT** <#reject>`__,
    `**RESOLUTION** <#resolution>`__, `**SCALE** <#scale>`__,
    `**SYMMETRY** <#symmetry>`__, `**TITLE** <#title>`__,
    `**WAVE** <#wavel>`__

NAME PROJECT <pname> CRYSTAL <xname> DATASET <dname>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[Note that the keywords PNAME <pname>, XNAME <xname> and DNAME <dname>
are also available, but the NAME keyword is preferred.]

Specify the project, crystal and dataset names for the output MTZ file.
It is strongly recommended that this information is given. Otherwise,
the default project, crystal and dataset names are "unknown", "unknown"
and "unknown<ddmmyy>" respectively (where <ddmmyy> is the date, with no
spaces).

The project-name specifies a particular structure solution project, the
crystal name specifies a physical crystal contributing to that project,
and the dataset-name specifies a particular dataset obtained from that
crystal. All three should be given.

CELL <a> <b> <c> [ <alpha> <beta> <gamma> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The cell dimensions in Angstroms and degrees. The angles default to 90
degrees. If this key is omitted then the cell dimensions are taken from
the input file (normally OMIT this command).

ANOMALOUS YES \| NO
~~~~~~~~~~~~~~~~~~~

Specify whether or not input file contains anomalous data. Defaults to
YES.

MONITOR <nmon>
~~~~~~~~~~~~~~

Monitors the <nmon>th reflection and writes it to the output stream. The
default is every 1000th reflection.

REJECT <h k l>
~~~~~~~~~~~~~~

If specified then the specific hkl will not be written to the MTZ file.

RESOLUTION <maxres> <minres>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If specified then the all reflections outside the range will be excluded
from the MTZ file. The limits can be given in either order and in
Angstrom or SineSquared(theta)/LambdaSquared units. The default is that
all reflections are output.

 SCALE <scale>
~~~~~~~~~~~~~~

All intensities and their sigmas will be scaled by <scale>. The default
is 1.0.

SYMMETRY <SG name\|SG number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keyword can be given as the space group name or number. If this
keyword is not present attempt will be made to read it from input file.

TITLE <string>
~~~~~~~~~~~~~~

This title will become the title/history in the MTZ file.

WAVE <wavelength>
~~~~~~~~~~~~~~~~~

The wavelength associated with this data. (Will become compulsory)

 END
~~~~

Terminates the keyword input. Must be last keyword.

 EXAMPLES
---------

A unix example scipt is availabe in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`scalepack2mtz.exam <../examples/unix/runnable/scalepack2mtz.exam>`__

SEE ALSO
--------

`cad <cad.html>`__, `truncate <truncate.html>`__, `Data
Harvesting <harvesting.html>`__.
