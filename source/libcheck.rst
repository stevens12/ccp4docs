LIBCHECK (CCP4: supported Program)
==================================

NAME
----

**libcheck** - Generates and manages the library files which provide
complete chemical and geometric descriptions of residues and ligands
used in refinement.

SYNOPSIS
--------

-  Create a 'complete' monomer description from a 'minimal' description
-  Create a complete monomer description from an input coordinate file
-  Search the database for matching monomers based on element types and
   bonds
-  Generate PDB file with idealised coordinates and a PostScript picture
   of the monomer
-  Manage the monomer libraries - combine libraries

[`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

The refinement program `REFMAC <refmac5.html>`__ requires a complete
geometric description of all **monomers** (*i.e.* any molecular entity,
*e.g.* 'protein residue' or 'ligand') in an input structure.

A complete monomer description includes:

-  a list of atoms, with their identifier, element symbol, chemical type
   and formal charge
-  a list of covalent bonds, target values and standard deviations
-  a list of angles, target values and standard deviations
-  a list of torsion angles, target values and standard deviations
-  a list of chiral centres, with sign
-  a list of atoms which lie in a plane
-  a tree representation of the connectivity of the monomer

The VDW and ionic radii for each atom chemical type are tabulated in a
data file $CLIBD\_MON/ener\_lib.cif and the inter-monomer restraints
(*e.g.* the peptide, sugar and nucleic acid links), are defined in
$CLIBD\_MON/mon\_lib\_com.cif.

See the `full explanation of monomer
descriptions <intro_mon_lib.html>`__. There are descriptions for
commonly occurring residues and ligands in the library files in the
directory $CLIBD\_MON (see `**library
files** <intro_mon_lib.html#library_files>`__) but for any other novel
monomer the crystallographer must provide LIBCHECK with their own model
coordinates or `**minimal
description** <intro_mon_lib.html#minimal_description>`__ from which the
program can derive a `**complete
description** <intro_mon_lib.html#complete_description>`__.

CONTENTS
--------

-  `**Introduction** <#introduction>`__
-  `**What is a Monomer Description?** <intro_mon_lib.html>`__
-  `**Keyworded Input** <#keywords>`__
-  `**Keyword definitions** <#keydefn>`__
-  `**Program function** <#function>`__
-  `**Input and output files** <#files>`__
-  `**Examples** <#examples>`__

   -  `**Example of Batch file** <#examples_batch>`__
   -  `**Example of minimal description for
      monomer** <#examples_minimal>`__
   -  `**Example of complete description for
      monomer** <#examples_complete>`__

-  `**Credits** <#credits>`__

Introduction
------------

The main use of LIBCHECK is to generate a complete monomer description
from a minimal amount of chemical information. The essential minimal
information is defined in `minimal
description <intro_mon_lib.html#minimal_description>`__.

The output files will give the complete description, a set of
coordinates and a sketch of the new monomer.

The user can generate the minimal description required as input to
LIBCHECK in the following ways:

-  The easiest way to interact with LIBCHECK is via the CCP4i SKETCHER.
   This allows the user to prepare a minimal description to pass to
   LIBCHECK, either by editing a similar monomer extracted from the
   library, by drawing the monomer from scratch, or by reading a
   coordinate file and editing it appropriately. Thus SKETCHER acts as
   an interface to LIBCHECK and takes more or less the same input.
-  If the user is confident that a monomer in the library has a proper
   minimal description it can be passed straight to LIBCHECK.
-  A list of coordinates in
   `PDB <http://www.rcsb.org/pdb/docs/format/pdbguide2.2/guide2.2_frame.html>`__,
   `CIF <http://www.iucr.org/iucr-top/cif/index.html>`__ or
   `CSD <http://www.ccdc.cam.ac.uk/support/csd_doc/zdocmain.html>`__
   format, which includes all hydrogen atoms (SKETCHER cannot read CSD
   format).
-  A list of coordinates in PDB, CIF or CSD format without hydrogen
   atoms. This should be read into the CCP4i SKETCHER for the user to
   provide the bond order which is difficult to deduce from coordinates
   alone.
-  As a "minimal description" file which contains one of the required
   combinations of information (see `minimal
   description <intro_mon_lib.html#minimal_description>`__).
-  A SMILES string can be converted to a minimal description by the
   program `SMILES2DICT <smiles2dict.html>`__ (for SMILES itself, refer
   to `Daylight - SMILES
   Toolkit <http://www.daylight.com/products/smiles_kit.html>`__).

Please note that after generating a complete description from a minimal
description by any route it is essential to check the complete
description carefully since all subsequent refinement will rely on this.

Other important uses of LIBCHECK are:

-  Produce a coordinate file in PDB or CIF format with postscript
   picture from a complete monomer description. These coordinates can
   then be used in model building software.
-  Search and check the presence of a specific monomer by matching
   minimal descriptions.
-  It can do book-keeping tasks, viz. merge two library files, and
   create a complete index of all library files.

KEYWORDED INPUT
---------------

LIBCHECK was written to be used interactively, but can be used in batch.
If used interactively, the program automatically produces a batch
command file during dialogue. This feature might be useful when
repeating calculations. Keywords with short explanations are printed by
the program at the beginning of execution. In batch command files all
keywords must be preceded by an underscore (*e.g.* \_DOC).

For interactive use, you first have to answer a question:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Do you want to have FILE-DOCUMENT *makecif.doc*? <N \| Y \| A>

Default: <N>

N
    do not produce DOC-file
Y
    produce DOC-file with new contents
A
    keep old contents and add new information, *i.e.* if a file
    *makecif.doc* already exists, and the answer to question 1 is "A",
    the program will add any new information to the end of this file

The DOC-file contains the protocol of the running of the program. With
the DOC-file, the program creates a command (batch) file:
*libcheck.bat*.

After that, the rest of the keywords may be used in any order.
`**\_END** <#end>`__ must be the last keyword.

The available keywords are:

    `**\_ANGLE** <#angle>`__, `**\_DOC** <#doc>`__,
    `**\_COOR** <#coor>`__, `**\_FILE\_CIF** <#file_cif>`__,
    `**\_FILE\_CSD** <#file_csd>`__, `**\_FILE\_PDB** <#file_pdb>`__,
    `**\_FILE\_L** <#file_l>`__, `**\_FILE\_L2** <#file_l2>`__,
    `**\_FILE\_O** <#file_o>`__, `**\_HFLAG** <#hflag>`__,
    `**\_IND** <#ind>`__, `**\_LIST** <#list>`__,
    `**\_NODIST** <#nodist>`__, `**\_MON** <#mon>`__,
    `**\_REF** <#ref>`__, `**\_END** <#end>`__

Usage of LIBCHECK:
^^^^^^^^^^^^^^^^^^

#. Create a complete description from a minimal description which comes
   from a distributed library or is provided by the user. Use keywords:

       `**MON** <#mon>`__, `**ANGLE** <#angle>`__,
       `**HFLAG** <#hflag>`__, `**REF** <#ref>`__,
       `**\_FILE\_L** <#file_l>`__, `**NODIST** <#nodist>`__

#. | Generate a complete description from a set of coordinates. This
     should work if the coordinates are reliable (*e.g.* derived from an
     ultra high resolution structure or energy minimised in some way) or
     if all hydrogen atoms are defined.
   | It is recommended to access LIBCHECK via CCP4i SKETCHER to
     visualise the information in the coordinate file and to supplement
     it if necessary. If the user is confident that the input
     coordinates are good, the target values for bond lengths, bond
     angles, torsion angles, and chiral centres can be extracted from
     the coordinates, and these will override the values derived from
     $CLIBD\_MON/ener\_lib.cif. (keyword: `**COOR** <#coor>`__) The
     coordinates can be in PDB, CIF, or CSD format (keywords:
     `**FILE\_PDB** <#pdb>`__, `**FILE\_CIF** <#cif>`__,
     `**FILE\_CSD** <#csd>`__). For this option, the following keywords
     are associated with coordinates:

       `**FILE\_PDB** <#file_pdb>`__, `**FILE\_CIF** <#file_cif>`__,
       `**FILE\_CSD** <#file_csd>`__, `**COOR** <#coor>`__,

#. Check a library entry, compare with complete description. Use
   keywords:

       `**MON** <#mon>`__

#. The output is governed by these keywords:

       `**FILE\_OUT** <#file_o>`__, `**DOC** <#doc>`__,
       `**LIST** <#list>`__

#. Combining two library files:

       `**FILE\_L** <#file_l>`__, `**FILE\_L2** <#file_l2>`__

#. Create a complete index of all library entries. This is used for
   efficient search within the libraries.

       `**IND** <#ind>`__

KEYWORD DEFINITIONS
-------------------

ANGLE <0.0>
~~~~~~~~~~~

+-----------------------------------------------------------------------+------------+
| Rotation angle for PostScript picture of monomer around the X axis:   | |image0|   |
+-----------------------------------------------------------------------+------------+

COOR <N \| Y>
~~~~~~~~~~~~~

Default: <N>

**Y**
    Use observed values of target parameters for bond lengths, bond
    angles, torsion angles and chirality taken from the input coordinate
    file instead of the value from the distributed libraries. This is
    useful if the input coordinates are reliable, *e.g.* from an ultra
    high resolution structure or energy minimised.

FILE\_CIF <filename>
~~~~~~~~~~~~~~~~~~~~

FILE\_CSD <filename>
~~~~~~~~~~~~~~~~~~~~

FILE\_PDB <filename>
~~~~~~~~~~~~~~~~~~~~

Coordinates input in mmCIF, CSD or PDB format.

FILE\_L <filename>
~~~~~~~~~~~~~~~~~~

Additional input library file. The distribution files are read by
default - only user supplied file needs to be specified with this
keyword.

FILE\_L2 <filename>
~~~~~~~~~~~~~~~~~~~

Combine the library file <filename> with the file specified by
`FILE\_L <#file_l>`__. The program will only perform the file merging if
this option is present.

FILE\_O <filename>
~~~~~~~~~~~~~~~~~~

Overrides the default output file names so

-  The library file libcheck\_<monomer>.cif becomes
   <filename>\_<monomer>.cif
-  The coordinate file libcheck\_<monomer>.pdb becomes
   <filename>\_<monomer>.pdb
-  The postscript file libcheck\_<monomer>.ps becomes
   <filename>\_<monomer>.ps

*filename* should not contain a dot (.); the extension will be added
automatically.

HFLAG <Y \| A \| N>
~~~~~~~~~~~~~~~~~~~

Default: <Y>

**Y**
    Output the input hydrogen atoms without any modification
**A**
    Generate hydrogens in their riding positions from the atom chemical
    types
**N**
    Output a coordinate file without hydrogen atoms

IND <N \| Y>
~~~~~~~~~~~~

Default: <N>

**Y**
    Create index of *mon\_lib.cif* to output file:
    *new\_mon\_lib\_ind.cif*

LIST <M \| S \| L>
~~~~~~~~~~~~~~~~~~

Default: <M>

Controls output to the log file.

**S**
    short output
**M**
    medium output
**L**
    long output

MON <monomer>
~~~~~~~~~~~~~

[compulsory if specific monomer is to be identified]

The input <monomer> name is case sensitive, and must match the library
name exactly. For the indicated monomer output

-  a library file which contains the description - default name
   libcheck\_\ **monomer**.cif
-  a PDB file containing idealised coordinates - default name
   libcheck\_\ **monomer**.pdb
-  a PostScript file containing a picture of the monomer - default name
   libcheck\_\ **monomer**.ps

If <monomer> is \* a list of all monomers in the library is output to
the log file.

NODIST <N \| Y>
~~~~~~~~~~~~~~~

Default: <N>

**Y**
    Do not read the distributed library file - this can speed up running
    LIBCHECK if the required monomer description is in a user generated
    library file (`FILE\_L <#file_l>`__).

REF <Y \| N>
~~~~~~~~~~~~

Default: <Y>

**Y**
    An energy minimisation of E\_total for the new monomer is carried
    out in both cartesian and torsion angle space.

::

            E_total = E_bond + E_angle + E_tors + E_vdw + E_hb

                     E_bond  = sum ( Kb * (Bobs     -Bidl)**2    )
                     E_angle = sum ( Ka * (ANGLEobs -ANGLEidl)**2)
                     E_tors  = sum ( Kt * (PHIobs   -PHIidl)**2  )
                     E_vdw   = Lennard-Jones 6-12 potential.

where ``Ka``, ``Kb`` and ``Kt`` are force field constants.

END
~~~

This must be the last keyword.

This will generate a set of coordinates with ideal bonds, angles, chiral
centres and planes. A different idealisation procedure is done in
`REFMAC <refmac5.html>`__.

PROGRAM FUNCTION
----------------

Roadmap of library entry check
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When checking monomer descriptions, LIBCHECK first of all checks the
presence of the description of the specific monomer in the library
entry.

::

             +---------------------------------------+
             ! Look up the name in the library entry !
             ! Is it in the library entry ?          !
             +---------------------------------------+
                    !                     !
                 no !                     ! yes
                    v                     v
           +---------------+        +----------------------+
           ! Global search !   yes  !  Minimal description !
           !   matching ?  !--->----!    available?        !
           +---------------+        +----------------------+
                   !                   !           ! 
                no !                   ! yes       ! no
                   !                   !           v
                   v                   v           ! 
                   !                   !           !
                   !                   !           !               
            +---------------------------------+    v
            ! create new complete description !
            +---------------------------------+    !
                            !                      !
                            !                      !
                            v                      v
                         +-----------------------------+
                         !  Create coordinate file     !
                         !        Refinement           !
                         !  Create PostScript file     !
                         +-----------------------------+

Procedure to create a new complete description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The program will create a complete description and write it to the
output file. It will generate a new set of coordinates and create a
PostScript file with a picture of new monomer.

This is the procedure

#. If a minimal description is available take the list of bonds and all
   other available information available: atom chemical types, bond
   orders, chiralities and definitions of planes. If there is no minimal
   description then derive a list of bonds from input coordinates and
   take the element type for each atom.
#. Create a list of chiral centres and planes either by analysing the
   given coordinates or by using given atom chemical types and bond
   orders.
#. Define the rings from the connectivity list.
#. Define or check the atom chemical types using the list of bonds,
   rings and actual angles. The presence of hydrogens in the coordinate
   list is very helpful.
#. Check and correct the list of chiralities and planes using atom
   chemical types.
#. Correct the list of planes by trying to find common elements.
#. Create tree-like structure of the connectivity in the monomer.
#. Add hydrogen atoms to the list of atoms.
#. assign hydrogen atoms to the appropriate planes.
#. Get target values of bond lengths, angles and torsions from either
   the input coordinates or the energetic library.
#. Calculate observed values of parameters.
#. If there is no coordinate file, generate and refine coordinates.
#. Write files with a new library description, coordinates and create a
   PostScript picture.

Input and output files
----------------------

Input files
~~~~~~~~~~~

LIBCHECK will automatically read the distributed monomer library files
unless the keyword NODIST is used. The distributed files are listed in
`Introduction to monomer libraries <intro_mon_lib.html>`__.

LIBCHECK can optionally read

-  a user-generated minimal description which is in the standard monomer
   library library
-  coordinates in PDB, CIF, or CSD format

Output files
~~~~~~~~~~~~

-  a new complete description of the monomer in monomer library format
   (file extension **.lib**): this will have standard values of bonds,
   angles, torsion angles, chirality centres, planar groups either taken
   from the ener\_lib.lib or from the input coordinate values
-  atomic coordinates derived from the complete description of the
   monomer; the structure has been refined using both coordinate
   geometry and torsion angle definitions.
-  a PostScript file with pictures of the monomer

EXAMPLES
--------

Example of Batch file
~~~~~~~~~~~~~~~~~~~~~

::

    # --------------------------------
    libcheck <<stop
    # --------------------------------
    # first line :   "_DOC  <N>,Y,A "
    #   N - means without DOC-file: "libcheck.doc"
    #   Y - create new file or rewrite if it is old file
    #   A - means to keep old contents and add new information
    #
    _DOC  y
    #
    #
    #      Keywords:
    #
    _MON  j1
    _FILE_L  j1min.lib
    _file_o j1new
    _END
    stop

Example of minimal description in j1min.lib
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    data_comp_list
    loop_
    _chem_comp.id
    _chem_comp.name
    _chem_comp.group
    _chem_comp.desc_level
    j1   'j1'  'non-polymer'   M
    #
    data_comp_j1
    #
    loop_
    _chem_comp_atom.comp_id                 
    _chem_comp_atom.atom_id                 
    _chem_comp_atom.type_symbol
     j1  P  P
     j1  OPP  O
     j1  O1P  O
     j1  O2P  O
     j1  O5*  O
     j1  N1   N
     j1  C6   C
     j1  C2   C
     j1  O2   O
     j1  N3   N
     j1  C4   C
     j1  O4   O
     j1  C5   C
     j1  C5A  C
     j1  C2*  C
     j1  C5*  C
     j1  C4*  C
     j1  O4*  O
     j1  C1*  C
     j1  C3*  C
     j1  O3*  O
    loop_
    _chem_comp_bond.comp_id                 
    _chem_comp_bond.atom_id_1               
    _chem_comp_bond.atom_id_2
    _chem_comp_bond.type
     j1  P   OPP  single
     j1  P   O1P  double
     j1  P   O2P  single
     j1  P   O5*  single
     j1  O5* C5*  single
     j1  N1  C6   single
     j1  N1  C2   single
     j1  N1  C1*  single
     j1  C6  C5   double
     j1  C2  O2   double
     j1  C2  N3   single
     j1  N3  C4   single
     j1  C4  O4   double
     j1  C4  C5   single
     j1  C5  C5A  single
     j1  C2* C1*  single
     j1  C2* C3*  single
     j1  C5* C4*  single
     j1  C4* O4*  single
     j1  C4* C3*  single
     j1  O4* C1*  single
     j1  C3* O3*  single
    loop_
    _chem_comp_chir.comp_id
    _chem_comp_chir.id
    _chem_comp_chir.atom_id_centre
    _chem_comp_chir.atom_id_1
    _chem_comp_chir.atom_id_2
    _chem_comp_chir.atom_id_3
    _chem_comp_chir.volume_sign
     j1       chir_01  C4*  C5*  O4*  C3*     positiv

Example of complete description in j1new.lib
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    data_comp_list
    loop_
    _chem_comp.id
    _chem_comp.three_letter_code
    _chem_comp.name
    _chem_comp.group
    _chem_comp.number_atoms_all
    _chem_comp.number_atoms_nh
    _chem_comp.desc_level
    j1       j1  'j1                                  ' non-polymer        36  21 .
    #
    # --- DESCRIPTION OF MONOMERS ---
    #
    data_comp_j1
    #
    loop_
    _chem_comp_atom.comp_id
    _chem_comp_atom.atom_id
    _chem_comp_atom.type_symbol
    _chem_comp_atom.type_energy
    _chem_comp_atom.partial_charge
     j1            O4   O    O         0.000
     j1            C4   C    CR6       0.000
     j1            N3   N    NR16      0.000
     j1            HN3  H    HNR6      0.000
     j1            C2   C    CR6       0.000
     j1            O2   O    O         0.000
     j1            C5   C    CR6       0.000
     j1            C5A  C    CH3       0.000
     j1            H5A3 H    HCH3      0.000
     j1            H5A2 H    HCH3      0.000
     j1            H5A1 H    HCH3      0.000
     j1            C6   C    CR16      0.000
     j1            H6   H    HCR6      0.000
     j1            N1   N    NR6       0.000
     j1            C1*  C    CH1       0.000
     j1            H1*  H    HCH1      0.000
     j1            C2*  C    CH2       0.000
     j1            H2*2 H    HCH2      0.000
     j1            H2*1 H    HCH2      0.000
     j1            O4*  O    O2        0.000
     j1            C4*  C    CH1       0.000
     j1            H4*  H    HCH1      0.000
     j1            C3*  C    CH1       0.000
     j1            H3*  H    HCH1      0.000
     j1            O3*  O    OH1       0.000
     j1            HO3* H    HOH1      0.000
     j1            C5*  C    CH2       0.000
     j1            H5*1 H    HCH2      0.000
     j1            H5*2 H    HCH2      0.000
     j1            O5*  O    O2        0.000
     j1            P    P    P         0.000
     j1            O1P  O    OP        0.000
     j1            O2P  O    OH1       0.000
     j1            HO2P H    HOH1      0.000
     j1            OPP  O    OH1       0.000
     j1            HOPP H    HOH1      0.000
    loop_
    _chem_comp_tree.comp_id
    _chem_comp_tree.atom_id
    _chem_comp_tree.atom_back
    _chem_comp_tree.atom_forward
    _chem_comp_tree.connect_type
     j1       O4   n/a  C4   START
     j1       C4   O4   C5   .
     j1       N3   C4   C2   .
     j1       HN3  N3   .    .
     j1       C2   N3   O2   .
     j1       O2   C2   .    .
     j1       C5   C4   C6   .
     j1       C5A  C5   H5A1 .
     j1       H5A3 C5A  .    .
     j1       H5A2 C5A  .    .
     j1       H5A1 C5A  .    .
     j1       C6   C5   N1   .
     j1       H6   C6   .    .
     j1       N1   C6   C1*  .
     j1       C1*  N1   O4*  .
     j1       H1*  C1*  .    .
     j1       C2*  C1*  H2*1 .
     j1       H2*2 C2*  .    .
     j1       H2*1 C2*  .    .
     j1       O4*  C1*  C4*  .
     j1       C4*  O4*  C5*  .
     j1       H4*  C4*  .    .
     j1       C3*  C4*  O3*  .
     j1       H3*  C3*  .    .
     j1       O3*  C3*  HO3* .
     j1       HO3* O3*  .    .
     j1       C5*  C4*  O5*  .
     j1       H5*1 C5*  .    .
     j1       H5*2 C5*  .    .
     j1       O5*  C5*  P    .
     j1       P    O5*  OPP  .
     j1       O1P  P    .    .
     j1       O2P  P    HO2P .
     j1       HO2P O2P  .    .
     j1       OPP  P    HOPP .
     j1       HOPP OPP  .    END
     j1       N1   C2   .    ADD
     j1       C2*  C3*  .    ADD
    loop_
    _chem_comp_bond.comp_id
    _chem_comp_bond.atom_id_1
    _chem_comp_bond.atom_id_2
    _chem_comp_bond.type
    _chem_comp_bond.value_dist
    _chem_comp_bond.value_dist_esd
     j1       OPP  P       single      1.699    0.020
     j1       O1P  P       double      1.610    0.020
     j1       O2P  P       single      1.699    0.020
     j1       P    O5*     single      1.610    0.020
     j1       O5*  C5*     single      1.426    0.020
     j1       N1   C6      single      1.337    0.020
     j1       N1   C2      single      1.350    0.020
     j1       C1*  N1      single      1.465    0.020
     j1       C6   C5      double      1.390    0.020
     j1       O2   C2      double      1.330    0.020
     j1       C2   N3      single      1.337    0.020
     j1       N3   C4      single      1.337    0.020
     j1       C4   O4      double      1.330    0.020
     j1       C5   C4      single      1.384    0.020
     j1       C5A  C5      single      1.506    0.020
     j1       C2*  C1*     single      1.524    0.020
     j1       C2*  C3*     single      1.524    0.020
     j1       C5*  C4*     single      1.524    0.020
     j1       C4*  O4*     single      1.426    0.020
     j1       C3*  C4*     single      1.524    0.020
     j1       O4*  C1*     single      1.426    0.020
     j1       O3*  C3*     single      1.432    0.020
     j1       HOPP OPP     single      0.967    0.020
     j1       HO2P O2P     single      0.967    0.020
     j1       H6   C6      single      1.083    0.020
     j1       HN3  N3      single      1.040    0.020
     j1       H5A1 C5A     single      1.059    0.020
     j1       H5A2 C5A     single      1.059    0.020
     j1       H5A3 C5A     single      1.059    0.020
     j1       H2*1 C2*     single      1.092    0.020
     j1       H2*2 C2*     single      1.092    0.020
     j1       H5*1 C5*     single      1.092    0.020
     j1       H5*2 C5*     single      1.092    0.020
     j1       H4*  C4*     single      1.099    0.020
     j1       H1*  C1*     single      1.099    0.020
     j1       H3*  C3*     single      1.099    0.020
     j1       HO3* O3*     single      0.967    0.020
    loop_
    _chem_comp_angle.comp_id
    _chem_comp_angle.atom_id_1
    _chem_comp_angle.atom_id_2
    _chem_comp_angle.atom_id_3
    _chem_comp_angle.value_angle
    _chem_comp_angle.value_angle_esd
     j1       O4   C4   N3    120.000    3.000
     j1       O4   C4   C5    120.000    3.000
     j1       N3   C4   C5    120.000    3.000
     j1       C4   N3   HN3   120.000    3.000
     j1       C4   N3   C2    120.000    3.000
     j1       HN3  N3   C2    120.000    3.000
     j1       N3   C2   O2    120.000    3.000
     j1       N3   C2   N1    120.000    3.000
     j1       O2   C2   N1    120.000    3.000
     j1       C4   C5   C5A   120.000    3.000
     j1       C4   C5   C6    120.000    3.000
     j1       C5A  C5   C6    120.000    3.000
     j1       C5   C5A  H5A3  109.470    3.000
     j1       C5   C5A  H5A2  109.470    3.000
     j1       C5   C5A  H5A1  109.470    3.000
     j1       H5A3 C5A  H5A2  109.470    3.000
     j1       H5A3 C5A  H5A1  109.470    3.000
     j1       H5A2 C5A  H5A1  109.470    3.000
     j1       C5   C6   H6    120.000    3.000
     j1       C5   C6   N1    120.000    3.000
     j1       H6   C6   N1    120.000    3.000
     j1       C6   N1   C1*   120.000    3.000
     j1       C6   N1   C2    120.000    3.000
     j1       C1*  N1   C2    120.000    3.000
     j1       N1   C1*  H1*   109.470    3.000
     j1       N1   C1*  C2*   109.470    3.000
     j1       N1   C1*  O4*   109.470    3.000
     j1       H1*  C1*  C2*   108.340    3.000
     j1       H1*  C1*  O4*   109.470    3.000
     j1       C2*  C1*  O4*   109.470    3.000
     j1       C1*  C2*  H2*2  109.470    3.000
     j1       C1*  C2*  H2*1  109.470    3.000
     j1       C1*  C2*  C3*   111.000    3.000
     j1       H2*2 C2*  H2*1  107.900    3.000
     j1       H2*2 C2*  C3*   109.470    3.000
     j1       H2*1 C2*  C3*   109.470    3.000
     j1       C1*  O4*  C4*   111.800    3.000
     j1       O4*  C4*  H4*   109.470    3.000
     j1       O4*  C4*  C3*   109.470    3.000
     j1       O4*  C4*  C5*   109.470    3.000
     j1       H4*  C4*  C3*   108.340    3.000
     j1       H4*  C4*  C5*   108.340    3.000
     j1       C3*  C4*  C5*   111.000    3.000
     j1       C4*  C3*  H3*   108.340    3.000
     j1       C4*  C3*  O3*   109.470    3.000
     j1       C4*  C3*  C2*   111.000    3.000
     j1       H3*  C3*  O3*   109.470    3.000
     j1       H3*  C3*  C2*   108.340    3.000
     j1       O3*  C3*  C2*   109.470    3.000
     j1       C3*  O3*  HO3*  109.470    3.000
     j1       C4*  C5*  H5*1  109.470    3.000
     j1       C4*  C5*  H5*2  109.470    3.000
     j1       C4*  C5*  O5*   109.470    3.000
     j1       H5*1 C5*  H5*2  107.900    3.000
     j1       H5*1 C5*  O5*   109.470    3.000
     j1       H5*2 C5*  O5*   109.470    3.000
     j1       C5*  O5*  P     120.500    3.000
     j1       O5*  P    O1P   108.200    3.000
     j1       O5*  P    O2P   109.500    3.000
     j1       O5*  P    OPP   109.500    3.000
     j1       O1P  P    O2P   109.500    3.000
     j1       O1P  P    OPP   109.500    3.000
     j1       O2P  P    OPP   109.500    3.000
     j1       P    O2P  HO2P  120.000    3.000
     j1       P    OPP  HOPP  120.000    3.000
    loop_
    _chem_comp_tor.comp_id
    _chem_comp_tor.id
    _chem_comp_tor.atom_id_1
    _chem_comp_tor.atom_id_2
    _chem_comp_tor.atom_id_3
    _chem_comp_tor.atom_id_4
    _chem_comp_tor.value_angle
    _chem_comp_tor.value_angle_esd
    _chem_comp_tor.period
     j1       CONST_1  O4   C4   N3   C2     180.000    0.000   0
     j1       CONST_2  C4   N3   C2   O2     180.000    0.000   0
     j1       CONST_3  C4   N3   C2   N1       0.000    0.000   0
     j1       CONST_4  O4   C4   C5   C6     180.000    0.000   0
     j1       var_1    C4   C5   C5A  H5A1   180.000   20.000   1
     j1       CONST_5  C4   C5   C6   N1       0.000    0.000   0
     j1       CONST_6  C5   C6   N1   C1*    180.000    0.000   0
     j1       CONST_7  C5   C6   N1   C2       0.000    0.000   0
     j1       var_2    C6   N1   C1*  O4*    180.000   20.000   1
     j1       var_3    N1   C1*  C2*  C3*    180.000   20.000   3
     j1       var_4    N1   C1*  O4*  C4*    180.000   20.000   1
     j1       var_5    C1*  O4*  C4*  C5*    180.000   20.000   1
     j1       var_6    O4*  C4*  C3*  O3*    180.000   20.000   3
     j1       var_7    O4*  C4*  C3*  C2*      0.000   20.000   3
     j1       var_8    C4*  C3*  O3*  HO3*   180.000   20.000   1
     j1       var_9    O4*  C4*  C5*  O5*    180.000   20.000   3
     j1       var_10   C4*  C5*  O5*  P      180.000   20.000   1
     j1       var_11   C5*  O5*  P    OPP    180.000   20.000   1
     j1       var_12   O5*  P    O2P  HO2P   180.000   20.000   1
     j1       var_13   O5*  P    OPP  HOPP   180.000   20.000   1
    loop_
    _chem_comp_chir.comp_id
    _chem_comp_chir.id
    _chem_comp_chir.atom_id_centre
    _chem_comp_chir.atom_id_1
    _chem_comp_chir.atom_id_2
    _chem_comp_chir.atom_id_3
    _chem_comp_chir.volume_sign
     j1       chir_01  C4*  C5*  O4*  C3*     positiv
     j1       chir_02  C1*  N1   C2*  O4*     positiv
     j1       chir_03  C3*  C2*  C4*  O3*     positiv
    loop_
    _chem_comp_plane_atom.comp_id
    _chem_comp_plane_atom.plane_id
    _chem_comp_plane_atom.atom_id
    _chem_comp_plane_atom.dist_esd
     j1       plan-1    N1      0.020
     j1       plan-1    C6      0.020
     j1       plan-1    C2      0.020
     j1       plan-1    C1*     0.020
     j1       plan-1    N3      0.020
     j1       plan-1    C4      0.020
     j1       plan-1    C5      0.020
     j1       plan-1    O4      0.020
     j1       plan-1    H6      0.020
     j1       plan-1    HN3     0.020

AUTHOR
------

A.A.Vagin, alexei@ysbl.york.ac.uk

CREDITS
~~~~~~~

Garib Murshudov, Eleanor Dodson, Maria Turkenburg, Liz Potterton, Kim
Henrick

.. |image0| image:: images/xaxistr.gif

