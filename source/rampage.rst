RAMPAGE (CCP4: Supported Program)
=================================

NAME
----

**rampage** - Ramachandran plots using the Richardsons' data.

SYNOPSIS
--------

| **rapper $CCP4/share/rapper/params.xml rampage** **--pdb** *filename*
  **--rampage-postscript** *filename* **--rapper-dir** *directoryname*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

RAMPAGE is an offshoot of RAPPER which generates a Ramachandran plot
using data derived by the Richardsons and coworkers. It is recommended
that it be used for this purpose in preference to PROCHECK, which is
based on much older data.

The Ramachandran diagram plots phi versus psi dihedral angles for each
residue in the input pdb file. The diagram is divided into favoured,
allowed and disallowed regions, whose contouring is based on density
dependent smoothing for 81234 non-Glycine, non-Proline and
non-preProline residues with *B* < 30 from 500 high-resolution protein
structures. Regions are also defined for Glycine, Proline and
preProline.

INPUT/OUTPUT FILES
------------------

    .. rubric:: --pdb *filename*
       :name: pdb-filename

    (Compulsory) Input pdb file name.

    .. rubric:: --rampage-postscript *filename*
       :name: rampage-postscript-filename

    Output postscript file name.

KEYWORDED INPUT
---------------

    .. rubric:: -rapper-dir *directory name*
       :name: rapper-dir-directory-name

    Name of the directory containing the data library. This should
    normally be set to $CCP4/user/share.

REFERENCES
----------

#. S.C. Lovell, I.W. Davis, W.B. Arendall III, P.I.W. de Bakker, J.M.
   Word, M.G. Prisant, J.S. Richardson, D.C. Richardson, Structure
   validation by Calpha geometry: phi,psi and Cbeta deviation.
   *Proteins: Struct. Funct. Genet.* **50** 437-450 (2003).

AUTHORS
-------

Nicholas Furnham, Paul de Bakker, Mark DePristo, Reshma Shetty, Swanand
Gore and Tom Blundell.

SEE ALSO
--------

`RAPPER <rapper.html>`__
