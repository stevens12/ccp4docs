GETAX (CCP4: Supported Program)
===============================

NAME
----

**GETAX** - real space correlation search

--------------

PURPOSE
-------

| Real space searching for rotation axis of a D<n> or C<n> multimer (
  <n> = 2,3,4,5,6,... ).
| If you have:

a multimer in the asymmetric unit

some initial phases

a peak in a selfrotation function

| you can start using this program to find the translational part of
  your NCS operators.
| It has worked in several cases with even very poor phases ( 20
  molecules/au, <fom>=0.25 to about 6Å resolution).

--------------

VERSION
-------

Version 2.5 (14. April 1998)

--------------

SYNOPSIS
--------

| **getax MAPIN** *foo.map* [ **XYZIN** *foo1.pdb* ] [ **XYZOUT**
  *foo2.pdb* ] [ **MAPOUT** *bar.map* ]
| [`Keyworded input <#keywords>`__]

--------------

DESCRIPTION
-----------

**GETAX** is a program to search for your non-crystallographic symmetry
if a first map is available. The only knowledge you need is a
selfrotation solution (from *e.g.* `POLARRFN <polarrfn.html>`__) and a
crude knowledge of the size/shape of your molecule(s).

--------------

--------------

INPUT/OUTPUT FILES
------------------

--------------

MAPIN
^^^^^

| map covering a whole unit cell with axis order X=fast, Y=medium,
  Z=slow changing index.
| This map can be a 6 Angstroem MIR map on a 2 Angstroem grid. So if you
  use `fft <fft.html>`__ you don't have to worry about getting the grid
  right, since `fft <fft.html>`__ takes 1/3rd of the high resolution
  anyway. Sometimes it can be helpful to try finer grid spacings (this
  slows down the calculation, though).
| Depending on your spacegroup, you will have to change the extent
  and/or axis order with `MAPMASK <mapmask.html>`__.

--------------

XYZIN
^^^^^

| a PDB file with an initial model for the correlation search.
| see: `INPUT XYZ <#input>`__

--------------

XYZOUT
^^^^^^

| a PDB file with the initial search sphere/slice as build and used by
  GETAX.
| This can be a PDB file with orthogonal coordinates either before
  (`OUTPUT XYZ <#outputxyz>`__) or after initial interpolation (`OUTPUT
  GXYZ <#outputgxyz>`__).
| To be sure that everything works fine: have a look at this output file
  with your favourite graphics program (O or RASMOL or whatever ...):
  there should be no overlap between the segments and the rotation axis
  should be properly oriented.

--------------

MAPOUT
^^^^^^

is an output map with correlation coefficients at each grid point. There
are two possible correlation coefficients:

+------+-----------------------------+-----------------------------------+
| 1.   | CC                          | [`OUTPUT MAP <#outputmap>`__]     |
+------+-----------------------------+-----------------------------------+
| 2.   | CC \* ( 1.0 - sd(CC)/CC )   | [`OUTPUT SMAP <#outputsmap>`__]   |
+------+-----------------------------+-----------------------------------+

This is the most important output: you can read it into PEAKMAX to
extract peak positions which correspond to centers of your rotation
axis. Or even better: use your favourite graphics program (*e.g.* O) and
look for high peaks and/or long stretches of "density".

--------------

--------------

KEYWORDED INPUT
---------------

Available keywords are:

    `**CHECK** <#check>`__, `**END** <#end>`__, `**INPUT** <#input>`__,
    `**MINDEN** <#minden>`__, `**ORTHO** <#ortho>`__,
    `**OUTPUT** <#output>`__, `**POLAR** <#polar>`__,
    `**REPORT** <#report>`__, `**SKIP** <#skip>`__,
    `**SLICE** <#slice>`__, `**SPHERE** <#sphere>`__,
    `**STEP** <#step>`__, `**XYZLIMIT** <#xyzlimit>`__

--------------

COMPULSORY KEYWORDS:
~~~~~~~~~~~~~~~~~~~~

POLAR <omega> <phi> <kappa> [<omega-2> <phi-2> [<kappa-2>] ]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| Polar angles of a selfrotation solution (definition as in
  `POLARRFN <polarrfn.html>`__).
| Combining two selfrotation solutions:
| If you have a twofold perpendicular to your rotation axis (*e.g.* D4
  symmetry) you can give the polar angles as <omega-2> and <phi-2>
  (<kappa-2> defaults to 180.0). A corresponding sphere/disk will be
  built. The program stops if the two rotations aren't perpendicular. If
  they are perpendicular within an error of 5 degrees, the program
  calclulates a new 2-fold which now is exactly perpendicular, thus
  correcting possible rounding errors of *e.g.*
  `POLARRFN <polarrfn.html>`__.

--------------

ADDITIONAL KEYWORDS:
~~~~~~~~~~~~~~~~~~~~

--------------

ORTHO <ncode>
^^^^^^^^^^^^^

| Polar angles given on POLAR card are for orthogonalization code
  <ncode>.
| ncode = orthogonalization code:

<ncode> =1: axes along a, c\* x a, c\* (Brookhaven standard, default)

<ncode> =2: axes along b, a\* x b, a\*

<ncode> =3: axes along c, b\* x c, b\*

<ncode> =4: axes along a+b, c\* x (a+b), c\*

<ncode> =5: axes along a\*, c x a\*, c ( Rollett )

<ncode> =6: axes along a, b\*, a x b\*

<ncode> =7: axes along a\*, b, a\* x b (TNT convention)

--------------

SPHERE <outer-radius> [<inner-radius>]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| defines a spherical shape of your multimer.
| Builds a sphere with radius <outer-radius>. You can omit a smaller
  inner sphere by giving <inner-radius>.
| The sphere will be divided into <ifold> segments (where <ifold> is
  determined by <kappa>) and rotated so that its rotation axis is
  parallel with the selfrotation axis and its center is at (0 0 0). You
  can write this sphere out to logical `XYZOUT <#xyzout>`__.
| To get a rough idea what your protein looks like: use the molecular
  weight Mr to get radius of assumed spherical protein:

::


                               1.23 * Mr * 0.75
                   radius =  ( ---------------- ) ^ 0.333
                                      pi

default: <outer-radius>=25. <inner-radius>=0.

--------------

SLICE <outer-radius> <height> [<inner-radius>]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| defines a different shape of your multimer.
| Builds a disk with outer radius <outer-radius> and height <height>.
  You can omit a smaller inner circle by giving <inner-radius>.
| The disk will be divided into <ifold> segments (where <ifold> is
  determined by <kappa>) and rotated so that its rotation axis is
  parallel with the selfrotation axis and its center is at (0 0 0). You
  can write this disk out to logical `XYZOUT <#xyzout>`__.
| default: <outer-radius>=25. <height>=15. <inner-radius>=0.

--------------

CHECK [[NO]CORR] [[NO]PACK] [[N]AX1/[N]AX2/[N]AX3/[N]AX4]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| which checks to perform:

| **[NO]CORR**
| [do not] calculate correlation coefficient at each search position

| **[NO]PACK**
| [do not] calculate amount of overlapping of all segments after
  applying all symmetry operators.
| This didn't help a lot in my cases, but it's perhaps worth trying if
  your multimer covers a whole asymmetric unit.

| **[[N]AX1/[N]AX2/[N]AX3/[N]AX4]**
| after getting the correlation at each grid point, this correlation map
  is [not] searched for long stretches of high correlation by using
  points on the rotation axis (defined with keyword `POLAR <#polar>`__).
| different weighting schemes are available:

AX1: CC = <CC>\_axis(i)

AX2: CC = CC \* <CC>\_axis(i)

AX3: CC = CC \* ( <CC>\_axis(i) / CCmax )

AX4: CC = CC \* ( <CC>\_axis(j) / CCmax )

where:

+-----------------+-----+-----------------------------------------------------------------------------------------------------------------------------+
| CC              | =   | correlation at position i                                                                                                   |
+-----------------+-----+-----------------------------------------------------------------------------------------------------------------------------+
| CCmax           | =   | maximum correlation found                                                                                                   |
+-----------------+-----+-----------------------------------------------------------------------------------------------------------------------------+
| <CC>\_axis(i)   | =   | average correlation for all points on rotation axis when centred at position i                                              |
+-----------------+-----+-----------------------------------------------------------------------------------------------------------------------------+
| <CC>\_axis(j)   | =   | average correlation for all points i on rotation axis when centred at position j and weighted by 1/distance to position i   |
+-----------------+-----+-----------------------------------------------------------------------------------------------------------------------------+

| AX2 and AX3 don't make any difference in the result (but AX3 keeps the
  absolute values of the output correlation map at a reasonable height).
| defaults: CORR NOPACK AX4

--------------

SKIP [AUTO <askip>]/[<iskip>]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| Saves CPU time by using only a limit number of the points describing a
  sphere/slice.
| Takes only every <iskip>th point of each segment in your sphere/disk
  to compute correlation coefficients. This is a good idea if your
  sphere/disk is rather big. It can save a lot of CPU. But take care
  that you keep at least ~500 points in each segment.
| If keyword AUTO is present, the actual value of iskip is set so that
  approximately <askip> points per segment are used.
| default: AUTO 500

--------------

STEP <istep>
^^^^^^^^^^^^

| Step along each cell axis (in grid units).
| Unless you have calculated your map on a very fine grid, it does make
  things worse. And perhaps you'll miss the right solution !! It doesn't
  save a lot of CPU, since we have to interpolate the values at the end
  anyway.
| default: <istep>=1

--------------

MINDEN <minden>
^^^^^^^^^^^^^^^

| Correlation coefficients will only be calculated if the density for
  all segments in the sphere/disk is .gt. <minden>\*sigma.
| The default is also a very reasonable value.
| default: <minden>=-999.

--------------

XYZLIMIT <xmin> <xmax> <ymin> <ymax> <zmin> <zmax>
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| Limits (in grid points) for search.
| Unless you know already where to look for your multimer, I would
  always search the whole unit cell.
| default: whole unit cell

--------------

OUTPUT [XYZ/GXYZ] [MAP/NOMAP] [SMAP]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

MAP/SMAP = output MAPOUT

MAP = map with CC at each grid point

SMAP = map with CC\*(1.-sd(CC)/CC) at each grid point (probably only
useful with high symmetries: 4-fold,6-fold,D4,...)

NOMAP = no MAPOUT

XYZ/GXYZ = output XYZOUT

XYZ = orthogonal coordinates before interpolation

GXYZ = orthogonal coordinates after interpolation

default: MAP

--------------

INPUT XYZ
^^^^^^^^^

| read in PDB file to define the shape of your molecules.
| If you have a pretty good idea what your molecule looks like and how
  it is oriented (but not positioned) this could be quite helpful. But
  some restrictions:

different molecules/segments have to have different chain-ids

for each chain id there should be EXACTLY the same amount of atoms in
exactly the same order

the multimer should be centred at the origin

--------------

REPORT <report> <top>
^^^^^^^^^^^^^^^^^^^^^

| Not only reports the maximum correlation found so far, but also every
  correlation .gt. <report>.
| At the end of the search the found correlations are sorted according
  to height and the <top> number is reported.
| default: <report>=1. <top>=20

--------------

END
^^^

Terminates input.

--------------

EXAMPLES
--------

A unix example script for performing a simple NCS search can be found in
$CEXAM/unix/non-runnable/

-  `getax-ncs-search <../examples/unix/non-runnable/getax-ncs-search>`__

though it will need to be edited before use.

Other examples:
~~~~~~~~~~~~~~~

| 1. simple 2-fold

::

          getax mapin mlphare_6.0.map \
                mapout getax_sphere.map \
                <<end_ip >getax_sphere.log
          POLAR 51.7 90 180
          SPHERE 25.0
          END
          end_ip

          peakmax mapin getax_sphere.map \
                  <<end_ip >getax_sphere.peakmax
          THRE RMS 4
          NUMP 100
          OUTP NONE
          end_ip
          

2. D4 symmetry

::

          getax mapin mlphare_6.0.map \
                mapout getax_slice.map \
                <<end_ip >getax_slice.log
          POLAR 48.7 116.7 90.0 90.0 28.4 180.0
          SKIP AUTO 1000
          SLICE 25.0 15.0 5.0
          REPORT 0.100
          CHECK NAX4
          END
          end_ip

          peakmax mapin getax_sphere.map \
                  <<end_ip >getax_sphere.peakmax
          THRE RMS 4
          NUMP 100
          OUTP NONE
          end_ip
          

--------------

AUTHOR
------

Clemens Vonrhein

REFERENCES
----------

#. C. Vonrhein and G. E. Schulz, *Acta Cryst.*, **D55**, 225 - 229
   (1999)
   Locating proper non-crystallographic symmetry in low-resolution
   electron-density maps with the program GETAX.

--------------

SEE ALSO
--------

`fft <fft.html>`__\ (1), `mapmask <mapmask.html>`__\ (1),
`peakmax <peakmax.html>`__\ (1), `dm <dm.html>`__\ (1),
`ncsmask <ncsmask.html>`__\ (1).

--------------

Last modification: 24.10.2013
