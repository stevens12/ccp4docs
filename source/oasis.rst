OASIS (CCP4: Supported Program)
===============================

NAME
----

**oasis** - a direct-methods program for SAD/SIR phasing and
reciprocal-space fragment extension

SYNOPSIS
--------

| **oasis HKLIN** *foo\_in.mtz* **HKLOUT** *foo\_out.mtz* [**XYZIN**
  *foo\_in.pdb*]
| [`Keyworded input <#keywords>`__]

CONTENTS
--------

#. `DESCRIPTION <#description>`__
#. `INPUT AND OUTPUT FILES <#files>`__
#. `KEYWORDED INPUT <#keywords>`__
#. `EXAMPLES <#examples>`__
#. `AUTHORS <#authors>`__
#. `REFERENCES <#references>`__

| 

DESCRIPTION
-----------

OASIS-2006 is a computer program for *ab initio* SAD/SIR phasing and
reciprocal-space fragment extension. The phase problem is reduced to a
sign problem once the anomalous-scatterer or the heavy-atom substitution
sites are located. The sign problem is then solved by a direct method.
OASIS-2006 also performs reciprocal-space fragment extension **with** or
**without** SAD/SIR information. This is particularly useful when the
total contribution of known fragments is not large enough for Fourier
recycling.

INPUT AND OUTPUT FILES
----------------------

    HKLIN

        Input data file (CCP4 mtz format).

    | XYZIN (optional)

        This is a .pdb file for providing partial structure information,
        which is usually obtained from automatic model building
        programs. The file should contain CRYST and SCALE information at
        the beginning. Dummy atoms will be rejected in subsequent
        processing.

    | FRCIN (optional)

        This file contains partial structure information extracted from
        a \*.pdb file using the script *pdb2frc.csh*. Atomic positional
        parameters are converted to fractional coordinates. Dummy atoms
        will be reserved and assigned as carbon atoms.

    HKLOUT

    Output data file (CCP4 mtz format).

KEYWORDED INPUT
---------------

The possible keywords are:

    **`TIT <#tit>`__,** **`SAD/SIR/DMR <#running_mode>`__**,
    **`LABIN <#labin>`__**, **`CON <#con>`__**, **`COS <#cos>`__**,
    **`FOM <#fom>`__**, **`AOE <#aoe>`__**, **`KMI <#kmi>`__**,
    **`CYC <#cyc>`__**, **`ANO <#ano>`__**, **`POS <#pos>`__**,
    **`FRA <#fra>`__**, **`NHA <#nha>`__**, **`SED <#sed>`__**,
    **`LIM <#lim>`__**

| 

TIT <title string>
~~~~~~~~~~~~~~~~~~

| (optional)
| This keyword creates a job-title string, which consists of up to 80
  characters.

SAD/SIR/DMR
~~~~~~~~~~~

| (compulsory)
| This specifies the calculation mode:

    \ **SAD**

        *Ab initio* phasing or fragment extension with SAD data

    \ **SIR**

        *Ab initio* phasing or fragment extension with SIR data

    \ **DMR**

        Fragment extension **without** SAD/SIR information. This can be
        used as **D**\ irect-method **MR**-model completion with native
        data in the absence of SAD signals.

LABIN FP=... SIGFP=... [FPH=... SIGFPH=...] DANO=... SIGDANO=... [PHIC=...]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(compulsory)

    FP, SIGFP:

        averaged structure-factor magnitude [F(+) + F(-)]/2 and its
        standard deviation in `**SAD** <#sad>`__ mode;
        Fobs of the native protein and its standard deviation in
        `**SIR** <#sir>`__ or `**DMR** <#dmr>`__ mode.

    FPH, SIGFPH:

        Fobs of the derivative of SIR data and its standard deviation.

    DANO, SIGDANO:

        Bijvoet difference, F(+) - F(-), of SAD data and its standard
        deviation.

    PHIC:

        standard phase for comparison with the resultant phase

    F(+), SIGF(+), F(-), SIGF(-):

        structure factor amplitudes for hkl and its Friedel mate -h-k-l.

| 

The acceptable formats for different calculation modes are as follows:

    **SAD:**

        LABIN FP=...  SIGFP=...  DANO=...  SIGDANO=...  [PHIC=...]
           or
        LABIN F(+)=...  SIGF(+)=...  F(-)=...  SIGF(-)=...  [PHIC=...]

    **SIR:**

        LABIN FP=...  SIGFP=...  FPH=...  SIGFPH=...  [PHIC=...]

    **DMR:**

        LABIN FP=...  SIGFP=...  [PHIC=...]

*Note: If the keyword LABIN is missing, the program will assume that SAD
data with the format
"FP=FP SIGFP=SIGFP DANO=DANO SIGDANO=SIGDANO"
are to be input. Any departures from this will result in errors.*

| 

CON <atom type> <number of atoms in ASU >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (compulsory)
| Example:

::

      CON    C 770   N 185   O 231   H 1232   CU 1   

| 
| This keyword specifies contents in the **asymmetric unit**, which can
  be approximately estimated as follows:
| Let *r* be the number of residues in the asymmetric unit, then the
  contents will be C 5\ *r*, N 1.2\ *r*, O 1.5\ *r*, H 8\ *r* plus atoms
  other than the above chemical elements.
| An alternative and more accurate way to calculate contents in
  asymmetric unit is to run the script *seq2con.csh* with the sequence
  file by issuing the command: **seq2con.csh *name*.pir**

| 

COS
~~~

| (optional)
| By default, COS(delta\_phi) values calculated from experimental
  Bijvoet differences will be modified to obey uniform distribution
  within the range of 1 to -1. This will cope with large experimental
  errors. The keyword COS informs the program NOT to fit COS(delta\_phi)
  values to uniform distribution. Instead, COS(delta\_phi) values within
  the range of 1 to -1 will be kept unchanged and, values greater than 1
  will be cut to 1 while values smaller than -1 will be set to -1. It is
  NOT recommended to use the keyword COS except when you are very
  confident about the measured Bijvoet differences.

FOM
~~~

| (optional)
| By default the output figures-of-merit for individual reflections will
  be fitted so as they are uniformly distributed from 0 to 1. Usually
  this leads to a better electron-density map for the subsequent density
  modification. The keyword FOM informs the program to turn off this
  function. This is provided for use when the user is unsatisfied with
  the default phasing result.

AOE <value>
~~~~~~~~~~~

| (optional)
| By default the average value of EXP[-(sigma\_H)\*\*2/2] in the P+
  formula will be tuned automatically to 0.5. The user can use the
  keyword AOE to set the tuned average EXP[-(sigma\_H)\*\*2/2] to other
  values within the range of 0 to 1.

KMI <value>
~~~~~~~~~~~

| (compulsory).
| This keyword specifies the minimum kappa value of three-phase
  structure invariants accepted in direct-method phasing. The smaller
  the KMI value, the larger the number of structure invariants involved
  in the phasing process and the more reliable the phasing results. A
  KMI value of 0.03 would usually lead to a satisfactory result, while a
  value of 0.01 may in most cases be even better. However, for a big
  protein, a KMI of 0.01 may cause problems in computing time. It is
  recommended to check the total number of three-phase structure
  invariants (phase relationships) output by the program and adjust the
  value of kappa minimum accordingly so as the total number of structure
  invariants is kept within the range of 10,000,000 ~ 50,000,000.

CYC <ncycle>
~~~~~~~~~~~~

| (optional)
| Number of cycles for P+ formula iteration (default: 2).

ANO <atom> <f">
~~~~~~~~~~~~~~~

| (compulsory in `**SAD** <#sad>`__ mode)
| chemical symbol of the anomalous scatterer and the corresponding f"
  value - the imaginary-part correction to the atomic scattering factor.
| Example:

::

      ANO   HG 7.686
            ZN 0.678

The script crossec.csh can help with calculating anomalous corrections,
for example the command: **crossec.csh Br 0.9191** will calculate
anomalous corrections to the atomic scattering factor of Br at the
wavelength of 0.9191Å.

| 

POS   <atom\_1>   <1>   <x\_1>   <y\_1>   <z\_1>     <occupancy\_1>   <B-factor\_1>
                .
                .
                .
          <atom\_n>   <n>   <x\_n>   <y\_n>   <z\_n>     <occupancy\_n>   <B-factor\_n>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (compulsory in `**SAD** <#sad>`__/`**SIR** <#sir>`__ mode)
| Fractional coordinates of anomalous scatterer(s) or replacement heavy
  atom(s) in the **asymmetric unit**.

| 

FRA   <atom\_1>   <1>   <x\_1>   <y\_1>   <z\_1>     <occupancy\_1>   <B-factor\_1>
                .
                .
                .
          <atom\_n>   <n>   <x\_n>   <y\_n>   <z\_n>     <occupancy\_n>   <B-factor\_n>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (optional)
| This keyword specifies atomic parameters of the known fragment(s) (in
  fractional coordinates in the **asymmetric unit**) for
  partial-structure iteration when the PDB file is not provided.

NHA <value>
~~~~~~~~~~~

| (optional in `**DMR** <#dmr>`__ mode)
| This specifies the percentage of atoms to be included in artificial
  partial structure (default: 5).

SED <value>
~~~~~~~~~~~

| (optional in `**DMR** <#dmr>`__ mode)
| This specifies the seed value of the random-number generator used for
  creating artificial partial structure (default: 1).

LIM <dmin>
~~~~~~~~~~

| (optional)
| This specifies the high resolution cutoff in Angstroms for reflections
  involved in the calculation (default: no cutoff).
|  
|  
|  

EXAMPLES
--------

Runnable examples
~~~~~~~~~~~~~~~~~

a. Examples of SAD phasing:
^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `**Example 1: *Ab initio* SAD phasing** <#example1>`__
-  `**Example 2: Fragment extension with SAD data** <#example2>`__

b. Example of MR-model completion:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  **Example 3: MR-model completion without SAD/SIR information**

| 

Example 1: [*Ab initio* SAD phasing]
------------------------------------

::

    #!/bin/csh -f
    oasis06 hklin pscp.mtz hklout pscp_oasis_1.mtz << +
    TIT Ab initio SAD phasing of pscp
    SAD
    LABIN FP=FP SIGFP=SIGFP DANO=DANO SIGDANO=SIGDANO
    CON C 1683 N 458 O 571 BR 9 S 3
    KMI 0.03
    ANO BR 3.8143
    POS
    BR 1 0.63036 0.20655 0.00000 0.42 14.700
    BR 2 0.50000 0.00000 0.00716 0.28 14.400
    BR 3 0.14603 0.42154 0.25609 0.44 29.700
    BR 4 0.44531 0.41895 0.24849 0.19 3.700
    BR 5 0.90769 0.26779 0.02868 0.39 30.700
    BR 6 0.69134 0.20471 0.27918 0.38 30.200
    BR 7 0.59988 0.42259 0.11135 0.36 25.000
    BR 8 0.65702 0.37410 0.06040 0.33 25.500
    BR 9 0.69461 0.39047 0.25676 0.26 20.100
    +

Example 2: [Fragment extension with SAD data]
---------------------------------------------

::

    #!/bin/csh -f
    oasis06 hklin azurin.mtz frcin azurin_1.frc hklout azurin_1_oasis.mtz << +
    TIT Fragment extension with azurin SAD data
    SAD
    LABIN FP=FP SIGFP=SIGFP DANO=DANO SIGDANO=SIGDANO
    CON C 600 N 165 O 185 S 10 CU 1
    ANO CU 2.168
    POS
       CU 1 0.1401 0.8162 -0.0021 1.0 11.7852
    KMI 0.02
    +

Example 3: [MR-model completion without SAD/SIR information]
------------------------------------------------------------

::

    #!/bin/csh -f
    oasis06 hklin 0_start.mtz frcin 0_start.frc hklout 1ujz_1_oasis.mtz << +
    TIT MR-model completion of 1ujz
    DMR
    NHA 5
    SED 9
    LABIN FP=FP SIGFP=SIGFP
    CON C 1086 N 312 O 334 S 3
    COS
    FOM
    KMI 0.05
    +

AUTHORS
-------

| Tao Zhang, Yao He, Yuan-xin Gu, Chao-de Zheng, Jia-wei Wang & Hai-fu
  Fan
| *Beijing National Laboratory for Condensed Matter Physics,
  Institute of Physics, Chinese Academy of Sciences, Beijing 100080,
  China*
| and
| Quan Hao
| *MacCHESS, 273 Wilson Synchrotron Lab, Cornell University, Ithaca, NY
  14853-8001, U.S.A.*
| Emails: `gu@cryst.iphy.ac.cn <mailto:%20gu@cryst.iphy.ac.cn>`__;
  `fanhf@cryst.iphy.ac.cn <mailto:%20fanhf@cryst.iphy.ac.cn>`__

::

REFERENCES
----------

#. Fan, H.F. & Gu, Y.X. (1985). "Combining direct methods with
   isomorphous replacement or anomalous scattering data III. The
   incorporation of partial structure information". *Acta Cryst.*
   **A41**, 280-284.
#. Hao, Q., Gu, Y.X., Zheng, C.D. & Fan, H.F. (2000). "OASIS: a program
   for breaking phase ambiguity in OAS or SIR". *J. Appl. Cryst.*
   **33**, 980-981.
#. Wang, J.W., Chen, J.R., Gu, Y.X., Zheng, C.D. & Fan, H.F. (2004).
   "Direct-method SAD phasing with partial-structure iteration - towards
   automation". *Acta Cryst.* **D60**, 1991-1996.
#. He, Y., Yao, D.Q., Gu, Y.X., Lin, Z.J., Zheng, C.D. & Fan, H.F.
   (2007). "OASIS and MR-model completion". *Acta Cryst.* **D63** (in
   the press).
#. He, Y., Gu, Y.X., Lin, Z.J., Zheng, C.D. & Fan, H.F. (2007). "SIR
   phasing by combination of SOLVE/RESOLVE and dual-space fragment
   extension". (To be published)
#. Zhang, T., He, Y., Gu, Y.X., Zheng, C.D. Hao, Q., Wang, J.W. & Fan,
   H.F. (2007). "OASIS-2006, a direct-methods program for SAD/SIR
   phasing and reciprocal-space fragment extension". (To be published).

| 
| \* PDF files of papers in the REFERENCES are available on
  `*http://cryst.iphy.ac.cn* <http://cryst.iphy.ac.cn>`__  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
|  
