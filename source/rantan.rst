RANTAN (CCP4: Supported Program)
================================

NAME
----

**rantan** - is a Direct Method module for the determination of heavy
atom positions in a macro-molecule structure or to determine a small
molecule structure.

SYNOPSIS
--------

| **rantan hklin** *foo\_in.mtz* **hklout** *foo\_out.mtz*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

RANTAN [1] reads in the E values calculated by `ECALC <ecalc.html>`__
and the known phases with weights if they are available. The programme
determines the reflections for fixing origin and enantiomorph by
CONVERGENCE procedure from MULTAN [2]. A set of random phases with
weights (default 0.25) will be assigned to the rest of the large E
values in the starting set. The phases will be refined by the tangent
formula [3] and expanded to the whole set of large E values which are
enough to determine the positions of heavy atoms or a small molecule
structure.

The traditional figures of merit are used to indicate solutions. Up to 5
sets of refined phases and weights with the best combined figures of
merit are output.

Input phases with weights greater than 0.85 can be used as known phases
and of these those phases with weights greater than 0.98 will not be
refined until the last two cycles. These so-called "known" phases will
be combined with randomly generated phases as a starting set [4]. If
there are more than 80% of large E values with "known" phases, only one
set of refined phases will be output.

All the items in the input file will be echoed in the output file plus
the new phases and weights. For MAD data, `REVISE <revise.html>`__
should be used first to obtain the estimates of FM, from which E values
can be calculated by ECALC and input to RANTAN.

RANTAN can be used to determine a small molecule structure too, but all
the default values are for the determination of heavy atom sites in a
macro-molecule structure. When ECALC is used to calculate the E values
for a small molecule, the number of reflections in each shell in ECALC
should be smaller than default (200).

KEYWORDED INPUT
---------------

| The various data control lines are identified by keywords. Only the
  first 4 characters of a keyword are significant. The keywords can be
  in any order, except END (if present) which must be last. Numbers and
  characters in "[ ]" are optional. The only compulsory keyword is
  LABIN. The available keywords are:
| `**EMAX** <#emax>`__, `**EMIN** <#emin>`__, `**EPSI** <#epsi>`__,
  `**KMAX** <#kmax>`__, `**KMIN** <#kmin>`__, `**LABI** <#labin>`__,
  `**LABO** <#labout>`__, `**LIST** <#list>`__, `**MAXS** <#maxs>`__,
  `**NOUT** <#nout>`__, `**NRAN** <#nran>`__, `**NREF** <#nref>`__,
  `**NSET** <#nset>`__, `**NZRO** <#nzro>`__, `**RESO** <#reso>`__,
  `**SKIP** <#skip>`__, `**SWTR** <#swtr>`__, `**TITL** <#title>`__,
  `**WFOM** <#wfom>`__, `**WMIN** <#wmin>`__, `**WTRI** <#wtri>`__,
  `**END** <#end>`__.

TITLE <title>
~~~~~~~~~~~~~

(optional) 80 character title to replace old title in MTZ file.

LABIN <program label>=<file label>...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(COMPULSORY) This keyword defines which items are to be used in the
calculation. The following <program label>s can be assigned:

::

      EVAL      PHI        WT

    Example:

      LABI EVAL=EM_RE

      LABI EVAL=EM_RE PHI=AC WT=WTAC

LABOUT <program label>=<file label>...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) This keyword allows the user to assign their own labels to
the extra labels in the output file. All labels in LABIN will
automatically be in the output file. The following <program label>s can
be assigned:

::

      PHI1  WT1  PHI2  WT2  PHI3  WT3  PHI4  WT4  PHI5  WT5

    Example:

      LABO PHI1=NEWPHASE1 WT1=NEWWT1 -
           PHI2=NEWPHASE2 WT2=NEWWT2 -
           PHI3=NEWPHASE3 WT3=NEWWT3

In cases where the "known" phases are more than 80% of the number of
large E values, only one set of refined phases will be output. So only
PHI1 and WT1 can be assigned by LABOUT or do not assign any labels if
the number of "known" phases is unknown and then PHI1 and WT1 will be in
the output file.

WFOM [ABS <wabs>] [PSI <wpsi>] [RES <wres>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (optional) The weights of three figures of merit for calculating the
  combined figure of merit; the programme will normalize the values to
  obtain wabs + wpsi + wres = 1.0. If small E values to calculate
  PSIZERO are not reliable, a lower value can be given to wpsi.
| Default: wabs = 0.1, wpsi = 0.4 and wres = 0.5.

::

    Example:

      WFOM ABS 0.3 PSI 0.1 RES 0.6

EMAX <emax>
~~~~~~~~~~~

| (optional) The maximum E value to be used in RANTAN. This can be used
  to exclude particularly large E values.
| Default: emax = 5.0.

::

    Example:

      EMAX 4.5

EMIN <emin>
~~~~~~~~~~~

| (optional) The minimum E value to be used in RANTAN.
| Default: emin = 1.2.

::

    Example:

      EMIN 1.0

EPSI <epsi>
~~~~~~~~~~~

| (optional) The maximum E value to be used to calculate PSIZERO figure
  of merit.
| Default: epsi = 0.3.

::

    Example:

      EPSI 0.1

KMIN <kmin>
~~~~~~~~~~~

| (optional) The minimum KAPPA value for a triplet to be used in RANTAN.
| Default: kmin = 0.6.

::

    Example:

      KMIN 1.0

KMAX <kmax>
~~~~~~~~~~~

| (optional) The maximum KAPPA value for a triplet to be used in RANTAN.
| Default: kmax = 50.0.

::

    Example:

      KMAX 20.0

RESO <res1> <res2>
~~~~~~~~~~~~~~~~~~

| (optional) The resolution range of E values from <res1> to <res2> used
  in RANTAN.
| Default: same as the whole data set.

::

    Example:

      RESO 10.0 3.5
    or
      RESO 3.5 10.0

MAXS <maxs>
~~~~~~~~~~~

| (optional) The number of sets of random phases (number of trials). The
  maximum number is 2000.
| Default: maxs = 500.

::

    Example:

      MAXS 200

LIST
~~~~

| (optional) Print out full details about running RANTAN.
| Default: Print out normally enough information about running RANTAN.

::

    Example:

      LIST

NRAN <nran>
~~~~~~~~~~~

| (optional) The number of random phases to be assigned to the starting
  set (size of starting set).
| Default: nran = 250.

::

    Example:

      NRAN 600

NREF <nref>
~~~~~~~~~~~

| (optional) The number of large E values to be used in RANTAN. The
  maximum number is to keep <nref> + <nzro> less than 2000.
| Default: nref = 600.

::

    Example:

      NREF 800

NZRO <nzro>
~~~~~~~~~~~

| (optional) The number of small E values to be used to calculate
  PSIZERO. Normally default value is large enough.
| Default: nzro = 100.

::

    Example:

      NZRO 50

NSET <nset1> <nset2> ...
~~~~~~~~~~~~~~~~~~~~~~~~

| (optional) The particular set number assigned by user to be refined
  and output. User can use this keyword to investigate any set of phases
  and maps after the first run of RANTAN. The number must be given in
  accretion order. The maximum number of sets assigned by user is 5.
| Default: no set number assigned by user.

::

    Example:

      NSET 4 39 199 250

NOUT <nout>
~~~~~~~~~~~

| (optional) The number of the best refined phase sets to be output. The
  maximum number is 5.
| Default: nout = 3.

::

    Example:

      NOUT 5

SKIP <skip>
~~~~~~~~~~~

| (optional) The first <skip> phase sets will be skipped and RANTAN
  starts from set <skip>+1.
| Default: skip = 0.

::

    Example:

      SKIP 500

WMIN <wmin>
~~~~~~~~~~~

| (optional) The weight for a random phase. Higher weights can be used
  if known phases are used or the first run of RANTAN was not
  successful.
| Default: wmin = 0.25.

::

    Example:

      WMIN 0.45

WTRI <wtri>
~~~~~~~~~~~

| (optional) The weight for a triplet; the value depends on the number
  and type of atoms in the unit cell.
| Default: wtri = 0.1.

::

    Example:

      WTRI 0.2

SWTR
~~~~

| (optional) There are two procedures in RANTAN to refine random phases:
  a fast procedure called FASTAN and a statistical weighting tangent
  formula called SWTR which should be used when most of the ABS figures
  of merit are greater than 1.3.
| Default: FASTAN.

::

    Example:

      SWTR

END
~~~

of input. If present, this must be the last keyword.

INPUT AND OUTPUT FILES
----------------------

| The input files are the keyword file and a standard MTZ reflection
  data file.
| Input:
| HKLIN input data file(MTZ).
| Output:
| HKLOUT output data file(MTZ).

Here are the definitions for each label:

::

         Name        Item

         H, K, L     Miller indices.

         EVAL        E values (normalized structure factors).
         PHI         Known phases.
         WT          Weights of known phases.

         PHI1        New phases of the best set 1.
         WT1         Weights for the new phases of the best set 1.

         PHI2        New phases of the best set 2.
         WT2         Weights for the new phases of the best set 2.

         PHI3        New phases of the best set 3.
         WT3         Weights for the new phases of the best set 3.

         PHI4        New phases of the best set 4.
         WT4         Weights for the new phases of the best set 4.

         PHI5        New phases of the best set 5.
         WT5         Weights for the new phases of the best set 5.

PROGRAM OUTPUT
--------------

The program output starts with details from the input keyword data
lines. Then information from the input MTZ file follows. An error
message will be printed out if any illegal input in the keyword data
lines has been found and the program will stop.

The full details of running RANTAN will be printed out if the keyword
`LIST <#list>`__ is given and that will include all the E values, the
convergence map and figures of merit for each of the `MAXS <#maxs>`__
phase sets. Otherwise E values and convergence map will not be printed
out and the figures of merit for the phase sets will be printed out only
if the combined figure of merit is greater than 0.6. If a refined phase
set is similar to a previous phase set, it will be flagged as such.

The best `NOUT <#nout>`__ phase sets, as determined by the combined
figures of merit, are then listed. Note that if the combined figures of
merit are similar, then the phase sets may be related by an origin
shift, symmetry operation and/or change of hand. Finally, details of the
output MTZ file are listed, including the additional columns for the
`NOUT <#nout>`__ phase sets.

REFERENCE
---------

#. Yao Jia-xing, (1981). *Acta. Cryst.* **A**\ 37, 642-644.
#. Germain,G. and Woolfson,M.M. (1968) *Acta. Cryst.* **B**\ 24, 91-97.
#. Karle,J. and Hauptman,H. (1956) *Acta. Cryst.* **9**, 635.
#. Yao Jia-xing, (1983). *Acta. Cryst.* **A**\ 39, 35-37.

AUTHORS
-------

Yao Jia-xing

EXAMPLES
--------

This example is to use E values after REVISE and ECALC:

::

    rantan \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-rantan.mtz \
    << eof
    TITLE   testing RANTAN.
    LABI EVAL=EM_RE

    LABO PHI1=NEWPHASE1 WT1=NEWT1 -
         PHI2=NEWPHASE2 WT2=NEWT2

    RESO 10.0 3.5

    END
    eof

This example is to use known phases with weights:

::

    rantan \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-rantan.mtz \
    << eof
    TITLE   testing RANTAN.
    LABI EVAL=EM_RE PHI=AC WT=WTAC

    RESO 10.0 3.5
    NREF 800
    WMIN 0.45
    MAXS 100
    NOUT 1

    END
    eof
