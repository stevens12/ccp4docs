MAPROT (CCP4: Supported Program)
================================

NAME
----

**maprot** - map skewing, interpolating, rotating, averaging and
correlation masking program

SYNOPSIS
--------

| **maprot MAPIN** *foo.map* [ **WRKIN** *bar.map* ] [ **MSKIN**
  *bar.msk* ] [ **MAPOUT** *foobar.map* ] [ **WRKOUT** *foobar.map* ] [
  **CUTOUT** *foobar.map* ]
| [`Keyworded input <#keywords>`__]

 DESCRIPTION
------------

\`maprot' is a general map skewing, interpolating, rotating and
averaging program. It can be used to interpolate a map onto a coarser or
finer grid, to skew density into a different unit cell, to rotate a map
to allow plotting of arbitrary sections, to rotate fragments for
molecular replacement or NCS searching, or as part of a single- or
multi-crystal averaging calculation. It can also be used to generate
\`correlation maps' for the automatic determination of averaging masks
(`reference [1] <#reference1>`__). Finally, it may be used to 'cut'
density from a map and transfer it into another cell with or without a
rotation. This is a step in the identification of cross-crystal
translations, and general 6-d ncs searches.

\`maprot' works with two maps. The first is the \`cell' map, which obeys
the crystal symmetry and unit cell repeat. The second is 'work' map
which does NOT have any symmetry or repeat. The input cell map (MAPIN)
must generally cover exactly one unit cell. The work map may store
density copied from the cell map, or supply density to be copied into
the cell map.

Multiple transformation operators may be supplied. When copying density
from the cell map into the work map, then all the related regions of the
cell map are all averaged onto the work map. If a mask is specified for
the work map, then it is only filled within this mask.

It is also possible to expand from the work map onto the crystal map.
This allows a full map averaging calculation to be performed in two
steps, or multi-xtal averaging to be performed by multiple runs of the
program.

The correlation mode produces a \`local correlation map' from the input
density (either from the crystal-map in the case of a single crystal
with NCS, or between the crystal- and work-maps in the multi-crystal
case). This can be contoured in a graphics program to locate the
NCS-related part of the unit cell, and/or used to generate an averaging
mask using `\`mapmask' <mapmask.html>`__. Multiple correlation maps from
different combinations of operators can be combined by summation in
\`mapmask' (Note: In the case of proper NCS, a correlation mask will
show the multimer. In the case of improper NCS it will give just one
monomer).

Finally, \`maprot' can be used to cut a region of density out of a map
and insert it into a blank map, as a step in a phased NCS determination
or cross-crystal translation function. In this mode only the work map
and mask are supplied. The crystal map cell and grid are set by program
keywords. The masked density is transferred from the work map into the
crystal map using \`MODE TO' and the supplied operator (which may be the
identity operator). Only that portion of the crystal map covering the
masked density is output to \`CUTOUT'.

Remember that "cell" maps will obey crystal symmetry and unit cell
repeats; "work" maps do not. See the `examples <#examples>`__ for the
calculation you are performing.

 INPUT/OUTPUT FILES
-------------------

 MAPIN
~~~~~~

Input crystal map. This must cover the whole unit cell (not an
asymmetric unit). A suitable map may be prepared using the \`mapmask'
with just the \`XYZLIM CELL' keyword.

 WRKIN
~~~~~~

Optional starting density for the work map. If this map is not supplied,
the density in the work map defaults to zero, and the grid sampling and
extent will be determined from `MSKIN <#mskin>`__ or the user keywords
(BE CAREFUL if you have created a "dummy" map using a set of atomic
coordinates; it is important to scale that map by 0.00 to avoid
transferring unwanted electron density into the output work map. See
`example <#examples>`__). The values of WRKIN outside any input mask
will remain unchanged in `WRKOUT <#wrkout>`__, the values inside a mask
will be scale \* (initial value + rotated density 1 + rotated density 2
+ ...)

 MSKIN
~~~~~~

Optional mask for the work map. This can determine the grid and cell for
the work map, and must be consistent with WRKIN. If neither WRKIN nor
MSKIN are specified, then the work map must be described by the
`CELL <#cell>`__, `GRID <#grid>`__, `XYZLIM <#xyzlim>`__ cards. If a
mask is not specified, then the whole of the work map will be considered
inside the mask and filled with rotated density.

 MAPOUT
~~~~~~~

is the final crystal map. This will only differ from `MAPIN <#mapin>`__
if the `MODE TO <#mode_to>`__ or `MODE BOTH <#mode_both>`__ options are
used.

 WRKOUT
~~~~~~~

is the output work map, which will contain rotated density from the cell
`MAPIN <#mapin>`__ plus density transferred from `WRKIN <#wrkin>`__.

 CUTOUT
~~~~~~~

is the output map from density cutting, containing the rotated masked
data. This map only covers the volume of the rotated mask.

Either `MAPOUT <#mapout>`__ or `WRKOUT <#wrkout>`__ or both may be
assigned.

 KEYWORDED INPUT
----------------

Possible keywords are:

    `**AVERAGE** <#average>`__, `**CELL** <#cell>`__,
    `**GRID** <#grid>`__, `**INVERT** <#invert>`__,
    `**MODE** <#mode>`__, `**RADIUS** <#radius>`__,
    `**SCALE** <#scale>`__, `**SYMMETRY** <#symmetry>`__,
    `**XYZLIM** <#xyzlim>`__

 MODE FROM/TO/BOTH/CORR
~~~~~~~~~~~~~~~~~~~~~~~

| Sets the mode of operation.
| \`FROM' signals that one or more copies of the electron density from a
  cell map (`MAPIN <#mapin>`__) will be transferred to a work map
  (`WRKOUT <#wrkout>`__). The cell map is transformed by each set of
  operators in turn, and all copies are summed in the
  `WRKOUT <#wrkout>`__ map.
| \`TO'' signals that density from a work map (`WRKIN <#wrkin>`__) will
  be transferred to a cell map (`MAPOUT <#mapout>`__) using symmetry
  operators and unit cell repeats obtained from the `MAPIN <#mapin>`__
  header. It overwrites the density in the input cell map with the
  masked density from the work map.
| \`BOTH' performs both operations in turn and is only useful if you
  have multiple copies of a molecule in the asymmetric unit. It first
  averages the electron density from the cell map (`MAPIN <#mapin>`__)
  to `WRKOUT <#wrkout>`__, then recreates the cell map
  (`MAPOUT <#mapout>`__) with the averaged and (hopefully improved)
  electron density. It requires a mask.
| \`CORR' calculates a local correlation over the whole of the work map.
  If two averaging operators are given, then the two regions of density
  mapped from the cell to the work map by those operators are
  correlated, otherwise the density of the input work map is correlated
  with the density of the cell map mapped by the one operator (the first
  case is used when identifying a non-crystallographic symmetry mask,
  the second can be used to identify correlated density between crystal
  forms).

 CELL [ WORK \| XTAL ] <a> <b> <c> <alpha> <beta> <gamma>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set the unit cell parameters for the work map (CELL WORK) or the crystal
map (CELL XTAL). CELL WORK should be specified if and only if neither
`MSKIN <#mskin>`__ nor `WRKIN <#wrkin>`__ are set. CELL XTAL should only
be specified if `MAPIN <#mapin>`__ is not set.

 GRID [ WORK \| XTAL ] <nx> <ny> <nz>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set the grid for the work map (GRID WORK) or the crystal map (GRID
XTAL). GRID WORK should be specified if and only if neither
`MSKIN <#mskin>`__ nor `WRKIN <#wrkin>`__ are set. GRID XTAL should only
be specified if `MAPIN <#mapin>`__ is not set.

 XYZLIM <x1> <x2> <y1> <y2> <z1> <z2>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set the work map extent. <x1>-<z2> are given in grid units or in
fractional coordinates. This should be specified if and only if neither
`MSKIN <#mskin>`__ nor `WRKIN <#wrkin>`__ are set.

 SYMMETRY [ WORK \| XTAL ] <spacegroup name or number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set spacegroup symmetry of the work or crystal map. Unlike the normal
SYMMETRY cards, the spacegroup MUST be given by number. Alternatively,
the conventional SYMM <spacegroup name> keyword may be used to set the
symmetry of the work map only.

 SCALE <s>
~~~~~~~~~~

Set scale factor to apply to the work map inside the mask between
rotating FROM and TO the cell map.

 RADIUS <r>
~~~~~~~~~~~

Set the radius of with the local correlation is performed in `\`MODE
CORR' <#mode_corr>`__. Increase this if the correlation mask shows many
spurious peaks. Default <r>=8.0 Angstrom.

 AVERAGE
~~~~~~~~

Set a density rotation and translation operator. This keyword is
followed by a rotation/translation matrix on subsequent lines in either
CCP4 or O/RAVE format. One AVERGE keyword, followed by the corresponding
operator, is given for each rotation/translation operation to be
performed. The format is the same as the `\`dm' <dm.html#average>`__ and
`\`ncsmask' <ncsmask.html#average>`__ \`AVERAGE' cards).

 CCP4 Formats: (see also the program `\`lsqkab' <lsqkab.html>`__)
    ::

        ROTATE EULER <alpha> <beta> <gamma>     (Euler angles)
        TRANSLATE <t1> <t2> <t3>

 or
    ::

        ROTATE POLAR <omega> <phi> <kappa>      (Polar angles)
        TRANSLATE <t1> <t2> <t3>

 or
    ::

        ROTATE MATRIX <r11> <r12> <r13> <r21> <r22> <r23> <r31> <r32> <r33>
        TRANSLATE <t1> <t2> <t3>

 O/RAVE Format
    OMAT
    <r11> <r21> <r31>
    <r12> <r22> <r32>
    <r13> <r23> <r33>
    <t1> <t2> <t3>
    where
    x' = <r11>x + <r12>y + <r13>z + <t1>
    y' = <r21>x + <r22>y + <r23>z + <t2>
    z' = <r31>x + <r32>y + <r33>z + <t3>
    (note that the rotation matrix is transposed with respect to CCP4
    matrix format.)

These are the operations which map the density in the region covered by
the input mask onto the other equivalent regions. The first operator
must be the identity matrix. The mask is input in CCP4 mask (mode 0)
format on the input file label `MSKIN <#mskin>`__, and should cover just
one monomer or averaging domain, NOT the whole unit cell.

 INVERT
~~~~~~~

Invert all the operators.

 EXAMPLES
---------

 To interpolate a map to a different grid:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    #
    maprot mapin ins.map wrkout ins_fine.map << eof
    MODE FROM
    CELL WORK 82.5 82.5 34 90 90 120
    GRID WORK 120 120 60
    XYZLIM 0 119 0 119 0 59
    SYMM WORK 146
    AVER
    ROTA POLAR 0 0 0
    TRANS 0 0 0
    eof

 To rotate a map 30 degrees about z for plotting (note cell and symm are no longer useful)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    #
    maprot mapin ins.map wrkout ins_fine.map << eof
    MODE FROM
    CELL WORK 100 100 50 90 90 120
    GRID WORK 180 180 90
    XYZLIM 0 179 0 179 0 89
    SYMM WORK P1
    AVER
    ROTA MATRIX  0.866  0.500  0.000  -
                -0.500  0.866  0.000  -
                 0.000  0.000  1.000
    TRANS    0.000  0.000  0.000
    eof

 To make an averaged copy of the molecule covered by MSKIN to look at on the graphics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    #
    maprot mapin chm.map mskin chm.msk wrkout chm_for_graf.map << eof
    MODE FROM
    AVER
    OMAT
      1.000  0.000  0.000
      0.000  1.000  0.000
      0.000  0.000  1.000
      0.000  0.000  0.000
    AVER
    OMAT
     -0.43073    -0.62689    -0.64921
      0.04987     0.70173    -0.71070
      0.90110    -0.33850    -0.27099
      43.635 38.059 62.726
    AVER
    OMAT
     -0.43073     0.04987     0.90110
     -0.62689     0.70173    -0.33850
     -0.64921    -0.71070    -0.27099
      82.989 15.401 -8.928
    eof

 To make an averaged map which can be back-transformed for phase improvement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    #
    maprot mapin chm.map mskin chm.msk mapout chm_av.map << eof
    MODE BOTH
    SCALE 0.33333
    AVER
    ROTA POLAR  0.0  0.0  0.0
    TRANS  0.0  0.0  0.0
    AVER
    ROTA POLAR  113.28130 103.41944 120.33858
    TRANS  43.635 38.059 62.726
    AVER
    ROTA POLAR   66.58067 -76.78019 119.69176
    TRANS  82.989 15.401 -8.928
    eof

 To make a correlation map, convert it to a mask, and tidy up the mask
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    #
    maprot mapin $SCRATCH/chm_mir.map 
           wrkout $SCRATCH/chmcorrel.map << eof
    SYMM WORK 1
    CELL WORK 90.3 90.3 129.8 90 90 90
    XYZLIM     -20 80 24 120 -60 48 
    GRID WORK 80 80 120
    RADI 6
    MODE CORR
    AVER
    ROTA POLAR  0.0  0.0  0.0
    TRANS  0.0  0.0  0.0
    AVER
    ROTA POLAR  113.28130 103.41944 120.33858
    TRANS  43.635 38.059 62.726
    END
    eof
    #
    # now mask the multimer: multimer is 6% of unit cell
    #
    mapmask mapin $SCRATCH/chmcorrel.map 
            mskout $SCRATCH/chmcorrel.msk << eof
    MASK VOLU 0.06
    eof
    #
    # Now we might edit this mask by hand, or use ncsmask to get rid of
    # all but the largest lump of mask
    #
    ncsmask mskin $SCRATCH/chmcorrel.msk 
            mskout $SCRATCH/cmncs.msk << eof
    PEAK 1
    eof

 Density cutting: density is cut from one cell and placed in a new cell
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    #
    # Cut density into a 100A virtual cell.
    # The density could be moved by specifying a non-identity operator.
    # For cross-crystal translation function, give the rotation
    # operator and the cell from the alternate crystal form.
    #
    maprot wrkin gmtocut.map mskin gmtocut.msk cutout cut.map << eof
    MODE TO
    CELL XTAL 100. 100. 100. 90. 90. 90.
    GRID XTAL 150 150 150
    AVER
    ROTA POLAR  0.0  0.0  0.0
    TRANS  0.0  0.0  0.0
    eof

 BUGS
-----

Since the work map will take the same grid as the mask, it is necessary
to use a fine grid for the mask, especially when doing averaging
calculations. The grid should be at least as fine as the cell map.

When performing density cutting, If the masked region is larger than the
dimensions of the crystal map in any direction, the overlapped density
will appear at the edges of the CUTOUT map.

REFERENCES
----------

#. Stein *et al.*, *Structure* **2**, 45-47 (1994)

SEE ALSO
--------

`mapmask <mapmask.html>`__, `ncsmask <ncsmask.html>`__,
`rotmat <rotmat.html>`__, `dm <dm.html>`__
