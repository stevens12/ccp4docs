DTREK2MTZ (CCP4: Supported Program)
===================================

NAME
----

**dtrek2mtz** - converts d\*trek scalemerge output into MTZ format.

SYNOPSIS
--------

| **dtrek2mtz** **HKLIN** *foo.dat* **HKLOUT** *foo.mtz*
| [`Keyworded input <#keywords>`__]

 DESCRIPTION
------------

This program converts merged data from d\*trek into an MTZ format. After
the conversion use `TRUNCATE <truncate.html>`__ to convert the Is into
Fs. Finally, `CAD <cad.html>`__ (or `sortmtz <sortmtz>`__) should be run
to sort the data and put it into the correct asymmetric unit for CCP4.

Assumption: if I+ and I- are on separate lines, then I- appears first.
This assumption is checked and a count of the failure of the assumption
is output AND I+/I- are swapped. BUT if this happens, you should be very
wary of the result mtz file since d\*TREK designates I+ and I- properly.

These are converted to the weighted means Imean and SigImean

::

      Imean = (I+/SIGI+  +  I-/SIGI-) / (1/SIGI+  +  1/SIGI-)
      SigImean = 1 / (1/SIGI+  +  1/SIGI-)

 MTZ datasets and Data Harvesting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Columns in MTZ files are organised into projects, crystals and datasets.
This information is used in subsequent processing to make sensible
decisions based on which dataset a column belongs to. Project, crystal
and dataset names should be assigned using the `NAME <#name>`__ keyword.
These will be written to the output MTZ file, and be inherited by
subsequent programs. `Data harvesting <harvesting.html>`__ also uses the
project and dataset names to write out appropriate harvest files for
deposition.

 INPUT/OUTPUT FILES
-------------------

 HKLIN
    is the input ASCII reflection list file from d\*trek. The d\*TREK
    reflection list file usually starts like this:

    ::

            nNumIntCols nNumFloatCols nNumStringCols
            column_names
            ...
            4   0   3 51178.9  2796.6 

    In the absence of anomalous data there should be 5 columns H, K, L,
    I and SIGI, and with anomalous data there should be 7 columns H, K,
    L, I+, SIGI+, I- and SIGI-.

 HKLOUT
    is the output MTZ file containing labels H, K, L, IMEAN, SIGIMEAN
    and if anomalous data is present I(+), SIGI(+), I(-), SIGI(-) as
    well.

 KEYWORDED INPUT
----------------

Available keywords are:

    `**ANOMALOUS** <#anomalous>`__, `**CELL** <#cell>`__,
    `**END** <#end>`__, `**MONITOR** <#monitor>`__,
    `**NAME** <#name>`__, `**REJECT** <#reject>`__,
    `**RESOLUTION** <#resolution>`__, `**SCALE** <#scale>`__,
    `**SYMMETRY** <#symmetry>`__, `**TITLE** <#title>`__
    , `**WAVE** <#wavel>`__

NAME PROJECT <pname> CRYSTAL <xname> DATASET <dname>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[Note that the keywords PNAME <pname>, XNAME <xname> and DNAME <dname>
are also available, but the NAME keyword is preferred.]

Specify the project, crystal and dataset names for the output MTZ file.
It is strongly recommended that this information is given. Otherwise,
the default project, crystal and dataset names are "unknown", "unknown"
and "unknown<ddmmyy>" respectively (where <ddmmyy> is the date, with no
spaces).

The project-name specifies a particular structure solution project, the
crystal name specifies a physical crystal contributing to that project,
and the dataset-name specifies a particular dataset obtained from that
crystal. All three should be given.

CELL <a> <b> <c> [ <alpha> <beta> <gamma> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The cell dimensions in Angstroms and degrees.

If the d\*trek reflection list file does not include the cell
dimensions, then this keyword is *compulsory*. The angles default to 90
degrees.

ANOMALOUS YES \| NO
~~~~~~~~~~~~~~~~~~~

Specify whether or not input file contains anomalous data. Defaults to
YES.

MONITOR <nmon>
~~~~~~~~~~~~~~

Monitors the <nmon>th reflection and writes it to the output stream. The
default is every 1000th reflection.

REJECT <h k l>
~~~~~~~~~~~~~~

If specified then the specific hkl will not be written to the MTZ file.

RESOLUTION <maxres> <minres>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If specified then the all reflections outside the range will be excluded
from the MTZ file. The limits can be given in either order and in
Angstrom or SineSquared(theta)/lambdaSquared units. The default is that
all reflections are output.

 SCALE <scale>
~~~~~~~~~~~~~~

All intensities and their sigmas will be scaled by <scale>. The default
is 1.0.

SYMMETRY <SG name\|SG number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the d\*trek reflection list file does not include the spacegroup,
then this keyword is *compulsory* and can be given as the space group
name or number.

TITLE <string>
~~~~~~~~~~~~~~

This title will become the title/history in the MTZ file.

WAVE <wavelength>
~~~~~~~~~~~~~~~~~

The wavelength associated with this data. (Will become compulsory)

 END
~~~~

Terminates the keyword input. Must be last keyword.

SEE ALSO
--------

`scalepack2mtz <scalepack2mtz.html>`__, `cad <cad.html>`__,
`truncate <truncate.html>`__, `Data Harvesting <harvesting.html>`__.

AUTHOR
------

J. W. Pflugrath (Dec. 1999) Modified from SCALEPACK2MTZ
