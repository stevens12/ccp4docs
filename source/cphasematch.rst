CPHASEMATCH (CCP4: Supported Program)
=====================================

NAME
----

**cphasematch** - Calculate agreement between phase sets, allowing for
possible origin differences

SYNOPSIS
--------

| **cphasematch** **-mtzin** *filename* **-mtzout** *filename*
  **-colin-fo** *colpath* **-colin-hl-1** *colpath* **-colin-phifom-1**
  *colpath* **-colin-fc-1** *colpath* **-colin-hl-2** *colpath*
  **-colin-phifom-2** *colpath* **-colin-fc-2** *colpath* **-colout**
  *colpath* **-resolution-bins** *number-of-bins* **-no-origin-hand**
  **[-stdin]**
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

'cphasematch' will calculate the agreement between two phase sets,
represented either by a phase / figure of merit pair, or as a set of
Hendrickson Lattman coefficients. By default, cphasematch will check for
possible origin shifts between the two sets.

INPUT/OUTPUT FILES
------------------

-mtzin
    Input MTZ file containing the phase sets to be compared. These can
    be represented as a phase / figure of merit pair, or as a set of
    Hendrickson Lattman coefficients. These columns can be created from
    a set of atomic coordinates (i.e. a PDB file) by running csfcalc.
-mtzout
    Output MTZ file containing, in addition to the input columns, phase
    information for the second phase set (HL coefficients, phi/fom, Fc
    depending on what was supplied) adjusted for any origin shift which
    has been applied.
<dphi>
    mean phase difference
w1<dphi>
    mean phase difference weighted by the FOM of the first set
wFcorr
    correlation coefficient between the structure factor amplitudes of
    the first and second sets, weighted by the cosine of the phase
    difference
Qfom1
    Average ratio between the cosines of the phase difference and the
    phase error (latter from the FOM).

Problems
--------

None known.

AUTHOR
------

Kevin Cowtan, York.

SEE ALSO
--------

`phistats <phistats.html>`__
