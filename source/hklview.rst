HKLVIEW (CCP4: Supported Program)
=================================

NAME
----

**hklview** - X-windows program, displays zones of reciprocal space as
pseudo-precession images.

SYNOPSIS
--------

**hklview** [options] [ *foo\_in.mtz* ]

 DESCRIPTION
------------

This program displays zones of reciprocal space as pseudo-precession
images, on an X-windows terminal, using the xdl\_view subroutine library
written by John Campbell at Daresbury Laboratory. Spots are represented
as squares of size & darkness which depend on intensity (or
alternatively amplitude). The program reads an MTZ file containing
either:

(a)
    unmerged intensities, in which case the spots are displayed with
    their original measured indices (if there are redundant
    observations, only the last will be drawn). Partially recorded
    reflections may be summed or ignored.

or

(b)
    merged data (usually amplitudes F), in which case the reflections
    are expanded by the crystallographic symmetry to fill the sphere,
    *i.e.* the whole zone will be displayed.

| A zone is defined by a vector zvec and a level number zlev, such that
  the condition for a reflection h lying in the zone is that h . zvec =
  zlev
| For example, zones (hk0), (h0l) & (0kl) are defined by zlev = 0, zvec
  = (001), (010), (100)

Command line options:

+---------------------+------------------------------+
| -d <resolution>     | set resolution limit in A    |
+---------------------+------------------------------+
| -c <column label>   | set column label             |
+---------------------+------------------------------+
| -r[awname]          | don't add .mtz to filename   |
+---------------------+------------------------------+

 INPUT FILE
-----------

This may be assigned from the menu option hkl file, or by giving the
file name on the command line (*i.e.* hklview file\_name). In either
case, the default extension .mtz will be added if absent, unless the
command line option -rawname [-r is sufficient] is given.

 PROGRAM FUNCTION
-----------------

The display window is divided into 4 main areas: `Display Parameter
area <#display_parameters>`__, `Output area <#output>`__, `Main
menu <#main>`__, `Image display area <#image_display>`__.

The left mouse button is the main pointer, the middle button is used on
the image area to control the magnify window, the right button for
sub-menu options in the image area.

 Display parameter area
    This allows a number of parameters affecting the display & other
    options to be changed. Yes/No toggles can be changed by clicking the
    left mouse button on the Yes/No button. Numbers & strings can be
    changed by clicking on the number with the left button, then typing
    the number, terminated by <cr>. See also `more detailed description
    below <#details_display_parameters>`__.
 Output area
    This area reports the results of clicking on the image, etc. No
    input is accepted. See also `more detailed description
    below <#details_output>`__.
 Main menu
    Click left button to select an option. See also `more detailed
    description below <#details_main>`__.
 Image display area
    In the window area, the left mouse button can be used to select a
    point or a rectangle by dragging. Clicking on a spot will give the
    indices hkl, & the intensity (or amplitude) in the Output area.
    Selected areas may be zoomed by (a) clicking on the Zoom button at
    the right-hand side of the image display header, then selecting a
    rectangle, or (b) selecting a rectangle then clicking on the menu
    option Zoom; click Zoom again to restore the full image.

    The middle mouse button controls the magnify window (on the left of
    the image display header). Clicking the middle button on the magnify
    window toggles a double size display. Clicking the middle button on
    the image display freezes the magnify window, allowing pixels in the
    magnify window to be picked (but not rectangles).

    The top right panel of the image display header contains:-

    #. Min & Max image values for the grey level scaling (equivalent to
       Scale low & high in the parameter area, but less permanent)
    #. Cursor position in pixels. In zoomed images, this is the pixel
       position in the ZOOMED image, not in the whole image.
    #. Overlay On/Off/Offset menu, toggles display of circle and spot
       overlays (right button to select)
    #. Contrast slider: controls display contrast (left button)
    #. Colour table selection: probably only Black on white & White on
       black are useful
       (*N.B.* "Yellow if >Max" option makes pixels yellow if they are
       greater than scale-high (=Max) value)
    #. Mag menu: controls magnification in magnify window
    #. PS button: sends image to a Postscript file (after a little
       dialogue)
    #. Zoom button: click then select rectangle to zoom. Click twice to
       restore to original

DETAILS
~~~~~~~

Display parameters
^^^^^^^^^^^^^^^^^^

#.  Scale low, high: define scaling range, *i.e.* the values of an image
   pixel to be displayed as white or black. The first image displayed
   will be scaled automatically, and the range may also be recalculated
   with the `Rescale image <#rescale_image>`__ option in the menu, but
   you may wish to change this. These numbers are exactly equivalent to
   the Min/Max numbers on the image panel, but numbers set here will
   carry over to all images. Use `Redraw image <#redraw_image>`__ option
   to redisplay image with altered scaling.
#.  Pick area: controls number of pixels displayed by `Pick <#pick>`__
#.  Outer circle: define resolution (Å) for the outer circle drawn by
   the `Circles <#circles>`__ option. Default to edge of zone. The
   `actual resolutions of the circles <#circle_resolution>`__ are given
   in the output area.
#.  Resolution: set outer resolution limit (Å) for storing reflections
   from the hkl file. If a file is open, it will be reread, & the
   reflections stored.
#.  Column label: specify the column label for I or F in the file. If
   this is blank (default), then the first column of type J or F in the
   file will be used.
#.  Sum partials: only relevant if the hkl file includes partially
   recorded reflections. Toggle to sum partials or to omit partials
   (default sum).

Output
^^^^^^

#.  Pixel: last pixel hit (Yms, Zms) or 1st corner of rectangle
#.  Resolution: of last pixel hit (in Angstrom)
#.  Spacing: result of `Measure <#measure>`__ option
#.  Zoom factor: if `Zoom <#zoom>`__ option used
#.  Circle resolution: resolution of circles drawn by
   `Circles <#circles>`__ option
#.  Zone normal: vector defining zone direction, *e.g.* 0 0 1 defines
   zone hkN, where N is the zone level
#.  Zone level: zone level number
#.  hkl: indices of last hit reflection
#.  F\|I sd: stored value of F or I & standard deviation, for last hit
   reflection.
#.  Batch number, full\|partial flag: only for unmerged data
#.  Intensity display \| Amplitude displayed: intensity is displayed by
   default (even if file contains F), but `amplitude (F) <#display_F>`__
   may be selected from menu
#.  Original indices \| Symmetry applied: for unmerged files (M/ISYM
   column present in file), reflections are drawn according to their
   original indices. For merged data files, the spacegroup symmetry is
   used to generate the whole zone.

Main menu
^^^^^^^^^

#.  hkl file: give name of hkl file to be read
#.  Reread file: read hkl file again (*e.g.* after changing column label
   assignment)
#.  Display F \| I: toggle display of spots graded by intensity or
   amplitude F. Intensity is displayed by default (even if file contains
   F). Note that the display will not change until a new zone is
   selected.
#.  Zone normal: define zone normal vector and level, and draw it. A
   zone is defined by a vector zvec and a level number zlev, such that
   the condition for a reflection h lying in the zone is that
   h . zvec = zlev
#.  0kl, h0l, hk0, hhl: choose & draw appropriate zone
#.  Nkl, hNl, hkN: set `zone level <#output_zone_normal>`__, choose &
   draw appropriate zone
#.  Next level, Previous level: increment or decrement zone level, then
   draw zone. Leave zone normal unchanged.
#.  Level number: set zone level, then draw zone. Leave zone normal
   unchanged.
#.  Redraw image: redraw without any overlays
#.  Rescale image: reset the scale low & high, & redraw image
#.  Pick: toggle display of figure field around picked point on image.
#.  Measure: toggle measurement of reciprocal lattice spacing. Pick two
   spots on a reciprocal lattice row (left button), then type number of
   orders, & the `lattice spacing <#spacing>`__ is output in the Output
   area. Beware skew cells, this gives you 1/a\* etc, not a!
#.  Circles: toggle display of resolution circles. The `resolution of
   the outer circle <#outer_circle>`__ is defined in the parameter
   table, the `resolutions of all the circles <#circle_resolution>`__ is
   given in the output area.
#.  Axes off \| on: click to remove axes & legend, click again to put
   them back
#.  Zoom: zoom a part of the image. Select an area by dragging a
   rectangle on the image (left button), then click Zoom. A square area
   enclosing the selected rectangle will be displayed. Zooming may be
   repeated. To restore full image, click Zoom when no rectangle is
   selected. Note that zoom does not recreate the zone image, but just
   magnifies a part of it.
#.  Exit: exit program

SEE ALSO
--------

`hklplot <hklplot.html>`__.

AUTHOR
------

Phil Evans, MRC LMB, Cambridge
