COORD\_FORMAT (CCP4: Supported Program)
=======================================

NAME
----

**coord\_format** - fix PDB format and convert to/from other formats

SYNOPSIS
--------

| **coord\_format xyzin** *foo\_in.xyz* **xyzout** *foo\_out.xyz*
| [Key-worded input file]

DESCRIPTION
-----------

``coord_format`` has two purposes:

#. Syntax checking on input is less strict than other programs.
   Therefore, a corrupted PDB file can be read in, and output as a
   "standardised" PDB file. It is the user's responsibility to check
   that the program has interpreted the input correctly.
#. The program will convert between PDB, mmCIF and MMDB binary formats.
   Because of the limitations of the various formats, this conversion is
   not exact, and again the user should check the output carefully.

INPUT AND OUTPUT FILES
----------------------

XYZIN
~~~~~

Input coordinate file. The input format is detected automatically.

XYZOUT
~~~~~~

Output coordinate file, in format selected by `OUTPUT <#output>`__
keyword.

KEYWORDED INPUT
---------------

OUTPUT [ PDB \| CIF \| BIN ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (default is PDB)
| Select output file format: PDB = standard PDB format, CIF = mmCIF
  format, and BIN = MMDB binary format.

FIXBLANK
~~~~~~~~

Fix files with a missing or blank chain ID. For example, it is quite
common to find that files in the PDB archive which contain a single
protein chain do not have a chain ID set. If this option is set, the
program will assign affected residues to the next available chain ID. If
there appears to be more than one chain involved (e.g. both protein and
waters have a blank chain ID), then more than one chain ID will be
created.

If all residues in a file have an associated chain ID, then this option
will have no effect, i.e. it is safe to use.

END
~~~

End keyworded input.

PROGRAM OUTPUT
--------------

The program currently gives a short summary of the commands received.

AUTHORS
-------

| Eugene Krissinel, European Bioinformatics Institute, Cambridge, UK.
| Martyn Winn, Daresbury Laboratory, UK
