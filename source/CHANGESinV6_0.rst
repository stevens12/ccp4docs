CCP4 v6.0 Program Changes
=========================

INDEX

A list of general acknowledgements for testing CCP4 v6.0 can be found
`here <ACKNOWLEDGEMENTS_V6_0.html>`__.

--------------

Building etc.:

-  Clipper/fftw built by default
-  cctbx/phaser are included as separate packages. The main CCP4 build
   will build them if these packages are included.
-  There are a number of fixes for the AIX build.
-  to\_small\_refmac.csh/to\_big\_refmac.csh: shell scripts to reduce
   and restore the array sizes for Refmac5 prior to compilation (in
   $CPROG/refmac5\_/)
-  A new ccp4.setup-zsh for ZSH users (Bill Scott)
-  --with-x is now the {default;} use --disable-x if you have problems
   with the X-windows build
-  The script patch\_ccp4.sh in {$CETC} can be used to automatically
   download and apply available source code fixes to the suite for
   UNIX-based systems. Note that any recompilation must still be
   performed manually afterwards - see Appendix M of the CCP4
   installation instructions.

--------------

Program changes:

-  ANISOANL: now checks for non-positive-definite Us in XYZIN and lists
   to log file (all options)
-  AREAIMOL: new REPORT keyword allows control of output for contact
   areas and reporting of area by residue
-  CIF2MTZ: several new column labels added, including anomalous
-  COORD\_FORMAT: new keyword FIXBLANK
-  DM: program now produces a data harvesting file
-  MapSlicer: supports reading and display of mask files (new "mask"
   mode); displays "greyscale" view superimposed on contours; saves user
   preferences for contour levels and other settings between sessions.
-  MTZ2VARIOUS: if F(+) and F(-) are input for SHELX output then
   rescaling is done within the program to produce pseudo-intensities;
   OUTPUT CIF option updated to use tokens from the PDB exchange
   dictionary for anomalous data and HL coefficients.
-  PDBCUR: new keywords SUMMARISE, DELHYDROGEN, MOSTPROB, CUTOCC, and
   TRANSLATE (MDW)
-  PDB\_EXTRACT: new version 1.700
-  PDBSET: new function ATRENUMBER (renumber atoms sequentially without
   gaps in output file)
-  PEAKMAX: new keyword EXCLUDE (turns suppression of peaks at the edge
   of the map on or off)
-  REFMAC: new version 5.2.0019
-  MOLREP: new version 9.0.09
-  MOSFLM: new version 6.2.5
-  SFCHECK: new version 7.0.18
-  SFTOOLS: new options LIST/SET DCELL/DWAVE for dataset information
-  Matthews\_coeff: outputs the Kantardjieff and Rupp resolution based
   probabilities
-  FFTBIG replaces FFT
-  SCALEPACK2MTZ SYMM keyword not compulsory anymore, will try to pick
   up spacegroup in the input file.
-  TLSANL: ANISOU line in XYZOUT is now adjusted according to ISOOUT
   keyword. Summary tags used to highlight important quantities.

--------------

Library changes:

-  CCPERR/ccperror: each time ccperror is called it invokes a
   user-defined callback function which has previously been set using
   new C library function "ccp4SetCallback".
-  CCTBX library included as a separate package (Paul Adams, Ralf
   Grosse-Kunstleve et al)

--------------

New programs:

-  BP3: multivariate likelihood substructure refinement and phasing of
   S/MIR(AS) and/or S/MAD (Raj Pannu)
-  CHAINSAW: new molecular replacement utility that mutates a pdb file
   to poly-serine using a sequence alignment (Norman Stein)
-  PDB\_MERGE: jiffy to merge two PDB files (Martyn Winn)
-  PHASER 1.3: molecular replacement (Randy Read et al)
-  PIRATE (cpirate): statistical phase improvement (Kevin Cowtan)
-  SUPERPOSE: secondary structure alignment (Eugene Krissinel)

--------------

Graphical User Interface:

-  Version 1.4.4
-  AREAIMOL: new option to generate pairwise area differences for
   "molecules" (defined as arbitrary groups of chains) in a PDB file.
-  CRANK: interface to automated structure solution via experimental
   phasing (currently SAD, SIR and SIRAS data)
-  SHELX C/D/E: interface to running the SHELX C, D and E programs
   standalone or as a pipeline in order to perform heavy atom search and
   phasing
-  CHAINSAW: interface to new program of the same {name;} mutates a pdb
   file to poly-serine using a sequence alignment for molecular
   replacement
-  Scale and Merge Intensities (SCALA): option to output Scalepack
   formatted reflection files suitable for input into SHELX or SOLVE
   (corresponds to OUTPUT POLISH UNMERGED option of SCALA)
-  Run Remote Jobs: option to use either rsh or ssh to run jobs on
   remote machines.
-  Output in different map formats: the interface should now only offer
   output in the formats (O, QUANTA) that can actually be created with
   the user's setup
-  CreateLabinLine4: extended version of CreateLabinLine that allows
   groups of up to four related labels to be specified, for example H-L
   coefficients or F(+)/sigF(+) and F(-)/sigF(-) groups. This command
   also supports a -command option similar to other CreateLine-type
   commands. See the documentation for details.
-  FindExecutable: new command, return full path of a program executable
-  Install Task: export task now allows save/restore of {parameters;}
   tasks can be defined as basename/directory pairs; version number
   included in archive filename.
-  Logfile viewer: when viewing a long file, there is now the option to
   look at the previous frame as well as the next one.
-  MTZ handling: new commands GroupMtzCols, MtzColSameDataset,
   GetMtzColType, GetMtzGroupByType.
-  WarningMessage: has new options -image, -font and -color
-  Superpose: when fitting atoms from one PDB file to another, atom
   numbers given by the user now refer to those specified in the file
   (rather than to the positions in the file)
-  types.def: added new types \_res\_file (ShelxD result file) and
   "\_phs\_file" (XtalView phases file)
-  File Selection for Windows: drive can also be chosen from the file
   browser.
-  Disabled tasks: some task interfaces will be unavailable to users if
   "prerequisites" (as defined in the taskname\_prereq procedure in the
   task interface file) are not found on the user's system. "Disabled"
   means that the task button will be non-functional and the text will
   be greyed-out. Check the documentation
   $CCP4I\_HELP/general/additional.html for information on how to enable
   these tasks.
-  Project Switching: Having a small menu button enabling to instantly
   switch between projects without passing through "directories"
   interface
-  Database search/sort: Having a new Button search/sort popping up a
   window enabling users to do various kinds of simple/advanced search
   and/or sorting also be able to refine sorts or searches.
-  CreateToggleFrame: new option -justify allows programmer to specify
   the positioning of the subframes (left, right or center).

--------------

Documentation:

-  Added documentation on adding doc comments for CCP4i core commands
   ($CCP4I\_HELP/progdocs/documenting.html)
-  alternate\_origins.html: updated with spacegroup numbers, information
   about alternative settings and Hermann-Mauguin symbols
-  Loggraphs: documentation for TABLE, GRAPHS, SCATTER etc now in
   loggraphformat.html (see "File Formats" section)

--------------

Withdrawal:

-  fft (replaced by fftbig )

--------------

Examples:

-  abs, bp3, chainsaw, pdb\_merge, phaser, pirate, superpose examples
   added
-  New examples for using python interface to cctbx and phaser
   (cctbx\_py\_test.exam and phaser\_py\_test.exam)
