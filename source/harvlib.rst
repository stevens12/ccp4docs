HARVLIB (CCP4: Library)
=======================

NAME
----

Harvlib - Subroutine library for writing CIF harvest files.

OVERVIEW
--------

The following subroutines are used by certain CCP4 programs when writing
out harvest files. These subroutines call functions in the CCIF library.

DESCRIPTION OF ROUTINES
-----------------------

SUBROUTINE Hatom\_type\_scat(AtName,a1,a2,a3,a4,b1,b2,b3,b4,c)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Character                                                                |
| AtName\*(\*)                                                             |
+--------------------------------------------------------------------------+
| Real                                                                     |
| a1,a2,a3,a4,b1,b2,b3,b4,c                                                |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_atom\_type.symbol                                                      |
| Element symbol of atom specie(s) representing this atom type.            |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_a1                                      |
| The a1 Cromer-Mann scattering-factor coefficient used to calculate the   |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_a2                                      |
| The a2 Cromer-Mann scattering-factor coefficient used to calculate the   |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_a3                                      |
| The a3 Cromer-Mann scattering-factor coefficient used to calculate the   |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_a4                                      |
| The a4 Cromer-Mann scattering-factor coefficient used to calculate the   |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_b1                                      |
| The b1 Cromer-Mann scattering-factor coefficient used to calculate the   |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_b2                                      |
| The b2 Cromer-Mann scattering-factor coefficient used to calculate the   |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_b3                                      |
| The b3 Cromer-Mann scattering-factor coefficient used to calculate the   |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_b4                                      |
| The b4 Cromer-Mann scattering-factor coefficient used to calculate the   |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+
| \_atom\_type.scat\_Cromer\_Mann\_c                                       |
| The c Cromer-Mann scattering-factor coefficient used to calculate the    |
| scattering factors for this atom type.                                   |
+--------------------------------------------------------------------------+

| 

SUBROUTINE HCell(Cell)
~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Cell(6)                                                                  |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_cell.length\_a                                                         |
| Unit-cell length a corresponding to the structure reported.              |
+--------------------------------------------------------------------------+
| \_cell.length\_b                                                         |
| Unit cell length b corresponding to the structure reported.              |
+--------------------------------------------------------------------------+
| \_cell.length\_c                                                         |
| Unit cell length c corresponding to the structure reported.              |
+--------------------------------------------------------------------------+
| \_cell.length\_alpha                                                     |
| Unit cell angle alpha corresponding to the structure reported.           |
+--------------------------------------------------------------------------+
| \_cell.length\_beta                                                      |
| Unit cell angle beta corresponding to the structure reported.            |
+--------------------------------------------------------------------------+
| \_cell.length\_gamma                                                     |
| Unit cell angle gamma corresponding to the structure reported.           |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hcell\_percent\_solvent(Fraction)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Fraction                                                                 |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_exptl\_crystal.percent\_solvent                                        |
| Writes the fraction of cell occupied by solvent calculated from crystal  |
| cell and contents, expressed as percent solvent.                         |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hdata\_reduction\_method(Method,Nlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nlines                                                                   |
+--------------------------------------------------------------------------+
| Character                                                                |
| Method(Nlines)\*80                                                       |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_reflns.data\_reduction\_method                                         |
| The method used in reducing the data. Note that this is not the computer |
| program used, which is described in the SOFTWARE category, but the       |
| method itself.                                                           |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hdensity\_Matthews(Fraction)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Fraction                                                                 |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_exptl\_crystal.density\_Matthews                                       |
| The density of the crystal, expressed as the ratio of the volume of the  |
| asymmetric unit to the molecular mass of a monomer of the structure, in  |
| units of angstroms^3^ per dalton. Ref: Matthews, B. W. (1960). J. Mol.   |
| Biol., 33, 491-497                                                       |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hinitialise(Pkage,ProgName,ProgVersion,ProjectName,DataSetName,UseCWD,Private, IVALND,VALND,RowLimit)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| VALND                                                                    |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| IVALND,RowLimit                                                          |
+--------------------------------------------------------------------------+
| Logical                                                                  |
| Private,UseCWD                                                           |
+--------------------------------------------------------------------------+
| Character                                                                |
| DataSetName\*(\*),ProjectName\*(\*),ProgVersion\*(\*),ProgName\*(\*),Pka |
| ge\*(\*)                                                                 |
+--------------------------------------------------------------------------+

**Does the following:**

Checks if $HARVESTHOME/DepositFiles directory exists and permissions;
creates filename for deposit information; sets Project Name and Dataset
Name;

| 

SUBROUTINE Hmerge\_reject\_criterion(Rcriteria,Nlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nlines                                                                   |
+--------------------------------------------------------------------------+
| Character                                                                |
| Rcriteria(Nlines)\*80                                                    |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_reflns.merge\_reject\_criterion                                        |
| Criteria used in averaging equivalent Intensities during the data        |
| reduction stage to generate the set of reduced unique data. The          |
| description of the rejection criteria for outliers that was used may     |
| include the criteria for either the scaling and for the merging of a set |
| of observations.                                                         |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hoverall\_observations(Ntotal,R1,R2)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| Ntotal                                                                   |
+--------------------------------------------------------------------------+
| Real                                                                     |
| R1,R2                                                                    |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_reflns.overall\_d\_resn\_high                                          |
| The highest resolution for the interplanar spacing for the number of     |
| reflections processed from the complete set of images collected. This is |
| the smallest d value. Units (Angstroms or nanometres) should be          |
| specified.                                                               |
+--------------------------------------------------------------------------+
| \_reflns.overall\_d\_res\_low                                            |
| The lowest resolution for the interplanar spacing for the number of      |
| reflections processed from the complete set of images collected. This is |
| the largest d value. Units (Angstroms or nanometres) should be           |
| specified.                                                               |
+--------------------------------------------------------------------------+
| \_reflns.overall\_number\_observations                                   |
| The total number of observations of reflections processed from the       |
| complete set of images collected within the resolution limits            |
| \_reflns.overall\_d\_res\_low and \_reflns.overall\_d\_res\_high. This   |
| is a count of the number of experimental measurements of Bragg           |
| intensities made in the experiment, not the number of merged (unique)    |
| reflections. The value of \_reflns.observed\_criterion DOES NOT apply to |
| this set of reflections.                                                 |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hparse(line,Ibeg,Iend,Ityp,Fvalue,Cvalue,Idec,N)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Character                                                                |
| line                                                                     |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| Ibeg(\*),Idec(\*),Iend(\*),Ityp(\*)                                      |
+--------------------------------------------------------------------------+
| Character                                                                |
| Cvalue(\*)\*4                                                            |
+--------------------------------------------------------------------------+

**Does the following:**

Free format read routine. This is really a scanner, not a parser. It
scans the line into N tokens which are separated by delimiters and
updates the information arrays for each, as below. The default
delimiters are space, tab, comma and equals; they may be changed using
PARSDL. Adjacent commas delimit \`null' fields (the same as empty
strings). strings may be unquoted or single- or double-quoted if they
don't contain delimiters, but must be surrounded by delimiters to be
recognised. This allows literal quotes to be read, e.g. "ab"c" will be
recognised as the token \`ab"c'. An unquoted \`!' or \`#' in line
introduces a trailing comment, which is ignored.

| 

SUBROUTINE Hphasing\_mir\_der\_p2(Power,RCullis,Reflns)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Resomin(2,\*),Resoman(2,\*),Criteria(\*)                                 |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| NumSitesDer(\*)                                                          |
+--------------------------------------------------------------------------+
| Character                                                                |
| DerID(\*)\*80                                                            |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.id                                                   |
| The value of \_phasing\_MIR\_der.id must uniquely identify a record in   |
| the PHASING\_MIR\_DER list. Note that this item need not be a number; it |
| can be any unique identifier.                                            |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.number\_of\_sites                                    |
| The number of heavy atom sites in this derivative.                       |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.d\_res\_high                                         |
| The highest resolution for the interplanar spacing in the reflection     |
| data used for this derivative. This is the smallest d value.             |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.d\_res\_low                                          |
| The lowest resolution for the interplanar spacing in the reflection data |
| used for this derivative. This is the highest d value.                   |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.ceflns\_criteria                                     |
| Criteria used to limit the reflections used in the phasing calculations. |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.Power\_centric                                       |
| The mean phasing power P for centric reflections in this derivative.     |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.Power\_acentric                                      |
| The mean phasing power P for acentric reflections in this derivative.    |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.R\_cullis\_centric                                   |
| Residual factor R~cullis~ for centric reflections in this derivative.    |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.R\_cullis\_acentric                                  |
| Residual factor R~cullis,acen~ for acentric reflections in this          |
| derivative.                                                              |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.R\_cullis\_anomalous                                 |
| Residual factor R~cullis,ano~ for anomalous reflections in this          |
| derivative.                                                              |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.Reflns\_acentric                                     |
| The number of acentric reflections used in phasing for this derivative.  |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.Reflns\_anomalous                                    |
| The number of anomalous reflections used in phasing for this derivative. |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der.Reflns\_centric                                      |
| The number of centric reflections used in phasing for this derivative.   |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hphasing\_mir\_der\_site(DerID,NumDerSites,B,Atype,X,Y,Z,Occ,OccEsd,anom,AnomEsd)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| anom(\*),AnomEsd(\*),B(\*),Occ(\*),OccEsd(\*),X(\*),Y(\*),Z(\*)          |
+--------------------------------------------------------------------------+
| Character                                                                |
| Atype(\*)\*4                                                             |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.der\_id                                        |
| This data item is a pointer to \_phasing\_MIR\_der.id in the             |
| PHASING\_MIR\_DER category.                                              |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.atom\_type\_symbol                             |
| This data item is a pointer to \_atom\_type.symbol in the ATOM\_TYPE     |
| category. The scattering factors referenced via this data item should be |
| those used in the refinement of the heavy atom data; in some cases this  |
| is the scattering factor to the single heavy atom, in others these are   |
| the scattering factors for an atomic cluster.                            |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.fract\_x                                       |
| The x coordinate of this heavy-atom position in this derivative          |
| specified as a fraction of \_cell.length\_a.                             |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.fract\_y                                       |
| The y coordinate of this heavy-atom position in this derivative          |
| specified as a fraction of \_cell.length\_b.                             |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.fract\_z                                       |
| The z coordinate of this heavy-atom position in this derivative          |
| specified as a fraction of \_cell.length\_c.                             |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.B\_iso                                         |
| Isotropic temperature factor for this heavy-atom site in this            |
| derivative.                                                              |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.Occupancy\_iso                                 |
| The relative real isotropic occupancy of the atom type present at this   |
| heavy-atom site in a given derivative. This atom occupancy will probably |
| be on an arbitrary scale.                                                |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.Occupancy\_iso\_su                             |
| The standard uncertainty (e.s.d.) of                                     |
| \_phasing\_MIR\_der\_site.occupancy\_iso.                                |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.Occupancy\_anom                                |
| The relative anomalous occupancy of the atom type present at this        |
| heavy-atom site in a given derivative. This atom occupancy will probably |
| be on an arbitrary scale.                                                |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_der\_site.Occupancy\_anom\_su                            |
| The standard uncertainty (e.s.d.) of                                     |
| \_phasing\_MIR\_der\_site.occupancy\_anom.                               |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hphasing\_mir\_native(R1,R2,SigmaNat,fomT,fomC,fomA,Mt,Mc,Ma)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| R1,R2,fomT,fomC,fomA,SigmaNat                                            |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| Mt,Mc,Ma                                                                 |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_phasing\_MIR.entry\_id                                                 |
| This data item is a pointer to \_entry.id in the ENTRY category.         |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.d\_res\_high                                              |
| The highest resolution in angstroms for the interplanar spacing in the   |
| reflection data used for the native data set. This is the smallest d     |
| value.                                                                   |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.d\_res\_low                                               |
| The lowest resolution in angstroms for the interplanar spacing in the    |
| reflection data used for the native data set. This is the largest d      |
| value.                                                                   |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.FOM                                                       |
| The mean value of the figure of merit m for all reflections phased in    |
| the native data set.                                                     |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.FOM\_centric                                              |
| The mean value of the figure of merit m for the centric reflections      |
| phased in the native data set.                                           |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.FOM\_acentric                                             |
| The mean value of the figure of merit m for the acentric reflections     |
| phased in the native data set.                                           |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.reflns                                                    |
| The total number of reflections phased in the native data set.           |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.reflns\_centric                                           |
| The number of centric reflections phased in the native data set.         |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.reflns\_acentric                                          |
| The number of acentric reflections phased in the native data set.        |
+--------------------------------------------------------------------------+
| \_phasing\_MIR.reflns\_criterion                                         |
| Criterion used to limit the reflections used in the phasing              |
| calculations.                                                            |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hphasing\_mir\_native\_shell(R1,R2,KRa,fomA,KRc,fomC,KRo,fomO)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| R1,R2,KRa,fomA,KRc,fomC,KRo,fomO                                         |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| KRa,KRc,KRo                                                              |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_phasing\_MIR\_shell.d\_res\_high                                       |
| The highest resolution for the interplanar spacing in the reflection     |
| data in this shell. This is the smallest d value. Note that the          |
| resolution limits of shells in the items                                 |
| \_phasing\_MIT\_Shell.d\_res\_high and \_phasing\_MIR\_shell.d\_res\_low |
| are independent of the resolution limits of shells in the items          |
| \_reflns\_shell.d\_res\_high and \_reflns\_shell.d\_res\_low.            |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_shell.d\_res\_low                                        |
| The lowest resolution for the interplanar spacing in the reflection data |
| in this shell. This is the largest d value. Note that the resolution     |
| limits of shells in the items \_phasing\_MIT\_Shell.d\_res\_high and     |
| \_phasing\_MIR\_shell.d\_res\_low are independent of the resolution      |
| limits of shells in the items \_reflns\_shell.d\_res\_high and           |
| \_reflns\_shell.d\_res\_low.                                             |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_shell.FOM                                                |
| The mean value of the figure of merit m for reflections in this shell.   |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_shell.FOM\_centric                                       |
| The mean value of the figure of merit m for centric reflections in this  |
| shell.                                                                   |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_shell.FOM\_acentric                                      |
| The mean value of the figure of merit m for acentric reflections in this |
| shell.                                                                   |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_shell.reflns                                             |
| The number of reflections in this shell.                                 |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_shell.reflns\_centric                                    |
| The number of centric reflections in this shell.                         |
+--------------------------------------------------------------------------+
| \_phasing\_MIR\_shell.reflns\_acentric                                   |
| The number of acentric reflections in this shell.                        |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_corr\_esu(Corr,FreeCorr,DPI,FreeESU,Good,GoodFree,ESUml,bESU)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Corr,FreeCorr,DPI,FreeESU,Good,GoodFree,ESUml,bESU                       |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| Lenstr                                                                   |
+--------------------------------------------------------------------------+

| 

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.correlation\_coeff\_Fo\_to\_Fc                                  |
| The correlation coefficient between the observed and calculated          |
| structure factors for reflections included in the refinement.            |
+--------------------------------------------------------------------------+
| \_refine.correlation\_coeff\_Fo\_to\_Fc\_Free                            |
| The correlation coefficient between the observed and calculated          |
| structure factors for reflections not included in the refinement (free   |
| reflections).                                                            |
+--------------------------------------------------------------------------+
| \_refine.goodness\_of\_fit\_work                                         |
| NO DESCRIPTION                                                           |
+--------------------------------------------------------------------------+
| \_refine.goodness\_of\_fit\_FreeR                                        |
| NO DESCRIPTION                                                           |
+--------------------------------------------------------------------------+
| \_refine.overall\_SU\_ML                                                 |
| The overall standard uncertainty (e.s.d.) of the positional parameters   |
| based on a maximum likelihood residual.                                  |
+--------------------------------------------------------------------------+
| \_refine.overall\_SU\_B                                                  |
| The overall standard uncertainty (e.s.d.) of the thermal parameters      |
| based on a maximum likelihood residual.                                  |
+--------------------------------------------------------------------------+
| \_refine.overall\_SU\_R\_Cruickshank\_DPI                                |
| The overall standard uncertainty (e.s.d.) of the thermal parameters      |
| based on the crystallographic R value, expressed in a formalism known as |
| the dispersion precision indicator (DPI).                                |
+--------------------------------------------------------------------------+
| \_refine.overall\_SU\_R\_free                                            |
| The overall standard uncertainty (e.s.d.) of the thermal parameters      |
| based on the free R value.                                               |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_details(NHlines,Hlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| NHlines                                                                  |
+--------------------------------------------------------------------------+
| Character                                                                |
| Hlines(NHlines)\*80                                                      |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.ls\_weighting\_details                                          |
| A description of special aspects of the weighting scheme used in         |
| least-squares refinement. Used to describe the weighting when the value  |
| of \_refine.ls\_weighting\_scheme is specified as 'calc'.                |
+--------------------------------------------------------------------------+
| \_refine.ls\_weighting\_scheme                                           |
| The weighting scheme applied in the least-squares process. The standard  |
| code may be followed by a description of the weight (but see             |
| \_refine\_ls\_weighting\_details for a preferred approach).              |
+--------------------------------------------------------------------------+
| \_refine.details                                                         |
| Description of special aspects of the refinement process.                |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_fnmin(Nval,Nterms,Vterms,Cterms,Wterms)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Vterms(\*),Wterms(\*)                                                    |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nterms(\*)                                                               |
+--------------------------------------------------------------------------+
| Character                                                                |
| Cterms(Nval)\*80                                                         |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine\_funct\_minimized.type                                          |
| The type of the function being minimized.                                |
+--------------------------------------------------------------------------+
| \_refine\_funct\_minimized.number\_terms                                 |
| The number of observations in this term. For example, if the term is a   |
| residual of the X-ray intensities this item would contain the number of  |
| reflections used in the refinement.                                      |
+--------------------------------------------------------------------------+
| \_refine\_funct\_minimized.residual                                      |
| The residual for this term of the function which was minimized in        |
| refinement.                                                              |
+--------------------------------------------------------------------------+
| \_refine\_funct\_minimized.weight                                        |
| The weight applied to this term of the function which was minimized in   |
| the refinement.                                                          |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_fom(fom,Freefom)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| fom,Freefom                                                              |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.overall\_FOM\_work\_R\_set                                      |
| Average figure of merit of phases of reflections not included in the     |
| refinement. This value is derived from the likelihood function.          |
+--------------------------------------------------------------------------+
| \_refine.overall\_FOM\_free\_R\_set                                      |
| Average figure of merit of phases of reflections included in the         |
| refinement. This value is derived from the likelihood function.          |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_ls\_matrix\_type(STRING)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Character                                                                |
| String\*(\*)                                                             |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.ls\_matrix\_type                                                |
| Type of matrix used to accumulate the least-squares derivatives.         |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_ls\_overall\_reso(R1,R2)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| R1,R2                                                                    |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.ls\_d\_res\_high                                                |
| The highest resolution in angstroms for the interplanar spacing in the   |
| reflection data used in refinement. This is the smallest d value.        |
+--------------------------------------------------------------------------+
| \_refine.ls\_d\_res\_low                                                 |
| The lowest resolution in angstroms for the interplanar spacing in the    |
| reflection data used in refinement. This is the largest d value.         |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_NparNrestNconstr(NPARMR,NRESTR,NCONSTR)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| NPARMR,NRESTR,NCONSTR                                                    |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.ls\_number\_parameters                                          |
| The number of parameters refined in the least-squares process. If        |
| possible this number should include some contribution from the           |
| restrained parameters. The restrained parameters are distinct from the   |
| constrained parameters (where one or more parameters are linearly        |
| dependent on the refined value of another). Least-squares restraints     |
| often depend on geometry or energy considerations and this makes their   |
| direct contribution to this number, and to the goodness-of-fit           |
| calculation, difficult to assess.                                        |
+--------------------------------------------------------------------------+
| \_refine.ls\_number\_restraints                                          |
| The number of restrained parameters. These are parameters which are not  |
| directly dependent on another refined parameter. Often restrained        |
| parameters involve geometry or energy dependencies. See also             |
| \_atom\_site.constraints and \_atom\_site\_refinement\_flags. A general  |
| description of refinement constraints may appear in \_refine.details.    |
+--------------------------------------------------------------------------+
| \_refine.ls\_number\_constraints                                         |
| The number of constrained (non-refined or dependent) parameters in the   |
| least-squares process. These may be due to symmetry or any other         |
| constraint process (e.g. rigid-body refinement). See also                |
| \_atom\_site.constraints and \_atom\_site\_refinement\_flags. A general  |
| description of constraints may appear in \_refine.details.               |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_obscriteria(Criteria)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Criteria                                                                 |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_reflns.observed\_criterion                                             |
| The criterion used to classify a reflection as 'observed'. This          |
| criterion is usually expressed in terms of a sigma(I) or sigma(F)        |
| threshold.                                                               |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_restraints(Rtype,Num,rmsd,sigd,criteria,Nreject)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Character                                                                |
| Rtype\*(\*),criteria\*(\*)                                               |
+--------------------------------------------------------------------------+
| Integer Num,Nreject                                                      |
+--------------------------------------------------------------------------+
| Real rmsd,sigd                                                           |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine\_ls\_restr.type                                                 |
| The type of the parameter being restrained. Explicit sets of data values |
| are provided for the programs PROTIN/PROLSQ (beginning with p\_) and     |
| RESTRAIN (beginning with RESTRAIN\_). As computer programs will evolve,  |
| these data values are given as examples, and not as an enumeration list. |
| Computer programs converting a data block to a refinement table will     |
| expect the exact form of the data values given here to be used.          |
+--------------------------------------------------------------------------+
| \_refine\_ls\_restr.number                                               |
| The number of parameters of this type subjected to restraint in          |
| least-squares refinement.                                                |
+--------------------------------------------------------------------------+
| \_refine\_ls\_restr.dev\_ideal                                           |
| For the given parameter type, the root-mean-square deviation between the |
| ideal values used as restraints in the least-squares refinement and the  |
| values obtained by refinement. For instance, bond distances may deviate  |
| by 0.018 \\%A (r.m.s.) from ideal values in current model.               |
+--------------------------------------------------------------------------+
| \_refine\_ls\_restr.dev\_ideal\_target                                   |
| For the given parameter type, the target root-mean-square deviation      |
| between the ideal values used as restraints in the least-squares         |
| refinement and the values obtained by refinement.                        |
+--------------------------------------------------------------------------+
| \_refine\_ls\_restr.rejects                                              |
| The number of parameters of this type that deviate from ideal values by  |
| more than the amount defined in \_refine\_ls\_restr.criterion in the     |
| model obtained by restrained least-squares refinement.                   |
+--------------------------------------------------------------------------+
| \_refine\_ls\_restr.criterion                                            |
| A criterion used to define a parameter value that deviates significantly |
| from its ideal value in the model obtained by restrained least-squares   |
| refinement.                                                              |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_rfacts(Nall,Nobs,Nmiss,Nwork,Nfree,NfreeMiss,PercentObs,PercentFree,Rall,Robs,Rwork,Rfree, Wall,Wobs,Wwork,Wfree)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nall,Nobs,Nmiss,Nwork,Nfree,NfreeMiss                                    |
+--------------------------------------------------------------------------+
| Real                                                                     |
| PercentObs,PercentFree,Rall,Robs,Rwork,Rfree,Wall,Wobs,Wwork,Wfree       |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.ls\_number\_reflns\_all                                         |
| The number of reflections that satisfy the resolution limits established |
| by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low.               |
+--------------------------------------------------------------------------+
| \_refine.ls\_number\_reflns\_obs                                         |
| The number of reflections that satisfy the resolution limits established |
| by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low and the        |
| observation limit established by \_reflns.observed\_criterion.           |
+--------------------------------------------------------------------------+
| \_refine.ls\_number\_reflns\_R\_work                                     |
| The number of reflections that satisfy the resolution limits established |
| by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low and the        |
| observation limit established by \_reflns.observed\_criterion, and that  |
| were used as the working (i.e. included in refinement) reflections when  |
| refinement included calculation of a "free" R factor. Details of how     |
| reflections were assigned to the working and test sets are given in      |
| \_reflns.R\_free\_details.                                               |
+--------------------------------------------------------------------------+
| \_refine.ls\_number\_reflns\_R\_free                                     |
| The number of reflections that satisfy the resolution limits established |
| by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low and the        |
| observation limit established by \_reflns.observed\_criterion, and that  |
| were used as the test (i.e. excluded from refinement) reflections when   |
| refinement included calculation of a "free" R factor. Details of how     |
| reflections were assigned to the working and test sets are given in      |
| \_reflns.R\_free\_details.                                               |
+--------------------------------------------------------------------------+
| \_refine.ls\_percent\_reflns\_obs                                        |
| The number of reflections that satisfy the resolution limits established |
| by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low and the        |
| observation limit established by \_reflns.observed\_criterion, expressed |
| as a percentage of the number of geometrically observable reflections    |
| that satisfy the resolution limits.                                      |
+--------------------------------------------------------------------------+
| \_refine.ls\_percent\_reflns\_R\_free                                    |
| The number of reflections that satisfy the resolution limits established |
| by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low and the        |
| observation limit established by \_reflns.observed\_criterion, and that  |
| were used as the test (i.e. excluded from refinement) reflections when   |
| refinement included calculation of a "free" R factor, espressed as a     |
| percentage of the number of geometrically observable reflections that    |
| satisfy the resolution limits.                                           |
+--------------------------------------------------------------------------+
| \_refine.ls\_R\_factor\_all                                              |
| Residual factor R for all reflections that satisfy the resolution limits |
| established by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low.   |
+--------------------------------------------------------------------------+
| \_refine.ls\_R\_factor\_obs                                              |
| Residual factor R for reflections that satisfy the resolution limits     |
| established by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low    |
| and the observation limit established by \_reflns.observed\_criterion.   |
| \_refine.ls\_R\_factor\_obs should not be confused with                  |
| \_refine.ls\_R\_factor\_R\_work; the former reports the results of a     |
| refinement in which all observed reflections were used, the latter a     |
| refinement in which a subset of the observed reflections were excluded   |
| from refinement for the calculation of a "free" R factor. However, it    |
| would be meaningful to quote both values if a "free" R factor were       |
| calculated for most of the refinement, but all of the observed           |
| reflections were used in the final rounds of refinement; such a protocol |
| should be explained in \_refine.details.                                 |
+--------------------------------------------------------------------------+
| \_refine.ls\_R\_factor\_R\_work                                          |
| Residual factor R for reflections that satisfy the resolution limits     |
| established by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low    |
| and the observation limit established by \_reflns.observed\_criterion,   |
| and that were used as the working (i.e., included in refinement)         |
| reflections when refinement included calculation of a "free" R factor.   |
| Details of how reflections were assigned to the working and test sets    |
| are given in \_reflns.R\_free\_details. \_refine.ls\_R\_factor\_obs      |
| should not be confused with \_refine.ls\_R\_factor\_R\_work; the former  |
| reports the results of a refinement in which all observed reflections    |
| were used, the latter a refinement in which a subset of the observed     |
| reflections were excluded from refinement for the calculation of a       |
| "free" R factor. However, it would be meaningful to quote both values if |
| a "free" R factor were calculated for most of the refinement, but all of |
| the observed reflections were used in the final rounds of refinement;    |
| such a protocol should be explained in \_refine.details.                 |
+--------------------------------------------------------------------------+
| \_refine.ls\_R\_factor\_R\_free                                          |
| Residual factor R for reflections that satisfy the resolution limits     |
| established by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low    |
| and the observation limit established by \_reflns.observed\_criterion,   |
| and that were used as the test (i.e., excluded from refinement)          |
| reflections when refinement included calculation of a "free" R factor.   |
| Details of how reflections were assigned to the working and test sets    |
| are given in \_reflns.R\_free\_details.                                  |
+--------------------------------------------------------------------------+
| \_refine.ls\_wR\_factor\_all                                             |
| Weighted residual factor wR for all reflections that satisfy the         |
| resolution limits established by \_refine.ls\_d\_res\_high and           |
| \_refine.ls\_d\_res\_low.                                                |
+--------------------------------------------------------------------------+
| \_refine.ls\_wR\_factor\_obs                                             |
| Weighted residual factor wR for reflections that satisfy the resolution  |
| limits established by \_refine.ls\_d\_res\_high and                      |
| \_refine.ls\_d\_res\_low and the obervation limit established by         |
| \_reflns.observed\_criterion.                                            |
+--------------------------------------------------------------------------+
| \_refine.ls\_wR\_factor\_R\_work                                         |
| Weighted residual factor wR for reflections that satisfy the resolution  |
| limits established by \_refine.ls\_d\_res\_high and                      |
| \_refine.ls\_d\_res\_low and the observation limit established by        |
| \_reflns.observed\_criterion, and that were used as the working (i.e.,   |
| included in refinement) reflections when refinement included calculation |
| of a "free" R factor. Details of how reflections were assigned to the    |
| working and test sets are given in \_reflns.R\_free\_details.            |
+--------------------------------------------------------------------------+
| \_refine.ls\_wR\_factor\_R\_free                                         |
| Weighted residual factor wR for reflections that satisfy the resolution  |
| limits established by \_refine.ls\_d\_res\_high and                      |
| \_refine.ls\_d\_res\_low and the observation limit established by        |
| \_reflns.observed\_criterion, and that were used as the test (i.e.,      |
| excluded from refinement) reflections when refinement included           |
| calculation of a "free" R factor. Details of how reflections were        |
| assigned to the working and test sets are given in                       |
| \_reflns.R\_free\_details.                                               |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_rg(RDhigh,RDlow,RGall,RGwork,RGfree)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| RGall,RGwork,RGfree,RDhigh,RDlow                                         |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine\_analyze.RG\_d\_res\_high                                       |
| The value of the high-resolution cutoff in angstroms used in calculation |
| of the Hamilton generalized R factor (RG) stored in                      |
| refine\_analyze.RG\_work and \_refine\_analyze.RG\_free.                 |
+--------------------------------------------------------------------------+
| \_refine\_analyze.RG\_d\_res\_low                                        |
| The value of the low-resolution cutoff in angstroms used in calculation  |
| of the Hamilton generalized R factor (RG) stored in                      |
| refine\_analyze.RG\_work and \_refine\_analyze.RG\_free.                 |
+--------------------------------------------------------------------------+
| \_refine\_analyze.RG\_all                                                |
| The Hamilton generalised R-factor (see W.C. Hamilton (1965) Acta Cryst., |
| 18, 502-510. ) for all reflections that satisfy the resolution limits    |
| established by \_refine.ls\_d\_res\_high and \_refine.ls\_d\_res\_low.   |
+--------------------------------------------------------------------------+
| \_refine\_analyze.RG\_work                                               |
| The Hamilton generalized R factor for all reflections that satisfy the   |
| resolution limits established by \_refine\_analyze.RG\_d\_res\_high and  |
| \_refine\_analyze.RG\_d\_res\_low and for those reflections included in  |
| the working set when a free R set of reflections are omitted from the    |
| refinement.                                                              |
+--------------------------------------------------------------------------+
| \_refine\_analyze.RG\_free                                               |
| The Hamilton generalized R factor for all reflections that satisfy the   |
| resolution limits established by \_refine\_analyze.RG\_d\_res\_high and  |
| \_refine\_analyze.RG\_d\_res\_low for the free R set of reflections that |
| were excluded from the refinement.                                       |
+--------------------------------------------------------------------------+
| \_refine\_analyze.RG\_free\_work\_ratio                                  |
| The observed ratio of RGfree to RGwork. The expected RG ratio is the     |
| value that should be achievable at the end of a structure refinement     |
| when only random uncorrelated errors exist in data and model provided    |
| that the observations are properly weighted. When compared with the      |
| observed RG ratio it may indicate that a structure has not reached       |
| convergence or a model has been over-refined with no corresponding       |
| improvement in the model.                                                |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_rmsobs2dict(Rtype,Clow,Chigh,Usigma)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Character                                                                |
| Rtype\*(\*)                                                              |
+--------------------------------------------------------------------------+
| Real                                                                     |
| Clow,Chigh,Usigma                                                        |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine\_ls\_restr\_type.type                                           |
| This data item is a pointer to \_refine\_ls\_restr.type in the           |
| REFINE\_LS\_RESTR category.                                              |
+--------------------------------------------------------------------------+
| \_refine\_ls\_restr\_type.distance\_cutoff\_low                          |
| The lower limit in angstroms of the distance range applied to the        |
| current restraint type.                                                  |
+--------------------------------------------------------------------------+
| \_refine\_ls\_restr\_type.distance\_cutoff\_high                         |
| The upper limit in angstroms of the distance range applied to the        |
| current restraint type.                                                  |
+--------------------------------------------------------------------------+
| \_refine\_ls\_restr\_type.U\_sigma\_weights                              |
| \_refine\_ls\_restr.U\_sigma\_weights is a coefficient used in the       |
| calculation of the weight for thermal parameter restraints in the        |
| program RESTRAIN. The equation used to calculate the actual weight from  |
| this coefficient depends upon the value of \_refine\_ls\_restr.type -    |
| either "r.m.s. diffs for Uiso atoms at distance \*" or "r.m.s. diffs for |
| Uaniso atoms at distance \*". A similarity restraint is applied to the   |
| thermal parameters of any pair of atoms which are subject to an          |
| interatomic distance restraint (i.e. 1-2 and 1-3 bonded atoms). The      |
| thermal parameter restraints are categorized by the distance between the |
| two affected atoms. The expected r.m.s. differences in thermal           |
| parameter, either Uiso or Uaniso, are listed for each shell in           |
| \_refine\_ls\_restr.rmsdev\_dictionary.                                  |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_shell(Rlow,Rhigh,NrefAll,NrefObs,Nmiss,NrefWork,PercentObs,RfacAll,RfacObs,Nfree,Rfree,Rwork,WgtRfac, WgtRobs,WgtRwork,WgtUsed,Wexpress)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Rlow,Rhigh,PercentObs,RfacObs,RfacAll,Rfree,Rwork,WgtRfac,WgtRobs,WgtRwo |
| rk,Wgtused,Wexpress                                                      |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| NrefAll,NrefObs,Nmiss,NrefWork,Nfree                                     |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.d\_res\_high                                         |
| The highest resolution for the interplanar spacing in the reflection     |
| data in this shell. This is the largest d value.                         |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.d\_res\_low                                          |
| The lowest resolution for the interplanar spacing in the reflection data |
| in this shell. This is the smallest d value.                             |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.number\_reflns\_all                                  |
| The number of reflections that satisfy the resolution limits established |
| by \_refine\_ls\_shell.d\_res\_high and \_refine\_ls\_shell.d\_res\_low. |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.number\_reflns\_obs                                  |
| The number of reflections that satisfy the resolution limits established |
| by \_refine\_ls\_shell.d\_res\_high and \_refine\_ls\_shell.d\_res\_low  |
| and the observation criterion established by                             |
| \_reflns.observed\_criterion.                                            |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.number\_reflns\_R\_work                              |
| The number of reflections that satisfy the resolution limits established |
| by \_refine\_ls\_shell.d\_res\_high and \_refine\_ls\_shell.d\_res\_low  |
| and the observation limit established by \_reflns.observed\_criterion,   |
| and that were used as the working (i.e., included in refinement)         |
| reflections when refinement included calculation of a "free" R factor.   |
| Details of how reflections were assigned to the working and test sets    |
| are given in \_reflns.R\_free\_details.                                  |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.percent\_reflns\_obs                                 |
| The number of reflections that satisfy the resolution limits established |
| by \_refine\_ls\_shell.d\_res\_high and \_refine\_ls\_shell.d\_res\_low  |
| and the observation criterion established by                             |
| \_reflns.observed\_criterion, expressed as a percentage of the number of |
| geometrically observable reflections that satisfy the resolution limits. |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.R\_factor\_all                                       |
| Residual factor R for reflections that satisfy the resolution limits     |
| established by \_refine\_ls\_shell.d\_res\_high and                      |
| \_refine\_ls\_shell.d\_res\_low.                                         |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.R\_factor\_obs                                       |
| Residual factor R for reflections that satisfy the resolution limits     |
| established by \_refine\_ls\_shell.d\_res\_high and                      |
| \_refine\_ls\_shell.d\_res\_low and the observation criterion            |
| established by \_reflns.observed\_criterion.                             |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.number\_reflns\_R\_free                              |
| The number of reflections that satisfy the resolution limits established |
| by \_refine\_ls\_shell.d\_res\_high and \_refine\_ls\_shell.d\_res\_low  |
| and the observation limit established by \_reflns.observed\_criterion,   |
| and that were used as the test (i.e., excluded from refinement)          |
| reflections when refinement included calculation of a "free" R factor.   |
| Details of how reflections were assigned to the working and test sets    |
| are given in \_reflns.R\_free\_details.                                  |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.R\_factor\_R\_free                                   |
| Residual factor R for reflections that satisfy the resolution limits     |
| established by \_refine\_ls\_shell.d\_res\_high and                      |
| \_refine\_ls\_shell.d\_res\_low and the observation limit established by |
| \_reflns.observed\_criterion, and that were used as the test (i.e.,      |
| excluded from refinement) reflections when refinement included           |
| calculation of a "free" R factor. Details of how reflections were        |
| assigned to the working and test sets are given in                       |
| \_reflns.R\_free\_details.                                               |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.R\_factor\_R\_work                                   |
| Residual factor R for reflections that satisfy the resolution limits     |
| established by \_refine\_ls\_shell.d\_res\_high and                      |
| \_refine\_ls\_shell.d\_res\_low and the observation limit established by |
| \_reflns.observed\_criterion, and that were used as the working (i.e.,   |
| included in refinement) reflections when refinement included calculation |
| of a "free" R factor. Details of how reflections were assigned to the    |
| working and test sets are given in \_reflns.R\_free\_details.            |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.wR\_factor\_all                                      |
| Weighted residual factor wR for reflections that satisfy the resolution  |
| limits established by \_refine\_ls\_shell.d\_res\_high and               |
| \_refine\_ls\_shell.d\_res\_low.                                         |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.wR\_factor\_obs                                      |
| Weighted residual factor wR for reflections that satisfy the resolution  |
| limits established by \_refine\_ls\_shell.d\_res\_high and               |
| \_refine\_ls\_shell.d\_res\_low and the observation criterion            |
| established by \_reflns.observed\_criterion.                             |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.wR\_factor\_R\_work                                  |
| Weighted residual factor wR for reflections that satisfy the resolution  |
| limits established by \_refine\_ls\_shell.d\_res\_high and               |
| \_refine\_ls\_shell.d\_res\_low and the observation limit established by |
| \_reflns.observed\_criterion, and that were used as the working (i.e.,   |
| included in refinement) reflections when refinement included calculation |
| of a "free" R factor. Details of how reflections were assigned to the    |
| working and test sets are given in \_reflns.R\_free\_details.            |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.weight\_used                                         |
| The mean weighting used for the reflections in the resolution limits for |
| the shell. The weights used may for example include experimental sigmas  |
| in weighting; or by weighting using average diagonal term of Xray and    |
| geometry - same as PROLSQ where weighting equates                        |
| wmat\*average\_diagonal\_of\_geometry to average\_diagonal\_of\_Xray     |
| terms; or Weighting equates wgrad\*RMS\_gradient\_of\_geometry to        |
| RMS\_gradient\_of\_Xray terms; or The relative weighting for Xray and    |
| geom terms is governed by sigma. Wsigma applied to Xray terms as         |
| 1/(Wsigma\*\*2)                                                          |
+--------------------------------------------------------------------------+
| \_refine\_ls\_shell.weight\_exp                                          |
| NO DESCRIPTION IN CIF\_MM.DIC                                            |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_solvent\_model(NHlines,Hlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| NHlines                                                                  |
+--------------------------------------------------------------------------+
| Character                                                                |
| Hlines(NHline)\*80                                                       |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.solvent\_model\_details                                         |
| Special aspects of the solvent model used in refinement.                 |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_wght\_details(NHlines,Hlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| NHlines                                                                  |
+--------------------------------------------------------------------------+
| Character                                                                |
| Hlines(NHlines)\*80                                                      |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.ls\_weighting\_details                                          |
| A description of special aspects of the weighting scheme used in         |
| least-squares refinement. Used to describe the weighting when the value  |
| of \_refine.ls\_weighting\_scheme is specified as 'calc'.                |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefine\_wghtScheme(WghtScheme)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Character                                                                |
| WghtScheme\*(\*)                                                         |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refine.ls\_weighting\_scheme                                           |
| The weighting scheme applied in the least-squares process. The standard  |
| code may be followed by a description of the weight (but see             |
| \_refine\_ls\_weighting\_details for a preferred approach).              |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hrefln\_sys\_abs(IH,IK,IL,F,SF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| F,SF                                                                     |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| IH,IK,IL                                                                 |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_refln\_sys\_abs.index\_h                                               |
| Miller index h of the reflection. The values of the Miller indices in    |
| the REFLN\_SYS\_ABS category must correspond to the cell defined by cell |
| lengths and cell angles in the CELL category.                            |
+--------------------------------------------------------------------------+
| \_refln\_sys\_abs.index\_k                                               |
| Miller index k of the reflection. The values of the Miller indices in    |
| the REFLN\_SYS\_ABS category must correspond to the cell defined by cell |
| lengths and cell angles in the CELL category.                            |
+--------------------------------------------------------------------------+
| \_refln\_sys\_abs.index\_l                                               |
| Miller index l of the reflection. The values of the Miller indices in    |
| the REFLN\_SYS\_ABS category must correspond to the cell defined by cell |
| lengths and cell angles in the CELL category.                            |
+--------------------------------------------------------------------------+
| \_refln\_sys\_abs.I                                                      |
| The measured value of the intensity in arbitrary units.                  |
+--------------------------------------------------------------------------+
| \_refln\_sys\_abs.sigmaI                                                 |
| The standard uncertainty (e.s.d.) of \_refln\_sys\_abs.I, in arbitrary   |
| units.                                                                   |
+--------------------------------------------------------------------------+
| \_refln\_sys\_abs.I\_over\_sigmaI                                        |
| The ratio of \_refln\_sys\_abs.I to \_refln\_sys\_abs.sigmaI. Used to    |
| evaluate whether a reflection that should be systematically absent       |
| according to the designated space group is in fact absent.               |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hreflns(WilsonB,R1,R2,Criteria,AMI,AMF,Nref)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| WilsonB,R1,R1,AMI,AMF                                                    |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nref                                                                     |
+--------------------------------------------------------------------------+
| Character                                                                |
| Critera\*(\*)                                                            |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_reflns.B\_iso\_Wilson\_estimate                                        |
| The value of the overall isotropic temperature factor estimated from the |
| slope of the Wilson plot.                                                |
+--------------------------------------------------------------------------+
| \_reflns.entry\_id                                                       |
| This data item is a pointer to \_entry.id in the ENTRY category.         |
+--------------------------------------------------------------------------+
| \_reflns.data\_set\_id                                                   |
| The value of \_reflns.data\_set\_id identifies the particular data set   |
| for which harvest information is presented in the data block.            |
+--------------------------------------------------------------------------+
| \_reflns.d\_resolution\_high                                             |
| The highest resolution for the interplanar spacings in the reflection    |
| data. This is the smallest d value.                                      |
+--------------------------------------------------------------------------+
| \_reflns.d\_resolution\_low                                              |
| The lowest resolution for the interplanar spacings in the reflection     |
| data. This is the largest d value.                                       |
+--------------------------------------------------------------------------+
| \_reflns.observed\_criterion                                             |
| The criterion used to classify a reflection as 'observed'. This          |
| criterion is usually expressed in terms of a sigma(I) or sigma(F)        |
| threshold.                                                               |
+--------------------------------------------------------------------------+
| \_reflns.mean<I\_over\_sigI>\_obs\_all                                   |
| Overall Mean <I/sigma(I)> for the total ensemble of reflections observed |
+--------------------------------------------------------------------------+
| \_reflns.mean<F\_over\_sigF>\_obs\_all                                   |
| Overall Mean <F/sigma(F)> for the total ensemble of reflections observed |
+--------------------------------------------------------------------------+
| \_reflns.number\_obs                                                     |
| The number of reflections in the REFLN list (not the DIFFRN\_REFLN list) |
| classified as observed (see \_reflns.observed\_criterion). This number   |
| may contain Friedel equivalent reflections according to the nature of    |
| the structure and the procedures used.                                   |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hreflns\_intensity\_shell(Z,ACT,ACO,CT,CO)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| Z,ACT,ACO,CT,CO                                                          |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_EBI\_reflns\_intensity\_shell.Z                                        |
| Z is defined as I/<I> for the range of 4\*((Sintheta/Lamda)\*\*2)        |
+--------------------------------------------------------------------------+
| \_EBI\_reflns\_intensity\_shell.NZ\_acentric\_theory                     |
| The theoretical acentric wilson Distribution of Intensity magnitudes     |
| given in shells of N(Z)                                                  |
+--------------------------------------------------------------------------+
| \_EBI\_reflns\_intensity\_shell.NZ\_acentric\_observed                   |
| The observed acentric wilson Distribution of Intensity magnitudes given  |
| in shells of N(Z)                                                        |
+--------------------------------------------------------------------------+
| \_EBI\_reflns\_intensity\_shell.NZ\_centric\_theory                      |
| The theoretical centric wilson Distribution of Intensity magnitudes      |
| given in shells of N(Z)                                                  |
+--------------------------------------------------------------------------+
| \_EBI\_reflns\_intensity\_shell.NZ\_centric\_observed                    |
| The observed centric wilson Distribution of Intensity magnitudes given   |
| in shells of N(Z)                                                        |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hreflns\_overall\_merge\_p1(R1,R2,nmeas,nuniq,ncent,nano,fsigi,Rfac,ranom,sdIsignal,fpbias,Ntb)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| nmeas,nuniq,ncent,nano,Ntb                                               |
+--------------------------------------------------------------------------+
| Real                                                                     |
| R1,R2,fsigi,Rfac,ranom,sdIsignal,fpbias                                  |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_diffrn\_reflns.d\_res\_high                                            |
| The highest resolution in angstroms for the interplanar spacing for all  |
| the reflections merged. This is the smallest d value.                    |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.d\_res\_low                                             |
| The lowest resolution in angstroms for the interplanar spacing for all   |
| the reflections merged. This is the largest d value.                     |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.meanI\_over\_sigI\_all                                  |
| The ratio of the mean of the intensities of all reflections merged       |
| within the resolution limits \_diffrn\_reflns.d\_res\_low and            |
| \_diffrn\_reflns.d\_res\_high to the mean of the standard uncertainties  |
| of the intensities of all reflections in the same resolution limits.     |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.number\_measured\_all                                   |
| The total number of reflections measured and used in merging for the     |
| resolution limits \_diffrn\_reflns.d\_res\_low and                       |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.number\_unique\_all                                     |
| The total number of reflections measured which are symmetrically unique  |
| after merging for the resolution limits \_diffrn\_reflns.d\_res\_low and |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.number\_centric\_all                                    |
| The total number of reflections measured which are symmetrically unique  |
| after merging and classified as centric for the resolution limits        |
| \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high.          |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.number\_anomalous\_all                                  |
| The total number of reflections measured which are symmetrically unique  |
| after merging and that an anomalous difference was present in the        |
| overall set of observations, for the resolution limits                   |
| \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high.          |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.Rmerge\_I\_all                                          |
| The value of Rmerge(I) for all reflections in the resolution limits      |
| \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high.          |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.Rmerge\_I\_anomalous\_all                               |
| The value of Rmerge for anomalous data in the resolution limits          |
| \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high.          |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.meanI\_over\_sd\_all                                    |
| The ratio of the mean of the intensities of all reflections merged in    |
| the resolution limits \_diffrn\_reflns.d\_res\_low and                   |
| \_diffrn\_reflns.d\_res\_high, to the average standard deviation derived |
| from experimental SDs, after application of SDFAC SDADD SDB              |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.mean\_fract\_bias                                       |
| The overall Mean Fractional partial bias in the resolution limits        |
| \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.num\_fract\_bias\_in\_mean                              |
| The overall number of reflections used in the mean Fractional partial    |
| bias in the resolution limits \_diffrn\_reflns.d\_res\_low and           |
| \_diffrn\_reflns.d\_res\_high                                            |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hreflns\_overall\_merge\_p2(complt,Rmult,pcv,pcvo,rmeas,rmeaso,anomfrc)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| complt,Rmult,pcv,pcvo,rmeas,rmeaso,anomfrc                               |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_diffrn\_reflns.percent\_possible\_all                                  |
| The percentage of geometrically possible reflections represented by all  |
| reflections measured in the resolution limits                            |
| \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high.          |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.multiplicity                                            |
| The mean number of redundancy for the intensities of all reflections     |
| measured in the resolution limits \_diffrn\_reflns.d\_res\_low and       |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.PCV                                                     |
| The pooled standard deviation (the statistically valid measure of the    |
| noise level) is divided by the sum of the intensities (the signal level) |
| in the resolution limits \_diffrn\_reflns.d\_res\_low and                |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.PCV\_mean                                               |
| The pooled standard deviation (the statistically valid measure of the    |
| noise level) is divided by the sum of the intensities (the signal level) |
| in the resolution limits \_diffrn\_reflns.d\_res\_low and                |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.Rmeas                                                   |
| The multiplicity-weighted Rmerge relative to I+ or I- in the resolution  |
| limits \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high.   |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.Rmeas\_mean                                             |
| The multiplicity-weighted Rmerge relative to overall mean I in the       |
| resolution limits \_diffrn\_reflns.d\_res\_low and                       |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.anom\_diff\_percent\_meas                               |
| AnomFrc is the % of measured acentric reflections for which an anomalous |
| difference has been measured in the resolution limits                    |
| \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high.          |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hreflns\_overall\_merge\_p3(aihmin,aihmax,t1f,nssf,t2f,t3f,t1p,nssp,t2p,t3p)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| nssf, nssp                                                               |
+--------------------------------------------------------------------------+
| Real                                                                     |
| aihmin,aihmax,t1f,t2f,t3f,t1p,t2p,t3p                                    |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_diffrn\_reflns.min\_intensity                                          |
| The minimum intensity for a reflection measured after scaling and        |
| merging in the resolution limits \_diffrn\_reflns.d\_res\_low and        |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.max\_intensity                                          |
| The maximum intensity for a reflection measured after scaling and        |
| merging in the resolution limits \_diffrn\_reflns.d\_res\_low and        |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.Intensity\_rms\_fully\_recorded                         |
| The overall mean rms of intensity for all fully recorded reflections in  |
| the resolution limits \_diffrn\_reflns.d\_res\_low and                   |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.num\_fully\_measured                                    |
| The total number of fully recorded reflections merged in the resolution  |
| limits \_diffrn\_reflns.d\_res\_low and \_diffrn\_reflns.d\_res\_high.   |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.mean\_scatter\_over\_sd\_full                           |
| The mean (scatter/SD) for all fully recorded reflections scaled/merged   |
| in the resolution limits \_diffrn\_reflns.d\_res\_low and                |
| \_diffrn\_reflns.d\_res\_high by the normal probability analysis of      |
| deviations.                                                              |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.sigma\_scatter\_over\_sd\_full                          |
| The sigma (scatter/SD) for all fully recorded reflections scaled/merged  |
| in the resolution limits \_diffrn\_reflns.d\_res\_low and                |
| \_diffrn\_reflns.d\_res\_high by the normal probability analysis of      |
| deviations.                                                              |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.Intensity\_rms\_partially\_recorded                     |
| The overall mean rms of intensity for all partially recorded reflections |
| in the resolution limits \_diffrn\_reflns.d\_res\_low and                |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.num\_partials\_measured                                 |
| The total number of partially recorded reflections merged in the         |
| resolution limits \_diffrn\_reflns.d\_res\_low and                       |
| \_diffrn\_reflns.d\_res\_high.                                           |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.mean\_scatter\_over\_sd\_part                           |
| The mean (scatter/SD) for all partially recorded reflections             |
| scaled/merged in the resolution limits \_diffrn\_reflns.d\_res\_low and  |
| \_diffrn\_reflns.d\_res\_high by the normal probability analysis of      |
| deviations.                                                              |
+--------------------------------------------------------------------------+
| \_diffrn\_reflns.sigma\_scatter\_over\_sd\_part                          |
| The sigma (scatter/SD) for all partially recorded reflections            |
| scaled/merged in the resolution limits \_diffrn\_reflns.d\_res\_low and  |
| \_diffrn\_reflns.d\_res\_high by the normal probability analysis of      |
| deviations.                                                              |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hreflns\_scaling\_shell(R1,R2,Nref,AI,AF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| R1,R2,AI,AF                                                              |
+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nref                                                                     |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_EBI\_tmp\_reflns\_scaling\_shell.d\_res\_high                          |
| The highest resolution for the interplanar spacing in the reflection     |
| data for the dataset in this shell. This is the smallest d value.        |
+--------------------------------------------------------------------------+
| \_EBI\_tmp\_reflns\_scaling\_shell.d\_res\_low                           |
| The lowest resolution for the interplanar spacing in the reflection data |
| for the dataset in this shell. This is the highest d value.              |
+--------------------------------------------------------------------------+
| \_EBI\_tmp\_reflns\_scaling\_shell.num\_reflns\_observed                 |
| The total number of reflections in this shell.                           |
+--------------------------------------------------------------------------+
| \_EBI\_tmp\_reflns\_scaling\_shell.mean<I\_over\_sigI>\_obs              |
| Mean <I/sigma(I)> for the total number of reflections in this shell.     |
+--------------------------------------------------------------------------+
| \_EBI\_tmp\_reflns\_scaling\_shell.mean<F\_over\_sigF>\_obs              |
| Mean <F/sigma(F)> for the total number of reflections in this shell.     |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hreflns\_shell\_p1(R1,R2,Nmeas,Nuniq,Ncent,Nanom,fsigi,Rfac,Rcum,Ranom,sdIsignal,fpbias,Npb)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nmeas,Nuniq,Ncent,Nanom,Npb                                              |
+--------------------------------------------------------------------------+
| Real                                                                     |
| R1,R2,fsigi,Rfac,Rcum,Ranom,sdIsignal,FPbias                             |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_reflns\_shell.d\_res\_high                                             |
| The highest resolution in angstroms for the interplanar spacing in the   |
| reflections in this shell. This is the smallest d value.                 |
+--------------------------------------------------------------------------+
| \_reflns\_shell.d\_res\_low                                              |
| The lowest resolution in angstroms for the interplanar spacing in the    |
| reflections in this shell. This is the largest d value.                  |
+--------------------------------------------------------------------------+
| \_reflns\_shell.number\_measured\_all                                    |
| The total number of reflections measured for this resolution shell.      |
+--------------------------------------------------------------------------+
| \_reflns\_shell.number\_unique\_all                                      |
| The total number of measured reflections which are symmetrically unique  |
| after merging for this resolution shell.                                 |
+--------------------------------------------------------------------------+
| \_reflns\_shell.number\_centric\_all                                     |
| The total number of centric reflections measured for this resolution     |
| shell.                                                                   |
+--------------------------------------------------------------------------+
| \_reflns\_shell.number\_anomalous\_all                                   |
| The total number of anomalous reflections measured for this resolution   |
| shell.                                                                   |
+--------------------------------------------------------------------------+
| \_reflns\_shell.Rmerge\_I\_all                                           |
| The value of Rmerge(I) for all reflections in a given shell.             |
+--------------------------------------------------------------------------+
| \_reflns\_shell.Rmerge\_I\_all\_cumulative                               |
| The cumulative Rmerge up to this range of shells                         |
+--------------------------------------------------------------------------+
| \_reflns\_shell.Rmerge\_I\_anomalous\_all                                |
| The value of Rmerge for anomalous data in a given shell.                 |
+--------------------------------------------------------------------------+
| \_reflns\_shell.meanI\_over\_sd\_all                                     |
| The ratio of the mean of the intensities of all reflections in this      |
| shell to the average standard deviation derived from experimental SDs,   |
| after application of SDFAC SDADD SDB                                     |
+--------------------------------------------------------------------------+
| \_reflns\_shell.meanI\_over\_sigI\_all                                   |
| The ratio of the mean of the intensities of all reflections in this      |
| shell to the mean of the standard uncertainties of the intensities of    |
| all reflections in the resolution shell.                                 |
+--------------------------------------------------------------------------+
| \_reflns\_shell.mean\_fract\_bias                                        |
| The Mean Fractional partial bias in the resolution shell.                |
+--------------------------------------------------------------------------+
| \_reflns\_shell.num\_fract\_bias\_in\_mean                               |
| The number of reflections used in the mean Fractional partial bias in    |
| the resolution shell.                                                    |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hreflns\_shell\_p2(R1,R2,complt,ccmplt,amltpl,pcvo,pcv,rmeas,rmeaso,anomfrc)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Real                                                                     |
| complt,ccmplt,amltpl,pcvo,pcv,rmeas,rmeaso,anomfrc,R1,R2                 |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_reflns\_shell.percent\_possible\_all                                   |
| The percentage of geometrically possible reflections represented by all  |
| reflections measured for this resolution shell.                          |
+--------------------------------------------------------------------------+
| \_reflns\_shell.cum\_percent\_possible\_all                              |
| The cumulative percentage of geometrically possible reflections up to    |
| this range of shells.                                                    |
+--------------------------------------------------------------------------+
| \_reflns\_shell.multiplicity                                             |
| The mean number of redundancy for the intensities of all reflections in  |
| the resolution shell.                                                    |
+--------------------------------------------------------------------------+
| \_reflns\_shell.PCV\_mean                                                |
| The pooled standard deviation (the statistically valid measure of the    |
| noise level) is divided by the sum of the intensities (the signal level) |
| in the resolution shell.                                                 |
+--------------------------------------------------------------------------+
| \_reflns\_shell.PCV                                                      |
| The pooled coefficient of variation relative to I+ or I-. The pooled     |
| standard deviation (the statistically valid measure of the noise level)  |
| is divided by the sum of the intensities (the signal level) in the       |
| resolution shell.                                                        |
+--------------------------------------------------------------------------+
| \_reflns\_shell.Rmeas                                                    |
| The multiplicity-weighted Rmerge relative to I+ or I- in the resolution  |
| shell.                                                                   |
+--------------------------------------------------------------------------+
| \_reflns\_shell.Rmeas\_mean                                              |
| The multiplicity-weighted Rmerge relative to overall mean I in the       |
| resolution shell.                                                        |
+--------------------------------------------------------------------------+
| \_reflns\_shell.anom\_diff\_percent\_meas                                |
| AnomFrc is the % of measured acentric reflections for which an anomalous |
| difference has been measured in the resolution shell, after merging and  |
| scaling of the data set. The rejection criteria and treatment of         |
| Bijvoet-related observations (I+ & I-) is covered by the                 |
| \_reflns.merge\_reject\_criterion definition, as are all observations of |
| reflections.                                                             |
+--------------------------------------------------------------------------+

| 

SUBROUTINE Hsoftware(SoftwareClass,SoftwareAuthor,SoftwareEmail,SoftwareDescr)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Character                                                                |
| SoftwareAuthor\* (\*),SoftwareEmail\* (\*),SoftwareDescr\*               |
| (\*),SoftwareClass\* (\*)                                                |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_software.classification                                                |
| The classification of the program according to its major function.       |
+--------------------------------------------------------------------------+
| \_software.contact\_author                                               |
| The recognized contact author of the software. This could be the         |
| original author, modifier of the code, or maintainer, but should be the  |
| individual most commonly associated with the code.                       |
+--------------------------------------------------------------------------+
| \_software.contact\_author\_email                                        |
| The email address of the \_software.contact\_author.                     |
+--------------------------------------------------------------------------+
| \_software.description                                                   |
| Description of the software.                                             |
+--------------------------------------------------------------------------+

| 

SUBROUTINE HSymmetry(IntTabNum,SpaceGrpNam,NumEquiv,RFsymm)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| IntTabNum,NumEquiv                                                       |
+--------------------------------------------------------------------------+
| Character                                                                |
| SpaceGrpNam\*10                                                          |
+--------------------------------------------------------------------------+
| Real                                                                     |
| RFsymm(4,4,\*)                                                           |
+--------------------------------------------------------------------------+

**Writes the following CIF items:**

+--------------------------------------------------------------------------+
| \_Symmetry.Int\_Tables\_number                                           |
| Space-group number from International Tables for Crystallography, Vol. A |
| (1987).                                                                  |
+--------------------------------------------------------------------------+
| \_Symmetry.space\_group\_name\_H-M                                       |
| Hermann-Mauguin space-group symbol. Note that the H-M symbol does not    |
| necessarily contain complete information about the symmetry and the      |
| space-group origin. If used always supply the FULL symbol from           |
| International Tables for Crystallography, Vol. A (1987) and indicate the |
| origin and the setting if it is not implicit. If there is any doubt that |
| the equivalent positions can be uniquely deduced from this symbol        |
| specify the \_symmetry\_equiv.pos\_as\_xyz or                            |
| \_symmetry.space\_group\_name\_Hall data items as well. Leave spaces     |
| between symbols referring to different axes.                             |
+--------------------------------------------------------------------------+
| \_Symmetry\_equiv.id                                                     |
| The value of \_symmetry\_equiv.id must uniquely identify a record in the |
| SYMMETRY\_EQUIV category. Note that this item need not be a number; it   |
| can be any unique identifier.                                            |
+--------------------------------------------------------------------------+
| \_Symmetry\_equiv.pos\_as\_xyz                                           |
| Symmetry equivalent position in the 'xyz' representation. Except for the |
| space group P1, these data are repeated in a loop. The format of the     |
| data item is as per International Tables for Crystallography, Vol. A.    |
| (1987). All equivalent positions should be entered, including those for  |
| lattice centring and a centre of symmetry, if present.                   |
+--------------------------------------------------------------------------+

| 

SUBROUTINE HSymTrn(Nsym,Rsm,Symchs)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nsym                                                                     |
+--------------------------------------------------------------------------+
| Real                                                                     |
| Rsm(4,4,\*)                                                              |
+--------------------------------------------------------------------------+
| Character                                                                |
| Symchs(MaxSymmetry)\*80                                                  |
+--------------------------------------------------------------------------+

**Does the following:**

Translates the Symmetry matrices into INT TAB character strings for each
symmetry operation and prints the real and reciprocal space operators on
standard output.

| 

SUBROUTINE HTrnSym(Nsym,Rsm,Symchs)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

+--------------------------------------------------------------------------+
| Integer                                                                  |
| Nsym                                                                     |
+--------------------------------------------------------------------------+
| Real                                                                     |
| Rsm(4,4,\*)                                                              |
+--------------------------------------------------------------------------+
| Character                                                                |
| Symchs(MaxSymmetry)\*80                                                  |
+--------------------------------------------------------------------------+

**Does the following:**

Symmetry translation from matrix back to characters. This translates the
Symmetry matrices into INT TAB character strings.

SOURCE:
http://ndbserver.rutgers.edu/mmcif/dictionaries/html/cif_mm.dic/Index/index.html
