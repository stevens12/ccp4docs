SFTOOLS (CCP4: Supported Program)
=================================

NAME
----

**sftools** - Reflection data file utility program including some
density map handling

SYNOPSIS
--------

| **sftools**
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

The SFTOOLS program is a toolbox of commands to analyze, manipulate, and
convert structure factor files. The program has been written with an
interactive user in mind and will provide on-line help, suggestions and
warnings as appropriate. A list of the currently implemented commands
plus descriptions of their use is given in the `command reference
section <#reference>`__ at the end of this document. The list of
commands can always be obtained by just pressing the return key when the
program asks for a command. To get the command descriptions, the user
should type the name of a command followed by the keyword HELP.

SFTOOLS is very flexible which gives the user great power to accomplish
many tasks. At the same time the program contains many checks to protect
you, as far as possible, from using the flexibility inappropriately.
This design goal makes it more difficult to write a script to run the
program as a batch job. If you have to make a script then you may find
it easier to run the program once interactively and paste the commands
into the script file as you go. At this moment it is not possible to
obtain a log file of your interactive session, you will have to run the
program from a script if you really need one. Creating a log file will
be implemented in the next release of the program.

SFTOOLS has also some density map functionality. It can read/write,
CCP4, BIOMOL and DSN6 maps, and it can create maps from structure
factors and vice versa. A very useful combination is to create your map
(`FFT <#fft>`__), cut out a block of density surrounding your protein
(`MAPLIMIT <#maplimit>`__), and write it out as a DSN6 file
(`MAPOUT <#mapout>`__), all in one interactive session.

A lot of the functionality of SFTOOLS is also covered by a large number
of other CCP4 programs. The main advantage of using SFTOOLS is that you
can do all your manipulations and analyses in one session without having
to run multiple programs and create script files and output files with
intermediate results. In addition, many users like the interactive and
user friendly nature of the program.

The rest of this manual consists of 3 parts:

-  `General program description <#general>`__
-  `Summary and examples of major commands <#examples>`__
-  `Command reference section <#reference>`__

--------------

General program description
---------------------------

| SFTOOLS consists of a central data base and a set of subroutines that
  interact with it. The data base consists of 3 large arrays plus
  several other variables to store the header information. When reading
  file formats with less information than the MTZ format the user is
  asked to supply the missing information.
| Here are the four most important data base components:

IHKL
    This is a 1-dimensional array that stores the reflection indices.
    Each index triplet is packed into a 4-byte integer which is used as
    a unique reflection identifier. Each index ideally occurs only once
    and obeys the CCP4 asymmetric unit definitions. Only multi-record
    MTZ files can have multiple reflections with the same indices.
    Unlike other CCP4 programs, indices are not considered as data base
    columns and their content can only be changed with the
    `REINDEX <#reindex>`__ command.
    The size of the IHKL array is determined by parameter MAXREF in the
    sftools.inc file.
REF
    The REF array contains all the actual data columns. In the program
    it is a 1-dimensional array, but it appears as a 2-dimensional array
    to the user, *e.g.* rows=reflections and columns=Fobs, Sigma, Phase
    etc... The user can identify a specific column by typing "COLUMN
    column\_id". column\_id is either the column label or the column
    number. The column label is case insensitive but may need to be
    quoted if it contains unusual characters. Labels will fail if
    columns with identical labels exist (bad practice). Column numbers
    should always work. Note that the indices do not count as columns so
    if your file contains H K L Fobs Sigma, then Fobs is column 1.
    SFTOOLS can also read, write, and create density maps. Maps are
    stored at the end of the REF array whereas reflections are stored at
    the beginning.
    The size of the REF array is determined by parameter MAXDAT in the
    sftools.inc file. The number of columns times the number of
    reflections in your file should be less then MAXDAT. At the same
    time the number of reflections should be less then MAXREF and the
    number of columns less then MAXCOL.
ACTIVE
    The ACTIVE column is a boolean flag that indicates which reflections
    should be used and which should be ignored. Initially all
    reflections are "active" but this can be changed with the
    `SELECT <#select>`__ keyword. For instance to restrict all following
    commands to 10 - 3 Angstrom resolution you would type "SELECT resol
    > 3 < 10". You can use SELECT ALL to make all reflections active
    again.
    The size of the ACTIVE array is determined by parameter MAXREF in
    the sftools.inc file.
TYPE
    The TYPE array stores the column type of each column as a 1
    character code. The codes follow the `MTZ definition for column
    types <#coltyp>`__. SFTOOLS actively uses the column types to
    prevent you from comparing apples and oranges, keeping phase
    information consistent *etc*.

Filling the data base
~~~~~~~~~~~~~~~~~~~~~

After starting the program you will want to load data into the data
base. In most cases this means reading a structure factor file. To do
this use the `READ <#read_examples>`__ command. If the data base was
empty then SFTOOLS will read both the header and the diffraction data.
If a non-MTZ file is read the program will prompt for missing header
information. If you read in a second structure factor file (or you
prepared the header information with the `SET ALL <#set>`__ command or
by reading a density map) then SFTOOLS will compare the header
information on the second file with what is in the data base. It will
report inconsistencies, but in all cases the information in the data
base will be kept. It is useful to have an empty MTZ with header
information for your project but no data. When reading a non-MTZ file
then you first read the empty MTZ to get the header information. This
way you avoid to have to enter the header information and the possible
typos that that might introduce.

When a second (or more) structure factor file is read, the program will
add new columns to the data base to store the data from the new file.
Data for reflections that are already present in the data base will be
appended to that reflection. Data for reflections that are not yet
present in the data base will be stored in newly created reflections. In
this case MNFs are inserted in the old columns (see the 0 0 1 reflection
below). When a reflection was present in the data base but not in the
new structure factor file, then the columns for the new structure factor
file are set to MNF (see the 0 0 3 reflection below). An example is
given below.

::

    Assume that you have the following two structure factor files

    file1.mtz              file2.mtz

    0 0 2 12.4  3.2        0 0 1 82.7 10.1
    0 0 3 68.7 12.1        0 0 2 13.8  3.8
    0 1 3  3.2 17.0        0 1 3  4.8 17.2
    0 1 5 34.0  5.8        0 1 5 37.1  4.8

    in SFTOOLS you type:

    read file1.mtz
    read file2.mtz

    The data base will then contain

    0 0 1  MNF  MNF 82.7 10.1
    0 0 2 12.4  3.2 13.8  3.8
    0 0 3 68.7 12.1  MNF  MNF
    0 1 3  3.2 17.0  4.8 17.2
    0 1 5 34.0  5.8 37.1  4.8

SFTOOLS will merge, sort and check your data. For further consistency
checks you can give the `CHECKHKL <#checkhkl>`__ command after reading a
file. This also gives you a few seconds to think about your next action
while CHECKHKL is working.

Data representation
~~~~~~~~~~~~~~~~~~~

All data, including integers, are stored as real numbers. Phase
information is stored in radians but they will appear in degrees to the
user (as long as the column type is set to "P"). RFREE flags follow the
CCP4 definition in which the data is divided into N RFREE sets with
flags running from 0 to N-1. When reading/writing non-MTZ files, the
RFREE flag definitions will be converted to/from the CCP4 definition,
respectively. The RFREE set with a flag of 0 is considered to be the
actual RFREE set.

When reflections have missing data in one or more of their columns, then
this is indicated by a Missing Number Flag (MNF). SFTOOLS uses a value
of -1.0E+10 to indicate missing data. When reading/writing MTZ files,
the SFTOOLS MNF is converted from/to the CCP4 MNF definition
respectively. It is crucial that proper MNFs are present. If you have
missing data without MNFs, then you can introduce MNFs easily with the
`ABSENT <#absent>`__ command.

Asymmetric unit definition
~~~~~~~~~~~~~~~~~~~~~~~~~~

Several commands need to know how the asymmetric unit is defined. The
default is to use the CCP4 definition and it is highly recommended to
stick with it (in the next release SFTOOLS will enforce adherence to the
CCP4 definition). If you use a different definition then you can either
convert to the CCP4 definition with the `REDUCE <#reduce>`__ command or
you have to specify the definition each time you use a command that is
definition dependent (this is very error prone, that is why it will be
changed in the next release).

Input parsing
~~~~~~~~~~~~~

| The program input is analyzed by a parser that divides the input into
  words. Words may be delimited by one or more spaces or any of the
  following characters **( ) , < > = [ ]**. If you need to generate
  words including any of these characters or spaces (*e.g.* in a file
  name or column label) then the word(s) must be enclosed in double
  quotes.
| Real numbers can be read in any format acceptable to FORTRAN, examples
  are: 1234 1234.5 1.2345E+3. Integers should be made up exclusively by
  digits 0 to 9 except for the first character which may be a minus
  sign. Character input is case insensitive except for file names.

The often used keyword COLUMN may be abbreviated to COL. Likewise,
RESOLUTION may be replaced by RESOL. A more consistent method for
abbreviating keywords will be implemented later.

Command arguments
~~~~~~~~~~~~~~~~~

| It is advised for most commands to give the command name and its
  arguments on a single line. If you leave out the arguments 3 things
  can happen:

The subroutine uses default values

The program requests the missing information interactively

The program complains and suggests that you read the help information

CCP4 compatibility
~~~~~~~~~~~~~~~~~~

| SFTOOLS is meant to adhere to the MTZ standard in all respects.
  However, the author does not use CCP4 extensively so there may still
  be some minor differences (`please report
  them <mailto:ccp4@dl.ac.uk>`__). Specific treatment of multi-record
  MTZ files is limited at this point. Most options will probably work
  fine with them but this has not been tested.
| CCP4 supports maps of several types. SFTOOLS can read and write
  standard CCP4 electron density maps. The use of other map types has
  not been tested.

The user interface of SFTOOLS was developed independently of CCP4
practices and CCP4 parsing routines. Definitions of LABIN, LABOUT,
HKLIN, HKLOUT and other common features of the CCP4 user interface are
not supported. However, the user interface was designed for interactive
use and should be easy to get used to. SFTOOLS can also prepare tabular
data. Commands that prepare tables also allow the user some control over
the table creation. In the next release of SFTOOLS this table control
will be handled by a separate command. In the future tables may also
support the use of `XLOGGRAPH <xloggraph.html>`__.

--------------

Summary and examples of major commands
--------------------------------------

Filling the data base
~~~~~~~~~~~~~~~~~~~~~

`READ <#read_examples>`__
    Reading structure factor files
`MAPIN <#mapread_examples>`__
    Reading electron density maps
`SET <#set_examples>`__
    Creating the header

Saving (part of) the data base
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`WRITE <#write_examples>`__
    Writing structure factor files
`MAPOUT <#mapwrite_examples>`__
    Writing electron density maps

Analyzing the data base
~~~~~~~~~~~~~~~~~~~~~~~

`LIST <#list_examples>`__
    Listing the data base content
`CHECKHKL <#checkhkl_examples>`__
    Data checking and statistics
`COMPLETE <#complete_examples>`__
    Checking data completeness
`CORREL <#correl_examples>`__
    Correlation coefficients between columns as function of resolution
`PHASHFT <#phashft_examples>`__
    Phase shifts between phase columns as function of resolution
`PLOT <#plot_examples>`__
    Prepare tables with data for plotting
`MAP <#map_examples>`__
    Get map statistics based on structure factors
`MAPSTAT <#mapstat_examples>`__
    Get map statistics based on a real map

Modifying the data base
~~~~~~~~~~~~~~~~~~~~~~~

`ABSENT <#absent_examples>`__
    Creating missing number flags
`CALC <#calc_examples>`__
    Perform column-based arithmetic (very important!)
`SELECT <#select_examples>`__
    Selecting reflections (very important!)
`DELETE <#delete_examples>`__
    Delete columns or maps
`PURGE <#purge_examples>`__
    Delete reflections
`EXPAND <#expand_examples>`__
    Expand to lower symmetry space group
`FFT <#fft_examples>`__
    Create a map from structure factors
`MAP2SF <#map2sf_examples>`__
    Create structure factors from a map
`HLCONV <#hlconv_examples>`__
    Create Hendrickson Lattman coefficients from phase and figure of
    merit
`I2F <#i2f_examples>`__
    Convert intensities to amplitudes
`MAPLIMIT <#maplimit_examples>`__
    Select a subsection of a map
`REINDEX <#reindex_examples>`__
    Modify indices by matrix multiplication
`RFREE <#rfree_examples>`__
    Select an RFREE set
`SET <#set_examples>`__
    Modify header information
`SORT <#sort_examples>`__
    Sort the reflections
`REDUCE <#reduce_examples>`__
    Reduce reflections to the asymmetric unit
`MERGE <#merge_examples>`__
    Merge reflections with identical indices.

--------------

Reading structure factor files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To read a structure factor file, SFTOOLS needs to know the name of the
file, the format of the file, and the columns to be read. The name has
to be supplied by the user. By default SFTOOLS reads all columns and it
stores them in file order. However, the user can use the
`COLUMN <#read_column>`__ keyword to control the columns to be read and
their order in the data base. The file format will in most cases be
determined from the file name extension, see below. If the format and
extension do not match then the user can explicitly specify the format
by a `FORMAT <#read_format>`__ keyword.

+-----------------------+-------------+------------------+
| File format           | Extension   | FORMAT keyword   |
+=======================+=============+==================+
| MTZ (CCP4)            | .mtz        | MTZ              |
+-----------------------+-------------+------------------+
| MDF (OLD BIOMOL)      | .mdf        | MDF              |
+-----------------------+-------------+------------------+
| BIOMOL ASCII FORMAT   | .snd        | SND              |
+-----------------------+-------------+------------------+
| X-PLOR                | .xpl        | XPL              |
+-----------------------+-------------+------------------+
| TNT                   | .tnt        | TNT              |
+-----------------------+-------------+------------------+
| Phases short format   | .31         | 31               |
+-----------------------+-------------+------------------+
| Phases long format    | .31         | 31               |
+-----------------------+-------------+------------------+
| XtalView fin format   | .fin        | FIN              |
+-----------------------+-------------+------------------+
| XtalView phs format   | .phs        | PHS              |
+-----------------------+-------------+------------------+
| XtalView double fin   | .df         | DF               |
+-----------------------+-------------+------------------+
| Generic ASCII file    | .fmt        | FMT              |
+-----------------------+-------------+------------------+

In addition to reflection information, SFTOOLS also needs some header
information. The MTZ format has the most complete header information and
SFTOOLS needs most of it. If you read a non-MTZ file then SFTOOLS will
prompt you for the missing header information. It is worthwhile to make
an MTZ file for your project with just the header information in it. If
you have to read a non-MTZ file then just first read in the MTZ with the
header info and there will be no more questions asked.

| SFTOOLS can read any generic ASCII file if:

There are no header lines

All reflections have the same format and number of columns

The indices H, K, and L are in columns 1, 2, and 3, respectively

Most ASCII files will be readable as is, however, if yours is not
well-behaved then you can fix it as follows.

#. Use an editor or the Unix tail command to get rid of header lines:
   ``tail +3 file_in > file_out``
   skips the first 2 lines.
#. Use the Unix awk command to force all reflections to have the same
   format or to place the indices in the first three columns:
   ``awk '{printf("%4d%4d%4d%8.1f%8.1f",$3,$4,$5,$1,$2)}' file_in > file_out``
   swaps the order of the columns {1,2,3,4,5 to 3,4,5,1,2} and forces
   each line to have the same format.

For a detailed description see `READ <#read>`__ and `Filling the data
base <#filling>`__

Examples
^^^^^^^^

READ filename.mtz
    Read all data from an MTZ file named filename.mtz
READ filename.mtz COL 3 4
    Read columns 3 and 4 from an MTZ file named filename.mtz
READ filename.mtz COL 3 4 1 2
    Read columns 1 to 4 from an MTZ file named filename.mtz but reverse
    the order of columns 1&2 and 3&4
READ filename.mtz COL NONE
    Read only the header data from an MTZ file named filename.mtz
READ filename.mtz COL
    Read data from an MTZ file named filename.mtz. SFTOOLS will list the
    label of each column and asks if it should be included
READ filename.hkl MTZ
    Read all data from an MTZ file named filename.hkl
READ filename.xpl COLUMN Fcalc phase
    Read "Fcalc" and "phase" columns from an XPLOR file named
    filename.xpl. SFTOOLS will ask for extra header information
READ header.mtz
read filename.xpl
    Read an XPLOR file named filename.xpl but now the header information
    will be taken from the header.mtz file

--------------

Reading electron density maps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To read an electron density file, SFTOOLS only needs to know the name of
the file and the format of the file. The name has to be supplied by the
user and in most cases the format can be determined from the file name
extension, see below. If the format and extension do not match then the
user can explicitly specify the format by a `FORMAT <#mapin_format>`__
keyword.

+----------------------+-------------+------------------+
| File format          | Extension   | FORMAT keyword   |
+======================+=============+==================+
| CCP4 map             | .map        | MAP              |
+----------------------+-------------+------------------+
| BIOMOL map           | .mff        | MFF              |
+----------------------+-------------+------------------+
| FRODO/O map (DSN6)   | .dn6        | DN6              |
+----------------------+-------------+------------------+

| SFTOOLS always reads the complete density map. Only one map can be in
  memory at any time. Use `DELETE MAP <#delete_map>`__ to remove an old
  map before reading a new one. SFTOOLS will use the header information
  in the input map and will prompt the user for missing information.
| CCP4 and BIOMOL maps can be sectioned in any order, however, SFTOOLS
  will internally always store the map in ABC order (sections along C
  and rows along B). For CCP4 maps not all map types may be supported,
  but the standard electron density maps should work.

**Note:** DSN6 files use only a single byte per grid point. It may be
better to recreate your map from the map coefficients or to re-read the
original MFF or CCP4 electron density file.

For a detailed description see `MAPIN <#mapin>`__

Examples
^^^^^^^^

MAPIN filename.map
    Read a CCP4 electron density map
MAPIN filename.rho MAP
    Read a CCP4 electron density map
MAPIN filename.dn6
    Read a DSN6 electron density map

--------------

Creating the header
~~~~~~~~~~~~~~~~~~~

To enter the header information without reading it from a file you use
the `SET ALL <#set_all>`__ command. The program will prompt you for the
header information.

For a detailed description see `SET <#set>`__

Examples
^^^^^^^^

SET ALL
    Fill in all header entries. SFTOOLS will ask you for the information
SET ALL
write header.mtz
    Fill in all header entries and write an MTZ file with just this
    information

--------------

Writing structure factor files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To write a structure factor file, SFTOOLS needs to know the name of the
file, the format of the file, and the columns to be written. The name
has to be supplied by the user. By default SFTOOLS writes all columns in
the same order as they are stored in the data base. However, the user
can use the `COLUMN <#write_column>`__ keyword to control the columns to
write and their order in the output file. The file format will in most
cases be determined from the file name extension, see the `extensions
table <#sf_extensions>`__. In order to write a generic ASCII formatted
file you have to specify the FORTRAN format string. It is absolutely
required that the first 3 values are written as integers since these
will be the indices. **ALL** other values **MUST** be written as reals
because that is how they are stored in the program.

Some of the supported file formats are restricted in the number of
columns and/or types of data that they accept. The user should use the
`COL <#write_column>`__ keyword if needed to write the appropriate data.
The requirements are detailed in the `WRITE <#write>`__ description.

For a detailed description see `WRITE <#write>`__

Examples
^^^^^^^^

WRITE filename.mtz
    Write all data to an MTZ named filename.mtz
WRITE filename.hkl MTZ
    Write all data to an MTZ named filename.hkl
WRITE filename.mtz COL Fscaled phase
    Write columns with labels "Fscaled" and "phase" to an MTZ named
    filename.mtz
WRITE filename.mtz COL 3 4 1 2
    Write columns 1 to 4 to an MTZ file named filename.mtz but reverse
    the order of columns 1&2 and 3&4
WRITE filename.mtz COL NONE
    Write only the header data to an MTZ file named filename.mtz
WRITE filename.mtz COL
    Write an MTZ file named filename.mtz. SFTOOLS will list the label of
    each data base column and ask if it should be written
WRITE filename.xpl COL 1 3 4
    Write columns 1, 3 and 4 to an XPLOR file named filename.xpl
WRITE filename.hkl format(3i5,2f8.3) COL 1 2
    Write columns 1 and 2 in the given format to an ASCII file named
    filename.hkl

--------------

Writing electron density maps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To write an electron density file, SFTOOLS only needs to know the name
of the file and the format of the file. The name has to be supplied by
the user and in most cases the format can be determined from the file
name extension, see below. If the format and extension do not match then
the user can explicitly specify the format by a
`FORMAT <#mapout_format>`__ keyword, see the `extensions
table <#map_extensions>`__.

The current version of SFTOOLS always writes the complete density map.
To write a subsection of the map you should use the
`MAPLIMIT <#maplimit>`__ command. In the next release the MAPLIMIT
functions will be moved to the `MAPOUT <#mapout>`__ command in a more
general way. By default, SFTOOLS will write a map in ABC order (sections
along C and rows along B). For CCP4 and BIOMOL maps the user can specify
a different order by adding the `ORDER <#mapout_order>`__ keyword.

For a detailed description see `MAPOUT <#mapout>`__

Examples
^^^^^^^^

MAPOUT filename.map
    Write a CCP4 electron density map to a file named filename.map
MAPOUT filename.dn6
    Write a DSN6 map for FRODO/O to a file named filename.dn6
MAPOUT filename.rho MAP
    Write a CCP4 electron density map to a file named filename.rho
MAPOUT filename.mff ORDER A C B
    Write a BIOMOL map to a file named filename.mff. Sections will be
    along the B axis and rows along the C axis)

--------------

Listing the data base content
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `list <#list>`__ command is used to list the actual contents of the
data base. It accepts the following keywords: **`TITLE <#list_title>`__,
`LABELS <#list_labels>`__, `TYPE <#list_type>`__, `CELL <#list_cell>`__,
`SORT <#list_sort>`__, `SPACEGROUP <#list_spacegroup>`__, and
`REFLECTIONS <#list_reflections>`__**. Each keyword limits the listing
to the indicated information. If no keyword is given all information,
except reflections, will be listed. Keywords can be abbreviated to the
first 3 characters.

For a detailed description see `LIST <#list>`__

Examples
^^^^^^^^

LIST
    List all header information
LIST CELL
    List the unit cell parameters
LIST REF
    List the reflection indices, resolution and first 6 columns
LIST REF COL 8 9
    List the reflection indices, resolution and columns 8 and 9

--------------

Data checking and statistics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`CHECKHKL <#checkhkl>`__ reports potential problems with your data like:
presence of systematically absent reflections, centric reflections with
illegal phase, reflections outside asymmetric unit, multiple reflections
with the same indices *etc.*.. It also lists for each column the
average, minimum and maximum value, the minimum and maximum resolution,
and the number of missing data. It is nice to keep the CHECKHKL output
as part of the documentation of a new data set.

For a detailed description see `CHECKHKL <#checkhkl>`__

Examples
^^^^^^^^

CHECKHKL
    Report problems and statistics

--------------

Checking data completeness
~~~~~~~~~~~~~~~~~~~~~~~~~~

The `COMPLETE <#complete>`__ command reports the completeness of your
data as function of resolution. It can make bin-wise or cumulative
tables and it can list percentage completeness as well as number of
observed reflections. This option also reports a "100% criterium". This
is the resolution in Angstrom of a 100% complete hypothetical data set
with the same number of reflections as your data set. It is nice to keep
the COMPLETE output as part of the documentation of a new data set.

The `PDB <#complete_mode_pdb>`__ keyword allows you to write a PDB file
with pseudo-atoms for each observed and each absent reflection.
Visualization with your favorite graphics program can tell you if data
is randomly or systematically missing.

For a detailed description see `COMPLETE <#complete>`__

Examples
^^^^^^^^

COMPLETE
    Report bin-wise percentage completeness of first 10 columns
COMPLETE CUMULATIVE
    Report cumulative percentage completeness of first 10 columns
COMPLETE COL 11
    Report bin-wise percentage completeness of column 11
COMPLETE PDB
    Create OBSERVED.PDB with reciprocal lattice as pseudo atoms

--------------

Correlation coefficients between columns
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `CORREL <#correl>`__ command reports the correlation coefficient
between two columns as function of resolution. It also gives an R-value
but this only makes sense if the columns are on the same scale.

For a detailed description see `CORREL <#correl>`__

Examples
^^^^^^^^

CORREL COL 1 3
    Report correlation between columns 1 and 3

--------------

Phase shifts between phase columns as function of resolution
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `PHASHFT <#phashft>`__ command reports the average phase difference
between two columns as function of resolution. The keyword
`COSINE <#phashft_cosine>`__ can be used to list the cosine of the phase
error.

For a detailed description see `PHASHFT <#phashft>`__

Examples
^^^^^^^^

PHASHFT COL 2 4
    Report average phase difference between columns 2 and 4
PHASHFT COSINE COL 2 4
    Report cosine of the phase difference between columns 2 and 4

--------------

Prepare tables with data for plotting
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `PLOT <#plot>`__ command lists the bin-wise average of a column.
Bins can be either as function of resolution or as function of another
column. In the latter case the reflections are sorted by the other
column before binning.

For a detailed description see `PLOT <#plot>`__

Examples
^^^^^^^^

PLOT COL 1 VERSUS RESOL
    Tabulate average value of col 1 in resolution bins
PLOT COL fobs VERSUS COL sigma
    Tabulate average value of the column labeled "fobs" for bins of
    increasing values in the column labeled "sigma"

--------------

Get map statistics based on structure factors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The rms value of a map can be derived from the map amplitudes, *i.e.* it
is independent of the phases. The correlation between two maps can be
calculated when the structure factors of both maps are known (amplitudes
and phases). In both cases it is not necessary to actually calculate the
map.

For a detailed description see `MAP <#map>`__

Examples
^^^^^^^^

MAP RMS 1
    Report the map rms based on structure factor amplitudes in column 1
MAP CORREL Fobs Pcalc Fcalc Pcalc
    Report the correlation between two maps. Map 1 is defined by
    structure factors in columns with labels Fobs and Pcalc. Map 2 is
    defined by structure factors in columns with labels Fcalc and Pcalc.
    The phase column of the two maps does not have to be the same as in
    this example

--------------

Get map statistics based on a real map
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After a map has been read or generated with the `FFT <#fft>`__ command,
the `MAPSTAT <#mapstat>`__ command will list the header information and
statistics for the map and individual sections.

For a detailed description see `MAPSTAT <#mapstat>`__

Examples
^^^^^^^^

MAPSTAT
    List the header info plus statistics for the map as well as for
    individual sections along the C-axis
MAPSTAT SECTION b
    List the header info plus statistics for the map as well as for
    individual sections along the B-axis.

--------------

Creating missing number flags
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have missing data then it is crucial for SFTOOLS that they are
flagged by Missing Number Flags (MNFs). Old-style CCP4 programs relied
on a SIGMA column being zero to indicated missing data. SFTOOLS will
warn you when you read such an old-style file. Inserting proper MNFs is
easy with the `ABSENT <#absent>`__ command. The basic command structure
is

::

    ABSENT COL col_ids IF SELECTION

**SELECTION** can be any valid selection (see `SELECT <#select>`__),
whereas **COL col\_ids** is a list of column labels or column numbers.
MNFs are inserted in all listed columns for the reflections that fit the
selection criterium. It is a good idea to give a `SELECT
ALL <#select_criterium_all>`__ command prior to using the ABSENT command
to make sure that all reflections will be used.

For a detailed description see `ABSENT <#absent>`__

Examples
^^^^^^^^

ABSENT COL 1 2 IF COL 2 = 0
    Set MNFs in columns 1 and 2 for reflections which have column 2 = 0
ABSENT COL 1 2 IF COL Iobs < 0
    Set MNFs in columns 1 and 2 for reflections which have a negative
    value in the column with label Iobs

--------------

Perform column-based arithmetic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `CALC <#calc>`__ command is one of the most important commands and
it gives you a lot of flexibility to accomplish non-standard tasks.
Basically, the CALC command allows you to calculate a column of data
based on existing data columns, constants, build-in functions and
arithmetic operators. It is worth reading the rather extensive
documentation for the CALC command.

For a detailed description see `CALC <#calc>`__

Examples
^^^^^^^^

CALC R COL f\_over\_sigma = COL fobs COL sigma /
    Create a column with label f\_over\_sigma which contains fobs /
    sigma
CALC R COL resolsq = resol 2 \*\*
    Create a column with label resolsq which contains the square of the
    reflection resolution (resol is a built-in function)
CALC F COL Fsimulated = COL Fcalc ran\_g 10 \* +
    Create a column with label Fsimulated which contains the value of
    column Fcalc plus 10 times a random number from a Gaussian
    distribution with average = 0 and variance = 1
CALC (COL Fmap Pmap) = (COL Fcomplex Pnative) (COL Fnative Pnative) -
    Create two columns labeled Fmap and Pmap which contain map
    coefficients for a Fcomplex - Fnative difference Fourier map

--------------

Selection of reflections
~~~~~~~~~~~~~~~~~~~~~~~~

Just like the CALC command mentioned `above <#calc>`__, the
`SELECT <#select>`__ command is very important to let you accomplish
both common and advanced tasks. Basically the SELECT command allows you
to select a subset of the reflections based on the reflection indices,
data column values, and some special properties. Multiple SELECT
commands can be combined to give even more control. For a full
description of all options you will have to read the `reference
section <#select>`__.

For a detailed description see `SELECT <#select>`__

Examples (many more options exist)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

SELECT ALL
    Select all reflections
SELECT COL 1 = ABSENT
    Select all reflections with a MNF in column 1
SELECT INVERT
    Invert the current selection
SELECT RESOL > 2.5
    Select reflections with a resolution numerically greater then 2.5
    (Note: this selects the lowest resolution data)
SELECT RESOL > 2.5 < 10.0
    Select reflections in the resolution range from 2.5 to 10 Angstrom
SELECT COL fobs > COL sigma
    Select reflections with a larger value in column fobs then column
    sigma
SELECT INDEX h = 0
    Select reflections in the 0kl plane
SELECT INDEX l ZONE 2n + 1
    Select reflections with an odd l index
SELECT INDEX h + k + l ZONE 2n + 1
    Select reflections with h+k+l is odd
SELECT MULTI 3
    Select reflections on a 3-fold axis
SELECT NOT MULTI 3
    Select reflections that are not on a 3-fold axis
SELECT PHAERR
    Select centric reflections with an invalid phase
SELECT SYSABS
    Select reflections that should have been systematically absent
SELECT NONE
SELECT PLUS INDEX h = 0
SELECT PLUS INDEX k = 0
SELECT PLUS INDEX l = 0
SELECT RESOL > 5
    Select reflections with (H=0 or K=0 or L=0) and (resolution > 5
    Angstrom)

--------------

Delete columns or maps
~~~~~~~~~~~~~~~~~~~~~~

The `DELETE <#delete>`__ command removes maps and data columns to free
up space in the data base.

For a detailed description see `DELETE <#delete>`__

Examples
^^^^^^^^

DELETE MAP
    Delete the density map
DELETE COL 3 4
    Delete columns 3 and 4

--------------

Delete reflections
~~~~~~~~~~~~~~~~~~

The `PURGE <#purge>`__ command removes reflections to free up space in
the data base. Reflections to be removed should be deselected by the
SELECT command prior to using purge. See `SELECT <#select_examples>`__
for more information on the SELECT command.

For a detailed description see `PURGE <#purge>`__

Examples
^^^^^^^^

PURGE
    Delete all reflections that are currently not selected
PURGE NODATA
    Delete all reflections that have MNFs in all columns

--------------

Expand to lower symmetry space group
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes you may want to reduce the symmetry of your space group and
explicitly generate the symmetry related reflections. In most cases you
will want to expand to P1 and generate data for a full hemisphere of
reciprocal space. The `EXPAND <#expand>`__ command allows you to do this
while updating your phase and/or Hendrickson-Lattman coefficients as
needed. You can even expand to non-P1 space groups as long as it is a
proper sub space group.

For a detailed description see `EXPAND <#expand>`__

Examples
^^^^^^^^

EXPAND
    Expand to P1
EXPAND 3
    Expand to P2

--------------

Create a map from structure factors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `FFT <#fft>`__ command takes two columns with structure factors and
uses them to create a density map using an in-memory fast Fourier
transform. The structure factors will be left unchanged. The resulting
map will cover exactly a full unit cell.

For a detailed description see `FFT <#fft>`__

Examples
^^^^^^^^

FFT COL 1 2
    Create a map based on structure factors in columns 1 and 2

--------------

Create structure factors from a map
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `MAP2SF <#map2sf>`__ command inverts a density map to structure
factors using an in-memory fast Fourier transform. The map must already
be in memory before using the MAP2SF command and the map will be
destroyed upon completion of the command.

For a detailed description see `MAP2SF <#map2sf>`__

Examples
^^^^^^^^

MAP2SF
    Invert the map held in memory and store the resulting structure
    factors in columns with labels COLUMN\_###, where ### is the next
    available column number. This only generates structure factors for
    reflections that are already in the data base
MAP2SF COL Fmap Pmap
    As above, but use column labels Fmap and Pmap instead
MAP2SF RESOL 2.5 COL Fmap Pmap
    As above, but generate all reflections to 2.5 Angstrom resolution.
    (Even for reflections that were not yet present in the data base)

--------------

Create Hendrickson Lattman coefficients from phase and fom
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `HLCONV <#hlconv>`__ command generates Hendrickson Lattman
coefficients from the phase and figure of merit of a reflection. Because
the latter represent a unimodal phase distribution, only the A and B
coefficients are generated. You should not use this command if your
phase and FOM were not derived from a unimodal distribution (*e.g.*
heavy atom phasing). In that case use your original Hendrickson Lattman
coefficients.

For a detailed description see `HLCONV <#hlconv>`__

Examples
^^^^^^^^

HLCONV COL 2 3
    Generate the A and B Hendrickson Lattman coefficients based on
    phases and fom in columns 2 and 3 respectively. The new columns will
    have labels PA and PB

--------------

Convert intensities to amplitudes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `I2F <#i2f>`__ command takes two columns with an intensity and its
sigma and produces two new columns with the amplitude and corresponding
sigma. Negative intensities are set to MNFs. This command treats weak
data reasonably, but it is recommended that you use the CCP4
`TRUNCATE <truncate.html>`__ program.

For a detailed description see `I2F <#i2f>`__

Examples
^^^^^^^^

I2F col 1 2
    Convert the intensity and its sigma in columns 1 and 2 to the
    amplitude and its sigma. The converted data is stored in columns
    with labels Fobs and Sigma-Fobs

--------------

Select a subsection of a map
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `MAPLIMIT <#maplimit>`__ command allows you to select a subsection
of a map. You can specify the new map limits as grid points, fractional,
or orthogonal coordinates. Alternatively, you can give the name of a PDB
file and the new limits will be set to cover the molecule.

**NOTE: The current implementation does not allow the output map to
exceed the length of a unit cell axis. This limitation will disappear in
the next release of SFTOOLS.**

For a detailed description see `MAPLIMIT <#maplimit>`__

Examples
^^^^^^^^

MAPLIMIT GRID -12 34 0 56 3 29
    Truncate the map so that it covers grid points -12 to 34 along x, 0
    to 56 along y and 3 to 29 along the c axis.
MAPLIMIT PDB file.pdb PLUS 5
    Truncate the map so that it covers the molecule in the pdb file with
    name "file.pdb" plus a 5 Angstrom boundary region.

--------------

Modify indices by matrix multiplication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `REINDEX <#reindex>`__ command can be used to change the reflection
indices. All indices will be multiplied by a user-specified matrix. It
is up to the user to make sure the matrix is appropriate. The program
will also transform the global and dataset cell parameters according to
the supplied matrix. However, the user will have to update the space
group if needed. Phase columns and Hendrickson-Lattman coefficients will
not be updated. The indices will be resorted and reduced to the CCP4
asymmetric unit.

For a detailed description see `REINDEX <#reindex>`__

Examples
^^^^^^^^

REINDEX MATRIX 1 0 0     0 0 1     0 1 0
    Transforms H K L into H L K

--------------

Select an RFREE set
~~~~~~~~~~~~~~~~~~~

The `RFREE <#rfree>`__ command randomly selects a set of reflections to
be used for the Rfree set. Alternatively, the user can ask for thin
shells of reflections to be used for the Rfree set. The latter may be of
interest in cases with high non-crystallographic symmetry. However, to
get a reasonable number of shells, the individual shells are much
thinner than one would like.

For a detailed description see `RFREE <#rfree>`__

Examples
^^^^^^^^

RFREE 2000
    Select approximately 2000 reflections for the rfree set
RFREE 0.05
    Select approximately 5% of the reflections for the rfree set
RFREE 0.05 SHELL
    As above but select the reflections in thin resolution shells

--------------

Modify header information
~~~~~~~~~~~~~~~~~~~~~~~~~

All header information can be changed with the `SET <#set>`__ command.
The keywords **`ALL <#set_all>`__, `TITLE <#set_title>`__,
`LABELS <#set_labels>`__, `TYPES <#set_types>`__, `SORT <#set_sort>`__,
`CELL <#set_cell>`__, and `SPACEGROUP <#set_spacegroup>`__** can be
given to indicate which header item you wish to change. It is sufficient
to type only the first 3 characters of each keyword.

For a detailed description see `SET <#set>`__

Examples
^^^^^^^^

SET ALL
    SFTOOLS will prompt for all header information
SET CELL
    SFTOOLS will prompt for the unit cell parameters
SET TYPES
    SFTOOLS will prompt for all column types
SET TYP COL 3
    SFTOOLS will prompt for the type of column 3

--------------

Sort the reflections
~~~~~~~~~~~~~~~~~~~~

The `SORT <#sort>`__ command can be used to sort the reflections by
index, by resolution or by the value of one of the columns. This can be
helpful if you want to look at your reflections with the `LIST
REF <#list_reflections>`__ command.

For a detailed description see `SORT <#sort>`__

Examples
^^^^^^^^

SORT
    Sort the reflections with l changing fast, k intermediate and
SORT l k h
    Sort the reflections with h changing fast, k intermediate and l
    changing slow
SORT RESOL
    Sort the reflections by resolution. Highest resolution comes first
SORT RESOL DOWN
    Sort the reflections by resolution. Highest resolution comes last
SORT COL 1
    Sort the reflections by column 1. Lowest values of column 1 come
    first

--------------

Reduce reflections to the asymmetric unit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `REDUCE <#reduce>`__ command forces all indices to be within the
given asymmetric unit definition. By default the CCP4 definition is used
and it is recommended not to deviate from that. Phases and
Hendrickson-Lattman coefficients will be updated if necessary.

For a detailed description see `REDUCE <#reduce>`__

Examples
^^^^^^^^

REDUCE
    Force all indices to obey the CCP4 asymmetric unit definition
REDUCE TNT
    Force all indices to obey the TNT asymmetric unit definition

--------------

Merge reflections with identical indices.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `MERGE <#merge>`__ command merges reflections with identical
indices. With the exception of multi-record MTZ files, this situation
should not occur. In some benign cases the multiple reflections contain
identical information in all columns or only one reflection contains
valid data for any one column (the rest has MNFs). In the latter case
the MERGE command can be applied safely. If two or more reflections
contain non-identical data for one or more columns then you can force
merging by averaging by giving the `MERGE
AVERAGE <#merge_mode_average>`__ command. However, you most likely have
a major problem with your data and MERGE AVERAGE should only be used if
you know what caused the problem.

For a detailed description see `MERGE <#merge>`__

Examples
^^^^^^^^

MERGE
    Merge reflections if it is "safe"
MERGE AVERAGE
    Merge reflections by averaging

--------------

Command reference section
-------------------------

Following is the list of on-line help information. You can get this
during interactive use by typing the name of the command followed by the
keyword "HELP". *E.g.* ABSENT HELP will give you the help information
for the ABSENT command.

The available keywords/commands are:

    `**ABSENT** <#absent>`__, `**CALC** <#calc>`__,
    `**CHECKHKL** <#checkhkl>`__, `**COMPLETE** <#complete>`__,
    `**CORREL** <#correl>`__, `**DELETE** <#delete>`__,
    `**EXPAND** <#expand>`__, `**FFT** <#fft>`__,
    `**FOURPT** <#fourpt>`__, `**FULLMDF** <#fullmdf>`__,
    `**FULLMTZ** <#fullmtz>`__, `**HLCONV** <#hlconv>`__,
    `**I2F** <#i2f>`__, `**LIST** <#list>`__, `**MAP** <#map>`__,
    `**MAP2SF** <#map2sf>`__, `**MAPIN** <#mapin>`__,
    `**MAPLIMIT** <#maplimit>`__, `**MAPOUT** <#mapout>`__,
    `**MAPSTAT** <#mapstat>`__, `**MERGE** <#merge>`__,
    `**MODE** <#mode>`__, `**OPTION1** <#option1>`__,
    `**PHASHFT** <#phashft>`__, `**PLOT** <#plot>`__,
    `**PURGE** <#purge>`__, `**READ** <#read>`__,
    `**REDUCE** <#reduce>`__, `**REINDEX** <#reindex>`__,
    `**RFREE** <#rfree>`__, `**SELECT** <#select>`__,
    `**SET** <#set>`__, `**SORT** <#sort>`__, `**STOP** <#stop>`__,
    `**WINDOW** <#window>`__, `**WRITE** <#write>`__

--------------

ABSENT
------

::

      OPTION: ABSENT COL col_ids IF COL col_id = 0
      ============================================

      Option to generate Missing Number Flags (MNFs)

      This command will set the values in the columns
      specified by col_ids to absent if the value in
      the column specified by col_id is zero. A column
      identifier can be either the column number or the
      column label.
      The part following the IF keyword can actually be
      any valid selection expression (give SELECT HELP
      for more information on selection expressions)
      Reflections that are already deselected due to a
      previous SELECT command will not get any MNFs
      Also note that after this command a SELECT ALL
      command will be issued automatically.

      EXAMPLES:(assuming column 2 has label sigma)

      ABSENT COL 1 2 IF COL 2 = 0
      ABSENT COL 1 2 3 4 IF COL sigma = 0
      ABSENT COL 3 4 IF RESOL > 3.0

--------------

CALC
----

::

      OPTION: CALCulate  RESULT = expression
      ======================================

      Command to perform arithmetic on data columns.
      The expression is evaluated and stored in the
      column(s) indicated by result.

      The expression may contain operands and
      operators and should follow the Polish notation.
      This means that first the operands are given and
      then the operator. (e.g. 5+3 becomes 5 3 +). It
      is the users responsibility to assure that the
      correct number of operands are given for each
      operator (see list below). Operands may either be
      a value, a column_id or some kind of function
     - VALUE : integer, real or complex value. Complex
               values consisting of an amplitude and
               phase must be enclosed in parentheses
               e.g. i=(1.0 90.0) or square brackets if
               they represent the real and imaginary
               parts. e.g. i=[0.0 1.0].

     - COLUMN: A COLumn keyword followed by 1 or 2
               column_ids (numbers or labels). Give
               1 column_id if you wish to use the value
               in that column. Give 2 column_ids if the
               columns contain complex data. Enclose
               the keyword and the ids in parentheses
               if the columns contain an amplitude and
               a phase. Use square brackets if they
               contain the real and imaginary parts.
               If a column value is absent then the
               whole expression evaluates to absent.

     FUNCTION  one of the following built-in functions

     - C     : This evaluates to 0 for centric and to
               1 for acentric reflections
     - M     : This evaluates to the reflection
               multiplicity (e.g. 2 on two-fold etc)
     - P     : This evaluates to the phase restriction
               for centric reflection or MNF for
               non-centrics
     - H     : Returns the H-index for this reflection
     - K     : Returns the K-index for this reflection
     - L     : Returns the L-index for this reflection
     - RAN_U : Return a random number from a uniform
               distribution between 0 and 1. The set of
               random numbers depends on a seed value
               which is 1234 by default. To modify the
               seed value (and hence get a novel set of
               random numbers) you must issue a command
               CALC SEED number
               where number can be any positive integer
     - RAN_G : Return a random number from a Gaussian
               distribution with mean 0 and variance 1
               (see RAN_U for the SEED value)
     - RFREE(r)
               Randomly returns 0 and 1 values for use
               in RFREE for XPLOR. If 0 < r < 1 then r
               represents the fraction of data to use
               for the free R-factor. If r > 1 then it
               represents the absolute number of
               reflections to use.
     - STOL  : This evaluates to sin(theta/lambda)
     - STOLSQ: This evaluates to sin(theta/lambda)**2

      RESULT : Indicates where to store the result of
               the expression. RESULT can be:
      TYPE COLumn c1, (COLumn c1 c2), [COLumn c1 c2]
      In the first case a result is stored in column c1
      The column type will be set to TYPE or to R if no
      type is specified.
      In the second case a complex result is stored in
      columns c1 and c2 as an amplitude and phase resp.
      The column types are set by the program. In the
      third case, the complex result is stored as the
      real and imaginary parts resp. again with the
      column types set automatically.
      The result columns are created if necessary

      OPERATOR (REAL OPERANDS, COMPLEX OPERANDS)

          + (2,4) : Summation
          - (2,4) : Subtraction
          * (2,4) : Multiplication
          / (2,4) : Division
         ** (2,3) : Power. The base may be complex, the
                    exponent not (it can be any! real)
        SIN (1,-) : Sine
        COS (1,-) : Cosine
        TAN (1,-) : Tangent
        DTR (1,-) : Convert from degrees to radians
        RTD (1,-) : Convert from radians to degrees
        ABS (1,2) : Absolute value. For complex values
                    this is the complex vector length
         LN (1,-) : Natural logarithm
        EXP (1,-) : Exponent
        MIN (2,4) : Minimum of two values
        MAX (2,4) : Maximum of two values

      EXAMPLES:
      (assume the data base contains 4 columns named; fobs, sigma, fcalc and phase)
      CALC COL sigma = 0
        set column sigma to 0
      CALC COL sigma = COL fobs 10 /
        set sigma to fobs/10. The result column will
        keep column type Q
      CALC COL F/sigF = COL 1 COL 2 /
        add a column labeled F/sigF and store fobs
        over sigma in it. The result column will get
        column type R
      CALC COL stol = stol
        add a column labeled stol and store sin(theta)
        over lambda in it for each reflection
      CALC COL resol = 0.5 stol /
        create a column labeled resol and store the
        resolution in Angstrom {1/(2*stol)}
      CALC COL 5 = m
        add a column labeled COLUMN_005 and store the
        multiplicity for each reflection, e.g. 6 for
        00L reflections in hexagonal space group.
      CALC COL 7 = m
        identical including label COLUMN_005
      CALC COL centric = 1 c -
        add a column labeled centric that contains 0
        for acentrics and 1 for centrics
      CALC COL fcalc = COL fcalc ran_g 100 * +
        ran_g returns a random number from a Gaussian
        distribution. Multiply this by 100 to get a
        mean of 0 and variance of 100 (st. dev. of 10)
        and then add it to your fcalc
      CALC q COL newsigma = 2 COL sigma *
        add column with label newsigma twice the value
        found in column sigma. Set the type of the new
        column to Q!!!
      CALC J COL iobs = 0.001 COL fobs * COL fobs *
        add column with label iobs and type J iequal to 
        0.001*fobs*fobs.  ( intensity scaled by 0.001)
      CALC Q COL sigiobs = 0.002 COL sigma * COL fobs *
        add column with label sigiobs and type Q equal to 
        0.002*fobs*sigma.  ( SigI = 2*fobs*sigma scaled by 0.001)
      CALC (COL Fnew Pnew) = (COL 1 4) (COL 3 4) -
        create columns Fnew and Pnew and store in it
        fobs - fcalc as amplitude and phase.
        Column types are set to F and P
      CALC [ COL real imag ] = (COL 3 4)
        convert fcalc and phase to real and imagenary
        components and stored these in columns labeled
        real and imag, column types are set to R and R
      CALC COL test = rfree(0.05)
        select 5% of the reflections for XPLOR RFREE
      CALC COL test = rfree(3500)
        select 3500 reflections for XPLOR RFREE

--------------

CHECKHKL
--------

::

      OPTION: CHECKHKL [ASUNIT]
      =========================

      Command to check the reflection indices. It will
      report the following:
      - number of reflections outside asymmetric unit
      - number of centrosymmetric reflections
      - number of unallowed reflections
      - number of reflections found on the crystallographic symmetry axes

      ASUNIT  Asymmetric unit specification by:
      CCP4    Keyword to use CCP4 definition (default)
      BIOMOL  Keyword to use BIOMOL definition
      TNT     Keyword to use TNT definition
      MATRIX m11 m12 m13 m21 m22 m23 m31 m32 m33
              Give REDUCE HELP to get more information
              on how to define the asymmetric unit by
              the MATRIX keyword

--------------

COMPLETE
--------

::

      OPTION: COMPLETE [SETTINGS] [MODE] [ASUNIT] [COLUMN]
      ====================================================

      Command to list the data completeness. Only the
      reflections within the asymmetric unit are used.

      SETTINGS is one of the following:
      SHELLS n   Display data in n shells (n is saved)
      RESOL  r   Set maximum resolution [A] (default is
                 the maximum observed resolution)

      MODE is one of the following:
      NUMBERS    list the number of reflections in each
                 resolution bin. The default is to
                 print the completeness in percentage
      CUMULATIVE list the cumulative completeness. The
                 default is to list in resolution bins
      PDB        Make PDB file OBSERVED.PDB in which
                 OBS & ABS residues indicate observed
                 and absent reflections in rec. space
                 Observed reflections will have their
                 B-value set to the value in column 1
                 (Nice if column 1 contains F/SIGF)
      TABLE      Keyword. When given, the contents of
                 the table will be written to the file
                 COMPLETE.TAB (without header & totals)

      ASUNIT  Asymmetric unit specification by:
      CCP4    Keyword to use CCP4 definition (default)
      BIOMOL  Keyword to use BIOMOL definition
      TNT     Keyword to use TNT definition
      MATRIX m11 m12 m13 m21 m22 m23 m31 m32 m33
              Give REDUCE HELP to get more information
              on how to define the asymmetric unit by
              the MATRIX keyword

      COLUMN  By default, COMPLETE will list the
              completeness of the first 10 columns. The
              COLUMN keyword can be used to specify up
              to 10 specific columns.

      EXAMPLE: COMPLETE COL 1 3 5
      (list the completeness of columns 1, 3 & 5)

      Data completeness cannot be caught in a single
      number and resolution limits reported in the
      literature strongly depend on criteria unknown to
      the reader. A more objective (still not ideal)
      specification would be to state: the number of
      observed reflections corresponds to a 100%
      complete dataset to X angstrom resolution. a good
      estimate of the value X is given by SFTOOLS

--------------

CORREL
------

::

      OPTION: CORREL [COLUMN c1 c2] [SETTINGS] [TABLE]
      ================================================

      Command to calculate the correlation and R-factor
      between two columns.

      COLUMN      Keyword which is followed by
      - c1 c2     2 column-id''s, names or numbers

      SETTINGS changes the table layout as follows:
      SHELLS      Keyword followed by
      - nshell    displays data in nshell shells
      RESOLUTION  Keyword followed by
      - rmin rmax Resolution limits [A] (e.g. 10 2)

      TABLE      Keyword. When given, the contents of
                 the table will be written to the file
                 CORREL.TAB (without header & totals)

      The column labels of the table are:

      NSHELL     Number of resolution shell
      DMIN DMAX  Minimum/maximum resolution
      RFACT      Rfactor in percent
                 ( 200*Sum|col1-col2|/sum(col1+col2) )
      CORREL     Correlation coefficient
      F1 F2      Average value of col1 and col2 in the
                 resolution bin
      NREF       Number of reflections used for the
                 statistics

--------------

DELETE
------

::

      OPTION: DELETE [COLUMN col_ids] [MAP]
      =====================================

      Command to remove information from the data base
      COLUMN   : Keyword that is followed by column-id
                 Label(s) or number(s) to identify the
                 column(s) that will be deleted
      MAP      : Keyword to indicate that the map has
                 to be deleted

      EXAMPLES:

      DELETE COLUMN 5 6
      DELETE MAP

--------------

EXPAND
------

::

      OPTION: EXPAND [SPAGRP] [SILENT] [ASUNIT]
      =========================================

      Expand the reflections to lower symmetry.
      Phase information will be treated, the spacegroup
      information will be updated and the expanded.
      Reflections will be sorted.

      SPAGRP Space group number to which to expand. This
             must be a proper subgroup of the original
             space group (default is 1)
      SILENT Keyword to suppress output
      ASUNIT Asymmetric unit specification which is:
      CCP4   Keyword for CCP4 definition (default)
      BIOMOL Keyword for BIOMOL definition
      TNT    Keyword for TNT definition
      MATRIX Keyword followed by 9 integers specifying
             the asymmetric unit. Give REDUCE HELP for
             more details on the MATRIX definitions

      WARNING: if you expand to a SPACEGROUP that has
      its origin at a different position then the
      phases will not be correct (e.g. P212121 to P21)

      EXAMPLES:

      EXPAND           (expand to P1)
      EXPAND 3         (expand to P2)

--------------

FFT
---

::

      OPTION: FFT COL id1 id2 [STEP x] [GRID nx ny nz]
      ================================================

      Calculate a map based on the Fourier coefficients
      in columns id1 (amplitude) and id2 (phase).
      All active reflections are used. To restrict the
      data to a certain resolution use option SELECT.

      STEP  Force the program to use a grid size that
            is as close to x Angstrom as allowed
      GRID  Force the program to use nx, ny and nz
            grid points along the a, b and c axes
            respectively. The numbers must be multiples
            of 2, 3 and 5 and nx must be positive. If
            an illegal number is given the next valid
            number will be used

      If neither STEP nor GRID are specified the grid
      size defaults to 1/3 of the max. resolution

      Note: If your amplitudes are on absolute scale
      your map will be on absolute scale as well

      EXAMPLES
      ========

      FFT COL fobs pmir
      FFT COL 1 2
      FFT COL 1 2 GRID 96 60 81
      FFT COL 1 2 STEP 0.8

--------------

FOURPT
------

::

      OPTION: FOURPT MAPTYPE COLUMNS
      ==============================

      Command to generate Fourier coefficients for
      several standard transforms.

      MAPTYPE must be one of the following numbers:
       0 = ( F1 -  F2),P1      : Fo-Fc-map
       1 = (2F1 -  F2),P1      : 2Fo-Fc-map
       2 = (3F1 - 2F2),P1      : 3Fo-2Fc-map
       3 = |F1|,P1 + |F2|,P2
       4 = |F1|,P1 - |F2|,P2
       5 = |F1|,P1             : Straight Fourier
       6 = |F1|**2             : Patterson
       7 = (F1-F2)**2          : Patterson
       8 = |F1|*|F2|,(P1-P2)   : Phased trans. function
       9 = |F1|*|F2|,(-P1-P2)  : Phased trans. function

      COLUMNS is the COL keyword followed by column
      specifications. The columns have to be specified
      in the order in which they occur in the formulae
      F1 and F2 represent amplitude columns. P1 and P2
      represent phase columns. The map coefficients are
      stored in 2 new columns with labels FMAP and PMAP
      (amplitude and phase respectively). The Patterson
      map options generate only one column with label
      FPAT. If a dummy phase column with zeroes is
      required you could make it with the command:
      CALC COL PPAT = 0
      If you wish to weight your coefficients with a
      figure of merit you can do so with the command
      CALC COL FMAP = COL FMAP COL FOM *

      EXAMPLE: FOURPT 1 COL fobs fcalc pcalc

--------------

FULLMDF
-------

::


      NOTICE: Option FULLMDF will become obsolete in the next release and
              will no longer be supported. Try to avoid using it in scripts.

      OPTION: FULLMDF
      ===============

      This option converts a CCP4 MTZ into a BIOMOL MTZ
      In the first step the user must indicate how to
      generate absence flags.
      If a column with anomalous data is found (column
      type = D) then user input will also be requested
      to convert the anomalous data.
      The conversion between asymmetric unit definitions
      will be made automatically.

--------------

FULLMTZ
-------

::


      NOTICE: Option FULLMTZ will become obsolete in the next release and
              will no longer be supported. Try to avoid using it in scripts.

      OPTION: FULLMTZ
      ===============

      This option automatically replaces absence flags
      (-1E+10) by zeroes.

      If a column with anomalous data is found (column
      type is D) then user input will be requested if
      and how the anomalous data has to be converted

      Indices will be reduced to the CCP4 asymmetric
      unit automatically

--------------

HLCONV
------

::

      OPTION: HLCONV COL phase fom
      ============================

      Given a phase and figure of merit, this command
      will generated the A and B Hendrickson-Lattman
      coefficients. PLEASE NOTE that this is only
      advisable if your source of phase information
      is unimodal. If you have heavy atom phases then
      you SHOULD use the original bimodal phase
      distributions derived from them. This option
      may be useful to generate HL coefficients for
      phased refinement with XPLOR or REFMAC, based
      on FOM and phases from a map after density
      modification

      Example:
      HLCONV col 2 3

--------------

I2F
---

::

      OPTION: I2F COL I SIGI
      ======================

      Command to convert intensities plus their
      sigmas to amplitudes and corresponding sigmas.
      The argument consists of the keyword COL
      followed by the column labels (or numbers) of
      the columns containing I and SigI respectively

      Example: I2F COL 1 2
      Example: I2F COL Iobs Sigma-I

      The results are stored in columns with labels
      Fobs and Sigma-Fobs. NOTE: if columns with these
      labels exist then they will be overwritten !!!
      The I2F conversion is calculated as:

      Fobs = SQRT(Iobs)
      Sigma-Fobs = SQRT( SQRT( Iobs**2 + 0.5*Sigma-I**2) - I)


      NOTE: it may be better to use the CCP4 program
      TRUNCATE for this step since it treats negative
      and very weak data more elegantly (I2F flags
      negative data as absent!!!)

--------------

LIST
----

::

      OPTION: LIST [ITEMS]
      ====================

      Command to list header information
      -ITEMS is a list of header items to be shown
       (it suffices to give the first 3 characters)
       The default is to list all header items

       TITLE        title cards
       LABELS       column labels, numbers and types
       TYPE         column labels, numbers and types
       CELL         unit cell parameters
       DCELL       crystal-specific unit cell parameters
       DWAVE       dataset-specific wavelength
       SORT         sort order
       SPACEGROUP   space group related data

       REFLECTIONS  reflection data. This argument.
                    can be followed by COL and one to
                    six column specifiers

       Example: LIST REF col 1 2 11 12
                LIST SPA DCELL
                LIST

--------------

MAP
---

::

      OPTION: MAP RMS F1 [SHELLS n]
      OPTION: MAP CORREL F1 P1  F2 P2 [SHELLS n]
      ==========================================

      Option to calculate the r.m.s. of a map based
      on the amplitude of its coefficients or to
      calculate the correlation coefficient between
      two maps given the coefficients for both maps.
      The statistics will be listed as function of
      resolution.

      RMS    Keyword to calculate the map r.m.s.
             based on the amplitude in column F1

      CORREL Keyword to calculate the correlation
             coefficient. F1 P1 and F2 P2 are the
             column specifiers for the coefficients
             of map 1 and map 2 respectively

      SHELLS n   Keyword followed by the number of
                 shells to use in the analysis.
                 The given value will become the new
                 default. The original default is 20


      You can give only one keyword at a time, but
      if you give the CORREL keyword the program
      will automatically give you the r.m.s. values
      of the two maps being correlated

      EXAMPLE:

      MAP RMS 1
      (rms of a map based on amplitudes in column 1
      MAP CORREL fobs pmir faveraged paveraged
      (correlation between mir map and averaged map)
      MAP CORREL 1 2 5 6 SHELLS 10
      (As before but list only 10 resolution shells)

--------------

MAP2SF
------

::

      OPTION: MAP2SF [RESOL r] [F000] [COL id1 id2]
      =============================================

      Calculate Fourier coefficients from a map. The
      map to be transformed already has to reside in
      memory. You will always get coefficients for all
      reflections that are already in the data base
      (even if they are unselected).

      RESOL r
      This keyword indicates that ALL legal reflections
      (except F000) to a resolution of "r" Angstrom
      will be stored. The default is to store only the
      reflections for which there is already an entry
      in the data base. If there are no reflections in
      the data base and RESOL is not specified, then
      all legal reflections to a resolution of 3 times
      the largest grid spacing are calculated

      F000
      If given this indicates that F000 should also be
      kept (WARNING, many programs, potentially incl.
      SFTOOLS, do not like the infinite resolution of
      F000).

      COL id1 id2
      If id1 and id2 are text labels, then id1 and id2
      are given to the new amplitude and phase columns
      If id1 and id2 are numbers of existing columns
      then the new amplitude and phase columns will
      overwrite the id1 and id2 columns. If the COL
      keyword is not given then SFTOOLS will create 2
      columns with COLUMN_NNN labels where NNN is the
      column number

      Note: If your map is on absolute scale then the
      Fourier coefficients will be as well

      EXAMPLES
      ========

      MAP2SF
      MAP2SF COL Fnew Pnew
      MAP2SF RESOL 2.0 col 3 4
      MAP2SF RESOL 3 F000 col 3 4

--------------

MAPIN
-----

::

      OPTION: MAPIN FILENAME [FORMAT]
      ===============================

      Command to read in an electron density file.

      FILENAME    is the name of the file. The program
        will try to deduce the file format from the
        extension. It will recognize .mff, .map, .dn6
      FORMAT      can be one of the following:
        MFF, MAP, DN6
        This overrules the file name extension format

      File formats are:
      MFF   BIOMOL format
      MAP   CCP4 real*4 format
      DN6   FRODO / O map format

      NOTE: DN6 maps for hardware other then SGI may not
            be compatible with SFTOOLS. Please let me
            know about problems

      EXAMPLE:

      MAPIN file.mff
      MAPIN file.map
      MAPIN file.dn6
      MAPIN file.rho MFF

      Notes:
      The input map can be in any order. However it is
      stored internally in A-B-C order. There can only
      be one map in memory. An existing map has to be
      deleted before reading a new one (DELETE MAP)

--------------

MAPLIMIT
--------

::


      NOTICE: Option MAPLIMIT will become obsolete in the next release and
              will no longer be supported. Try to avoid using it in scripts.
              In the next release the functionality of MAPLIMIT will be
              moved to the MAPOUT command. This allows a fully general and
              robust implementation.

      OPTION: MAPLIMIT TYPE value(s)
      ==============================

      Command to restrict a map to certain limits.
      The limits can be specified in various ways as
      indicated by the TYPE keyword which can be one
      of the following:

      PDB filename PLUS r
        This returns a block of density that surrounds
        the atoms in the PDB file leaving an extra
        "r" Angstrom on all sides. If "PLUS r" is not
        specified a default of 5 Angstrom is used

      GRID a_l a_u b_l b_u c_l c_u
        This returns a block of density with lower and
        upper limits along a, b and c as given in grid
        coordinates

      FRACT a_l a_u b_l b_u c_l c_u
        This returns a block of density with lower and
        upper limits along a, b and c as given in
        fractional coordinates

      ORTHO a_l a_u b_l b_u c_l c_u
        This returns a block of density with lower and
        upper limits along a, b and c as given in
        orthogonal (Angstrom) coordinates

      EXAMPLES:

      MAPLIMIT PDB protein.pdb PLUS 4
      MAPLIMIT FRACT 0 0.5 0 0.5 0 0.5
      MAPLIMIT GRID -5 34 0 60 12 49

      Notes:
      Every gridpoint in the new map must be present in
      the current map. It is allowed to go outside the
      unix cell, but none of the new axes can be larger
      than a full unit cell axis. E.g. if the a-axis
      runs from 0 to 99, you can ask for the subset
      -5 to 20, but not -5 to 99

--------------

MAPOUT
------

::

      OPTION: MAPOUT FILENAME [FORMAT] [ORDER a b c]
      ==============================================

      Command to write a electron density file.

      -FILENAME is the name of the file. The program
       will try to deduce the file format from the
       extension. It will recognize .mff, .map, .dn6
      FORMAT   can be one of the following:
       MFF, MAP, DN6
       This overrules the file name extension format
      ORDER   By default the map is written with
              sections along C, rows along B and grid
              steps along A (ORDER A B C). By giving
              different permutations of A B C you can
              change the map layout.

      File formats are:
      MFF   BIOMOL format
      MAP   CCP4 real*4 format
      DN6   FRODO / O map format

      NOTE: I have only created DN6 maps for display on SGI
            If you use it on different hardware then please
            let me know if it works or not.

      EXAMPLE:

      MAPOUT file.mff
      MAPOUT file.map
      MAPOUT file.dn6
      MAPOUT file.rho MFF
      MAPOUT file.map ORDER A C B
      (sections along B, rows along C)

--------------

MAPSTAT
-------

::

      OPTION: MAPSTAT [SECTION a b c]
      ===============================

      Command to list header and statistics of the
      current map. Statistics are given per section
      and for the whole map. You can specify which
      axis to use as sectioning axis by the SECTION
      keyword followed by the required sectioning
      axis. If no SECTION keyword is given then
      sections will be along the C-axis

      EXAMPLES:

      MAPSTAT
      MAPSTAT SECTION a
      MAPSTAT SECTION b

--------------

MERGE
-----

::

      OPTION: MERGE MODE
      ==================

      Command to merge reflections with identical
      indices. There are two modes:
      SAFE     Reflections are only merged if none of
               the columns has valid data entries in
               more than one reflection [default]
      AVERAGE  Merging is done by taking the average
               of the reflections

      MERGE merges multiple reflections with identical
      HKL, it does not merge different columns!!!
      The AVERAGE option exists, but you should never
      need to use it. If you need it you probably have
      some kind of problem with your data

      EXAMPLES:

      MERGE SAFE
      MERGE AVERAGE   (if you know what you are doing)

--------------

MODE
----

::


      NOTICE: Option MODE will become obsolete in the next release and
              will no longer be supported. Try to avoid using it in scripts.

      OPTION: MODE BATCH | INTERACTIVE
      ================================

      Bring the program in or out of Batch mode
      Use MODE BATCH when running SFTOOLS in batch
      This suppresses some questions by SFTOOLS, e.g.
      to hit return to see the next page in LIST REF
      Use MODE INTERACTIVE to return to interactive
      mode if that would ever be useful

      EXAMPLES:

      MODE BATCH
      MODE INTERACTIVE

--------------

OPTION1
-------

::


      NOTICE: Option MODE will no longer be supported in the next release.
              If you want to add your own options please contact me.

      OPTION: OPTION1 ....
      ====================

      Option1 is a dummy option that provides a means
      for users to add their own option. To do so,
      insert your code in this subroutine and modify
      this help text. If you wish to give the option a
      more sensible name then you have to modify the
      calling sequence in the main routine, the name
      in routine lstopt and of course the name in the
      subroutine header and in the last write statement

      If you plan to do some major programming, then it
      might be wise to contact me first, since your
      option might already exist in my experimental
      version. In addition I could supply you with a
      makefile and some other goodies

      If your option is of general interest, then you
      are urged to return the source code to me so that
      it can be implemented in the official release

      Bart Hazes, e-mail bart.hazes@ualberta.ca

--------------

PHASHFT
-------

::

      OPTION: PHASHFT [COL c1 c2] [SETTINGS] [TABLE] [COSINE]
      =======================================================

      Command to calculate the average phase difference
      between two columns of data.

      COLUMN      Keyword followed by
      - c1 c2     2 column-id''s, names or numbers

      SETTINGS changes the table layout as follows:
      SHELLS      Keyword followed by
      - nshell    Displays data in nshell shells
      RESOLUTION  Keyword followed by
      - rmin rmax Resolution limits [A] (default is to
                  use the observed resolution limits)

      TABLE     Keyword. When given, the contents of
                the table will be written to the file
                PHASHFT.TAB (without header & totals)

      COSINE    Keyword to instruct the subroutine to
                list the cosine of the phase difference
                (Default is degrees)

--------------

PLOT
----

::

      OPTION: PLOT COL c1 VERSUS [RESOL] [COL c2]
      ===========================================

      Command to create a plot file with the binwise
      averages of column c1 on the y-axis. The x-axis
      can be either resolution bins or bins of values
      found in column c2.

      COL c1   Column keyword followed by column name
               or number

      VERSUS   Keyword to be typed literally

      RESOL    Keyword to be typed literally if you
               wish to plot as function of resolution

      COL c2   Column keyword followed by column name
               or number. The data in column c1 will
               be given as function of the data in
               column c2

      SHELLS n Keyword followed by the number of bins

      EXAMPLE: plot col fobs versus col sigma

      The output file (PLOT.DATA) has 5 columns
      - The lower limit of the bin
      - The upper limit of the bin
      - The midpoint of the bin
      - The average of col c1 for the bin
      - The number of reflections in the bin

--------------

PURGE
-----

::

      OPTION: PURGE [SELECT] [NODATA] [YES]
      =====================================

      Command to delete records. Records to be deleted
      must first be deselected by command SELECT.
      Command SELECT may be given from the main menu
      or optionally from within PURGE. For help on
      SELECT, type SELECT HELP from the main menu

      YES     Keyword to indicate that no confirmation
              should be requested. By default, PURGE
              will ask a confirmation before starting
              Keyword yes must appear before select

      NODATA  Keyword to indicate that all reflections
      which have absent flags in all columns should be
      deleted

--------------

READ
----

::

     OPTION: READ FILENAME [FORMAT] [COLUMN columns]
     ===============================================

     Command to read a structure factor file.

     -FILENAME is the name of the file. The program
      will try to deduce the file format from the
      extension. It will recognize .mtz, .mdf, .snd,
      .xpl, .tnt, .31, .fin, .phs, .df, and .fmt
     FORMAT can be one of the following: MTZ, MDF,
      SND, XPL, TNT, 31, FIN, PHS, DF or FMT
      This overrules the file name extension format
     COLUMN Keyword to select specific columns
      Columns can be specified by numbers or labels
      (also mixed), and in any order. The order of
      columns in SFTOOLS will reflect the specified
      order and not the order on the MTZ. A column
      may even be listed more than once!
      If no columns are given, then the program will
      ask the user for each column  whether or not
      to include it. If no COLUMN keyword is given
      all columns are read
      THE COLUMN KEYWORD MUST BE THE LAST ARGUMENT !

     File formats are:
     MTZ   CCP4 format
     MDF   old BIOMOL format
     SND   BIOMOL ascii format
     XPL   XPLOR format
     TNT   TNT format
     31    PHASES short and long binary formats
     FIN   Xtalview fin-format
     PHS   Xtalview phs-format
     DF    Xtalview double fin-format
     FMT   File in arbitrary ASCII format

     NOTE: All formats except MTZ may need additional
     header information. The program will request this
     information interactively if necessary. All but
     the MTZ, MDF, SND and FMT formats have certain
     restrictions on the number, order, and type of
     columns. It is the users responsibility to use the
     COLUMN keyword to read the appropriate number and
     types of columns. SFTOOLS will check that you
     obey these restrictions.

     EXAMPLES:

     READ myfile.mtz
     (read all columns of myfile.mtz in mtz format)

     READ myfile.hkl MTZ COLUMN 3 1 2
     (read columns 1, 2 and 3 of myfile.hkl in mtz
     format. Column 3 will be column 1 in SFTOOLS)

     READ myfile.mtz COL
     (read myfile.mtz in mtz format and ask for each
      column whether or not it should be read)

     READ myfile.fmt
     (read the ASCII file myfile.fmt The program will
      figure out the format for you based on the first
      line(s) of the file. It will ignore character
      data amidst reflection data but you must edit
      out header lines (try the unix TAIL command).
      Also, all lines must have the same format as the
      first line(s). The program will ask you how many
      lines each reflection uses so that it can handle
      reflections that span more than one line. It
      also asks you how phase information is encoded
      Your file must contain the indices in the first
      three data columns (preceding character data is
      no problem though). If this is not the case then
      you can use the unix AWK command to fix it.
      The following awk command would correct a file
      which has the indices in columns 3, 4 and 5

      awk '{printf("%4d%4d%4d%8.1f%8.1f",$3,$4,$5,$1,$2)}' file_in > file_out

      One last note: if your file contains missing
      data in some manner (e.g. Sigma=0) then you will
      have to use the ABSENT command to insert proper
      missing number flags (MNFs) for these reflections

--------------

REDUCE
------

::

      OPTION: REDUCE [SILENT] [ASUNIT]
      ================================

      Command to reduce reflections to the asymmetric
      unit. Phases & Hendrickson/Lattman coefficients
      (if present) will be updated

      SILENT  Keyword to suppress output
      ASUNIT  Asymmetric unit definition which can be:
      CCP4    Use CCP4 definitions (default)
      BIOMOL  Use BIOMOL definitions
      TNT     Use TNT definitions
      MATRIX  Keyword followed by 9 integers i1-i9
              which define a packing matrix definition

      The asymmetric unit is defined as the set of
      reflections which maximize:
      (i1*h+i2*k+i3*l)*1024**2+(i4*h+i5*k+i6*l)*1024+
       i7*h+i8*k+i9*l

      Example: P4 hkl:h>=0,l>=0 0kl:k>=0 h>0:k>0
      MATRIX  0 0 1  1 1 0  0 1 0
      0 0 1 selects the hemisphere L>=0
      1 1 0 selects the quadrant H+K>=0
      0 1 0 selects the 0kl instead of h0l

      EXAMPLE:
      REDUCE CCP4
      Reduces all indices to the asymmetric unit defined by CCP4

--------------

REINDEX
-------

::

      OPTION: REINDEX [MATRIX]
      ========================

      Command to transform the indices.
      MATRIX  Keyword followed by 9 numbers specifying
              the transformation matrix (row-wise). The
              program does not check the validity of
              the matrix and phases will not be changed
              The user also has to update the header
              manually if required

      After the transformation, the data are resorted
      and all cells transformed.

      EXAMPLE:
      REINDEX MATRIX 1 0 0  0 0 1  0 1 0
      Transforms H K L into H L K

--------------

RFREE
-----

::

      OPTION: RFREE nr_of_rfree [SHELL [width]]

      NOTE: this subroutine uses the CCP4 definition
      of RFREE data with 0=test set and 1=working set
      When you write out an XPLOR file then these flags
      are automatically converted to the reverse
      definition of XPLOR. A future version will also
      allow flags > 1 for multi-set RFREE flags

      Create a column named RFREE with values set to 0
      for RFREE reflections and 1 for the others. The
      number of RFREE reflections is determined by the
      nr_of_rfree argument. By default RFREE
      reflections are selected at random.

      If the optional SHELL keyword is given then RFREE
      reflections will be taken in resolution shells
      The width of the resolution shells is 1/400th of
      the resolution range unless specified otherwise
      by the user. You may wish to try this if you have
      non-crystallographic symmetry. However, to get a
      reasonable number of shells they have to be
      thinner than I would like. In the future I may allow to
      actually input your NCS symmetry

--------------

SELECT
------

::

      OPTION: SELECT [MODE] TARGET CRITERIUM
      ======================================

      Command to select a subset of reflections
      MODE may be one of the following words
       PLUS    : Add selected records to the set
       MINUS   : Remove selected records from the set
       ONLY    : Only selected records will be kept
       NOT     : Invert the selection criterium
                 (NOT can combine with other modes)
      CRITERIUM is one the following:
       = val   : select records with target = val
       > val   : select records with target > val
       < val   : select records with target < val
       >= val  : select records with target >= val
       <= val  : select records with target <= val
       <> val  : select records with target <> val
                 NOTE: "val" should be either a number
                       or a column reference, e.g. col 1
       ZONE z  : Only when target is INDEX !!! Selects
                 indices in zone z, where z is e.g.
                 2n or 3n or 2n + 1 etc
       PRESENT : > -1.0E+10 < 1.0E+10
       ABSENT  : NOT PRESENT
       ALL     : select all records, no target needed
       NONE    : deselect all records, no target needed
       INVERT  : select unselected records & vice versa
       SYSABS  : select records with an illegal hkl
       CENTRO  : select centrosymmetric reflections
       ASUERR  : select reflections outside as. unit
       PHAERR  : select centrosymmetric reflections
                   with an illegal phase
       MULTI n : select records with multiplicity of n

      TARGET is one of the following:
      COLUMN c1  Keyword followed by column-id
                 Criterium refers to column c1
      RESOL      Criterium refers to the resolution
      INDEX idx  Keyword followed by index-code
                 index-code is [[+|-]H][[+|-]K][[+|-]L]

      EXAMPLES:
      ========

      In the next examples CURRENT SELECTION is the
      selection from some previous SELECT command

      MODES: ONLY, PLUS, MINUS, NOT
      =============================
      SELECT col 2 > 0
        current selection AND this selection. So the
        AND operator is the default. E.g. you get a
        subset of the current selection
      SELECT only col 2 > 0
        select refs that match this criterium
        independent of the current selection
      SELECT plus col 2 > 0
        current selection OR this selection
      SELECT minus col 2 > 0
        current selection AND NOT this selection
      SELECT not col 2 > 0
        current selection AND NOT this selection
      SELECT plus not col 2 > 0
        current selection or not this selection


      TARGETS: COLUMN, RESOL, INDEX
      =============================
      SELECT column 2 > 0 < 100
        select if value in column 2 is > 2 and < 100
      SELECT column 1 > column 2
        select if value in column 2 is > in column 1
      SELECT resol < 8
        select if resolution is better then 8 A
      SELECT INDEX h = 0
        select the 0kl plane
      SELECT INDEX h - k = 0
        select the hhl plane
      SELECT INDEX h + k + l zone 2n + 1
        select reflections with h+k+l is odd
      SELECT index h = 0
      SELECT index k = 0
      SELECT index l zone 2n +1
        select 00l reflections with l is odd

      CRITERIUM
      =========
      SELECT col 1 = present
        select reflection if column 1 is not MNF
      SELECT col 1 = absent
        select reflection if column 1 is an MNF
      SELECT all
        select all reflections
      SELECT none
        deselect all reflections
      SELECT invert
        invert the selection currently in memory
      SELECT sysabs
        select reflections with indices that should be
        systematically absent (e.g. hkl = 0 3 0 in P2sub1)
      SELECT centro
        select centrosymmetric reflections
      SELECT asuerr
        select reflections outside the asymmetric unit
      SELECT phaerr
        select centrosymmetric reflections with illegal
        phase
      SELECT multi n
        select reflections on a n-fold symmetry axis
        (n must be an integer)

--------------

SET
---

::

      OPTION: SET ITEMS
      =================

      Command to set header information
      -ITEMS is a list of header items to be changed
       ALL = all header items,
       TITLE = title cards,
       LABELS = column labels,
       TYPES = column types,
       CELL = unit cell parameters (not used by MTZ),
       DCELL = crystal-specific unit cell parameters,
       DWAVE = dataset-specific wavelength,
       SORT = sort order,
       SPACEGROUP = space group related data

      The program will list the old header information.
      The information given by the user will be used to
      replace the old data. If the user just gives a
      return, the old data will be kept.

      For the DCELL and DWAVE keywords the user will be given a list
      of crystals and datasets respectively. Only one line can be updated
      at a time.

      For the LABELS and TYPES keywords it is possible
      to modify a single column by giving the column
      ID for that column

      Examples:
      SET DCELL
      SET TYPES
      SET TYPES COL 5

--------------

SORT
----

::

      OPTION: SORT CRITERIUM DIRECTION
      ================================

      Command to sort the structure factor data base.

      CRITERIUM can be one of the following
      -[h k l] sort order may be specified as h k l
       or 1 2 3 or permutations thereof. The default
       is to sort on the current sort order as defined
       in the header info. The indices are always
       sorted in ascending order
      -RESOLution  Sort on resolution
      -COLumn      Sort on content of column id

      DIRECTION can be either:
      -UP    Ascending sort order (this is the default)
      -DOWN  Descending sort order

      Missing data is treated internally as a very
      small number (-1E+10) and will be sorted as such

      WARNING, the RESOL and COL options will mess up
      the index sort order. It is advised to restore
      the old sort order by another call to SORT to
      prevent funny behavior of subsequent options or
      programs :-)

      EXAMPLE: SORT l k h
               SORT resol
               SORT resol down
               SORT col fobs

--------------

STOP
----

::

    OPTION: STOP
    ============

    Terminate the program

--------------

WINDOW
------

::


      Notice: This option is still in construction. Use it with care!

      OPTION: WINDOW NWIN COL col_id 
      ===============================

      This option creates a new column in which it
      stores a local average of column col_id as
      function of resolution. The average is calculated
      by sorting the reflections by resolution and then
      sliding a window of NWIN reflections along all
      data points in column col_id. The central point
      in the window is set to the average value. For
      the first and last NWIN/2 reflections, which are
      never in the center of the window, the average
      will be obtained by linear extrapolation

--------------

WRITE
-----

::

     OPTION: WRITE FILENAME [FORMAT] [COLUMN columns] [HEADER]
     =========================================================

     Command to write a structure factor file.

     -FILENAME is the name of the file. The program
      will try to deduce the file format from the
      extension. It will recognize .mtz, .mdf, .snd,
      .xpl, .tnt, .31, .fin, .phs and .df
     FORMAT can be one of the following:
      MTZ, MDF, SND, XPL, TNT, 31, FIN, PHS or DF
      or a valid FORTRAN format string (see below).
      This overrules the file name extension format
     COLUMN Keyword to select specific columns.
      Columns can be specified by numbers or labels
      (also mixed), and in any order. The order of
      columns in the output file will reflect the
      specified order and not the order in SFTOOLS
      A column may also be given, and therefore
      written, twice (not always allowed, e.g. MTZ)
      If no columns are given, then the program will
      ask the user for each column whether or not to
      include it. If no COLUMN keyword is given all
      columns are read.
      THE COLUMN KEYWORD MUST BE THE LAST ARGUMENT !

     HEADER Keyword to specify that only the header
            should be written to the given file

     File formats are:
     MTZ   CCP4 format
     MDF   old BIOMOL format
     SND   BIOMOL ascii format
     XPL   XPLOR format
     TNT   TNT format
     31    PHASES long and short binary formats
     FIN   Xtalview fin-format
     PHS   Xtalview phs-format
     DF    Xtalview double fin-format

     NOTE: several file formats do not allow absent
      data. If one or more written columns of a
      reflection contain absent data then the whole
      reflection is rejected

     NOTE: For .fin and .df files sigma and amplitude
      columns are set to 9999 and 0 respectively if
      the sigma column is 0 or either column contains
      absent data

     NOTE: TNT files can have the following columns
      in the given order:
      Fobs, Sigma
      Fobs, Sigma, Phase, FOM
      Fobs, Sigma, Rfree
      Fobs, Sigma, Rfree, Phase, FOM
      Rfree, if present, MUST be column three.

     NOTE: Only the following columns make sense for
      XPLOR: FOBS, SIGMA, FOM, PHASE, WEIGHT, TEST
             FCALC, FPARTIAL, PA, PB, PC and PD.
      Make sure you write out at most one of each type.
      Use the COLUMN option to prevent the output of
      non-sensible columns. If the first 3 characters
      of a column label match the first 3 characters
      of FOBS, SIGMA, FOM, PHASE, WEIGHT, TEST, FCALC,
      FPARTIAL, PA, PB, PC or PD then SFTOOLS assumes
      that that is the XPLOR column type. If your
      column label does not match any of these, SFTOOLS
      will ask you to specify the XPLOR column type

     If you provide a Fortran FORMAT string then the
     first three values MUST be written as integers
     since these are the indices. ALL!!! other values
     MUST!!! be written as reals because that is how
     they are stored inside the program

     Examples:
     WRITE myfile.mtz COLUMN 1 2
     (write column 1 and 2 to myfile.mtz as MTZ file)
     WRITE myfile.hkl MTZ COL 2 1
     (write mtz with column 1 and 2 in reversed order)
     WRITE myfile.hkl FORMAT('HKL ',3i5,2f8.3)
     (write four columns in given format)

--------------

MTZ column types
----------------

::

    Allowed are the following types :

    H = index h, k or l   P = phase angle
    J = intensity         F = amplitude
    D = anom. diff.       Q = standard deviation
    W = weight factor     B = batch number
    I = any integer       R = any real
    A = phase prob. coeff. (Hendrickson/Lattman)
    Y = M/ISYM, partial/reject flag and sym. number

--------------

Notes
-----

Memory requirements are governed by parameters set in the sftools.inc
file, and these may need to be increased for large problems. The program
will inform you if your problem exceeds the current limits. If the
program aborts before any program output is displayed (you may see a
message "killed") then the array sizes are set too large to be handled
by your computer. You must either decrease the array sizes, free up or
buy more memory, or increase your swap file.

Program updates in between CCP4 releases will be made available by the
author. Please subscribe to the SFTOOLS users list (by sending
`me <mailto:bart.hazes@ualberta.ca>`__ an e-mail) or subscribe to the
`CCP4 bulletin board <http://www.ccp4.ac.uk/ccp4bb.php>`__ to be
notified of changes.

History
-------

SFTOOLS was created during 1993 at the university of Groningen while
converting the BIOMOL software package to the MTZ standard. During the
period 1994 till 1998 development was continued a the University of
Alberta. In 1998 SFTOOLS was integrated into the CCP4 program package.

 REFERENCES
-----------

#. B. Hazes, unpublished results

AUTHOR
------

Program written by Bart Hazes.

bart.hazes@ualberta.ca
