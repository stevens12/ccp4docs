MTZMADMOD (CCP4: Supported Program)
===================================

NAME
----

**mtzMADmod** - Generate F+/F- or F/D from other for anomalous data.

SYNOPSIS
--------

| **mtzMADmod hklin** *foo\_in.mtz* **hklout** *foo\_out.mtz*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

When anomalous data is present, the program `TRUNCATE <truncate.html>`__
outputs both the columns F+/F- (structure factor amplitudes for hkl and
its Friedel mate -h-k-l) and the columns F/D (mean structure factor
amplitude and anomalous difference), amongst others. However, in some
cases, an MTZ file may only contain one, and it may be necessary to
derive the other. That is the purpose of mtzMADmod.

If the input file contains F+/F- (as specified on the `LABIN <#labin>`__
line; up to 20 column pairs can be given), then F/D are derived as:

::


        F = 0.5*( F(+) + F(-) )
        D = F(+) - F(-)

The corresponding standard deviations are:

::


        SIGF = 0.5*SIGD
        SIGD = sqrt( SIGF(+)**2 + SIGF(-)**2 )

Alternatively, if the input file contains F/D (as specified on the
`LABIN <#labin>`__ line; up to 20 column pairs can be given), then F+/F-
are derived as:

::


        F(+) = F + 0.5*D
        F(-) = F - 0.5*D

The corresponding standard deviations are:

::


        SIGF(+) = sqrt( SIGF**2 + 0.25*SIGD**2 )
        SIGF(-) = SIGF(+)

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords, those
available being:

    `**END** <#end>`__, `**LABIN** <#labin>`__\ (compulsory),
    `**LABOUT** <#labout>`__, `**TITLE** <#title>`__

 LABIN <program label>=<file label>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(Compulsory.) A line giving the labels of the input columns from HKLIN
to be converted. Only the columns specified will be converted. The
allowed program labels are Fi SIGFi Di SIGDi (i=1,20) or Fi(+) SIGFi(+)
Fi(-) SIGFi(-) (i=1,20).

Note that the column types of Fi SIGFi Di SIGDi Fi(+) SIGFi(+) Fi(-)
SIGFi(-) should be 'F','Q','D','Q','G','L','G','L' respectively.

 LABOUT <program label>=<file label>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A line giving the labels of the output columns for HKLOUT. The program
labels are Fi SIGFi Di SIGDi Fi(+) SIGFi(+) Fi(-) SIGFi(-) (i=1,20).

 TITLE <title>
~~~~~~~~~~~~~~

Title to be used in output log file and in output hkl file.

 END
~~~~

Terminate input.

 INPUT AND OUTPUT FILES
-----------------------

The input files are

 Control data
    Keyworded input.
 HKLIN
    The input reflection data file in standard MTZ format.

The output files are

 HKLOUT
    The output reflection data file in standard MTZ format.

EXAMPLES
--------

Deriving F+/F- columns from F/D columns for 4 wavelength MAD dataset:

::


    mtzMADmod \
    hklin yb_l1234.mtz \
    hklout yb_l1234_pm.mtz <<eof
    LABI -
         F1=FYBL1 SIGF1=SFYBL1 D1=DYBL1 SIGD1=SDYBL1  -
         F2=FYBL2 SIGF2=SFYBL2 D2=DYBL2 SIGD2=SDYBL2  -
         F3=FYBL3 SIGF3=SFYBL3 D3=DYBL3 SIGD3=SDYBL3  -
         F4=FYBL4 SIGF4=SFYBL4 D4=DYBL4 SIGD4=SDYBL4 
    LABO -
         F1(+)=F_YBL+1 SIGF1(+)=SIGF_YBL+1 F1(-)=F_YBL-1 SIGF1(-)=SIGF_YBL-1   -
         F2(+)=F_YBL+2 SIGF2(+)=SIGF_YBL+2 F2(-)=F_YBL-2 SIGF2(-)=SIGF_YBL-2   -
         F3(+)=F_YBL+3 SIGF3(+)=SIGF_YBL+3 F3(-)=F_YBL-3 SIGF3(-)=SIGF_YBL-3   -
         F4(+)=F_YBL+4 SIGF4(+)=SIGF_YBL+4 F4(-)=F_YBL-4 SIGF4(-)=SIGF_YBL-4  
    END
    eof

Simple unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`mtzMADmod.exam <../examples/unix/runnable/mtzMADmod.exam>`__

AUTHOR
------

Eleanor Dodson, York University
