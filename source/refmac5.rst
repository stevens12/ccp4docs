REFMAC5 (CCP4: Supported Program)
=================================

User's manual for the program refmac 5.5
----------------------------------------

CONTENTS
--------

**General**

`Description <refmac5/description.html>`__

`References <refmac5/references.html>`__

`Credits <refmac5/credits.html>`__

`Bugs and Features <refmac5/bugs_and_features.html>`__

`**Keywords** <refmac5/keywords/index.html>`__

`New keywords for version 5.5 <refmac5/keywords/keywords_5_5.html>`__
|new|

`Principal X-ray keywords <refmac5/keywords/xray-principal.html>`__

`General X-ray keywords <refmac5/keywords/xray-general.html>`__

`Restraints keywords <refmac5/keywords/restraints.html>`__

`Harvesting keywords <refmac5/keywords/harvesting.html>`__

`**Usage** <refmac5/usage/index.html>`__

`TLS <refmac5/usage/tls_usage.html>`__

`Examples <refmac5/usage/examples.html>`__

`**Files** <refmac5/files/index.html>`__

`Input script <refmac5/files/input-script.html>`__

`Log file <refmac5/files/log.html>`__

`Input and output coordinate files <refmac5/files/coordinates.html>`__

`Input and output reflection files <mtzformat.html>`__

`Input and output TLS parameter files <refmac5/files/tls.html>`__

`Dictionary of ligands <mon_lib.html>`__

`**Dictionary** <refmac5/dictionary/index.html>`__

`Full description of dictionary <mon_lib.html>`__

`Dictionary entry from coordinate
file(s) <refmac5/dictionary/coord-dict.html>`__

`Dictionary entry from minimum
description <refmac5/dictionary/minimum-dict.html>`__

`Interactive sketcher to make dictionary
entry <../ccp4i/help/modules/sketcher.html>`__

`How to avoid the three letter residue name
problem <refmac5/files/coordinates.html#pdb_modres>`__

`Retrieve coordinates from dictionary <libcheck.html>`__

`List of standard monomer
descriptions <refmac5/dictionary/list-of-ligands.html>`__

`Links to non-commercial editors to derive coordinates from chemical
description <refmac5/dictionary/editor-links.html>`__

`**Theory** <refmac5/theory/index.html>`__

`Chirality <refmac5/theory/chiral.html>`__

`Latest version of refmac and related programs, direct from the Author's
website <http://www.ysbl.york.ac.uk/~garib/refmac/latest_refmac.html>`__

.. |new| image:: images/new.gif
   :width: 30px
   :height: 20px
