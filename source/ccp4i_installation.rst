.. raw:: html

   <div align="right">

.. rubric:: |image0|
   :name: section

.. raw:: html

   </div>

Installation of Version 2.1.0 of CCP4Interface
==============================================

**Released with CCP4 Version 6.3**

See also the `INSTALL file <../INSTALL>`__ for advice on installing the
main CCP4 suite.

Contents
~~~~~~~~

`What is Tcl/Tk/BLT? <#what_is_tcl>`__

`Do You Need to Install a New Tcl/Tk? <#do_you_need>`__

`Downloading Tcl/Tk/BLT <#where_to_get_it>`__

`Installing CCP4i <#installing%20the%20interface>`__

`Configuring CCP4i to Use Local Resources <#configure>`__

`The TEMPORARY Directory <#temporary>`__

`Programs not Distributed by CCP4 <#non-CCP4>`__

`Problems? <#problems>`__

| 
| See also
| `CCP4i Main Page <http://www.ccp4.ac.uk/ccp4i_main.html>`__
| `Problems with
  CCP4i <http://www.ccp4.ac.uk/ccp4i_main.html#problems>`__
| `Problems installing
  Tcl/Tk/BLT <http://www.ccp4.ac.uk/ccp4i/install_tcltkblt.html>`__.

What is Tcl/Tk and BLT?
~~~~~~~~~~~~~~~~~~~~~~~

Tcl and Tk are two separate but very closely linked packages - Tcl is a
scripting language like Perl and Tk is X-based 2D graphics. Both
packages are written in C and require compiling and building. A combined
version of the two packages is built into a program called **wish**. A
Tcl program without the graphics is called **tclsh**.

Wish is an interpreter which will process command line input or scripts.
The CCP4 Interface is a series of Tcl/Tk scripts. Tcl/Tk scripts can
have command structure and procedures and can be built up like Fortran
or C programs but they do not need to be compiled - they are interpreted
at run time.

There are several packages which provide add-on features to Tcl/Tk - the
interface uses the **BLT** package which provides graph plotting in the
loggraph utility. The combined Tcl/Tk/BLT program is called **bltwish**.

Do You Need to Install a New Tcl/Tk?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Tcl8.3/Tk8.3/BLT2.4** or later are the recommended versions. The
interface may run under earlier versions however it is possible that you
will experience problems.

You may already have Tcl/Tk installed on your machine. At least some
flavours of Linux come with Tcl/Tk installed.

NB: The pre-built CCP4 core packages for Linux, Mac and Windows now
include Tcl/Tk, so it is normally not necessary to install them.

Testing Tcl/Tk installed version
''''''''''''''''''''''''''''''''

You can find out which version is installed by typing:

::

    > bltwish
    % puts $tk_version
    % exit

Downloading Tcl/Tk/BLT
~~~~~~~~~~~~~~~~~~~~~~

You can download Tcl/Tk/BLT from the CCP4 FTP site, which carries the
source code as well precompiled executables for a number of major
platforms.

Go to ftp://ftp.ccp4.ac.uk/tcltk/README.html to access the files, and to
find more detailed instructions on how to install them.

Installing the CCP4Interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The CCP4 Setup File
^^^^^^^^^^^^^^^^^^^

Once you have installed CCP4 you will have the interface in the
directory $CCP4/ccp4i - this is referred to by the environment variable
CCP4I\_TOP which is set in the ccp4.setup script. Also set in the
ccp4.setup script is the environment variable CCP4I\_TCLTK which should
refer to the directory containing the wish, tclsh and bltwish
executables.

Finding the Right tclsh/wish/bltwish
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are three scripts in **$CCP4I\_TOP/ccp4i/bin** which are run to
start up the interface. The first thing these scripts do is ``exec`` one
of the Tcl/Tk/Blt executables which it expects to find in the directory
$CCP4I\_TCLTK (which is defined in the setup file as described
`above <#ccp4_setup>`__).

+--------------------------------+-------------------------------------------+
| **$CCP4I\_TOP/bin/ccp4i**      | uses bltwish                              |
+--------------------------------+-------------------------------------------+
| **$CCP4I\_TOP/bin/ccp4ish**    | uses tclsh8.\* (8.3 or better required)   |
+--------------------------------+-------------------------------------------+
| **$CCP4I\_TOP/bin/loggraph**   | uses bltwish                              |
+--------------------------------+-------------------------------------------+

If you do not have all the executables in the same place or the
specified version of wish/tclsh/blt you will need to edit (some of)
these scripts.

After that the commands to run CCP4i are:

::

    > source $CCP4/include/ccp4.setup
    > ccp4i [-help]

Once you have CCP4i running you can use the graphical interface to use
the configuration options:

Configuring CCP4i to Use Local Resources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The interface needs access to local resources (such as a web browser to
display help text) and must be configured so that it can find them. Once
the basic CCP4i is installed this can be done through an interface:

::

    > ccp4i -c<onfigure>

The Help button on the top right of the window will bring up
documentation in your web browser if (Catch 22!) CCP4i is configured
correctly to find your browser. If not then point your browser at:

``$CCP4I_TOP/help/general/configure.html`` (equivalent to
`$CCP4/ccp4i/help/general/configure.html <../ccp4i/help/general/configure.html>`__).

The TEMPORARY Directory
~~~~~~~~~~~~~~~~~~~~~~~

CCP4i has the idea of a TEMPORARY directory which by default is the same
as $CCP4\_SCR. CCP4i scripts put large output files such as map files in
the TEMPORARY directory. If you run jobs remotely or in batch from CCP4i
and the $CCP4\_SCR is local to the remote machine (*e.g.* /usr/tmp or
/tmp) then it may be difficult for users to access these files. In this
case you will need to set TEMPORARY to something such as
$HOME/temporary. You set the default definition of TEMORARY for all
users by editing the file $CCP4I\_TOP/etc/directories.def and changing
the definition of the parameter DEF\_DIR\_PATH,1 (see the file for
example). You may also need to warn users to create the appropriate
directory. Users can reset the definition using the *Directories and
ProjectDir* option in CCP4i.

Programs not Distributed by CCP4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CCP4i uses non-CCP4 programs to convert map file formats (mapman for O
and mbkall for Quanta) but if you have the graphics program installed
then you should have the conversion program available already.

In addition CCP4i has interfaces to some non-CCP4 programs, and some
non-CCP4 software packages come with their own CCP4i interfaces. A list
of these packages can be found in the `Additional
Programs <../ccp4i/help/general/additional.html>`__ section of the CCP4i
documentation.

Problems?
~~~~~~~~~

Please contact us if you have any problems: ***ccp4@ccp4.ac.uk***.

.. |image0| image:: ccp4i/help/images/weblogo263.gif
   :width: 245px
   :height: 120px
