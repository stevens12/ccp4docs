REFORIGIN (CCP4: Unsupported Program)
=====================================

NAME
----

**reforigin** - apply best origin shift to PDB atom co-ords according to
reference file

SYNOPSIS
--------

**reforigin XYZIN** *working.pdb* **XYZREF** *reference.pdb* **XYZOUT**
*output.pdb* [ **SPACEGROUP** *<sg\_name or sg\_num>* ] [ **DMAX**
*<dmax>* ] [ **CAONLY** *<true \| false>* ]

DESCRIPTION
-----------

"reforigin" automatically re-origins (and changes the a.u. if necessary)
a PDB dataset to a reference dataset (useful after MR when the model and
target structures have the same space group and cell, or nearly the same
cell). The required transformations are point-group dependent.

INPUT AND OUTPUT FILES
----------------------

The following input and output files are used by the program:

Input Files:

 XYZIN
    Input PDB file which is to be re-origined.
 XYZREF
    Input PDB file with reference structure.

The two files XYZIN and XYZREF need to contain the same list of atoms.

Output Files:

 XYZOUT
    Output file that contains the coordinates after the transformation
    and origin shift.

OTHER INPUT
-----------

Note that this input is currently set on the command line.

**SPACEGROUP** *<sg\_name or sg\_num>*
    This is optional if the spacegroup is in the working CRYST1 record.
**DMAX** *<dmax>*
    Maximum RMS deviation for fitting co-ordinates: default = 5 A.
**CAONLY** *<true \| false>*
    If true, use only the CA atoms to determine required
    transformations. If alternative conformations are present, the "A"
    conformation is assumed. The default is false, i.e. use all atoms.

OUTPUT
------

If the program is able to fit XYZIN to XYZREF, it will output:

::


    Translation modulus:                  3        3        6
    Equivalent position:                        -X+Y,  -X,  Z
    Fractional origin shift:         0.6667   0.3333   0.3333
    Orthogonal origin shift:         28.997   16.741   52.138
    RMS deviation:                    3.428

This gives the primitive symmetry operation applied (e.g. -X+Y, -X, Z)
and the translation (centering and/or origin shift, e.g. 2/3, 1/3, 1/3).
The RMS deviation is between the atoms of XYZREF and the transformed
atoms of XYZIN.

PROBLEMS
--------

The program assumes the standard orthogonalisation convention.

SEE ALSO
--------

`pdbset <pdbset.html>`__ - can apply arbitrary translations, see SHIFT
keyword.

AUTHORS
-------

Ian Tickle, Astex Technology
