ACORN (CCP4: Supported Program)
===============================

NAME
----

**acorn** - *ab initio* procedure for the determination of protein
structure using atomic resolution data or artificially extended data to
atomic resolution, and for finding sub-structures from anomalous or
isomorphous differences.

SYNOPSIS
--------

| **acorn hklin** *foo\_in.mtz* **hklout** *foo\_out.mtz* [**xyzin**
  *foo\_in.pdb*]
| [`Keyworded input <#keywords>`__]

CONTENTS
--------

`**DESCRIPTION** <#descgeneral>`__

-  `**General Description** <#descgeneral>`__
-  `**ACORN-MR** <#desacorn-mr>`__
-  `**ACORN-PHASE** <#desacorn-ph>`__
-  `**SCENARIOS** <#scenarios>`__

`**KEYWORDED INPUT** <#keywords>`__

-  `**General Keywords to select data, select model and preform
   miscellaneous functions** <#general>`__
-  `**Keywords Specific for ACORN-MR** <#ACORN-MR>`__
-  `**Keywords Specific for ACORN-PHASE** <#ACORN-PHASE>`__

`**INPUT AND OUTPUT FILES** <#files>`__

`**PRINT OUTPUT** <#print>`__

`**EXAMPLES** <#examples>`__

\ **Examples starting from some known atomic coordinates.**

-  `**Example 1: Use a constellation of correctly placed atoms as the
   starting point. For example, a Zn atom placed from sharpened atomic
   resolution Patterson.** <#example1>`__
-  `**Example 2: Use a constellation of correctly placed anomalous
   scatterers as the starting point. These may be sulphurs found from
   the anomalous diffraction data.** <#example2>`__
-  `**Example 3: Use a fragment already positioned by some other MR
   procedure (*e.g.* AMoRe).** <#example2>`__
-  `**Example 4: Test for the best result from 10 possible heavy atoms
   positioned from the Harker vectors alone, *i.e.* they are not
   necessarily on the same origin or hand.** <#example4>`__

\ **Examples starting from a large set of randomly positioned atoms.**

-  `**Example 5: Use a randomly placed single atom as a starting point
   for a complete structure solution. This is usually successful for
   metallo-proteins, for finding substructures from isomorphous or
   anomalous difference data and for solving small molecular
   structures.** <#example5>`__
-  `**Example 6: Use a randomly placed single atom as a starting point
   to determine the position of heavy atoms in a protein structure from
   the anomalous differences only.** <#example6>`__
-  `**Example 7: Use a randomly placed single atom as a starting point
   to determine the position of the sub-structure of anomalously
   scattering atoms in a protein structure from both the anomalous and
   isomorphous differences.** <#example7>`__
-  `**Example 8: Use a randomly placed single atom as a starting point
   to solve a small molecular structure.** <#example8>`__

\ **Examples starting from a molecular replacement search.**

-  `**Example 9: Use standard alpha helices as search models for a
   complete molecular replacement (MR) search.** <#example9>`__
-  `**Example 10: Use standard alpha helices as search models for random
   MR search.** <#example10>`__
-  `**Example 11: Use a fragment from a similar structure for MR
   search.** <#example11>`__

\ **Examples (re)starting using previously determined results.**

-  `**Example 12: Use externally determined phases and weights as a
   starting point.** <#example12>`__
-  `**Example 13: Restart using results from previous run of
   ACORN.** <#example13>`__
-  `**Example 14: Use the results from AMoRe given as Euler angles and
   translation components to position the model atoms
   correctly.** <#example14>`__

\ **Examples extending data resolution to 1.0 Å and starting with
positioned fragments or experimental phases.**

-  `**Example 15: Use all extended reflections in the file and start
   from a heavy atom.** <#example15>`__
-  `**Example 16: Use the extended reflections within 50.0 to 1.0 Å and
   start from Molecular Replacement model.** <#example16>`__
-  `**Example 17: Use the extended reflections within 50.0 to 1.0 Å and
   start from experimental phases.** <#example17>`__

`**AUTHOR** <#author>`__

`**REFERENCES** <#references>`__

`**SEE ALSO** <#seealso>`__

DESCRIPTION
-----------

ACORN [`1 <#reference1>`__] [`2 <#reference2>`__] [`3 <#reference1>`__]
[`4 <#reference2>`__] is a flexible and fast *ab initio* procedure to
solve a structure when the data are sufficient to separate atom sites in
the E maps. There are three catagories where this holds:

a. proteins with diffraction data to 1.2Å or artificially extended data
   to 1.0Å with observed data as low as 1.7Å;
b. small molecules where the data clearly reach to atomic resolution;
c. well-separated sub-structures such as Se or S atoms, or other
   anomalous/isomorphous scatterers, where even low resolution data will
   suffice.

The initial phase sets are generated from the atomic coordinates of a
putative structural fragment. The fragment can be made up in various
ways. Firstly, in simple cases, such as metalloproteins, small molecules
or for finding sub-structures, it is sufficient to make many tests
starting from a single randomly placed atom. Secondly, complete proteins
can certainly be solved starting from their sub-set of correctly
positioned heavier atoms, such as the sulphur sites which can be found
from their anomalous contributions. Thirdly, a molecular replacement
solution will provide a good starting set of phases for a protein which
diffracts to high resolution. The starting model may consist of the
whole contents of the asymmetric unit, a domain, or a smaller fragment
such as a standard alpha helix. The size of the fragment can be less
than 5% of the scattering matter of the unit cell. The fragments can in
principle be positioned using MR searches with normalised data within
ACORN.

Alternatively phase information from other procedures, for instance low
resolution phases previously determined for a protein for which there is
now atomic resolution data available, can be used to initiate the
procedure.

The starting phase sets are refined primarily using Dynamic Density
Modification (`DDM <#formuladdm>`__), supplemented by Patterson
superposition (`SUPP <#formulasupp>`__), real space Sayre Equation
Refinement (`SER <#formulaser>`__) and weak density enhancement (ENHS).

The observed reflections are divided into three groups: strong, medium
and weak reflections according to their normalized structure factors
(E-values). Correlation coefficients (CCs) between the observed and
calculated E-values for each class are used in different ways throughout
the procedures. All reflections are used to select likely trial sets;
the strong and weak reflections are used in the phase refinement and the
CC for the medium reflections acts as the figure of merit. This is a
major strength of ACORN: this CC provides a simple and unequivocal
criterion of correctness for a phase set.

When the resolution for observed reflections is only between 1.2Å and
1.7Å the `**UNIQUE** <unique.html>`__ procedure has to be run first to
extend data resolution to 1.0Å. Then in ACORN an expected value of E=1.0
will be given to all extended reflections using keyword
`**EXTEND** <#exte>`__.

The program has two main parts: ACORN-MR and ACORN-PHASE.

ACORN-MR:
~~~~~~~~~

ACORN-MR allows the evaluation of multiple starting models. These are
required

a. for the MR positioning of small fragments of a protein; such as an
   idealised alpha helix or a well characterised domain
b. for the use of a single atom as a model, useful for a metalloprotein
c. to position sub-substructures where starting from a single randomly
   placed atom is the essence of the application.

In all applications where a previously positioned fragment is available,
ACORN-MR is not required.

Furthermore, ACORN-MR provides an alternative molecular replacement
search procedure using normalised structure factors generated from the
observed data and the model. When the data are of a sufficiently high
resolution and the fit between model and structure is very good, this
works well. The best results have been obtained using idealised alpha
helices which may make up only a small part of the new structure but
which fit exactly. When searching for domains or whole molecules using
ACORN-MR it is not necessary to use atomic resolution data. In fact the
results from other MR programs using lower resolution data may be
obtained more quickly, and be more reliable.

If the "model" is a single atom, a rotation function search is not
required, and the MR "translational" search allows the systematic trial
of all starting points in the Cheshire Cell. This procedure can be used
to find anomalous scatterers from anomalous data where the resolution
can be as low as 3Å to 4Å. The grid needs to be about max\_resln/3.0

The results are scored using the highest CCs for all reflections, and
typically the best 100 or so are tested as starting sets for phase
refinement using ACORN-PHASE.

The solutions from the rotation function are given in terms of the
Eulerian angles: alpha, beta and gamma using the convention described by
Tony Crowther as in all CCP4 programs, *e.g.* ALMN, LSQKAB, PDBSET, DM
and AMoRe. The required ranges for the translational search are governed
by the space group and will be chosen by the program.

ACORN-PHASE:
~~~~~~~~~~~~

ACORN-PHASE refines a starting set of phases using DDM taking the CC for
the medium E-values to indicate a likely solution. There are four phase
refinement procedures in the suite, of which DDM is the most important:

-  Dynamic Density Modification (DDM) eliminates the negative densities,
   and truncates the highest density (for the first cycle this will be
   at the sites of the starting coordinates). The rest of density is
   modified according to a formula based on the standard deviation of
   the map and the cycle number. There are three kinds of map
   coefficients used in DDM and called DDM0, DDM1 and DDM2, see
   `**DDMK** <#ddmk>`__.
-  Patterson superposition (SUPP) generates a semi-sharpened Patterson
   sum-function map from the starting fragment.
-  Sayre Equation Refinement (SER) is carried out in real space using
   Fast Fourier Transforms instead of working directly with the phase
   relationships; the equations are identical but the real space
   formulation is much faster.
-  Weak density enhancement (ENHS) is used to find an envelope and raise
   the weak densities within the envelope.

ACORN-PHASE first uses DDM. If no solution can be found, a few cycles of
Sayre Equation Refinement or one cycle of enhancement may modify the
phase set sufficiently to allow the DDM algorithm to function more
effectively. The number of trials can be set by the user. See
`**NTRY** <#ntry>`__.

ACORN will stop automatically if the value of CC for the medium E-values
in DDM becomes greater than a preset value. The default value is
determined by ACORN, but this value needs to be adjusted according to
the data quality, particularly when determining anomalous scatterers
from SAD or MAD data. Maps calculated from the refined phases are
normally excellent; proteins can be built automatically and for
sub-structures the highest peaks indicate the true sites.

Reflection input:
^^^^^^^^^^^^^^^^^

ACORN reads pre-calculated E-values and amplitudes. Within CCP4 E-values
can be generated by `**ECALC** <ecalc.html>`__. The data resoluton can
be extended using CCP4 program `**UNIQUE** <unique.html>`__ and then
ACORN uses keyword `**EXTEND** <#exte>`__ to control how to use the
extended reflections. Known phases and weights can be input and refined
by ACORN-PHASE directly. Only the best set of phases and weights, that
with the highest CC for the medium reflections, is output to the
*foo\_out.mtz* file.

SCENARIOS:
~~~~~~~~~~

Solving an atomic resolution protein structure with a known starting fragment:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some known atom positions are available. These may be:

a. anomalous scatterers such as a metal site found in a metallo-protein.
   `**Example 1** <#example1>`__.
b. a sub-structure such as the sulphurs or seleniums. `**Example
   2** <#example2>`__.
c. a model obtained from some MR search. `**Example 3** <#example3>`__.

ACORN-PHASE will refine the initial phases calculated from these known
atom positions and produce an unbiased map.

d. If a set of possible atom sites is available which are not
   necessarily consistent, and which may not be on the same origin or
   hand, ACORN-PHASE can test each site in turn and select the best
   phase set. For instance they may be solutions to a Patterson
   superposition search of the Harker sections. `**Example
   4** <#example4>`__.

Solving an atomic resolution metallo-protein structure or finding the sub-structure or solving a small molecule structure by testing many randomly placed atoms:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

e. It is expected that the structure contains a heavy atom: many
   randomly positioned single atom sites are checked, the best 100 or so
   sets refined, and the best phase set of these output. `**Example
   5** <#example5>`__. `**Example 6** <#example6>`__. `**Example
   7** <#example7>`__. `**Example 8** <#example8>`__.
f. Solving a small molecule structure:
   Again, many randomly positioned single atom starting sites are
   checked and refined, and the best phase set output. The default
   parameters are set for the determination of a macromolecular
   structure, so it is best to set the resolution to the highest
   avalable, and to make sure the grid sampling is about 0.3Å. The
   `CONTENTS <#cont>`__ list should also be given if the Sayre Equation
   is to be used. `**Example 8** <#example8>`__.
g. To determine a sub-structure:
   Many randomly positioned single atom starting sites are checked and
   refined, and the best phase set output. Of course atomic resolution
   data is not required to find the sub-structure sites. `**Example
   6** <#example6>`__. `**Example 7** <#example7>`__.

Carrying out a MR search as a preliminary to phase refinement:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

h. Most proteins contain some standard fragments such as Alpha helices
   or Beta sheets. There is a coordinate library for such standard
   fragments available in $CLIBD/fraglib/. Good results have been
   obtained searching for the correct position of idealised Alpha
   helices (main chain plus CB) which then provide the starting fragment
   for ACORN-PHASE. Other motifs or standard fragments can be used in
   the same way. `**Example 9** <#example9>`__. `**Example
   10** <#example10>`__. `**Example 11** <#example11>`__.

Using some prior phase information:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

i. Some known phase information is available: use ACORN-PHASE to refine
   it. `**Example 12** <#example12>`__.

There is an option to restart the procedure:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

j. ACORN can be restarted using likely solutions from a previous run or
   using solutions from AMoRe. `**Example 13** <#example13>`__.
   `**Example 14** <#example14>`__.

Using extended reflections:
^^^^^^^^^^^^^^^^^^^^^^^^^^^

k. Use a known heavy atom position with all extended reflections.
   `**Example 15** <#example15>`__.
l. Use MR model from MOLREP with the extended reflections from 50.0 to
   1.0 Å. `**Example 16** <#example16>`__.
m. Use experimental phases from SAD with the extended reflections from
   50.0 to 1.0 Å. `**Example 17** <#example17>`__.

KEYWORDED INPUT
---------------

The data control is keyworded. Only the first 4 characters of a keyword
are significant. They can be in any order, except END (if present) which
must be last. Numbers and characters in "[ ]" are optional. Anything
input on a line after an exclamation mark or hash ("!" or "#") is
ignored and lines can be continued by using a minus sign.

It is essential to give LABIN to describe and define the input data, and
one keyword to direct the mode of ACORN (either to phase, restart or do
a MR search). Most other keywords are optional.

The keywords can be subdivided into groups:

-  `**General Keywords to select data and model** <#general>`__
-  `**Keywords for miscellaneous functions** <#miscell>`__
-  `**Keywords Specific for ACORN-MR** <#ACORN-MR>`__
-  `**Keywords Specific for ACORN-PHASE** <#ACORN-PHASE>`__

General keywords to select the data and model:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `**Reflection Selection Keywords** <#refsel>`__
-  `**Atom Selection Keywords** <#atmsel>`__

Keywords to restart the procedure, and for miscellaneous functions:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `**Restart the program using results from a previous
   run** <#restart>`__
-  `**Keywords for miscellaneous functions** <#miscell>`__

Keywords to Select reflections, and organise input data:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

LABIN <program label>=<file label>...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(COMPULSORY)

This keyword defines which items are to be used in the calculation. The
following program labels can be assigned, of which the first three are
essential. E is the normalised amplitude used as the target for the
structure solution. It may be based on the amplitude for a whole
structure or on an anomalous or isomorphous difference, or an estimate
of FH or FA derived from both disperive and anomalous differences. See
`**ECALC** <ecalc.html>`__.

::

      E     FP     SIGFP   [PHIN]     [WTIN]    [FCIN]    [FT]   PHIFT]
      E     Normalized structure factors.
      FP    Observed structure factors.
      SIGFP Sigma of FP.
      PHIN  Known phases.
      WTIN  Weights of known phases.
      FCIN  Calculated magnitudes with the known phases.
      FT    Amplitudes from final structure model.
      PHIFT Phases from final structure model.

If PHIFT is assigned ACORN will calculate phase error with proper origin
shifts for both enantiomorphs.

Example: To calculat phase errors.

::

      LABI E=EO FP=FP SIFFP=SIGFP FT=FC PHIFT=PHIC

If PHIN and WTIN are assigned they are used as the set of initial phases
with weights for ACORN-PHASE. If WTIN is not given, all weights default
to 1.0. If FCIN is given ACORN will calculate the weights from the
magnitudes.

Example: Use the experimental phases PHIB and FOM as the initial phase
set.

::

      LABI E=EO FP=FP SIFFP=SIGFP PHIN=PHIB WTIN=FOM
      LABI E=EO FP=FP SIFFP=SIGFP PHIN=PHIB FCIN=FCcal

ECUT <ecut>
~~~~~~~~~~~

(optional) - Default: <ecut> = 5.0

The observed reflections with E-values greater than <ecut> will be
rejected and treated as extended reflections. This can be used to
exclude outliers.

Example: ECUT 4.5

ESTRONG <estrong>
~~~~~~~~~~~~~~~~~

(optional - alternative to `**NSTRONG** <#nstr>`__) - Default: <estrong>
= value determined by ACORN.

The lower limit of E-values for the "strong" class of observed
reflections. These are used in the phase refinement, but ACORN will
calculate final phases and weights for all reflections and output them
to *foo\_out.mtz*.

Example: ESTRONG 1.0

NSTRONG <nstrong>
~~~~~~~~~~~~~~~~~

(optional - alternative to `**ESTRONG** <#estr>`__) - Default: use value
of <estrong> and derive <nstrong> as the number of observed reflections
this selects.

The number of strong observed reflections to be used. The program will
set <estrong> appropriately.

Example: Use 25000 strong reflections.

::

      NSTRONG 25000

EWEAK <eweak>
~~~~~~~~~~~~~

(optional - alternative to `**NWEAK** <#nwea>`__) - Default: <eweak> =
0.1

The upper limit of E-values for the "weak" class of observed
reflections. These are only used in the SER refinement of phases.

Example: The reflections with E-values less than 0.2 make up the "weak"
class of reflections.

::

      EWEAK 0.2

NWEAK <nweak>
~~~~~~~~~~~~~

(optional - alternative to `**EWEAK** <#ewea>`__) - Default: use EWEAK
0.1 and derive <nweak> as the number of observed reflections this
selects.

The number of weak reflections to be used. The program will set <eweak>
appropriately.

Example: Use 500 weak reflections.

::

      NWEAK 500

The "medium" class of reflections
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The medium observed reflections are all those with E-values between
<eweak> and <estrong>. These reflections are not used for phase
refinement and provide the cross validation set. If the CC for this
class increases to a reasonable value, it indicates a solution.

EXCLUDE <sigcut>
~~~~~~~~~~~~~~~~

(optional) - Default: <sigcut>=0.0. Use all the observed reflections.

The reflections with FP less than <sigcut>\*SIGFP will be rejected and
treated as the extended reflections.

Example: The reflections with FP less than 2.0\*SIGFP will be rejected.

::

      EXCL 2.0

RESOLUTION <res\_low> <res\_high>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: Use all observed reflections in the file.

The resolution range of the observed reflections to be used - resolution
limits are given in Å in either order.

Example: Use observed reflections from 20.0 to 1.5 Å.

::

      RESO 20.0 1.5
    or
      RESO 1.5 20.0

EXTEND <res\_low> <res\_high>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: Use all extended reflections.

Giving EXTE without other parameters: to use all extended reflections in
the file that is pre-provided by CCP4 program
`**UNIQUE** <unique.html>`__.

<res\_low> and <res\_hight> give the resolution range of the extended
reflections to be used - resolution limits are given in Å in either
order.

Example: Use extended reflections from 50.0 to 1.0 Å.

::

      EXTE 50.0 1.0
    or
      EXTE 1.0 50.0

NOEXTEND
~~~~~~~~

(optional) - Do not use any extended reflections.

Example:

::

      NOEXTE

Keywords to Select atoms for initial fragment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The coordinates given in XYZIN can be used in various ways; the default
option is to use them all as input for a MR search or for phase
refinement.

If no control keyword is given, the program defaults to doing phasing
refinement with all atoms, equivalent to `**NAFRAG ALL** <#nafr>`__ and
`**POSI 1** <#ranstart>`__.

It is also possible to use only some of them beginning from different
starting points in the file. This is controlled by combinations of the
keywords `**NAFRAG** <#nafr>`__, `**NSTART** <#nsta>`__ and
`**POSI** <#ranstart>`__.

NAFRAG <nafrag> [ALL]
~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: ALL *i.e.* use all atoms in XYZIN (assigned to
*foo\_in.pdb*)

Pick up <nafrag> atoms from *foo\_in.pdb*.

Example: The fragment uses 50 atoms starting from the first from
*foo\_in.pdb* file.

::

      NAFRAG 50 

NSTART <nstart>
~~~~~~~~~~~~~~~

(optional) - Default: <nstart> = 1

The fragment runs from the <nstart>th atom in *foo\_in.pdb* with a
length of <nafrag>.

Example: The fragment consists of atom 5 to 14 from *foo\_in.pdb*.

::

      NAFRAG 10
      NSTART  5

POSI [<nrand>]
~~~~~~~~~~~~~~

(optional) - Default: <nrand> = 1

<nrand> randomly selected starting points will be chosen from
*foo\_in.pdb* file, each with length <nafrag>. For example if there are
10 possible heavy atom sites in *foo\_in.pdb*, you can use the following
input to test all ten of the positions one by one.

Example: Use 10 sets of one atom fragments.

::

      NAFRAG 1
      POSI 10

RATM [<nratm>] [<percentage>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: <nratm> = 50000. <percentage> = 20%. No XYZIN
required.

This keyword can be used without assigning XYZIN. ACORN generates
<nratm> sets of single random atoms as starting fragments. The CC will
be calculated for [<percentage>]% of all reflections and sorted. Then
for the top 1000 sets the CC for all reflections is calculated and again
sorted. The phases from the top sets are then generated and refined. For
space group P1 two atoms are required; one at the origin and another
randomly placed one.

Example: Generate 5000 sets of single random atom fragments and test
them against 10% of reflections.

::

      RATM 5000 10.0

Restart the program using results from a previous run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

INITIAL <mesg> <numb1> [<numb2>] [<numb3>] [<numb4>] [<numb5>] [<numb6>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - No Defaults

The keyword allows the user to input some of the output from the log
file generated by a previous run of ACORN. All useful information is
flagged by the word INITIAL.

<mesg>
    indicates which method was used to provide the initial output. It
    must be one of POSI, RATM, RROT, ROTF, TRAN and RTRA.
<numb1>
    For POSI <numb1> defines the starting atom number in the XYZIN file.
    The *foo\_in.pdb* file should be the same as that in the previous
    run.
<numb1> <numb2> <numb3>
    For RROT, ROTF, TRAN and RTRA these define the Eulerian angles
    ALPHA, BETA and GAMMA.
    For RATM these are the fractional coordinates X\_f, Y\_f and Z\_f.
<numb1> <numb2> <numb3> <numb4> <numb5> <numb6>
    FOR TRAN and RTRA these define the Eulerian angles and the
    fractional translation. ALPHA, BETA and GAMMA Tx Ty Tz .

Default: Do not use the results from previous run of ACORN.

Examples:

::

      INITIAL TRAN 117.07  48.00 141.00  0.05  0.00  0.19  !! ALPHA BETA GAMMA Tx Ty Tz
    or
      INITIAL RATM 0.27304   0.09822   0.62551             !! X_f   Y_f  Z_f

Miscellaneous Rarely used Keywords:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

LABOUT <program label>=<file label>...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional)

This keyword allows the user to assign their own labels for the final
phase and weight written to the output file *foo\_out.mtz*. It is useful
if you want to restart ACORN to test a different starting model; the
output phases can be grouped in the same mtz file. All columns in the
input file *foo\_in.mtz* will be copied to the output file
*foo\_out.mtz*. The following <program label>s can be assigned:

::

      EOEXT PHIOUT WTOUT ECOUT
      EOEXT  E value for observed reflections plus extended reflections.
      PHIOUT The final phases from the last cycle of DDMK.
      WTOUT  The final weights from the last cycle of DDMK.
      ECOUT  Map coefficients from the last cycle of DDMK.

Example: Use labels PHIOUTse17 and WTOUTse17 instead of default PHIOUT
and WTOUT in *foo\_out.mtz*.

::

      LABO PHIOUT=PHIOUTse17 WTOUT=WTOUTse17

CONTENT <symbol1> <number> <symbol2> <number> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: ACORN estimates the numbers for elements C, N, O
and S assuming the molecule is a protein and the solvent content is 50%.

NB1: It is only used for the Sayre Refinement of phases.

NB2: It takes no account of anomalous signal.

For small molecules or if there is some unusual feature such as a Fe-S
cluster in a small protein it might be sensible to specify the contents
more precisely.

<symbol>
    element symbol.
<number>
    number of <symbol> in UNIT CELL.

Example: To use ACORN to solve rubredoxin.

::

      CONT C 256 N 61 O 88 S 5 FE 1 

TITLE <title>
~~~~~~~~~~~~~

(optional)

80 character title to replace old title in MTZ file.

GRID <grid>
~~~~~~~~~~~

(optional) - Default: <grid> = 0.3333 Å

A factor in Å controlling the sampling along cell edge for FFT.

Example: Use 0.5 Å grid for speed up ACORN process, but lose the
accuracy.

::

      GRID  0.5

SEED <iseed>
~~~~~~~~~~~~

(optional) - Default: <iseed> = 1

Integer seed for random number generater. This keyword allows the user
to obtain a different set of random numbers.

Example: To obtain second set of random numbers.

::

      SEED 2

END
~~~

of input. If present, this must be the last keyword.

Keywords for ACORN-MR to do fragment selection and molecular replacement searches:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**For each case ACORN tests many solutions by first calculating the CC
for a given percentage of all reflections, to a limited resolution.
These are sorted and for the top 1000 solutions, the CC is recalculated
for all reflections for the full resolution range. The best 100 of these
solutions are used as starting points for further refinement.**

    `**ROTF** <#rotf>`__, `**RROT** <#rrot>`__, `**RTRA** <#rtra>`__,
    `**TRAN** <#tran>`__, `**SOLUTION** <#solu>`__.

ROTF [<step>] [<percentage>] [<resoR>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: <step> = 3.0; <percentage> = 20%; <resoR> =
<res\_high>

The rotation function.

<step>
    The step size of rotation in degrees for Eulerian angles ALPHA, BETA
    and GAMMA.
<percentage>
    <percentage>% of reflections to be used in first stage of ROTF.
<resoR>
    The resolution limit for the first stage of ROTF.

In the first stage of ROTF ACORN will rotate the model <step> degrees at
a time and calculate the CC for each step for <percentage>% of the
reflections to a resolution of <resoR>. The CCs are then recalculated
with all data to the full resolution range for the best 1000 solutions.
These are again ranked according to CC and translation function searches
done for a chosen rotation solution specified by <nsrot> (see
`**TRAN** <#tran>`__). The default is to use the top solution only.

Example: Use 2.0 degrees rotation step with 100% reflections in 2.0 Å
resolution in first stage.

::

      ROTF 2.0 100.0 2.0

RROT [<nrot>] [<percentage>] [<resoR>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: <nrot> = 50000; <percentage> = 20%; <resor> =
<res\_high>

The random rotation function.

<nrot>
    The number of sets of random orientations.
<percentage>
    <percentage>% of reflections to be used in first stage of RROT.
<resoR>
    The resolution limit for the first stage of RROT.

ACORN will generate <nrot> sets of random orientations rather than doing
a systematic search.

Example: Generate 10000 random orientations and check the CC for 100% of
reflections to 2.0 Å resolution in first stage.

::

      RROT 10000 100.0 2.0

TRAN [<nsrot>]
~~~~~~~~~~~~~~

(optional) - Default: <nsrot> = 1

The translation function.

<nsrot>
    The solution number from the results of rotation function (maximum
    50).

ACORN will carry out translational searches for <nsrot> results of
rotation function using FFT techniques based on fitting between observed
and calculated intensities. Then CCs for structure factors are
calculated and the final solutions are sorted on CCs. The best 1000
solutions are saved and ACORN-PHASE will refine the phases according to
this order.

Example: Use 10 solutions from rotation function.

::

      TRAN 10

RTRA [<ntran>] [<percentage>] [<nsrot>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: <ntran> = 50000; <percentage> = 20%; <nsrot> = 1

The random translation function.

<ntran>
    The number of sets for random shifts on X, Y and Z.
<percentage>
    <percentage>% of reflections to be used in first stage of RTRA.
<nsrot>
    The solution No. from the results of rotation function.

ACORN will generate and rank <ntran> sets of random translation shifts
for <nsrot>th solution from the rotation function. ACORN-PHASE will
refine the phases according to the final order.

Example: Generate 5000 sets of random shifts using 10% reflections for
solution 2 from rotation function.

::

      RTRA 5000 10.0 2

SOLUTION <nset> <alpha> <beta> <gamma> [<X\_f>] [<Y\_f>] [<Z\_f>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional)

The keyword allows the user to input the results of AMoRe to generate a
starting fragment. The *foo\_in.pdb*, to which the rotation and
translations are applied must be the XYZOUT produced by the AMoRe
TABFUN.

**This is NOT recommended**. A better procedure is to generate the
output model from AMoRe or MOLREP, then input this as *foo\_in.pdb*,
using the ACORN-PHASE option NAFRAG ALL.

The lines starting with the word SOLUTION in the .log or .mr files can
be included in the ACORN script.

<nset>
    set number.
<alpha> <beta> <gamma>
    Eulerian angles of rotation function.
<X\_f> <Y\_f> <Z\_f>
    shifts from translation function in fractions of the observed unit
    cell edges.

Default: Do not use AMoRe result.

Example: Use one solution from AMoRe.

::

      SOLUTIONTF1_1   1  271.99   90.00   35.68  0.4500  0.0000  0.2794

Keywords for ACORN-PHASE:
~~~~~~~~~~~~~~~~~~~~~~~~~

    `**CUTDDM** <#cutd>`__, `**CCFINISH** <#ccfi>`__,
    `**PSFINISH** <#psfi>`__, `**MAXSET** <#maxs>`__,
    `**NTRY** <#ntry>`__, `**NCDDM** <#ncdd>`__, `**NCSER** <#ncse>`__,
    `**SUPP** <#supp>`__, `**ENHS** <#enhs>`__, `**RADIUS** <#radi>`__,
    `**SOLVENT** <#solv>`__, `**DDMK** <#ddmk>`__,
    `**CCDDM1** <#ccdd>`__.

CUTDDM <cutd>
~~~~~~~~~~~~~

(optional) - Default: <cutd> = 3.0

The factor to select the upper cut off value for density during the DDM
cycles.

The upper cut off is set to <ncycle>\*<cutd>\*<sigro> for first 5
cycles, where

<ncycle>
    is the cycle number of DDM.
<sigro>
    is the standard deviation of the density map.

and for subsequent cycles is held as 5\*<cutd>\*<sigro>. The final upper
cut off value will be not less than 0.1\*<nct>\*<romax> and not greater
than 0.8\*<romax> where <romax> is the maximum density in the map and
<nct> is the cycle number in each try.

When there are some atoms much heavier than others, and especially when
searching for sub-structures with a variety of atom types, it may be
necessary to increase <cutd>. However for structures where the atom
types are all more or less the same, the default value is appropriate.

Example: Set the upper cut off factor to 5.0.

::

      CUTD 5.0

CCFINISH <ccfinish>
~~~~~~~~~~~~~~~~~~~

(optional) - Default: <ccfinish> = value determined by the ACORN.

If CC is greater than <ccfinish> at the end of refinement pass, ACORN
will output the refined phases and weights for all the reflections, then
stop.

Example: ACORN will stop at CC greater than 0.25.

::

      CCFIN 0.25

PSFINISH <psfinish>
~~~~~~~~~~~~~~~~~~~

(optional) - Default: <psfinish> = 0.5 degrees

If the phase shift between two consecutive cycles of DDM is less than
<psfinish> the DDM try will finish and ACORN-PHASE will go to the next
step if any.

Example: Set the phase shift between two cycles to 0.8 degrees.

::

      PSFIN 0.8

MAXSET <maxset>
~~~~~~~~~~~~~~~

(optional) - Default: <maxset> = 100

The number of sets of initial phases for ACORN-PHASE to refine. The
maximum number is 1000.

Example: Refine 500 sets of initial phases.

::

      MAXSET 500

NTRY <ntry>
~~~~~~~~~~~

(optional) - Default: <ntry> = 10

The number of tries to cycle through DDM, ENHS and SER phase refinement
for each set of initial phases. The maximum number is 500. In each try
ACORN-PHASE will combine the procedures `**DDMK** <#ddmk>`__ and SER for
the number of cycles specified in `**NCDDM** <#ncdd>`__ and
`**NCSER** <#ncse>`__ or one cycle of enhancement `**ENHS** <#enhs>`__.

If the average phase shift between two cycles is less than <psfinish>
(see `**PSFINISH** <#psfi>`__) or if the CC is not increasing after 5
cycles the try will be terminated, and the procedure will go on to the
next try. ACORN will stop if the CC is greater than <ccfinish> (see
`**CCFINISH** <#ccfi>`__).

Example 1: Use 8 tries with default values for ENHS, NCSER and NCDDM.

::

      NTRY 8

which is the same as

::

      NTRY 8
      ENHS    0   0   0   0   1   1   1   1
      NCSER   0   2   2   2   0   0   0   0
      NCDDM 500 500 500 500 500 500 500 500

Example 2: If user wants to do 2 cycles of SER and 200 cycles of DDM for
the first try and one cycle of SER and 300 cycles of DDM for the other 9
tries. When SER is used in a try then ENHS will be set to 0 for this
try. Then

::

      NTRY 10
      NCSER   2   1   1   1   1   1   1   1   1   1
      NCDDM 200 300 300 300 300 300 300 300 300 300

NCSER <nc01> [<nc02>] ... [<nc500>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: <nc01> <nc02> ... <nc500> = 0 2 2 2 0 0 ... 0 if
the number of atoms in the fragment less then 50. Otherwise Sayre
Equation Refinement will not be used.

The maximum number of cycles in each try for Sayre Equation Refinement.

<nc01> <nc02> ... <nc500>
    number of cycles.

Example: Use 2 cycles of SER in first try and one cycle of SER for the
second and third try.

::

      NTRY 3
      NCSER 2 1 1

NCDDM <nc01> [<nc02>] ... [<nc500>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: <nc01> <nc02> ... <nc500> = 500 500 ... 500

The maximum number of cycles in each try for Dynamic Density
Modification.

<nc01> <nc02> ... <nc500>
    number of cycles.

Example: Use 50 cycles of DDM for the first three tries and 500 cycles
of DDM for the fourth try.

::

      NTRY 4
      NCDDM 50 50 50 500

SUPP <nsup>
~~~~~~~~~~~

(optional) - Default: ACORN determines if the Patterson superposition
function is used or not.

Use the Patterson superposition function to improve the initial phases
before phase refinement by SER and DDM.

User can set <nsup> = 0 not to use SER or <nsup> = 1 to use SER.

Example: Use the Patterson superposition function.

::

      SUPP 1

DDMK <nc01> [<nc02>] ... [<nc500>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: ACORN will use DDM0 first in each try. When CC is
greater than <ccddm1> DDM1 will be used. When CC is greater than
<ccddm2> DDM2 will be used. See `**CCDDM** <#ccdd>`__.

There are three kinds of map coefficients used in DDM procedure:

<nc01>=0 --- DDM0: Wt\*Eo for the observed reflections and Wt\*Ex for
the extended reflections.

<nc01>=1 --- DDM1: Wt\*(2\*Eo-Ec) for the observed reflections and
Wt\*(2\*Ex-Ec) for the extended reflections.

<nc01>=2 --- DDM2: 2\*M\*Eo-SigmaA\*Ec for the observed reflections and
2\*M\*Ex-SigmaA\*Ec for the extended reflections.

where Eo - the observed normalized structure factors. Ex - the extended
normalized structure factors (Ex=1.0). Ec - the calculated normalized
structure factors from a fragment or a modified map. M - a figure of
merit from SigmaA.

Example: Use DDM0 for the first 5 tries and DDM1 for try 6 to 9. DDM2
for try 10.

::

      NTRY 10
      DDMK 0 0 0 0 0 1 1 1 1 2

CCDDM <ccddm1> <ccddm2>
~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: <ccddm1> = 0.12, <ccddm2> = 0.20.

If CC is greater than <ccddm1> ACORN will use DDM1 automatically with
default `**DDMK** <#ddmk>`__. If CC is greater than <ccddm2> ACORN will
use DDM2 automatically with default `**DDMK** <#ddmk>`__.

Example: ACORN will use DDM1 at CC greater than 0.10 and DDM2 at CC
greater than 0.15 with default `**DDMK** <#ddmk>`__.

::

      CCDD 0.10 0.15

ENHS <nc01> [<nc02>] ... [<nc500>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(optional) - Default: <nc01> <nc02> ... <nc500> = 0 1 ... 1

<nc01>=1 means using weak density enhancement in cycle 1 for each try.
The enhancement will not be used if the Sayre Equation Refinement is
used.

Example: Use two cycles of SER for try 2 to 5 and ENHS for next 5 tries.

::

      NTRY 10
      ENHS  0 0 0 0 0 1 1 1 1 1
      NCSER 0 2 2 2 2 0 0 0 0 0

RADIUS <radius>
~~~~~~~~~~~~~~~

(optional) - Default: <radius> = 4.0 Å

The radius value used to calculate envelop.

Example: ACORN will use 5.0 Å of radius to calculate envelope.

::

      RADI 5.0

SOLVENT <solvent>
~~~~~~~~~~~~~~~~~

(optional) - Default: <solvent> = 0.5

The ratio of solvent region.

Example: ACORN will set 45% as solvent region to calculate envelope.

::

      SOLV 0.45

INPUT AND OUTPUT FILES
----------------------

The input files are the keyword file, a standard MTZ reflection data
file and a PDB file containing a fragment if needed.

Input:

    HKLIN
        input data file(MTZ).
    XYZIN
        input fragment file(PDB)

Output:

    HKLOUT
        output data file(MTZ).

Here are the definitions for the labels:

Name

Item

H, K, L

Miller indices.

 

E

E-values (normalized structure factors).

FP

Observed structure factors.

SIGFP

Sigma of FP.

PHIN

Known phases.

WTIN

Weights of known phases.

FCIN

Calculated magnitudes with the known phases.

 

EOEXT

E values for observed and extended reflections.

PHIOUT

New phases from last cycle of the best set.

WTOUT

Weights from last cycle of the best set.

PHIOUT

Map coefficients from last cycle of DDM0, DDM1 or DDM2.

PRINTER OUTPUT
--------------

The printed output starts with details from the input keyword data
lines. Then information from the input MTZ file follows. An error
message will be printed out if any illegal input in the keyword data
lines has been found and the program will stop.

EXAMPLES
--------

-  `**Example 1: Use a constellation of correctly placed atoms as the
   starting point.** <#example1>`__
-  `**Example 2: Use a constellation of correctly placed atoms as the
   starting point.** <#example2>`__
-  `**Example 3: Use a fragment already positioned by some other MR
   procedure (*e.g.* AMoRe).** <#example3>`__
-  `**Example 4: Test for the best result from 10 possible heavy
   atoms.** <#example4>`__
-  `**Example 5: Use a randomly placed single atom as a starting point
   for a complete structure solution.** <#example5>`__
-  `**Example 6: Determine the position of heavy atoms in a protein
   structure from the anomalous and isomorphous
   differences.** <#example6>`__
-  `**Example 7: Use a randomly placed single atom as a starting point
   to determine the position of the sub-structure from both the
   anomalous differences.** <#example7>`__
-  `**Example 8: Solve a small molecular structure.** <#example8>`__
-  `**Example 9: Use standard alpha helices as search models for a
   complete molecular replacment (MR) search.** <#example9>`__
-  `**Example 10: Use standard alpha helices as search models for random
   MR search.** <#example10>`__
-  `**Example 11: Use a fragment from a similar structure for MR
   search.** <#example11>`__
-  `**Example 12: Use externally determined phases and weights as a
   starting point.** <#example12>`__
-  `**Example 13: Restart using results from previous run of
   ACORN.** <#example13>`__
-  `**Example 14: Use the results from AMoRe given as Euler angles and
   translation components to position the model atoms
   correctly.** <#example14>`__
-  `**Example 15: Use all extended reflections in the file and start
   from a heavy atom.** <#example15>`__
-  `**Example 16: Use the extended reflections within 50.0 to 1.0 Å and
   start from molecular replacement model.** <#example16>`__
-  `**Example 17: Use the extended reflections within 50.0 to 1.0 Å and
   start from experimental phases.** <#example17>`__

Example 1:
~~~~~~~~~~

This example uses the two known Zn atom positions which are in the file
$HOME/test-zn.pdb to calculate the starting phases then refines them by
500 cycles of DDM (default for ACORN-PHASE): Since the Zn atoms are much
heavier than the protein atoms, the density CUToff is set higher than
the default.

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $HOME/test-zn.pdb \
    << eof

    !General keywords:
    TITLE   Start from input ZN position(s).
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    POSI 1
    CUTD 5.0

    END
    eof

Example 2:
~~~~~~~~~~

This example uses the two known S atom positions which are in the file
$HOME/sulphurs.pdb to calculate the starting phases then refines them by
500 cycles of DDM (default for ACORN-PHASE):

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $HOME/sulphurs.pdb \
    << eof

    !General keywords:
    TITLE   Start from input ZN position(s).
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    POSI 1

    END
    eof

Example 3:
~~~~~~~~~~

This example uses a fragment in the correct orientation and position
found by AMoRe to calculate a set of initial phases. The phases are
refined, first with Patterson superposition then with 3 tries each with
2 cycles of SER and 200 cycles of DDM:

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $HOME/solution-amore-pdbset.pdb \
    << eof

    !General keywords:
    TITLE   Start from a fragment positioned by AMoRe.
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    POSI 1
    !NAFRAG ALL

    !Keywords for ACORN-PHASE:
    SUPP 1
    NTRY 3
    NCSER 2 2 2
    NCDDM 200 200 200

    END
    eof

Example 4:
~~~~~~~~~~

This example calculates the CC of each atom from 10 possible Selenium
atoms and refines the best set of the phases, ie that with the largest
CC, using only DDM (default for ACORN-PHASE):

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $HOME/test-10Se.pdb \
    << eof

    !General keywords:
    TITLE   Start from the best Selenium atom from 10 positions.
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    NAFRAG 1
    POSI 10

    !Keywords for ACORN-PHASE:
    MAXSET 1

    END
    eof

Example 5:
~~~~~~~~~~

This example can be used to place a sub structure, or solve a
macro-molecular structure which contains calculates the CC for 5000
randomly positioned "single atom fragments" for 20% of reflections (the
default) and sorts them on CC. The top 1000 sets are selected to
re-calculate the full CC for all reflections which are again sorted.
Then the best MAXSET (default 100) phase sets are refined.

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    << eof

    TITLE   ACORN willl use 5000 sets of one random atom.
    LABI E=EO FP=FP SIGFP=SIGFP

    RATM 5000

    END
    eof

Example 6:
~~~~~~~~~~

This example is to find the Selenium atoms using the estimate of the
sub-structure amplitude generated from the MAD measurements by
`**REVISE** <revise.html>`__. The keyword CONTENT indicates that there
are 80 Selenium atoms in the **unit cell** and the resolution is cut to
use only reflections from 10.0 to 3.5 Å. The keywords NSTR and NWEA are
used to select 800 strong and 400 weak reflections. THE CC for 10000
sets of single randomly placed atom fragments are checked with 20% of
the reflections and the initial phases for the top 100 sets are refined.
There are 20 tries with the default NCSER and NCDDM:

::

    #!/bin/csh -f
    #  Use REVISE to generate FM
    #
    acorn \
    HKLIN $HOME/test-mad-revise.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    << eof

    !General keywords:
    TITLE   to determine Selenium atoms.
    LABI E=EM_RE FP=FM_RE SIGFP=SFM_RE 
    cont SE 80
    reso 10.0 3.5
    nstr 800
    nwea 400

    ratm 10000

    !  After the Random  atom search ACORN will refine the phases for the best 100 solutions 
    ! the default value for MAXSET is 100.
    end
    eof

Example 7:
~~~~~~~~~~

This example is to find the Selenium atoms using the estimate of the
sub-structure amplitude taken from the Peak anomalous difference. The
keyword CONTENT indicates that there are 80 Selenium atoms in the **unit
cell** and the resolution is cut to use only reflections from 10.0 to
3.5 Å. Anomalous differences greater than 30 ( These are on an arbitrary
scale and the cutoff value ischosen by looking at the loggraph analyses
from SCALEIT) and weak F+ and F- are excluded. The keywords NSTR and
NWEA are used to select 800 strong and 400 weak reflections. THE CC for
10000 sets of single randomly placed atom fragments are checked with 20%
of the reflections and the initial phases for the top 100 sets are
refined. There are 20 tries with the default NCSER and NCDDM:

::

    #!/bin/csh -f
    #
    ecalc \
    HKLIN $HOME/test-sad-data.mtz \
    HKLOUT $HOME/test-sad-E.mtz \
    << eof
    LABI FPH=F_peak(+) SIGFPH=SIGF_peak(+) FP=F_peak(-) SIGFP=SIGF_peak(-)
    LABO E=Esad
    EXCL SIGPH 3.0 SIGP 3 DIFF 30
    END
    eof
    #
    acorn \
    HKLIN  $HOME/test-sad-E.mtz \
    hklout $SCRATCH/test-acornsadE.mtz \
    << eof

    !General keywords:
    TITLE   to determine Selenium atoms.
    LABI E=Esad FP=F_peak SIGFP=SIGF_peak 
    cont SE 80
    reso 10.0 3.5
    nstr 800
    nwea 400

    !Keywords for ACORN-MR:
    ratm 10000

    !Keywords for ACORN-PHASE:
    !  The Random  atom search will refine the phases for the best 100 solutions 
    ! the default value for MAXSET is 100.
    CCFIN 0.1 !  The final correlation coefficient will be lower for SAD terms.
    end
    eof

Example 8:
~~~~~~~~~~

This example solves a small molecular structure. The keywords RESO and
GRID are needed for small molecule determination. The CC for 2000 sets
of single randomly placed atom fragments are checked with all the data
and the top 100 sets of the initial phases refined by 2 cycles of SER
and 300 cycles of DDM:

::

    #!/bin/csh -f
    #
    acorn \
    HKLIN $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    << eof

    !General keywords:
    TITLE   to solve a small molecular structure.
    LABI E=EO FP=FP SIGFP=SIGFP
    RESO 50.0 0.8
    GRID 0.3

    !Keywords for ACORN-MR:
    RATM 2000 100.0

    !Keywords for ACORN-PHASE:
    NCSER 2
    NCDDM 300

    end
    eof

Example 9:
~~~~~~~~~~

This example uses 50 atoms (10 residues) from the library idealised
alpha helix for a MR search with the default parameters. ACORN-PHASE
will refine the phases from the top 100 sets of positioned alpha helices
by DDM only:

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $CLIBD/fraglib/theor-helix-70.pdb \
    << eof

    !General keywords:
    TITLE   Start from standard alpha helices.
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    NAFRAG 50
    ROTF
    TRAN

    END
    eof

Example 10:
~~~~~~~~~~~

This example is the same as example 9, but uses random rotation and
random translation starting values to select the 100 most likely
positions for the alpha helix.

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $CLIBD/fraglib/theor-helix-70.pdb \
    << eof

    !General keywords:
    TITLE   Start from standard alpha helices.
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    NAFRAG 50
    RROT 
    RTRA 

    END
    eof

Example 11:
~~~~~~~~~~~

This example uses a suitable fragment for a MR search. The rotation
function is first calculated with a step size of 3.0 degrees for 20% of
the reflections to 2.0 Å resolution. The best orientation is selected,
and 50000 trials of random translational shifts are evaluated, again for
20% of the reflections to 1.0 Å resolution. ACORN-PHASE will refine the
top 100 solutions for the translation search by DDM(default for
ACORN-PHASE):

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $HOME/test-other-similar.pdb \
    << eof

    !General keywords:
    TITLE   Start from other similar fragment.
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    ROTF 3 20.0 2.0
    RTRA 50000 20.0 1

    END
    eof

Example 12:
~~~~~~~~~~~

This example uses known phases with input magnitudes and assign new
labels for the final output phases and weights. ACORN-PHASE will use 500
cycles of DDM for the first try then one cycles of ENH and 500 cycles of
DDM for the second try (default ENHS and NCDDM):

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    << eof

    !General keywords:
    TITLE   Start from input known phases and magnitudes.
    LABI E=EO FP=FP SIGFP=SIGFP PHIN=AC FCIN=FCcal
    LABO PHIOUT=NEWPHASE WTOUT=NEWT 

    !Keywords for ACORN-PHASE:
    NTRY 2
    ENHS 0 1 
    NCDDM 500 500
    END
    eof

Example 13:
~~~~~~~~~~~

This example takes 9 solutions from a previous run of ACORN-MR for
positoning standard alpha helices. The input rotation angles and
translational shifts are applied to the first 50 atoms of
$CLIBD/fraglib/theor-helix-70.pdb the phases generated from these
fragments are refined by DDM, the default:

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $CLIBD/fraglib/theor-helix-70.pdb \
    << eof

    !General keywords:
    TITLE   Use results from previous run of ACORN.
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    NAFRAG 50
     INITIAL TRAN   117.07    48.00   141.00  0.04537  0.00000  0.19480
     INITIAL TRAN   117.07    48.00   141.00  0.04331  0.00000  0.19480
     INITIAL TRAN   117.07    48.00   141.00  0.04537  0.00000  0.19784
     INITIAL TRAN   117.07    48.00   141.00  0.04331  0.00000  0.19784
     INITIAL TRAN   117.07    48.00   141.00  0.11136  0.00000  0.09740
     INITIAL TRAN   117.07    48.00   141.00  0.10311  0.00000  0.05174
     INITIAL TRAN   117.07    48.00   141.00  0.15467  0.00000  0.40177
     INITIAL TRAN   117.07    48.00   141.00  0.23716  0.00000  0.31959
     INITIAL TRAN   117.07    48.00   141.00  0.08043  0.00000  0.49308

    END
    eof

Example 14:
~~~~~~~~~~~

This example tests 7 solutions from AMoRe. AMoRe has been used with
input Es, and the TABLE structure factors have also been normalised (see
`AMoRe documentation <amore.html>`__). The input rotation angles and
translational shifts are applied to the XYZOUT PDB file generated in the
TABFUN stage by AMoRe and the resultant phases refined by DDM, the
default:

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $HOME/amore-TABFUN-trial.pdb \
    << eof

    !General keywords:
    TITLE   Use the solutions from AMoRe.
    LABI E=EO FP=FP SIGFP=SIGFP

    !Keywords for ACORN-MR:
    SOLUTIONTF1_4   1  272.09   90.00   35.82  0.0312  0.0000  0.0268
    SOLUTIONTF1_5   1  269.00   90.00  214.85  0.4688  0.0000  0.4732
    SOLUTIONTF1_2   1  271.38   74.93   28.08  0.4125  0.0000  0.4911
    SOLUTIONTF1_18  1  346.95   45.50  205.48  0.2000  0.0000  0.4464
    SOLUTIONTF1_1   1   57.94   82.77  157.51  0.2250  0.0000  0.4464
    SOLUTIONTF1_19  1  241.70   39.54   77.96  0.1125  0.0000  0.0446
    SOLUTIONTF1_14  1   60.65   12.39  109.53  0.4000  0.0000  0.4286

    END
    eof

Example 15:
~~~~~~~~~~~

This example uses the observed reflections within 9.0 to 1.55 Å plus all
the extended reflections in the file $HOME/test.mtz and the one known Zn
atom position which is in the file $HOME/test-zn.pdb to calculate the
starting phases then refines them by one cycle of SUPP and 10 tries with
SER from try 2 to 4 and ENHS for rest of tries. DDM1 will be used for
try 5 to 9 and DDM2 will be used for final try. Since the Zn atoms are
much heavier than the protein atoms, the density CUToff is set higher
than the default.

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $HOME/test-zn.pdb \
    << eof
                                                                                    
    !General keywords:
    TITLE   Start from input ZN position(s).
    LABI E=EO FP=FP SIGFP=SIGFP
                            
    !Keywords for select reflections:
    RESO 9.0 1.55
                               
    !Keywords for ACORN-MR:
    POSI 1

    !Keywords for ACORN-PHASE:
    CUTD 5.0
    SUPP 1
    NTRY 10
    NCSER 0 2 2 2
    DDMK 0 0 0 0 1 1 1 1 1 2
                                                                                    
    END
    eof

Example 16:
~~~~~~~~~~~

This example uses the observed strong E>0.8 within 20.0 to 1.65 Å plus
the extended reflections wihtin 50.0 to 1.0 Å in the file $HOME/test.mtz
and the MR model from MOLREP which is in the file $HOME/test-molrep.pdb
to calculate the starting phases then refines them by 150 tries with
default ENHS and DDMK values. Set 45% of the solvent region by SOLV
0.45.

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    xyzin $HOME/test-molrep.pdb \
    << eof
                                                                                    
    !General keywords:
    TITLE   Start from input MOLREP model.
    LABI E=EO FP=FP SIGFP=SIGFP
                            
    !Keywords for select reflections:
    RESO 20.0 1.65
    EXTEND 50.0 1.0
    ESTRO 0.8 
                               
    !Keywords for ACORN-MR:
    POSI 1

    !Keywords for ACORN-PHASE:
    SOLV 0.45
    NTRY 150
                                                                                    
    END
    eof

Example 17:
~~~~~~~~~~~

This example uses the observed strong E>0.8 within 10.0 to 1.50 Å plus
the extended reflections wihtin 50.0 to 1.0 Å and the experimental
phases from SAD data in the file $HOME/test.mtz and then refines them by
default values. Set 45% of the solvent region by SOLV 0.45.

::

    #!/bin/csh -f
    #
    acorn \
    hklin $HOME/test.mtz \
    hklout $SCRATCH/test-acorn.mtz \
    << eof
                                                                                    
    !General keywords:
    TITLE   Start from input experiment phases with weights.
    LABI E=EO FP=FP SIGFP=SIGFP PHIN=PHI_Se16 WTIN=WT_Se16
                            
    !Keywords for select reflections:
    RESO 10.0 1.50
    EXTEND 50.0 1.0
    ESTRO 0.8 
                               
    !Keywords for ACORN-PHASE:
    SOLV 0.45

    END
    eof

AUTHOR
------

| Yao Jia-xing
| Email: yao@ysbl.york.ac.uk

REFERENCES
----------

#. Foadi,J., Woolfson,M.M., Dodson,E.J., Wilson,K.S., Yao Jia-xing and
   Zheng Chao-de (2000) *Acta. Cryst.* **D56**, 1137-1147.
#. Yao Jia-xing (2002) *Acta. Cryst.* **D58**, 1941-1947.
#. Yao Jia-xing, Woolfson,M.M., Wilson,K.S. and Dodson,E.J. (2002) *Z.
   Kristallogr.* **217**, 636-643.
#. Yao Jia-xing, Woolfson,M.M., Wilson,K.S. and Dodson,E.J. (2005)
   *Acta. Cryst.* **D61**, 1465-1475.

SEE ALSO
--------

    `**AMoRe** <amore.html>`__ `**DM** <dm.html>`__
    `**PDBSET** <pdbset.html>`__ `**UNIQUE** <unique.html>`__
