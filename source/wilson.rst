WILSON (CCP4: Supported Program)
================================

NAME
----

**wilson** - Wilson plot, absolute scale and temperature factor

SYNOPSIS
--------

| **wilson hklin** *input.mtz*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

This program reads a file of amplitudes (or of intensities), and
performs a Wilson plot of the observed intensities against resolution to
determine an absolute scale and temperature factor.

If the atoms are randomly distributed in the cell, then

::

      <f**2> = Scale * <Fobs**2> * exp (-2B (sin theta/lambda)**2)

where <f\*\*2> is the mean squared atomic form factor summed over all
atoms in the unit cell. Thus a plot of ln(<f\*\*2>/<Fobs\*\*2>) against
2(sin theta/lambda)\*\*2 will have slope -B and an intercept Scale. The
program derives Scale and B by a least squares fit.

There may be a problem in evaluating <Fobs\*\*2> if all the weak data
have been systematically omitted (which should NOT be the case for data
measured in any proper manner). If weak reflections are missing from the
data, use the WILSON ALL option. The program then estimates the expected
number of reflections in each resolution shell and then calculates
<Fobs\*\*2> by dividing by the number of predicted reflections. The
program also produces a log graph of the numbers of observed and missing
reflections against resolution, and will work with either amplitudes or
with intensities directly depending upon which labels are specified in
the `LABIN <#labin>`__ keyword.

Note that the scale determined by this procedure is very approximate,
unless very high resolution data are available. See also the
determination done by \`\ ``dm``'.

KEYWORDED INPUT
---------------

The available keywords are:

    `**CONTENTS** <#contents>`__, `**END** <#end>`__,
    `**HEADER** <#header>`__, `**LABIN** <#labin>`__,
    `**NRESIDUE** <#nresidue>`__, `**RANGES** <#ranges>`__,
    `**RESOLUTION** <#resolution>`__, `**RSCALE** <#rscale>`__,
    `**SYMMETRY** <#symmetry>`__, `**TITLE** <#title>`__,
    `**VPAT** <#vpat>`__, `**WILSON** <#wilson>`__.

*N.B.*, one of CONTENTS or NRESIDUES is compulsory.

TITLE <title>
~~~~~~~~~~~~~

Specify the TITLE to write to the log file.

RANGES <Nbin> \| <Range>
~~~~~~~~~~~~~~~~~~~~~~~~

<Nbin> is number of resolution bins over the resolution range of the
Wilson Plot. <Range> is the width of ranges on 4sin\*\*2
theta/lambda\*\*2 and is an alternative to <Nbin>. The resolution range
of the Wilson PLot is defined by `RSCALE <#rscale>`__. The default if
this card is absent is <Nbin>=30.

RESOLUTION <Dmin> <Dmax>
~~~~~~~~~~~~~~~~~~~~~~~~

Resolution limits, either 4(sin theta/lambda)\*\*2 or d in Angstrom
(either order). Reflections outside these limits will be excluded from
all analysis and omitted on output. Defaults are taken from the range of
data in the input file (*i.e.* all data are included).

RSCALE <Dmin> <Dmax>
~~~~~~~~~~~~~~~~~~~~

Resolution limits for scaling, specified as for
`RESOLUTION <#resolution>`__. This option allows you to exclude low
resolution reflections from the Wilson plot: it is probably a good idea
to include only high resolution data (beyond 3Å, if you have any data
there) in the Wilson plot. This is because the assumptions made in
determining the Wilson statistics are not valid for the low resolution
reflections. The default for the high resolution is the same as that for
the RESOLUTION card. The default low resolution is 4.0Å if the high
resolution limit is less than 3.5Å.

WILSON OBSERVED \| ALL
~~~~~~~~~~~~~~~~~~~~~~

    OBSERVED
        use observed reflection count to determine average intensity
        (default). This is the normal option.
    ALL
        use total number of possible reflections to determine average
        intensity (*i.e.* assume missing data are all zero). THIS IS NOT
        NORMALLY CORRECT.

CONTENTS <element> <natoms> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<element> is an element name (including hydrogen) for which there are
<atoms> atoms in the asymmetric unit, *e.g.*

::

      CONTENTS  H 746 C  454  N 115    O 139   S 12  ! Must include hydrogens

NOTE the program `RWCONTENTS <rwcontents.html>`__ reads a PDB file and
finds out how many Carbons, *etc*. there are in the file, and gives an
estimate of the number of hydrogens.

NRESIDUE <Nres>
~~~~~~~~~~~~~~~

<Nres> is the number of residues expected in the asymmetric unit. A very
approximate atom composition is calculated using:

::

       mean mass of an amino acid = 110
       add on one ordered water per amino acid = ca. 128

This is then taken as 5 C + 1.35 N + 1.5 O + 8 H /residue as number of
atoms in asymmetric unit.

VPAT <vpat>
~~~~~~~~~~~

<vpat> is the volume per atom, default 10.

HEADER [NONE \| BRIEF \| HISTORY \| ALL] [NOBATCH \| BATCH \| ORIENTATION]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Controls printout from reading file and batch headers.

a. file header printing:

       NONE
           no header printed
       BRIEF
           brief header (default)
       HISTORY
           brief + history
       ALL
           full header printed

b. batch header printing:

       NOBATCH
           no batch header printed
       BATCH
           batch titles printed (default)
       ORIENTATION
           batch orientation data also printed

SYMMETRY <space group>
~~~~~~~~~~~~~~~~~~~~~~

The default is to use the symmetry from the input HKLIN file. Normally
OMIT this command.

LABIN <program label>=<file label> .....
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Column assignments for the program columns.

    FP
        amplitude
    SIGFP
        standard deviation of amplitude
    IP
        intensity
    SIGIP
        standard deviation of intensity

Note that the FP/IP labels are mutually exclusive - you specify if the
program uses either amplitudes or intensities, but not both.

END
~~~

Terminate input

SEE ALSO
--------

`dm <dm.html>`__, rwcontents (1), `xloggraph <xloggraph.html>`__.

AUTHORS
-------

Eleanor Dodson, Phil Evans

EXAMPLE
-------

::

    wilson  hklin u1afobs  <<EOF-wil
    title U1A native, Weis (3/91), 2.5A, pass 3.2
    wilson
    resolution  20 2.4
    rscale      4.5 2.4
    nresidues   190
    labin  FP=FP SIGFP=SIGFP
    EOF-wil
