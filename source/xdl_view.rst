XDL\_VIEW (CCP4: Library)
=========================

NAME
----

**xdl\_view** -XDL\_VIEW version 4.3 X-windows based tootkit

SYNOPSIS
--------

**xdl\_view**

DESCRIPTION
-----------

XDL\_VIEW is a set of 'view-object' routines, developed originally for
use by the SERC Daresbury Laboratory Protein Crystallography project
team for the development of the Laue software and for potential use with
a much wider range of applications. These routines are designed to be
used by applications to provide a user interface within an X-windows
environment.

XDL\_VIEW is used by the CCP4 programs X-windows programs
(`HKLVIEW <hklview.html>`__,\ `IPDISP <ipdisp.html>`__,
`XDLDATAMAN <xdldataman.html>`__, `XDLMAPMAN <xdlmapman.html>`__,
`XLOGGRAPH <xloggraph.html>`__ and
`XPLOT84DRIVER <xplot84driver.html>`__) as well as
`MOSFLM <mosflm.html>`__ and `ROTGEN <rotgen.html>`__.

Full documentation for the XDL\_VIEW routines can be found in `the
xdl\_view 4.0 manual <../x-windows/xdl_view/doc/xdl_view_top.html>`__

AUTHORS
-------

::

    John W. Campbell and Dave Love < d.love@dl.ac.uk >
    CCLRC Daresbury Laboratory
