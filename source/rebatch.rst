REBATCH (CCP4: Supported Program)
=================================

NAME
----

**rebatch** - alter batch numbers and other batch information in an
unmerged MTZ file (saves re-running Mosflm).

SYNOPSIS
--------

| **rebatch HKLIN** *foo\_in.mtz* **HKLOUT** *foo\_out.mtz*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

A program to alter batch numbers in an unmerged MTZ file. This may be
necessary to give different batches number to all your MTZ files (saves
running ``Mosflm`` again).

If datasets are defined in the header, or if BATCH PNAME & XNAME & DNAME
commands are given, then the dataset cell dimension and wavelength
properties will be checked and reset if necessary. This allows the file
to be fixed up if these properties have not been properly set by e.g.
``Mosflm``. Priority for setting the dataset cell & wavelength is:-

#. Values set explicitly with CELL or WAVELENGTH commands
#. Values present in input file as dataset properties
#. Values from the batch headers: these are checked for consistency
   within batches belonging to each dataset, and warnings are printed if
   inconsistency is found.

The program will also patch the detector limits in the batch headers.
These are needed for the detector scaling options in
`Scala <scala.html>`__.

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords. Only the
first 4 letters of each keyword are necessary. Most keywords are
optional.

`**BATCH** <#batch>`__, `**TITLE** <#title>`__,
`**HISTORY** <#history>`__, `**DETECTOR** <#detector>`__,
`**CELL** <#cell>`__, `**WAVELENGTH** <#wavelength>`__,
`**END** <#end>`__

 BATCH <batch\_range \| batch\_list \| ALL> ADD <nadd> \| START <nstart> \| REJECT \| INCLUDE \| MAXWIDTH \| PNAME <projectname> XNAME <crystalname> DNAME <datasetname>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Define batch number editing for either a range of batches (b1 TO b2), a
list of batches ( b1 b2 b3 ...), or ALL [default].

Editing options are:

     ADD <nadd>
        Add <nadd> to the batch number for all batches specified. Note
        that <nadd> may be negative, but the resulting batch numbers
        must all be .gt. 0
     START <nstart>
        Renumber all batches matching the specification serially from
        <nstart>, e.g. if the file contains batches 100, 101, 102, 110,
        111, then the command BATCH 1 TO 200 START 1000 will renumber
        them to 1000, 1001, 1002, 1003, 1004
     REJECT
        Reject these batches.
     INCLUDE
        Include these batches: the first INCLUDE command will reject all
        other batches.
     MAXWIDTH
        Rejects reflections having width larger than that specified for
        each input batch
     PNAME <projectname> XNAME <crystalname> DNAME <datasetname>
        Set or reset project/crystal/dataset names for these batches.
        PROJECT, CRYSTAL and DATASET can be used as alternatives to
        PNAME, XNAME and DNAME.

Only one editing option can be given for each BATCH command (apart from
PNAME, XNAME and DNAME, which should be given together), but a series of
BATCH commands may be given, and have a cumulative effect.

 TITLE <title>
~~~~~~~~~~~~~~

New title for file HKLIN, to be put in main MTZ header.

 HISTORY <history>
~~~~~~~~~~~~~~~~~~

New history record for file HKLIN.

 DETECTOR <xmin> <xmax> <ymin> <ymax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Reset limits of detector coordinates XDET, YDET for all batches. These
are needed for the detector scaling options in `Scala <scala.html>`__.

 CELL <a> <b> <c> <alpha> <beta> <gamma>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Reset cell dimensions for the file header and all datasets. Batch cell
dimensions are not altered.

 WAVELENGTH <lambda>
~~~~~~~~~~~~~~~~~~~~

Reset wavelength for all datasets. Batch wavelengths are not altered.

 END
~~~~

End keyworded input. EOF will also work.

SEE ALSO
--------

`scala <scala.html>`__

EXAMPLES
--------

A unix example file is available on $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`rebatch <../examples/unix/runnable/rebatch.exam>`__
