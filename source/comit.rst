COMIT (CCP4: Supported Program)
===============================

NAME
----

**comit** - Composite omit maps

SYNOPSIS
--------

| **ccomit** **-pdbin** *filename* **-mtzin** *filename* **-mtzout**
  *filename* **-mapout** *filename* **-prefix** *filename* **-colin-fo**
  *colpath* **-colin-fc** *colpath* **-colout** *colpath* **-nomit**
  *number of spheres* **-pad-radius** *radius/A* **-verbose**
  *verbosity* **-stdin**
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

'comit' Calculates composite omit maps by omitting spheres of density
from the assymetric unit and reconsructing those spheres using sigma-a
weighted maps.

'comit' can use one of two methods. A fast calculation using the output
from refmac can generate a very good approximation to the full composite
omit map with the correct choice of input MTZ columns. A slow
calculation involves re-running refmac on mutliple versions of the model
with a few atom occupancies set to zero in each case. In slow mode the
program is run twice, once to generate the input models for refmac, and
once to assemple the output maps from refmac.

HOW TO RUN COMIT
----------------

Examples
~~~~~~~~

Fast method:

    ::

        comit --mtzin my.mtz --colin-fo FP,SIGFP --colin-fc FC_ALL,PHIC_ALL --mtzout omit.mtz

Slow method:

    ::

        ~/clipper21/brigantine/comit --pdbin my.pdb --prefix omit

        foreach name ( omit_0*.pdb )

        refmac5 XYZIN $name XYZOUT refmac.pdb HKLIN /home/cowtan/test/ejd/gg_scaleit2.mtz HKLOUT $name:r.mtz << eof
        refi type REST resi MLKF meth CGMAT bref ISOT
        ncyc 10
        solvent YES
        weight AUTO
        labin  FP=FP SIGFP=SIGFP FREE=FreeR_flag
        eof

        end

        ~/clipper21/brigantine/comit --prefix omit --mtzout omit.mtz

INPUT/OUTPUT FILES
------------------

Note: Fast or slow mode are determined by the absence or presence of the
'prefix' keyword. In slow mode, the first and second runs are determined
by the presence or absence of the 'pdbin' keyword.

Fast mode
~~~~~~~~~

-mtzin
    MTZ file from refmac.
-mtzout
    [Optional] MTZ file with added omit map coefficients.
-mapout
    [Optional] Omit map.

Slow mode
~~~~~~~~~

-pdbin
    Atomic model. A series of models will be created for refinement in
    refmac with some atom occupancies set to zero.
-prefix
    Prefix for output models (pre-refmac) or input mtz files
    (post-refmac).
-mtzout
    [Optional] MTZ file with added omit map coefficients.
-mapout
    [Optional] Omit map.

KEYWORDED INPUT
---------------

See `Note on keyword input <#notekeywords>`__.

    .. rubric:: -colin-fo *colpath*
       :name: colin-fo-colpath

    [Fast mode] Observed F and sigma for work structure. See `Note on
    column paths <#notecolpaths>`__.

    .. rubric:: -colin-fc *colpath*
       :name: colin-fc-colpath

    [Fast mode] Map coefficients for work structure. These should
    normally be set to FC\_ALL,PHICALL after refmac. Use of FWT,PHWT is
    generally inadvisable, leading to a much more biased map, however
    for very poor models this may be better than nothing. See `Note on
    column paths <#notecolpaths>`__.

    .. rubric:: -nomit *number of spheres*
       :name: nomit-number-of-spheres

    Approximate number of regions to omit. Default = 20

    .. rubric:: -pad-radius *radius/A*
       :name: pad-radius-radiusa

    Padding radius to separate included and excluded density. Default =
    3A

    .. rubric:: -verbose *verbosity*
       :name: verbose-verbosity

Note on column paths:
^^^^^^^^^^^^^^^^^^^^^

| When using the command line, MTZ columns are described as groups using
  a comma separated format. A more specific form includes the crystal
  and dataset name sparated by slashes, and enclosing any column labels
  in square brackets. If your data was generated by another column-group
  using program, you can just specify the name of the group. e.g.
| `` FP,SIGFP /*/*/[FP,SIGFP] /native/peak/Fobs`` (The last case expands
  to two column names: Fobs.F\_phi.F and Fobs.F\_phi.phi)

Note on keyword input:
^^^^^^^^^^^^^^^^^^^^^^

Keywords may appear on the command line, or by specifying the '-stdin'
flag, on standard input. In the latter case, one keyword is given per
line and the '-' is optional, and the rest of the line is the argument
of that keyword if required, so quoting is not used in this case.

Reading the Ouput:
------------------

Problems:
---------

AUTHOR
------

Kevin Cowtan, York.

SEE ALSO
--------


