AMORE (CCP4: Supported Program)
===============================

NAME
----

| **AMoRe** - Jorge Navaza's state-of-the-art molecular replacement
  package, updated February 1999.
| *The SORTFUN and TABFUN output are NOT compatible with the old
  version.*
| New keyword `CRYSTAL <#tabfun_crystal>`__ for TABFUN.
| [`Keyworded input <#keywords>`__]

CONTENTS
--------

-  `DESCRIPTION <#description>`__
-  `Further details of the PROGRAM FUNCTIONS <#function>`__
-  `LIKELY PROBLEMS <#likely_problems>`__
-  `KEYWORDED INPUT <#keywords>`__

   -  `SORTFUN keywords <#keywords_sortfun>`__
   -  `TABFUN keywords <#keywords_tabfun>`__
   -  `ROTFUN Keywords <#keywords_rotfun>`__
   -  `TRAFUN keywords <#keywords_trafun>`__
   -  `FITFUN keywords <#keywords_fitfun>`__
   -  `REORIENTATE keywords <#keywords_reorientate>`__

-  `NOTES <#notes>`__

   -  `Memory allocation <#memory_allocation>`__
   -  `Rotation matrix definitions <#rotation_matrix_definitions>`__
   -  `Orthogonalisation codes <#orthogonalisation_codes>`__

-  `EXAMPLES <#examples>`__

   #. `The command scripts for the complete procedure to find 3
      molecules for spmi (usually run from CCP4i). <#example1>`__

      a. `TABFUN for MODEL FRAGMENT in large "unit cell" <#example1a>`__
      b. `SORTFUN where MTZ file contains cell and
         symmetry <#example1b>`__
      c. `ROTFUN - straightforward case <#example1c>`__
      d. `TRAFUN one (first) molecule in P61 <#example1d>`__
      e. `TRAFUN one (first) molecule in P65 - same rotation solutions
         as 1d <#example1e>`__
      f. `TRAFUN search for second molecule in P61 <#example1f>`__
      g. `TRAFUN search for second molecule in P65 <#example1g>`__
      h. `TRAFUN search for third molecule in P65 - higher correlation
         and lower Rfactor than P61 <#example1h>`__
      i. `FITFUN - three molecules in P65 <#example1i>`__
      j. `Build the solution file with PDBSET, checking symmetry clashes
         with DISTANG <#example1j>`__

   #. `Tabulating structure factors generated from a blob of electron
      density into a large "P1 unit cell" to give a finely sampled
      reciprocal lattice <#example2>`__

      -  This uses NCSMASK, MAPMASK, MAPROT, SFALL

   #. `Using the locked rotation function <#example3>`__
   #. `Using a non-crystallographic translation vector to find pairs of
      solutions in the same orientation <#example4>`__

-  `AUTHORS <#authors>`__
-  `REFERENCES <#references>`__
-  `SEE ALSO <#see_also>`__

DESCRIPTION
-----------

AMoRe includes routines to run a complete molecular replacement.

As well as carrying out ROTATION and TRANSLATION searches against
various targets, and doing RIGID BODY REFINEMENT, there are routines to
reformat the observed data from the new crystal form, and to generate
and tabulate structure factors from the model in a large P1 cell. See
`reference [1] <#reference1>`__.

The steps are usually carried out in the following order:

#. The observed data is extended to cover a hemisphere of reciprocal
   space and reformatted.
#. Structure factors for the model are tabulated on a fine grid
   (corresponding to a large "unit cell"). This is the key to the
   program's speed. All subsequent structure factors required for the
   searches are obtained by interpolating into this table. The structure
   factors can be calculated within AMoRe from a set of coordinates,
   using the option TABFUN, or generated outside the program and read in
   using the option SORTFUN.
#. The rotation function is run searching for Patterson correlation
   within a sphere centred on the origin. This allows the Patterson to
   be expressed in terms of spherical harmonics, and the calculation to
   exploit FFT techniques. Two different types of indicators of a good
   solution are given (see also `below <#solution_criteria>`__):

   a. The correlation between the observed and model pattersons;
   b. Correlation coefficients and Rfactors between the observed Fs or
      Is and generated Fs or Is from a model with the given orientation.

AMoRe requires a LOT of memory and this may cause problems on some
machines. However this new release is considerably less demanding than
the older one (see `Memory allocation <#memory_allocation>`__).

Further details of the PROGRAM FUNCTIONS
----------------------------------------

Step\_1 SORTFUN - reading, extending, sorting and reformatting a list of reflections
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input:
    **HKLIN**
    Standard MTZ file (maybe observed data, structure factors generated
    by some technique, E values from ECALC, *etc.*)
Optional Input:
    **Memory allocation parameters**
    SORTING\_NR (default estimated from the resolution and cell
    dimensions). This can be reset if necessary.
Output:
    **Either HKLPCK0** (see `option 1 <#sortfun_option1>`__)
    Packed file of H K L [F SIGF] or [FOM\*F PHI] NMULT in hemisphere:
    h,+-k,+-l. This is a binary file which also holds the unit cell,
    symmetry operators, and maximum h, k,l and resolution (see `example
    [1b] <#example1b>`__).
    \ **or table<i>** (see `option 2 <#sortfun_option2>`__)
    Table of the finely sampled inverse Fourier coefficients (*i.e.*,
    structure factors which have been read in from a previously prepared
    MTZ file). These must extend a little past the required resolution
    of the calculations to allow for interpolation. This is a binary
    file which also holds the large "unit cell", maximum h, k, l, and
    resolution (see `example [2] <#example2>`__).

Option 1:

-  Packs and sorts H K L [Fobs SigFobs] or [W\*Fobs PhiObs] to an
   internal form for use in later steps.

Option 2:

-  Packs an input list of H K L FC PHIC for use as a table. This format
   is described `below <#function_tabfun>`__. This gives the user great
   flexibility to try different types of search models. For example,
   structure factors can be generated from modified electron density
   maps, or calculated structure factors can be converted to E values
   (see `example [2] <#example2>`__).

Step\_2 TABFUN - reading model coordinates, repositioning them and generating structure factors from them
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input:
    **XYZIN<i>**
    Standard PDB file for the model
Optional Input:
    **Memory allocation parameters**
    TABLING\_MI, TABLING\_MR, TABLING\_MC
    (defaults estimated from the model dimensions and the sampling
    required). They may need to reset to the values specified in the log
    file.
Output:
    **XYZOUT<i>**
    Coordinates after repositioning
Output:
    **table<i>**
    Table of the finely sampled structure factors generated from the
    shifted model, and calculated for a large "unit cell".
Output:
    **Log File**
    Contains vital information about the coordinates which will be used
    at later stages of the procedure (*e.g.* Minimal Box, Centre of
    Mass, Rotation, Maximal distance from Centre of Mass)
Optional Output:
    **HKLOUT<i>**
    This is rarely used, but can be useful for checking purposes. ASCII
    file of finely sampled inverse Fourier coefficients as H K L FC PHIC
    (*i.e.* structure factors)

The procedure is:

#. The model coordinates are translated so that their centre of gravity
   is at the origin. They can then be rotated so that the principal axes
   of inertia of the model are parallel to the a, b and c axes of the
   "minimal box" which just contains the model. The dimensions of the
   "minimal box" are determined, and the "maximum distance" of any
   coordinate from the centre of mass.
   You may choose not to ROTATE the model; in some cases results may
   then be simpler to interpret. For instance if you want to compare
   results from several models it is convenient to allow the first model
   to ROTATE, then to fit all others to these repositioned coordinates
   which will have been output to the assigned XYZOUT. It may also be
   useful if you expect some predictable result; *e.g.* that the new
   crystallographic symmetry axes should map onto those of the model
   structure.
   Hint: It can help to understand results if some "pseudo" atoms are
   added to the model PDB file. For example if you have a two fold axis
   in the original structure put 2 coordinates on this axis. If the
   model forms a tetramer centred at (Xt,Yt,Zt) include this coordinate
   plus 3 which lie on the tetramer axes.
#. Structure factors are generated from the modified coordinates for a
   "CELL" with dimensions SCALE\*minimal\_a, SCALE\*minimal\_b,
   SCALE\*minimal\_c and all angles = 90 . SCALE has the default value
   of 4, but can be reset by the `SAMPLE <#tabfun_sample>`__ keyword.
   All later structure factors and gradients for the model in its
   various orientations are interpolated from this data.

   ::

          Expected Error in R factor with SCALE = 4  -  3 %
          Expected Error in R factor with SCALE = 3  -  9 %
          Expected Error in R factor with SCALE = 2  - 17 %

   You may need to generate tables for several models, *e.g.* for
   different domains. Up to four different table<i> files can be
   assigned during the translation search, and for rigid body
   refinement.

Step\_3 ROTFUN
~~~~~~~~~~~~~~

Runs the rotation function. Does the following four stages (they can be
run separately but I can't think why..).

Step\_3a GENERATE\_Stage
~~~~~~~~~~~~~~~~~~~~~~~~

Keyword: `GENERATE <#rotfun_generate>`__ - calculates structure factors
for model in a suitable cell, and packs them in the same format as the
output of SORTFUN.

Input:
    **table<i>**
    See `above <#table>`__
Optional Input:
    **Memory allocation parameters**
    ROTING\_MI, ROTING\_MR, ROTING\_MC, ROTING\_MD
    (defaults estimated from crystal cell, and the dimensions of the
    model table). These can be reset if necessary.
Output:
    **HKLPCK1**

Step\_3b CALCULATE\_Spherical\_HARMONICS\_Stage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Keyword: `CLMN <#rotfun_clmn>`__ - calculates spherical harmonics for
crystal and models.

Input:
    **HKLPCK<i>** (HKLPCK0 for crystal, HKLPCK1 for model)
Output:
    **CLMN<i>**

Step\_3c ROTATION\_Stage
~~~~~~~~~~~~~~~~~~~~~~~~

Keyword: `ROTATE <#rotfun_rotate>`__ - calculates rotation function and
finds many possible solutions by Patterson overlap.

Input:
    **CLMN<i>** (CLMN0 for crystal, CLMN1 for model)
Output:
    **SOLUTIONRC**
    For the ***CROSS*** rotation function the output rotational
    solutions are given in terms of the Eulerian angles, alpha, beta and
    gamma with each line flagged: SOLUTIONRC.
    The Eulerian angles use the convention described by Tony Crowther
    which is used in all CCP4 programs, *e.g.* ALMN, LSQKAB, PDBSET and
    DM. They define a rotation matrix which moves the model molecule
    into the proper orientation for the new crystal form. The model is
    first rotated through gamma about Zo, then through beta about the
    new Yo, then through alpha about the new Zo. Positive rotation is
    clockwise when looking along the axis from the origin. See elsewhere
    for details of the `definitions of the rotation
    matrix <#rotation_matrix_definitions>`__ and the `orthogonalisation
    conventions <#orthogonalisation_codes>`__ which define Zo Yo and Xo.
    Four solution criteria are tabulated:

    -  CC\_F is the correlation coefficient between the observed
       amplitudes for the crystal and the calculated amplitudes for the
       model. It is surprising that this is a satisfactory target, since
       the model amplitudes are generated in a P1 cell, but it does seem
       to be the most effective discriminator. It is sensible to sort
       the solutions on this target.
    -  RF\_F is the classic R factor between the observed amplitudes for
       the crystal and the calculated amplitudes for the model. Again,
       it is surprising that this is a satisfactory target, since the
       model amplitudes are generated in a P1 cell, but it does seem to
       be reasonable, although the CC\_F is probably better.
    -  CC\_I is the correlation coefficient between the observed
       intensities for the crystal and the sum of calculated intensities
       for all symmetry equivalents of the model, *i.e.* the intensities
       are summed, but without any correction for the relative
       positioning of the symmetry related molecules.
    -  CC\_P is the Patterson correlation coefficient between the
       crystal and the model pattersons evaluated within the defined
       sphere centred on the Patterson origin.

    **or SOLUTIONRS**
    For the ***SELF*** rotation function the solution is given in terms
    of Eulerian and polar angles with each line flagged: SOLUTIONRS.
    If Kappa is 180 or 120 then you may have a 2-fold or a 3 fold
    rotation between NCS related molecules.
    If you expect higher symmetry, *e.g.* 222 complex, check that the
    angles between related axes are perpendicular (Test DC\_X1\*DCX2 +
    DC\_Y1\*DCY2 +DC\_Z1\*DCZ2 = 0).
Output:
    **MAPOUT**
    A map of the rotation function can be output in the standard CCP4
    format. This is assigned to MAPOUT and can be contoured in the usual
    way (`NPO <npo.html>`__). It is sectioned along beta.

Step\_3d REORIENTATE\_Stage
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Keyword: `SHIFT <#rotfun_shift>`__ - converts the Eulerian angle
solutions determined for the model stored in XYZOUT<i> to give solutions
to be applied to original MODEL.

Input:
    Centre of Mass and Eulerian angles which were applied to the
    original MODEL in TABFUN.
Output:
    Some rotational solutions appropriate for the original coordinates.

This can be replaced by `PDBSET <pdbset.html>`__; see `example
[1j] <#example1j>`__.

Step\_4 TRAFUN
~~~~~~~~~~~~~~

Calculates the translation function using various target options.

Input:
    **HKLPCK0**
    Crystal h k l output by SORTING step.
Input:
    **table<i>**
    For any model(s) you wish to use.
Optional Input:
    **Memory allocation parameters**
    TRAING\_NR, TRAING\_MEQ, TRAING\_MRT, TRAING\_MT, TRAING\_MR
    (defaults estimated from crystal cell, and the dimensions of the
    model table). These can be reset if necessary.)
Input:
    A list of solutions to the Rotation function output obtained in
    Step\_3.
    The search for several molecules can be done by finding first one
    molecule, then FIXing it whilst searching for a second molecule,
    *etc.*
Output:
    A list of solutions flagged as: **SOLUTIONTF**.
    Each has: *Alpha\_i Beta\_i Gamma\_i Xf\_i Yf\_i Zf\_i CC\_F RF\_F
    CC\_I Dmin.*
    The Xf, Yf and Zf are fractions of the observed unit cell edges.
    CC\_F RF\_F CC\_I are described `above <#solution_criteria>`__. Dmin
    is the shortest distance between the centres of mass of the symmetry
    equivalent molecules. This can be used to identify solutions which
    overlap their symmetry mates.
Output:
    **MAPOUT**
    A map of the translation function can be output in the standard CCP4
    format.
    This is assigned to MAPOUT and can be contoured in the usual way
    (`NPO <npo.html>`__). The same file assignment is used for each
    TRANSLATION search you make, so if you want to contour your
    favourite solution you will need to rerun the calculation with only
    that SOLUTION. Remember it may be very large; assign it to a scratch
    area, or /dev/null if this causes problems.

Step\_5 FITFUN
~~~~~~~~~~~~~~

Performs rigid-body refinement for any specified solution of the
rotation or translation search, see `reference [5] <#reference5>`__.

Input:
    **HKLPCK0**
    Crystal h k l output by SORTING step.
Input:
    **table<i>**
    For any model(s) you wish to use.
Optional Input:
    **Memory allocation parameters**
    FITING\_MEQ, FITING\_MT, FITING\_NR, FITING\_NP
    (defaults estimated from crystal cell, and the dimensions of the
    model table). These can be reset if necessary.)
Input:
    A list of solutions.
Output:
    A list of solutions flagged as: **SOLUTIONF**.
    They are given as: Alpha\_i Beta\_i Gamma\_i Xf\_i Yf\_i Zf\_i CC\_F
    RF\_F CC\_I with the conventions described above.

Check that the CCs and RF\_F have improved.

Step\_6 REORIENTATE
~~~~~~~~~~~~~~~~~~~

This works out the appropriate rotation and translation parameters to
apply to the initial model (can also be done while running ROTFUN or
FITFUN).

Input:
    Centre of Mass and Eulerian angles which were applied to the
    original MODEL in TABFUN.
Input:
    The refined rotation and translation parameters output by FITFUN.
Input:
    **HKLPCK0**
    To extract the unit cell of new crystal form.
Output:
    A list of solutions given as:
    Alpha\_i Beta\_i Gamma\_i XA\_i YA\_i ZA\_i
    Correlation\_coefficient\_i Rfactor\_i. The XA, YA and ZA are given
    in Angstroms. Each line is flagged: **Shifted\_sol**.

LIKELY PROBLEMS
---------------

Some common errors:

-  You must run both CLMN calculations with the same resolution limits
   and sphere radius.
-  The HKLPCK files all pack the hkl and symmetry flag into one integer.
   The program checks the maximum values of H K L and NM ( =
   2\*Nsym\_primitive + 1) allowed for packing into a 32 bit integer.
   This is most restrictive at the Translation function stage which
   needs to store coefficients for all reflection pairs; H-Hj, K-Kj L-Lj
   where the Hj, Kj, and Lj are symmetry equivalents of H,K,& L, thus
   needs maximum values for the coefficients which are double the actual
   ones for the data.
-  See also `Memory allocation <#memory_allocation>`__ below concerning
   possible problems with memory.

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords. Only the
first 4 characters of a keyword are significant. Records may be
continued across line breaks using & or - as the last character on the
line to be continued. The available keywords are listed below grouped
according to their function:

General Keywords used at any stage:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

VERBOSE
    produces lots of output.
TITLE
    to help you know what you did.

Function keywords:
~~~~~~~~~~~~~~~~~~

These call the appropriate procedures.

SORTFUN
    calls SORTING procedure to sort and pack reflections.
TABFUN
    calls TABLING procedure to prepare structure factors from the model.
ROTFUN
    calls ROTING procedure for the rotation function (must be followed
    by `GENE <#rotfun_generate>`__ and/or `CLMN <#rotfun_clmn>`__ and/or
    `ROTA <#rotfun_rotate>`__).
TRAFUN
    calls TRAING procedure for the translation function.
FITFUN
    calls FITING procedure for rigid body fitting.
SHIFT
    calls REORIENTATE procedure to apply shifts to the model final
    solution.

Other primary keywords:
~~~~~~~~~~~~~~~~~~~~~~~

May be used for the given functions.

+------------------+------------------------------------------------------------------------------------------------+
| Keyword          | Used in                                                                                        |
+==================+================================================================================================+
| LABIN            | `SORTFUN <#sortfun_labin>`__                                                                   |
+------------------+------------------------------------------------------------------------------------------------+
| CRYSTAL          | `TABFUN <#tabfun_crystal>`__, `TRAFUN <#trafun_crystal>`__, `FITFUN <#fitfun_crystal>`__       |
+------------------+------------------------------------------------------------------------------------------------+
| MODEL            | `TABFUN <#tabfun_model>`__                                                                     |
+------------------+------------------------------------------------------------------------------------------------+
| SAMPLE           | `TABFUN <#tabfun_sample>`__                                                                    |
+------------------+------------------------------------------------------------------------------------------------+
| GENERATE         | `ROTFUN <#rotfun_generate>`__                                                                  |
+------------------+------------------------------------------------------------------------------------------------+
| CLMN             | `ROTFUN <#rotfun_clmn>`__                                                                      |
+------------------+------------------------------------------------------------------------------------------------+
| ROTATE           | `ROTFUN <#rotfun_rotate>`__                                                                    |
+------------------+------------------------------------------------------------------------------------------------+
| SHIFT            | `ROTFUN <#rotfun_shift>`__, `FITFUN <#fitfun_shift>`__, `REORIENTATE <#reorientate_shift>`__   |
+------------------+------------------------------------------------------------------------------------------------+
| SOLUTION         | `TRAFUN <#trafun_solution>`__, `FITFUN <#fitfun_solution>`__                                   |
+------------------+------------------------------------------------------------------------------------------------+
| SYMMETRY         | `TRAFUN <#trafun_symmetry>`__, `FITFUN <#fitfun_symmetry>`__                                   |
+------------------+------------------------------------------------------------------------------------------------+
| REFSOLUTION      | `FITFUN <#fitfun_refsolution>`__                                                               |
+------------------+------------------------------------------------------------------------------------------------+
| `END <#end>`__   |                                                                                                |
+------------------+------------------------------------------------------------------------------------------------+

Subsidiary keywords:
~~~~~~~~~~~~~~~~~~~~

These modify the following primary keywords. Most use sensible defaults.

+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Keyword                                 | Subsidiary Keywords                                                                                                                                                                                                                                                                |
+=========================================+====================================================================================================================================================================================================================================================================================+
| SORTFUN                                 | `RESOLUTION <#sortfun_resolution>`__, `MODEL <#sortfun_model>`__                                                                                                                                                                                                                   |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `LABIN <#sortfun_labin>`__              | FP=?? SIGFP=?? PHI=?? FOM=?? FC=?? PHIC=??                                                                                                                                                                                                                                         |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| TABFUN                                  | `NOROTATE <#tabfun_norotate>`__, `NOTRANSLATE <#tabfun_notranslate>`__, `NOTAB <#tabfun_notab>`__, `HKLOUT <#tabfun_hklout>`__, `SFOUT <#tabfun_sfout>`__                                                                                                                          |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `MODEL <#tabfun_model>`__               | `BTARGET <#tabfun_model_btarget>`__, `BREPLACE <#tabfun_model_breplace>`__, `BADD <#tabfun_model_badd>`__, `NORMALISE <#tabfun_model_norm>`__                                                                                                                                      |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `CRYSTAL <#tabfun_crystal>`__           | `ORTH <#tabfun_crystal_orth>`__                                                                                                                                                                                                                                                    |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SAMPLE <#tabfun_sample>`__             | `RESOLUTION <#tabfun_sample_resolution>`__, `SCALE <#tabfun_sample_scale>`__, `SHANNON <#tabfun_sample_shannon>`__                                                                                                                                                                 |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ROTFUN                                  |                                                                                                                                                                                                                                                                                    |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `GENERATE <#rotfun_generate>`__         | `RESOLUTION <#rotfun_generate_resolution>`__, `CELL\_MODEL <#rotfun_generate_cell_model>`__                                                                                                                                                                                        |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `CLMN <#rotfun_clmn>`__                 | `CRYSTAL <#rotfun_clmn_crystal>`__, `MODEL <#rotfun_clmn_model>`__, `ORTH <#rotfun_clmn_orth>`__, `FLIM <#rotfun_clmn_flim>`__, `SHARP \| BADD <#rotfun_clmn_sharpen>`__, `RESOLUTION <#rotfun_clmn_resolution>`__, `SPHERE <#rotfun_clmn_sphere>`__                               |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `ROTATE <#rotfun_rotate>`__             | `CROSS \| SELF <#rotfun_rotate_cross>`__, `MODEL <#rotfun_rotate_model>`__, `BESLIM <#rotfun_rotate_beslim>`__, `STEP <#rotfun_rotate_step>`__, `PKLIM <#rotfun_rotate_pklim>`__, `NPIC <#rotfun_rotate_npic>`__, `BMAX <#rotfun_rotate_bmax>`__, `LOCK <#rotfun_rotate_lock>`__   |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SHIFT <#rotfun_shift>`__               | `COM <#rotfun_shift_com>`__, `EULER <#rotfun_shift_euler>`__                                                                                                                                                                                                                       |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| TRAFUN                                  | `CB, CO, PT or PTF, HL, CC <#trafun_target>`__, `NMOL <#trafun_nmol>`__, `NCSTRANS <#trafun_ncstrans>`__, `RESOLUTION <#trafun_resolution>`__, `PKLIM <#trafun_pklim>`__, `NPIC <#trafun_npic>`__                                                                                  |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SYMMETRY <#trafun_symmetry>`__         |                                                                                                                                                                                                                                                                                    |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `CRYSTAL <#trafun_crystal>`__           | FLIM, ORTH, SHARP \| BADD, RESOLUTION                                                                                                                                                                                                                                              |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SOLUTION <#trafun_solution>`__         | `FIX <#trafun_solution_fix>`__                                                                                                                                                                                                                                                     |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| FITFUN                                  | `NMOL <#fitfun_nmol>`__, `RESOLUTION <#fitfun_resolution>`__, `ITER <#fitfun_iter>`__, `CONV <#fitfun_conv>`__                                                                                                                                                                     |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `CRYSTAL <#fitfun_crystal>`__           | FLIM, ORTH, SHARP \| BADD, RESOLUTION                                                                                                                                                                                                                                              |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SYMMETRY <#fitfun_symmetry>`__         |                                                                                                                                                                                                                                                                                    |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `REFSOLUTION <#fitfun_refsolution>`__   | BF AL BE GA X Y Z                                                                                                                                                                                                                                                                  |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SOLUTION <#fitfun_solution>`__         |                                                                                                                                                                                                                                                                                    |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SHIFT <#fitfun_shift>`__               | `COM <#fitfun_shift_com>`__, `EULER <#fitfun_shift_euler>`__                                                                                                                                                                                                                       |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| REORIENTATE                             |                                                                                                                                                                                                                                                                                    |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SHIFT <#reorientate_shift>`__          | `COM <#reorientate_shift_com>`__, `EULER <#reorientate_shift_euler>`__                                                                                                                                                                                                             |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `SOLUTION <#reorientate_solution>`__    |                                                                                                                                                                                                                                                                                    |
+-----------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SORTFUN keywords
----------------

SORTFUN [ RESOLUTION <rmin> <rmax> ] [ MODEL ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This signals the beginning of Step\_1 SORTFUN.

RESOLUTION
    <rmin> and <rmax> define the resolution range for all statistics.
    Can be put in as 4sin(theta)\*\*2/lambda\*\*2 limits, or as Angstrom
    limits in any order (defaults to MTZ resolution). Data output to
    HKLPCK0 are restricted to the outer resolution cutoff.
MODEL
    This signals that the structure factors input from HKLIN are to be
    used to make a table. This requires that they have been calculated
    from a model placed in a large unit cell and therefore the structure
    factors are sampled on a very fine grid.
    (See part of `example [2] <#example2>`__).

LABIN <column\_assignment> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[Compulsory]

A line giving the names of the input data items to be selected followed
by <program\_label>=<file\_label> assignments. Acceptable labels are:

::

    FP SIGFP PHI [W]    FC PHIC

| FC PHIC must be assigned for structure factors input.
| FP must be assigned for creating the list of observations.
| If PHI and optionally W is assigned, W\*FP and PHI are stored and can
  be used for phased translation searches.

Example:

::

        LABIN FP=F [ SIGFP=SIGF or PHI=PHIexptl W=FOM]
        LABIN FC=FC_domainA PHIC=PHIC_domainA

TABFUN keywords
---------------

TABFUN [ NOROTATE ] [ NOTRANSLATE ] [ NOTAB ] [ HKLOUT ] [ SFOUT ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This signals the beginning of Step\_2 TABFUN.

NOROTATE
    Do not rotate the model before initialising calculation.
NOTRANSLATE
    Do not translate the model before initialising calculation.
    Use this extremely rarely. AMoRe assumes your molecule lies roughly
    at the origin of the test cell. If you have already run TABFUN, and
    you wanted to carve pieces out of XYZOUT to do rigid body fitting on
    segments, it is useful to make a table for each fragment with the
    TABFUN NOROTATE NOSHIFT option. Similarly if you want to fit another
    possible model over the first XYZOUT. ***NEVER*** use this in an
    initial pass.
NOTAB
    Does not produce a table - just orientate the molecule if
    appropriate and move the molecule's centre of mass to the origin.
    This coordinate file can then be used to calculate structure factors
    and generate Es which can be read in to produce a table file.
HKLOUT
    The contents of the table can also be output as an ASCII list of H K
    L FC PHIC. This may be useful for checking.
SFOUT
    An alias for `HKLOUT <#tabfun_hklout>`__.

MODEL <i> BTARGET <btar> BREPLACE <brep> BADD <badd> NORMALISE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<i> is the model number and is followed by all information needed to
work with the model. At least one model must be specified to get any
output.

BTARGET <btar>
    The value <btar> should be set to the estimated B value of the
    crystal. Then the model B values will be modified to match this
    target.
    Default: Do not use this correction.
BREPLACE <brep>
    Replace all B factors in the model with <brep>.
    Default: Use input B factors.
BADD <badd>
    Add <badd> to all input model B factors. If <badd> is negative the
    model \`structure factors' are sharpened.
    Default: BADD = 0.00
NORMALISE
    This indicates that the crystal amplitudes are given as E values,
    and the model B factors must be modified to generate normalised
    model \`structure factors'.

PLEASE NOTE that if all the B-factors are zero in your model, then
<badd> MUST be set to a sensible positive value.

The coordinates written to XYZOUT will have the same B-factors as the
input coordinates, but the table will be generated using the modified
B-factors.

Examples:

::

    MODEL 1 BTARGET 23.5
    MODEL 1 BREPLACE 0 BADD -10

Other primary keywords (optional):
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CRYSTAL <a> <b> <c> <alpha> <beta> <gamma> ORTH <i>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Optional.

Cell dimensions for observed data used to generate PDB style header for
XYZOUT. The default is to use the TABFUN cell to generate the CRYST1 and
SCALEi records.

ORTH <i>
    orthogonalisation code. See `below <#orthogonalisation_codes>`__ for
    conventions (default <i>=1).

Example:

::

    CRYSTAL  112.32 112.32 85.14 90 90 120 ORTH 1

SAMPLE <i> [ RESOLUTION <dmin> SCALE <scale> SHANNON <sharat> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<i> is the model number and is followed by the sampling control
parameters.

RESOLUTION <dmin>
    <dmin> (in Angstroms) is the resolution limit of generated structure
    factors. There is no point in setting this higher than the maximum
    resolution given in SORTFUN.
SCALE <scale>
    Optional: default = 4.
    A model \`cell' created equal to (minimal box)\*<scale>. This
    controls how finely the model structure factors are sampled in
    reciprocal space.
SHANNON <sharat>
    <sharat> is the Shannon rate for sampling the coordinate map. The
    default is 2.5. If the B factors have been sharpened it is wise to
    use a finer grid, *i.e.* increase <sharat> to 3.5 or 4.

Example:

::

    SAMPLE 1 RESO 3 SHANN 2.5 SCALE 4.0

ROTFUN Keywords {Step\_3}
-------------------------

ROTFUN
~~~~~~

This signals the beginning of Step\_3 ROTFUN with subsequent keywords as
follows.

Generate {Step\_3a}
~~~~~~~~~~~~~~~~~~~

GENERATE <i> [ RESOLUTION <rmin> <rmax> CELL\_MODEL <a> <b> <c> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| <i> is the model number.
| This routine calculates the model \`structure factors' in a suitable
  P1 cell, and writes them in the same format as the SORTFUN output for
  the crystal amplitudes. The file is assigned to HKLPCK1.

RESOLUTION <rmin> <rmax>
    Resolution range for data output. Can be put in as
    4sin(theta)\*\*2/lambda\*\*2 or as Angstrom limits in either order.
    Choose the maximum resolution you may wish to use; this step need
    only be run once for each model and a subset extracted with the
    resolution limits given in CLMN.
CELL\_MODEL <a> <b> <c>
    for model structure factor generation (the angles are always 90
    degrees).
    Opinions differ as to the values to use.
    Eleanor Dodson says:

        "This model cell needs to be chosen carefully. Ideally you need
        to use dimensions of Twice maximal distance from Centre of Mass
        + SPHERE\_<Irmax> + a small safety term."

    She says always use a cubic cell because elongated cells can cause
    trouble.

    Navaza suggests using

        {smallest box containing model} + {integration radius (<Irmax>)}
        + resolution

    (not necessarily cubic)

    and others consider the cell dimensions less critical providing they
    are chosen large enough to avoid self-vectors.
    The maximal distance and minimal box are output by the TABFUN step.

Example:

::

    GENERATE RESO 20 3.2 CELL_MODEL 89 89 89

Calculate spherical harmonics {Step\_3b}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CLMN [ CRYSTAL \| MODEL <i> ] ORTH <i> FLIM <fmin> <fmax> [ SHARP \| BADD <badd> ] RESO <rmin> <rmax> SPHERE <rmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculates spherical harmonics for crystal and models.

CRYSTAL
    The input is HKLPCK0 for CRYSTAL
MODEL <i>
    HKLPCK1 for MODEL 1.
ORTH <i>
    Orthogonalisation code (see `below <#orthogonalisation_codes>`__ for
    code). Only needed for CRYSTAL. Except for monoclinic spacegroups
    with B unique, when ORTH = 3 may be useful, all orthogonalisation
    codes should be set to 1. Even for the monoclinic case it is usually
    easier to leave the code as 1.
    (default ORTH=1)
FLIM <fmin> <fmax>
    Minimum and maximum values of F used (rarely used option).
SHARP or BADD<badd>
    Sharpening B value for structure factors. This can be used to modify
    the input F by exp\*\*{-<badd>\*sin\*\*2(theta)/lambda\*\*2} before
    squaring, *i.e.* a negative <badd> will sharpen the data.
RESOLUTION <rmin> <rmax>
    Can be put in as 4sin(theta)\*\*2/lambda\*\*2 or as Angstrom limits
    in either order. These limits will truncate the H K L listed in
    HKLPCK. It is important that the SAME resolution limits are used for
    both the MODEL and the CRYSTAL.
SPHERE <Irmax>
    <Irmax> is the radius of the integration sphere in Angstroms. Tips:

    #. This should not be greater than your model's Maximal distance
       from Centre of Mass output by TABFUN. David Blow points out that
       for a spherical molecule 75-80% of the molecular diameter
       includes about 80% of the integrated Patterson density. Ian
       Tickle suggests using 75% of the minimum diameter in general.
    #. The volume of the sphere should probably not exceed the volume of
       the asymmetric unit.
    #. If the radius is greater than half the minimum cell edge you will
       be including some Patterson vectors twice. Opinions differ on how
       important this is, but the program warns about this case.

    Other factors like the shape of the model may influence you;
    remember this is the RADIUS within which the interesting self
    vectors should lie.

Examples:

::

    CLMN CRYSTAL RESO  20.0  4.0  SPHERE   30  -
                      ORTH  1   SHARP -10.0 FLIM 0.E0 1.E8  
    CLMN MODEL 1      RESO  20.0  4.0  SPHERE  30

Rotation {Step\_3c}
~~~~~~~~~~~~~~~~~~~

ROTATE [ CROSS \| SELF ] MODEL <i> BESLIM <lmin> <lsup> STEP <stepsize> PKLIM <rp> NPIC <np> BMAX <bmax> LOCK <nrot> [EULER/POLAR] <rot\_angle1> <rot\_angle2> <rot\_angle3> (nrot sets)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This routine calculates the rotation function.

CROSS or SELF
    Flags whether calculation is to be a SELF rotation, which will only
    need CLMN0 as input, or a CROSS rotation function which will need
    CLMN0 and some CLMN<i>. The correlation between self- and
    cross-rotation functions can be analysed with the program
    `RFCORR <rfcorr.html>`__.
MODEL <i>
    HKLPCK<i> for MODEL <i>.
BESLIM <lmin> <lsup>
    Expansion using spherical harmonic functions between <lmin> and
    <lsup> is done. Low order terms (*i.e.* for l = 2 or 4) tend to be
    governed by the crystal symmetry; excluding them may reduce the
    final peak heights, but make the rotation parameters more precise
    and make multiple solutions have more equal heights. The upper cut
    off is governed by the ratio of the integration radius to the
    resolution. The upper default is 500. The lower cut off has a
    similar effect to the inner cutoff radius for the Patterson vectors.
    However in some cases it helps to include all terms. Now the default
    is to test all lower limits of 2, 4, 6, 8 and 10 and see which gives
    the best contrast.
STEP <stepsize>
    Angular step size for Alpha, Beta and Gamma in degrees (default
    2.5). Defaults to sensible value for resolution requested. Should be
    checked from: STEP ~ 360 / ( 2\*<lsup> +1 )
PKLIM <rp>
    Output all peaks above <rp> \* {maximum peak height}. Default: 0.5
    for Cross rotations, 0.2 for self rotations. Maximum self rotation
    peak will always be the origin peak. The peak search algorithm is
    not very satisfactory for Beta limits, beta = 0 and beta = <bmax>.
    Default = 0.5.
NPIC <np>
    Number of peaks to output (limited to 99).
BMAX <bmax>
    Optional.
    Maximum BETA angle to consider (default 180, or 90 if you have a 2
    fold axis perpendicular to the first rotation axis (*e.g.* in
    pointgroups Pmmm, P622, P422 *etc.*).
LOCK <NROT> followed by optional EULER or POLAR flag and NROT sets of
**rotation** angles to describe the self rotations.
    These control the locked rotation function (see `reference
    [6] <#reference6>`__).
    The angles MUST refer to the SAME orthogonalisation convention as
    you are using for the CROSS rotation. See `example
    [3] <#example3>`__.
    If there are several molecules in the crystal asymmetric unit, AND
    you know the rotations which relate them to each other, *i.e* you
    have a solutions to the SELF ROTATION, then the solutions to the
    cross rotation can be searched to find sets which are related by the
    expected NCS operators. If you do not have a closed group things are
    messy. The self rotation always finds pairs of solutions, *i.e.*
    that which rotates Mol1 to Mol2, and that which rotates Mol2 to
    Mol1. These are the inverse of each other; in Polar coordinates,
    they have the form (Omega,Phi,Kappa) and (Omega,Phi,-Kappa), and the
    Eulerian equivalent is (Alpha, Beta, Gamma) and (-Gamma,-Beta,
    -Alpha).
    It is not altogether easy to decide what to do, and you need to have
    some idea of how many molecules you expect to find in the asymmetric
    unit, and how they may be arranged. This can be complicated to sort
    out; if there is a hexamer in the crystal, you would expect to find
    3 two-fold axes, all perpendicular to a three fold axis - if two
    axes are perpendicular, look at the product of their direct cosines:

    ::

        DC1(axis1)*DC1(axis2) + DC2(axis1)*DC2(axis2) + DC3(axis1)*DC3(axis2) = 0.0

    For TRAP, where the 11-fold rotation axis is perpendicular to a
    crystallographic 2 fold axis, the self rotation showed both a single
    peak at (Omega, Phi, 360/11) and 11 2-fold axes. This did NOT mean
    that TRAP contained 11 dimers, although the self rotation results
    were consistent with such a conclusion. AMoRe does not at present
    generate all symmetry equivalents of SELF rotation solutions so it
    is sensible to use ROTMAT to give a complete list.
    If you believe you have a proper rotation with a clear solution with
    Kappa equal 360/n, Kappa =180 ( 2-fold), or 120 (3-fold) or 72
    (5-fold) and the NCS operators form closed group, then you would
    specify NROT = n-1, followed by n-1 sets of polar angles to define
    the rotations: (Omega,Phi,360/n) and (Omega,Phi,2\*360/n) etc. In
    this case, every self rotation solution and its inverse belong to
    the set.
    If say, you expect 222 NCS symmetry with 3 intersecting 2-fold axes,
    you would set NROT="3" and specify the three sets of two fold axes:
    (Omega1,Phi1,180), (Omega2,Phi2,180) and (Omega3,Phi3,180).

Example

::

    ROTA CROSS MODEL 1 [ BESLIMI 6 120 STEP 2.5 PKLIM 0.5 NPIC 100     LOCK 1 POLAR 54 45 180]

Reorientation {Step\_3d}
~~~~~~~~~~~~~~~~~~~~~~~~

SHIFT <Model\_number> COM <Xcom> <Ycom> <Zcom> EULER <alpha> <beta> <gamma>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Reorientate stage. Moves Eulerian angle solutions determined for shifted
model stored in XYZOUT<Model\_number> to give solutions to be applied to
original model. Needed if you want your solutions converted back to ones
to apply to original coordinates.

COM <Xcom> <Ycom> <Zcom>
    Coordinates of the molecule's centre of mass output by TABFUN.
EULER <alpha> <beta> <gamma>
    Rotation angles applied to the original model output by TABFUN.

Example

::

    SHIFT 1 COM 17.3 -10.5 28.7 EULER 301.2 35.7 185.2

TRAFUN keywords {Step\_4}
-------------------------

TRAFUN [ CB \| CO \| PT \| PTF \| HL \| CC ] NMOL <nmol> [NCSTRANS <U\_vec> <V\_vec> <W\_vec>] [RESOLUTION <rmin> <rmax> ] [ PKLIM <rp> ] [ NPIC <np> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are various translation function targets. Each takes each
orientation solution in turn and searches for the NPIC "best"
translational Xi Yi Zi for this orientation. Good solutions should give
high correlation coefficients between FP and FC, and low Rfactors. Only
one target can be specified for each run.

CB \| CO - the method of Crowther and Blow (default).
    CB(T) = <DeltaI(obs) \* I(calc)(T)>
    The convolution (designated by "\*") of the observed Patterson
    (after subtraction of the contribution of the self vectors) with the
    calculated one for each value of the translation vector T.
PT \| PTF - Phased translation function.
    This can either use externally generated phases for the model
    (option PTF; input at SORTFUN) or for many body problems phases
    derived from the FIXed molecules (option PT).
    It looks for the best overlap of the 2 maps: (Fp:PHI model) and
    (Fc:PHI model). See `reference [4] <#reference4>`__.
HL - Harada-Lifchitz.
    HL(T) = <DeltaI(obs) \* I(calc)(T)> / < I(calc)(T)>
    Here the convolution has been "normalised".
CC - correlation coefficient.
    CC(T) = <DeltaI(obs) \* I(calc)(T)> / sqrt( < DeltaI(obs)\*\*2 \*
    I(calc)(T)\*\*2>
    This function is powerful but much slower.

Each function tests each orientation solution in turn and searches for
the best translational Xi Yi Zi for this orientation. Good solutions
should give high correlation coefficients between FP and FC, and low
Rfactors. For the first molecule all <Xi> <Yi> <Zi> belonging to the
Cheshire cell are searched (see `reference [7] <#reference7>`__). The
Cheshire cell is the minimum volume which will allow a unique solution.
For the first molecule it will be the cell which covers a volume from
one possible origin to the next - you can usually see it by inspection
of International Tables, *e.g.*: For P212121, the Cheshire cell is
0-0.5,0-0.5,0-0.5. For P21 the Cheshire cell is 0-0.5,any y,0-0.5. If
you are searching for the NMOLth molecule of a set, the Cheshire cell
will now be the whole primitive volume. You have assigned the origin by
choosing the position of the first molecule, and the other molecules
will have to be positioned relative to that choice.

A map of the Cheshire cell for each search is written to the file
assigned to MAPOUT. *N.B.* the same file is used for all solutions -
only the final one will be saved. If you wish to plot your best solution
you will have to recalculate it.

Translation functions use a great deal of memory. The whole FFT
transform is held in memory at once, and the calculation is done over a
set of reciprocal lattice coefficients which can be twice the size of
Hmax, Kmax, Lmax.

NMOL <nmol>
    Number of molecules to search for (maximum 65). The program assumes
    you have solutions for <nmol>-1 molecules and searches for the best
    fit for the <nmol>-th one. The <nmol>-1 solutions must be FIXed; see
    `examples [1f], [1g], [1h] <#example1f>`__. Default = 1. It is more
    complicated if you are using a `NCS translation
    vector <#trafun_ncstrans>`__.
NCSTRANS <U\_vec> <V\_vec> <W\_vec>
    If there is a non-crystallographic translation between two molecules
    in the unit cell, this will be indicated by a large ( > 20% of
    origin) peak in the native 4Å Patterson; see CCP4i Task: Analyse
    Data for MR) it is best to search for the two related molecules at
    the same time. You need to give the TRAFUN the coordinates of the
    Patterson vector, <U\_vec> <V\_vec> <W\_vec>. This always requires
    that <nmol> is advanced by 2 for the next cycle of TRANSLATION
    searching. For the first pass, set nmol as 1, and the program will
    position a pair of molecules with the same orientation, and
    translations related by <U\_vec> <V\_vec> <W\_vec>. For the next
    pass set <nmol> as 3, FIX both these molecules, and search for the
    next pair. See `examples [1f], [1g], [1h] <#example1f>`__ and
    `example 4 <#example4>`__. Default = 1.
RESOLUTION <rmin> <rmax>
    Can be put in as 4sin(theta)\*\*2/lambda\*\*2 or as Angstrom limits
    in any order.
PKLIM <rp>
    Output all peaks above <rp\*>{maximum peak height}. Default 0.5.
NPIC <np>
    Number of peaks to output from the translation function map for each
    orientation. Default 10. Be aware that the highest peaks in the
    translation function map do not necessarily correspond to the
    highest correlation coefficients. All targets are prone to generate
    "noise" peaks, and good solutions usually satisfy all 3 criteria:
    High T1 peak, high correlation coefficient, low Rfactor.

Example

::

    TRAFUN CO NMOL 1 RESO 8 4 PKLIM 0.5 NPIC 10  NCStran  0.03 0.0 0.5

Other optional keywords
~~~~~~~~~~~~~~~~~~~~~~~

SYMMETRY <spg>
~~~~~~~~~~~~~~

(Optional)

Spacegroup name or spacegroup number. It will default to that of the
CRYSTAL data, picked up at the SORTFUN step. You may need to change it
to test other possibilities; *e.g.* enantiomorphic spacegroups - P65
instead of P61. If you are not sure of your spacegroup, the translation
function is a good way to distinguish the true spacegroup; *e.g.* you
may need to test all possible orthorhombic possibilities; P222; P2 2 21;
P2 21 2; P2 21 21; P21 2 2; P 21 2 21; P21 21 2; P 21 21 21; See
`example [1d], [1e] <#example1d>`__.

CRYSTAL FLIM <fmin> <fmax> ORTH <i> [ SHARP \| BADD <badd> ] RESOLUTION <rmin> <rmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(Optional)

Information used to modify the CRYSTAL amplitudes. See descriptions
`above for CLMN <#rotfun_clmn>`__.

Example:

::

    CRYSTAL ORTH 1 FLIMI 0.E0 1.E8 SHARP 0.0

Other compulsory keywords
~~~~~~~~~~~~~~~~~~~~~~~~~

SOLUTION [FIX] <i> <alphai> <betai> <gammai> [ <Xi> <Yi> <Zi> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

FIX <i> <alphai> <betai> <gammai> <Xi> <Yi> <Zi>
    If the molecule generated by this solution is FIXed, the last 6
    parameters define its position in the cell. Structure factors
    calculated from this molecule will be added to those generated for
    molecules which are being searched for.
    When searching for a single molecule, a list of possible
    orientations from the rotation function (labelled SOLUTIONRC in
    ROTFUN output) is required.
    Molecules are found sequentially. When searching for the nth
    molecule of a set, there must be sets of (n-1) previously determined
    solutions to the translation function. These are labelled with the
    key word FIX. For example to find the 2nd molecule fix one solution:

    ::

        SOLUTIONTF1 FIX 1 <alpha1> <beta1> <gamma1>  <X1> <Y1> <Z1>

    followed by the set of possible rotation function solutions. Each
    rotation orientation is tested in turn with the previous input FIXed
    solution. If you want to test several translation solutions, you can
    repeat the FIX information, and again follow it with the set of
    possible rotation function solutions.

    To find the 3rd molecule fix a pair of solutions:

    ::

        SOLUTIONTF1 FIX 1 <alpha1> <beta1> <gamma1>  <X1> <Y1> <Z1>
        SOLUTIONTF2 FIX 1 <alpha2> <beta2> <gamma2>  <X2> <Y2> <Z2>

    There is a limit of 99 (calculated as NMOL\* Number\_of\_solutionrc)
    on the number of orientation solutions which can be included in one
    run. However there is no extra overhead in submitting several runs.
    This list should come last and is terminated by end-of-file or the
    keyword `END <#end>`__.

    The list of solutions can be extracted from ROTFUN (and TRAFUN)
    output using grep and edited in here.
<i>
    <i> is the number for the appropriate table<i>.
<alphai> <betai> <gammai>
    Euler angles output by ROTFUN. If there are no clear maxima you
    should test many solutions. Correct solutions have been found from
    rotation solutions which were far down the list.

Examples

::

    SOLUTIONTF FIX 1 27.8 100.7 350.1 0.146 0.566 0.00 17.4 52.5
    SOLUTIONRC 1 25.211 105.573 339.440

HINTS
~~~~~

To extract the rotation information, \`grep' (Unix) for \`SOLUTIONRC' in
the ROTFUN output. Edit the resulting list to include only those
solutions you want to run the translation search on, and include them in
the input data *e.g.* with \`@<file>'.

If you are searching for the <nmol>th molecule of a set, you must FIX
<nmol>-1 solutions and search for the <nmol>th one. You will probably
have several sets of the fixed solutions to test, plus many possible
orientation solutions.

FIXed solutions will be extracted from your previous TRAFUN log. They
will be followed by the list of solutions to the Rotation function
output by Step\_3. Structure factors calculated from the FIXed solutions
are added to those generated for search molecules.

To extract the information for FIXed, grep for \`SOLUTIONTF'. You will
need to sort these to find those with the highest correlation
coefficients, and lowest Rfactors.

::

     sort -r +8 -9 tra.list > tra_cc.list  # sort on correlation coefficient.
     sort +9 -10 tra.list > tra_rf.list  # sort on Rfactor

(Be careful to keep sets of solutions together!)

See the Unix plumbing in the example scripts, *e.g.*, \`auto-amore'.

FITFUN keywords {Step\_5}
-------------------------

FITFUN [ NMOL <nmol> RESOLUTION <rmin> <rmax> ITER <niter> CONV <con>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This signals the beginning of Step\_5 FITFUN which performs Rigid-body
refinement. It minimises the sum over all hkl of
({Fo\*exp(-Bs\*\*2)}\*\*2 - {k\*Fc\*\*2})\*\*2 with respect to scale,
B-factor and rotation and translation parameters.

Subsidiary words after FITFUN (many same as TRAFUN):

NMOL <nmol>
    Number of molecules to fit. All are fitted together by an iterative
    procedure.
RESOLUTION <rmin> <rmax>
    Can be put in as 4sin(theta)\*\*2/lambda\*\*2 or as Angstrom limits
    in any order. Often sensible to "fit" the molecules against high
    resolution data if the sequence homology is close.
ITER <niter>
    Number of iterations (default 10).
CONV <con>
    Convergence acceptance (default 0.001).

Example

::

    FITFUN NMOL 3 RESO 20 4.5 ITER 10 CONV 1.E-3 

Extra keywords
~~~~~~~~~~~~~~

CRYSTAL FLIM <fmin> <fmax> ORTH <i> [ SHARP \| BADD <badd> ] RESOLUTION <rmin> <rmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(Optional)

Information used to modify the CRYSTAL amplitudes. See descriptions
`above for CLMN <#rotfun_clmn>`__.

SYMM <spg>
~~~~~~~~~~

(Optional)

Spacegroup name or spacegroup number. It will default to that of the
CRYSTAL data, picked up at the SORTFUN step. You may need to change it
to test other possibilities; *e.g.* enantiomorphic spacegroups - P65
instead of P61.

REFSOLUTION [ BF ] [ AL ] [ BE ] [ GA ] [ X ] [ Y ] [ Z ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Refinement to be done for any of temperature factor, alpha, beta, gamma,
x, y, z. Remember - in polar spacegroups you cannot refine either y or z
parameter for one solution. This defaults to sensible values for
different space groups.

Optional: program chooses sensible defaults.

Example

::

    REFSOL AL BE GA X Y Z BF

SOLUTION <i> <alphai> <betai> <gammai> [ <Xi> <Yi> <Zi> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<i>
    Model number for input. Different solutions may require different
    model numbers. Assign all table<i>.
<alphai> <betai> <gammai>
    Euler angles output by ROTFUN. If there is no clear maximum you
    should test many solutions. Correct solutions have been found from
    rotation solutions which were far down the list.
<Xi> <Yi> <Zi> [ <CCi> <RFi> ]
    These three parameters define the molecules position in the cell. It
    is often convenient to keep the correlation coefficient and R factor
    on the solution line. It helps to monitor solutions - subsequent
    steps should improve these parameters! The solutions are refined in
    sets of NMOL. There may be up to 99 solutions given (99/NMOL sets).

Examples

::

    SOLUTIONTF 1  25.1  105.6  339.5 0.1139  0.5691  0.0000
    SOLUTIONTF 1  27.6  100.6 350.3 0.1461 0.5716 0.6476 48 51
    SOLUTIONTF 1  27.7  115.9 353.5 0.1439 0.6027 0.3584 49 54

This list is terminated by end-of-file or the keyword `END <#end>`__.

This list of Eulerian angles and translations can be extracted from the
log file and edited in here. To extract the information from the
previous log file, grep for \`SOLUTIONTF'. You will need to sort these
to find those with the highest correlation coefficients, and lowest
Rfactors as `described in step\_4a <#solution_grep>`__, and edit to
include only those solutions you want to run the rigid body refinement
on to include them in the input data.

SHIFT <Model\_number> COM <Xcom> <Ycom> <Zcom> EULER <alpha> <beta> <gamma>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Reorientate stage. Moves Eulerian angle solutions determined for shifted
model stored in XYZOUT<i> to give solutions to be applied to original
MODEL. Needed if you want your solutions converted back to ones to apply
to original coordinates.

COM <Xcom> <Ycom> <Zcom>
    coordinates of the molecules centre of mass output by TABFUN
EULER <alpha> <beta> <gamma>
    rotation angles applied to the original model output by TABFUN.

Example

::

    SHIFT 1 COM 17.3 -10.5 28.7 EULER 301.2 35.7 185.2

REORIENTATE keywords {Step\_6}
------------------------------

SHIFT <Model\_number> COM <Xcom> <Ycom> <Zcom> EULER <alpha> <beta> <gamma>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This signals the beginning of Step\_6 - reorientate stage. This step can
be run as a standalone step or as part of `ROTFUN <#rotfun_shift>`__ or
`FITFUN <#fitfun_shift>`__. It moves Eulerian angle solutions determined
for shifted model stored in XYZOUT<i> to give solutions to be applied to
original MODEL. Needed if you want your solutions converted back to ones
to apply to original coordinates.

COM <Xcom> <Ycom> <Zcom>
    Coordinates of the molecule's centre of mass output by TABFUN
EULER <alpha> <beta> <gamma>
    Rotation angles applied to the original model output by TABFUN.

Example

::

    SHIFT 1 COM 17.3 -10.5 28.7 EULER 301.2 35.7 185.2

Compulsory following keyword
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SOLUTION <i> <alphai> <betai> <gammai> <Xi> <Yi> <Zi>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There may be up to 99 solutions given. This list is terminated by
end-of-file or the keyword `END <#end>`__.

Examples

::

    SOLUTIONTF 1  25.1  105.6 339.5 0.1139 0.5691 0.0000
    SOLUTIONTF 1  27.6  100.6 350.3 0.1461 0.5716 0.6476 43.5 46.5
    SOLUTIONTF 1  27.7  115.9 353.5 0.1439 0.6027 0.3584 41.3 47.3

END
---

Must be last keyword. Used as termination for list of solutions.

NOTES
-----

Memory allocation
~~~~~~~~~~~~~~~~~

The program has been made more memory-efficient, but still uses a lot,
at several points a whole Fourier transform is held in memory. The
defaults are estimated to allow the observed and tabulated structure
factors to be stored. However if the estimate is too low it is able to
use dynamic memory allocation; the amount to be allocated at runtime is
parameterised by assigning values to logical names. There may be some
trial and error involved in setting appropriate values.

If the allocation for an array isn't large enough, the program stops
with a message which should indicate at least which parameter needs to
be increased and, in most cases, to what value. If the message doesn't
make it clear what needs to be increased, please report the fact. Using
the keyword VERBOSE may give more indication. The current values are
printed in the output (look for \`Memory allocation'). They may be
changed by giving the appropriate logical names an integer value (which
represents the size of an array) in any of possible ways:

-  On the command line *e.g.*, \`TABLING\_MR 5400000';
-  From the environment:
   setenv TABLING\_MR 5400000 # csh
   TABLING\_MR=5400000 # sh
-  By editing $CINCL/default.def *e.g.* with a line:
   TABLING\_MR=5400000

The last option may be most appropriate on a system with lots of memory
to provide large defaults and the distributed default.def contains
commented-out values for a \`big' version used at York and Cambridge.

Rotation matrix definitions
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The convention is that the orthogonalised coordinates of "crystal 2"
(usually the model) are rotated to overlap the orthogonalised
coordinates of crystal 1.

::

    i.e. [XO1]    = [ROT] [XO2]
         [YO1]            [YO2]
         [ZO1]            [ZO2]

This means that axis permutations introduced by using NCODE = 2, 3 or 4
will result in apparently different solutions, although the effect on
the fractional coordinates is the same.

In Polar angles:

    If ``l m n`` are the direction cosines of the axis about which the
    rotation ``k`` = kappa takes place, and:

    ::

        ( l )    ( sin omega cos phi )
        ( m )  = ( sin omega sin phi )
        ( n )    ( cos omega )

    where omega is the angle the rotation axis makes to the ZO
    direction, and phi is the angle the projection of the rotation axis
    onto the XO-YO plane makes to the XO axis.

    ::

        [ROT] =
        ( l**2+(m**2+n**2)cos k     lm(1-cos k)-nsin k        nl(1-cos k)+msin k   )
        ( lm(1-cos k)+nsin k        m**2+(l**2+n**2)cos k     mn(1-cos k)-lsin k   )
        ( nl(1-cos k)-msin k        mn(1-cos k)+lsin k        n*2+(l**2+m**2)cos k )

    Note that if omega = 0 or 180, then phi is indeterminate and is
    flagged as 999 in the SOLUTIONs output by AMoRe.

In Eulerian angles:

    | If a (alpha) represents a rotation about the initial ZO axis,
    | b (beta) represents a rotation about the new position of the YO
      axis, and
    | g (gamma) represents a rotation about the final ZO axis:

    ::

        [ROT] =
        ( cosa cosb cosg - sina sing     -cosa cosb sing - sina cosg     cosa sinb )
        ( sina cosb cosg + cosa sing     -sina cosb sing + cosa cosg     sina sinb )
        ( -sinb cosg                     sinb sing                       cosb      )

Orthogonalisation codes
~~~~~~~~~~~~~~~~~~~~~~~

::

    orthogonalisation code NCODE
       = 1, orthogonal x y z along a,c*xa,c* (Brookhaven, default)
       = 2                         b,a*xb,a*
       = 3                         c,b*xc,b*
       = 4                         a+b,c*x(a+b),c*
       = 5                         a*,cxa*,c   (Rollett)

EXAMPLES
--------

#. 

   ::

       The automated procedure to find 3 molecules for spmi.
       Usually this would be run from the interface but the command scripts are these.
       The space group is either P61 or P65.

   i.   ::

            Tabling run to generate structure factors from model;

   ii.  ::

            Sorting run to reformat observed reflections;

   iii. ::

            Rotation Patterson search;

   iv.  ::

            Translation search for one molecule in space group P61;

   v.   ::

            Translation search for one molecule in space group P65
                (The rotation solutions are the same for either P61 or P65)

   vi.  ::

            The correlation coefficient are higher for the P65 spacegroup.
                To make absolutely sure search for the 2nd molecule in both P61 
                and P65, but as expected P65 is much the better result.

   a. 

      ::

          #   #############
          #    tabling run:
          #   #############
          #
          #  The B factor for the crystal obtained from the Wilson plot is 23.5
          #
          #  TABFUN first rotates and shifts the model coordinates  to the origin
          #  then produces a table of structure factors in a large unit cell:
          #
          #    xyzout contains the rotated and shifted coordinates.
          #
          amore  xyzin1  search.pdb 
                 xyzout1 searchrot.pdb \
                 table1 search-sfs.tab << eof
          TITLE :  Produce table for MODEL FRAGMENT
          VERBOSE
          TABFUN
          CRYSTAL  112.32 112.32 85.14 90 90 120 ORTH 1
          MODEL 1 BTARGET 23.5
          SAMPLE 1 RESO 2.5 SHANN 2.5 SCALE       4.0
          eof

   b. 

      ::

          #   ############
          #    sorting run:
          #   #############
          # MTZ file contains cell and symmetry.
          #
          amore hklin spmi_trun.mtz 
                hklpck0 spmipch.hkl <<  eof
          TITLE   ** spmi  packing h k l F for crystal**
          SORTFUN RESOL 100.  2.5
          LABI FP=F  SIGFP=SIGF
          eof

   c. 

      ::

          #   ############
          #    roting run:
          #   ############
          #
          #  straightforward rotation function.
          #
          amore  table1 search-sfs.tab \
                 HKLPCK1 $CCP4_SCR/search.hkl \
                 hklpck0 spmipch.hkl \
                 clmn1 $CCP4_SCR/search.clmn \
                 clmn0 $CCP4_SCR/spmipch.clmn  \
                 MAPOUT $CCP4_SCR/amore_cross.map <<  eof
          ROTFUN
          VERB
          TITLE : Generate HKLPCK1 from MODEL FRAGMENT   1
          GENE 1   RESO 100.0 3.0  CELL_MODEL 80 75 65
          CLMN CRYSTAL ORTH  1 RESO  20.0  4.0  SPHERE   30
          CLMN MODEL 1     RESO  20.0  4.0 SPHERE   30
          ROTA  CROSS  MODEL 1  PKLIM 0.5  NPIC 100
          eof

   d. 

      ::

          #   #############################
          #    traing run:   NMOL = 1 - P61
          #   #############################
          #
          amore  table1 search-sfs.tab \
                 HKLPCK0 spmipch.hkl \
                 MAPOUT $CCP4_SCR/amore_transjunk1.map <<  eof
          TRAFUN CB   NMOL 1 RESO 8 4  PKLIM 0.5  NPIC 10
          SYMM P61
          VERB
          TITLE : Translation function P61 - one molecule
          SOLUTIONRC 1    25.211   105.573   339.440
          SOLUTIONRC 1    27.757   100.743   350.082
          SOLUTIONRC 1    27.939   115.792   353.601
          SOLUTIONRC 1    27.596    60.308    43.149
          SOLUTIONRC 1    38.604    77.537   160.999
          SOLUTIONRC 1    16.079   130.379   261.311
          SOLUTIONRC 1     7.264    66.987    88.523
          SOLUTIONRC 1     4.345    82.989    95.253
          SOLUTIONRC 1    26.903    76.829    37.613
          SOLUTIONRC 1     1.477    33.145    73.636
          SOLUTIONRC 1    42.057   104.775   163.088
          SOLUTIONRC 1     0.492    90.289   275.552
          SOLUTIONRC 1    53.344   135.528   269.211
          SOLUTIONRC 1    34.118    74.264   244.711
          SOLUTIONRC 1    42.237   147.472   263.153
          SOLUTIONRC 1    33.968     5.665   291.432
          eof

   e. 

      ::

          #   #############################
          #    traing run:   SYMMETRY P65 - same rotation solns
          #   #############################
          #
          amore  table1 search-sfs.tab \
                 HKLPCK0 spmipch.hkl \
                 MAPOUT $CCP4_SCR/amore_transjunk5.map <<  eof
          TRAFUN CB   NMOL 1 RESO 8 4  PKLIM 0.5  NPIC 10
          SYMM P65
          VERB
          TITLE : Translation function P65 - one molecule
          SOLUTIONRC 1    25.211   105.573   339.440
          SOLUTIONRC 1    27.757   100.743   350.082
          SOLUTIONRC 1    27.939   115.792   353.601
          SOLUTIONRC 1    27.596    60.308    43.149
          SOLUTIONRC 1    38.604    77.537   160.999
          SOLUTIONRC 1    16.079   130.379   261.311
          SOLUTIONRC 1     7.264    66.987    88.523
          SOLUTIONRC 1     4.345    82.989    95.253
          SOLUTIONRC 1    26.903    76.829    37.613
          SOLUTIONRC 1     1.477    33.145    73.636
          SOLUTIONRC 1    42.057   104.775   163.088
          SOLUTIONRC 1     0.492    90.289   275.552
          SOLUTIONRC 1    53.344   135.528   269.211
          SOLUTIONRC 1    34.118    74.264   244.711
          SOLUTIONRC 1    42.237   147.472   263.153
          SOLUTIONRC 1    33.968     5.665   291.432
          eof

   f. 

      ::

          #   #############################
          #    traing run:   SEarch for 2nd molecule P61
          #   #############################
          #
          amore  table1 search-sfs.tab  \
                 HKLPCK0 spmipch.hkl <<  eof
          TRAFUN PTF   NMOL 2 RESO 8 4  PKLIM 0.5  NPIC 10
          SYMM P61
          VERB
          TITLE : Translation function P61  - 2 mols together.
          SOLUTIONTF FIX 1    27.76   100.74   350.08  0.145  0.566  0.000 17.4 52.5
          SOLUTIONRC 1    27.94   115.80   353.60
          SOLUTIONRC 1    25.21   105.57   339.45
          SOLUTIONRC 1    27.94   115.80   353.60
          SOLUTIONRC 1    27.76   100.74   350.08
          eof

   g. 

      ::

          #   #############################
          #    traing run:   2nd Molecule - P65
          #   #############################
          #
          amore  table1 search-sfs.tab  \
                 HKLPCK0 spmipch.hkl <<  eof
          TRAFUN PTF   NMOL 2 RESO 8 4  PKLIM 0.5  NPIC 10
          SYMM P65
          VERB
          TITLE : Translation function P65  - 2 mols together.
          SOLUTIONTF FIX 1    27.76   100.74   350.08  0.116  0.437  0.000 19.4 51.7
          SOLUTIONRC 1    27.94   115.80   353.60
          SOLUTIONRC 1    25.21   105.57   339.45
          SOLUTIONRC 1    27.94   115.80   353.60
          SOLUTIONRC 1    27.76   100.74   350.08
          eof

   h. 

      ::

          #   ###########################
          #  traing run:   Search for 3rd molecule - P65
          #   ###########################
          #
          #  (no point in testing P61 now - P65 gives higher correlations and lower Rfactor)
          #
          amore  table1 search-sfs.tab  \
                 HKLPCK0 spmipch.hkl  \
                 TRAFUN trafun.9 <<  eof
          TRAFUN PTF   NMOL 3 RESO 8 4   PKLIM 0.5  NPIC 10
          SYMM P65
          VERB
          TITLE : Translation function P65  - 2 mols together.
          SOLUTIONTF FIX 1    25.21   105.57   339.45  0.113  0.567  0.000 38.0 46.7
          SOLUTIONTF FIX 1    27.76   100.74   350.08  0.146  0.571  0.652 38.0 46.7
          SOLUTIONRC 1    27.94   115.80   353.60

          SOLUTIONTF FIX 1    25.21   105.57   339.45  0.111  0.567  0.000 35.8 47.0
          SOLUTIONTF FIX 1    27.94   115.80   353.60  0.144  0.603  0.358 35.8 47.0
          SOLUTIONRC 1    27.76   100.74   350.08

          SOLUTIONTF FIX 1    27.76   100.74   350.08  0.145  0.566  0.000 31.3 48.8 
          SOLUTIONTF FIX 1    27.94   115.80   353.60  0.144  0.603  0.705 31.3 48.8
          SOLUTIONRC 1    25.21   105.57   339.45
          eof

   i. 

      ::

          #   ############
          #    fiting run: 3 molecules Symm P65
          #   ############
          #
          amore  table1 search-sfs.tab  \
                 HKLPCK0 spmipch.hkl <<eof
          FITFUN  NMOL 3  RESO 20 4.5  
          TITLE *** spmi   structure ***
          SYMM P65
          VERBOSE
          REFSOL   AL     BE   GA     X   Y    Z   BF
          SOLUTIONTF 1  25.02  105.58  339.46 0.113 0.569 0.000 27.5 51.7
          SOLUTIONTF 1  27.60  100.60  350.29 0.146 0.571 0.649 43.5 46.5
          SOLUTIONTF 1  27.72  115.95  353.54 0.143 0.602 0.351 41.3 47.3
          eof

   j. 

      ::

          #
          #  Build the solution file with with PDBSET.
          #
          Assume the following three solutions from AMoRe:
          # SOLUTIONF     1   56.35   74.98  145.14  0.3883 -0.0061  0.2757 55.7 45.2 57.1  28
          # SOLUTIONF     1  295.44   70.84  148.61  0.8273  0.9301  0.2737 55.7 45.2 57.1  29
          # SOLUTIONF     1  164.23   69.22  147.81  0.0896  0.8444  0.2876 55.7 45.2 57.1  30
          Then:
          pdbset \
          xyzin /y/ccp4/work/model-rot.pdb \
          xyzout /y/ccp4/work/model-rot-sol1.pdb \
          <<eof
          CELL 78.700   40.400   56.000  90.00 117.10  90.00 
          SYMM C2
          rotat euler   56.35   74.98  145.14  
          shift frac 0.3883 -0.0061  0.2757 55.7 45.2 57.1  28
          chain A
          end
          eof
          #
          pdbset \
          xyzin /y/ccp4/work/model-rot.pdb \
          xyzout /y/ccp4/work/model-rot-sol2.pdb \
          <<eof
          # Use -0.5,-0.5,0 = other C2 solution
          CELL 78.700   40.400   56.000  90.00 117.10  90.00 
          SYMM C2
          rotat euler  295.44   70.84  148.61
          shift frac 0.3273  0.4301  0.2737 55.7 45.2 57.1  29
          chain B
          end
          eof
          #
          pdbset \
          xyzin /y/ccp4/work/model-rot.pdb \
          xyzout /y/ccp4/work/model-rot-sol3.pdb \
          <<eof
          #  Subtract 1 from y
          CELL 78.700   40.400   56.000  90.00 117.10  90.00 
          SYMM C2
          rotat euler  164.23   69.22  147.81
          shift frac 0.0896 -0.1556  0.2876 55.7 45.2 57.1  30
          chain C
          end
          eof
          #
          cat the three solution coordinates into one pdb file - model-rot-sol123.pdb.
          Check if there are bad symmetry clashes.

          distang \
          xyzin /y/ccp4/work/model-rot-sol123.pdb \
          <<eof
          SYMM C2
          RADI CA 2
          eof

#. 

   ::

       #
       # Tabulating structure factors generated from a blob of electron density
       # The blob has been placed in a large "P1 unit cell" to give a finely sampled reciprocal lattice.
       #

       #!/bin/csh -f
       ###########################################################
       #
       # There are lots of alternative ways of getting a masked block of density.
       #  You first need a mask. This does not need to cover the whole molecule.
       # The simplest technique I have used is to place a large sphere
       # at the centre of mass of a likely region.
       # This can be done by placing an "atom" at the centre of mass of a likely region
       # and specified  a large atomic radius for it. 
       #
       # Another way is to edit bones generated from a map to include only those 
       # which are likely to belong to one molecule, then use bones_to_pdb to write out a 
       # cordinate file, and use ncsmask with that set, and the default atom radius.
       #  ( 3A I think..)
       #
       ####################################################################
       #  Make a spherical mask centred at the centroid of the chosen block of 
       #  density.
       ###########################################################
       #  P65_block_com.pdb 
       # REMARK Centre of Mass: X ~35/102, Y~ 42/102, Z~75/96 = (0.343 0.412 0.781)
       # REMARK COM in As:  0.343 * 208.4 = 71.510 ; 0.412 *208.4 =85.812 0.781=75.156 
       #  P65_block_com.pdb
       CRYSTL  208.400  208.400   96.200  90.00  90.00 120.00    P65
       ATOM      1  N   COM C   3      71.510  85.812  75.156  1.00 41.63           N
       #
       # Set atomic radius; i.e. radius of sphere to 18Å
       ncsmask xyzin ./P65_block_com.pdb \
       mskout $SCRATCH/P65_block_com.msk <<eof
       #  I have taken a 1A grid.
       GRID  204  204   96
       AXIS   Y    X    Z
       RADIUS 18
       END
       eof
       #
       ###########################################################
       # extend the  DM map to the same limits as the msk;
       #   you will have to look at the log of Step 1.
       #  ( You can get the mask extent by typing 
       #     prmap mapin $CCP4_SCR/P65_block_com.msk )
       ###########################################################
       mapmask mapin /y/work2/suresh//nat3_au5_hg2_dm.map \
       mapout $CCP4_SCR//nat3_au5_hg2_dm.ext << eof
       GRID 204  204   96
       XYZLIM  57  93    62 101    56 91
       END
       eof
       #
       #
       ###########################################################
       #   Now the clever bit - put the "masked" density in the big P1 cell:
       #
       maprot \
       wrkin $CCP4_SCR//nat3_au5_hg2_dm.ext \
       mskin $CCP4_SCR/P65_block_com.msk \
       mapout $CCP4_SCR/nat3_au5_hg2_dm_cent_bigdummycell.map \
       <<eof
       # "MODE TO" moves the WRKIN map ( after masking with MSKIN) to the given cell and grid.
       MODE TO
       #  No averaging; this is the identity..
       GRID XTAL 300 300 300                         ! Fine grid for structure factors
       CELL XTAL 240.000  240.000  240.000  90.00  90.00  90.00
       SYMM P1
       AVERAGE 1
       ROTATE EULER 0 0 0
       TRANS 0 0 0
       END
       eof
       #
       ###########################################################
       #
       #  Generate structure factors from this density ready for Amore
       # Then delete the *bigdummy*maps - they are HUGE..
       sfall \
       mapin  $CCP4_SCR/nat3_au5_hg2_dm_cent_bigdummycell.map \
       hklout $CCP4_SCR/nat3_au5_hg2_dm_cent_bigdummycell.mtz \
       <<eof
       MODE SFCALC MAPIN
       SYMM P1
       RESO 37 2.5
       LABO FC=FC1 PHIC=PHIC1
       END
       eof
       #
       #  Now read these SFS from the mtz file into Amore  and generate the table
       #  Then the molecular replacement can continue as above.
       #
       amore \
       hklin $CCP4_SCR/nat3_au5_hg2_dm_cent_bigdummycell.mtz \
       table1 $CCP4_SCR/nat3_au5_hg2_dm_cent_bigdummycell.tab  \
       <<eof
       VERBOSE
       TITLE   ** packing h k l For the "model" structure factors.
       SORTFUN MODEL 100 2.5
       LABI FC=FC1  PHIC=PHIC1
       eof
       #
       eof

#. 

   ::

       #
       # Using the locked rotation function
       #
       amore 
       HKLPCK0 bgltp2peak+resolve.hkl 
       CLMN0 $CCP4_SCR/bgltp2peak+resolve_0.clmn 
       MAPOUT $CCP4_SCR/insmon_304_rot.map 
       table1 newbuiltA_MR_trial.tab 
       CLMN1 $CCP4_SCR/newbuiltA_MR_trial.clmn 
       HKLPCK1 $CCP4_SCR/insmon_304_3_hkl.tmp
       <<eof
       title Run bglt _ locked rotn_ polar 42.87 0 180
       rotfun
       generate 1  resolution 15.0 3.0  cell_model 81.366 81.096 84.366
       clmn crystal  orth 1  resolution 15.0 3.0
       clmn  model 1  resolution 15.0 3.0  sphere 24.936
       rotate CROSS  model 1  npic 20  pklim 0.5    lock 1 polar 43 0 180
       # or 
       #rotate CROSS  model 1  npic 20  pklim 0.5    lock 1 euler 0 86 180
       end
       eof

#. 

   ::

       #
       # Using a non-crystallographic translation 
       #vector to find pairs of solutions in the same orientation.
       #
       amore 
       HKLPCK0 /y/work/ccp4/dm-av-noav-sharp+dm-jtfree.hkl 
       MAPOUT /tmp/ccp4/hpce_225_tran.map 
       table1 xv11Aa_MR_trial.tab
       <<eof
        title Hpce P212121 _ test NCS vector
       trafun PTF NMOL 1 -
           resolution 91.287 3.0 -
           npic 20 -
           pklim 0.5 NCST 0.028 0 0.5
       crystal orth 1
       symmetry P212121
       SOLUTION   1 359.78 360.00 0.00 0.0000 0.0000 0.0000 20.7 55.9 28.5 25.3 1
       SOLUTION   1 179.92 0.00 0.00 0.0000 0.0000 0.0000 20.7 55.9 28.5 25.3 3
       SOLUTION   1 2.12 0.00 0.00 0.0000 0.0000 0.0000 18.3 56.6 25.5 22.0 6
       SOLUTION   1 29.44 64.50 28.89 0.0000 0.0000 0.0000 16.1 57.3 22.3 15.7 11
       SOLUTION   1 94.46 79.60 349.04 0.0000 0.0000 0.0000 14.3 57.7 18.7 16.8 16
       SOLUTION   1 359.92 85.64 181.65 0.0000 0.0000 0.0000 14.2 57.9 18.3 9.9 17
       SOLUTION   1 25.17 67.80 29.14 0.0000 0.0000 0.0000 14.2 57.7 18.7 16.5 18
       SOLUTION   1 41.98 49.77 203.98 0.0000 0.0000 0.0000 14.1 57.8 19.3 14.0 19
       eof
       amore 
       HKLPCK0 /y/work/ccp4/dm-av-noav-sharp+dm-jtfree.hkl 
       MAPOUT /tmp/ccp4/hpce_227_tran.map 
       table1 xv11Aa_MR_trial.tab
       <<eof
        title Hpce P212121 _ test NCS vector
       trafun PTF NMOL 3 -
           resolution 91.287 3.0 -
           npic 20 -
           pklim 0.5 NCST 0.028 0 0.5
       crystal orth 1
       symmetry P212121
       SOLUTION fix 1 359.78 0.00 0.00 -0.0025 0.4977 0.4797 40.6 56.5 40.2 1 81.2
       SOLUTION fix 1 359.78 0.00 0.00 0.0255 0.4977 -0.0203 40.6 56.5 40.2 1 76.9
       SOLUTION   1 359.78 360.00 0.00 0.0000 0.0000 0.0000 20.7 55.9 28.5 25.3 1
       SOLUTION fix 1 359.78 0.00 0.00 -0.0025 0.4977 0.4797 40.6 56.5 40.2 1 81.2
       SOLUTION fix 1 359.78 0.00 0.00 0.0255 0.4977 -0.0203 40.6 56.5 40.2 1 76.9
       SOLUTION   1 29.44 64.50 28.89 0.0000 0.0000 0.0000 16.1 57.3 22.3 15.7 11
       SOLUTION fix 1 29.44 64.50 28.89 0.2282 0.2346 0.3130 31.4 59.8 29.1 1 74.2
       SOLUTION fix 1 29.44 64.50 28.89 0.2562 0.2346 -0.1870 31.4 59.8 29.1 1 74.2
       SOLUTION   1 359.78 360.00 0.00 0.0000 0.0000 0.0000 20.7 55.9 28.5 25.3 1
       SOLUTION fix 1 29.44 64.50 28.89 0.2282 0.2346 0.3130 31.4 59.8 29.1 1 74.2
       SOLUTION fix 1 29.44 64.50 28.89 0.2562 0.2346 -0.1870 31.4 59.8 29.1 1 74.2
       SOLUTION   1 29.44 64.50 28.89 0.0000 0.0000 0.0000 16.1 57.3 22.3 15.7 11
       eof

AUTHORS
-------

Jorge Navaza. Adapted for CCP4 by Eleanor Dodson.

REFERENCES
----------

#. J.Navaza, *Acta Cryst.* **A50**, 157-163 (1994)
   (General reference.)
#. J.Navaza. *Acta Cryst.* **A43**, 645-653 (1987)
   (Radial quadrature instead of bessel expansion)
#. J.Navaza. *Acta Cryst.* **A46**, 619-620 (1990)
   (Stable recurrence relationship for rotation matrices.)
#. G.A.Bentley, Some applications of the phased translation function
   using calculated phases in *Molecular Replacement*, Proceedings of
   the Daresbury Study Weekend, (1992) DL/SCI/R33
#. E.E.Castellano et al., Fast Rigid-body Refinement for
   Molecular-replacement Techniques, *J. Appl. Cryst.* **25**, 281-4
   (1992).
#. J.Navaza. *Acta Cryst.* **D49**, 588-591 (1993)
#. F.L.Hirshfeld *Acta Cryst.* **A24**, 301-311 (1968)

SEE ALSO
--------

`almn <almn.html>`__, `ecalc <ecalc.html>`__, `lsqkab <lsqkab.html>`__,
`npo <npo.html>`__, `pdbset <pdbset.html>`__, `rfcorr <rfcorr.html>`__
