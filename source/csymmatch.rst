CSYMMATCH (CCP4: Supported Program)
===================================

NAME
----

**csymmatch** - Apply symmetry and cell shifts to each chain in 'pdbin'
to obtain the best match to 'pdbin-ref'

SYNOPSIS
--------

| **csymmatch** **-pdbin-ref** *filename* **-pdbin** *filename*
  **-pdbout** *filename* **-connectivity-radius** *radius/A*
  **-origin-hand** **[-stdin]**
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

'csymmatch' will move chains in file 'pdbin' to match as far as possible
chains in file 'pdbin-ref', using symmetry operations and allowed origin
shifts to generate the best fit. The shifted coordinates are written out
in 'pdbout'.

Note that this is **not** a structural superposition. 'csymmatch' only
uses a fixed set of symmetry-related transformations.

It makes an intelligent guess based on chain ID and proximity as to what
is a contiguous unit in the file, and moves on that basis. Separate
chains are always moved separately. Monomers within a chain which do not
have close contiguity to their neighbours - e.g. waters, broken chains -
are also moved separately.

INPUT/OUTPUT FILES
------------------

-pdbin-ref

Input PDB file containing the reference structure.

-pdbin

Input PDB file containing the coordinates to be shifted.

-pdbout

Output PDB file containing the shifted coordinates.

KEYWORDED INPUT
---------------

See `Note on keyword input <#notekeywords>`__.

-connectivity-radius *radius*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Default: 2.0 A
| Radius (in Angstrom) used to determine which fragments form a
  contiguous unit, to be moved together.

-origin-hand
~~~~~~~~~~~~

Also try all possible origin shifts, and changes of hand.

Note on keyword input:
^^^^^^^^^^^^^^^^^^^^^^

Keywords may appear on the command line, or by specifying the '-stdin'
flag, on standard input. In the latter case, one keyword is given per
line and the '-' is optional, and the rest of the line is the argument
of that keyword if required, so quoting is not used in this case. End
input with <Ctrl-D> (END is not recognised).

Program Output
--------------

The program reports any change of hand, or origin shift used (which is
applied to the entire contents of 'pdbin'). It then outputs the symmetry
operation and lattice shift used for each chain identified in 'pdbin'.

It also outputs a normalised score for each positioned chain, which is
the average map value at the atoms of that chain. The map in question is
calculated from the reference structure. A well positioned chain will
tend to overlap the reference map, and lead to a higher score (closer to
1.0).

Problems
--------

One limitation: Without `-origin-hand <#origin-hand>`__ it doesn't try
origin shifts. With `-origin-hand <#origin-hand>`__ it tries all
possible origin shifts, not just those consistent with the spacegroup
symmetry. Normally the symmetry of the search target wins out and you
get an exact answer, but with very poor models it can occasionally give
garbage.

AUTHOR
------

Kevin Cowtan, York.

SEE ALSO
--------

`reforigin <reforigin.html>`__, solution\_check (Balbes)
