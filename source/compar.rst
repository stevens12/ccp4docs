COMPAR (CCP4: Unsupported Program)
==================================

NAME
----

**compar** - comparison of coordinates.

SYNOPSIS
--------

| **compar XYZIN1** *foo\_in1.pdb* **XYZIN2** *foo\_in2.pdb* [
  **XYZIN3** *foo\_in3.pdb* ] **RMSTAB** *foo\_out.rms*
| [Input cards]

DESCRIPTION
-----------

Mini prog to compar 2 or 3 sets of atomic coordinates.

INPUT AND OUTPUT FILES
----------------------

Input
~~~~~

2 or 3 coordinate files in PDB format assigned to XYZIN1, XYZIN2, [
XYZIN3 ].

Output
~~~~~~

A file assigned to RMSTAB which can be used for SQUID. This file lists,
for each residue, the rms difference in coordinates and in B-factor for
main chain atoms and for side chain atoms.

INPUT
-----

Input is not keyworded. The following 3 lines of input are expected:

#. <title>
#. <Nsets> - number of coordinate files for comparison (2 or 3).
#. <Del(xyz)> <DEl(b) > - coordinates are monitored if Del(xyz) or
   DEl(b) are greater than these values.

For example:

::


        Comparison before and after refinement.
        2
        3.0 20.0

EXAMPLES
--------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`compar.exam <../examples/unix/runnable/compar.exam>`__
