LSQKAB (CCP4: Supported Program)
================================

NAME
----

**lsqkab** - Work out and apply various transformations to coordinate
files.

SYNOPSIS
--------

| **lsqkab xyzinm** *tomatch.brk* **xyzinf** *reference.brk* **xyzout**
  *matched.brk*
| **lsqkab xyzinm** *totransform.brk* **xyzout** *transformed.brk*

[`Keyworded input <#keywords>`__]

| **Note:** XYZINM has the aliases XYZIN2, WORKCD and XYZINW
| XYZINF has the aliases XYZIN1, REFRCD and XYZINR
| XYZOUT has the alias LSQOP

DESCRIPTION
-----------

Primary use
~~~~~~~~~~~

Optimises the fit of a subset of atomic coordinates from one file
(assigned to XYZINM) to the same subset of another file (assigned to
XYZINF). The "working" coordinates in XYZINM are to be moved; the
"reference" coordinates in XYZINF are fixed. The program assumes both
sets of coordinates are in the PDB format. If there are two or more
conformations, the first (labelled A) is chosen for comparison.

The subset can be defined either by residue numbers with options for
main chain only, side chain only or CAs, or by the position of the atom
in the file.

The centroid of the moving subset is first shifted to that of the fixed
set and then a rotation matrix is derived using a method suggested by
Kabsch (`reference [1] <#reference1>`__). This rotation matrix is
defined relative to the orthogonal coordinate system.

The whole of the moving coordinate set can be transformed and output in
an orientation matching that of the fixed reference set. The output file
assigned to XYZOUT. You can give CELL dimensions to set up the CRYST1
and SCALEi lines, otherwise they will be copied from the XYZINM
coordinate file.

There are various output options described on the input cards.

Secondary purpose - much better done in PDBSET
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The rotating coordinate set contained in file XYZINM can be transformed
by a given rotation matrix and translation (see `ROTATE <#rotate>`__ and
`TRANSLATE <#translate>`__ keywords), and then output in a new
orientation to the file XYZOUT.

KEYWORDED INPUT
---------------

The keywords are: `FIT <#fit>`__, `MATCH <#match>`__,
`OUTPUT <#output>`__, `RADIUS <#radius>`__, `ROTATE <#rotate>`__,
`TITLE <#title>`__, `TRANSLATE <#translate>`__, `CELL <#cell>`__,
`END <#end>`__.

FIT [ ATOM \| RESIDUE ] <arguments>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Describes the rotating coordinate subset taken from XYZINM. Keyword must
be followed by ATOM (alias WATOM) or RESIDUE (alias WRESIDUE).

ATOM <atom1> TO <atom2>
    the atom range to fit.
RESIDUE [ CA \| MAIN \| SIDE \| ALL ] <res1> TO <res2> [ CHAIN <chnam> ]
    atom type used for fitting (default ALL), followed by residue range.
    CHAIN (alias WCHAIN) describes the rotating chain name, if needed

For fitting the next card must be `MATCH <#match>`__.

MATCH <n1> TO <n2> [ CHAIN <chnam> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Describes the reference coordinate subset taken from XYZINF.

<n1>, <n2>
    two numbers to define atom or residue range (interpretation depends
    on previous `FIT <#fit>`__ keyword; ATOM or RESIDUE subkeywords can
    be included but are actually ignored).
CHAIN <chnam>
    (alias RCHAIN) the reference chain name if needed.

CELL a b c [alpha beta gamma ncode]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default alpha beta gamma 90.0 90.0 90.0. Default ncode 1.

Read cell dimensions and prepare CRYST1 & SCALE header records for
XYZOUT. These will replace any cell dimensions read from th input
working coordinates. The fractional translation vector will be given
relative to these cell dimensions.

ncode selects the orthogonalization matrix to be used.

::

         Code :-
            = 1  axes along a, c* x a, c*  (Brookhaven standard, default)
            = 2  axes along b, a* x b, a*
            = 3  axes along c, b* x c, b*
            = 4  axes along a+b, c* x (a+b), c*
            = 5  axes along a*, c x a*, c       ( Rollett )
            = 6  axes along a, b*, a x b*
            = 7  axes along a*, b, a* x b   (TNT convention)

OUTPUT [ XYZ / RMS / DELTAS ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Must be followed by XYZ and/or RMS and/or DELTAS.

XYZ
    Output a PDB file of coordinates to the file assigned to xyzout. The
    cell is assumed to be that of the ROTATING coordinates.
RMS
    Output a table of rms distances between atom pairs for the main
    chain and side chain of each selected residue. (If you are only
    fitting CA or main chain atoms, the side chain RMS will be zero. If
    the "residue" has no peptide unit, the main chain RMS will be zero).
    It can be input into a plotting program such as SQUID.
DELTAS
    A list of ALL differences between atom pairs is written to a file
    assigned to DELTAS.

RADIUS <radchk> [ <Xc> <Yc> <Zc> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Use atoms in a sphere for fitting.

radchk: All atoms more than radchk Angstroms from the reference centroid
are excluded.

radchk Xc Yc Zc: All atoms more than radchk Angstroms from the reference
point Xc Yc Zc (in As) are excluded

ROTATE [ EULERIAN / POLAR / DCS / MATRIX ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

must be followed by EULERIAN or POLAR or DCS (direction cosines) or
MATRix. All generate a matrix for rotation of XYZINM.

::

       EULERIAN    alpha beta gamma
       POLAR       omega phi kappa
       DCS         dc1 dc2 dc3  kappa
       MATRIX      nine elements: m11 m12 m13  m21 m22 m23  m31 m32 m33

TRANSLATE <t1> <t2> <t3>
~~~~~~~~~~~~~~~~~~~~~~~~

Must be followed by t1 t2 t3, the translation vector in Ås along PDB
axes.

| Alternative:
| TRANslate FRAC <t1> <t2> <t3>
| translation vector in fractions of ROTATING cell edge.

| Input coordinates are rotated by matrix [R] then translated by vector
  tr.
| [Xop] = [R] \* [Xip] + [tr].

TITLE <title>
~~~~~~~~~~~~~

END
~~~

Terminate input and begin program.

REQUIRED FILES
--------------

#. You will always want a set of coordinates to rotate. Assign a PDB
   file to logical name XYZINM.
#. If you want to FIT/MATCH, you need a reference fixed set of
   coordinates. Assign a PDB file to logical name XYZINF.
#. If you have requested xyz output, open coordinate file with logical
   name XYZOUT.
#. If you have requested rms output, open file with logical name RMSTAB.
#. If you have requested deltas output, open file with logical name
   DELTAS.

KNOWN PROBLEMS
--------------

There has been a report that in some cases using the FIT RESIDUE CA
options, the fitting has been \`\`reversed'' e.g. the commands

::

    FIT RESIDUE CA 7 TO 12
    MATCH 7 TO 12

result in the CA atom of residue 12 in the rotating set is fitted to the
CA of residue 7 in the reference set and so on.

The reported workaround is to split the FIT/MATCH commands, for example:

::

    FIT RESIDUE CA 7 TO 9
    MATCH 7 TO 9
    FIT RESIDUE CA 10 TO 12
    MATCH 10 TO 12

which apparently works in most cases.

*(Thanks to Toby Galbraith)*

REFERENCE
---------

#. Kabsch W. *Acta. Cryst.* **A32** 922-923 (1976).

EXAMPLES
--------

UNIX example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`lsqkab.exam <../examples/unix/runnable/lsqkab.exam>`__ (Examples of
fitting and applying transformations).

Very complicated match
~~~~~~~~~~~~~~~~~~~~~~

::

    lsqkab xyzinf ../data/takaxp_model8_9.pdb 
        xyzinm ../data/takap21.pdb xyzout {$SCRATCH}lsq_cat_residues.pdb 
        DELTAS {$SCRATCH}lsq_cat_residues.delta << END-lsqkab 
    title matching catalytic residues of 3A to 2.1A taka
    output  XYZ
    output  deltas
    fit RESIDU side 155 TO 156 CHAIN A 
    MATCH RESIDU 155 TO 156 CHAIN A
    fit RESIDU side 210 TO 210 CHAIN A 
    MATCH RESIDU 210 TO 210 CHAIN A
    fit RESIDU side 206 TO 206 CHAIN A 
    MATCH RESIDU 206 TO 206 CHAIN A
    fit RESIDU side 122 TO 122 CHAIN A 
    MATCH RESIDU 122 TO 122 CHAIN A
    fit RESIDU side 83 TO 83 CHAIN A 
    MATCH RESIDU 83 TO 83 CHAIN A
    fit RESIDU side 168 TO 168 CHAIN A 
    MATCH RESIDU 168 TO 168 CHAIN A
    fit RESIDU side 74 TO 74 CHAIN A 
    MATCH RESIDU 74 TO 74 CHAIN A
    fit RESIDU side 35 TO 35 CHAIN A 
    MATCH RESIDU 35 TO 35 CHAIN A
    fit RESIDU side 79 TO 79 CHAIN A 
    MATCH RESIDU 79 TO 79 CHAIN A
    fit RESIDU side 340 TO 340 CHAIN A 
    MATCH RESIDU 340 TO 340 CHAIN A
    fit RESIDU side 344 TO 344 CHAIN A 
    MATCH RESIDU 344 TO 344 CHAIN A
    fit RESIDU side 296 TO 297 CHAIN A 
    MATCH RESIDU 296 TO 297 CHAIN A
    fit RESIDU side 230 TO 233 CHAIN A 
    MATCH RESIDU 230 TO 233 CHAIN A
    fit RESIDU side 209 TO 209 CHAIN A 
    MATCH RESIDU 209 TO 209 CHAIN A
    end
    END-lsqkab
