`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmaplib\_f.c File Reference
===========================

Fortran API for input, output and manipulation of CCP4 map files.
`More... <#_details>`__

| ``#include <string.h>``
| ``#include <stdio.h>``
| ``#include <fcntl.h>``
| ``#include "cmaplib_f.h"``
| ``#include "cmap_errno.h"``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_parser.h"``
| ``#include "csymlib.h"``
| ``#include "ccp4_general.h"``

| 

Functions
---------

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a12>`__ (MWRHDL, mwrhdl,(int \*iunit,
const fpstr mapnam, const fpstr title, int \*nsecs, int iuvw[3], int
mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float
cell[6], int \*lspgrp, int \*lmode, int mapnam\_len, int
title\_len),(int \*iunit, const fpstr mapnam, const fpstr title, int
\*nsecs, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int
\*nv1, int \*nv2, float cell[6], int \*lspgrp, int \*lmode),(int
\*iunit, const fpstr mapnam, int mapnam\_len, const fpstr title, int
title\_len, int \*nsecs, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1,
int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int
\*lmode))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_OPEN\_HEADER\_BY\_NAME,
ccp4\_map\_write\_open\_header\_by\_name,(int \*iunit, const fpstr
mapnam, const fpstr title, int \*nsecs, int iuvw[3], int mxyz[3], int
\*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6], int
\*lspgrp, int \*lmode, int mapnam\_len, int title\_len),(int \*iunit,
const fpstr mapnam, const fpstr title, int \*nsecs, int iuvw[3], int
mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float
cell[6], int \*lspgrp, int \*lmode),(int \*iunit, const fpstr mapnam,
int mapnam\_len, const fpstr title, int title\_len, int \*nsecs, int
iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int
\*nv2, float cell[6], int \*lspgrp, int \*lmode))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a14>`__ (MWRHDR, mwrhdr,(int \*iunit,
const fpstr title, int \*nsecs, int iuvw[3], int mxyz[3], int \*nw1, int
\*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int
\*lmode, int title\_len),(int \*iunit, const fpstr title, int \*nsecs,
int iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1,
int \*nv2, float cell[6], int \*lspgrp, int \*lmode),(int \*iunit, const
fpstr title, int title\_len, int \*nsecs, int iuvw[3], int mxyz[3], int
\*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6], int
\*lspgrp, int \*lmode))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_OPEN\_HEADER\_BY\_ID,
ccp4\_map\_write\_open\_header\_by\_id,(int \*iunit, const fpstr title,
int \*nsecs, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2,
int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int \*lmode, int
title\_len),(int \*iunit, const fpstr title, int \*nsecs, int iuvw[3],
int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2,
float cell[6], int \*lspgrp, int \*lmode),(int \*iunit, const fpstr
title, int title\_len, int \*nsecs, int iuvw[3], int mxyz[3], int \*nw1,
int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp,
int \*lmode))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a16>`__ (MRDHDS, mrdhds,(int \*iunit,
const fpstr mapnam, fpstr title, int \*nsec, int iuvw[3], int mxyz[3],
int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6],
int \*lspgrp, int \*lmode, float \*rhmin, float \*rhmax, float \*rhmean,
float \*rhrms, int \*ifail, int \*iprint, int mapnam\_len, int
title\_len),(int \*iunit, const fpstr mapnam, fpstr title, int \*nsec,
int iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1,
int \*nv2, float cell[6], int \*lspgrp, int \*lmode, float \*rhmin,
float \*rhmax, float \*rhmean, float \*rhrms, int \*ifail, int
\*iprint),(int \*iunit, const fpstr mapnam, int mapnam\_len, fpstr
title, int title\_len, int \*nsec, int iuvw[3], int mxyz[3], int \*nw1,
int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp,
int \*lmode, float \*rhmin, float \*rhmax, float \*rhmean, float
\*rhrms, int \*ifail, int \*iprint))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_READ\_OPEN\_HEADER\_CHECK,
ccp4\_map\_read\_open\_header\_check,(int \*iunit, const fpstr mapnam,
fpstr title, int \*nsec, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1,
int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int
\*lmode, float \*rhmin, float \*rhmax, float \*rhmean, float \*rhrms,
int \*ifail, int \*iprint, int mapnam\_len, int title\_len),(int
\*iunit, const fpstr mapnam, fpstr title, int \*nsec, int iuvw[3], int
mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float
cell[6], int \*lspgrp, int \*lmode, float \*rhmin, float \*rhmax, float
\*rhmean, float \*rhrms, int \*ifail, int \*iprint),(int \*iunit, const
fpstr mapnam, int mapnam\_len, fpstr title, int title\_len, int \*nsec,
int iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1,
int \*nv2, float cell[6], int \*lspgrp, int \*lmode, float \*rhmin,
float \*rhmax, float \*rhmean, float \*rhrms, int \*ifail, int
\*iprint))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a18>`__ (MRDHDR, mrdhdr,(int \*iunit,
const fpstr mapnam, fpstr title, int \*nsec, int iuvw[3], int mxyz[3],
int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6],
int \*lspgrp, int \*lmode, float \*rhmin, float \*rhmax, float \*rhmean,
float \*rhrms, int mapnam\_len, int title\_len),(int \*iunit, const
fpstr mapnam, fpstr title, int \*nsec, int iuvw[3], int mxyz[3], int
\*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6], int
\*lspgrp, int \*lmode, float \*rhmin, float \*rhmax, float \*rhmean,
float \*rhrms),(int \*iunit, const fpstr mapnam, int mapnam\_len, fpstr
title, int title\_len, int \*nsec, int iuvw[3], int mxyz[3], int \*nw1,
int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp,
int \*lmode, float \*rhmin, float \*rhmax, float \*rhmean, float
\*rhrms))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_READ\_OPEN\_HEADER,
ccp4\_map\_read\_open\_header,(int \*iunit, const fpstr mapnam, fpstr
title, int \*nsec, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int
\*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int \*lmode,
float \*rhmin, float \*rhmax, float \*rhmean, float \*rhrms, int
mapnam\_len, int title\_len),(int \*iunit, const fpstr mapnam, fpstr
title, int \*nsec, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int
\*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int \*lmode,
float \*rhmin, float \*rhmax, float \*rhmean, float \*rhrms),(int
\*iunit, const fpstr mapnam, int mapnam\_len, fpstr title, int
title\_len, int \*nsec, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1,
int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int
\*lmode, float \*rhmin, float \*rhmax, float \*rhmean, float \*rhrms))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a20>`__ (MWCLOSE, mwclose,(int
\*iunit),(int \*iunit),(int \*iunit))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_CLOSE\_AUTO,
ccp4\_map\_write\_close\_auto,(int \*iunit),(int \*iunit),(int \*iunit))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a22>`__ (MCLOSE, mclose,(int \*iunit,
float \*min, float \*max, float \*mean, float \*rms),(int \*iunit, float
\*min, float \*max, float \*mean, float \*rms),(int \*iunit, float
\*min, float \*max, float \*mean, float \*rms))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_CLOSE\_USER\_SUM,
ccp4\_map\_write\_close\_user\_sum,(int \*iunit, float \*min, float
\*max, float \*mean, float \*rms),(int \*iunit, float \*min, float
\*max, float \*mean, float \*rms),(int \*iunit, float \*min, float
\*max, float \*mean, float \*rms))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a24>`__ (MSYWRT, msywrt,(int \*iunit,
int \*nsym, float \*rot),(int \*iunit, int \*nsym, float \*rot),(int
\*iunit, int \*nsym, float \*rot))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_SYMM\_MATRIX,
ccp4\_map\_write\_symm\_matrix,(int \*iunit, int \*nsym, float
\*rot),(int \*iunit, int \*nsym, float \*rot),(int \*iunit, int \*nsym,
float \*rot))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a26>`__ (MSYPUT, msyput,(int \*sunit,
int \*spacegroup, int \*iunit),(int \*sunit, int \*spacegroup, int
\*iunit),(int \*sunit, int \*spacegroup, int \*iunit))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_SPGNAME,
ccp4\_map\_write\_spgname,(int \*sunit, int \*spacegroup, int
\*iunit),(int \*sunit, int \*spacegroup, int \*iunit),(int \*sunit, int
\*spacegroup, int \*iunit))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a28>`__ (MSPEW, mspew,(int \*iunit,
float \*section),(int \*iunit, float \*section),(int \*iunit, float
\*section))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_ALL\_SECTION,
ccp4\_map\_write\_all\_section,(int \*iunit, float \*section),(int
\*iunit, float \*section),(int \*iunit, float \*section))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a30>`__ (MGULP, mgulp,(int \*iunit,
float \*section, int \*ier),(int \*iunit, float \*section, int
\*ier),(int \*iunit, float \*section, int \*ier))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_READ\_WHOLE\_SECTION\_AS\_MODE,
ccp4\_map\_read\_whole\_section\_as\_mode,(int \*iunit, float \*section,
int \*ier),(int \*iunit, float \*section, int \*ier),(int \*iunit, float
\*section, int \*ier))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a32>`__ (MWRSEC, mwrsec,(int \*iunit,
float \*section, int \*mu, int \*mv, int \*iu1, int \*iu2, int \*iv1,
int \*iv2),(int \*iunit, float \*section, int \*mu, int \*mv, int \*iu1,
int \*iu2, int \*iv1, int \*iv2),(int \*iunit, float \*section, int
\*mu, int \*mv, int \*iu1, int \*iu2, int \*iv1, int \*iv2))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_PART\_SECTION,
ccp4\_map\_write\_part\_section,(int \*iunit, float \*section, int \*mu,
int \*mv, int \*iu1, int \*iu2, int \*iv1, int \*iv2),(int \*iunit,
float \*section, int \*mu, int \*mv, int \*iu1, int \*iu2, int \*iv1,
int \*iv2),(int \*iunit, float \*section, int \*mu, int \*mv, int \*iu1,
int \*iu2, int \*iv1, int \*iv2))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a34>`__ (MRDLIN, mrdlin,(int \*iunit,
float \*line, int \*ier),(int \*iunit, float \*line, int \*ier),(int
\*iunit, float \*line, int \*ier))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_READ\_LINE\_AS\_MODE,
ccp4\_map\_read\_line\_as\_mode,(int \*iunit, float \*line, int
\*ier),(int \*iunit, float \*line, int \*ier),(int \*iunit, float
\*line, int \*ier))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a36>`__ (MPOSN, mposn,(int \*iunit,
int \*sec),(int \*iunit, int \*sec),(int \*iunit, int \*sec))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_READ\_POSITION\_SELECTION,
ccp4\_map\_read\_position\_selection,(int \*iunit, int \*sec),(int
\*iunit, int \*sec),(int \*iunit, int \*sec))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a38>`__ (MSYCPY, msycpy,(int \*iunit,
int \*ounit),(int \*iunit, int \*ounit),(int \*iunit, int \*ounit))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_COPY\_SYMMETRY,
ccp4\_map\_copy\_symmetry,(int \*iunit, int \*ounit),(int \*iunit, int
\*ounit),(int \*iunit, int \*ounit))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a40>`__ (MTTREP, mttrep,(const fpstr
label, int \*posn, int label\_len),(const fpstr label, int
\*posn),(const fpstr label, int label\_len, int \*posn))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_REPLACE\_TITLE,
ccp4\_map\_write\_replace\_title,(const fpstr label, int \*posn, int
label\_len),(const fpstr label, int \*posn),(const fpstr label, int
label\_len, int \*posn))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a42>`__ (MTTCPY, mttcpy,(const fpstr
label, int label\_len),(const fpstr label),(const fpstr label, int
label\_len))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_COPY\_TITLE, ccp4\_map\_copy\_title,(const
fpstr label, int label\_len),(const fpstr label),(const fpstr label, int
label\_len))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a44>`__ (MSKPUT, mskput,(float
\*skew\_rot, float \*skew\_trans),(float \*skew\_rot, float
\*skew\_trans),(float \*skew\_rot, float \*skew\_trans))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_WRITE\_SKEW\_INFO,
ccp4\_map\_write\_skew\_info,(float \*skew\_rot, float
\*skew\_trans),(float \*skew\_rot, float \*skew\_trans),(float
\*skew\_rot, float \*skew\_trans))

 

`FORTRAN\_FUN <cmaplib__f_8c.html#a46>`__ (int, MSKGET, mskget,(float
\*mask\_rot, float \*mask\_trans),(float \*mask\_rot, float
\*mask\_trans),(float \*mask\_rot, float \*mask\_trans))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a47>`__
(CCP4\_MAP\_WRITE\_SECTION\_HEADER,
ccp4\_map\_write\_section\_header,(int \*iunit, float \*section, const
fpstr local\_hdr, int local\_hdr\_len),(int \*iunit, float \*section,
const fpstr local\_hdr),(int \*iunit, float \*section, const fpstr
local\_hdr, int local\_hdr\_len))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a48>`__
(CCP4\_MAP\_READ\_SECTION\_HEADER, ccp4\_map\_read\_section\_header,(int
\*iunit, float \*section, const fpstr local\_hdr, int \*ier, int
local\_hdr\_len),(int \*iunit, float \*section, const fpstr local\_hdr,
int \*ier),(int \*iunit, float \*section, const fpstr local\_hdr, int
local\_hdr\_len, int \*ier))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_SET\_LOCAL\_HEADER,
ccp4\_map\_set\_local\_header,(int \*iunit, int \*size),(int \*iunit,
int \*size),(int \*iunit, int \*size))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a50>`__ (MWFNAM, mwfnam,(fpstr
logname, int logname\_len),(fpstr logname),(fpstr logname, int
logname\_len))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_GET\_LAST\_WRITE\_FILENAME,
ccp4\_map\_get\_last\_write\_filename,(fpstr logname, int
logname\_len),(fpstr logname),(fpstr logname, int logname\_len))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a52>`__ (MSTMST, mstmst,(int
\*map),(int \*map),(int \*map))

 

`FORTRAN\_SUBR <cmaplib__f_8c.html#a53>`__ (MODECV, modecv,(float
\*output, float \*input, int \*number, int \*mode),(float \*output,
float \*input, int \*number, int \*mode),(float \*output, float \*input,
int \*number, int \*mode))

  

**FORTRAN\_SUBR** (CCP4\_MAP\_MODE\_TO\_REAL,
ccp4\_map\_mode\_to\_real,(float \*output, float \*input, int \*number,
int \*mode),(float \*output, float \*input, int \*number, int
\*mode),(float \*output, float \*input, int \*number, int \*mode))

  

**FORTRAN\_FUN** (int, NBYTXX, nbytxx,(int \*nwords),(int \*nwords),(int
\*nwords))

--------------

Detailed Description
--------------------

Fortran API for input, output and manipulation of CCP4 map files.

 **Author:**
    Charles Ballard

--------------

Function Documentation
----------------------

FORTRAN\_FUN

( 

int 

 ,

MSKGET 

 ,

mskget 

 ,

(float \*mask\_rot, float \*mask\_trans) 

 ,

(float \*mask\_rot, float \*mask\_trans) 

 ,

(float \*mask\_rot, float \*mask\_trans) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mskget: @skew\_rot: (I) skew         |
|                                      | rotation matrix S[3][3]; S11, S12,   |
|                                      | etc @skew\_trans (I) skew            |
|                                      | translation vector T[3] returns:     |
|                                      | value of skew\_flag.                 |
|                                      |                                      |
|                                      | description: Get skew translation    |
|                                      | from input header struct. Skew       |
|                                      | transformation form orthogonal atom  |
|                                      | fram to orthogonal map frame:        |
|                                      | Xo(map) = S\*(Xo(atoms)-T)           |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MODECV 

 ,

modecv 

 ,

(float \*output, float \*input, int \*number, int \*mode) 

 ,

(float \*output, float \*input, int \*number, int \*mode) 

 ,

(float \*output, float \*input, int \*number, int \*mode) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | modecv: @output: (O) output array    |
|                                      | contain translated reals @input: (I) |
|                                      | pre-translation array (read from     |
|                                      | file) @number: (I) number of         |
|                                      | elements to be translated @mode: (I) |
|                                      | storage mode, = 0 FORTRAN character  |
|                                      | (uint8) = 1 FORTRAN I\*2 (uint16) =  |
|                                      | 3 complex short = 4 complex real     |
|                                      | (uint32)                             |
|                                      |                                      |
|                                      | description: Convert @number items   |
|                                      | form @input (mode @mode) to float in |
|                                      | @output.                             |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MSTMST 

 ,

mstmst 

 ,

(int \*map) 

 ,

(int \*map) 

 ,

(int \*map) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mstmst: @map: (O) "MAP "             |
|                                      |                                      |
|                                      | description: Set integer MAP to      |
|                                      | string "MAP "                        |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MWFNAM 

 ,

mwfnam 

 ,

(fpstr logname, int logname\_len) 

 ,

(fpstr logname) 

 ,

(fpstr logname, int logname\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mwfnam: @logname: (O) logical name   |
|                                      | of last open file                    |
|                                      |                                      |
|                                      | description: Returns the logical     |
|                                      | name for the last written to file    |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

CCP4\_MAP\_READ\_SECTION\_HEADER 

 ,

ccp4\_map\_read\_section\_header 

 ,

(int \*iunit, float \*section, const fpstr local\_hdr, int \*ier, int
local\_hdr\_len) 

 ,

(int \*iunit, float \*section, const fpstr local\_hdr, int \*ier) 

 ,

(int \*iunit, float \*section, const fpstr local\_hdr, int
local\_hdr\_len, int \*ier) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_map\_read\_section\_header:    |
|                                      | @iunit: (I) map stream number        |
|                                      | @section: (O) array containing       |
|                                      | section of map data read from file   |
|                                      | @local\_hdr: (O) local header        |
|                                      | information as CHAR array @ier: (O)  |
|                                      | error code =0, OK =non-zero, error   |
|                                      | or end-of-file                       |
|                                      |                                      |
|                                      | description: Read next whole map     |
|                                      | section from stream @iunit to array  |
|                                      | @section. @section is returned in    |
|                                      | the same mode as in the file, no     |
|                                      | data conversion is performed.        |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

CCP4\_MAP\_WRITE\_SECTION\_HEADER 

 ,

ccp4\_map\_write\_section\_header 

 ,

(int \*iunit, float \*section, const fpstr local\_hdr, int
local\_hdr\_len) 

 ,

(int \*iunit, float \*section, const fpstr local\_hdr) 

 ,

(int \*iunit, float \*section, const fpstr local\_hdr, int
local\_hdr\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_map\_write\_section\_header:   |
|                                      | @iunit: (I) stream number @section:  |
|                                      | (I) array holding the map section.   |
|                                      | @local\_hdr: (I) local header        |
|                                      | information                          |
|                                      |                                      |
|                                      | description: Write whole map section |
|                                      | to stream @iunit. This routine is    |
|                                      | only suitable if the whole map       |
|                                      | section is to be written.            |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MSKPUT 

 ,

mskput 

 ,

(float \*skew\_rot, float \*skew\_trans) 

 ,

(float \*skew\_rot, float \*skew\_trans) 

 ,

(float \*skew\_rot, float \*skew\_trans) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mskput: @skew\_rot: (I) skew         |
|                                      | rotation matrix S[3][3];S11, S12,    |
|                                      | etc @skew\_trans (I) skew            |
|                                      | translation vector T[3]              |
|                                      |                                      |
|                                      | description: Put skew transformation |
|                                      | in header struct of current output   |
|                                      | mapfile. Note: Phil Evans (9/93),    |
|                                      | should probably not use this         |
|                                      | routine.                             |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MTTCPY 

 ,

mttcpy 

 ,

(const fpstr label, int label\_len) 

 ,

(const fpstr label) 

 ,

(const fpstr label, int label\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mttcpy: @label: (I) new label        |
|                                      |                                      |
|                                      | description: Copy all labels from    |
|                                      | the current input map to the header  |
|                                      | (struct) of the current output       |
|                                      | mapfile, adding @label to the end.   |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MTTREP 

 ,

mttrep 

 ,

(const fpstr label, int \*posn, int label\_len) 

 ,

(const fpstr label, int \*posn) 

 ,

(const fpstr label, int label\_len, int \*posn) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mttrep: @label: (I) new label (max.  |
|                                      | length 80 chars) @posn: (I) position |
|                                      | for label in label array.            |
|                                      |                                      |
|                                      | description: Replace label at @posn  |
|                                      | in header of current output mapfile. |
|                                      | Maximum of 10 labels, is exceeded    |
|                                      | overwrites final label.              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MSYCPY 

 ,

msycpy 

 ,

(int \*iunit, int \*ounit) 

 ,

(int \*iunit, int \*ounit) 

 ,

(int \*iunit, int \*ounit) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | msycpy: @iunit: (I) input map stream |
|                                      | number @ounit: (I) output map stream |
|                                      | number                               |
|                                      |                                      |
|                                      | description: Copy symmetry data from |
|                                      | file on iostream @iunit to the file  |
|                                      | on iostream @iunit (after header     |
|                                      | initialisation calls).               |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MPOSN 

 ,

mposn 

 ,

(int \*iunit, int \*sec) 

 ,

(int \*iunit, int \*sec) 

 ,

(int \*iunit, int \*sec) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mposn: @iunit: (I) stream number     |
|                                      | @sec: (I) position the input map     |
|                                      | before @sec                          |
|                                      |                                      |
|                                      | description: Sets the position in    |
|                                      | the map file so that the next        |
|                                      | section to be read is @sec. Note:    |
|                                      | the section number is input.         |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MRDLIN 

 ,

mrdlin 

 ,

(int \*iunit, float \*line, int \*ier) 

 ,

(int \*iunit, float \*line, int \*ier) 

 ,

(int \*iunit, float \*line, int \*ier) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mrdlin: @iunit: (I) stream number    |
|                                      | @line: (O) array to contain the line |
|                                      | of data from the map @ier: (O) error |
|                                      | flag =0, OK =non-zero, error or EOF. |
|                                      |                                      |
|                                      | description: Read the next line of   |
|                                      | the map from stream @iunit into      |
|                                      | array @line. returned in the same    |
|                                      | mode as in file, no data conversion  |
|                                      | is performed.                        |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MWRSEC 

 ,

mwrsec 

 ,

(int \*iunit, float \*section, int \*mu, int \*mv, int \*iu1, int \*iu2,
int \*iv1, int \*iv2) 

 ,

(int \*iunit, float \*section, int \*mu, int \*mv, int \*iu1, int \*iu2,
int \*iv1, int \*iv2) 

 ,

(int \*iunit, float \*section, int \*mu, int \*mv, int \*iu1, int \*iu2,
int \*iv1, int \*iv2) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mwrsec: @iunit: (I) map stream       |
|                                      | number @section: (I) array holding   |
|                                      | the map section @mu: (I) the number  |
|                                      | of points along the whole fast axis  |
|                                      | @mv: (I) the number of points along  |
|                                      | the whole medium axis @iu1: (I) the  |
|                                      | starting array index along the fast  |
|                                      | axis @iu2: (I) the final array index |
|                                      | along the fast axis @iv1: (I) the    |
|                                      | starting array index along the       |
|                                      | medium axis @iv2: (I) the final      |
|                                      | array index along the medium axis    |
|                                      |                                      |
|                                      | description: Write part of map       |
|                                      | section X(MU,MV) to stream @iunit    |
|                                      |                                      |
|                                      | Note: the C routine this calls uses  |
|                                      | the C/C++ convention of [) rather    |
|                                      | than []. Therefore the the @iu1 and  |
|                                      | @iv1 values are decremented by 1,    |
|                                      | and the @iu2 and @iv2 are unchanged  |
|                                      | as paramenters (C uses 0 offset for  |
|                                      | arrays). Also note that the          |
|                                      | dimensions are fall the storage      |
|                                      | array, not the map.                  |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MGULP 

 ,

mgulp 

 ,

(int \*iunit, float \*section, int \*ier) 

 ,

(int \*iunit, float \*section, int \*ier) 

 ,

(int \*iunit, float \*section, int \*ier) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mgulp: @iunit: (I) map stream number |
|                                      | @section: (O) array containing       |
|                                      | section of map data read from file   |
|                                      | @ier: (O) error code =0, OK          |
|                                      | =non-zero, error or end-of-file      |
|                                      |                                      |
|                                      | description: Read next whole map     |
|                                      | section from stream @iunit to array  |
|                                      | @section. @section is returned in    |
|                                      | the same mode as in the file, no     |
|                                      | data conversion is performed.        |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MSPEW 

 ,

mspew 

 ,

(int \*iunit, float \*section) 

 ,

(int \*iunit, float \*section) 

 ,

(int \*iunit, float \*section) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mspew: @iunit: (I) stream number     |
|                                      | @section: (I) array holding the map  |
|                                      | section.                             |
|                                      |                                      |
|                                      | description: Write whole map section |
|                                      | to stream @iunit. This routine is    |
|                                      | only suitable if the whole map       |
|                                      | section is to be written.            |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MSYPUT 

 ,

msyput 

 ,

(int \*sunit, int \*spacegroup, int \*iunit) 

 ,

(int \*sunit, int \*spacegroup, int \*iunit) 

 ,

(int \*sunit, int \*spacegroup, int \*iunit) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | msyput: @sunit: (I) iostream to use  |
|                                      | for SYMOP library (ignored)          |
|                                      | @spacegroup:(I) spacegroup number    |
|                                      | @iunit: (I) map stream number        |
|                                      |                                      |
|                                      | description: Copy symmetry operators |
|                                      | to map stream @iunit, leaving space  |
|                                      | at head of file for n\_byt\_hdr      |
|                                      | items of header records.             |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MSYWRT 

 ,

msywrt 

 ,

(int \*iunit, int \*nsym, float \*rot) 

 ,

(int \*iunit, int \*nsym, float \*rot) 

 ,

(int \*iunit, int \*nsym, float \*rot) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | msywrt: @iunit: (I) map stream       |
|                                      | number @nsym: (I) number of symmetry |
|                                      | operators @rot: (I)                  |
|                                      | rotation/translation matrices        |
|                                      | (4,4,nsym)                           |
|                                      |                                      |
|                                      | description: Write @nsym symmetry    |
|                                      | operators to iostream @iunit. note:  |
|                                      | the symmetry operators are written   |
|                                      | to the file one per line, and may    |
|                                      | have a different format to those in  |
|                                      | the SYMOP file (uses symtr3()).      |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MCLOSE 

 ,

mclose 

 ,

(int \*iunit, float \*min, float \*max, float \*mean, float \*rms) 

 ,

(int \*iunit, float \*min, float \*max, float \*mean, float \*rms) 

 ,

(int \*iunit, float \*min, float \*max, float \*mean, float \*rms) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mclose: @iunit: (I) map stream       |
|                                      | number @min: (I) minimum density in  |
|                                      | map @max: (I) maximum density in map |
|                                      | @mean: (I) the sum of all the        |
|                                      | densities in the map. (this will be  |
|                                      | divided internally by the number of  |
|                                      | points in the map to give the mean   |
|                                      | density which is then stored) @rms   |
|                                      | (I) the sum of the squares of the    |
|                                      | density values in the map (this will |
|                                      | be used internally to calculate the  |
|                                      | rms deviation from the mean value,   |
|                                      | which is then stored)                |
|                                      |                                      |
|                                      | description: Write out header to map |
|                                      | file on stream @iunit, and close it. |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MWCLOSE 

 ,

mwclose 

 ,

(int \*iunit) 

 ,

(int \*iunit) 

 ,

(int \*iunit) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | @iunit: (I) stream number            |
|                                      |                                      |
|                                      | description: Write out header to     |
|                                      | mapfile on stream @iunit, and close  |
|                                      | it.                                  |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MRDHDR 

 ,

mrdhdr 

 ,

(int \*iunit, const fpstr mapnam, fpstr title, int \*nsec, int iuvw[3],
int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2,
float cell[6], int \*lspgrp, int \*lmode, float \*rhmin, float \*rhmax,
float \*rhmean, float \*rhrms, int mapnam\_len, int title\_len) 

 ,

(int \*iunit, const fpstr mapnam, fpstr title, int \*nsec, int iuvw[3],
int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2,
float cell[6], int \*lspgrp, int \*lmode, float \*rhmin, float \*rhmax,
float \*rhmean, float \*rhrms) 

 ,

(int \*iunit, const fpstr mapnam, int mapnam\_len, fpstr title, int
title\_len, int \*nsec, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1,
int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int
\*lmode, float \*rhmin, float \*rhmax, float \*rhmean, float \*rhrms) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mrdhdr: @iunit: (I) stream number    |
|                                      | @mapnam: (I) logical (file) name     |
|                                      | @title: (O) title of map (char[80])  |
|                                      | @nsec: (O) number of sections        |
|                                      | @iuvw:[3] (O) fast, medium, slow     |
|                                      | axes (1,2,3 for X,Y,Z) @mxyz:[3] (O) |
|                                      | sampling intervals along x,y,z @nw1: |
|                                      | (O) first section number @nu1: (O)   |
|                                      | limits on fast axis @nu2: (O) @nv1:  |
|                                      | (O) limits on medium axis @nv2: (0)  |
|                                      | @cell:[6] (O) cell dimensions in     |
|                                      | Angstroms and degrees @lspgrp: (O)   |
|                                      | space-group number @lmode: (O) map   |
|                                      | data mode @rhmin: (O) minimum        |
|                                      | density @rhmax: (O) maximum density  |
|                                      | @rhmean: (O) mean density deviation  |
|                                      | @rhrms: (O) rms deviation            |
|                                      |                                      |
|                                      | description: Read map header from    |
|                                      | stream @iunit, logical name in       |
|                                      | @mapnam. Differs from mrdhds() in    |
|                                      | that there are no IFAIL nor IPRINT   |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MRDHDS 

 ,

mrdhds 

 ,

(int \*iunit, const fpstr mapnam, fpstr title, int \*nsec, int iuvw[3],
int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2,
float cell[6], int \*lspgrp, int \*lmode, float \*rhmin, float \*rhmax,
float \*rhmean, float \*rhrms, int \*ifail, int \*iprint, int
mapnam\_len, int title\_len) 

 ,

(int \*iunit, const fpstr mapnam, fpstr title, int \*nsec, int iuvw[3],
int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2,
float cell[6], int \*lspgrp, int \*lmode, float \*rhmin, float \*rhmax,
float \*rhmean, float \*rhrms, int \*ifail, int \*iprint) 

 ,

(int \*iunit, const fpstr mapnam, int mapnam\_len, fpstr title, int
title\_len, int \*nsec, int iuvw[3], int mxyz[3], int \*nw1, int \*nu1,
int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int
\*lmode, float \*rhmin, float \*rhmax, float \*rhmean, float \*rhrms,
int \*ifail, int \*iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mrdhds: @iunit: (I) stream number    |
|                                      | @mapnam: (I) logical (file) name     |
|                                      | @title: (O) title of map (char[80])  |
|                                      | @nsec: (O) number of sections        |
|                                      | @iuvw:[3] (O) fast, medium, slow     |
|                                      | axes (1,2,3 for X,Y,Z) @mxyz:[3] (O) |
|                                      | sampling intervals along x,y,z @nw1: |
|                                      | (O) first section number @nu1: (O)   |
|                                      | limits on fast axis @nu2: (O) @nv1:  |
|                                      | (O) limits on medium axis @nv2: (O)  |
|                                      | @cell:[6] (O) cell dimensions in     |
|                                      | Angstroms and degrees @lspgrp: (O)   |
|                                      | space-group number @lmode: (O) map   |
|                                      | data mode @rhmin: (O) minimum        |
|                                      | density @rhmax: (O) maximum density  |
|                                      | @rhmean: (O) mean densitry @rhrms:   |
|                                      | (O) rms deviation form mean density  |
|                                      | @ifail: (I/O) On input: =0, stop on  |
|                                      | error =1, return on error On output: |
|                                      | =-1, error =unchanged, no error      |
|                                      | @iprint: (I) =0, silent =other,      |
|                                      | print info                           |
|                                      |                                      |
|                                      | description: Read map header from    |
|                                      | stream @iunit, logical name in       |
|                                      | @mapnam                              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MWRHDR 

 ,

mwrhdr 

 ,

(int \*iunit, const fpstr title, int \*nsecs, int iuvw[3], int mxyz[3],
int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6],
int \*lspgrp, int \*lmode, int title\_len) 

 ,

(int \*iunit, const fpstr title, int \*nsecs, int iuvw[3], int mxyz[3],
int \*nw1, int \*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6],
int \*lspgrp, int \*lmode) 

 ,

(int \*iunit, const fpstr title, int title\_len, int \*nsecs, int
iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int
\*nv2, float cell[6], int \*lspgrp, int \*lmode) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mwrhdr: @iunit: (I) map stream       |
|                                      | number, internal to program @title:  |
|                                      | (I) map title @nsecs: (I) number of  |
|                                      | sections in the map @iuvw:[3] (I)    |
|                                      | fast, medium and slow axes storage   |
|                                      | @mxyz:[3] (I) sampling intervals     |
|                                      | along X, Y, Z @nw1: (I) number of    |
|                                      | first section @nu1: (I) start of     |
|                                      | section on fast axis @nu2: (I) end   |
|                                      | of section on fast axis @nv1: (I)    |
|                                      | start of section on medium axis      |
|                                      | @nv2: (I) end of section on medium   |
|                                      | axis @cell:[6] (I) cell dimensions   |
|                                      | in Angstroms and degrees @lspgrp:    |
|                                      | (I) space group number @lmode: (I)   |
|                                      | map data mode =0, FORTRAN logical\*1 |
|                                      | (char) =1, FORTRAN integer\*2        |
|                                      | (short?) =2, FORTRAN real\*4 (float) |
|                                      | =3, complex integer\*2 =4, complex   |
|                                      | real =5, ==0 =10, bricked byte map   |
|                                      |                                      |
|                                      | description: similar to mwrhdl().    |
|                                      | Difference is that the logical       |
|                                      | filename for the map is assumed to   |
|                                      | be MAPOUT.                           |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MWRHDL 

 ,

mwrhdl 

 ,

(int \*iunit, const fpstr mapnam, const fpstr title, int \*nsecs, int
iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int
\*nv2, float cell[6], int \*lspgrp, int \*lmode, int mapnam\_len, int
title\_len) 

 ,

(int \*iunit, const fpstr mapnam, const fpstr title, int \*nsecs, int
iuvw[3], int mxyz[3], int \*nw1, int \*nu1, int \*nu2, int \*nv1, int
\*nv2, float cell[6], int \*lspgrp, int \*lmode) 

 ,

(int \*iunit, const fpstr mapnam, int mapnam\_len, const fpstr title,
int title\_len, int \*nsecs, int iuvw[3], int mxyz[3], int \*nw1, int
\*nu1, int \*nu2, int \*nv1, int \*nv2, float cell[6], int \*lspgrp, int
\*lmode) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | mwrhdl: @iunit: (I) map stream       |
|                                      | number, internal to program @mapnam: |
|                                      | (I) logical file name @title: (I)    |
|                                      | map title @nsecs: (I) number of      |
|                                      | sections in the map @iuvw:[3] (I)    |
|                                      | fast, medium and slow axes storage   |
|                                      | @mxyz:[3] (I) sampling intervals     |
|                                      | along X, Y, Z @nw1: (I) number of    |
|                                      | first section @nu1: (I) start of     |
|                                      | section on fast axis @nu2: (I) end   |
|                                      | of section on fast axis @nv1: (I)    |
|                                      | start of section on medium axis      |
|                                      | @nv2: (I) end of section on medium   |
|                                      | axis @cell:[6] (I) cell dimensions   |
|                                      | in Angstroms and degrees @lspgrp:    |
|                                      | (I) space group number @lmode: (I)   |
|                                      | map data mode =0, FORTRAN logical\*1 |
|                                      | (char) =1, FORTRAN integer\*2        |
|                                      | (short?) =2, FORTRAN real\*4 (float) |
|                                      | =3, complex integer\*2 =4, complex   |
|                                      | real =5, ==0 =10, bricked byte map   |
|                                      |                                      |
|                                      | description: Function used to open   |
|                                      | an output map file and set up the    |
|                                      | header information. The actual       |
|                                      | header is only written to the file   |
|                                      | when the file is closed              |
+--------------------------------------+--------------------------------------+
