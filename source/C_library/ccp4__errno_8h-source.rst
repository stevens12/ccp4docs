`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_errno.h
=============

`Go to the documentation of this file. <ccp4__errno_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_errno.h: Header file for error handling routines
    00003      Copyright (C) 2001  CCLRC, Charles Ballard and Martyn Winn
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00015 #ifndef __CCP4_ERROR_GUARD
    00016 #define __CCP4_ERROR_GUARD
    00017 
    00018 #include <errno.h>
    00019 
    00020 static char rcsidhe[] = "$Id$";
    00021 
    00022 #ifndef CCP4_ERRSYSTEM
    00023 #define CCP4_ERRSYSTEM(x) (((x)&0xfff)<<24)
    00024 #endif
    00025 #ifndef CCP4_ERRLEVEL
    00026 #define CCP4_ERRLEVEL(x)  (((x)&0xf)<<16)
    00027 #endif
    00028 #ifndef CCP4_ERRSETLEVEL
    00029 #define CCP4_ERRSETLEVEL(y,x) ((y) & (~CCP4_ERRLEVEL(0xf)) | CCP4_ERRLEVEL(x)))
    00030 #endif
    00031 #ifndef CCP4_ERRGETSYS   
    00032 #define CCP4_ERRGETSYS(x)   (((x)>>24)&0xfff)
    00033 #endif
    00034 #ifndef CCP4_ERRGETLEVEL
    00035 #define CCP4_ERRGETLEVEL(x) (((x)>>16)&0xf)
    00036 #endif
    00037 #ifndef CCP4_ERRGETCODE
    00038 #define CCP4_ERRGETCODE(x)  ((x)&0xffff)
    00039 #endif
    00040 
    00041 #define CCP4_ERR_SYS CCP4_ERRSYSTEM(0x0)
    00042 #define CCP4_ERR_FILE CCP4_ERRSYSTEM(0x1)
    00043 #define CCP4_ERR_COORD CCP4_ERRSYSTEM(0x2)
    00044 #define CCP4_ERR_MTZ CCP4_ERRSYSTEM(0x3)
    00045 #define CCP4_ERR_MAP CCP4_ERRSYSTEM(0x4)
    00046 #define CCP4_ERR_UTILS CCP4_ERRSYSTEM(0x5)
    00047 #define CCP4_ERR_PARS CCP4_ERRSYSTEM(0x6)
    00048 #define CCP4_ERR_SYM CCP4_ERRSYSTEM(0x7)
    00049 #define CCP4_ERR_GEN CCP4_ERRSYSTEM(0x8)
    00050 
    00051 #define CCP4_COUNT(x) sizeof(x)/sizeof(x[0])
    00052 
    00082 #ifdef __cplusplus
    00083 extern "C" {
    00084 #endif
    00085 extern int ccp4_errno;
    00086 #ifdef __cplusplus
    00087 }
    00088 #endif
    00089 
    00090 #ifdef __cplusplus
    00091 namespace CCP4 {
    00092 extern "C" {
    00093 #endif
    00094 
    00101 void ccp4_error( const char *);
    00102 
    00110 const char *ccp4_strerror( int);
    00111 
    00116 void ccp4_fatal(const char *);
    00117 
    00131 int ccp4_liberr_verbosity(int iverb);
    00132 
    00147 void ccp4_signal(const int, const char *const, void (*)());
    00148 
    00149 int cfile_perror(const char *);
    00150 
    00151 #ifdef __cplusplus
    00152 }
    00153 }
    00154 #endif
    00155 
    00156 #endif  

.. raw:: html

   </div>
