`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_unitcell.h File Reference
===============================

| ``#include <math.h>``

`Go to the source code of this file. <ccp4__unitcell_8h-source.html>`__

| 

Functions
---------

double 

`ccp4uc\_frac\_orth\_mat <ccp4__unitcell_8h.html#a0>`__ (const double
cell[6], const int ncode, double ro[3][3], double rf[3][3])

double 

`ccp4uc\_calc\_rcell <ccp4__unitcell_8h.html#a1>`__ (const double
cell[6], double rcell[6])

void 

`ccp4uc\_orth\_to\_frac <ccp4__unitcell_8h.html#a2>`__ (const double
rf[3][3], const double xo[3], double xf[3])

void 

`ccp4uc\_frac\_to\_orth <ccp4__unitcell_8h.html#a3>`__ (const double
ro[3][3], const double xf[3], double xo[3])

void 

`ccp4uc\_orthu\_to\_fracu <ccp4__unitcell_8h.html#a4>`__ (const double
rf[3][3], const double uo[6], double uf[6])

void 

`ccp4uc\_fracu\_to\_orthu <ccp4__unitcell_8h.html#a5>`__ (const double
ro[3][3], const double uf[6], double uo[6])

double 

`ccp4uc\_calc\_cell\_volume <ccp4__unitcell_8h.html#a6>`__ (const double
cell[6])

int 

`ccp4uc\_cells\_differ <ccp4__unitcell_8h.html#a7>`__ (const double
cell1[6], const double cell2[6], const double tolerance)

int 

`ccp4uc\_is\_rhombohedral <ccp4__unitcell_8h.html#a8>`__ (const float
cell[6], const float tolerance)

int 

`ccp4uc\_is\_hexagonal <ccp4__unitcell_8h.html#a9>`__ (const float
cell[6], const float tolerance)

--------------

Detailed Description
--------------------

C library for manipulations based on cell parameters. Martyn Winn

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +-------------------------------------+------+-----------------+-------- |
| ---------+------+----+                                                   |
| | double ccp4uc\_calc\_cell\_volume   | (    | const double    |   *cell |
| *\ [6]   | )    |    |                                                   |
| +-------------------------------------+------+-----------------+-------- |
| ---------+------+----+                                                   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Calculate cell volume from cell      |
|                                      | parameters.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+----+               |
|                                      |     | *cell*    |    |               |
|                                      |     +-----------+----+               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Cell volume.                     |
+--------------------------------------+--------------------------------------+

double ccp4uc\_calc\_rcell

( 

const double 

  *cell*\ [6],

double 

  *rcell*\ [6]

) 

+--------------------------------------+--------------------------------------+
|                                      | From input cell, find dimensions of  |
|                                      | reciprocal cell.                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----+              |
|                                      |     | *cell*     |    |              |
|                                      |     +------------+----+              |
|                                      |     | *rcell*    |    |              |
|                                      |     +------------+----+              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Reciprocal cell volume           |
+--------------------------------------+--------------------------------------+

int ccp4uc\_cells\_differ

( 

const double 

  *cell1*\ [6],

const double 

  *cell2*\ [6],

const double 

  *tolerance*

) 

+--------------------------------------+--------------------------------------+
|                                      | Check cells agree within tolerance.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ----------------+                    |
|                                      |     | *cell1*        | First cell.   |
|                                      |                 |                    |
|                                      |     +----------------+-------------- |
|                                      | ----------------+                    |
|                                      |     | *cell2*        | Second cell.  |
|                                      |                 |                    |
|                                      |     +----------------+-------------- |
|                                      | ----------------+                    |
|                                      |     | *tolerance*    | A tolerance f |
|                                      | or agreement.   |                    |
|                                      |     +----------------+-------------- |
|                                      | ----------------+                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if cells differ by more than   |
|                                      |     tolerance, 0 otherwise.          |
+--------------------------------------+--------------------------------------+

double ccp4uc\_frac\_orth\_mat

( 

const double 

  *cell*\ [6],

const int 

  *ncode*,

double 

  *ro*\ [3][3],

double 

  *rf*\ [3][3]

) 

+--------------------------------------+--------------------------------------+
|                                      | From input cell and                  |
|                                      | orthogonalisation code, find         |
|                                      | orthogonalisation and                |
|                                      | fractionalisation matrices.          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----+              |
|                                      |     | *cell*     |    |              |
|                                      |     +------------+----+              |
|                                      |     | *ncode*    |    |              |
|                                      |     +------------+----+              |
|                                      |     | *ro*       |    |              |
|                                      |     +------------+----+              |
|                                      |     | *rf*       |    |              |
|                                      |     +------------+----+              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Cell volume                      |
+--------------------------------------+--------------------------------------+

void ccp4uc\_frac\_to\_orth

( 

const double 

  *ro*\ [3][3],

const double 

  *xf*\ [3],

double 

  *xo*\ [3]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert fractional to orthogonal     |
|                                      | coordinates.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+----+                 |
|                                      |     | *ro*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |     | *xf*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |     | *xo*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

void ccp4uc\_fracu\_to\_orthu

( 

const double 

  *ro*\ [3][3],

const double 

  *uf*\ [6],

double 

  *uo*\ [6]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert fractional to orthogonal u   |
|                                      | matrix.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+----+                 |
|                                      |     | *ro*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |     | *uf*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |     | *uo*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

int ccp4uc\_is\_hexagonal

( 

const float 

  *cell*\ [6],

const float 

  *tolerance*

) 

+--------------------------------------+--------------------------------------+
|                                      | Check if cell parameters conform to  |
|                                      | a hexagonal setting.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *cell*         | Cell paramete |
|                                      | rs. Angles are assumed to be in degr |
|                                      | ees.   |                             |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *tolerance*    | A tolerance f |
|                                      | or agreement.                        |
|                                      |        |                             |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if cell parameters conform, 0  |
|                                      |     otherwise.                       |
+--------------------------------------+--------------------------------------+

int ccp4uc\_is\_rhombohedral

( 

const float 

  *cell*\ [6],

const float 

  *tolerance*

) 

+--------------------------------------+--------------------------------------+
|                                      | Check if cell parameters conform to  |
|                                      | a rhombohedral setting.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *cell*         | Cell paramete |
|                                      | rs. Angles are assumed to be in degr |
|                                      | ees.   |                             |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *tolerance*    | A tolerance f |
|                                      | or agreement.                        |
|                                      |        |                             |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if cell parameters conform, 0  |
|                                      |     otherwise.                       |
+--------------------------------------+--------------------------------------+

void ccp4uc\_orth\_to\_frac

( 

const double 

  *rf*\ [3][3],

const double 

  *xo*\ [3],

double 

  *xf*\ [3]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert orthogonal to fractional     |
|                                      | coordinates. Translation only if     |
|                                      | deliberate origin shift - does this  |
|                                      | ever happen? Leave it to the         |
|                                      | application.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+----+                 |
|                                      |     | *rf*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |     | *xo*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |     | *xf*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

void ccp4uc\_orthu\_to\_fracu

( 

const double 

  *rf*\ [3][3],

const double 

  *uo*\ [6],

double 

  *uf*\ [6]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert orthogonal to fractional u   |
|                                      | matrix.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+----+                 |
|                                      |     | *rf*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |     | *uo*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |     | *uf*    |    |                 |
|                                      |     +---------+----+                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+
