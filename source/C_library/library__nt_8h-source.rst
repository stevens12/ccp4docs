`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library\_nt.h
=============

.. raw:: html

   <div class="fragment">

::

    00001 /* % This was apart of library.c but has been split off so it can be used   */
    00002 /* % with other c programs in the suite.                                    */
    00003 /*                                                                          */
    00004 /* \section{Portability}                                                    */
    00005 /*                                                                          */
    00006 /* We aim for compatibility with K\&R\index{K&R@K\&R C} and                 */
    00007 /* \index{ANSI C@\ac{ansi} C} \ac{ansi}~C\@ as well as \idx{VAX             */
    00008 /*   C}\index{C!VAX}.  One particularly annoying                            */
    00009 /* consequence is that we can't rely on [[#elif]] preprocessor              */
    00010 /* directives.  I don't know whether anything needs to be changed for the   */
    00011 /* new \idx{DEC C}, which is apparently \ac{ansi}\dots{}                    */
    00012 /*                                                                          */
    00013 /*                                                                          */
    00014 /* \section{Code}                                                           */
    00015 /*                                                                          */
    00016 /* These are the components of the code.  The \LA{}guarded code\RA{} is     */
    00017 /* executed when we've identified the platform.                             */
    00018 /*                                                                          */
    00019 /*                                                                          */
    00020 /* There are several platform dependencies in the code which we need to     */
    00021 /* get right:                                                               */
    00022 /* \begin{description}                                                      */
    00023 /* \item[Fortran/C conventions] The calling conventions for C from          */
    00024 /*   \ft{} vary from compiler to compiler;                                  */
    00025 /* \item[Header files etc.] We can't assume everything has \ac{ansi} C      */
    00026 /*   or \ac{posix} libraries;                                               */
    00027 /* \item[Real number format] for the transparent binary i/o;                */
    00028 /* \item[Special things] The [[putenv]]/[[setenv]] call isn't defined in    */
    00029 /*   the current standards.                                                 */
    00030 /* \end{description}                                                        */
    00031 /*                                                                          */
    00032 /* Each type of system we know about should cause [[KNOWN_MACHINE]] to be   */
    00033 /* defined and also define \verb+CALL_LIKE_+\meta{something} to be          */
    00034 /* defined.  Thus if you know system \meta{foo} has a \ft{}                 */
    00035 /* calling convention like the native Sun compiler uses, define             */
    00036 /* [[CALL_LIKE_SUN]] and you won't need to examine the definitions of the   */
    00037 /* interface functions below.  Further tests on the system type may be      */
    00038 /* necessary e.g., to get the include files right.                          */
    00039 /*                                                                          */
    00040 /* \subsection{Assumptions}\index{assumption}                               */
    00041 /*                                                                          */
    00042 /* Note that it's assumed below that a \ft{} [[INTEGER]]                    */
    00043 /* corresponds to a C [[int]] and a \ft{} [[REAL]] corresponds to           */
    00044 /* a C [[float]].                                                           */
    00045 /*                                                                          */
    00046 /* Also, the identity of certain \idx{calling conventions} is only          */
    00047 /* guaranteed if the routines have only a single \ft{}                      */
    00048 /* \index{CHARACTER variables@{\tt CHARACTER} variables}                    */
    00049 /* [[CHARACTER]]-type argument since sometimes the length of each such      */
    00050 /* argument is given after it in the parameter list and sometimes they      */
    00051 /* are all collected at the end of the list.                                */
    00052 /*                                                                          */
    00053 /* \subsection{Platform identification}                                     */
    00054 /*                                                                          */
    00055 /* Apart from the possibility of using the \idx{Netlib} {\tt                */
    00056 /*   f2c}\index{f2c@{\tt f2c}} compiler we currently assume that each       */
    00057 /* system uses the vendor-supplied \ft{} compiler\index{FORTRAN             */
    00058 /*   compiler@\ft{} compiler}.\index{platforms}                             */
    00059 /*                                                                          */
    00060 /*                                                                          */
    00061 /* This is for \idx{IBM} Unix systems---\idx{RS/6000} models, at least.     */
    00062 /* The compiler can append \verb+_+ to external names, but we assume the    */
    00063 /* default where this doesn't happen.  See {\tt configure} for the          */
    00064 /* enforcement of this.                                                     */
    00065 /*                                                                          */
    00066 /* <identifying the platform>=                                              */
    00067 #if defined (_AIX) || defined(___AIX)
    00068 #  define KNOWN_MACHINE
    00069 #  define CALL_LIKE_HPUX 1
    00070 #endif
    00071 /* This is for \idx{Alliant} \idx{FX28xx}, at least, e.g.\ the FX2800 at LMB\@. */
    00072 /*                                                                          */
    00073 /* <identifying the platform>=                                              */
    00074 #if defined (alliant)
    00075 #  define KNOWN_MACHINE
    00076 #  define CALL_LIKE_SUN 1
    00077 #endif
    00078 /* The \idx{Ardent} \idx{Stardent}/\idx{Titan} support probably doesn't     */
    00079 /* work at present.                                                         */
    00080 /*                                                                          */
    00081 /* <identifying the platform>=                                              */
    00082 #if defined (ardent) || defined (titan)
    00083 #  ifndef stardent
    00084 #    define stardent
    00085 #  endif
    00086 #endif
    00087 #if defined (stardent)
    00088 #  define KNOWN_MACHINE
    00089 #  define CALL_LIKE_STARDENT 1
    00090 #endif
    00091 /* There seem to be two possible ways of identifying a \idx{Convex}         */
    00092 /* (`C' series, at least) system.  [[__convexc__]] is documented in OS10    */
    00093 /* but [[__convex__]] seems to be there as well (and was probably           */
    00094 /* documented in OS8).                                                      */
    00095 /*                                                                          */
    00096 /* <identifying the platform>=                                              */
    00097 #if defined (__convex__) || defined (__convexc__)
    00098 #  define KNOWN_MACHINE
    00099 #  define CALL_LIKE_SUN 1
    00100 #endif
    00101 /* The \idx{Evans and Sutherland} \idx{ESV1} workstation can operate in the */
    00102 /* \idx{BSD} or \idx{SYSV} universes.  It doesn't seem to be properly       */
    00103 /* \index{POSIX@\ac{posix}}\ac{posix}- or \ac{ansi}                         */
    00104 /* C-compliant.\index{ANSI C@\ac{ansi} C}                                   */
    00105 /*                                                                          */
    00106 /* <identifying the platform>=                                              */
    00107 #if defined (ESV)
    00108 #  define KNOWN_MACHINE
    00109 #  define CALL_LIKE_SUN 1
    00110 #endif
    00111 /* This cover \idx{Hewlett Packard} 9000/750 (RISC) models, at least.  Others */
    00112 /* may vary.                                                                */
    00113 /*                                                                          */
    00114 /* <identifying the platform>=                                              */
    00115 #if defined (__hpux) 
    00116 #  define KNOWN_MACHINE
    00117 #  define CALL_LIKE_HPUX 1
    00118 #endif
    00119 /* \idx{Silicon Graphics} \idx{IRIX} systems \idx{Iris}es, \idx{Indigo}s,   */
    00120 /* \idx{Crimson}s etc. (at least version 4 up) are                          */
    00121 /* \ac{ansi}\index{ANSI C@\ac{ansi} C} and                                  */
    00122 /*  \ac{posix}\index{POSIX@\ac{posix}} compliant.                           */
    00123 /*                                                                          */
    00124 /* <identifying the platform>=                                              */
    00125 #ifdef __sgi   /* in ANSI mode */
    00126 #  ifndef sgi
    00127 #    define sgi
    00128 #  endif
    00129 #endif
    00130 #if defined (sgi)
    00131 #  define KNOWN_MACHINE
    00132 #  define CALL_LIKE_SUN 1
    00133 #endif
    00134 /* \idx{Solbourne}s are \idx{Sun} clones.                                   */
    00135 /*                                                                          */
    00136 /* <identifying the platform>=                                              */
    00137 #if defined (solbourne) 
    00138 #  ifndef sun
    00139 #   define sun               /* don't know whether it's defined or not */
    00140 #  endif
    00141 #endif
    00142 /* THis is OK for \idx{Solaris}1 and~2.                                     */
    00143 /*                                                                          */
    00144 /* <identifying the platform>=                                              */
    00145 #if defined (sun) || defined (__sun)
    00146 #  define KNOWN_MACHINE
    00147 #  define CALL_LIKE_SUN 1
    00148 #  if !defined(__STDC__) || defined(__GNUC__)
    00149 #    if !defined(G77)
    00150       extern char *sys_errlist [];
    00151 #     define strerror(i) sys_errlist[i] /* k&r compiler doesn't have it */
    00152 #    endif
    00153 #  endif
    00154 #endif
    00155 /* \idx{DEC} \idx{OSF/1} (\idx{Alpha}) and \idx{Ultrix} use the same        */
    00156 /* calling conventions, at least.  The documentation I saw for OSF/1 said   */
    00157 /* that [[__OSF1__]] is defined, but it's reported that you need            */
    00158 /* [[__osf__]] (in come cases?).                                            */
    00159 /*                                                                          */
    00160 /* <identifying the platform>=                                              */
    00161 #if defined (ultrix) || defined(__OSF1__) || defined(__osf__)
    00162 #  define KNOWN_MACHINE
    00163 #  define CALL_LIKE_SUN 1
    00164 #endif
    00165 /* \idx{VMS} is a law unto itself, of course.  Help for VAX C doesn't       */
    00166 /* actually say [[VMS]] is defined (as opposed to [[vms]]), although it     */
    00167 /* seems to be.  The other possibilities are for DEC C in strict            */
    00168 /* ANSI mode.  NB: we now don't use the C code under VMS due to the         */
    00169 /* apparent bugs in the DEC C RTL\@.                                        */
    00170 /*                                                                          */
    00171 /* <identifying the platform>=                                              */
    00172 #ifndef VMS
    00173 #  if defined (vms) || defined (__vms) || defined (__VMS)
    00174 #    define VMS
    00175 #  endif
    00176 #endif
    00177 #if defined (VMS)
    00178 #  define KNOWN_MACHINE
    00179 #endif
    00180 
    00181 /* First attemt at porting to nt wsing the visual fortran and MVC++         */
    00182 /* MVS stands for Microsoft Visual Studio - better than VMS                 */
    00183 #if defined(_MVS) 
    00184 #  define CALL_LIKE_MVS 1
    00185 #  define KNOWN_MACHINE
    00186 #endif
    00187 /* Generic linux, may not have the G77 compilers */
    00188 #if defined (linux)
    00189 #  undef CALL_LIKE_SUN
    00190 #  define KNOWN_MACHINE
    00191 #  define CALL_LIKE_SUN 1
    00192 #endif
    00193 
    00194 /* {\tt f2c}\index{f2c@{\tt f2c}} misses the MIL--STD                       */
    00195 /* \idx{bit-twiddling intrinsics}.  Its calling                             */
    00196 /* convention is like \idx{Sun} only for a {\em single\/} [[CHARACTER]]     */
    00197 /* variable in the parameter list!  {\tt g77}\index{g77@{\tt g77}} has      */
    00198 /* (will have!)\ the same calling convention and library as {\tt f2c} but   */
    00199 /* does support the MIL--STD intrinsics (amongst other things).  The        */
    00200 /* alpha-test version doesn't have a {\tt BYTE} (or {\tt INTEGER*1})        */
    00201 /* type, so can't be used at present.                                       */
    00202 /*                                                                          */
    00203 /* <identifying the platform>=                                              */
    00204 #if defined(F2C) || defined(G77)
    00205 #  undef CALL_LIKE_SUN
    00206 #  define CALL_LIKE_SUN 1
    00207 #  define KNOWN_MACHINE
    00208 #endif
    00209 /* If we haven't identified the system type, we want to stop with an        */
    00210 /* error message.  Indenting [[#error]] works with \ac{ansi} C              */
    00211 /* \index{ANSI C@\ac{ansi} C}                                               */
    00212 /* and doesn't fall over with K\&R\index{K&R@K\&R C} as                     */
    00213 /* it would if un-indented, even when the test is false.                    */
    00214 /*                                                                          */
    00215 /* <guarded code>=                                                          */
    00216 #if ! defined (KNOWN_MACHINE)
    00217   #error System type is not known -- see the Installation Guide
    00218 #else
    00219 /* At this stage we've identified the platform and are in business.  Here   */
    00220 /* are the components we have to put together.                              */
    00221 /*                                                                          */
    00222 /* <general code>=                                                          */
    00223 /* \section{Header files}                                                   */
    00224 /*                                                                          */
    00225 /* If the system has \ac{posix} stuff, we want to ensure it's used.         */
    00226 /*                                                                          */
    00227 /* <header files>=                                                          */
    00228 #ifndef _POSIX_SOURCE
    00229 #define _POSIX_SOURCE
    00230 #endif
    00231 /* <header files>=                                                          */
    00232 #include <stdio.h>
    00233 
    00234 #if defined (VMS)
    00235 #  include <descrip.h>          /* non-POSIX */
    00236 #  define NOUNISTD
    00237 #else
    00238 #  include <sys/types.h>
    00239 #  include <sys/stat.h>
    00240 #  ifndef _MVS
    00241 #    include <sys/times.h>
    00242 #  endif
    00243 #  ifdef _MVS
    00244 #    define NOUNISTD
    00245 #  endif
    00246 #endif
    00247 
    00248 #ifdef stardent                 /* who knows if this works anyhow... */
    00249 #  include <sys/types.h>
    00250 #  include <malloc.h>           /* non-POSIX */
    00251 #else
    00252 #  include <stddef.h>
    00253 #endif
    00254 /* BSD might need {\tt strings.h} and [[index]] instead of [[strchr]].      */
    00255 /* This is an \ac{ansi} header, not \ac{posix.1}.                           */
    00256 /*                                                                          */
    00257 /* <header files>=                                                          */
    00258 #include <string.h>
    00259 /* Some systems like \idx{ESV} don't have {\tt unistd.h}, and the           */
    00260 /* configuration makes the appropriate [[#define]] of [[NOUNISTD]].         */
    00261 /*                                                                          */
    00262 /* <header files>=                                                          */
    00263 #ifndef NOUNISTD
    00264 #  include <unistd.h>
    00265 #else
    00266 #  ifndef VMS 
    00267 #    ifndef _MVS
    00268 #      include <sys/file.h>     /* ESV, old Concentrix */ /* non-POSIX */
    00269 #    endif
    00270 #  endif
    00271 #endif
    00272 #ifndef NOSTDLIB                /* for TitanOS 4.2, at least? */
    00273 #  include <stdlib.h>
    00274 #endif
    00275 
    00276 #include <errno.h>
    00277 #include <ctype.h>
    00278 
    00279 #if defined(_AIX) || defined (__hpux) || defined(F2C) ||\
    00280     defined(G77) || defined(_WIN32)/* would do no harm on others, though */
    00281 #  include <time.h>
    00282 #endif
    00283 /* We need INT_MAX and DBL_MAX defined for routine Hgetlimits               */
    00284 /* These should be in {\tt limits.h} and {\tt float.h} respectively         */
    00285 /* (this is POSIX standard?).                                               */
    00286 /*                                                                          */
    00287 /* <header files>=                                                          */
    00288 
    00289 #include <limits.h>
    00290 #include <float.h>
    00291 
    00292 /* For f2c we need this for typedefs.  We assume it's on an include         */
    00293 /* path where (g)cc will find it.  The [[#define]] is to avoid the          */
    00294 /* undefinition of macros like [[sgi]].                                     */
    00295 /*                                                                          */
    00296 /* this has been altered for g2c.h as f2c.h is not always distributed       */
    00297 /* <header files>=                                                          */
    00298 #if defined (F2C)
    00299 #  define Skip_f2c_Undefs
    00300 #  include "f2c.h"
    00301 #endif
    00302 #if defined (G77)
    00303 #  define Skip_f2c_Undefs       /* g2c.h infelicity... */
    00304 #  if defined (HAVE_G2C_H)
    00305 #    include "g2c.h"
    00306 #  else
    00307 #    include "f2c.h"
    00308 #  endif
    00309 #endif
    00310 /* \section{[[#define]]s}                                                   */
    00311 /* \subsection{Defaults and customisable items}                             */
    00312 /*                                                                          */
    00313 /* \fixme{We should be able to get help from \ac{posix} on the filename     */
    00314 /* length and open files limits}                                            */
    00315 /*                                                                          */
    00316 /* <[[#define]]s>=                                                          */
    00317 #define MAXFLEN       500       /* the maximum length of a filename in CCP4 */
    00318 #define MAXFILES       10    /* maximum number of files open symultaneously */
    00319 #define DEFMODE         2    /* default mode access for random access files */
    00320 /* These constants record the current i/o status of a stream (needed to     */
    00321 /* know if an [[fseek]] is needed or not before the next i/o operation).    */
    00322 /*                                                                          */
    00323 /* <[[#define]]s>=                                                          */
    00324 #define IRRELEVANT_OP   0
    00325 #define READ_OP         1
    00326 #define WRITE_OP        2
    00327 /* \subsection{Machine dependent stuff}                                     */
    00328 /* These should be defined in {\tt stdlib.h}, but this isn't present, for   */
    00329 /* instance in \idx{Alliant} \idx{Concentrix} before release~3 or with the  */
    00330 /* bundled \idx{SunOS} {\tt cc}.                                            */
    00331 /*                                                                          */
    00332 /* <[[#define]]s>=                                                          */
    00333 #ifndef SEEK_SET
    00334 #  define SEEK_SET 0
    00335 #  define SEEK_CUR 1
    00336 #  define SEEK_END 2
    00337 #endif /* ! SEEK_SET */
    00338 /* <[[#define]]s>=                                                          */
    00339 #if defined (ardent) || defined (titan) || defined (stardent)
    00340   struct Str_Desc {
    00341     char *Str_pointer;
    00342     int  Str_length;
    00343     int id;
    00344   };
    00345 #endif
    00346 /* \subsection{File mode definitions}                                       */
    00347 /*                                                                          */
    00348 /* Here are the deinfitions of the {\tt diskio} modes, specifying the       */
    00349 /* type of data transfer: bytes, half-words, integers, reals,               */
    00350 /* half(integer)-word complex and complex, respectively:                    */
    00351 /*                                                                          */
    00352 /* <[[#define]]s>=                                                          */
    00353 #define BYTE  0
    00354 #define INT16 1   
    00355 #define INT32 6
    00356 #define FLOAT32 2
    00357 #define COMP32  3
    00358 #define COMP64  4
    00359 /* \section{Converting foreign binary number formats}                       */
    00360 /*                                                                          */
    00361 /* The library is intended to allow the binary file formats (\idx{MTZ}      */
    00362 /* and map\index{map files}) to be read satisfactorily if they were         */
    00363 /* written on another platform.  Such files are always written in the       */
    00364 /* {\em native\/} real or integer number format with a `\idx{machine        */
    00365 /*   stamp}' in the file to identify the formats involved.  Then, if        */
    00366 /* necessary, conversion is done from the foreign format to native when     */
    00367 /* the file is read.  There is thus only a significant overhead for files   */
    00368 /* imported                                                                 */
    00369 /* from platforms with different number formats; locally-written files      */
    00370 /* are read back optimally and there is no write overhead.  This is in      */
    00371 /* contrast, for instance, to the \idx{XDR} approach (and \idx{HDF}?),      */
    00372 /* where a canonical external format is used.                               */
    00373 /*                                                                          */
    00374 /* When converting from foreign to native formats we're potentially faced   */
    00375 /* with a combinatorial explosion---currently combinations of \ac{ieee}     */
    00376 /* little-endian, \ac{ieee} big-endian, \idx{VAX} and \idx{Convex} native   */
    00377 /* formats.  (This applies only to real number formats---fortunately        */
    00378 /* everything we're interested in has \idx{twos complement} integers.)  Thus we */
    00379 /* first make sure that the format is converted to canonical form (which    */
    00380 /* we choose as big-endian \ac{ieee}, following XDR) and then, if           */
    00381 /* necessary, to the native format in a separate stage.  This is going to   */
    00382 /* be somewhat slower than it might be, but what the heck\dots{}            */
    00383 /*                                                                          */
    00384 /* The basic idea of this is due to David Wild (EMBL, Hamburg, 1991).       */
    00385 /* His original, partially-functional implementation used code from the     */
    00386 /* \idx{HDF} 3.1 distribution.  This re-write is by Dave Love, very         */
    00387 /* loosely based on HDF3.3, but doing the conversion in-place.  It works    */
    00388 /* for the full set of relevant systems and no longer has MTZ- and          */
    00389 /* map-specific code in [[copen]].  (HDF stuff can be found on {\tt         */
    00390 /*   ftp.ncsa.uiuc.edu}.)                                                   */
    00391 /*                                                                          */
    00392 /* \subsection{`Machine stamps'}                                            */
    00393 /*                                                                          */
    00394 /* Here's how we specify the number formats for machines.  The              */
    00395 /* `\idx{machine stamp}' is a 32-bit quantity containing a set of four      */
    00396 /* `nibbles' (half-bytes)---only half the space is used.  Each nibble is    */
    00397 /* a number specifying the representation of (in C terms) [[double]]        */
    00398 /* ($d$), [[float]] ($f$), [[int]] ($i$)) and [[unsigned char]] ($c$)       */
    00399 /* types.  Thus each stamp is of the form $\mbox{{\tt 0x}}dfic0000$.  The   */
    00400 /* values for the nibbles may be taken from the list (following HDF):       */
    00401 /* \begin{quote}                                                            */
    00402 /*   \begin{tabular}{ll}                                                    */
    00403 /*       1 & Big-endian \ac{ieee}\\                                         */
    00404 /*       2 & VAX \\                                                         */
    00405 /*       3 & Cray \\                                                        */
    00406 /*       4 & Little-endian \ac{ieee}\\                                      */
    00407 /*       5 & Convex native \\                                               */
    00408 /*       6 & Fijitsu VP                                                     */
    00409 /*   \end{tabular}                                                          */
    00410 /* \end{quote}                                                              */
    00411 /* \idx{Cray} isn't relevant to us because it's not a 32-bit machine        */
    00412 /* and we don't currently have a use for the \idx{Fujitsu} one, which isn't */
    00413 /* implemented here.  We ignore the possibility of                          */
    00414 /* non-\ac{ascii}\index{ASCII@\ac{ascii}} characters which might need       */
    00415 /* converting e.g., from \ac{ebcdic}\index{EBCDIC@\ac{ebcdic}} and $c$ is   */
    00416 /* always $1$; also $f$ and $d$ are the same (as per \idx{Fortran}).  See the */
    00417 /* \idx{HDF} code for character code possibilities.                         */
    00418 /*                                                                          */
    00419 /* Here are the tags for different formats (`\idx{class info codes}'),      */
    00420 /* not all relevant:                                                        */
    00421 /*                                                                          */
    00422 /* <[[#define]]s>=                                                          */
    00423 /* class info codes for int */
    00424 #define DFNTI_MBO       1       /* Motorola byte order 2's compl */
    00425 #define DFNTI_IBO       4       /* Intel byte order 2's compl */
    00426 
    00427 /* class info codes for float */
    00428 #define DFNTF_BEIEEE    1       /* big endian IEEE (canonical) */
    00429 #define DFNTF_VAX       2       /* Vax format */
    00430 #define DFNTF_CONVEXNATIVE 5    /* Convex native floats */
    00431 #define DFNTF_LEIEEE    4       /* little-endian IEEE format */
    00432 /* Here are the definitions                                                 */
    00433 /* we're interested in.  Note\index{assumption} that some of the symbols    */
    00434 /* tested here to determine the machine type might need to be qualified     */
    00435 /* in the future where they don't necessarily determine the architecture.   */
    00436 /* We just need to set [[nativeFT]] and [[nativeIT]], which determine the   */
    00437 /* native real and integer formats.                                         */
    00438 /* First an obvious one:                                                    */
    00439 /*                                                                          */
    00440 /* <[[#define]]s>=                                                          */
    00441 #if defined (VAX) || defined (vax) /* gcc seems to use vax */
    00442 #  define NATIVEFT DFNTF_VAX
    00443 #  define NATIVEIT DFNTI_IBO
    00444 #endif
    00445 /* Here are the possibilities for little-endian \ac{ieee}.  (The            */
    00446 /* \idx{MIPS} compilers define [[MIPSEL]] or [[MIPSEB]] depending on the    */
    00447 /* mode in which the the chip operates.)  The architectures covered here    */
    00448 /* include some R\meta{nnnn} (e.g., \idx{DECstations}), \idx{i860} and      */
    00449 /* other \idx{Intel} chips like \idx{PCs} and \idx{Alpha} (sometimes!).     */
    00450 /*                                                                          */
    00451 /* <[[#define]]s>=                                                          */
    00452 #if defined(MIPSEL) || defined(alliant) || defined(i386) || defined(i860)
    00453 #  define NATIVEIT DFNTI_IBO
    00454 #  define NATIVEFT DFNTF_LEIEEE
    00455 #endif
    00456 /* Here is a first attempt at machines using the powerPC chip.              */
    00457 /* Specifically, this has been tried on PowerMacs running LinuxPPC, which   */
    00458 /* appears to be big-endian. But in principle the powerPC chip can support  */
    00459 /* both big-endian and little-endian OS's under software control. The       */
    00460 /* symbol "powerpc" appears in gcc-2.8.1/config/rs6000/linux.h and appears  */
    00461 /* to distinguish LinuxPPC from other OS's for this chip.                   */
    00462 #if defined (powerpc)
    00463 #  define NATIVEIT DFNTI_MBO
    00464 #  define NATIVEFT DFNTF_BEIEEE
    00465 #endif
    00466 /* \idx{Alpha} \idx{VMS} is a pain: compiler switches can force             */
    00467 /* \idx{VAX} or \ac{ieee} number formats.  Thus if we know it's an Alpha,   */
    00468 /* we have to check for VMS and then what sort of VMS numbers.  [OSF and    */
    00469 /* OpenVMS define [[__alpha]], OpenVMS, only [[__ALPHA]].\index{Alpha}      */
    00470 /*                                                                          */
    00471 /* <[[#define]]s>=                                                          */
    00472 #ifdef __alpha
    00473 #  ifdef VMS
    00474 #    if __IEEE_FLOAT == 1
    00475 #      define NATIVEFT DFNTF_LEIEEE
    00476 #    else
    00477 #      define NATIVEFT DFNTF_VAX
    00478 #    endif
    00479 #  else                       /* assume OSF/1 */
    00480 #    define NATIVEFT DFNTF_LEIEEE
    00481 #  endif
    00482 #  define NATIVEIT DFNTI_IBO
    00483 #endif
    00484 /* Big-endian \ac{ieee} includes other R\meta{nnnn} like SGI machines,      */
    00485 /* \idx{HP} beasts (\idx{68k}-based or \idx{RISC}), \idx{RS/6000} and all   */
    00486 /* \idx{Sun}s except the obsolete i386-based ones.                          */
    00487 /* \idx{Apollo}s are also apparently in this category.                      */
    00488 /*                                                                          */
    00489 /* <[[#define]]s>=                                                          */
    00490 /* the VAX VMS compiler objected to splitting the following line */
    00491 #if defined(MIPSEB) || defined(__hpux) || defined(_AIX) || defined(m68k) || defined(mc68000) || defined(sparc) || defined (__sparc__)
    00492 #  define NATIVEIT DFNTI_MBO
    00493 #  define NATIVEFT DFNTF_BEIEEE
    00494 #endif
    00495 /* \idx{Convex}s can operate in either native or \ac{ieee} mode:            */
    00496 /*                                                                          */
    00497 /* <[[#define]]s>=                                                          */
    00498 #if defined(__convex__) || defined(__convexc__)
    00499 #  define NATIVEIT DFNTI_MBO
    00500 #  ifdef _IEEE_FLOAT_
    00501 #    define NATIVEFT DFNTF_BEIEEE
    00502 #  else
    00503 #    ifdef _CONVEX_FLOAT_
    00504 #      define NATIVEFT DFNTF_CONVEXNATIVE
    00505 #    else
    00506        #error "Can't determine Convex floating point type. Use native compiler"
    00507 #    endif
    00508 #  endif
    00509 #endif
    00510 #ifndef NATIVEFT
    00511   #error "Can't determine machine number format"
    00512 #endif
    00513 /* Here are the codes for data types which we can read from files and       */
    00514 /* translate.                                                               */
    00515 /*                                                                          */
    00516 /* <[[#define]]s>=                                                          */
    00517 #define DFNT_UINT       0       /* unsigned int */
    00518 #define DFNT_SINT       1       /* short int */
    00519 #define DFNT_INT        2       /* int */
    00520 #define DFNT_UCHAR      3       /* unsigned char */
    00521 #define DFNT_CHAR       4       /* char */
    00522 #define DFNT_FLOAT      5       /* float */
    00523 #define DFNT_DOUBLE     6       /* double */
    00524 /* These typedefs define 16-bit unsigned, 32-bit unsigned, 32-bit float     */
    00525 /* and 8-bit unsigned char types respectively.  You'd need to define        */
    00526 /* [[SIXTEENBIT]] for a compiler with 16-bit ints; using [[long]] here is   */
    00527 /* wrong, for instance, on \idx{OSF/1} \idx{Alpha} systems.                 */
    00528 /*                                                                          */
    00529 /* <typedefs>=                                                              */
    00530 typedef unsigned short uint16;
    00531 #ifdef SIXTEENBIT
    00532 typedef unsigned long uint32;
    00533 #else
    00534 typedef unsigned int uint32;
    00535 #endif
    00536 typedef float float32;
    00537 typedef unsigned char uint8;
    00538 /* typedef signed char sint8; */ /* not K&R ? */
    00539 #endif

.. raw:: html

   </div>
