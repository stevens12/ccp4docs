`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

binsort.h
=========

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      binsort.h: header for binary sorting functions
    00003 
    00004      This code is distributed under the terms and conditions of the
    00005      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00006      A copy of the CCP4 licence can be obtained by writing to the
    00007      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00008 */
    00009 /****************************************************************************
    00010   binsort.h
    00011   Z130891
    00012 
    00013 binsort - key data types definition.
    00014 ****************************************************************************/
    00015 
    00016 /*** Data types definition name begining with U means unsigned... ***/
    00017 
    00018 #define CHAR        (int)1
    00019 #define UCHAR       (int)2
    00020 #define SHORT       (int)3
    00021 #define USHORT      (int)4
    00022 #define LONG        (int)5
    00023 #define ULONG       (int)6
    00024 #define FLOAT       (int)7
    00025 #define DOUBLE      (int)8
    00026 
    00027 /*** Sorting order ***/
    00028 #define ASC         (int)0      /* ascending */
    00029 #define DESC        (int)1      /* descending */

.. raw:: html

   </div>
