`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

csymlib.h File Reference
========================

C-level library for symmetry information. `More... <#_details>`__

| ``#include "ccp4_spg.h"``

`Go to the source code of this file. <csymlib_8h-source.html>`__

| 

Functions
---------

CCP4SPG \* 

`ccp4spg\_load\_by\_standard\_num <csymlib_8h.html#a1>`__ (const int
numspg)

CCP4SPG \* 

`ccp4spg\_load\_by\_ccp4\_num <csymlib_8h.html#a2>`__ (const int
ccp4numspg)

CCP4SPG \* 

`ccp4spg\_load\_by\_spgname <csymlib_8h.html#a3>`__ (const char
\*spgname)

CCP4SPG \* 

`ccp4spg\_load\_by\_ccp4\_spgname <csymlib_8h.html#a4>`__ (const char
\*ccp4spgname)

CCP4SPG \* 

`ccp4\_spgrp\_reverse\_lookup <csymlib_8h.html#a5>`__ (const int nsym1,
const ccp4\_symop \*op1)

CCP4SPG \* 

`ccp4spg\_load\_spacegroup <csymlib_8h.html#a6>`__ (const int numspg,
const int ccp4numspg, const char \*spgname, const char \*ccp4spgname,
const int nsym1, const ccp4\_symop \*op1)

void 

`ccp4spg\_mem\_tidy <csymlib_8h.html#a7>`__ (void)

int 

`symfr\_driver <csymlib_8h.html#a8>`__ (const char \*line, float
rot[][4][4])

void 

`ccp4spg\_free <csymlib_8h.html#a9>`__ (CCP4SPG \*\*sp)

void 

`ccp4spg\_register\_by\_ccp4\_num <csymlib_8h.html#a10>`__ (int numspg)

void 

`ccp4spg\_register\_by\_symops <csymlib_8h.html#a11>`__ (int nops, float
rsm[][4][4])

int 

`ccp4\_spg\_get\_centering <csymlib_8h.html#a12>`__ (const char
\*symbol\_Hall, float cent\_ops[4][3])

int 

`ccp4spg\_load\_laue <csymlib_8h.html#a13>`__ (CCP4SPG \*spacegroup,
const int nlaue)

int 

`ASU\_1b <csymlib_8h.html#a14>`__ (const int h, const int k, const int
l)

int 

`ASU\_2\_m <csymlib_8h.html#a15>`__ (const int h, const int k, const int
l)

int 

`ASU\_mmm <csymlib_8h.html#a16>`__ (const int h, const int k, const int
l)

int 

`ASU\_4\_m <csymlib_8h.html#a17>`__ (const int h, const int k, const int
l)

int 

`ASU\_4\_mmm <csymlib_8h.html#a18>`__ (const int h, const int k, const
int l)

int 

`ASU\_3b <csymlib_8h.html#a19>`__ (const int h, const int k, const int
l)

int 

`ASU\_3bm <csymlib_8h.html#a20>`__ (const int h, const int k, const int
l)

int 

`ASU\_3bmx <csymlib_8h.html#a21>`__ (const int h, const int k, const int
l)

int 

`ASU\_6\_m <csymlib_8h.html#a22>`__ (const int h, const int k, const int
l)

int 

`ASU\_6\_mmm <csymlib_8h.html#a23>`__ (const int h, const int k, const
int l)

int 

`ASU\_m3b <csymlib_8h.html#a24>`__ (const int h, const int k, const int
l)

int 

`ASU\_m3bm <csymlib_8h.html#a25>`__ (const int h, const int k, const int
l)

char \* 

`ccp4spg\_symbol\_Hall <csymlib_8h.html#a26>`__ (CCP4SPG \*sp)

ccp4\_symop 

`ccp4\_symop\_invert <csymlib_8h.html#a27>`__ (const ccp4\_symop op1)

int 

`ccp4spg\_name\_equal <csymlib_8h.html#a28>`__ (const char \*spgname1,
const char \*spgname2)

int 

`ccp4spg\_name\_equal\_to\_lib <csymlib_8h.html#a29>`__ (const char
\*spgname\_lib, const char \*spgname\_match)

char \* 

`ccp4spg\_to\_shortname <csymlib_8h.html#a30>`__ (char \*shortname,
const char \*longname)

void 

`ccp4spg\_name\_de\_colon <csymlib_8h.html#a31>`__ (char \*name)

int 

`ccp4spg\_pgname\_equal <csymlib_8h.html#a32>`__ (const char \*pgname1,
const char \*pgname2)

ccp4\_symop \* 

`ccp4spg\_norm\_trans <csymlib_8h.html#a33>`__ (ccp4\_symop \*op)

int 

`ccp4\_spgrp\_equal <csymlib_8h.html#a34>`__ (int nsym1, const
ccp4\_symop \*op1, int nsym2, const ccp4\_symop \*op2)

int 

`ccp4\_spgrp\_equal\_order <csymlib_8h.html#a35>`__ (int nsym1, const
ccp4\_symop \*op1, int nsym2, const ccp4\_symop \*op2)

int 

`ccp4\_symop\_code <csymlib_8h.html#a36>`__ (ccp4\_symop op)

int 

`ccp4\_int\_compare <csymlib_8h.html#a37>`__ (const void \*p1, const
void \*p2)

int 

`ccp4spg\_is\_in\_pm\_asu <csymlib_8h.html#a38>`__ (const CCP4SPG \*sp,
const int h, const int k, const int l)

int 

`ccp4spg\_is\_in\_asu <csymlib_8h.html#a39>`__ (const CCP4SPG \*sp,
const int h, const int k, const int l)

int 

`ccp4spg\_put\_in\_asu <csymlib_8h.html#a40>`__ (const CCP4SPG \*sp,
const int hin, const int kin, const int lin, int \*hout, int \*kout, int
\*lout)

void 

`ccp4spg\_generate\_indices <csymlib_8h.html#a41>`__ (const CCP4SPG
\*sp, const int isym, const int hin, const int kin, const int lin, int
\*hout, int \*kout, int \*lout)

float 

`ccp4spg\_phase\_shift <csymlib_8h.html#a42>`__ (const int hin, const
int kin, const int lin, const float phasin, const float trans[3], const
int isign)

int 

`ccp4spg\_do\_chb <csymlib_8h.html#a43>`__ (const float chb[3][3])

void 

`ccp4spg\_set\_centric\_zones <csymlib_8h.html#a44>`__ (CCP4SPG \*sp)

int 

`ccp4spg\_is\_centric <csymlib_8h.html#a45>`__ (const CCP4SPG \*sp,
const int h, const int k, const int l)

int 

`ccp4spg\_check\_centric\_zone <csymlib_8h.html#a46>`__ (const int
nzone, const int h, const int k, const int l)

float 

`ccp4spg\_centric\_phase <csymlib_8h.html#a47>`__ (const CCP4SPG \*sp,
const int h, const int k, const int l)

void 

`ccp4spg\_print\_centric\_zones <csymlib_8h.html#a48>`__ (const CCP4SPG
\*sp)

char \* 

`ccp4spg\_describe\_centric\_zone <csymlib_8h.html#a49>`__ (const int
nzone, char \*centric\_zone)

void 

`ccp4spg\_set\_epsilon\_zones <csymlib_8h.html#a50>`__ (CCP4SPG \*sp)

int 

`ccp4spg\_get\_multiplicity <csymlib_8h.html#a51>`__ (const CCP4SPG
\*sp, const int h, const int k, const int l)

int 

`ccp4spg\_check\_epsilon\_zone <csymlib_8h.html#a52>`__ (const int
nzone, const int h, const int k, const int l)

void 

`ccp4spg\_print\_epsilon\_zones <csymlib_8h.html#a53>`__ (const CCP4SPG
\*sp)

char \* 

`ccp4spg\_describe\_epsilon\_zone <csymlib_8h.html#a54>`__ (const int
nzone, char \*epsilon\_zone)

int 

`ccp4spg\_is\_sysabs <csymlib_8h.html#a55>`__ (const CCP4SPG \*sp, const
int h, const int k, const int l)

int 

`ccp4spg\_generate\_origins <csymlib_8h.html#a56>`__ (const char
\*namspg, const int nsym, const float rsym[][4][4], float origins[][3],
int \*polarx, int \*polary, int \*polarz, const int iprint)

void 

`ccp4spg\_print\_recip\_spgrp <csymlib_8h.html#a57>`__ (const CCP4SPG
\*sp)

void 

`ccp4spg\_print\_recip\_ops <csymlib_8h.html#a58>`__ (const CCP4SPG
\*sp)

int 

`range\_to\_limits <csymlib_8h.html#a59>`__ (const char \*range, float
limits[2])

void 

`set\_fft\_grid <csymlib_8h.html#a60>`__ (CCP4SPG \*sp, const int nxmin,
const int nymin, const int nzmin, const float sample, int \*nx, int
\*ny, int \*nz)

int 

`all\_factors\_le\_19 <csymlib_8h.html#a61>`__ (const int n)

int 

`get\_grid\_sample <csymlib_8h.html#a62>`__ (const int minsmp, const int
nmul, const float sample)

int 

`ccp4spg\_check\_symm\_cell <csymlib_8h.html#a63>`__ (int nsym, float
rsym[][4][4], float cell[6])

--------------

Detailed Description
--------------------

C-level library for symmetry information.

Functions defining the C-level API for accessing spacegroup properties.
The primary spacegroup information comes from the data file syminfo.lib

 **Author:**
    Martyn Winn

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +----------------------------+------+--------------+---------+------+--- |
| -+                                                                       |
| | int all\_factors\_le\_19   | (    | const int    |   *n*   | )    |    |
|  |                                                                       |
| +----------------------------+------+--------------+---------+------+--- |
| -+                                                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Checks whether all factors of a      |
|                                      | number n are less than or equal to   |
|                                      | 19.                                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------+---------------------- |
|                                      | --+                                  |
|                                      |     | *n*    | Number to be tested.  |
|                                      |   |                                  |
|                                      |     +--------+---------------------- |
|                                      | --+                                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, O on failure.      |
+--------------------------------------+--------------------------------------+

int ASU\_1b

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 1bar.                          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_2\_m

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 2/m.                           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_3b

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 3bar.                          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_3bm

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 3bar1m.                        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_3bmx

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 3barm.                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_4\_m

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 4/m.                           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_4\_mmm

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 4/mmm.                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_6\_m

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 6/m.                           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_6\_mmm

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group 6/mmm.                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_m3b

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group m3bar.                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_m3bm

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group m3barm.                        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ASU\_mmm

( 

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test if reflection is in asu of Laue |
|                                      | group mmm.                           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu else 0               |
+--------------------------------------+--------------------------------------+

int ccp4\_int\_compare

( 

const void \* 

  *p1*,

const void \* 

  *p2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Comparison of symmetry operators     |
|                                      | encoded as integers. In              |
|                                      | ccp4\_spgrp\_equal, this is passed   |
|                                      | to the stdlib qsort.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | --------+                            |
|                                      |     | *p1*    | pointer to first int |
|                                      | eger    |                            |
|                                      |     +---------+--------------------- |
|                                      | --------+                            |
|                                      |     | *p1*    | pointer to second in |
|                                      | teger   |                            |
|                                      |     +---------+--------------------- |
|                                      | --------+                            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     difference between integers      |
+--------------------------------------+--------------------------------------+

int ccp4\_spg\_get\_centering

( 

const char \* 

  *symbol\_Hall*,

float 

  *cent\_ops*\ [4][3]

) 

+--------------------------------------+--------------------------------------+
|                                      | Derive centering operators from Hall |
|                                      | symbol (deprecated). Centering       |
|                                      | operators are now read from          |
|                                      | syminfo.lib                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------------+----------- |
|                                      | -------------------+                 |
|                                      |     | *symbol\_Hall*    | Hall symbo |
|                                      | l for spacegroup   |                 |
|                                      |     +-------------------+----------- |
|                                      | -------------------+                 |
|                                      |     | *cent\_ops*       | centering  |
|                                      | operators          |                 |
|                                      |     +-------------------+----------- |
|                                      | -------------------+                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of centering operators (0 |
|                                      |     if none found)                   |
+--------------------------------------+--------------------------------------+

int ccp4\_spgrp\_equal

( 

int 

  *nsym1*,

const ccp4\_symop \* 

  *op1*,

int 

  *nsym2*,

const ccp4\_symop \* 

  *op2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Sort and compare two symmetry        |
|                                      | operator lists. Kevin's code. The    |
|                                      | lists are coded as ints, which are   |
|                                      | then sorted and compared. Note that  |
|                                      | no changes are made to the input     |
|                                      | operators, so that operators         |
|                                      | differing by an integral number of   |
|                                      | unit cell translations are           |
|                                      | considered unequal. If this is not   |
|                                      | what you want, normalise the         |
|                                      | operators with ccp4spg\_norm\_trans  |
|                                      | first.                               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *nsym1*    | number of symmetr |
|                                      | y operators in first list    |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *op1*      | first list of sym |
|                                      | metry operators              |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *nsym2*    | number of symmetr |
|                                      | y operators in second list   |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *op2*      | second list of sy |
|                                      | mmetry operators             |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if they are equal else 0.      |
+--------------------------------------+--------------------------------------+

int ccp4\_spgrp\_equal\_order

( 

int 

  *nsym1*,

const ccp4\_symop \* 

  *op1*,

int 

  *nsym2*,

const ccp4\_symop \* 

  *op2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Compare two symmetry operator lists. |
|                                      | Kevin's code. The lists are coded as |
|                                      | ints, which are compared. Unlike     |
|                                      | ccp4\_spgrp\_equal, the lists are    |
|                                      | not sorted, so the same operators in |
|                                      | a different order will be considered |
|                                      | unequal.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *nsym1*    | number of symmetr |
|                                      | y operators in first list    |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *op1*      | first list of sym |
|                                      | metry operators              |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *nsym2*    | number of symmetr |
|                                      | y operators in second list   |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *op2*      | second list of sy |
|                                      | mmetry operators             |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if they are equal else 0.      |
+--------------------------------------+--------------------------------------+

CCP4SPG\* ccp4\_spgrp\_reverse\_lookup

( 

const int 

  *nsym1*,

const ccp4\_symop \* 

  *op1*

) 

+--------------------------------------+--------------------------------------+
|                                      | Look up spacegroup by symmetry       |
|                                      | operators and load properties.       |
|                                      | Allocates memory for the spacegroup  |
|                                      | structure. This can be freed later   |
|                                      | by                                   |
|                                      | `ccp4spg\_free <csymlib_8h.html#a9>` |
|                                      | __\ ().                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -------------------------------+     |
|                                      |     | *nsym1*    | number of operato |
|                                      | rs (including non-primitive)   |     |
|                                      |     +------------+------------------ |
|                                      | -------------------------------+     |
|                                      |     | *op1*      | pointer to array  |
|                                      | of operators                   |     |
|                                      |     +------------+------------------ |
|                                      | -------------------------------+     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to spacegroup            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+----------------+----------+------+--- |
| -+                                                                       |
| | int ccp4\_symop\_code   | (    | ccp4\_symop    |   *op*   | )    |    |
|  |                                                                       |
| +-------------------------+------+----------------+----------+------+--- |
| -+                                                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Make an integer coding of a symmetry |
|                                      | operator. The coding takes 30 bits:  |
|                                      | 18 for the rotation and 12 for the   |
|                                      | translation.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | +                                    |
|                                      |     | *op*    | symmetry operator    |
|                                      | |                                    |
|                                      |     +---------+--------------------- |
|                                      | +                                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     int code.                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------------+------+----------------------+----- |
| ------+------+----+                                                      |
| | ccp4\_symop ccp4\_symop\_invert   | (    | const ccp4\_symop    |   *o |
| p1*   | )    |    |                                                      |
| +-----------------------------------+------+----------------------+----- |
| ------+------+----+                                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | inverts a symmetry operator. The     |
|                                      | input operator is converted to a 4 x |
|                                      | 4 matrix, inverted, and converted    |
|                                      | back.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | ---------------+                     |
|                                      |     | *ccp4\_symop*    | input symme |
|                                      | try operator   |                     |
|                                      |     +------------------+------------ |
|                                      | ---------------+                     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     inverted symmetry operator       |
+--------------------------------------+--------------------------------------+

float ccp4spg\_centric\_phase

( 

const CCP4SPG \* 

  *sp*,

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Return phase of a centric reflection |
|                                      | in the range 0.0 <= phase < 180.0.   |
|                                      | You should first check that          |
|                                      | reflection really is centric.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *h*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *k*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *l*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     phase of a centric reflection    |
+--------------------------------------+--------------------------------------+

int ccp4spg\_check\_centric\_zone

( 

const int 

  *nzone*,

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Check indices against a centric zone |
|                                      | for a given spacegroup.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -------+                             |
|                                      |     | *nzone*    | index of centric  |
|                                      | zone   |                             |
|                                      |     +------------+------------------ |
|                                      | -------+                             |
|                                      |     | *h*        | reflection index  |
|                                      |        |                             |
|                                      |     +------------+------------------ |
|                                      | -------+                             |
|                                      |     | *k*        | reflection index  |
|                                      |        |                             |
|                                      |     +------------+------------------ |
|                                      | -------+                             |
|                                      |     | *l*        | reflection index  |
|                                      |        |                             |
|                                      |     +------------+------------------ |
|                                      | -------+                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 if in zone "nzone", non-zero   |
|                                      |     otherwise                        |
+--------------------------------------+--------------------------------------+

int ccp4spg\_check\_epsilon\_zone

( 

const int 

  *nzone*,

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Check indices against an epsilon     |
|                                      | zone for a given spacegroup.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *nzone*    | index of epsilon  |
|                                      | zone (runs from 1 to 13)   |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *h*        | reflection index  |
|                                      |                            |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *k*        | reflection index  |
|                                      |                            |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *l*        | reflection index  |
|                                      |                            |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 if in zone "nzone", non-zero   |
|                                      |     otherwise                        |
+--------------------------------------+--------------------------------------+

int ccp4spg\_check\_symm\_cell

( 

int 

  *nsym*,

float 

  *rsym*\ [][4][4],

float 

  *cell*\ [6]

) 

+--------------------------------------+--------------------------------------+
|                                      | Check for consistency between cell   |
|                                      | dimensions and spacegroup. Latter is |
|                                      | identified from symmetry operators.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | -----------+                         |
|                                      |     | *nsym*    | No. of symmetry op |
|                                      | erators.   |                         |
|                                      |     +-----------+------------------- |
|                                      | -----------+                         |
|                                      |     | *rsym*    | Symmetry operators |
|                                      | .          |                         |
|                                      |     +-----------+------------------- |
|                                      | -----------+                         |
|                                      |     | *cell*    | Cell dimensions.   |
|                                      |            |                         |
|                                      |     +-----------+------------------- |
|                                      | -----------+                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if they are consistent, 0 if   |
|                                      |     there is a problem.              |
+--------------------------------------+--------------------------------------+

char\* ccp4spg\_describe\_centric\_zone

( 

const int 

  *nzone*,

char \* 

  *centric\_zone*

) 

+--------------------------------------+--------------------------------------+
|                                      | Obtain string description of centric |
|                                      | zone.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ----------------------------+        |
|                                      |     | *nzone*            | index of  |
|                                      | centric zone                |        |
|                                      |     +--------------------+---------- |
|                                      | ----------------------------+        |
|                                      |     | *centric\_zone*    | string de |
|                                      | scription of centric zone   |        |
|                                      |     +--------------------+---------- |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     string description of centric    |
|                                      |     zone                             |
+--------------------------------------+--------------------------------------+

char\* ccp4spg\_describe\_epsilon\_zone

( 

const int 

  *nzone*,

char \* 

  *epsilon\_zone*

) 

+--------------------------------------+--------------------------------------+
|                                      | Obtain string description of epsilon |
|                                      | zone.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ----------------------------+        |
|                                      |     | *nzone*            | index of  |
|                                      | epsilon zone                |        |
|                                      |     +--------------------+---------- |
|                                      | ----------------------------+        |
|                                      |     | *epsilon\_zone*    | string de |
|                                      | scription of epsilon zone   |        |
|                                      |     +--------------------+---------- |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     string description of epsilon    |
|                                      |     zone                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+----------------+-------------------+-- |
| ----+----+                                                               |
| | int ccp4spg\_do\_chb   | (    | const float    |   *chb*\ [3][3]   | ) |
|     |    |                                                               |
| +------------------------+------+----------------+-------------------+-- |
| ----+----+                                                               |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Check whether change of basis is     |
|                                      | necessary, i.e. whether the change   |
|                                      | of basis matrix is not the identity. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | ------+                              |
|                                      |     | *chb*    | change of basis mat |
|                                      | rix   |                              |
|                                      |     +----------+-------------------- |
|                                      | ------+                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if change of basis is          |
|                                      |     necessary, 0 otherwise           |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------+------+-----------------+----------+------+----+ |
| | void ccp4spg\_free   | (    | CCP4SPG \*\*    |   *sp*   | )    |    | |
| +----------------------+------+-----------------+----------+------+----+ |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Free memory associated with          |
|                                      | spacegroup.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
+--------------------------------------+--------------------------------------+

void ccp4spg\_generate\_indices

( 

const CCP4SPG \* 

  *sp*,

const int 

  *isym*,

const int 

  *hin*,

const int 

  *kin*,

const int 

  *lin*,

int \* 

  *hout*,

int \* 

  *kout*,

int \* 

  *lout*

) 

+--------------------------------------+--------------------------------------+
|                                      | Transform reflection (hin,kin,lin)   |
|                                      | according to spacegroup "sp" and     |
|                                      | operation "isym". Resultant indices  |
|                                      | are placed in (hout,kout,lout).      |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |     | *sp*      | pointer to spacegr |
|                                      | oup                           |      |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |     | *isym*    | required operation |
|                                      | , see ccp4spg\_put\_in\_asu   |      |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |     | *hin*     | input reflection i |
|                                      | ndex                          |      |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |     | *kin*     | input reflection i |
|                                      | ndex                          |      |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |     | *lin*     | input reflection i |
|                                      | ndex                          |      |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |     | *hout*    | output reflection  |
|                                      | index                         |      |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |     | *kout*    | output reflection  |
|                                      | index                         |      |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |     | *lout*    | output reflection  |
|                                      | index                         |      |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------+      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

int ccp4spg\_generate\_origins

( 

const char \* 

  *namspg*,

const int 

  *nsym*,

const float 

  *rsym*\ [][4][4],

float 

  *origins*\ [][3],

int \* 

  *polarx*,

int \* 

  *polary*,

int \* 

  *polarz*,

const int 

  *iprint*

) 

+--------------------------------------+--------------------------------------+
|                                      | Translated from Alexei Vagin's       |
|                                      | CALC\_ORIG\_PS.                      |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |     | *namspg*     | Spacegroup name |
|                                      |  for printing only.                | |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |     | *nsym*       | Input number of |
|                                      |  symmetry operators.               | |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |     | *rsym*       | Input symmetry  |
|                                      | operators.                         | |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |     | *origins*    | Array containin |
|                                      | g alternative origins on output.   | |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |     | *polarx*     | Return whether  |
|                                      | polar along x axis.                | |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |     | *polary*     | Return whether  |
|                                      | polar along y axis.                | |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |     | *polarz*     | Return whether  |
|                                      | polar along z axis.                | |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |     | *iprint*     | If true, print  |
|                                      | out list of alternative origins.   | |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of alternate origins for  |
|                                      |     spacegroup.                      |
+--------------------------------------+--------------------------------------+

int ccp4spg\_get\_multiplicity

( 

const CCP4SPG \* 

  *sp*,

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Return reflection multiplicity       |
|                                      | factor for a given hkl in a given    |
|                                      | spacegroup.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *h*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *k*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *l*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     reflection multiplicity factor   |
+--------------------------------------+--------------------------------------+

int ccp4spg\_is\_centric

( 

const CCP4SPG \* 

  *sp*,

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Function to determine whether or not |
|                                      | h,k,l is a centric reflection in     |
|                                      | spacegroup "sp".                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | -----+                               |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p    |                               |
|                                      |     +---------+--------------------- |
|                                      | -----+                               |
|                                      |     | *h*     | input reflection ind |
|                                      | ex   |                               |
|                                      |     +---------+--------------------- |
|                                      | -----+                               |
|                                      |     | *k*     | input reflection ind |
|                                      | ex   |                               |
|                                      |     +---------+--------------------- |
|                                      | -----+                               |
|                                      |     | *l*     | input reflection ind |
|                                      | ex   |                               |
|                                      |     +---------+--------------------- |
|                                      | -----+                               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if h,k,l is centric, 0 if not  |
|                                      |     centric, and -1 if there is an   |
|                                      |     error.                           |
+--------------------------------------+--------------------------------------+

int ccp4spg\_is\_in\_asu

( 

const CCP4SPG \* 

  *sp*,

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test whether reflection is in asu.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *h*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *k*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *l*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu, 0 otherwise         |
+--------------------------------------+--------------------------------------+

int ccp4spg\_is\_in\_pm\_asu

( 

const CCP4SPG \* 

  *sp*,

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test whether reflection or it's      |
|                                      | Friedel mate is in asu.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *h*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *k*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *l*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu, -1 if -h -k -l is   |
|                                      |     in asu, 0 otherwise              |
+--------------------------------------+--------------------------------------+

int ccp4spg\_is\_sysabs

( 

const CCP4SPG \* 

  *sp*,

const int 

  *h*,

const int 

  *k*,

const int 

  *l*

) 

+--------------------------------------+--------------------------------------+
|                                      | Check if reflection is a systematic  |
|                                      | absence.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *h*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *k*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *l*     | reflection index     |
|                                      |     |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if reflection is a systematic  |
|                                      |     absence, 0 otherwise.            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------------------+------+--------------+------ |
| ------------+------+----+                                                |
| | CCP4SPG\* ccp4spg\_load\_by\_ccp4\_num   | (    | const int    |   *cc |
| p4numspg*   | )    |    |                                                |
| +------------------------------------------+------+--------------+------ |
| ------------+------+----+                                                |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Look up spacegroup by CCP4 number,   |
|                                      | and load properties. Allocates       |
|                                      | memory for the spacegroup structure. |
|                                      | This can be freed later by           |
|                                      | `ccp4spg\_free <csymlib_8h.html#a9>` |
|                                      | __\ ().                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | -------------+                       |
|                                      |     | *ccp4numspg*    | CCP4 spacegr |
|                                      | oup number   |                       |
|                                      |     +-----------------+------------- |
|                                      | -------------+                       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to spacegroup            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------------------------+------+----------------- |
| -+-------------------+------+----+                                       |
| | CCP4SPG\* ccp4spg\_load\_by\_ccp4\_spgname   | (    | const char \*    |
|  |   *ccp4spgname*   | )    |    |                                       |
| +----------------------------------------------+------+----------------- |
| -+-------------------+------+----+                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Look up spacegroup by name. This is  |
|                                      | for use by CCP4 programs and is more |
|                                      | complicated than                     |
|                                      | ccp4spg\_load\_by\_spgname. For each |
|                                      | spacegroup in syminfo.lib it checks  |
|                                      | the CCP4 spacegroup name first, and  |
|                                      | then the extended Hermann Mauguin    |
|                                      | symbol. Allocates memory for the     |
|                                      | spacegroup structure. This can be    |
|                                      | freed later by                       |
|                                      | `ccp4spg\_free <csymlib_8h.html#a9>` |
|                                      | __\ ().                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | --------+                            |
|                                      |     | *ccp4spgname*    | Spacegroup  |
|                                      | name.   |                            |
|                                      |     +------------------+------------ |
|                                      | --------+                            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to spacegroup            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------------------+------+------------------+---- |
| -----------+------+----+                                                 |
| | CCP4SPG\* ccp4spg\_load\_by\_spgname   | (    | const char \*    |   * |
| spgname*   | )    |    |                                                 |
| +----------------------------------------+------+------------------+---- |
| -----------+------+----+                                                 |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Look up spacegroup by the extended   |
|                                      | Hermann Mauguin symbol. Allocates    |
|                                      | memory for the spacegroup structure. |
|                                      | This can be freed later by           |
|                                      | `ccp4spg\_free <csymlib_8h.html#a9>` |
|                                      | __\ ().                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | -----------+                         |
|                                      |     | *spgname*    | Spacegroup name |
|                                      |  in form of extended Hermann Mauguin |
|                                      |  symbol.   |                         |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | -----------+                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to spacegroup            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------------------------+------+--------------+-- |
| ------------+------+----+                                                |
| | CCP4SPG\* ccp4spg\_load\_by\_standard\_num   | (    | const int    |   |
|  *numspg*   | )    |    |                                                |
| +----------------------------------------------+------+--------------+-- |
| ------------+------+----+                                                |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Look up spacegroup in standard       |
|                                      | setting by number, and load          |
|                                      | properties. Allocates memory for the |
|                                      | spacegroup structure. This can be    |
|                                      | freed later by                       |
|                                      | `ccp4spg\_free <csymlib_8h.html#a9>` |
|                                      | __\ ().                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----+                                |
|                                      |     | *numspg*    | spacegroup numbe |
|                                      | r   |                                |
|                                      |     +-------------+----------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to spacegroup            |
+--------------------------------------+--------------------------------------+

int ccp4spg\_load\_laue

( 

CCP4SPG \* 

  *spacegroup*,

const int 

  *nlaue*

) 

+--------------------------------------+--------------------------------------+
|                                      | Load Laue data into spacegroup       |
|                                      | structure.                           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | ---------------------------+         |
|                                      |     | *nlaue*         | CCP4 code fo |
|                                      | r Laue group               |         |
|                                      |     +-----------------+------------- |
|                                      | ---------------------------+         |
|                                      |     | *spacegroup*    | Pointer to C |
|                                      | CP4 spacegroup structure   |         |
|                                      |     +-----------------+------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, 1 on failure to    |
|                                      |     load Laue data                   |
+--------------------------------------+--------------------------------------+

CCP4SPG\* ccp4spg\_load\_spacegroup

( 

const int 

  *numspg*,

const int 

  *ccp4numspg*,

const char \* 

  *spgname*,

const char \* 

  *ccp4spgname*,

const int 

  *nsym1*,

const ccp4\_symop \* 

  *op1*

) 

+--------------------------------------+--------------------------------------+
|                                      | Look up spacegroup from SYMOP. This  |
|                                      | would not normally be called         |
|                                      | directly, but via one of the         |
|                                      | wrapping functions. Allocates memory |
|                                      | for the spacegroup structure. This   |
|                                      | can be freed later by                |
|                                      | `ccp4spg\_free <csymlib_8h.html#a9>` |
|                                      | __\ ().                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *numspg*         | spacegroup  |
|                                      | number                               |
|                                      |  |                                   |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *ccp4numspg*     | CCP4 spaceg |
|                                      | roup number                          |
|                                      |  |                                   |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *spgname*        | Spacegroup  |
|                                      | name.                                |
|                                      |  |                                   |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *ccp4spgname*    | Spacegroup  |
|                                      | name.                                |
|                                      |  |                                   |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *nsym1*          | number of o |
|                                      | perators (including non-primitive)   |
|                                      |  |                                   |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *op1*            | pointer to  |
|                                      | array of operators                   |
|                                      |  |                                   |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to spacegroup            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+---------+-----+------+----+         |
| | void ccp4spg\_mem\_tidy   | (    | void    |     | )    |    |         |
| +---------------------------+------+---------+-----+------+----+         |
+--------------------------------------------------------------------------+

+-----+--------------------------------------------------------------------------------------------------------------------------------+
|     | Free all memory malloc'd from static pointers. To be called before program exit. The function can be registered with atexit.   |
+-----+--------------------------------------------------------------------------------------------------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+------+------------+------------+---- |
| --+----+                                                                 |
| | void ccp4spg\_name\_de\_colon   | (    | char \*    |   *name*   | )   |
|   |    |                                                                 |
| +---------------------------------+------+------------+------------+---- |
| --+----+                                                                 |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Function to deal with                |
|                                      | colon-specified spacegroup settings. |
|                                      | E.g. 'R 3 :H' is converted to 'H 3   |
|                                      | '. Note that spaces are returned and |
|                                      | should be dealt with by the calling  |
|                                      | function.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | -+                                   |
|                                      |     | *name*    | Spacegroup name.   |
|                                      |  |                                   |
|                                      |     +-----------+------------------- |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

int ccp4spg\_name\_equal

( 

const char \* 

  *spgname1*,

const char \* 

  *spgname2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Compare two spacegroup names.        |
|                                      | Strings are converted to upper case  |
|                                      | before making the comparison, but    |
|                                      | otherwise match must be exact.       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------+                        |
|                                      |     | *spgname1*    | First spacegro |
|                                      | up name.    |                        |
|                                      |     +---------------+--------------- |
|                                      | ------------+                        |
|                                      |     | *spgname2*    | Second spacegr |
|                                      | oup name.   |                        |
|                                      |     +---------------+--------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if they are equal else 0.      |
+--------------------------------------+--------------------------------------+

int ccp4spg\_name\_equal\_to\_lib

( 

const char \* 

  *spgname\_lib*,

const char \* 

  *spgname\_match*

) 

+--------------------------------------+--------------------------------------+
|                                      | Try to match a spacegroup name to    |
|                                      | one from SYMINFO. Blanks are removed |
|                                      | when making the comparison. Strings  |
|                                      | are converted to upper case before   |
|                                      | making the comparison. If            |
|                                      | spgname\_lib has " 1 " and           |
|                                      | spgname\_match doesn't, then strip   |
|                                      | out " 1" to do "short" comparison.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *spgname1*    | First spacegro |
|                                      | up name, assumed to be a standard on |
|                                      | e obtained at some point from SYMINF |
|                                      | O                                    |
|                                      |      |                               |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *spgname2*    | Second spacegr |
|                                      | oup name that you are trying to matc |
|                                      | h to a standard SYMINFO one. E.g. it |
|                                      |  might have been provided by the use |
|                                      | r.   |                               |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if they are equal else 0.      |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------------------+------+-------------------+----- |
| -----+------+----+                                                       |
| | ccp4\_symop\* ccp4spg\_norm\_trans   | (    | ccp4\_symop \*    |   *o |
| p*   | )    |    |                                                       |
| +--------------------------------------+------+-------------------+----- |
| -----+------+----+                                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Function to normalise translations   |
|                                      | of a symmetry operator, i.e. to      |
|                                      | ensure 0.0 <= op.trn[i] < 1.0.       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ------------+                        |
|                                      |     | *op*    | pointer to symmetry  |
|                                      | operator.   |                        |
|                                      |     +---------+--------------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Pointer to normalised symmetry   |
|                                      |     operator.                        |
+--------------------------------------+--------------------------------------+

int ccp4spg\_pgname\_equal

( 

const char \* 

  *pgname1*,

const char \* 

  *pgname2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Compare two point group names.       |
|                                      | Blanks are removed when making the   |
|                                      | comparison. Strings are converted to |
|                                      | upper case before making the         |
|                                      | comparison. Any initial "PG" is      |
|                                      | ignored.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------+                        |
|                                      |     | *pgname1*    | First point gro |
|                                      | up name.    |                        |
|                                      |     +--------------+---------------- |
|                                      | ------------+                        |
|                                      |     | *pgname2*    | Second point gr |
|                                      | oup name.   |                        |
|                                      |     +--------------+---------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if they are equal else 0.      |
+--------------------------------------+--------------------------------------+

float ccp4spg\_phase\_shift

( 

const int 

  *hin*,

const int 

  *kin*,

const int 

  *lin*,

const float 

  *phasin*,

const float 

  *trans*\ [3],

const int 

  *isign*

) 

+--------------------------------------+--------------------------------------+
|                                      | Shift phase value associated with    |
|                                      | hin,kin,lin according to translation |
|                                      | and optional sign change. Return in  |
|                                      | range 0,360.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --------------+                      |
|                                      |     | *hin*       | reflection index |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | --------------+                      |
|                                      |     | *kin*       | reflection index |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | --------------+                      |
|                                      |     | *lin*       | reflection index |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | --------------+                      |
|                                      |     | *phasin*    | Input phase.     |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | --------------+                      |
|                                      |     | *trans*     | Requested transl |
|                                      | ation         |                      |
|                                      |     +-------------+----------------- |
|                                      | --------------+                      |
|                                      |     | *isign*     | If -1, change si |
|                                      | gn of phase   |                      |
|                                      |     +-------------+----------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     shifted phase                    |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------------+------+---------------------+-- |
| --------+------+----+                                                    |
| | void ccp4spg\_print\_centric\_zones   | (    | const CCP4SPG \*    |   |
|  *sp*   | )    |    |                                                    |
| +---------------------------------------+------+---------------------+-- |
| --------+------+----+                                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Print a summary of the centric zones |
|                                      | of a spacegroup.                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------------+------+---------------------+-- |
| --------+------+----+                                                    |
| | void ccp4spg\_print\_epsilon\_zones   | (    | const CCP4SPG \*    |   |
|  *sp*   | )    |    |                                                    |
| +---------------------------------------+------+---------------------+-- |
| --------+------+----+                                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Print a summary of the epsilon zones |
|                                      | of a spacegroup.                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------------+------+---------------------+------ |
| ----+------+----+                                                        |
| | void ccp4spg\_print\_recip\_ops   | (    | const CCP4SPG \*    |   *sp |
| *   | )    |    |                                                        |
| +-----------------------------------+------+---------------------+------ |
| ----+------+----+                                                        |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Print reciprocal symops.             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------------+------+---------------------+---- |
| ------+------+----+                                                      |
| | void ccp4spg\_print\_recip\_spgrp   | (    | const CCP4SPG \*    |   * |
| sp*   | )    |    |                                                      |
| +-------------------------------------+------+---------------------+---- |
| ------+------+----+                                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Print details on reciprocal          |
|                                      | spacegroup.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

int ccp4spg\_put\_in\_asu

( 

const CCP4SPG \* 

  *sp*,

const int 

  *hin*,

const int 

  *kin*,

const int 

  *lin*,

int \* 

  *hout*,

int \* 

  *kout*,

int \* 

  *lout*

) 

+--------------------------------------+--------------------------------------+
|                                      | Place reflection (hin,kin,lin) in    |
|                                      | the asymmetric unit of spacegroup    |
|                                      | "sp". Resultant indices are placed   |
|                                      | in (hout,kout,lout).                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *sp*      | pointer to spacegr |
|                                      | oup     |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *hin*     | input reflection i |
|                                      | ndex    |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *kin*     | input reflection i |
|                                      | ndex    |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *lin*     | input reflection i |
|                                      | ndex    |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *hout*    | output reflection  |
|                                      | index   |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *kout*    | output reflection  |
|                                      | index   |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *lout*    | output reflection  |
|                                      | index   |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     "isym" if successful, 0          |
|                                      |     otherwise. "isym" = 2\*isymop -  |
|                                      |     1 for reflections placed in the  |
|                                      |     positive asu, i.e. I+ of a       |
|                                      |     Friedel pair, and "isym" =       |
|                                      |     2\*isymop for reflections placed |
|                                      |     in the negative asu, i.e. I- of  |
|                                      |     a Friedel pair. Here "isymop" is |
|                                      |     the number of the symmetry       |
|                                      |     operator used.                   |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------------------+------+--------+------------- |
| -+------+----+                                                           |
| | void ccp4spg\_register\_by\_ccp4\_num   | (    | int    |   *numspg*   |
|  | )    |    |                                                           |
| +-----------------------------------------+------+--------+------------- |
| -+------+----+                                                           |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Look up spacegroup in standard       |
|                                      | setting by number and load into      |
|                                      | static storage of csymlib\_f.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----+                                |
|                                      |     | *numspg*    | spacegroup numbe |
|                                      | r   |                                |
|                                      |     +-------------+----------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

void ccp4spg\_register\_by\_symops

( 

int 

  *nops*,

float 

  *rsm*\ [][4][4]

) 

+--------------------------------------+--------------------------------------+
|                                      | Look up spacegroup by set of         |
|                                      | symmetry operators and load into     |
|                                      | static storage of csymlib\_f.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ---+                                 |
|                                      |     | *nops*    | number of symops   |
|                                      |    |                                 |
|                                      |     +-----------+------------------- |
|                                      | ---+                                 |
|                                      |     | *rsm*     | symmetry operators |
|                                      |    |                                 |
|                                      |     +-----------+------------------- |
|                                      | ---+                                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------------+------+---------------+---------- |
| +------+----+                                                            |
| | void ccp4spg\_set\_centric\_zones   | (    | CCP4SPG \*    |   *sp*    |
| | )    |    |                                                            |
| +-------------------------------------+------+---------------+---------- |
| +------+----+                                                            |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Set up centric zones for a given     |
|                                      | spacegroup. This is called upon      |
|                                      | loading a spacegroup.                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------------+------+---------------+---------- |
| +------+----+                                                            |
| | void ccp4spg\_set\_epsilon\_zones   | (    | CCP4SPG \*    |   *sp*    |
| | )    |    |                                                            |
| +-------------------------------------+------+---------------+---------- |
| +------+----+                                                            |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Set up epsilon zones for a given     |
|                                      | spacegroup. This is called upon      |
|                                      | loading a spacegroup.                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------------+------+---------------+----------+---- |
| --+----+                                                                 |
| | char\* ccp4spg\_symbol\_Hall   | (    | CCP4SPG \*    |   *sp*   | )   |
|   |    |                                                                 |
| +--------------------------------+------+---------------+----------+---- |
| --+----+                                                                 |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Function to return Hall symbol for   |
|                                      | spacegroup.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |     | *sp*    | pointer to spacegrou |
|                                      | p   |                                |
|                                      |     +---------+--------------------- |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to Hall symbol for       |
|                                      |     spacegroup                       |
+--------------------------------------+--------------------------------------+

char\* ccp4spg\_to\_shortname

( 

char \* 

  *shortname*,

const char \* 

  *longname*

) 

+--------------------------------------+--------------------------------------+
|                                      | Function to create "short" name of   |
|                                      | spacegroup. Blanks are removed, as   |
|                                      | are " 1" elements (except for the    |
|                                      | special case of "P 1").              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ----------------------------+        |
|                                      |     | *shortname*    | String long e |
|                                      | nough to hold short name.   |        |
|                                      |     +----------------+-------------- |
|                                      | ----------------------------+        |
|                                      |     | *longname*     | Long version  |
|                                      | of spacegroup name.         |        |
|                                      |     +----------------+-------------- |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Pointer to shortname.            |
+--------------------------------------+--------------------------------------+

int get\_grid\_sample

( 

const int 

  *minsmp*,

const int 

  *nmul*,

const float 

  *sample*

) 

+--------------------------------------+--------------------------------------+
|                                      | Sets a grid sample greater than      |
|                                      | minsmp, which has no prime factors   |
|                                      | greater than 19, and contains the    |
|                                      | factor nmul.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----+             |
|                                      |     | *minsmp*    |    |             |
|                                      |     +-------------+----+             |
|                                      |     | *nmul*      |    |             |
|                                      |     +-------------+----+             |
|                                      |     | *sample*    |    |             |
|                                      |     +-------------+----+             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Grid sample or -1 on failure.    |
+--------------------------------------+--------------------------------------+

int range\_to\_limits

( 

const char \* 

  *range*,

float 

  *limits*\ [2]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert string of type 0<=y<=1/4 to  |
|                                      | 0.0-delta, 0.25+delta, where delta   |
|                                      | is set to 0.00001 Makes many         |
|                                      | assumptions about string.            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -------+                             |
|                                      |     | *range*     | input string.    |
|                                      |        |                             |
|                                      |     +-------------+----------------- |
|                                      | -------+                             |
|                                      |     | *limits*    | output range lim |
|                                      | its.   |                             |
|                                      |     +-------------+----------------- |
|                                      | -------+                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success                     |
+--------------------------------------+--------------------------------------+

void set\_fft\_grid

( 

CCP4SPG \* 

  *sp*,

const int 

  *nxmin*,

const int 

  *nymin*,

const int 

  *nzmin*,

const float 

  *sample*,

int \* 

  *nx*,

int \* 

  *ny*,

int \* 

  *nz*

) 

+--------------------------------------+--------------------------------------+
|                                      | Sets an FFT grid for a spacegroup.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *sp*        | pointer to space |
|                                      | group                |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *nxmin*     | minimum sampling |
|                                      |  on x                |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *nymin*     | minimum sampling |
|                                      |  on y                |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *nzmin*     | minimum sampling |
|                                      |  on z                |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *sample*    | default fineness |
|                                      |  of sample           |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *nx*        | returns sampling |
|                                      |  intervals along x   |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *ny*        | returns sampling |
|                                      |  intervals along y   |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *nz*        | returns sampling |
|                                      |  intervals along z   |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

int symfr\_driver

( 

const char \* 

  *line*,

float 

  *rot*\ [][4][4]

) 

+--------------------------------------+--------------------------------------+
|                                      | Generate symop matrices from         |
|                                      | description strings This would not   |
|                                      | normally be called directly, but via |
|                                      | one of the wrapping functions SYMFR2 |
|                                      | and SYMFR3 in the Fortran API.       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *line*    | null-terminated st |
|                                      | ring containing symop descriptions   |
|                                      |  |                                   |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *rot*     | array of 4x4 matri |
|                                      | ces                                  |
|                                      |  |                                   |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of symops read, or -1 on  |
|                                      |     failure                          |
+--------------------------------------+--------------------------------------+
