`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CCP4 Library Utilities
----------------------

File list
---------

-  `library\_utils.c <library__utils_8c.html>`__
-  `ccp4\_general.c <ccp4__general_8c.html>`__
-  `ccp4\_parser.c <ccp4__parser_8c.html>`__
-  `ccp4\_program.c <ccp4__program_8c.html>`__

Overview
--------

The CCP4 C-library provides many utility functions which either give
specific CCP4 functionality (e.g. traditional keyword parsing) or are
just generally useful (platform independent date).
