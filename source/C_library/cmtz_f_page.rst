`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

Fortran API to CMTZ
-------------------

File list
---------

-  `cmtzlib\_f.c <cmtzlib__f_8c.html>`__

Overview
--------

This library consists of a set of wrappers to the CMTZ library giving
the same API as the original mtzlib.f For details of the API, see the
original `documentation <../mtzlib.html>`__. This document covers some
peculiarities of the C implementation.

Batches
-------

Batch headers are held as a linked list in the `MTZ <structMTZ.html>`__
data structure. Upon reading an `MTZ <structMTZ.html>`__ file, a list is
created holding the all the batch headers from the input file
(mtz->n\_orig\_bat holds the number read in). LRBATS will return a list
of batch numbers. LRBAT can be used to read the batch headers one at a
time.

LWBAT can be used to write new or updated batch headers. Updated batch
headers are in fact added as new batch headers at the end of the list of
input batch headers. This mimics the use of RBATW/CBATW in the Fortran
routines. There are problems with this approach, and this may be
re-implemented.
