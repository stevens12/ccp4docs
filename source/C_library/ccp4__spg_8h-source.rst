`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_spg.h
===========

`Go to the documentation of this file. <ccp4__spg_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_spg.h: Data structure for symmetry information
    00003      Copyright (C) 2001  CCLRC, Martyn Winn
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00022 #ifndef __CCP4_SPG__
    00023 #define __CCP4_SPG__
    00024 static char rcsidhsp[] = "$Id$";
    00025 
    00026 #ifdef  __cplusplus
    00027 namespace CSym {
    00028 extern "C" {
    00029 #endif
    00030 
    00031 /* Kevin's symmetry operator */
    00032 
    00033 typedef struct ccp4_symop_
    00034 {
    00035   float rot[3][3];
    00036   float trn[3];
    00037 } ccp4_symop;
    00038 
    00039 typedef struct ccp4_spacegroup_
    00040 {
    00041   int spg_num;            /* true spacegroup number */
    00042   int spg_ccp4_num;       /* CCP4 spacegroup number */
    00043   char symbol_Hall[40];   /* Hall symbol */
    00044   char symbol_xHM[20];    /* Extended Hermann Mauguin symbol  */
    00045   char symbol_old[20];    /* old spacegroup name */
    00046 
    00047   char point_group[20];   /* point group name */
    00048   char crystal[20];       /* crystal system */
    00049 
    00050   int nlaue;              /* CCP4 Laue class number, inferred from asu_descr */
    00051   char laue_name[20];     /* Laue class name */
    00052   int laue_sampling[3];   /* sampling factors for FFT */
    00053 
    00054   int npatt;              /* Patterson spacegroup number, inferred from asu_descr */
    00055   char patt_name[40];     /* Patterson spacegroup name */
    00056 
    00057   int nsymop;             /* total number of symmetry operations */
    00058   int nsymop_prim;        /* number of primitive symmetry operations */
    00059   ccp4_symop *symop;      /* symmetry matrices */
    00060   ccp4_symop *invsymop;   /* inverse symmetry matrices */
    00061 
    00062   float chb[3][3];        /* change of basis matrix from file */
    00063 
    00064   char asu_descr[80];     /* asu description from file */
    00065   int (*asufn)(const int, const int, const int); /* pointer to ASU function */
    00066 
    00067   int centrics[12];       /* symop which generates centric zone, 0 if none */
    00068   int epsilon[13];        /* flag which epsilon zones are applicable */
    00069 
    00070   char mapasu_zero_descr[80];  /* origin-based map asu: description from file */
    00071   float mapasu_zero[3];   /* origin-based map asu: upper limits */
    00072 
    00073   char mapasu_ccp4_descr[80];  /* CCP4 map asu: defaults to mapasu_zero */
    00074   float mapasu_ccp4[3];   /* CCP4 map asu: upper limits */
    00075 
    00076 } CCP4SPG;
    00077 
    00078 #ifdef __cplusplus
    00079 } }
    00080 #endif
    00081 
    00082 #endif  

.. raw:: html

   </div>
