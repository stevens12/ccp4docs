`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmtzlib.h File Reference
========================

C-level library for input, output and manipulation of
`MTZ <structMTZ.html>`__ files. `More... <#_details>`__

| ``#include "ccp4_utils.h"``
| ``#include "mtzdata.h"``

`Go to the source code of this file. <cmtzlib_8h-source.html>`__

| 

Functions
---------

`MTZ <structMTZ.html>`__ \* 

`MtzGet <cmtzlib_8h.html#a1>`__ (const char \*logname, int read\_refs)

`MTZ <structMTZ.html>`__ \* 

`MtzGetUserCellTolerance <cmtzlib_8h.html#a2>`__ (const char \*logname,
int read\_refs, const double cell\_tolerance)

int 

`MtzRrefl <cmtzlib_8h.html#a3>`__
(`CCP4File <library__file_8h.html#a0>`__ \*filein, int ncol, float
\*refldata)

int 

`MtzPut <cmtzlib_8h.html#a4>`__ (`MTZ <structMTZ.html>`__ \*mtz, const
char \*logname)

`CCP4File <library__file_8h.html#a0>`__ \* 

`MtzOpenForWrite <cmtzlib_8h.html#a5>`__ (const char \*logname)

int 

`MtzWhdrLine <cmtzlib_8h.html#a6>`__
(`CCP4File <library__file_8h.html#a0>`__ \*fileout, int nitems, char
buffer[])

int 

`MtzWrefl <cmtzlib_8h.html#a7>`__
(`CCP4File <library__file_8h.html#a0>`__ \*fileout, int ncol, float
\*refldata)

int 

`MtzDeleteRefl <cmtzlib_8h.html#a8>`__ (`MTZ <structMTZ.html>`__ \*mtz,
int iref)

`MTZ <structMTZ.html>`__ \* 

`MtzMalloc <cmtzlib_8h.html#a9>`__ (int nxtal, int nset[])

int 

`MtzFree <cmtzlib_8h.html#a10>`__ (`MTZ <structMTZ.html>`__ \*mtz)

`MTZCOL <structMTZCOL.html>`__ \* 

`MtzMallocCol <cmtzlib_8h.html#a11>`__ (`MTZ <structMTZ.html>`__ \*mtz,
int nref)

int 

`MtzFreeCol <cmtzlib_8h.html#a12>`__ (`MTZCOL <structMTZCOL.html>`__
\*col)

`MTZBAT <mtzdata_8h.html#a12>`__ \* 

`MtzMallocBatch <cmtzlib_8h.html#a13>`__ (void)

int 

`MtzFreeBatch <cmtzlib_8h.html#a14>`__ (`MTZBAT <mtzdata_8h.html#a12>`__
\*batch)

char \* 

`MtzCallocHist <cmtzlib_8h.html#a15>`__ (int nhist)

int 

`MtzFreeHist <cmtzlib_8h.html#a16>`__ (char \*hist)

void 

`MtzMemTidy <cmtzlib_8h.html#a17>`__ (void)

int 

`MtzNbat <cmtzlib_8h.html#a18>`__ (const `MTZ <structMTZ.html>`__ \*mtz)

int 

`MtzNref <cmtzlib_8h.html#a19>`__ (const `MTZ <structMTZ.html>`__ \*mtz)

int 

`MtzSpacegroupNumber <cmtzlib_8h.html#a20>`__ (const
`MTZ <structMTZ.html>`__ \*mtz)

int 

`MtzResLimits <cmtzlib_8h.html#a21>`__ (const `MTZ <structMTZ.html>`__
\*mtz, float \*minres, float \*maxres)

int 

`MtzNxtal <cmtzlib_8h.html#a22>`__ (const `MTZ <structMTZ.html>`__
\*mtz)

int 

`MtzNumActiveXtal <cmtzlib_8h.html#a23>`__ (const
`MTZ <structMTZ.html>`__ \*mtz)

`MTZXTAL <structMTZXTAL.html>`__ \*\* 

`MtzXtals <cmtzlib_8h.html#a24>`__ (`MTZ <structMTZ.html>`__ \*mtz)

`MTZXTAL <structMTZXTAL.html>`__ \* 

`MtzIxtal <cmtzlib_8h.html#a25>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const int ixtal)

char \* 

`MtzXtalPath <cmtzlib_8h.html#a26>`__ (const
`MTZXTAL <structMTZXTAL.html>`__ \*xtal)

`MTZXTAL <structMTZXTAL.html>`__ \* 

`MtzXtalLookup <cmtzlib_8h.html#a27>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const char \*label)

`MTZXTAL <structMTZXTAL.html>`__ \* 

`MtzAddXtal <cmtzlib_8h.html#a28>`__ (`MTZ <structMTZ.html>`__ \*mtz,
const char \*xname, const char \*pname, const float cell[6])

int 

`MtzNsetsInXtal <cmtzlib_8h.html#a29>`__ (const
`MTZXTAL <structMTZXTAL.html>`__ \*xtal)

int 

`MtzNumActiveSetsInXtal <cmtzlib_8h.html#a30>`__ (const
`MTZ <structMTZ.html>`__ \*mtz, const `MTZXTAL <structMTZXTAL.html>`__
\*xtal)

`MTZSET <structMTZSET.html>`__ \*\* 

`MtzSetsInXtal <cmtzlib_8h.html#a31>`__
(`MTZXTAL <structMTZXTAL.html>`__ \*xtal)

`MTZSET <structMTZSET.html>`__ \* 

`MtzIsetInXtal <cmtzlib_8h.html#a32>`__ (const
`MTZXTAL <structMTZXTAL.html>`__ \*xtal, const int iset)

int 

`MtzNset <cmtzlib_8h.html#a33>`__ (const `MTZ <structMTZ.html>`__ \*mtz)

int 

`MtzNumActiveSet <cmtzlib_8h.html#a34>`__ (const
`MTZ <structMTZ.html>`__ \*mtz)

`MTZXTAL <structMTZXTAL.html>`__ \* 

`MtzSetXtal <cmtzlib_8h.html#a35>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const `MTZSET <structMTZSET.html>`__ \*set)

char \* 

`MtzSetPath <cmtzlib_8h.html#a36>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const `MTZSET <structMTZSET.html>`__ \*set)

`MTZSET <structMTZSET.html>`__ \* 

`MtzSetLookup <cmtzlib_8h.html#a37>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const char \*label)

`MTZSET <structMTZSET.html>`__ \* 

`MtzAddDataset <cmtzlib_8h.html#a38>`__ (`MTZ <structMTZ.html>`__ \*mtz,
`MTZXTAL <structMTZXTAL.html>`__ \*xtl, const char \*dname, const float
wavelength)

int 

`MtzNcolsInSet <cmtzlib_8h.html#a39>`__ (const
`MTZSET <structMTZSET.html>`__ \*set)

int 

`MtzNumActiveColsInSet <cmtzlib_8h.html#a40>`__ (const
`MTZSET <structMTZSET.html>`__ \*set)

int 

`MtzNumSourceColsInSet <cmtzlib_8h.html#a41>`__ (const
`MTZSET <structMTZSET.html>`__ \*set)

int 

`MtzNbatchesInSet <cmtzlib_8h.html#a42>`__ (const
`MTZ <structMTZ.html>`__ \*mtz, const `MTZSET <structMTZSET.html>`__
\*set)

`MTZCOL <structMTZCOL.html>`__ \*\* 

`MtzColsInSet <cmtzlib_8h.html#a43>`__ (`MTZSET <structMTZSET.html>`__
\*set)

`MTZCOL <structMTZCOL.html>`__ \* 

`MtzIcolInSet <cmtzlib_8h.html#a44>`__ (const
`MTZSET <structMTZSET.html>`__ \*set, const int icol)

`MTZCOL <structMTZCOL.html>`__ \* 

`MtzAddColumn <cmtzlib_8h.html#a45>`__ (`MTZ <structMTZ.html>`__ \*mtz,
`MTZSET <structMTZSET.html>`__ \*set, const char \*label, const char
\*type)

int 

`MtzAssignHKLtoBase <cmtzlib_8h.html#a46>`__ (`MTZ <structMTZ.html>`__
\*mtz)

int 

`MtzAssignColumn <cmtzlib_8h.html#a47>`__ (`MTZ <structMTZ.html>`__
\*mtz, `MTZCOL <structMTZCOL.html>`__ \*col, const char crystal\_name[],
const char dataset\_name[])

int 

`MtzToggleColumn <cmtzlib_8h.html#a48>`__
(`MTZCOL <structMTZCOL.html>`__ \*col)

`MTZSET <structMTZSET.html>`__ \* 

`MtzColSet <cmtzlib_8h.html#a49>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const `MTZCOL <structMTZCOL.html>`__ \*col)

int 

`MtzNcol <cmtzlib_8h.html#a50>`__ (const `MTZ <structMTZ.html>`__ \*mtz)

int 

`MtzNumActiveCol <cmtzlib_8h.html#a51>`__ (const
`MTZ <structMTZ.html>`__ \*mtz)

int 

`MtzNumSourceCol <cmtzlib_8h.html#a52>`__ (const
`MTZ <structMTZ.html>`__ \*mtz)

char \* 

`MtzColPath <cmtzlib_8h.html#a53>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const `MTZCOL <structMTZCOL.html>`__ \*col)

int 

`MtzRJustPath <cmtzlib_8h.html#a54>`__ (char \*path, const char
\*partial, const int njust)

int 

`MtzPathMatch <cmtzlib_8h.html#a55>`__ (const char \*path1, const char
\*path2)

`MTZCOL <structMTZCOL.html>`__ \* 

`MtzColLookup <cmtzlib_8h.html#a56>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const char \*label)

char \* 

`MtzColType <cmtzlib_8h.html#a57>`__ (`MTZCOL <structMTZCOL.html>`__
\*col)

void 

`MtzDebugHierarchy <cmtzlib_8h.html#a58>`__ (const
`MTZ <structMTZ.html>`__ \*mtz)

int 

`MtzListColumn <cmtzlib_8h.html#a59>`__ (const `MTZ <structMTZ.html>`__
\*mtz, char clabs[][31], char ctyps[][3], int csetid[])

int 

`MtzListInputColumn <cmtzlib_8h.html#a60>`__ (const
`MTZ <structMTZ.html>`__ \*mtz, char clabs[][31], char ctyps[][3], int
csetid[])

int 

`MtzFindInd <cmtzlib_8h.html#a61>`__ (const `MTZ <structMTZ.html>`__
\*mtz, int \*ind\_xtal, int \*ind\_set, int ind\_col[3])

float 

`MtzInd2reso <cmtzlib_8h.html#a62>`__ (const int in[3], const double
coefhkl[6])

int 

`MtzHklcoeffs <cmtzlib_8h.html#a63>`__ (const float cell[6], double
coefhkl[6])

int 

`MtzArrayToBatch <cmtzlib_8h.html#a64>`__ (const int \*intbuf, const
float \*fltbuf, `MTZBAT <mtzdata_8h.html#a12>`__ \*batch)

int 

`MtzBatchToArray <cmtzlib_8h.html#a65>`__
(`MTZBAT <mtzdata_8h.html#a12>`__ \*batch, int \*intbuf, float \*fltbuf)

`MTZBAT <mtzdata_8h.html#a12>`__ \* 

`sort\_batches <cmtzlib_8h.html#a66>`__
(`MTZBAT <mtzdata_8h.html#a12>`__ \*batch, int numbat)

int 

`ccp4\_lrtitl <cmtzlib_8h.html#a67>`__ (const `MTZ <structMTZ.html>`__
\*mtz, char \*title)

int 

`ccp4\_lrhist <cmtzlib_8h.html#a68>`__ (const `MTZ <structMTZ.html>`__
\*mtz, char history[][MTZRECORDLENGTH], int nlines)

int 

`ccp4\_lrsort <cmtzlib_8h.html#a69>`__ (const `MTZ <structMTZ.html>`__
\*mtz, int isort[5])

int 

`ccp4\_lrbats <cmtzlib_8h.html#a70>`__ (const `MTZ <structMTZ.html>`__
\*mtz, int \*nbatx, int batchx[])

int 

`ccp4\_lrcell <cmtzlib_8h.html#a71>`__ (const
`MTZXTAL <structMTZXTAL.html>`__ \*xtl, float cell[])

int 

`ccp4\_lrsymi <cmtzlib_8h.html#a72>`__ (const `MTZ <structMTZ.html>`__
\*mtz, int \*nsympx, char \*ltypex, int \*nspgrx, char \*spgrnx, char
\*pgnamx)

int 

`ccp4\_lrsymm <cmtzlib_8h.html#a73>`__ (const `MTZ <structMTZ.html>`__
\*mtz, int \*nsymx, float rsymx[192][4][4])

int 

`MtzParseLabin <cmtzlib_8h.html#a74>`__ (char \*labin\_line, const char
prog\_labels[][31], const int nlprgi, char user\_labels[][2][31])

`MTZCOL <structMTZCOL.html>`__ \*\* 

`ccp4\_lrassn <cmtzlib_8h.html#a75>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const char labels[][31], const int nlabels, char types[][3])

int 

`ccp4\_lridx <cmtzlib_8h.html#a76>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const `MTZSET <structMTZSET.html>`__ \*set, char
crystal\_name[64], char dataset\_name[64], char project\_name[64], int
\*isets, float datcell[6], float \*datwave)

int 

`ccp4\_lrrefl <cmtzlib_8h.html#a77>`__ (const `MTZ <structMTZ.html>`__
\*mtz, float \*resol, float adata[], int logmss[], int iref)

int 

`ccp4\_lrreff <cmtzlib_8h.html#a78>`__ (const `MTZ <structMTZ.html>`__
\*mtz, float \*resol, float adata[], int logmss[], const
`MTZCOL <structMTZCOL.html>`__ \*lookup[], const int ncols, const int
iref)

int 

`ccp4\_ismnf <cmtzlib_8h.html#a79>`__ (const `MTZ <structMTZ.html>`__
\*mtz, const float datum)

int 

`ccp4\_lhprt <cmtzlib_8h.html#a80>`__ (const `MTZ <structMTZ.html>`__
\*mtz, int iprint)

int 

`ccp4\_lhprt\_adv <cmtzlib_8h.html#a81>`__ (const
`MTZ <structMTZ.html>`__ \*mtz, int iprint)

int 

`ccp4\_lrbat <cmtzlib_8h.html#a82>`__ (`MTZBAT <mtzdata_8h.html#a12>`__
\*batch, float \*buf, char \*charbuf, int iprint)

int 

`MtzPrintBatchHeader <cmtzlib_8h.html#a83>`__ (const
`MTZBAT <mtzdata_8h.html#a12>`__ \*batch)

int 

`ccp4\_lwtitl <cmtzlib_8h.html#a84>`__ (`MTZ <structMTZ.html>`__ \*mtz,
const char \*ftitle, int flag)

int 

`MtzSetSortOrder <cmtzlib_8h.html#a85>`__ (`MTZ <structMTZ.html>`__
\*mtz, `MTZCOL <structMTZCOL.html>`__ \*colsort[5])

int 

`MtzAddHistory <cmtzlib_8h.html#a86>`__ (`MTZ <structMTZ.html>`__ \*mtz,
const char history[][MTZRECORDLENGTH], const int nlines)

int 

`ccp4\_lwsymm <cmtzlib_8h.html#a87>`__ (`MTZ <structMTZ.html>`__ \*mtz,
int nsymx, int nsympx, float rsymx[192][4][4], char ltypex[], int
nspgrx, char spgrnx[], char pgnamx[])

 `MTZCOL <structMTZCOL.html>`__ \*\* 

**ccp4\_lwassn** (`MTZ <structMTZ.html>`__ \*mtz, const char
labels[][31], const int nlabels, const char types[][3], const int
iappnd)

 int 

**ccp4\_lwidx** (`MTZ <structMTZ.html>`__ \*mtz, const char
crystal\_name[], const char dataset\_name[], const char project\_name[],
const float datcell[6], const float \*datwave)

int 

`ccp4\_lwrefl <cmtzlib_8h.html#a90>`__ (`MTZ <structMTZ.html>`__ \*mtz,
const float adata[], `MTZCOL <structMTZCOL.html>`__ \*lookup[], const
int ncol, const int iref)

int 

`ccp4\_lwbat <cmtzlib_8h.html#a91>`__ (`MTZ <structMTZ.html>`__ \*mtz,
`MTZBAT <mtzdata_8h.html#a12>`__ \*batch, const int batno, const float
\*buf, const char \*charbuf)

 int 

**ccp4\_lwbsetid** (`MTZ <structMTZ.html>`__ \*mtz,
`MTZBAT <mtzdata_8h.html#a12>`__ \*batch, const char xname[], const char
dname[])

--------------

Detailed Description
--------------------

C-level library for input, output and manipulation of
`MTZ <structMTZ.html>`__ files.

Functions defining the C-level API for accessing
`MTZ <structMTZ.html>`__ files. MtzGet and MtzPut read and write
`MTZ <structMTZ.html>`__ files to/from a data structure defined in
`mtzdata.h <mtzdata_8h.html>`__ Other functions allow one to access data
structure members, and to manipulate the structure and the values of
structure members. Functions with names beginning ``ccp4_lr`` or
``ccp4_lw`` are primarily to support the Fortran API.

 **Author:**
    Martyn Winn

--------------

Function Documentation
----------------------

int ccp4\_ismnf

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const float 

  *datum*

) 

+--------------------------------------+--------------------------------------+
|                                      | Checks whether a particular          |
|                                      | reflection value represents missing  |
|                                      | data.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *mtz*      | Pointer to the `M |
|                                      | TZ <structMTZ.html>`__ struct, which |
|                                      |  holds the value representing missin |
|                                      | g data (the Missing Number Flag) aga |
|                                      | inst which the input datum is compar |
|                                      | ed.   |                              |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *datum*    | Reflection value  |
|                                      | to be checked.                       |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Returns 1 is datum is the MNF,   |
|                                      |     and 0 otherwise.                 |
+--------------------------------------+--------------------------------------+

int ccp4\_lhprt

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

int 

  *iprint*

) 

+--------------------------------------+--------------------------------------+
|                                      | Function to print header information |
|                                      | in traditional format.               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *mtz*       | Pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct   |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *iprint*    | Print level      |
|                                      |                              |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4\_lhprt\_adv

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

int 

  *iprint*

) 

+--------------------------------------+--------------------------------------+
|                                      | Function to print header information |
|                                      | in format appropriate to data        |
|                                      | structure hierarchy.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *mtz*       | Pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct   |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *iprint*    | Print level      |
|                                      |                              |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

`MTZCOL <structMTZCOL.html>`__\ \*\* ccp4\_lrassn

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const char 

  *labels*\ [][31],

const int 

  *nlabels*,

char 

  *types*\ [][3]

) 

+--------------------------------------+--------------------------------------+
|                                      | Finds columns in an                  |
|                                      | `MTZ <structMTZ.html>`__ struct      |
|                                      | according to column labels. Column   |
|                                      | types are checked for agreement      |
|                                      | between requested type (in argument  |
|                                      | 'types') and file type. If requested |
|                                      | type is blank, file type is returned |
|                                      | in argument 'types'. Note, this      |
|                                      | function is different from Fortranic |
|                                      | LRASSN, in that any conversion from  |
|                                      | program labels to user labels should |
|                                      | have been done previously.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------------------------+        |
|                                      |     | *mtz*        | Pointer to `MTZ |
|                                      |  <structMTZ.html>`__ struct.         |
|                                      |                             |        |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------------------------+        |
|                                      |     | *labels*     | Input array of  |
|                                      | column labels to be found in `MTZ <s |
|                                      | tructMTZ.html>`__ struct.   |        |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------------------------+        |
|                                      |     | *nlabels*    | Number of colum |
|                                      | ns to be found.                      |
|                                      |                             |        |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------------------------+        |
|                                      |     | *types*      | Input array of  |
|                                      | column types of columns to be found. |
|                                      |                             |        |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Array of pointers to columns in  |
|                                      |     `MTZ <structMTZ.html>`__ struct. |
|                                      |     Array element is NULL if column  |
|                                      |     not found.                       |
+--------------------------------------+--------------------------------------+

int ccp4\_lrbat

( 

`MTZBAT <mtzdata_8h.html#a12>`__ \* 

  *batch*,

float \* 

  *buf*,

char \* 

  *charbuf*,

int 

  *iprint*

) 

+--------------------------------------+--------------------------------------+
|                                      | Function to return batch header data |
|                                      | for a specified batch.               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *batch*      | Pointer to requ |
|                                      | ested batch.                         |
|                                      |           |                          |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *buf*        | On return, real |
|                                      |  and integer batch data.             |
|                                      |           |                          |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *charbuf*    | On return, char |
|                                      | acter batch data (title and axes nam |
|                                      | es).      |                          |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *iprint*     | =0 no printing, |
|                                      |  =1 print title only, >1 print full  |
|                                      | header.   |                          |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4\_lrbats

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

int \* 

  *nbatx*,

int 

  *batchx*\ []

) 

+--------------------------------------+--------------------------------------+
|                                      | Get batch numbers from               |
|                                      | `MTZ <structMTZ.html>`__ structure.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *mtz*       | Pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct.   |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *nbatx*     | Number of batche |
|                                      | s in input file.              |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *batchx*    | Returned array o |
|                                      | f batch numbers.              |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of batches.               |
+--------------------------------------+--------------------------------------+

int ccp4\_lrcell

( 

const `MTZXTAL <structMTZXTAL.html>`__ \* 

  *xtl*,

float 

  *cell*\ []

) 

+--------------------------------------+--------------------------------------+
|                                      | Get cell dimensions for a particular |
|                                      | crystal.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *xtl*     | Pointer to crystal |
|                                      | .       |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |     | *cell*    | Output cell dimens |
|                                      | ions.   |                            |
|                                      |     +-----------+------------------- |
|                                      | --------+                            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure.      |
+--------------------------------------+--------------------------------------+

int ccp4\_lrhist

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

char 

  *history*\ [][MTZRECORDLENGTH],

int 

  *nlines*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get history lines from               |
|                                      | `MTZ <structMTZ.html>`__ structure.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | -------------------------------+     |
|                                      |     | *mtz*        | Pointer to `MTZ |
|                                      |  <structMTZ.html>`__ struct.   |     |
|                                      |     +--------------+---------------- |
|                                      | -------------------------------+     |
|                                      |     | *history*    | Returned histor |
|                                      | y lines.                       |     |
|                                      |     +--------------+---------------- |
|                                      | -------------------------------+     |
|                                      |     | *nlines*     | Requested numbe |
|                                      | r of history lines.            |     |
|                                      |     +--------------+---------------- |
|                                      | -------------------------------+     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Actual number of history lines   |
|                                      |     returned.                        |
+--------------------------------------+--------------------------------------+

int ccp4\_lridx

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const `MTZSET <structMTZSET.html>`__ \* 

  *set*,

char 

  *crystal\_name*\ [64],

char 

  *dataset\_name*\ [64],

char 

  *project\_name*\ [64],

int \* 

  *isets*,

float 

  *datcell*\ [6],

float \* 

  *datwave*

) 

+--------------------------------------+--------------------------------------+
|                                      | Report information on a particular   |
|                                      | dataset. This represents the         |
|                                      | collection of data held in one       |
|                                      | series of dataset records in the     |
|                                      | `MTZ <structMTZ.html>`__ header. It  |
|                                      | is mainly useful for supporting old  |
|                                      | Fortran calls.                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *mtz*              | pointer t |
|                                      | o `MTZ <structMTZ.html>`__ struct    |
|                                      |          |                           |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *set*              | pointer t |
|                                      | o dataset                            |
|                                      |          |                           |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *crystal\_name*    | Crystal n |
|                                      | ame                                  |
|                                      |          |                           |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *dataset\_name*    | Dataset n |
|                                      | ame                                  |
|                                      |          |                           |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *project\_name*    | Project n |
|                                      | ame                                  |
|                                      |          |                           |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *isets*            | Dataset I |
|                                      | D.                                   |
|                                      |          |                           |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *datcell*          | Cell dime |
|                                      | nsions of crystal that dataset belon |
|                                      | gs to.   |                           |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *datwave*          | X-ray wav |
|                                      | elength associated with dataset.     |
|                                      |          |                           |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4\_lrreff

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

float \* 

  *resol*,

float 

  *adata*\ [],

int 

  *logmss*\ [],

const `MTZCOL <structMTZCOL.html>`__ \* 

  *lookup*\ [],

const int 

  *ncols*,

const int 

  *iref*

) 

+--------------------------------------+--------------------------------------+
|                                      | Returns iref'th reflection from file |
|                                      | held in `MTZ <structMTZ.html>`__     |
|                                      | struct mtz. Returns data for certain |
|                                      | columns held in input file, as       |
|                                      | specified by the column pointers     |
|                                      | held in lookup. In "in-memory" mode, |
|                                      | reflection data is taken from arrays |
|                                      | held in memory. In the traditional   |
|                                      | file-based mode, a reflection record |
|                                      | is read from the input file.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *mtz*       | pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct           |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *resol*     | resolution of re |
|                                      | flection (output).                   |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *adata*     | array of request |
|                                      | ed values (output).                  |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *logmss*    | array of flags f |
|                                      | or missing data (output). A value of |
|                                      |  1 indicates a Missing Number Flag,  |
|                                      | and a value of 0 indicates usable da |
|                                      | ta.   |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *lookup*    | array of pointer |
|                                      | s to requested columns               |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *ncols*     | number of reques |
|                                      | ted columns                          |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *iref*      | index of request |
|                                      | ed reflection (starting at 1).       |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if past last reflection, else  |
|                                      |     0                                |
+--------------------------------------+--------------------------------------+

int ccp4\_lrrefl

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

float \* 

  *resol*,

float 

  *adata*\ [],

int 

  *logmss*\ [],

int 

  *iref*

) 

+--------------------------------------+--------------------------------------+
|                                      | Returns iref'th reflection from file |
|                                      | held in `MTZ <structMTZ.html>`__     |
|                                      | struct mtz. Returns data for all     |
|                                      | columns held in input file, in the   |
|                                      | order that they are held in the      |
|                                      | source file. The value of            |
|                                      | col->source can be used to find      |
|                                      | particular columns. In "in-memory"   |
|                                      | mode, reflection data is taken from  |
|                                      | arrays held in memory. In the        |
|                                      | traditional file-based mode, a       |
|                                      | reflection record is read from the   |
|                                      | input file.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *mtz*       | pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct           |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *resol*     | resolution of re |
|                                      | flection (output).                   |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *adata*     | array of request |
|                                      | ed values (output).                  |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *logmss*    | array of flags f |
|                                      | or missing data (output). A value of |
|                                      |  1 indicates a Missing Number Flag,  |
|                                      | and a value of 0 indicates usable da |
|                                      | ta.   |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *iref*      | index of request |
|                                      | ed reflection (starting at 1).       |
|                                      |                                      |
|                                      |                                      |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if past last reflection, else  |
|                                      |     0                                |
+--------------------------------------+--------------------------------------+

int ccp4\_lrsort

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

int 

  *isort*\ [5]

) 

+--------------------------------------+--------------------------------------+
|                                      | Get sort order from                  |
|                                      | `MTZ <structMTZ.html>`__ structure.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *mtz*      | Pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct.   |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *isort*    | Returned sort ord |
|                                      | er.                          |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4\_lrsymi

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

int \* 

  *nsympx*,

char \* 

  *ltypex*,

int \* 

  *nspgrx*,

char \* 

  *spgrnx*,

char \* 

  *pgnamx*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get spacegroup information as held   |
|                                      | in `MTZ <structMTZ.html>`__ header.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *mtz*       | Pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct.   |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *nsympx*    | Number of primit |
|                                      | ive symmetry operators.       |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *ltypex*    | Lattice type (P, |
|                                      | A,B,C,I,F,R).                 |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *nspgrx*    | Spacegroup numbe |
|                                      | r.                            |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *spgrnx*    | Spacegroup name. |
|                                      |                               |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |     | *pgnamx*    | Pointgroup name. |
|                                      |                               |      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------+      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Spacegroup number.               |
+--------------------------------------+--------------------------------------+

int ccp4\_lrsymm

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

int \* 

  *nsymx*,

float 

  *rsymx*\ [192][4][4]

) 

+--------------------------------------+--------------------------------------+
|                                      | Get symmetry matrices from           |
|                                      | `MTZ <structMTZ.html>`__ structure.  |
|                                      | Note: ordering of matrices in rsymx  |
|                                      | was changed in April 2004.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *mtz*      | Pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct.           |
|                                      |                                      |
|                                      |                                      |
|                                      |                                 |    |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *nsymx*    | Number of symmetr |
|                                      | y operators held in `MTZ <structMTZ. |
|                                      | html>`__ header.                     |
|                                      |                                      |
|                                      |                                 |    |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *rsymx*    | Symmetry operator |
|                                      | s as 4 x 4 matrices, in the order th |
|                                      | ey are held in the `MTZ <structMTZ.h |
|                                      | tml>`__ header. Each matrix has tran |
|                                      | slations in elements [\*][3].   |    |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of symmetry operators.    |
+--------------------------------------+--------------------------------------+

int ccp4\_lrtitl

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

char \* 

  *title*

) 

+--------------------------------------+--------------------------------------+
|                                      | Returns title of                     |
|                                      | `MTZ <structMTZ.html>`__ structure   |
|                                      | 'mtz'                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |     | *mtz*      | pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct            |
|                                      | |                                    |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |     | *title*    | `MTZ <structMTZ.h |
|                                      | tml>`__ title as character string    |
|                                      | |                                    |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     length of title excluding        |
|                                      |     trailing blanks and terminating  |
|                                      |     null character.                  |
+--------------------------------------+--------------------------------------+

int ccp4\_lwbat

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

`MTZBAT <mtzdata_8h.html#a12>`__ \* 

  *batch*,

const int 

  *batno*,

const float \* 

  *buf*,

const char \* 

  *charbuf*

) 

+--------------------------------------+--------------------------------------+
|                                      | Write new batch information to       |
|                                      | 'batch' or if 'batch' is NULL create |
|                                      | new batch header with batch number   |
|                                      | 'batno'. If you try to create more   |
|                                      | than one new batch header with the   |
|                                      | same batch number, the function will |
|                                      | complain and return. It is OK to     |
|                                      | create a new batch with the same     |
|                                      | number as one read from file - this  |
|                                      | is the mechanism for changing batch  |
|                                      | headers.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *mtz*        | pointer to `MTZ |
|                                      |  <structMTZ.html>`__ struct   |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *batch*      | pointer to batc |
|                                      | h                             |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *batno*      | batch number    |
|                                      |                               |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *buf*        | pointer to batc |
|                                      | h array                       |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *charbuf*    | pointer to char |
|                                      | acter batch array             |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4\_lwrefl

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

const float 

  *adata*\ [],

`MTZCOL <structMTZCOL.html>`__ \* 

  *lookup*\ [],

const int 

  *ncol*,

const int 

  *iref*

) 

+--------------------------------------+--------------------------------------+
|                                      | Function to output reflection values |
|                                      | for iref'th reflection. The          |
|                                      | reflection values are provided in an |
|                                      | array "adata". The value in adata[i] |
|                                      | will be assigned to the              |
|                                      | `MTZ <structMTZ.html>`__ column      |
|                                      | pointed to by lookup[i]. Typically,  |
|                                      | lookup[i] is a pointer returned from |
|                                      | an earlier call to                   |
|                                      | `MtzAddColumn <cmtzlib_8h.html#a45>` |
|                                      | __\ ().                              |
|                                      | In "in-memory" mode, values are      |
|                                      | added/updated to column data held in |
|                                      | memory. In the traditional           |
|                                      | file-based mode, a reflection record |
|                                      | is written to file. This function    |
|                                      | will also update the column ranges   |
|                                      | and the crystal/file resolution      |
|                                      | limits.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *mtz*       | pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct           |
|                                      |     |                                |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *adata*     | array of reflect |
|                                      | ion column values.                   |
|                                      |     |                                |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *lookup*    | array of pointer |
|                                      | s to columns.                        |
|                                      |     |                                |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *ncol*      | number of column |
|                                      | s.                                   |
|                                      |     |                                |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *iref*      | Reflection numbe |
|                                      | r such that 1st reflection is iref=1 |
|                                      | .   |                                |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4\_lwsymm

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

int 

  *nsymx*,

int 

  *nsympx*,

float 

  *rsymx*\ [192][4][4],

char 

  *ltypex*\ [],

int 

  *nspgrx*,

char 

  *spgrnx*\ [],

char 

  *pgnamx*\ []

) 

+--------------------------------------+--------------------------------------+
|                                      | Write or update symmetry information |
|                                      | for `MTZ <structMTZ.html>`__ header. |
|                                      | This provides support for the        |
|                                      | Fortran API, and is not particularly |
|                                      | convenient for C programs. Note:     |
|                                      | ordering of matrices in rsymx was    |
|                                      | changed in November 2003.            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *mtz*       | pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct           |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nsymx*     | number of symmet |
|                                      | ry operators                         |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nsympx*    | number of primit |
|                                      | ive symmetry operators               |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *rsymx*     | array of symmetr |
|                                      | y operators (dimensions ordered in C |
|                                      |  convention, with translations in el |
|                                      | ements [\*][3])   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *ltypex*    | lattice type     |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nspgrx*    | spacegroup numbe |
|                                      | r                                    |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *spgrnx*    | spacegroup name  |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *pgnamx*    | point group name |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4\_lwtitl

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

const char \* 

  *ftitle*,

int 

  *flag*

) 

+--------------------------------------+--------------------------------------+
|                                      | Write header title for later output  |
|                                      | to file.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------+                        |
|                                      |     | *mtz*       | Pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct.          |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------+                        |
|                                      |     | *ftitle*    | Title string.    |
|                                      |                                      |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------+                        |
|                                      |     | *flag*      | If 0 overwrite e |
|                                      | xisting title, else append to existi |
|                                      | ng title.   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------+                        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

`MTZCOL <structMTZCOL.html>`__\ \* MtzAddColumn

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

`MTZSET <structMTZSET.html>`__ \* 

  *set*,

const char \* 

  *label*,

const char \* 

  *type*

) 

+--------------------------------------+--------------------------------------+
|                                      | Add a column to dataset set and      |
|                                      | create + fill with NAN               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *mtz*      | pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct   |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *set*      | pointer to datase |
|                                      | t                           |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *label*    | Column label      |
|                                      |                             |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *type*     | Column type       |
|                                      |                             |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to column                |
+--------------------------------------+--------------------------------------+

`MTZSET <structMTZSET.html>`__\ \* MtzAddDataset

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

`MTZXTAL <structMTZXTAL.html>`__ \* 

  *xtl*,

const char \* 

  *dname*,

const float 

  *wavelength*

) 

+--------------------------------------+--------------------------------------+
|                                      | Add a dataset to crystal xtl         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | ----------------------------------+  |
|                                      |     | *mtz*           | pointer to ` |
|                                      | MTZ <structMTZ.html>`__ struct.   |  |
|                                      |     +-----------------+------------- |
|                                      | ----------------------------------+  |
|                                      |     | *xtl*           | pointer to c |
|                                      | rystal struct.                    |  |
|                                      |     +-----------------+------------- |
|                                      | ----------------------------------+  |
|                                      |     | *dname*         | Dataset name |
|                                      |                                   |  |
|                                      |     +-----------------+------------- |
|                                      | ----------------------------------+  |
|                                      |     | *wavelength*    | X-ray wavele |
|                                      | ngth of dataset                   |  |
|                                      |     +-----------------+------------- |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to set                   |
+--------------------------------------+--------------------------------------+

int MtzAddHistory

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

const char 

  *history*\ [][MTZRECORDLENGTH],

const int 

  *nlines*

) 

+--------------------------------------+--------------------------------------+
|                                      | Adds history lines to the            |
|                                      | `MTZ <structMTZ.html>`__ header in   |
|                                      | front of existing history lines.     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *mtz*        | pointer to `MTZ |
|                                      |  <structMTZ.html>`__ struct   |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *history*    | lines to be add |
|                                      | ed                            |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *nlines*     | number of lines |
|                                      |  to be added                  |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     total number of history lines    |
+--------------------------------------+--------------------------------------+

`MTZXTAL <structMTZXTAL.html>`__\ \* MtzAddXtal

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

const char \* 

  *xname*,

const char \* 

  *pname*,

const float 

  *cell*\ [6]

) 

+--------------------------------------+--------------------------------------+
|                                      | Add a crystal to header mtz.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *mtz*      | pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct   |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *xname*    | Crystal name.     |
|                                      |                             |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *pname*    | Name of associate |
|                                      | d project.                  |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *cell*     | Cell dimensions o |
|                                      | f crystal.                  |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Pointer to crystal.              |
+--------------------------------------+--------------------------------------+

int MtzArrayToBatch

( 

const int \* 

  *intbuf*,

const float \* 

  *fltbuf*,

`MTZBAT <mtzdata_8h.html#a12>`__ \* 

  *batch*

) 

+--------------------------------------+--------------------------------------+
|                                      | Reads batch arrays into data         |
|                                      | structure.                           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------+                   |
|                                      |     | *intbuf*    | pointer to integ |
|                                      | er batch array   |                   |
|                                      |     +-------------+----------------- |
|                                      | -----------------+                   |
|                                      |     | *fltbuf*    | pointer to float |
|                                      |  batch array     |                   |
|                                      |     +-------------+----------------- |
|                                      | -----------------+                   |
|                                      |     | *batch*     | pointer to batch |
|                                      |  structure       |                   |
|                                      |     +-------------+----------------- |
|                                      | -----------------+                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int MtzAssignColumn

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

`MTZCOL <structMTZCOL.html>`__ \* 

  *col*,

const char 

  *crystal\_name*\ [],

const char 

  *dataset\_name*\ []

) 

+--------------------------------------+--------------------------------------+
|                                      | Assigns a column to a dataset        |
|                                      | identified by crystal\_name and      |
|                                      | dataset\_name. First, the function   |
|                                      | checks whether the column already    |
|                                      | belongs to this dataset, in which    |
|                                      | case it does nothing. Then it checks |
|                                      | if the requested dataset exists. If  |
|                                      | not, it is created, though it is     |
|                                      | better to explicitly create it       |
|                                      | beforehand. Finally, the column is   |
|                                      | assigned to the dataset.             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |     | *mtz*              | pointer t |
|                                      | o `MTZ <structMTZ.html>`__ struct    |
|                                      | |                                    |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |     | *col*              | pointer t |
|                                      | o column                             |
|                                      | |                                    |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |     | *crystal\_name*    | name of c |
|                                      | rystal containing dataset            |
|                                      | |                                    |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |     | *dataset\_name*    | name of d |
|                                      | ataset                               |
|                                      | |                                    |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+--------------------------------+---- |
| -------+------+----+                                                     |
| | int MtzAssignHKLtoBase   | (    | `MTZ <structMTZ.html>`__ \*    |   * |
| mtz*   | )    |    |                                                     |
| +--------------------------+------+--------------------------------+---- |
| -------+------+----+                                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Assigns HKL columns to the base      |
|                                      | dataset.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int MtzBatchToArray

( 

`MTZBAT <mtzdata_8h.html#a12>`__ \* 

  *batch*,

int \* 

  *intbuf*,

float \* 

  *fltbuf*

) 

+--------------------------------------+--------------------------------------+
|                                      | Writes data structure to batch       |
|                                      | arrays.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------+                   |
|                                      |     | *batch*     | pointer to batch |
|                                      |  structure       |                   |
|                                      |     +-------------+----------------- |
|                                      | -----------------+                   |
|                                      |     | *intbuf*    | pointer to integ |
|                                      | er batch array   |                   |
|                                      |     +-------------+----------------- |
|                                      | -----------------+                   |
|                                      |     | *fltbuf*    | pointer to float |
|                                      |  batch array     |                   |
|                                      |     +-------------+----------------- |
|                                      | -----------------+                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+--------+-------------+------+----+     |
| | char\* MtzCallocHist   | (    | int    |   *nhist*   | )    |    |     |
| +------------------------+------+--------+-------------+------+----+     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Allocates memory for the mtz history |
|                                      | with 'nhist' lines.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----+              |
|                                      |     | *nhist*    |    |              |
|                                      |     +------------+----+              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to history               |
+--------------------------------------+--------------------------------------+

`MTZCOL <structMTZCOL.html>`__\ \* MtzColLookup

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const char \* 

  *label*

) 

+--------------------------------------+--------------------------------------+
|                                      | Returns a pointer to the column of   |
|                                      | mtz with the given \`label\`, or     |
|                                      | NULL                                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *mtz*      | pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct   |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *label*    | Column label.     |
|                                      |                             |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to column                |
+--------------------------------------+--------------------------------------+

char\* MtzColPath

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const `MTZCOL <structMTZCOL.html>`__ \* 

  *col*

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the full path name of a       |
|                                      | column as "/xname/dname/label"       |
|                                      | Memory for the path name is assigned |
|                                      | with malloc, and can be free'd by    |
|                                      | the calling function.                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | ---------------------------+         |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct    |         |
|                                      |     +----------+-------------------- |
|                                      | ---------------------------+         |
|                                      |     | *col*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ column.   |         |
|                                      |     +----------+-------------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     full path name of column         |
+--------------------------------------+--------------------------------------+

`MTZSET <structMTZSET.html>`__\ \* MtzColSet

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const `MTZCOL <structMTZCOL.html>`__ \* 

  *col*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the dataset associated with a    |
|                                      | column.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *col*    | pointer to column o |
|                                      | f interest                |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to set containing column |
|                                      |     of interest, or NULL if "col" is |
|                                      |     not contained in "mtz".          |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------------------------------+------+---------- |
| ----------------------------+-----------+------+----+                    |
| | `MTZCOL <structMTZCOL.html>`__\ \*\* MtzColsInSet   | (    | `MTZSET < |
| structMTZSET.html>`__ \*    |   *set*   | )    |    |                    |
| +-----------------------------------------------------+------+---------- |
| ----------------------------+-----------+------+----+                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | For a given dataset, return array of |
|                                      | pointers to columns in that dataset. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --+                                  |
|                                      |     | *set*    | pointer to dataset  |
|                                      |   |                                  |
|                                      |     +----------+-------------------- |
|                                      | --+                                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     array of pointers to columns     |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------+------+--------------------------------------+--- |
| --------+------+----+                                                    |
| | char\* MtzColType   | (    | `MTZCOL <structMTZCOL.html>`__ \*    |    |
| *col*   | )    |    |                                                    |
| +---------------------+------+--------------------------------------+--- |
| --------+------+----+                                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the `MTZ <structMTZ.html>`__     |
|                                      | column type of a particular column.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | ---------------------------+         |
|                                      |     | *col*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ column.   |         |
|                                      |     +----------+-------------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     column type                      |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------------------------- |
| -+-----------+------+----+                                               |
| | void MtzDebugHierarchy   | (    | const `MTZ <structMTZ.html>`__ \*    |
|  |   *mtz*   | )    |    |                                               |
| +--------------------------+------+------------------------------------- |
| -+-----------+------+----+                                               |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Print summary of current             |
|                                      | crystal/dataset/column hierarchy.    |
|                                      | This is designed for debugging       |
|                                      | purposes rather than for the user.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

int MtzDeleteRefl

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

int 

  *iref*

) 

+--------------------------------------+--------------------------------------+
|                                      | Delete a reflection from the data    |
|                                      | structure. Beware, there is no going |
|                                      | back!                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------+        |
|                                      |     | *mtz*     | pointer to `MTZ <s |
|                                      | tructMTZ.html>`__ struct.   |        |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------+        |
|                                      |     | *iref*    | index of reflectio |
|                                      | n                           |        |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 if successful, 1 otherwise     |
+--------------------------------------+--------------------------------------+

int MtzFindInd

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

int \* 

  *ind\_xtal*,

int \* 

  *ind\_set*,

int 

  *ind\_col*\ [3]

) 

+--------------------------------------+--------------------------------------+
|                                      | Find where indices h, k, l are in    |
|                                      | `MTZ <structMTZ.html>`__ structure.  |
|                                      | Usually, they will be first 3        |
|                                      | columns of 1st dataset, but safest   |
|                                      | not to assume this.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | --------------------------------+    |
|                                      |     | *mtz*          | pointer to `M |
|                                      | TZ <structMTZ.html>`__ struct   |    |
|                                      |     +----------------+-------------- |
|                                      | --------------------------------+    |
|                                      |     | *ind\_xtal*    | crystal conta |
|                                      | ining indices                   |    |
|                                      |     +----------------+-------------- |
|                                      | --------------------------------+    |
|                                      |     | *ind\_set*     | dataset conta |
|                                      | ining indices                   |    |
|                                      |     +----------------+-------------- |
|                                      | --------------------------------+    |
|                                      |     | *ind\_col*     | 3 columns con |
|                                      | taining indices                 |    |
|                                      |     +----------------+-------------- |
|                                      | --------------------------------+    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------+------+--------------------------------+-----------+--- |
| ---+----+                                                                |
| | int MtzFree   | (    | `MTZ <structMTZ.html>`__ \*    |   *mtz*   | )  |
|    |    |                                                                |
| +---------------+------+--------------------------------+-----------+--- |
| ---+----+                                                                |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Frees the memory reserved for the    |
|                                      | `MTZ <structMTZ.html>`__ header      |
|                                      | struct.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ header struct.   |  |
|                                      |     +----------+-------------------- |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------+------+----------------------------------------+-- |
| -----------+------+----+                                                 |
| | int MtzFreeBatch   | (    | `MTZBAT <mtzdata_8h.html#a12>`__ \*    |   |
|  *batch*   | )    |    |                                                 |
| +--------------------+------+----------------------------------------+-- |
| -----------+------+----+                                                 |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Frees the memory reserved for        |
|                                      | 'batch'.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----+              |
|                                      |     | *batch*    |    |              |
|                                      |     +------------+----+              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------+------+--------------------------------------+------ |
| -----+------+----+                                                       |
| | int MtzFreeCol   | (    | `MTZCOL <structMTZCOL.html>`__ \*    |   *co |
| l*   | )    |    |                                                       |
| +------------------+------+--------------------------------------+------ |
| -----+------+----+                                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Frees the memory reserved for 'col'  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | ---------------------------+         |
|                                      |     | *col*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ column.   |         |
|                                      |     +----------+-------------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------+------+------------+------------+------+----+       |
| | int MtzFreeHist   | (    | char \*    |   *hist*   | )    |    |       |
| +-------------------+------+------------+------------+------+----+       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Frees the memory reserved for        |
|                                      | 'hist'.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+----+               |
|                                      |     | *hist*    |    |               |
|                                      |     +-----------+----+               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

`MTZ <structMTZ.html>`__\ \* MtzGet

( 

const char \* 

  *logname*,

int 

  *read\_refs*

) 

+--------------------------------------+--------------------------------------+
|                                      | Reads the contents of the            |
|                                      | `MTZ <structMTZ.html>`__ file into   |
|                                      | an `MTZ <structMTZ.html>`__          |
|                                      | structure.                           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *logname*       | (I) Logical  |
|                                      | name of `MTZ <structMTZ.html>`__ fil |
|                                      | e                                    |
|                                      |        |                             |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *read\_refs*    | (I) Whether  |
|                                      | to read reflections into memory (non |
|                                      | -zero) or to read later from file (z |
|                                      | ero)   |                             |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Pointer to                       |
|                                      |     `MTZ <structMTZ.html>`__ struct  |
+--------------------------------------+--------------------------------------+

`MTZ <structMTZ.html>`__\ \* MtzGetUserCellTolerance

( 

const char \* 

  *logname*,

int 

  *read\_refs*,

const double 

  *cell\_tolerance*

) 

+--------------------------------------+--------------------------------------+
|                                      | Reads the contents of the            |
|                                      | `MTZ <structMTZ.html>`__ file into   |
|                                      | an `MTZ <structMTZ.html>`__          |
|                                      | structure. As for function MtzGet    |
|                                      | except for extra argument            |
|                                      | cell\_tolerance.                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------------+-------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *logname*            | (I) Log |
|                                      | ical name of `MTZ <structMTZ.html>`_ |
|                                      | _ file                               |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |    |                                 |
|                                      |     +----------------------+-------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *read\_refs*         | (I) Whe |
|                                      | ther to read reflections into memory |
|                                      |  (non-zero) or to read later from fi |
|                                      | le (zero)                            |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |    |                                 |
|                                      |     +----------------------+-------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *cell\_tolerance*    | (I) Use |
|                                      | r-defined tolerance for ccp4uc\_cell |
|                                      | s\_differ. Setting this to zero mean |
|                                      | s that a new crystal is generated wh |
|                                      | enever dataset cell dimensions are d |
|                                      | ifferent. MtzGet allows for a certai |
|                                      | n variation within a single crystal. |
|                                      |    |                                 |
|                                      |     +----------------------+-------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Pointer to                       |
|                                      |     `MTZ <structMTZ.html>`__ struct  |
+--------------------------------------+--------------------------------------+

int MtzHklcoeffs

( 

const float 

  *cell*\ [6],

double 

  *coefhkl*\ [6]

) 

+--------------------------------------+--------------------------------------+
|                                      | Generate coefhkl coefficients from   |
|                                      | given cell parameters.               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *cell*       | cell dimensions |
|                                      |  to be used for resolution calculati |
|                                      | on.   |                              |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *coefhkl*    | double array of |
|                                      |  6 coefficients                      |
|                                      |       |                              |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

`MTZCOL <structMTZCOL.html>`__\ \* MtzIcolInSet

( 

const `MTZSET <structMTZSET.html>`__ \* 

  *set*,

const int 

  *icol*

) 

+--------------------------------------+--------------------------------------+
|                                      | For a given dataset, return pointer  |
|                                      | to the icol'th column in that        |
|                                      | dataset.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | ----------------+                    |
|                                      |     | *set*     | pointer to dataset |
|                                      |                                      |
|                                      |                 |                    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | ----------------+                    |
|                                      |     | *icol*    | number of the part |
|                                      | icular column (icol = 0 ... MtzNcols |
|                                      | InSet(set) -1   |                    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | ----------------+                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to specified column      |
+--------------------------------------+--------------------------------------+

float MtzInd2reso

( 

const int 

  *in*\ [3],

const double 

  *coefhkl*\ [6]

) 

+--------------------------------------+--------------------------------------+
|                                      | Calculate resolution from indices    |
|                                      | and coefhkl. coefhkl is obtained     |
|                                      | from MtzHklcoeffs.                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------+                  |
|                                      |     | *in*         | integer array o |
|                                      | f 3 indices       |                  |
|                                      |     +--------------+---------------- |
|                                      | ------------------+                  |
|                                      |     | *coefhkl*    | double array of |
|                                      |  6 coefficients   |                  |
|                                      |     +--------------+---------------- |
|                                      | ------------------+                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     resolution                       |
+--------------------------------------+--------------------------------------+

`MTZSET <structMTZSET.html>`__\ \* MtzIsetInXtal

( 

const `MTZXTAL <structMTZXTAL.html>`__ \* 

  *xtal*,

const int 

  *iset*

) 

+--------------------------------------+--------------------------------------+
|                                      | For a given crystal, return pointer  |
|                                      | to the iset'th dataset in that       |
|                                      | crystal.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | -------------------+                 |
|                                      |     | *xtal*    | pointer to the cry |
|                                      | stal struct                          |
|                                      |                    |                 |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | -------------------+                 |
|                                      |     | *iset*    | number of the part |
|                                      | icular dataset (iset = 0 ... MtzNset |
|                                      | sInXtal(xtal) -1   |                 |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | -------------------+                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to specified dataset     |
+--------------------------------------+--------------------------------------+

`MTZXTAL <structMTZXTAL.html>`__\ \* MtzIxtal

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const int 

  *ixtal*

) 

+--------------------------------------+--------------------------------------+
|                                      | Return pointer to the ixtal'th       |
|                                      | crystal.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *mtz*      | pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct            |
|                                      |                |                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *ixtal*    | number of the par |
|                                      | ticular crystal (ixtal = 0 ... MtzNx |
|                                      | tal(xtal) -1   |                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to the specified crystal |
+--------------------------------------+--------------------------------------+

int MtzListColumn

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

char 

  *clabs*\ [][31],

char 

  *ctyps*\ [][3],

int 

  *csetid*\ []

) 

+--------------------------------------+--------------------------------------+
|                                      | List of column information: label,   |
|                                      | type, dataset.                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *mtz*       | pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct   |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *clabs*     | List of labels ( |
|                                      | output).                     |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *ctyps*     | List of column t |
|                                      | ypes (output).               |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *csetid*    | List of dataset  |
|                                      | IDs (output).                |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of columns in current     |
|                                      |     structure.                       |
+--------------------------------------+--------------------------------------+

int MtzListInputColumn

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

char 

  *clabs*\ [][31],

char 

  *ctyps*\ [][3],

int 

  *csetid*\ []

) 

+--------------------------------------+--------------------------------------+
|                                      | List of column information from      |
|                                      | input file: label, type, dataset.    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *mtz*       | pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct   |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *clabs*     | List of labels ( |
|                                      | output).                     |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *ctyps*     | List of column t |
|                                      | ypes (output).               |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *csetid*    | List of dataset  |
|                                      | IDs (output).                |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of columns in input file. |
+--------------------------------------+--------------------------------------+

`MTZ <structMTZ.html>`__\ \* MtzMalloc

( 

int 

  *nxtal*,

int 

  *nset*\ []

) 

+--------------------------------------+--------------------------------------+
|                                      | Allocates memory for an              |
|                                      | `MTZ <structMTZ.html>`__ header      |
|                                      | structure. The structure can contain |
|                                      | 0, 1 or more crystals, and for each  |
|                                      | crystal 0, 1 or more datasets.       |
|                                      | Crystals have a name based on the    |
|                                      | time "NULL\_xnameHHMMSS" to ensure   |
|                                      | uniqueness (compared to crystals     |
|                                      | defined elsewhere - all new crystals |
|                                      | created here will (probably) have    |
|                                      | the same name). Crystals have the    |
|                                      | project name "NULL\_pname", and      |
|                                      | datasets have the name               |
|                                      | "NULL\_dname".                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *nxtal*    | Number of crystal |
|                                      | s to allocate.                    |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *nset*     | Number of dataset |
|                                      | s for each crystal to allocate.   |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to                       |
|                                      |     `MTZ <structMTZ.html>`__ header  |
|                                      |     struct                           |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------------------------------+------+-------- |
| -+-----+------+----+                                                     |
| | `MTZBAT <mtzdata_8h.html#a12>`__\ \* MtzMallocBatch   | (    | void    |
|  |     | )    |    |                                                     |
| +-------------------------------------------------------+------+-------- |
| -+-----+------+----+                                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Allocates memory for a single batch  |
|                                      | header.                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to batch                 |
+--------------------------------------+--------------------------------------+

`MTZCOL <structMTZCOL.html>`__\ \* MtzMallocCol

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

int 

  *nref*

) 

+--------------------------------------+--------------------------------------+
|                                      | Allocates memory for an              |
|                                      | `MTZ <structMTZ.html>`__ column.     |
|                                      | Space is allocated for the           |
|                                      | reflection data if and only if       |
|                                      | mtz->refs\_in\_memory is set.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | -----------------------------------+ |
|                                      |     | *mtz*     | pointer to `MTZ <s |
|                                      | tructMTZ.html>`__ header struct.   | |
|                                      |     +-----------+------------------- |
|                                      | -----------------------------------+ |
|                                      |     | *nref*    | number of reflecti |
|                                      | ons in column.                     | |
|                                      |     +-----------+------------------- |
|                                      | -----------------------------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to                       |
|                                      |     `MTZ <structMTZ.html>`__ column. |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------+------+---------+-----+------+----+                 |
| | void MtzMemTidy   | (    | void    |     | )    |    |                 |
| +-------------------+------+---------+-----+------+----+                 |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Free all memory malloc'd from static |
|                                      | pointers. To be called before        |
|                                      | program exit. The function can be    |
|                                      | registered with atexit.              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------+------+--------------------------------------+--------- |
| --+------+----+                                                          |
| | int MtzNbat   | (    | const `MTZ <structMTZ.html>`__ \*    |   *mtz*  |
|   | )    |    |                                                          |
| +---------------+------+--------------------------------------+--------- |
| --+------+----+                                                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the number of batches in the     |
|                                      | mtz.                                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of batches.               |
+--------------------------------------+--------------------------------------+

int MtzNbatchesInSet

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const `MTZSET <structMTZSET.html>`__ \* 

  *set*

) 

+--------------------------------------+--------------------------------------+
|                                      | For a given dataset, return number   |
|                                      | of batches in that dataset.          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *set*    | pointer to dataset  |
|                                      |                           |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of batches                |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------+------+--------------------------------------+--------- |
| --+------+----+                                                          |
| | int MtzNcol   | (    | const `MTZ <structMTZ.html>`__ \*    |   *mtz*  |
|   | )    |    |                                                          |
| +---------------+------+--------------------------------------+--------- |
| --+------+----+                                                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the number of columns in the     |
|                                      | `MTZ <structMTZ.html>`__ data        |
|                                      | structure.                           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of columns                |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------+------+------------------------------------------ |
| --+-----------+------+----+                                              |
| | int MtzNcolsInSet   | (    | const `MTZSET <structMTZSET.html>`__ \*   |
|   |   *set*   | )    |    |                                              |
| +---------------------+------+------------------------------------------ |
| --+-----------+------+----+                                              |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | For a given dataset, return number   |
|                                      | of columns in that dataset. This is  |
|                                      | simply set->ncol and so includes all |
|                                      | columns irrespective of col->active  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --+                                  |
|                                      |     | *set*    | pointer to dataset  |
|                                      |   |                                  |
|                                      |     +----------+-------------------- |
|                                      | --+                                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of columns                |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------+------+--------------------------------------+--------- |
| --+------+----+                                                          |
| | int MtzNref   | (    | const `MTZ <structMTZ.html>`__ \*    |   *mtz*  |
|   | )    |    |                                                          |
| +---------------+------+--------------------------------------+--------- |
| --+------+----+                                                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the number of reflections in the |
|                                      | mtz.                                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of reflections.           |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------+------+--------------------------------------+--------- |
| --+------+----+                                                          |
| | int MtzNset   | (    | const `MTZ <structMTZ.html>`__ \*    |   *mtz*  |
|   | )    |    |                                                          |
| +---------------+------+--------------------------------------+--------- |
| --+------+----+                                                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the number of datasets in the    |
|                                      | `MTZ <structMTZ.html>`__ structure   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     total number of datasets         |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------+------+----------------------------------------- |
| -----+------------+------+----+                                          |
| | int MtzNsetsInXtal   | (    | const `MTZXTAL <structMTZXTAL.html>`__ \ |
| *    |   *xtal*   | )    |    |                                          |
| +----------------------+------+----------------------------------------- |
| -----+------------+------+----+                                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | For a given crystal, return number   |
|                                      | of datasets in that crystal.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |     | *xtal*    | pointer to the cry |
|                                      | stal struct   |                      |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of datasets               |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------+------+--------------------------------------+- |
| ----------+------+----+                                                  |
| | int MtzNumActiveCol   | (    | const `MTZ <structMTZ.html>`__ \*    |  |
|   *mtz*   | )    |    |                                                  |
| +-----------------------+------+--------------------------------------+- |
| ----------+------+----+                                                  |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the number of active columns in  |
|                                      | the `MTZ <structMTZ.html>`__ data    |
|                                      | structure.                           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of columns                |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------+------+---------------------------------- |
| ----------+-----------+------+----+                                      |
| | int MtzNumActiveColsInSet   | (    | const `MTZSET <structMTZSET.html> |
| `__ \*    |   *set*   | )    |    |                                      |
| +-----------------------------+------+---------------------------------- |
| ----------+-----------+------+----+                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | For a given dataset, return number   |
|                                      | of active columns in that dataset.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --+                                  |
|                                      |     | *set*    | pointer to dataset  |
|                                      |   |                                  |
|                                      |     +----------+-------------------- |
|                                      | --+                                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of active columns         |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------+------+--------------------------------------+- |
| ----------+------+----+                                                  |
| | int MtzNumActiveSet   | (    | const `MTZ <structMTZ.html>`__ \*    |  |
|   *mtz*   | )    |    |                                                  |
| +-----------------------+------+--------------------------------------+- |
| ----------+------+----+                                                  |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the number of active datasets in |
|                                      | the `MTZ <structMTZ.html>`__         |
|                                      | structure                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     total number of datasets         |
+--------------------------------------+--------------------------------------+

int MtzNumActiveSetsInXtal

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const `MTZXTAL <structMTZXTAL.html>`__ \* 

  *xtal*

) 

+--------------------------------------+--------------------------------------+
|                                      | For a given crystal, return number   |
|                                      | of active datasets in that crystal.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |     | *xtal*    | pointer to the cry |
|                                      | stal struct   |                      |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of datasets               |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+--------------------------------------+ |
| -----------+------+----+                                                 |
| | int MtzNumActiveXtal   | (    | const `MTZ <structMTZ.html>`__ \*    | |
|    *mtz*   | )    |    |                                                 |
| +------------------------+------+--------------------------------------+ |
| -----------+------+----+                                                 |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the number of active crystals in |
|                                      | the `MTZ <structMTZ.html>`__         |
|                                      | structure                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of active crystals        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------+------+--------------------------------------+- |
| ----------+------+----+                                                  |
| | int MtzNumSourceCol   | (    | const `MTZ <structMTZ.html>`__ \*    |  |
|   *mtz*   | )    |    |                                                  |
| +-----------------------+------+--------------------------------------+- |
| ----------+------+----+                                                  |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the number of columns in the     |
|                                      | `MTZ <structMTZ.html>`__ data        |
|                                      | structure which have a source in an  |
|                                      | input file (i.e. non-zero source     |
|                                      | attribute).                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of columns                |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------+------+---------------------------------- |
| ----------+-----------+------+----+                                      |
| | int MtzNumSourceColsInSet   | (    | const `MTZSET <structMTZSET.html> |
| `__ \*    |   *set*   | )    |    |                                      |
| +-----------------------------+------+---------------------------------- |
| ----------+-----------+------+----+                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | For a given dataset, return number   |
|                                      | of columns in that dataset which     |
|                                      | have a source in an input file (i.e. |
|                                      | non-zero source attribute).          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --+                                  |
|                                      |     | *set*    | pointer to dataset  |
|                                      |   |                                  |
|                                      |     +----------+-------------------- |
|                                      | --+                                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of source columns         |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------+------+--------------------------------------+-------- |
| ---+------+----+                                                         |
| | int MtzNxtal   | (    | const `MTZ <structMTZ.html>`__ \*    |   *mtz* |
|    | )    |    |                                                         |
| +----------------+------+--------------------------------------+-------- |
| ---+------+----+                                                         |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the total number of crystals in  |
|                                      | the `MTZ <structMTZ.html>`__         |
|                                      | structure                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of active crystals        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------------------------------------+------+ |
| ------------------+---------------+------+----+                          |
| | `CCP4File <library__file_8h.html#a0>`__\ \* MtzOpenForWrite   | (    | |
|  const char \*    |   *logname*   | )    |    |                          |
| +---------------------------------------------------------------+------+ |
| ------------------+---------------+------+----+                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Opens a new `MTZ <structMTZ.html>`__ |
|                                      | file for writing. The output file    |
|                                      | can be specified either with a true  |
|                                      | filename, or more likely as a        |
|                                      | logical name corresponding to an     |
|                                      | environment variable or a CCP4       |
|                                      | command line argument such as        |
|                                      | HKLOUT.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------+       |
|                                      |     | *logname*    | logical name or |
|                                      |  filename for output file.   |       |
|                                      |     +--------------+---------------- |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to file or NULL on       |
|                                      |     failure                          |
+--------------------------------------+--------------------------------------+

int MtzParseLabin

( 

char \* 

  *labin\_line*,

const char 

  *prog\_labels*\ [][31],

const int 

  *nlprgi*,

char 

  *user\_labels*\ [][2][31]

) 

+--------------------------------------+--------------------------------------+
|                                      | Uses LABIN or LABOUT line to convert |
|                                      | program labels to user labels. This  |
|                                      | is a helper function, but does not   |
|                                      | access reflection structure at all.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *labin\_line*     | (I) LABIN/ |
|                                      | LABOUT line from Parser.             |
|                                      |                                      |
|                                      |                                      |
|                                      |               |                      |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *prog\_labels*    | (I) Progam |
|                                      |  labels.                             |
|                                      |                                      |
|                                      |                                      |
|                                      |               |                      |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *nlprgi*          | (I) Number |
|                                      |  of program labels.                  |
|                                      |                                      |
|                                      |                                      |
|                                      |               |                      |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *user\_labels*    | (O) On out |
|                                      | put, user-supplied file labels in co |
|                                      | rresponding positions. For unassigne |
|                                      | d program labels, user\_labels is em |
|                                      | pty string.   |                      |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of program labels         |
|                                      |     matched, or -1 if there was an   |
|                                      |     error.                           |
+--------------------------------------+--------------------------------------+

int MtzPathMatch

( 

const char \* 

  *path1*,

const char \* 

  *path2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test for match between two paths,    |
|                                      | including wildcards                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+---------------+   |
|                                      |     | *path1*    | First path    |   |
|                                      |     +------------+---------------+   |
|                                      |     | *path2*    | Second path   |   |
|                                      |     +------------+---------------+   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if paths match, else 0.        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------------------------------ |
| ----------+-------------+------+----+                                    |
| | int MtzPrintBatchHeader   | (    | const `MTZBAT <mtzdata_8h.html#a12> |
| `__ \*    |   *batch*   | )    |    |                                    |
| +---------------------------+------+------------------------------------ |
| ----------+-------------+------+----+                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Function to print batch header data  |
|                                      | for a specified batch to stdout.     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -------------+                       |
|                                      |     | *batch*    | Pointer to reques |
|                                      | ted batch.   |                       |
|                                      |     +------------+------------------ |
|                                      | -------------+                       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int MtzPut

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

const char \* 

  *logname*

) 

+--------------------------------------+--------------------------------------+
|                                      | Writes an `MTZ <structMTZ.html>`__   |
|                                      | data structure to disk. If file is   |
|                                      | already open, MtzPut uses file       |
|                                      | pointer in mtz struct, else uses     |
|                                      | logical name of file.                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | -------------------------------+     |
|                                      |     | *mtz*        | pointer to `MTZ |
|                                      |  <structMTZ.html>`__ struct.   |     |
|                                      |     +--------------+---------------- |
|                                      | -------------------------------+     |
|                                      |     | *logname*    | logical name fo |
|                                      | r output file or blank.        |     |
|                                      |     +--------------+---------------- |
|                                      | -------------------------------+     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int MtzResLimits

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

float \* 

  *minres*,

float \* 

  *maxres*

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the overall resolution limits |
|                                      | of the `MTZ <structMTZ.html>`__      |
|                                      | structure. These are the widest      |
|                                      | limits over all crystals present.    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *mtz*       | pointer to `MTZ  |
|                                      | <structMTZ.html>`__ struct   |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *minres*    | minimum resoluti |
|                                      | on                           |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |     | *maxres*    | maximum resoluti |
|                                      | on                           |       |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int MtzRJustPath

( 

char \* 

  *path*,

const char \* 

  *partial*,

const int 

  *njust*

) 

+--------------------------------------+--------------------------------------+
|                                      | Complete a right-justified path by   |
|                                      | prefixing with wildcards             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ----------------+                    |
|                                      |     | *path*       | Completed path. |
|                                      |                 |                    |
|                                      |     +--------------+---------------- |
|                                      | ----------------+                    |
|                                      |     | *partial*    | Partial right-j |
|                                      | ustified path   |                    |
|                                      |     +--------------+---------------- |
|                                      | ----------------+                    |
|                                      |     | *njust*      |                 |
|                                      |                 |                    |
|                                      |     +--------------+---------------- |
|                                      | ----------------+                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure.      |
+--------------------------------------+--------------------------------------+

int MtzRrefl

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *filein*,

int 

  *ncol*,

float \* 

  *refldata*

) 

+--------------------------------------+--------------------------------------+
|                                      | Reads reflection data from           |
|                                      | `MTZ <structMTZ.html>`__ file.       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *filein*      | pointer to inp |
|                                      | ut file       |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *ncol*        | number of colu |
|                                      | mns to read   |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *refldata*    | array of refle |
|                                      | ction data    |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     istat from ccp4\_file\_read      |
+--------------------------------------+--------------------------------------+

`MTZSET <structMTZSET.html>`__\ \* MtzSetLookup

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const char \* 

  *label*

) 

+--------------------------------------+--------------------------------------+
|                                      | Returns a pointer to the dataset of  |
|                                      | `MTZ <structMTZ.html>`__ with the    |
|                                      | given label.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *mtz*      | pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct.           |
|                                      |              |                       |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *label*    | Label of desired  |
|                                      | set. This could be <dname> or <xname |
|                                      | >/<dname>.   |                       |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to set or NULL if not    |
|                                      |     found                            |
+--------------------------------------+--------------------------------------+

char\* MtzSetPath

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const `MTZSET <structMTZSET.html>`__ \* 

  *set*

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the full path name of a       |
|                                      | dataset as "/xname/dname" The        |
|                                      | pointer to `MTZ <structMTZ.html>`__  |
|                                      | is required to do reverse lookup of  |
|                                      | xname. Memory for the path name is   |
|                                      | assigned with malloc, and can be     |
|                                      | free'd by the calling function.      |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *set*    | pointer to dataset  |
|                                      |                           |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to string containing     |
|                                      |     path name                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------------------------------+------+--------- |
| -------------------------------+------------+------+----+                |
| | `MTZSET <structMTZSET.html>`__\ \*\* MtzSetsInXtal   | (    | `MTZXTAL |
|  <structMTZXTAL.html>`__ \*    |   *xtal*   | )    |    |                |
| +------------------------------------------------------+------+--------- |
| -------------------------------+------------+------+----+                |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | For a given crystal, return array of |
|                                      | pointers to datasets in that         |
|                                      | crystal.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |     | *xtal*    | pointer to the cry |
|                                      | stal struct   |                      |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     array of pointers to datasets    |
+--------------------------------------+--------------------------------------+

int MtzSetSortOrder

( 

`MTZ <structMTZ.html>`__ \* 

  *mtz*,

`MTZCOL <structMTZCOL.html>`__ \* 

  *colsort*\ [5]

) 

+--------------------------------------+--------------------------------------+
|                                      | Sets the sort order in the           |
|                                      | `MTZ <structMTZ.html>`__ header. The |
|                                      | sort order is a list of columns to   |
|                                      | be used for sorting, to be applied   |
|                                      | in the order they appear in the      |
|                                      | list, i.e. sort first on colsort[0], |
|                                      | then on colsort[1], etc. If there    |
|                                      | are less than 5 columns to be used   |
|                                      | for sorting, some of colsort[] may   |
|                                      | be NULL.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *mtz*        | Pointer to `MTZ |
|                                      |  <structMTZ.html>`__ struct   |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |     | *colsort*    | Array of pointe |
|                                      | rs to columns.                |      |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------+      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

`MTZXTAL <structMTZXTAL.html>`__\ \* MtzSetXtal

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const `MTZSET <structMTZSET.html>`__ \* 

  *set*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the crystal associated with a    |
|                                      | dataset The pointer to               |
|                                      | `MTZ <structMTZ.html>`__ is required |
|                                      | to do reverse lookup of xname.       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *set*    | pointer to dataset  |
|                                      |                           |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to parent crystal, or    |
|                                      |     NULL if "set" is not present in  |
|                                      |     "mtz".                           |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------------------------------ |
| --+-----------+------+----+                                              |
| | int MtzSpacegroupNumber   | (    | const `MTZ <structMTZ.html>`__ \*   |
|   |   *mtz*   | )    |    |                                              |
| +---------------------------+------+------------------------------------ |
| --+-----------+------+----+                                              |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the spacegroup number (likely    |
|                                      | CCP4 convention).                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Spacegroup number.               |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------+------+--------------------------------------+- |
| ----------+------+----+                                                  |
| | int MtzToggleColumn   | (    | `MTZCOL <structMTZCOL.html>`__ \*    |  |
|   *col*   | )    |    |                                                  |
| +-----------------------+------+--------------------------------------+- |
| ----------+------+----+                                                  |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Toggle active flag of column. A      |
|                                      | value of 0 means inactive and will   |
|                                      | not be written out, whereas a value  |
|                                      | of 1 means active and will be        |
|                                      | written out.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | -+                                   |
|                                      |     | *col*    | pointer to column   |
|                                      |  |                                   |
|                                      |     +----------+-------------------- |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     New value of col->active.        |
+--------------------------------------+--------------------------------------+

int MtzWhdrLine

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *fileout*,

int 

  *nitems*,

char 

  *buffer*\ []

) 

+--------------------------------------+--------------------------------------+
|                                      | Write header record to fileout.      |
|                                      | Record is filled from buffer and     |
|                                      | padded by blanks to a total length   |
|                                      | of MTZRECORDLENGTH.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | -----------------+                   |
|                                      |     | *fileout*    | Pointer to outp |
|                                      | ut file.                             |
|                                      |                  |                   |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | -----------------+                   |
|                                      |     | *nitems*     | Number of chara |
|                                      | cters in buffer.                     |
|                                      |                  |                   |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | -----------------+                   |
|                                      |     | *buffer*     | Character buffe |
|                                      | r containing `MTZ <structMTZ.html>`_ |
|                                      | _ header line.   |                   |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | -----------------+                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of bytes written on       |
|                                      |     success, EOF on failure.         |
+--------------------------------------+--------------------------------------+

int MtzWrefl

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *fileout*,

int 

  *ncol*,

float \* 

  *refldata*

) 

+--------------------------------------+--------------------------------------+
|                                      | Write ncol column entries to fileout |
|                                      | from refldata.                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *fileout*     | pointer to out |
|                                      | put `MTZ <structMTZ.html>`__ file.   |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *ncol*        | number of refl |
|                                      | ection data items to write.          |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *refldata*    | array of refle |
|                                      | ction data items.                    |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of items written. If this |
|                                      |     is less than ncol, then that     |
|                                      |     indicates a write error.         |
+--------------------------------------+--------------------------------------+

`MTZXTAL <structMTZXTAL.html>`__\ \* MtzXtalLookup

( 

const `MTZ <structMTZ.html>`__ \* 

  *mtz*,

const char \* 

  *label*

) 

+--------------------------------------+--------------------------------------+
|                                      | Returns a pointer to the crystal of  |
|                                      | mtz with the given \`label\`, or     |
|                                      | NULL.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *mtz*      | pointer to `MTZ < |
|                                      | structMTZ.html>`__ struct   |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |     | *label*    |                   |
|                                      |                             |        |
|                                      |     +------------+------------------ |
|                                      | ----------------------------+        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to crystal               |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------+------+----------------------------------------- |
| -----+------------+------+----+                                          |
| | char\* MtzXtalPath   | (    | const `MTZXTAL <structMTZXTAL.html>`__ \ |
| *    |   *xtal*   | )    |    |                                          |
| +----------------------+------+----------------------------------------- |
| -----+------------+------+----+                                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Return the full path name of a       |
|                                      | crystal as "/xname" Memory for the   |
|                                      | path name is assigned with malloc,   |
|                                      | and can be free'd by the calling     |
|                                      | function.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |     | *xtal*    | pointer to the cry |
|                                      | stal struct   |                      |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to string containing     |
|                                      |     path name                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------------------------+------+------------ |
| --------------------+-----------+------+----+                            |
| | `MTZXTAL <structMTZXTAL.html>`__\ \*\* MtzXtals   | (    | `MTZ <struc |
| tMTZ.html>`__ \*    |   *mtz*   | )    |    |                            |
| +---------------------------------------------------+------+------------ |
| --------------------+-----------+------+----+                            |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Return array of pointers to          |
|                                      | crystals.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |     | *mtz*    | pointer to `MTZ <st |
|                                      | ructMTZ.html>`__ struct   |          |
|                                      |     +----------+-------------------- |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     array of pointers to crystals    |
+--------------------------------------+--------------------------------------+

`MTZBAT <mtzdata_8h.html#a12>`__\ \* sort\_batches

( 

`MTZBAT <mtzdata_8h.html#a12>`__ \* 

  *batch*,

int 

  *numbat*

) 

+--------------------------------------+--------------------------------------+
|                                      | Sort a linked list of batches on     |
|                                      | batch number. The function first     |
|                                      | checks whether batches are already   |
|                                      | in order, as they will usually be.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *batch*     | pointer to begin |
|                                      | ning of batch list   |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |     | *numbat*    | number of batche |
|                                      | s to be sorted       |               |
|                                      |     +-------------+----------------- |
|                                      | ---------------------+               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Sorted list if batches sorted,   |
|                                      |     original list if batches already |
|                                      |     in order                         |
+--------------------------------------+--------------------------------------+
