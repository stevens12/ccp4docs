`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmap\_skew.h
============

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      cmap_skew.h: header file for cmap_skew.c
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 #ifndef __GUARD_MAPLIB_SKEW
    00011 #define __GUARD_MAPLIB_SKEW
    00012 
    00013 #ifdef __cplusplus
    00014 extern "C" {
    00015 #endif
    00016 
    00017 int skew_set(const CMMFile_Skew *skew);
    00018 
    00019 #ifdef __cplusplus
    00020 }
    00021 #endif
    00022 
    00023 #endif    /* __GUARD_MAPLIB_SKEW */

.. raw:: html

   </div>
