`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

csymlib.h
=========

`Go to the documentation of this file. <csymlib_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      csymlib.h: header file for csymlib.c
    00003      Copyright (C) 2001  CCLRC, Martyn Winn
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00096 #ifndef __CSymLib__
    00097 #define __CSymLib__
    00098 
    00099 static char rcsidhs[] = "$Id$";
    00100 
    00101 /* note that definitions in ccp4_spg.h are within the CSym namespace */
    00102 #include "ccp4_spg.h"
    00103 
    00104 #ifdef  __cplusplus
    00105 namespace CSym {
    00106 extern "C" {
    00107 #endif
    00108 
    00115 CCP4SPG *ccp4spg_load_by_standard_num(const int numspg); 
    00116 
    00123 CCP4SPG *ccp4spg_load_by_ccp4_num(const int ccp4numspg); 
    00124 
    00131 CCP4SPG *ccp4spg_load_by_spgname(const char *spgname);
    00132 
    00142 CCP4SPG *ccp4spg_load_by_ccp4_spgname(const char *ccp4spgname);
    00143 
    00151 CCP4SPG * ccp4_spgrp_reverse_lookup(const int nsym1, const ccp4_symop *op1);
    00152 
    00166 CCP4SPG *ccp4spg_load_spacegroup(const int numspg, const int ccp4numspg,
    00167         const char *spgname, const char *ccp4spgname, 
    00168         const int nsym1, const ccp4_symop *op1); 
    00169 
    00174 void ccp4spg_mem_tidy(void);
    00175 
    00183 int symfr_driver (const char *line, float rot[][4][4]);
    00184 
    00188 void ccp4spg_free(CCP4SPG **sp);
    00189 
    00195 void ccp4spg_register_by_ccp4_num(int numspg);
    00196 
    00203 void ccp4spg_register_by_symops(int nops, float rsm[][4][4]);
    00204 
    00211 int ccp4_spg_get_centering(const char *symbol_Hall, float cent_ops[4][3]);
    00212 
    00218 int ccp4spg_load_laue(CCP4SPG* spacegroup, const int nlaue);
    00219 
    00223 int ASU_1b   (const int h, const int k, const int l);
    00224 
    00228 int ASU_2_m  (const int h, const int k, const int l);
    00229 
    00233 int ASU_mmm  (const int h, const int k, const int l);
    00234 
    00238 int ASU_4_m  (const int h, const int k, const int l);
    00239 
    00243 int ASU_4_mmm(const int h, const int k, const int l);
    00244 
    00248 int ASU_3b   (const int h, const int k, const int l);
    00249 
    00253 int ASU_3bm  (const int h, const int k, const int l);
    00254 
    00258 int ASU_3bmx (const int h, const int k, const int l);
    00259 
    00263 int ASU_6_m  (const int h, const int k, const int l);
    00264 
    00268 int ASU_6_mmm(const int h, const int k, const int l);
    00269 
    00273 int ASU_m3b  (const int h, const int k, const int l);
    00274 
    00278 int ASU_m3bm  (const int h, const int k, const int l);
    00279 
    00284 char *ccp4spg_symbol_Hall(CCP4SPG* sp);
    00285 
    00291 ccp4_symop ccp4_symop_invert( const ccp4_symop op1 );
    00292 
    00300 int ccp4spg_name_equal(const char *spgname1, const char *spgname2);
    00301 
    00313 int ccp4spg_name_equal_to_lib(const char *spgname_lib, const char *spgname_match);
    00314 
    00322 char *ccp4spg_to_shortname(char *shortname, const char *longname);
    00323 
    00330 void ccp4spg_name_de_colon(char *name);
    00331 
    00339 int ccp4spg_pgname_equal(const char *pgname1, const char *pgname2);
    00340 
    00346 ccp4_symop *ccp4spg_norm_trans(ccp4_symop *op);
    00347 
    00360 int ccp4_spgrp_equal( int nsym1, const ccp4_symop *op1, int nsym2, const ccp4_symop *op2);
    00361 
    00372 int ccp4_spgrp_equal_order( int nsym1, const ccp4_symop *op1, int nsym2, const ccp4_symop *op2);
    00373 
    00379 int ccp4_symop_code(ccp4_symop op);
    00380 
    00387 int ccp4_int_compare( const void *p1, const void *p2 );
    00388 
    00396 int ccp4spg_is_in_pm_asu(const CCP4SPG* sp, const int h, const int k, const int l);
    00397 
    00405 int ccp4spg_is_in_asu(const CCP4SPG* sp, const int h, const int k, const int l);
    00406 
    00421 int ccp4spg_put_in_asu(const CCP4SPG* sp, const int hin, const int kin, const int lin,
    00422                        int *hout, int *kout, int *lout );
    00423 
    00436 void ccp4spg_generate_indices(const CCP4SPG* sp, const int isym,
    00437                   const int hin, const int kin, const int lin,
    00438                               int *hout, int *kout, int *lout );
    00439 
    00450 float ccp4spg_phase_shift(const int hin, const int kin, const int lin,
    00451                           const float phasin, const float trans[3], const int isign);
    00452 
    00458 int ccp4spg_do_chb(const float chb[3][3]);
    00459 
    00465 void ccp4spg_set_centric_zones(CCP4SPG* sp);
    00466 
    00476 int ccp4spg_is_centric(const CCP4SPG* sp, const int h, const int k, const int l);
    00477 
    00485 int ccp4spg_check_centric_zone(const int nzone, const int h, const int k, const int l);
    00486 
    00495 float ccp4spg_centric_phase(const CCP4SPG* sp, const int h, const int k, const int l);
    00496 
    00501 void ccp4spg_print_centric_zones(const CCP4SPG* sp);
    00502 
    00508 char *ccp4spg_describe_centric_zone(const int nzone, char *centric_zone);
    00509 
    00515 void ccp4spg_set_epsilon_zones(CCP4SPG* sp);
    00516 
    00525 int ccp4spg_get_multiplicity(const CCP4SPG* sp, const int h, const int k, const int l);
    00526 
    00534 int ccp4spg_check_epsilon_zone(const int nzone, const int h, const int k, const int l);
    00535 
    00540 void ccp4spg_print_epsilon_zones(const CCP4SPG* sp);
    00541 
    00547 char *ccp4spg_describe_epsilon_zone(const int nzone, char *epsilon_zone);
    00548 
    00549 
    00557 int ccp4spg_is_sysabs(const CCP4SPG* sp, const int h, const int k, const int l);
    00558 
    00570 int ccp4spg_generate_origins(const char *namspg, const int nsym, const float rsym[][4][4],
    00571                              float origins[][3], int *polarx, int *polary, int *polarz,
    00572                              const int iprint);
    00573 
    00578 void ccp4spg_print_recip_spgrp(const CCP4SPG* sp);
    00579 
    00584 void ccp4spg_print_recip_ops(const CCP4SPG* sp);
    00585 
    00592 int range_to_limits(const char *range, float limits[2]);
    00593 
    00605 void set_fft_grid(CCP4SPG* sp, const int nxmin, const int nymin, const int nzmin, 
    00606                   const float sample, int *nx, int *ny, int *nz);
    00607 
    00613 int all_factors_le_19(const int n);
    00614 
    00622 int get_grid_sample(const int minsmp, const int nmul, const float sample);
    00623 
    00631 int ccp4spg_check_symm_cell(int nsym, float rsym[][4][4], float cell[6]);
    00632 
    00633 #ifdef __cplusplus
    00634 } }
    00635 #endif
    00636 #endif

.. raw:: html

   </div>
