`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_general.c File Reference
==============================

| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include <math.h>``
| ``#include <stdarg.h>``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_utils.h"``
| ``#include "ccp4_parser.h"``
| ``#include "ccp4_general.h"``
| ``#include "ccp4_program.h"``
| ``#include "cmtzlib.h"``
| ``#include "csymlib.h"``
| ``#include "ccp4_errno.h"``

| 

Functions
---------

 int 

**ccperror** (int ierr, const char \*message)

 int 

**ccperror\_noexit** (int ierr, const char \*message)

 int 

**ccp4printf** (int level, char \*format,...)

 int 

**ccp4fyp** (int argc, char \*\*argv)

 int 

**ccp4fyp\_cleanup** (int ienv, char \*\*envname, char \*\*envtype, char
\*\*envext, char \*logical\_name, char \*file\_name, char \*file\_type,
char \*file\_ext, char \*env\_file, char \*def\_file, char \*dir,
CCP4PARSERARRAY \*parser)

 int 

**ccp4setenv** (char \*logical\_name, char \*value, char \*\*envname,
char \*\*envtype, char \*\*envext, int \*ienv, int no\_overwrt)

 int 

**ccp4setenv\_cleanup** (char \*file\_ext, char \*file\_root, char
\*file\_path, char \*file\_name)

 int 

**ccpexists** (char \*filename)

 int 

**ccpputenv** (char \*logical\_name, char \*file\_name)

 void 

**ccp4\_banner** (void)

--------------

Detailed Description
--------------------

General library functions and utilities. Peter Briggs et al
