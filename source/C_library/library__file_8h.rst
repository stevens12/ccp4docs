`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library\_file.h File Reference
==============================

| ``#include "ccp4_sysdep.h"``
| ``#include "ccp4_types.h"``

`Go to the source code of this file. <library__file_8h-source.html>`__

| 

Compounds
---------

struct  

**\_CFileStruct**

| 

Typedefs
--------

typedef \_CFileStruct 

`CCP4File <library__file_8h.html#a0>`__

| 

Functions
---------

`CCP4File <library__file_8h.html#a0>`__ \* 

`ccp4\_file\_open <library__file_8h.html#a1>`__ (const char \*, const
int)

`CCP4File <library__file_8h.html#a0>`__ \* 

`ccp4\_file\_open\_file <library__file_8h.html#a2>`__ (const FILE \*,
const int)

`CCP4File <library__file_8h.html#a0>`__ \* 

`ccp4\_file\_open\_fd <library__file_8h.html#a3>`__ (const int, const
int)

int 

`ccp4\_file\_rarch <library__file_8h.html#a4>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_warch <library__file_8h.html#a5>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_close <library__file_8h.html#a6>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_mode <library__file_8h.html#a7>`__ (const
`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_setmode <library__file_8h.html#a8>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const int)

int 

`ccp4\_file\_setstamp <library__file_8h.html#a9>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const size\_t)

int 

`ccp4\_file\_itemsize <library__file_8h.html#a10>`__ (const
`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_setbyte <library__file_8h.html#a11>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const int)

 int 

**ccp4\_file\_byteorder** (`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_is\_write <library__file_8h.html#a13>`__ (const
`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_is\_read <library__file_8h.html#a14>`__ (const
`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_is\_append <library__file_8h.html#a15>`__ (const
`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_is\_scratch <library__file_8h.html#a16>`__ (const
`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_is\_buffered <library__file_8h.html#a17>`__ (const
`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_status <library__file_8h.html#a18>`__ (const
`CCP4File <library__file_8h.html#a0>`__ \*)

const char \* 

`ccp4\_file\_name <library__file_8h.html#a19>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_read <library__file_8h.html#a20>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, uint8 \*, size\_t)

int 

`ccp4\_file\_readcomp <library__file_8h.html#a21>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, uint8 \*, size\_t)

int 

`ccp4\_file\_readshortcomp <library__file_8h.html#a22>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, uint8 \*, size\_t)

int 

`ccp4\_file\_readfloat <library__file_8h.html#a23>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, uint8 \*, size\_t)

int 

`ccp4\_file\_readint <library__file_8h.html#a24>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, uint8 \*, size\_t)

int 

`ccp4\_file\_readshort <library__file_8h.html#a25>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, uint8 \*, size\_t)

int 

`ccp4\_file\_readchar <library__file_8h.html#a26>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, uint8 \*, size\_t)

int 

`ccp4\_file\_write <library__file_8h.html#a27>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const uint8 \*, size\_t)

int 

`ccp4\_file\_writecomp <library__file_8h.html#a28>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const uint8 \*, size\_t)

int 

`ccp4\_file\_writeshortcomp <library__file_8h.html#a29>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const uint8 \*, size\_t)

int 

`ccp4\_file\_writefloat <library__file_8h.html#a30>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const uint8 \*, size\_t)

int 

`ccp4\_file\_writeint <library__file_8h.html#a31>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const uint8 \*, size\_t)

int 

`ccp4\_file\_writeshort <library__file_8h.html#a32>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const uint8 \*, size\_t)

int 

`ccp4\_file\_writechar <library__file_8h.html#a33>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const uint8 \*, size\_t)

int 

`ccp4\_file\_seek <library__file_8h.html#a34>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, long, int)

void 

`ccp4\_file\_rewind <library__file_8h.html#a35>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

void 

`ccp4\_file\_flush <library__file_8h.html#a36>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

long 

`ccp4\_file\_length <library__file_8h.html#a37>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

long 

`ccp4\_file\_tell <library__file_8h.html#a38>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

int 

`ccp4\_file\_feof <library__file_8h.html#a39>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

void 

`ccp4\_file\_clearerr <library__file_8h.html#a40>`__
(`CCP4File <library__file_8h.html#a0>`__ \*)

void 

`ccp4\_file\_fatal <library__file_8h.html#a41>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, char \*)

char \* 

`ccp4\_file\_print <library__file_8h.html#a42>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, char \*, char \*)

int 

`ccp4\_file\_raw\_seek <library__file_8h.html#a43>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, long, int)

int 

`ccp4\_file\_raw\_read <library__file_8h.html#a44>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, char \*, size\_t)

int 

`ccp4\_file\_raw\_write <library__file_8h.html#a45>`__
(`CCP4File <library__file_8h.html#a0>`__ \*, const char \*, size\_t)

 int 

**ccp4\_file\_raw\_setstamp** (`CCP4File <library__file_8h.html#a0>`__
\*, const size\_t)

--------------

Detailed Description
--------------------

Functions for file i/o. Charles Ballard

--------------

Typedef Documentation
---------------------

+--------------------------------------------------------------------------+
| +-----------------------------------------+                              |
| | typedef struct \_CFileStruct CCP4File   |                              |
| +-----------------------------------------+                              |
+--------------------------------------------------------------------------+

+-----+----------------------+
|     | Generic CCP4 file.   |
+-----+----------------------+

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +-----------------------------+------+---------------------------------- |
| -------------+-------------+------+----+                                 |
| | void ccp4\_file\_clearerr   | (    | `CCP4File <library__file_8h.html# |
| a0>`__ \*    |   *cfile*   | )    |    |                                 |
| +-----------------------------+------+---------------------------------- |
| -------------+-------------+------+----+                                 |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_clearerr:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Clears error status of @cfile.       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
| | int ccp4\_file\_close   | (    | `CCP4File <library__file_8h.html#a0>` |
| __ \*    |   *cfile*   | )    |    |                                     |
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_close:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | close @cfile if owned, close         |
|                                      | (non-buffered) or fclose (buffered), |
|                                      | or fflush if stream not owned. Free  |
|                                      | resources.                           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

void ccp4\_file\_fatal

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

char \* 

  *message*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_fatal:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Die with error message based on      |
|                                      | @cfile error status.                 |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+--------------------------------------- |
| --------+-------------+------+----+                                      |
| | int ccp4\_file\_feof   | (    | `CCP4File <library__file_8h.html#a0>`_ |
| _ \*    |   *cfile*   | )    |    |                                      |
| +------------------------+------+--------------------------------------- |
| --------+-------------+------+----+                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_feof:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     true if @cfile is at EoF.        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------------------------- |
| ----------+-------------+------+----+                                    |
| | void ccp4\_file\_flush   | (    | `CCP4File <library__file_8h.html#a0> |
| `__ \*    |   *cfile*   | )    |    |                                    |
| +--------------------------+------+------------------------------------- |
| ----------+-------------+------+----+                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_flush:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | flush buffer contents of @cfile      |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------+------+--------------------------------- |
| --------------------+-------------+------+----+                          |
| | int ccp4\_file\_is\_append   | (    | const `CCP4File <library__file_8 |
| h.html#a0>`__ \*    |   *cfile*   | )    |    |                          |
| +------------------------------+------+--------------------------------- |
| --------------------+-------------+------+----+                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_is\_append:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is the @cfile in append mode         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true.                       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------------+------+------------------------------- |
| ----------------------+-------------+------+----+                        |
| | int ccp4\_file\_is\_buffered   | (    | const `CCP4File <library__file |
| _8h.html#a0>`__ \*    |   *cfile*   | )    |    |                        |
| +--------------------------------+------+------------------------------- |
| ----------------------+-------------+------+----+                        |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_is\_buffered:            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is the file buffered                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------+------+----------------------------------- |
| ------------------+-------------+------+----+                            |
| | int ccp4\_file\_is\_read   | (    | const `CCP4File <library__file_8h. |
| html#a0>`__ \*    |   *cfile*   | )    |    |                            |
| +----------------------------+------+----------------------------------- |
| ------------------+-------------+------+----+                            |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_is\_read:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is the @cfile readable               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true.                       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------+------+-------------------------------- |
| ---------------------+-------------+------+----+                         |
| | int ccp4\_file\_is\_scratch   | (    | const `CCP4File <library__file_ |
| 8h.html#a0>`__ \*    |   *cfile*   | )    |    |                         |
| +-------------------------------+------+-------------------------------- |
| ---------------------+-------------+------+----+                         |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_is\_scratch:             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is scratch file                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true.                       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------+------+---------------------------------- |
| -------------------+-------------+------+----+                           |
| | int ccp4\_file\_is\_write   | (    | const `CCP4File <library__file_8h |
| .html#a0>`__ \*    |   *cfile*   | )    |    |                           |
| +-----------------------------+------+---------------------------------- |
| -------------------+-------------+------+----+                           |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_is\_write:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is the @cfile writeable              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------+------+----------------------------------- |
| ------------------+-------------+------+----+                            |
| | int ccp4\_file\_itemsize   | (    | const `CCP4File <library__file_8h. |
| html#a0>`__ \*    |   *cfile*   | )    |    |                            |
| +----------------------------+------+----------------------------------- |
| ------------------+-------------+------+----+                            |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_itemsize:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     itemsize of @cfile.              |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------------------------------ |
| -----------+-------------+------+----+                                   |
| | long ccp4\_file\_length   | (    | `CCP4File <library__file_8h.html#a0 |
| >`__ \*    |   *cfile*   | )    |    |                                   |
| +---------------------------+------+------------------------------------ |
| -----------+-------------+------+----+                                   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_length:                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Length of file on disk.              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     length of @cfile on success, EOF |
|                                      |     on failure                       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+--------------------------------------- |
| --------------+-------------+------+----+                                |
| | int ccp4\_file\_mode   | (    | const `CCP4File <library__file_8h.html |
| #a0>`__ \*    |   *cfile*   | )    |    |                                |
| +------------------------+------+--------------------------------------- |
| --------------+-------------+------+----+                                |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_mode:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | get data mode of @cfile (BYTE =0,    |
|                                      | INT16 =1, INT32=6, FLOAT32 =2,       |
|                                      | COMP32 =3, COMP64 =4)                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     mode                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+------+------------------------------ |
| -----------------+-------------+------+----+                             |
| | const char\* ccp4\_file\_name   | (    | `CCP4File <library__file_8h.h |
| tml#a0>`__ \*    |   *cfile*   | )    |    |                             |
| +---------------------------------+------+------------------------------ |
| -----------------+-------------+------+----+                             |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_name:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | strdup @cfile->name                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     name of file as const char \*    |
+--------------------------------------+--------------------------------------+

`CCP4File <library__file_8h.html#a0>`__\ \* ccp4\_file\_open

( 

const char \* 

  *filename*,

const int 

  *flag*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_open:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *filename*    | (const char \* |
|                                      | ) filename                           |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |    |                                 |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *flag*        | (const int) i/ |
|                                      | o mode, possible values are O\_RDONL |
|                                      | Y, O\_WRONLY, O\_RDWR, O\_APPEND, O\ |
|                                      | _TMP, O\_CREAT, O\_TRUNC - see `ccp4 |
|                                      | \_sysdep.h <ccp4__sysdep_8h.html>`__ |
|                                      |    |                                 |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |                                      |
|                                      | initialise CCP4File struct for file  |
|                                      | filename with mode @flag. If         |
|                                      | !buffered use open(), otherwise      |
|                                      | fopen() The struct stat is check to  |
|                                      | determine if file is a regular file, |
|                                      | if it is, and is not stdin, it is    |
|                                      | assumed to be direct access.         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     (CCP4File \*) on success, NULL   |
|                                      |     on failure                       |
+--------------------------------------+--------------------------------------+

`CCP4File <library__file_8h.html#a0>`__\ \* ccp4\_file\_open\_fd

( 

const int 

  *fd*,

const int 

  *flag*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_open\_fd:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *fd*      | (const int) file d |
|                                      | escriptor                            |
|                                      |                                 |    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *flag*    | (const int) io mod |
|                                      | e (O\_RDONLY =0, O\_WRONLY =1, O\_RD |
|                                      | WR =2, O\_TMP =, O\_APPEND =)   |    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |                                      |
|                                      | initialise CCP4File struct with file |
|                                      | descriptor @fd and mode @flag The    |
|                                      | struct stat is check to determine if |
|                                      | file is a regular file, if it is,    |
|                                      | and is not stdin, it is assumed to   |
|                                      | be direct access.                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     (CCP4File \*) on success, NULL   |
|                                      |     on failure                       |
+--------------------------------------+--------------------------------------+

`CCP4File <library__file_8h.html#a0>`__\ \* ccp4\_file\_open\_file

( 

const FILE \* 

  *file*,

const int 

  *flag*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_open\_file:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *file*    | (const FILE \*) FI |
|                                      | LE struct                            |
|                                      |                                 |    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *flag*    | (const int) io mod |
|                                      | e (O\_RDONLY =0, O\_WRONLY =1, O\_RD |
|                                      | WR =2, O\_TMP =, O\_APPEND =)   |    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |                                      |
|                                      | open @cfile with existing handle     |
|                                      | FILE struct file and mode @flag. The |
|                                      | struct stat is check to determine if |
|                                      | file is a regular file, if it is,    |
|                                      | and is not stdin, it is assumed to   |
|                                      | be direct access.                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     (CCP4File \*) on success, NULL   |
|                                      |     on failure                       |
+--------------------------------------+--------------------------------------+

char\* ccp4\_file\_print

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

char \* 

  *msg\_start*,

char \* 

  *msg\_end*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_print:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     @cfile information in char array |
|                                      |     for printing.                    |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
| | int ccp4\_file\_rarch   | (    | `CCP4File <library__file_8h.html#a0>` |
| __ \*    |   *cfile*   | )    |    |                                     |
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_rarch:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | read machine stamp from file         |
|                                      | @cfile->stream. The machine stamp is |
|                                      | at @cfile->stamp\_loc items, set by  |
|                                      | `ccp4\_file\_setstamp <library__file |
|                                      | _8c.html#a24>`__\ ()                 |
|                                      | (default 0). NB. these values may be |
|                                      | overrriden with the environmental    |
|                                      | variable CONVERT\_FROM.              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     fileFT \| (fileIT<<8)            |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_raw\_read

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

char \* 

  *buffer*,

size\_t 

  *n\_items*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_raw\_read:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *cfile*       | \* (CCP4File \ |
|                                      | *)            |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *buffer*      | \* (char \*) i |
|                                      | nput array    |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *n\_items*    | (size\_t) numb |
|                                      | er of items   |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      | reads block of n\_items bytes from   |
|                                      | cfile to buffer via FILE struct      |
|                                      | cfile->stream(fread) or file desc    |
|                                      | cfile->fd read/\_read). Increments   |
|                                      | location value cfile->loc. The       |
|                                      | cfile->iostat flag is set on         |
|                                      | failure.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of bytes read.            |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_raw\_seek

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

long 

  *offset*,

int 

  *whence*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_raw\_seek:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |                            |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *offset*    | (long) offset in |
|                                      |  bytes                     |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *whence*    | (int) SEEK\_SET, |
|                                      |  SEEK\_CUR, or SEEK\_END   |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      | if the file is "seekable" (not       |
|                                      | stdin) the function seeks on @cfile  |
|                                      | by offset bytes using fseek/ftell    |
|                                      | (@cfile->stream) or lseek            |
|                                      | (@cfile->fd). SEEK\_SET is relative  |
|                                      | to start of file, SEEK\_CUR to       |
|                                      | current, SEEK\_END to end.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     offset in bytes on success, -1   |
|                                      |     on failure.                      |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_raw\_write

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const char \* 

  *buffer*,

size\_t 

  *n\_items*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_raw\_write:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *cfile*       | (CCP4File \*)  |
|                                      |               |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *buffer*      | (char \*) outp |
|                                      | ut array      |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *n\_items*    | (size\_t) numb |
|                                      | er of items   |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      | writes block of @n\_items bytes from |
|                                      | @buffer to @cfile via FILE struct    |
|                                      | @cfile->stream(fwrite) or file desc  |
|                                      | @cfile->fd(write/\_write).           |
|                                      | Increments @cfile->loc on success,   |
|                                      | or resets on failure, which is then  |
|                                      | used to determine the file length.   |
|                                      | On failure @cfile->iostat is set.    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of bytes written.         |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_read

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_read:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | mode dependent read function. Reads  |
|                                      | @nitems items from stream            |
|                                      | @cfile->stream to @buffer as         |
|                                      | determined by cfile->mode.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of items read on success, |
|                                      |     EOF on failure                   |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_readchar

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_readchar:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | character read function. Reads       |
|                                      | @nitems characters from stream       |
|                                      | @cfile->stream to @buffer.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of characters read on     |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_readcomp

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_readcomp:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | float complex {float,float} read     |
|                                      | function. Reads @nitems complex from |
|                                      | stream @cfile->stream to @buffer.    |
|                                      | Allows short count when eof is       |
|                                      | detected ( buffered input only).     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of complex read on        |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_readfloat

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_readfloat:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | float read function. Reads @nitems   |
|                                      | floats from stream @cfile->stream to |
|                                      | @buffer.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of floats read on         |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_readint

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_readint:                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | integer read function. Reads @nitems |
|                                      | int from stream @cfile->stream to    |
|                                      | @buffer.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of int read on success,   |
|                                      |     EOF on failure                   |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_readshort

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_readshort:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | short read function. Reads @nitems   |
|                                      | shorts from stream @cfile->stream to |
|                                      | @buffer.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of shorts read on         |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_readshortcomp

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_readshortcomp:           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | short complex {short,short} read     |
|                                      | function. Reads @nitems complex from |
|                                      | stream @cfile->stream to @buffer.    |
|                                      | Allows short count when eof is       |
|                                      | detected ( buffered input only).     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of complex read on        |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------------------------------ |
| -----------+-------------+------+----+                                   |
| | void ccp4\_file\_rewind   | (    | `CCP4File <library__file_8h.html#a0 |
| >`__ \*    |   *cfile*   | )    |    |                                   |
| +---------------------------+------+------------------------------------ |
| -----------+-------------+------+----+                                   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_rewind:                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Seek to start of file. Clear error   |
|                                      | status.                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_seek

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

long 

  *offset*,

int 

  *whence*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_seek:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |                            |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *offset*    | (long) offset in |
|                                      |  items                     |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *whence*    | (int) SEEK\_SET, |
|                                      |  SEEK\_CUR, or SEEK\_END   |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      | seeks on file by offset items.       |
|                                      | SEEK\_SET is relative to start of    |
|                                      | file, SEEK\_CUR to current,          |
|                                      | SEEK\_END to end.                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, -1 on failure      |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_setbyte

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const int 

  *byte\_order*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_setbyte:                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | -----+                               |
|                                      |     | *cfile*          | (CCP4File \ |
|                                      | *)   |                               |
|                                      |     +------------------+------------ |
|                                      | -----+                               |
|                                      |     | *byte\_order*    | (int)       |
|                                      |      |                               |
|                                      |     +------------------+------------ |
|                                      | -----+                               |
|                                      |                                      |
|                                      | set byte ordering for file Return:   |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_setmode

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const int 

  *mode*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_setmode:                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | +                                    |
|                                      |     | *cfile*    | (CCP4File \*)     |
|                                      | |                                    |
|                                      |     +------------+------------------ |
|                                      | +                                    |
|                                      |     | *mode*     | (int) io\_mode    |
|                                      | |                                    |
|                                      |     +------------+------------------ |
|                                      | +                                    |
|                                      |                                      |
|                                      | set the data mode of cfile to mode   |
|                                      | (BYTE (8 bit) = 0, INT16 (16 bit) =  |
|                                      | 1, INT32 (32 bit) = 6, FLOAT32 (32   |
|                                      | bit) = 2, COMP32 (2\*16 bit) = 3,    |
|                                      | COMP64 (2\*32 bit) = 4).             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure.    |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_setstamp

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const size\_t 

  *offset*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_setstamp:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | ----------------+                    |
|                                      |     | *cfile*         | (CCP4File \* |
|                                      | )               |                    |
|                                      |     +-----------------+------------- |
|                                      | ----------------+                    |
|                                      |     | *stamp\_loc*    | (size\_t) of |
|                                      | fset in items   |                    |
|                                      |     +-----------------+------------- |
|                                      | ----------------+                    |
|                                      |                                      |
|                                      | set the machine stamp offset in CCP4 |
|                                      | items determined by the mode of      |
|                                      | @cfile. See                          |
|                                      | `ccp4\_file\_setmode <library__file_ |
|                                      | 8c.html#a25>`__\ ().                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------------------------- |
| ----------------+-------------+------+----+                              |
| | int ccp4\_file\_status   | (    | const `CCP4File <library__file_8h.ht |
| ml#a0>`__ \*    |   *cfile*   | )    |    |                              |
| +--------------------------+------+------------------------------------- |
| ----------------+-------------+------+----+                              |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_status:                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     @cfile error status              |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
| | long ccp4\_file\_tell   | (    | `CCP4File <library__file_8h.html#a0>` |
| __ \*    |   *cfile*   | )    |    |                                     |
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_tell:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Current location in file, uses       |
|                                      | either ftell or lseek.               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     current offset of @cfile in      |
|                                      |     bytes.                           |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
| | int ccp4\_file\_warch   | (    | `CCP4File <library__file_8h.html#a0>` |
| __ \*    |   *cfile*   | )    |    |                                     |
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_warch:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File \*)   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | write machine stamp to file          |
|                                      | @cfile->stream. The machine stamp is |
|                                      | placed at @cfile->stamp\_loc items,  |
|                                      | set by                               |
|                                      | `ccp4\_file\_setstamp <library__file |
|                                      | _8c.html#a24>`__\ ()                 |
|                                      | (defaults to 0).                     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_write

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_write:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | mode dependent write function. Write |
|                                      | @nitems items from @buffer to        |
|                                      | @cfile->stream as determined by      |
|                                      | cfile->mode.                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of items written on       |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_writechar

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_writechar:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | char write function. Write @nitems   |
|                                      | items from @buffer to                |
|                                      | @cfile->stream.                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of bytes written on       |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_writecomp

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_writecomp:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | complex {float,float} write          |
|                                      | function. Write @nitems items from   |
|                                      | @buffer to @cfile->stream.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of complex items written  |
|                                      |     on success, EOF on failure       |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_writefloat

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_writefloat:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | float write function. Write @nitems  |
|                                      | items from @buffer to                |
|                                      | @cfile->stream.                      |
|                                      | Returns number of floats written on  |
|                                      | success, EOF on failure              |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_writeint

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_writeint:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | int write function. Write @nitems    |
|                                      | items from @buffer to                |
|                                      | @cfile->stream.                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of int written on         |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_writeshort

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_writeshort:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | short write function. Write @nitems  |
|                                      | items from @buffer to                |
|                                      | @cfile->stream.                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of short written on       |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4\_file\_writeshortcomp

( 

`CCP4File <library__file_8h.html#a0>`__ \* 

  *cfile*,

const uint8 \* 

  *buffer*,

size\_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4\_file\_writeshortcomp:          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File \*)    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 \*) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size\_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | short complex {short,short} write    |
|                                      | function. Write @nitems items from   |
|                                      | @buffer to @cfile->stream.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of complex items written  |
|                                      |     on success, EOF on failure       |
+--------------------------------------+--------------------------------------+
