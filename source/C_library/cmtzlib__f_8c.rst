`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmtzlib\_f.c File Reference
===========================

Fortran API for input, output and manipulation of
`MTZ <structMTZ.html>`__ files. `More... <#_details>`__

| ``#include <math.h>``
| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include <stdlib.h>``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_utils.h"``
| ``#include "cmtzlib.h"``
| ``#include "csymlib.h"``
| ``#include "ccp4_program.h"``
| ``#include "ccp4_general.h"``

| 

Defines
-------

 #define 

**CMTZLIB\_DEBUG**\ (x)

 #define 

**MFILES**   4

 #define 

**MAXSYM**   192

| 

Functions
---------

 void 

**MtzMemTidy** (void)

 int 

**MtzCheckSubInput** (const int mindx, const char \*subname, const int
rwmode)

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a23>`__ (MTZINI, mtzini,(),(),())

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a24>`__ (LROPEN, lropen,(int \*mindx,
fpstr filename, int \*iprint, int \*ifail, int filename\_len),(int
\*mindx, fpstr filename, int \*iprint, int \*ifail),(int \*mindx, fpstr
filename, int filename\_len, int \*iprint, int \*ifail))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a25>`__ (LRTITL, lrtitl,(int \*mindx,
fpstr ftitle, int \*len, int ftitle\_len),(int \*mindx, fpstr ftitle,
int \*len),(int \*mindx, fpstr ftitle, int ftitle\_len, int \*len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a26>`__ (LRHIST, lrhist,(int \*mindx,
fpstr hstrng, int \*nlines, int hstrng\_len),(int \*mindx, fpstr hstrng,
int \*nlines),(int \*mindx, fpstr hstrng, int hstrng\_len, int
\*nlines))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a27>`__ (LRINFO, lrinfo,(int \*mindx,
fpstr versnx, int \*ncolx, int \*nreflx, float \*ranges, int
versnx\_len),(int \*mindx, fpstr versnx, int \*ncolx, int \*nreflx,
float \*ranges),(int \*mindx, fpstr versnx, int versnx\_len, int
\*ncolx, int \*nreflx, float \*ranges))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a28>`__ (LRNCOL, lrncol,(int \*mindx,
int \*ncolx),(int \*mindx, int \*ncolx),(int \*mindx, int \*ncolx))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a29>`__ (LRNREF, lrnref,(int \*mindx,
int \*nreflx),(int \*mindx, int \*nreflx),(int \*mindx, int \*nreflx))

  

**FORTRAN\_SUBR** (LRSORT, lrsort,(int \*mindx, int sortx[5]),(int
\*mindx, int sortx[5]),(int \*mindx, int sortx[5]))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a31>`__ (LRBATS, lrbats,(int \*mindx,
int \*nbatx, int batchx[]),(int \*mindx, int \*nbatx, int batchx[]),(int
\*mindx, int \*nbatx, int batchx[]))

  

**FORTRAN\_SUBR** (LRCLAB, lrclab,(int \*mindx, fpstr clabs, fpstr
ctyps, int \*ncol, int clabs\_len, int ctyps\_len),(int \*mindx, fpstr
clabs, fpstr ctyps, int \*ncol),(int \*mindx, fpstr clabs, int
clabs\_len, fpstr ctyps, int ctyps\_len, int \*ncol))

  

**FORTRAN\_SUBR** (LRCLID, lrclid,(int \*mindx, int csetid[], int
\*ncol),(int \*mindx, int csetid[], int \*ncol),(int \*mindx, int
csetid[], int \*ncol))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a34>`__ (LRCELL, lrcell,(int \*mindx,
float cell[]),(int \*mindx, float cell[]),(int \*mindx, float cell[]))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a35>`__ (LRRSOL, lrrsol,(int \*mindx,
float \*minres, float \*maxres),(int \*mindx, float \*minres, float
\*maxres),(int \*mindx, float \*minres, float \*maxres))

  

**FORTRAN\_SUBR** (LRSYMI, lrsymi,(int \*mindx, int \*nsympx, fpstr
ltypex, int \*nspgrx, fpstr spgrnx, fpstr pgnamx, int ltypex\_len, int
spgrnx\_len, int pgnamx\_len),(int \*mindx, int \*nsympx, fpstr ltypex,
int \*nspgrx, fpstr spgrnx, fpstr pgnamx),(int \*mindx, int \*nsympx,
fpstr ltypex, int ltypex\_len, int \*nspgrx, fpstr spgrnx, int
spgrnx\_len, fpstr pgnamx, int pgnamx\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a37>`__ (LRSYMM, lrsymm,(int \*mindx,
int \*nsymx, float rsymx[MAXSYM][4][4]),(int \*mindx, int \*nsymx, float
rsymx[MAXSYM][4][4]),(int \*mindx, int \*nsymx, float
rsymx[MAXSYM][4][4]))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a38>`__ (LKYIN, lkyin,(const int
\*mindx, const fpstr lsprgi, const int \*nlprgi, const int \*ntok, const
fpstr labin\_line, const int ibeg[], const int iend[], int lsprgi\_len,
int labin\_line\_len),(const int \*mindx, const fpstr lsprgi, const int
\*nlprgi, const int \*ntok, const fpstr labin\_line, const int ibeg[],
const int iend[]),(const int \*mindx, const fpstr lsprgi, int
lsprgi\_len, const int \*nlprgi, const int \*ntok, const fpstr
labin\_line, int labin\_line\_len, const int ibeg[], const int iend[]))

  

**FORTRAN\_SUBR** (LKYOUT, lkyout,(const int \*mindx, const fpstr
lsprgo, const int \*nlprgo, const int \*ntok, const fpstr labin\_line,
const int ibeg[], const int iend[], int lsprgo\_len, int
labin\_line\_len),(const int \*mindx, const fpstr lsprgo, const int
\*nlprgo, const int \*ntok, const fpstr labin\_line, const int ibeg[],
const int iend[]),(const int \*mindx, const fpstr lsprgo, int
lsprgo\_len, const int \*nlprgo, const int \*ntok, const fpstr
labin\_line, int labin\_line\_len, const int ibeg[], const int iend[]))

  

**FORTRAN\_SUBR** (LKYSET, lkyset,(const fpstr lsprgi, const int
\*nlprgi, fpstr lsusrj, int kpoint[], const int \*itok, const int
\*ntok, const fpstr labin\_line, const int ibeg[], const int iend[], int
lsprgi\_len, int lsusrj\_len, int labin\_line\_len),(const fpstr lsprgi,
const int \*nlprgi, fpstr lsusrj, int kpoint[], const int \*itok, const
int \*ntok, const fpstr labin\_line, const int ibeg[], const int
iend[]),(const fpstr lsprgi, int lsprgi\_len, const int \*nlprgi, fpstr
lsusrj, int lsusrj\_len, int kpoint[], const int \*itok, const int
\*ntok, const fpstr labin\_line, int labin\_line\_len, const int ibeg[],
const int iend[]))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a41>`__ (LRASSN, lrassn,(const int
\*mindx, fpstr lsprgi, int \*nlprgi, int lookup[], fpstr ctprgi, int
lsprgi\_len, int ctprgi\_len),(const int \*mindx, fpstr lsprgi, int
\*nlprgi, int lookup[], fpstr ctprgi),(const int \*mindx, fpstr lsprgi,
int lsprgi\_len, int \*nlprgi, int lookup[], fpstr ctprgi, int
ctprgi\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a42>`__ (LRIDX, lridx,(const int
\*mindx, fpstr project\_name, fpstr crystal\_name, fpstr dataset\_name,
int \*isets, float \*datcell, float \*datwave, int \*ndatasets, int
project\_name\_len, int crystal\_name\_len, int
dataset\_name\_len),(const int \*mindx, fpstr project\_name, fpstr
crystal\_name, fpstr dataset\_name, int \*isets, float \*datcell, float
\*datwave, int \*ndatasets),(const int \*mindx, fpstr project\_name, int
project\_name\_len, fpstr crystal\_name, int crystal\_name\_len, fpstr
dataset\_name, int dataset\_name\_len, int \*isets, float \*datcell,
float \*datwave, int \*ndatasets))

  

**FORTRAN\_SUBR** (LRCELX, lrcelx,(const int \*mindx, const int \*iset,
float \*mtzcell),(const int \*mindx, const int \*iset, float
\*mtzcell),(const int \*mindx, const int \*iset, float \*mtzcell))

  

**FORTRAN\_SUBR** (LRIDC, lridc,(const int \*mindx, fpstr project\_name,
fpstr dataset\_name, int \*isets, float \*datcell, float \*datwave, int
\*ndatasets, int project\_name\_len, int dataset\_name\_len),(const int
\*mindx, fpstr project\_name, fpstr dataset\_name, int \*isets, float
\*datcell, float \*datwave, int \*ndatasets),(const int \*mindx, fpstr
project\_name, int project\_name\_len, fpstr dataset\_name, int
dataset\_name\_len, int \*isets, float \*datcell, float \*datwave, int
\*ndatasets))

  

**FORTRAN\_SUBR** (LRID, lrid,(const int \*mindx, fpstr project\_name,
fpstr dataset\_name, int \*isets, int \*ndatasets, int
project\_name\_len, int dataset\_name\_len),(const int \*mindx, fpstr
project\_name, fpstr dataset\_name, int \*isets, int \*ndatasets),(const
int \*mindx, fpstr project\_name, int project\_name\_len, fpstr
dataset\_name, int dataset\_name\_len, int \*isets, int \*ndatasets))

  

**FORTRAN\_SUBR** (LRSEEK, lrseek,(const int \*mindx, int
\*nrefl),(const int \*mindx, int \*nrefl),(const int \*mindx, int
\*nrefl))

  

**FORTRAN\_SUBR** (LRREFL, lrrefl,(const int \*mindx, float \*resol,
float adata[], ftn\_logical \*eof),(const int \*mindx, float \*resol,
float adata[], ftn\_logical \*eof),(const int \*mindx, float \*resol,
float adata[], ftn\_logical \*eof))

  

**FORTRAN\_SUBR** (LRREFM, lrrefm,(const int \*mindx, ftn\_logical
logmiss[]),(const int \*mindx, ftn\_logical logmiss[]),(const int
\*mindx, ftn\_logical logmiss[]))

  

**FORTRAN\_SUBR** (MTZ\_CHECK\_FOR\_MNF, mtz\_check\_for\_mnf,(const int
\*mindx, const int \*ndata, float adata[], ftn\_logical
logmiss[]),(const int \*mindx, const int \*ndata, float adata[],
ftn\_logical logmiss[]),(const int \*mindx, const int \*ndata, float
adata[], ftn\_logical logmiss[]))

  

**FORTRAN\_SUBR** (LHPRT, lhprt,(const int \*mindx, const int
\*iprint),(const int \*mindx, const int \*iprint),(const int \*mindx,
const int \*iprint))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a51>`__ (LRBAT, lrbat,(const int
\*mindx, int \*batno, float rbatch[], fpstr cbatch, const int \*iprint,
int cbatch\_len),(const int \*mindx, int \*batno, float rbatch[], fpstr
cbatch, const int \*iprint),(const int \*mindx, int \*batno, float
rbatch[], fpstr cbatch, int cbatch\_len, const int \*iprint))

  

**FORTRAN\_SUBR** (LBPRT, lbprt,(const int \*ibatch, const int \*iprint,
float rbatch[], fpstr cbatch, int cbatch\_len),(const int \*ibatch,
const int \*iprint, float rbatch[], fpstr cbatch),(const int \*ibatch,
const int \*iprint, float rbatch[], fpstr cbatch, int cbatch\_len))

  

**FORTRAN\_SUBR** (LRBRES, lrbres,(const int \*mindx, const int
\*batno),(const int \*mindx, const int \*batno),(const int \*mindx,
const int \*batno))

  

**FORTRAN\_SUBR** (LRBTIT, lrbtit,(const int \*mindx, const int \*batno,
fpstr tbatch, const int \*iprint, int tbatch\_len),(const int \*mindx,
const int \*batno, fpstr tbatch, const int \*iprint),(const int \*mindx,
const int \*batno, fpstr tbatch, int tbatch\_len, const int \*iprint))

  

**FORTRAN\_SUBR** (LRBSCL, lrbscl,(const int \*mindx, const int \*batno,
float batscl[], int \*nbatsc),(const int \*mindx, const int \*batno,
float batscl[], int \*nbatsc),(const int \*mindx, const int \*batno,
float batscl[], int \*nbatsc))

  

**FORTRAN\_SUBR** (LRBSETID, lrbsetid,(const int \*mindx, const int
\*batno, int \*bsetid),(const int \*mindx, const int \*batno, int
\*bsetid),(const int \*mindx, const int \*batno, int \*bsetid))

  

**FORTRAN\_SUBR** (LRREWD, lrrewd,(const int \*mindx),(const int
\*mindx),(const int \*mindx))

  

**FORTRAN\_SUBR** (LSTRSL, lstrsl,(const int \*mindx, const float \*a,
const float \*b, const float \*c, const float \*alpha, const float
\*beta, const float \*gamma),(const int \*mindx, const float \*a, const
float \*b, const float \*c, const float \*alpha, const float \*beta,
const float \*gamma),(const int \*mindx, const float \*a, const float
\*b, const float \*c, const float \*alpha, const float \*beta, const
float \*gamma))

  

**FORTRAN\_SUBR** (LSTLSQ1, lstlsq1,(float \*reso, const int \*mindx,
const int \*ih, const int \*ik, const int \*il),(float \*reso, const int
\*mindx, const int \*ih, const int \*ik, const int \*il),(float \*reso,
const int \*mindx, const int \*ih, const int \*ik, const int \*il))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a60>`__ (LWOPEN\_NOEXIT,
lwopen\_noexit,(const int \*mindx, fpstr filename, int \*ifail, int
filename\_len),(const int \*mindx, fpstr filename, int \*ifail),(const
int \*mindx, fpstr filename, int filename\_len, int \*ifail))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a61>`__ (LWOPEN, lwopen,(const int
\*mindx, fpstr filename, int filename\_len),(const int \*mindx, fpstr
filename),(const int \*mindx, fpstr filename, int filename\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a62>`__ (LWTITL, lwtitl,(const int
\*mindx, const fpstr ftitle, const int \*flag, int ftitle\_len),(const
int \*mindx, const fpstr ftitle, const int \*flag),(const int \*mindx,
const fpstr ftitle, int ftitle\_len, const int \*flag))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a63>`__ (LWSORT, lwsort,(const int
\*mindx, int sortx[5]),(const int \*mindx, int sortx[5]),(const int
\*mindx, int sortx[5]))

  

**FORTRAN\_SUBR** (LWHSTL, lwhstl,(int \*mindx, const fpstr hstrng, int
hstrng\_len),(int \*mindx, const fpstr hstrng),(int \*mindx, const fpstr
hstrng, int hstrng\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a65>`__ (LWID, lwid,(const int
\*mindx, const fpstr project\_name, const fpstr dataset\_name, int
project\_name\_len, int dataset\_name\_len),(const int \*mindx, const
fpstr project\_name, const fpstr dataset\_name),(const int \*mindx,
const fpstr project\_name, int project\_name\_len, const fpstr
dataset\_name, int dataset\_name\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a66>`__ (LWIDC, lwidc,(const int
\*mindx, const fpstr project\_name, const fpstr dataset\_name, float
datcell[6], float \*datwave, int project\_name\_len, int
dataset\_name\_len),(const int \*mindx, const fpstr project\_name, const
fpstr dataset\_name, float datcell[6], float \*datwave),(const int
\*mindx, const fpstr project\_name, int project\_name\_len, const fpstr
dataset\_name, int dataset\_name\_len, float datcell[6], float
\*datwave))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a67>`__ (LWIDX, lwidx,(const int
\*mindx, const fpstr project\_name, const fpstr crystal\_name, const
fpstr dataset\_name, float datcell[6], float \*datwave, int
project\_name\_len, int crystal\_name\_len, int
dataset\_name\_len),(const int \*mindx, const fpstr project\_name, const
fpstr crystal\_name, const fpstr dataset\_name, float datcell[6], float
\*datwave),(const int \*mindx, const fpstr project\_name, int
project\_name\_len, const fpstr crystal\_name, int crystal\_name\_len,
const fpstr dataset\_name, int dataset\_name\_len, float datcell[6],
float \*datwave))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a68>`__ (LWCELL, lwcell,(const int
\*mindx, float cell[6]),(const int \*mindx, float cell[6]),(const int
\*mindx, float cell[6]))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a69>`__ (LWIDAS, lwidas,(const int
\*mindx, int \*nlprgo, fpstr pname, fpstr dname, int \*iappnd, int
pname\_len, int dname\_len),(const int \*mindx, int \*nlprgo, fpstr
pname, fpstr dname, int \*iappnd),(const int \*mindx, int \*nlprgo,
fpstr pname, int pname\_len, fpstr dname, int dname\_len, int \*iappnd))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a70>`__ (LWIDASX, lwidasx,(const int
\*mindx, int \*nlprgo, fpstr xname, fpstr dname, int \*iappnd, int
xname\_len, int dname\_len),(const int \*mindx, int \*nlprgo, fpstr
xname, fpstr dname, int \*iappnd),(const int \*mindx, int \*nlprgo,
fpstr xname, int xname\_len, fpstr dname, int dname\_len, int \*iappnd))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a71>`__ (LWIDALL, lwidall,(const int
\*mindx, fpstr xname, fpstr dname, int xname\_len, int
dname\_len),(const int \*mindx, fpstr xname, fpstr dname),(const int
\*mindx, fpstr xname, int xname\_len, fpstr dname, int dname\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a72>`__ (LWSYMM, lwsymm,(int \*mindx,
int \*nsymx, int \*nsympx, float rsymx[MAXSYM][4][4], fpstr ltypex, int
\*nspgrx, fpstr spgrnx, fpstr pgnamx, int ltypex\_len, int spgrnx\_len,
int pgnamx\_len),(int \*mindx, int \*nsymx, int \*nsympx, float
rsymx[MAXSYM][4][4], fpstr ltypex, int \*nspgrx, fpstr spgrnx, fpstr
pgnamx),(int \*mindx, int \*nsymx, int \*nsympx, float
rsymx[MAXSYM][4][4], fpstr ltypex, int ltypex\_len, int \*nspgrx, fpstr
spgrnx, int spgrnx\_len, fpstr pgnamx, int pgnamx\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a73>`__ (LWASSN, lwassn,(const int
\*mindx, fpstr lsprgo, const int \*nlprgo, fpstr ctprgo, int \*iappnd,
int lsprgo\_len, int ctprgo\_len),(const int \*mindx, fpstr lsprgo,
const int \*nlprgo, fpstr ctprgo, int \*iappnd),(const int \*mindx,
fpstr lsprgo, int lsprgo\_len, const int \*nlprgo, fpstr ctprgo, int
ctprgo\_len, int \*iappnd))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a74>`__ (LWBAT, lwbat,(const int
\*mindx, int \*batno, float rbatch[], fpstr cbatch, int
cbatch\_len),(const int \*mindx, int \*batno, float rbatch[], fpstr
cbatch),(const int \*mindx, int \*batno, float rbatch[], fpstr cbatch,
int cbatch\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a75>`__ (LWBTIT, lwbtit,(const int
\*mindx, int \*batno, fpstr tbatch, int tbatch\_len),(const int \*mindx,
int \*batno, fpstr tbatch),(const int \*mindx, int \*batno, fpstr
tbatch, int tbatch\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a76>`__ (LWBSCL, lwbscl,(const int
\*mindx, int \*batno, float batscl[], int \*nbatsc),(const int \*mindx,
int \*batno, float batscl[], int \*nbatsc),(const int \*mindx, int
\*batno, float batscl[], int \*nbatsc))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a77>`__ (LWBSETID, lwbsetid,(const
int \*mindx, const int \*batno, const fpstr project\_name, const fpstr
dataset\_name, int project\_name\_len, int dataset\_name\_len),(const
int \*mindx, const int \*batno, const fpstr project\_name, const fpstr
dataset\_name),(const int \*mindx, const int \*batno, const fpstr
project\_name, int project\_name\_len, const fpstr dataset\_name, int
dataset\_name\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a78>`__ (LWBSETIDX, lwbsetidx,(const
int \*mindx, const int \*batno, const fpstr crystal\_name, const fpstr
dataset\_name, int crystal\_name\_len, int dataset\_name\_len),(const
int \*mindx, const int \*batno, const fpstr crystal\_name, const fpstr
dataset\_name),(const int \*mindx, const int \*batno, const fpstr
crystal\_name, int crystal\_name\_len, const fpstr dataset\_name, int
dataset\_name\_len))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a79>`__ (EQUAL\_MAGIC,
equal\_magic,(const int \*mindx, float adata[], const int \*ncol),(const
int \*mindx, float adata[], const int \*ncol),(const int \*mindx, float
adata[], const int \*ncol))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a80>`__ (SET\_MAGIC,
set\_magic,(const int \*mindx, float \*val\_magic, ftn\_logical
\*setval),(const int \*mindx, float \*val\_magic, ftn\_logical
\*setval),(const int \*mindx, float \*val\_magic, ftn\_logical
\*setval))

  

**FORTRAN\_SUBR** (RESET\_MAGIC, reset\_magic,(const int \*mindx, const
float adata[], float bdata[], const int \*ncol, const float
\*val\_magica, const float \*val\_magicb),(const int \*mindx, const
float adata[], float bdata[], const int \*ncol, const float
\*val\_magica, const float \*val\_magicb),(const int \*mindx, const
float adata[], float bdata[], const int \*ncol, const float
\*val\_magica, const float \*val\_magicb))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a82>`__ (LWREFL\_NOEXIT,
lwrefl\_noexit,(const int \*mindx, const float adata[], int
\*ifail),(const int \*mindx, const float adata[], int \*ifail),(const
int \*mindx, const float adata[], int \*ifail))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a83>`__ (LWREFL, lwrefl,(const int
\*mindx, const float adata[]),(const int \*mindx, const float
adata[]),(const int \*mindx, const float adata[]))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a84>`__ (LWCLOS\_NOEXIT,
lwclos\_noexit,(const int \*mindx, int \*iprint, int \*ifail),(const int
\*mindx, int \*iprint, int \*ifail),(const int \*mindx, int \*iprint,
int \*ifail))

 

`FORTRAN\_SUBR <cmtzlib__f_8c.html#a85>`__ (LWCLOS, lwclos,(const int
\*mindx, int \*iprint),(const int \*mindx, int \*iprint),(const int
\*mindx, int \*iprint))

  

**FORTRAN\_SUBR** (RBATHD, rbathd,(),(),())

  

**FORTRAN\_SUBR** (WBATHD, wbathd,(),(),())

  

**FORTRAN\_SUBR** (LRHDRL, lrhdrl,(),(),())

  

**FORTRAN\_SUBR** (SORTUP, sortup,(),(),())

  

**FORTRAN\_SUBR** (ADDLIN, addlin,(),(),())

  

**FORTRAN\_FUN** (int, NEXTLN, nextln,(),(),())

  

**FORTRAN\_SUBR** (IS\_MAGIC, is\_magic,(const float \*val\_magic, const
float \*valtst, ftn\_logical \*lvalms),(const float \*val\_magic, const
float \*valtst, ftn\_logical \*lvalms),(const float \*val\_magic, const
float \*valtst, ftn\_logical \*lvalms))

--------------

Detailed Description
--------------------

Fortran API for input, output and manipulation of
`MTZ <structMTZ.html>`__ files.

 **Author:**
    Martyn Winn

--------------

Function Documentation
----------------------

FORTRAN\_SUBR

( 

LWCLOS 

 ,

lwclos 

 ,

(const int \*mindx, int \*iprint) 

 ,

(const int \*mindx, int \*iprint) 

 ,

(const int \*mindx, int \*iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write `MTZ <structMTZ.html>`__ file  |
|                                      | header and close output file.        |
|                                      | Wrapper for MtzPut.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index              |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *iprint*    | (I) Specify whet |
|                                      | her to write output file header to l |
|                                      | og.   |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWCLOS\_NOEXIT 

 ,

lwclos\_noexit 

 ,

(const int \*mindx, int \*iprint, int \*ifail) 

 ,

(const int \*mindx, int \*iprint, int \*ifail) 

 ,

(const int \*mindx, int \*iprint, int \*ifail) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write `MTZ <structMTZ.html>`__ file  |
|                                      | header and close output file.        |
|                                      | Wrapper for MtzPut.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index              |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *iprint*    | (I) Specify whet |
|                                      | her to write output file header to l |
|                                      | og.   |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *ifail*     | (O) Returns 0 if |
|                                      |  successful, non-zero otherwise.     |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWREFL 

 ,

lwrefl 

 ,

(const int \*mindx, const float adata[]) 

 ,

(const int \*mindx, const float adata[]) 

 ,

(const int \*mindx, const float adata[]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write a one array of reflection data |
|                                      | to output file. This is a wrapper    |
|                                      | for ccp4\_lwrefl. This routine exits |
|                                      | upon failure.                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
|                                      |     | *mindx*    | (I) `MTZ <structM |
|                                      | TZ.html>`__ file index   |           |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
|                                      |     | *adata*    | (I) Array of refl |
|                                      | ection data to write.    |           |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWREFL\_NOEXIT 

 ,

lwrefl\_noexit 

 ,

(const int \*mindx, const float adata[], int \*ifail) 

 ,

(const int \*mindx, const float adata[], int \*ifail) 

 ,

(const int \*mindx, const float adata[], int \*ifail) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write a one array of reflection data |
|                                      | to output file. This is a wrapper    |
|                                      | for ccp4\_lwrefl.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *mindx*    | (I) `MTZ <structM |
|                                      | TZ.html>`__ file index            |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *adata*    | (I) Array of refl |
|                                      | ection data to write.             |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *ifail*    | (O) Returns 0 if  |
|                                      | successful, non-zero otherwise.   |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

SET\_MAGIC 

 ,

set\_magic 

 ,

(const int \*mindx, float \*val\_magic, ftn\_logical \*setval) 

 ,

(const int \*mindx, float \*val\_magic, ftn\_logical \*setval) 

 ,

(const int \*mindx, float \*val\_magic, ftn\_logical \*setval) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set or get MNF of file.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------------------+              |
|                                      |     | *mindx*         | `MTZ <struct |
|                                      | MTZ.html>`__ file index              |
|                                      |                                      |
|                                      |                                      |
|                                      |                       |              |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------------------+              |
|                                      |     | *val\_magic*    | Value of MNF |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                       |              |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------------------+              |
|                                      |     | *setval*        | If true, set |
|                                      |  the MNF with the value in val\_magi |
|                                      | c. If false, return value of MNF in  |
|                                      | val\_magic. Returned as true, unless |
|                                      |  there is an error.   |              |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

EQUAL\_MAGIC 

 ,

equal\_magic 

 ,

(const int \*mindx, float adata[], const int \*ncol) 

 ,

(const int \*mindx, float adata[], const int \*ncol) 

 ,

(const int \*mindx, float adata[], const int \*ncol) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set whole array to MNF. The value of |
|                                      | the MNF is taken from the            |
|                                      | `MTZ <structMTZ.html>`__ struct on   |
|                                      | unit mindx.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----------------------------------+ |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index                 | |
|                                      |     +------------+------------------ |
|                                      | -----------------------------------+ |
|                                      |     | *adata*    | Array of reflecti |
|                                      | on data to be initialised.         | |
|                                      |     +------------+------------------ |
|                                      | -----------------------------------+ |
|                                      |     | *ncol*     | Number of columns |
|                                      |  in the array to be initialised.   | |
|                                      |     +------------+------------------ |
|                                      | -----------------------------------+ |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWBSETIDX 

 ,

lwbsetidx 

 ,

(const int \*mindx, const int \*batno, const fpstr crystal\_name, const
fpstr dataset\_name, int crystal\_name\_len, int dataset\_name\_len) 

 ,

(const int \*mindx, const int \*batno, const fpstr crystal\_name, const
fpstr dataset\_name) 

 ,

(const int \*mindx, const int \*batno, const fpstr crystal\_name, int
crystal\_name\_len, const fpstr dataset\_name, int dataset\_name\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Assign a batch to a particular       |
|                                      | dataset, identified by crystal name  |
|                                      | and dataset name.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index   |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *batno*            | Serial nu |
|                                      | mber of batch.               |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *crystal\_name*    | Crystal N |
|                                      | ame                          |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *dataset\_name*    | Dataset N |
|                                      | ame                          |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWBSETID 

 ,

lwbsetid 

 ,

(const int \*mindx, const int \*batno, const fpstr project\_name, const
fpstr dataset\_name, int project\_name\_len, int dataset\_name\_len) 

 ,

(const int \*mindx, const int \*batno, const fpstr project\_name, const
fpstr dataset\_name) 

 ,

(const int \*mindx, const int \*batno, const fpstr project\_name, int
project\_name\_len, const fpstr dataset\_name, int dataset\_name\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Obsolete. Use LWBSETIDX              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index   |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *batno*            | Serial nu |
|                                      | mber of batch.               |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *project\_name*    | Project N |
|                                      | ame                          |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *dataset\_name*    | Dataset N |
|                                      | ame                          |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWBSCL 

 ,

lwbscl 

 ,

(const int \*mindx, int \*batno, float batscl[], int \*nbatsc) 

 ,

(const int \*mindx, int \*batno, float batscl[], int \*nbatsc) 

 ,

(const int \*mindx, int \*batno, float batscl[], int \*nbatsc) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write batch header for batch number  |
|                                      | batno. New batch scales are set.     |
|                                      | batno must correspond to             |
|                                      | pre-existing batch.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batno*     | Serial number of |
|                                      |  batch.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batscl*    | Array of batch s |
|                                      | cales.                |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *nbatsc*    | Number of batch  |
|                                      | scales.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWBTIT 

 ,

lwbtit 

 ,

(const int \*mindx, int \*batno, fpstr tbatch, int tbatch\_len) 

 ,

(const int \*mindx, int \*batno, fpstr tbatch) 

 ,

(const int \*mindx, int \*batno, fpstr tbatch, int tbatch\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write batch header for batch number  |
|                                      | batno. Only the batch title is       |
|                                      | provided, so dummy header is         |
|                                      | written.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batno*     | Serial number of |
|                                      |  batch.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *tbatch*    | Batch title.     |
|                                      |                       |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWBAT 

 ,

lwbat 

 ,

(const int \*mindx, int \*batno, float rbatch[], fpstr cbatch, int
cbatch\_len) 

 ,

(const int \*mindx, int \*batno, float rbatch[], fpstr cbatch) 

 ,

(const int \*mindx, int \*batno, float rbatch[], fpstr cbatch, int
cbatch\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write batch header for batch number  |
|                                      | batno.                               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batno*     | Serial number of |
|                                      |  batch.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *rbatch*    | Real/integer bat |
|                                      | ch information.       |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *cbatch*    | Character batch  |
|                                      | information.          |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWASSN 

 ,

lwassn 

 ,

(const int \*mindx, fpstr lsprgo, const int \*nlprgo, fpstr ctprgo, int
\*iappnd, int lsprgo\_len, int ctprgo\_len) 

 ,

(const int \*mindx, fpstr lsprgo, const int \*nlprgo, fpstr ctprgo, int
\*iappnd) 

 ,

(const int \*mindx, fpstr lsprgo, int lsprgo\_len, const int \*nlprgo,
fpstr ctprgo, int ctprgo\_len, int \*iappnd) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to assign columns of |
|                                      | output `MTZ <structMTZ.html>`__      |
|                                      | file. First this updates labels from |
|                                      | user\_label\_out if set by lkyout,   |
|                                      | then sets collookup\_out array of    |
|                                      | pointers to columns.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index.                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |           |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *lsprgo*    | array of output  |
|                                      | labels                               |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |           |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *nlprgo*    | number of output |
|                                      |  labels                              |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |           |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *ctprgo*    | array of output  |
|                                      | column types                         |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |           |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *iappnd*    | if = 0 replace a |
|                                      | ll existing columns, else if = 1 "ap |
|                                      | pend" to existing columns. Note that |
|                                      |  columns are appended to the relevan |
|                                      | t datasets and are not therefore nec |
|                                      | essarily at the end of the list of c |
|                                      | olumns.   |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWSYMM 

 ,

lwsymm 

 ,

(int \*mindx, int \*nsymx, int \*nsympx, float rsymx[MAXSYM][4][4],
fpstr ltypex, int \*nspgrx, fpstr spgrnx, fpstr pgnamx, int ltypex\_len,
int spgrnx\_len, int pgnamx\_len) 

 ,

(int \*mindx, int \*nsymx, int \*nsympx, float rsymx[MAXSYM][4][4],
fpstr ltypex, int \*nspgrx, fpstr spgrnx, fpstr pgnamx) 

 ,

(int \*mindx, int \*nsymx, int \*nsympx, float rsymx[MAXSYM][4][4],
fpstr ltypex, int ltypex\_len, int \*nspgrx, fpstr spgrnx, int
spgrnx\_len, fpstr pgnamx, int pgnamx\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write or update symmetry information |
|                                      | for `MTZ <structMTZ.html>`__ header. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index.             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nsymx*     | (I) number of sy |
|                                      | mmetry operators                     |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nsympx*    | (I) number of pr |
|                                      | imitive symmetry operators           |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *rsymx*     | (I) Array of sym |
|                                      | metry operators as 4 x 4 matrices. E |
|                                      | ach matrix is input with translation |
|                                      | s in elements [3][\*] (i.e. reversed |
|                                      |  with respect to the way the Fortran |
|                                      |  application sees it). This function |
|                                      |  reverses the order before passing t |
|                                      | o ccp4\_lwsymm.   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *ltypex*    | (I) lattice type |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nspgrx*    | (I) spacegroup n |
|                                      | umber                                |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *spgrnx*    | (I) spacegroup n |
|                                      | ame                                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *pgnamx*    | (I) point group  |
|                                      | name                                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWIDALL 

 ,

lwidall 

 ,

(const int \*mindx, fpstr xname, fpstr dname, int xname\_len, int
dname\_len) 

 ,

(const int \*mindx, fpstr xname, fpstr dname) 

 ,

(const int \*mindx, fpstr xname, int xname\_len, fpstr dname, int
dname\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Assign output columns to             |
|                                      | crystal/datasets. This is a simpler  |
|                                      | version of LWIDASX to assign all     |
|                                      | columns to one dataset (except for   |
|                                      | HKL which are assigned to base       |
|                                      | dataset).                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index.   |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *xname*    | Crystal name for  |
|                                      | all columns.          |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *dname*    | Dataset name for  |
|                                      | all columns.          |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWIDASX 

 ,

lwidasx 

 ,

(const int \*mindx, int \*nlprgo, fpstr xname, fpstr dname, int
\*iappnd, int xname\_len, int dname\_len) 

 ,

(const int \*mindx, int \*nlprgo, fpstr xname, fpstr dname, int
\*iappnd) 

 ,

(const int \*mindx, int \*nlprgo, fpstr xname, int xname\_len, fpstr
dname, int dname\_len, int \*iappnd) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Assign output columns to             |
|                                      | crystal/datasets.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index.                 |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nlprgo*    | Number of output |
|                                      |  columns.                            |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *xname*     | Array of crystal |
|                                      |  names for columns.                  |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *dname*     | Array of dataset |
|                                      |  names for columns.                  |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *iappnd*    | If 0 then assign |
|                                      |  all columns, if 1 then assign appen |
|                                      | ded columns.   |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWIDAS 

 ,

lwidas 

 ,

(const int \*mindx, int \*nlprgo, fpstr pname, fpstr dname, int
\*iappnd, int pname\_len, int dname\_len) 

 ,

(const int \*mindx, int \*nlprgo, fpstr pname, fpstr dname, int
\*iappnd) 

 ,

(const int \*mindx, int \*nlprgo, fpstr pname, int pname\_len, fpstr
dname, int dname\_len, int \*iappnd) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Obsolete - use LWIDASX.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index.                 |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nlprgo*    | Number of output |
|                                      |  columns.                            |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *pname*     | Array of project |
|                                      |  names.                              |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *dname*     | Array of dataset |
|                                      |  names.                              |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *iappnd*    | If 0 then assign |
|                                      |  all columns, if 1 then assign appen |
|                                      | ded columns.   |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWCELL 

 ,

lwcell 

 ,

(const int \*mindx, float cell[6]) 

 ,

(const int \*mindx, float cell[6]) 

 ,

(const int \*mindx, float cell[6]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to update cell of    |
|                                      | output `MTZ <structMTZ.html>`__      |
|                                      | file. Overall cell is obsolete - we  |
|                                      | only store crystal cell dimensions.  |
|                                      | Therefore this simply writes the     |
|                                      | cell dimensions for any crystal      |
|                                      | which has not yet been set. Crystal  |
|                                      | cell dimensions should be set        |
|                                      | directly with lwidc.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index.   |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *cell*     | Output cell dimen |
|                                      | sions.                |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWIDX 

 ,

lwidx 

 ,

(const int \*mindx, const fpstr project\_name, const fpstr
crystal\_name, const fpstr dataset\_name, float datcell[6], float
\*datwave, int project\_name\_len, int crystal\_name\_len, int
dataset\_name\_len) 

 ,

(const int \*mindx, const fpstr project\_name, const fpstr
crystal\_name, const fpstr dataset\_name, float datcell[6], float
\*datwave) 

 ,

(const int \*mindx, const fpstr project\_name, int project\_name\_len,
const fpstr crystal\_name, int crystal\_name\_len, const fpstr
dataset\_name, int dataset\_name\_len, float datcell[6], float
\*datwave) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to ccp4\_lwidx for   |
|                                      | writing dataset header information.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index.          |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *project\_name*    | Name of p |
|                                      | roject that parent crystal belongs t |
|                                      | o.   |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *crystal\_name*    | Name of p |
|                                      | arent crystal.                       |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *dataset\_name*    | Name of d |
|                                      | ataset.                              |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *datcell*          | Cell dime |
|                                      | nsions of parent crystal.            |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *datwave*          | Wavelengt |
|                                      | h of dataset.                        |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWIDC 

 ,

lwidc 

 ,

(const int \*mindx, const fpstr project\_name, const fpstr
dataset\_name, float datcell[6], float \*datwave, int
project\_name\_len, int dataset\_name\_len) 

 ,

(const int \*mindx, const fpstr project\_name, const fpstr
dataset\_name, float datcell[6], float \*datwave) 

 ,

(const int \*mindx, const fpstr project\_name, int project\_name\_len,
const fpstr dataset\_name, int dataset\_name\_len, float datcell[6],
float \*datwave) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to ccp4\_lwidx for   |
|                                      | writing dataset header information.  |
|                                      | As for LWIDX except crystal name is  |
|                                      | not provided, and defaults to        |
|                                      | supplied project name. This exists   |
|                                      | for backwards-compatibility - use    |
|                                      | LWIDX instead.                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index.          |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *project\_name*    | Name of p |
|                                      | roject that parent crystal belongs t |
|                                      | o.   |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *dataset\_name*    | Name of d |
|                                      | ataset.                              |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *datcell*          | Cell dime |
|                                      | nsions of parent crystal.            |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *datwave*          | Wavelengt |
|                                      | h of dataset.                        |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWID 

 ,

lwid 

 ,

(const int \*mindx, const fpstr project\_name, const fpstr
dataset\_name, int project\_name\_len, int dataset\_name\_len) 

 ,

(const int \*mindx, const fpstr project\_name, const fpstr
dataset\_name) 

 ,

(const int \*mindx, const fpstr project\_name, int project\_name\_len,
const fpstr dataset\_name, int dataset\_name\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to ccp4\_lwidx for   |
|                                      | writing dataset header information.  |
|                                      | As for LWIDX except crystal name is  |
|                                      | not provided, and defaults to        |
|                                      | supplied project name. Also cell and |
|                                      | wavelength are not provided and      |
|                                      | default to zero. This exists for     |
|                                      | backwards-compatibility - use LWIDX  |
|                                      | instead.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index.          |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *project\_name*    | Name of p |
|                                      | roject that parent crystal belongs t |
|                                      | o.   |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *dataset\_name*    | Name of d |
|                                      | ataset.                              |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWSORT 

 ,

lwsort 

 ,

(const int \*mindx, int sortx[5]) 

 ,

(const int \*mindx, int sortx[5]) 

 ,

(const int \*mindx, int sortx[5]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set sort order for output file. The  |
|                                      | integer array is stored as static.   |
|                                      | Try to set sort order now, but may   |
|                                      | not be possible if LWCLAB/LWASSN not |
|                                      | yet called.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index.   |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *sortx*    | Sort order as int |
|                                      | eger array.           |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWTITL 

 ,

lwtitl 

 ,

(const int \*mindx, const fpstr ftitle, const int \*flag, int
ftitle\_len) 

 ,

(const int \*mindx, const fpstr ftitle, const int \*flag) 

 ,

(const int \*mindx, const fpstr ftitle, int ftitle\_len, const int
\*flag) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set title for output file.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -----------------------------+       |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index.                 |
|                                      |                              |       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -----------------------------+       |
|                                      |     | *ftitle*    | Title to be adde |
|                                      | d to output `MTZ <structMTZ.html>`__ |
|                                      |  file.                       |       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -----------------------------+       |
|                                      |     | *flag*      | =0 replace old t |
|                                      | itle with new one, or =1 append new  |
|                                      | one to old, with one space   |       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -----------------------------+       |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWOPEN 

 ,

lwopen 

 ,

(const int \*mindx, fpstr filename, int filename\_len) 

 ,

(const int \*mindx, fpstr filename) 

 ,

(const int \*mindx, fpstr filename, int filename\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to open output       |
|                                      | `MTZ <structMTZ.html>`__ file. In    |
|                                      | fact, if reflection data is being    |
|                                      | held in memory, defer opening until  |
|                                      | MtzPut call. But if reflections are  |
|                                      | written immediately to file, need to |
|                                      | open now.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | -------------------------+           |
|                                      |     | *mindx*       | `MTZ <structMT |
|                                      | Z.html>`__ file index.   |           |
|                                      |     +---------------+--------------- |
|                                      | -------------------------+           |
|                                      |     | *filename*    | Output file na |
|                                      | me.                      |           |
|                                      |     +---------------+--------------- |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LWOPEN\_NOEXIT 

 ,

lwopen\_noexit 

 ,

(const int \*mindx, fpstr filename, int \*ifail, int filename\_len) 

 ,

(const int \*mindx, fpstr filename, int \*ifail) 

 ,

(const int \*mindx, fpstr filename, int filename\_len, int \*ifail) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to open output       |
|                                      | `MTZ <structMTZ.html>`__ file. In    |
|                                      | fact, if reflection data is being    |
|                                      | held in memory, defer opening until  |
|                                      | MtzPut call. But if reflections are  |
|                                      | written immediately to file, need to |
|                                      | open now.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *mindx*       | `MTZ <structMT |
|                                      | Z.html>`__ file index.               |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *filename*    | Output file na |
|                                      | me.                                  |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *ifail*       | (O) Returns 0  |
|                                      | if successful, non-zero otherwise.   |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRBAT 

 ,

lrbat 

 ,

(const int \*mindx, int \*batno, float rbatch[], fpstr cbatch, const int
\*iprint, int cbatch\_len) 

 ,

(const int \*mindx, int \*batno, float rbatch[], fpstr cbatch, const int
\*iprint) 

 ,

(const int \*mindx, int \*batno, float rbatch[], fpstr cbatch, int
cbatch\_len, const int \*iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for ccp4\_lrbat.     |
|                                      | Returns the header info for the next |
|                                      | batch from the multi-record          |
|                                      | `MTZ <structMTZ.html>`__ file open   |
|                                      | on index MINDX, as the two arrays    |
|                                      | RBATCH (for numbers) and CBATCH (for |
|                                      | characters).                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index                  |
|                                      |          |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *batno*     | On return, batch |
|                                      |  number                              |
|                                      |          |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *rbatch*    | On return, real  |
|                                      | and integer batch data.              |
|                                      |          |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *cbatch*    | On return, chara |
|                                      | cter batch data (title and axes name |
|                                      | s).      |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *iprint*    | =0 no printing,  |
|                                      | =1 print title only, >1 print full h |
|                                      | eader.   |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRIDX 

 ,

lridx 

 ,

(const int \*mindx, fpstr project\_name, fpstr crystal\_name, fpstr
dataset\_name, int \*isets, float \*datcell, float \*datwave, int
\*ndatasets, int project\_name\_len, int crystal\_name\_len, int
dataset\_name\_len) 

 ,

(const int \*mindx, fpstr project\_name, fpstr crystal\_name, fpstr
dataset\_name, int \*isets, float \*datcell, float \*datwave, int
\*ndatasets) 

 ,

(const int \*mindx, fpstr project\_name, int project\_name\_len, fpstr
crystal\_name, int crystal\_name\_len, fpstr dataset\_name, int
dataset\_name\_len, int \*isets, float \*datcell, float \*datwave, int
\*ndatasets) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for ccp4\_lridx.     |
|                                      | Return dataset information. Note     |
|                                      | requirement to input how much memory |
|                                      | allocated in calling routine.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index           |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *project\_name*    |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *crystal\_name*    |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *dataset\_name*    |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *isets*            |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *datcell*          |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *datwave*          |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *ndatasets*        | On input: |
|                                      |  space reserved for dataset informat |
|                                      | ion. On output: number of datasets f |
|                                      | ound.   |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRASSN 

 ,

lrassn 

 ,

(const int \*mindx, fpstr lsprgi, int \*nlprgi, int lookup[], fpstr
ctprgi, int lsprgi\_len, int ctprgi\_len) 

 ,

(const int \*mindx, fpstr lsprgi, int \*nlprgi, int lookup[], fpstr
ctprgi) 

 ,

(const int \*mindx, fpstr lsprgi, int lsprgi\_len, int \*nlprgi, int
lookup[], fpstr ctprgi, int ctprgi\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to ccp4\_lrassn.     |
|                                      | First this updates labels from       |
|                                      | user\_label\_in if set by lkyin,     |
|                                      | then sets collookup array of         |
|                                      | pointers to columns.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index.             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                          |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *lsprgi*    | (I) Array of pro |
|                                      | gram column labels. These are the co |
|                                      | lumn labels used by the program, as  |
|                                      | opposed to the column labels in the  |
|                                      | file.                                |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                          |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *nlprgi*    | (I) Number of in |
|                                      | put program labels.                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                          |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *lookup*    | (I/O) On input,  |
|                                      | indicates whether a column is compul |
|                                      | sory or not (-1 = hard compulsory -  |
|                                      | program will fail if column not foun |
|                                      | d, 1 = soft compulsory - program wil |
|                                      | l attempt to find column even if not |
|                                      |  assigned on LABIN, 0 = optional). O |
|                                      | n output, gives the index of the col |
|                                      | umn in the input file.   |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *ctprgi*    | (I) Array of col |
|                                      | umn types.                           |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                          |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LKYIN 

 ,

lkyin 

 ,

(const int \*mindx, const fpstr lsprgi, const int \*nlprgi, const int
\*ntok, const fpstr labin\_line, const int ibeg[], const int iend[], int
lsprgi\_len, int labin\_line\_len) 

 ,

(const int \*mindx, const fpstr lsprgi, const int \*nlprgi, const int
\*ntok, const fpstr labin\_line, const int ibeg[], const int iend[]) 

 ,

(const int \*mindx, const fpstr lsprgi, int lsprgi\_len, const int
\*nlprgi, const int \*ntok, const fpstr labin\_line, int
labin\_line\_len, const int ibeg[], const int iend[]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to MtzParseLabin.    |
|                                      | This matches tokens from the LABIN   |
|                                      | line against the program labels      |
|                                      | supplied by the program.             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *mindx*          | (I) `MTZ <s |
|                                      | tructMTZ.html>`__ file index.        |
|                                      |                                      |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *lsprgi*         | (I) Array o |
|                                      | f program column labels. These are t |
|                                      | he column labels used by the program |
|                                      | , as opposed to the column labels in |
|                                      |  the file.   |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *nlprgi*         | (I) Number  |
|                                      | of input program labels.             |
|                                      |                                      |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *ntok*           | (I) From Pa |
|                                      | rser: number of tokens on line       |
|                                      |                                      |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *labin\_line*    | (I) From Pa |
|                                      | rser: input line                     |
|                                      |                                      |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *ibeg*           | (I) From Pa |
|                                      | rser: array of starting delimiters f |
|                                      | or each token                        |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *iend*           | (I) From Pa |
|                                      | rser: array of ending delimiters for |
|                                      |  each token                          |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRSYMM 

 ,

lrsymm 

 ,

(int \*mindx, int \*nsymx, float rsymx[MAXSYM][4][4]) 

 ,

(int \*mindx, int \*nsymx, float rsymx[MAXSYM][4][4]) 

 ,

(int \*mindx, int \*nsymx, float rsymx[MAXSYM][4][4]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Get symmetry matrices from           |
|                                      | `MTZ <structMTZ.html>`__ structure.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *mindx*    | (I) `MTZ <structM |
|                                      | TZ.html>`__ file index.              |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                |     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *nsymx*    | (O) Number of sym |
|                                      | metry operators held in `MTZ <struct |
|                                      | MTZ.html>`__ header.                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                |     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *rsymx*    | (O) Symmetry oper |
|                                      | ators as 4 x 4 matrices, in the orde |
|                                      | r they are held in the `MTZ <structM |
|                                      | TZ.html>`__ header. Each matrix has  |
|                                      | translations in elements [3][\*]. No |
|                                      | te that a Fortran application will r |
|                                      | everse the order of indices.   |     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRRSOL 

 ,

lrrsol 

 ,

(int \*mindx, float \*minres, float \*maxres) 

 ,

(int \*mindx, float \*minres, float \*maxres) 

 ,

(int \*mindx, float \*minres, float \*maxres) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the overall resolution limits |
|                                      | of the `MTZ <structMTZ.html>`__      |
|                                      | structure. These are the widest      |
|                                      | limits over all crystals present.    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index   |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *minres*    | (O) minimum reso |
|                                      | lution                    |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *maxres*    | (O) maximum reso |
|                                      | lution                    |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRCELL 

 ,

lrcell 

 ,

(int \*mindx, float cell[]) 

 ,

(int \*mindx, float cell[]) 

 ,

(int \*mindx, float cell[]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the nominal cell dimensions   |
|                                      | of the `MTZ <structMTZ.html>`__      |
|                                      | structure. In fact, these are the    |
|                                      | cell dimensions of the 1st crystal   |
|                                      | in the `MTZ <structMTZ.html>`__      |
|                                      | structure. It is better to use the   |
|                                      | cell dimensions of the correct       |
|                                      | crystal, as obtained from LRIDX.     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
|                                      |     | *mindx*    | (I) `MTZ <structM |
|                                      | TZ.html>`__ file index   |           |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
|                                      |     | *cell*     | (O) Cell dimensio |
|                                      | ns.                      |           |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRBATS 

 ,

lrbats 

 ,

(int \*mindx, int \*nbatx, int batchx[]) 

 ,

(int \*mindx, int \*nbatx, int batchx[]) 

 ,

(int \*mindx, int \*nbatx, int batchx[]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for ccp4\_lrbats.    |
|                                      | May be called for non-multirecord    |
|                                      | file, just to check there are no     |
|                                      | batches.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *nbatx*     | Number of batche |
|                                      | s found.              |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batchx*    | Array of batch n |
|                                      | umbers.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRNREF 

 ,

lrnref 

 ,

(int \*mindx, int \*nreflx) 

 ,

(int \*mindx, int \*nreflx) 

 ,

(int \*mindx, int \*nreflx) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Returns the current reflection       |
|                                      | number from an                       |
|                                      | `MTZ <structMTZ.html>`__ file opened |
|                                      | for read. Files are normally read    |
|                                      | sequentially, the number returned is |
|                                      | the number of the \*NEXT\*           |
|                                      | reflection record to be read. If you |
|                                      | are going to jump about the file     |
|                                      | with LRSEEK then use this to record  |
|                                      | the current position before you      |
|                                      | start, so that it can be restored    |
|                                      | afterwards, if required.             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index                  |
|                                      |              |                       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *nreflx*    | the reflection r |
|                                      | ecord number of the next reflection  |
|                                      | to be read   |                       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRNCOL 

 ,

lrncol 

 ,

(int \*mindx, int \*ncolx) 

 ,

(int \*mindx, int \*ncolx) 

 ,

(int \*mindx, int \*ncolx) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to function          |
|                                      | returning number of columns read in  |
|                                      | from input file.                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index   |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *ncolx*    | Number of columns |
|                                      | .                    |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRINFO 

 ,

lrinfo 

 ,

(int \*mindx, fpstr versnx, int \*ncolx, int \*nreflx, float \*ranges,
int versnx\_len) 

 ,

(int \*mindx, fpstr versnx, int \*ncolx, int \*nreflx, float \*ranges) 

 ,

(int \*mindx, fpstr versnx, int versnx\_len, int \*ncolx, int \*nreflx,
float \*ranges) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to functions         |
|                                      | returning number of reflections,     |
|                                      | columns and ranges. In fact, it      |
|                                      | returns current values on MINDX      |
|                                      | rather than those of the input file. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *versnx*    | (O) `MTZ <struct |
|                                      | MTZ.html>`__ version. This is the ve |
|                                      | rsion of the current library rather  |
|                                      | than that of the input file. If thes |
|                                      | e differ, a warning will have been i |
|                                      | ssued by LROPEN/MtzGet.   |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *ncolx*     | Number of column |
|                                      | s.                                   |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *nreflx*    | Number of reflec |
|                                      | tions.                               |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *ranges*    | Array of column  |
|                                      | ranges.                              |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRHIST 

 ,

lrhist 

 ,

(int \*mindx, fpstr hstrng, int \*nlines, int hstrng\_len) 

 ,

(int \*mindx, fpstr hstrng, int \*nlines) 

 ,

(int \*mindx, fpstr hstrng, int hstrng\_len, int \*nlines) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Get history lines from               |
|                                      | `MTZ <structMTZ.html>`__ file opened |
|                                      | for read.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------+            |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index              |
|                                      |                                      |
|                                      |                                      |
|                                      |                         |            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------+            |
|                                      |     | *hstrng*    | (O) Array of his |
|                                      | tory lines.                          |
|                                      |                                      |
|                                      |                                      |
|                                      |                         |            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------+            |
|                                      |     | *nlines*    | (I/O) On input,  |
|                                      | dimension of hstrng, i.e. the maximu |
|                                      | m number of history lines to be retu |
|                                      | rned. On output, actual number of hi |
|                                      | story lines returned.   |            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------+            |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LRTITL 

 ,

lrtitl 

 ,

(int \*mindx, fpstr ftitle, int \*len, int ftitle\_len) 

 ,

(int \*mindx, fpstr ftitle, int \*len) 

 ,

(int \*mindx, fpstr ftitle, int ftitle\_len, int \*len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Get title from                       |
|                                      | `MTZ <structMTZ.html>`__ file opened |
|                                      | for read.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index   |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *ftitle*    | (O) Title.       |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *len*       | (O) Length of re |
|                                      | turned title.             |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

LROPEN 

 ,

lropen 

 ,

(int \*mindx, fpstr filename, int \*iprint, int \*ifail, int
filename\_len) 

 ,

(int \*mindx, fpstr filename, int \*iprint, int \*ifail) 

 ,

(int \*mindx, fpstr filename, int filename\_len, int \*iprint, int
\*ifail) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Open an `MTZ <structMTZ.html>`__     |
|                                      | file for reading. This is a wrapper  |
|                                      | to MtzGet.                           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *mindx*       | (I) `MTZ <stru |
|                                      | ctMTZ.html>`__ file index            |
|                                      |         |                            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *filename*    | (I) Filename t |
|                                      | o open (real or logical)             |
|                                      |         |                            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *iprint*      | (I) Specifies  |
|                                      | how much header information to print |
|                                      |  out.   |                            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *ifail*       | (O) Returns 0  |
|                                      | if successful, non-zero otherwise.   |
|                                      |         |                            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MTZINI 

 ,

mtzini 

 ,

() 

 ,

() 

 ,

() 

 

) 

+-----+------------------------------------------------------------------------------------------------------------------------------------------------+
|     | Mainly for backwards compatibility. The only thing it does now is initialise the html/summary stuff, which is usually done by CCPFYP anyway.   |
+-----+------------------------------------------------------------------------------------------------------------------------------------------------+
