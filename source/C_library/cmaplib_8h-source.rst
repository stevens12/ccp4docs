`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmaplib.h
=========

`Go to the documentation of this file. <cmaplib_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      cmaplib.h: C/C++ level API for accessing CCP4 map files
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00046 #ifndef __GUARD_MAPLIB
    00047 #define __GUARD_MAPLIB
    00048 
    00049 #include "ccp4_utils.h"
    00050 
    00051 #ifdef __cplusplus
    00052 namespace CMap_io {
    00053 typedef CCP4::CCP4File CCP4File;
    00054 extern "C" {
    00055 #endif
    00056 
    00057 typedef struct _CMMFile_Skew CMMFile_Skew;
    00058 typedef struct _CMMFile_Labels CMMFile_Labels;
    00059 typedef struct _CMMFile_Symop CMMFile_Symop;
    00060 typedef struct _CMMFile_Data CMMFile_Data;
    00061 typedef struct _CMMFile_Stats CMMFile_Stats;
    00062 typedef struct _CMMFile CMMFile;
    00063 
    00064 struct _CMMFile_Labels {
    00065   unsigned int number;
    00066   char *labels[10];
    00067 };
    00068 
    00069 struct _CMMFile_Skew {
    00070   float rotation[3][3];
    00071   float translation[3];
    00072 };
    00073 
    00074 struct _CMMFile_Symop {
    00075 unsigned int offset;
    00076 unsigned int size;
    00077 unsigned int number;
    00078 };
    00079 
    00080 struct _CMMFile_Data {
    00081   size_t offset;
    00082   size_t section_size;
    00083   size_t header_size;
    00084   size_t block_size;
    00085   unsigned int number;
    00086 };
    00087 
    00088 struct _CMMFile_Stats {
    00089   float offset;                /* pseudo zero value */
    00090   float min;                   /* minimum density value */
    00091   float max;                   /* maximum density value */
    00092   double mean;               /* sum of densities (less offset) */
    00093   double rms;              /* sum of square of densities (less offset) */
    00094   int total;                    /* number of summed densities */
    00095 };
    00096 
    00097 struct _CMMFile {
    00098 CCP4File *stream;
    00099 char *file_name;
    00100 unsigned int data_mode;
    00101 unsigned int close_mode;
    00102 float cell[6];
    00103 int spacegroup;
    00104 int map_dim[3];
    00105 int origin[3];
    00106 int cell_grid[3];
    00107 int axes_order[3];
    00108 CMMFile_Symop symop;
    00109 CMMFile_Data data;
    00110 CMMFile_Stats stats;
    00111 CMMFile_Labels labels;
    00112 CMMFile_Skew skew;
    00113 int reserved[8];
    00114 char user_access[24];
    00115 };
    00116 
    00117 /* open a file for read/write */
    00118 void *ccp4_cmap_open(const char *filename, int mode);
    00119 
    00120 /* close a file for read/write (dumping the header if write) */
    00121 void ccp4_cmap_close(CMMFile *mfile);
    00122 
    00123 /* set the close mode (calculation of map statistics) */
    00124 void ccp4_cmap_closemode(CMMFile *mfile, unsigned int closemode);
    00125 
    00126 /* seek to a section in the map (read mode only)*/
    00127 int ccp4_cmap_seek_section(CMMFile *mfile, int offset, unsigned int seek_mode);
    00128 
    00129 /* seek to a row in a section (read mode only)*/
    00130 int ccp4_cmap_seek_row(CMMFile *, int offset, unsigned int seek_mode);
    00131 
    00132 /* raw seek (read mode only)*/
    00133 int ccp4_cmap_seek_data(CMMFile *, int offset, unsigned int seek_mode);
    00134 
    00135 /* read a map section from file to memory */
    00136 int ccp4_cmap_read_section(CMMFile *mfile, void *section);
    00137 
    00138 /* read a row from file to memory */
    00139 int ccp4_cmap_read_row(CMMFile *mfile, void *row);
    00140 
    00141 /* read n_items from file to memory (item determined by data mode) */
    00142 int ccp4_cmap_read_data(const CMMFile *mfile, void *items, int n_items);
    00143 
    00144 /* write a map section from memory to file */
    00145 int ccp4_cmap_write_section(CMMFile *mfile, const void *section);
    00146 
    00147 /* write a map row from memory to file */
    00148 int ccp4_cmap_write_row(CMMFile *mfile, const void *row);
    00149 
    00150 /* write n_items from memory to file (item determined by data mode) */
    00151 int ccp4_cmap_write_data(CMMFile *mfile, const void *items, int n_items);
    00152 
    00153 /* read the section header corresponding to the current section */
    00154 int ccp4_cmap_read_section_header(const CMMFile *mfile, char *header);
    00155 
    00156 /* write the section header corresponding to the current section */
    00157 int ccp4_cmap_write_section_header(CMMFile *mfile, const char *header);
    00158 
    00159 /* get the header parameters */
    00160 void ccp4_cmap_get_cell(const CMMFile *mfile, float *cell);
    00161 void ccp4_cmap_get_grid(const CMMFile *mfile, int *grid);
    00162 void ccp4_cmap_get_origin(const CMMFile *mfile, int *origin);
    00163 void ccp4_cmap_get_order(const CMMFile *mfile, int *axes_order);
    00164 void ccp4_cmap_get_dim(const CMMFile *mfile, int *map_dim);
    00165 int ccp4_cmap_get_spacegroup(const CMMFile *mfile);
    00166 void ccp4_cmap_get_mapstats(const CMMFile *mfile, float *min, float* max, 
    00167                            double *mean, double *rms);
    00168 
    00169 /* set the header parameters */
    00170 void ccp4_cmap_set_cell(CMMFile *mfile, const float *cell);
    00171 void ccp4_cmap_set_grid(CMMFile *mfile, const int *grid);
    00172 void ccp4_cmap_set_origin(CMMFile *mfile, const int *origin);
    00173 void ccp4_cmap_set_order(CMMFile *mfile, const int *axes_order);
    00174 void ccp4_cmap_set_dim(CMMFile *mfile, const int *map_dim);
    00175 void ccp4_cmap_set_spacegroup(CMMFile *mfile, int spacegroup);
    00176 void ccp4_cmap_set_mapstats(CMMFile *mfile, const float min, const float max,
    00177                            const double mean, const double rms);
    00178 
    00179 /* get map file datamode */
    00180 unsigned int ccp4_cmap_get_datamode(const CMMFile *mfile);
    00181 
    00182 /* set map file datamode */
    00183 void ccp4_cmap_set_datamode(CMMFile *mfile, unsigned int datamode);
    00184 
    00185 /* get the local header size */
    00186 size_t ccp4_cmap_get_local_header(CMMFile *mfile);
    00187 
    00188 /* set the local header size (before data writing begins) */
    00189 void ccp4_cmap_set_local_header(CMMFile *mfile, size_t size);
    00190 
    00191 /* get the number of symops in the file */
    00192 int ccp4_cmap_num_symop(const CMMFile *mfile);
    00193 
    00194 /* seek among the symops strings */
    00195 int ccp4_cmap_seek_symop(CMMFile *mfile, int isymop, unsigned int whence);
    00196 
    00197 /* read a symop string of 80 characters */
    00198 int ccp4_cmap_get_symop(CMMFile *mfile, char *buffer);
    00199 
    00200 /* write a symop string of 80 characters */
    00201 int ccp4_cmap_set_symop(CMMFile *mfile, const char *buffer);
    00202 
    00203 /* get the mask */
    00204 int ccp4_cmap_get_mask(const CMMFile *mfile, float *skew_mat, float *skew_trans);
    00205 
    00206 /* set the mask */
    00207 int ccp4_cmap_set_mask(CMMFile *mfile, const float *skew_mat, const float *skew_trans);
    00208 
    00209 /* the number of labels used */
    00210 int ccp4_cmap_number_label(const CMMFile *mfile);
    00211 
    00212 /* set label at posn from C-string */
    00213 int ccp4_cmap_set_label(CMMFile *mfile, const char *label, int posn);
    00214 
    00215 /* return label at posn as C-string */
    00216 char *ccp4_cmap_get_label(const CMMFile *mfile, int posn);
    00217 
    00218 /* set title (label=0) */
    00219 int ccp4_cmap_set_title(CMMFile *mfile, const char *label);
    00220 
    00221 /* get title (label=0) */
    00222 char *ccp4_cmap_get_title(const CMMFile *mfile);
    00223 
    00224 #ifdef __cplusplus
    00225 }
    00226 }
    00227 #endif
    00228 
    00229 #endif  /* __GUARD_MAPLIB */

.. raw:: html

   </div>
