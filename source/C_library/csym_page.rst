`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CSYM library
------------

.. raw:: html

   <div class="fragment">

::




       

.. raw:: html

   </div>

File list
---------

-  `csymlib.h <csymlib_8h.html>`__ - contains details of the C/C++ API
-  `ccp4\_spg.h <ccp4__spg_8h.html>`__ - contains details of the
   spacegroup data structure

Overview
--------

The CSYM library is centred around a data file ``syminfo.lib`` which is
auto-generated from sgtbx (the Space Group Toolbox of
`cctbx <http://cctbx.sourceforge.net/>`__). A description of the
contents of this file is given in the `documentation <../symlib.html>`__
of the Fortran API.

A particular spacegroup in a particular setting is loaded into an
in-memory data structure by requesting a particular spacegroup name,
number, or set of operators. See the functions
``ccp4spg_load_by_standard_num``, ``ccp4spg_load_by_ccp4_num``,
``ccp4spg_load_by_spgname``, ``ccp4spg_load_by_ccp4_spgname`` and
``ccp4_spgrp_reverse_lookup``. Information on the in-memory data
structure is given in `ccp4\_spg.h <ccp4__spg_8h.html>`__ The memory can
be freed by the function ``ccp4spg_free``.

Functions are provided to:

-  Query the data structure, e.g. ``ccp4spg_symbol_Hall``, etc. (members
   of the structure can of course be obtained directly)
-  Check reciprocal space indices for a particular spacegroup, e.g.
   ``ccp4spg_is_in_asu``, ``ccp4spg_is_centric``,
   ``ccp4spg_get_multiplicity``, ``ccp4spg_is_sysabs``, etc.
-  Set appropriate grids for FFT, e.g. ``set_fft_grid``

Symmetry operators
------------------

Symmetry operators are expressed in a variety of ways:

-  Using the struct ``ccp4_symop``, which consists of a 3 x 3 rotation
   matrix and a translation vector.
-  As a 4 x 4 matrix, in which the rotation matrix is in the
   top-left-hand corner and the translation vector is in elements
   [\*][3]. Element [3][3] is set to 1.0
-  As a string, such as "-x+1/2,-y,z+1/2"

Check the function description for which form is expected. Often, there
are alternative functions if you wish to supply the operators in a
different form. There are also the following conversion functions:

-  rotandtrn\_to\_mat4
-  rotandtrn\_to\_symop
-  mat4\_to\_rotandtrn
-  mat4\_to\_symop
-  mat4\_to\_recip\_symop
-  symop\_to\_rotandtrn
-  symop\_to\_mat4

Note that the order of symmetry operators may be important in some
cases, for example in `MTZ <structMTZ.html>`__ files with a M/ISYM
column where ISYM encodes the symmetry operation used.

Examples
--------

See examples on `ftp area <ftp://ftp.ccp4.ac.uk/mdw/csym>`__
