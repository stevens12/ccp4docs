`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_unitcell\_f.c File Reference
==================================

| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_unitcell.h"``

| 

Functions
---------

  

**FORTRAN\_SUBR** (CCP4UC\_F\_FRAC\_ORTH\_MAT,
ccp4uc\_f\_frac\_orth\_mat,(const float cell[6], const int \*ncode,
float ro[3][3], float rf[3][3], float \*volume),(const float cell[6],
const int \*ncode, float ro[3][3], float rf[3][3], float
\*volume),(const float cell[6], const int \*ncode, float ro[3][3], float
rf[3][3], float \*volume))

  

**FORTRAN\_SUBR** (CCP4UC\_F\_CALC\_RCELL, ccp4uc\_f\_calc\_rcell,(const
float cell[6], float rcell[6], float \*rvolume),(const float cell[6],
float rcell[6], float \*rvolume),(const float cell[6], float rcell[6],
float \*rvolume))

  

**FORTRAN\_SUBR** (CCP4UC\_F\_ORTH\_TO\_FRAC,
ccp4uc\_f\_orth\_to\_frac,(const float rf[3][3], const float xo[3],
float xf[3]),(const float rf[3][3], const float xo[3], float
xf[3]),(const float rf[3][3], const float xo[3], float xf[3]))

  

**FORTRAN\_SUBR** (CCP4UC\_F\_FRAC\_TO\_ORTH,
ccp4uc\_f\_frac\_to\_orth,(const float ro[3][3], const float xf[3],
float xo[3]),(const float ro[3][3], const float xf[3], float
xo[3]),(const float ro[3][3], const float xf[3], float xo[3]))

  

**FORTRAN\_SUBR** (CCP4UC\_F\_ORTHU\_TO\_FRACU,
ccp4uc\_f\_orthu\_to\_fracu,(const float rf[3][3], const float uo[3],
float uf[3]),(const float rf[3][3], const float uo[3], float
uf[3]),(const float rf[3][3], const float uo[3], float uf[3]))

  

**FORTRAN\_SUBR** (CCP4UC\_F\_FRACU\_TO\_ORTHU,
ccp4uc\_f\_fracu\_to\_orthu,(const float ro[3][3], const float uf[3],
float uo[3]),(const float ro[3][3], const float uf[3], float
uo[3]),(const float ro[3][3], const float uf[3], float uo[3]))

  

**FORTRAN\_SUBR** (CELLCHK, cellchk,(const float cell1[6], const float
cell2[6], const float \*errfrc, int \*ierr),(const float cell1[6], const
float cell2[6], const float \*errfrc, int \*ierr),(const float cell1[6],
const float cell2[6], const float \*errfrc, int \*ierr))

--------------

Detailed Description
--------------------

Fortran API to `ccp4\_unitcell.c <ccp4__unitcell_8c.html>`__. Martyn
Winn
