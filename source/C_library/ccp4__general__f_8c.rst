`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_general\_f.c File Reference
=================================

| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include <stdlib.h>``
| ``#include <math.h>``
| ``#include "ccp4_errno.h"``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_parser.h"``
| ``#include "ccp4_program.h"``
| ``#include "ccp4_utils.h"``
| ``#include "ccp4_general.h"``
| ``#include "cmtzlib.h"``
| ``#include "csymlib.h"``

| 

Defines
-------

 #define 

**TMP\_LENGTH**   128

| 

Functions
---------

void 

`ccp4f\_mem\_tidy <ccp4__general__f_8c.html#a2>`__ (void)

  

**FORTRAN\_SUBR** (CCPFYP, ccpfyp,(),(),())

  

**FORTRAN\_SUBR** (CCP4H\_INIT\_CLIB, ccp4h\_init\_clib,(int \*ihtml,
int \*isumm),(int \*ihtml, int \*isumm),(int \*ihtml, int \*isumm))

  

**FORTRAN\_SUBR** (CCPUPC, ccpupc,(fpstr string, int string\_len),(fpstr
string),(fpstr string, int string\_len))

  

**FORTRAN\_SUBR** (CCPERR, ccperr,(const int \*istat, const fpstr
errstr, int errstr\_len),(const int \*istat, const fpstr errstr),(const
int \*istat, const fpstr errstr, int errstr\_len))

  

**FORTRAN\_SUBR** (QPRINT, qprint,(const int \*iflag, const fpstr msg,
int msg\_len),(const int \*iflag, const fpstr msg),(const int \*iflag,
const fpstr msg, int msg\_len))

 

`FORTRAN\_SUBR <ccp4__general__f_8c.html#a8>`__ (UIDATE, uidate,(int
\*imonth, int \*iday, int \*iyear),(int \*imonth, int \*iday, int
\*iyear),(int \*imonth, int \*iday, int \*iyear))

 

`FORTRAN\_SUBR <ccp4__general__f_8c.html#a9>`__ (CCPDAT, ccpdat,(fpstr
caldat, int caldat\_len),(fpstr caldat),(fpstr caldat, int caldat\_len))

  

**FORTRAN\_SUBR** (CCPTIM, ccptim,(int \*iflag, float \*cpu, float
\*elaps),(int \*iflag, float \*cpu, float \*elaps),(int \*iflag, float
\*cpu, float \*elaps))

  

**FORTRAN\_SUBR** (UTIME, utime,(fpstr ctime, int ctime\_len),(fpstr
ctime),(fpstr ctime, int ctime\_len))

  

**FORTRAN\_SUBR** (UCPUTM, ucputm,(float \*sec),(float \*sec),(float
\*sec))

  

**FORTRAN\_SUBR** (CCP4\_VERSION, ccp4\_version,(const fpstr version,
int version\_len),(const fpstr version),(const fpstr version, int
version\_len))

  

**FORTRAN\_SUBR** (CCP4\_PROG\_VERSION, ccp4\_prog\_version,(const fpstr
version, int \*iflag, int version\_len),(const fpstr version, int
\*iflag),(const fpstr version, int version\_len, int \*iflag))

  

**FORTRAN\_SUBR** (CCPVRS, ccpvrs,(const int \*ilp, const fpstr prog,
const fpstr vdate, int prog\_len, int vdate\_len),(const int \*ilp,
const fpstr prog, const fpstr vdate),(const int \*ilp, const fpstr prog,
int prog\_len, const fpstr vdate, int vdate\_len))

  

**FORTRAN\_SUBR** (CCPRCS, ccprcs,(const int \*ilp, const fpstr prog,
const fpstr rcsdat, int prog\_len, int rcsdat\_len),(const int \*ilp,
const fpstr prog, const fpstr rcsdat),(const int \*ilp, const fpstr
prog, int prog\_len, const fpstr rcsdat, int rcsdat\_len))

  

**FORTRAN\_SUBR** (CCPPNM, ccppnm,(const fpstr pnm, int pnm\_len),(const
fpstr pnm, int pnm\_len),(const fpstr pnm, int pnm\_len))

  

**FORTRAN\_FUN** (ftn\_logical, CCPEXS, ccpexs,(const fpstr name, int
name\_len),(const fpstr name),(const fpstr name, int name\_len))

 

`FORTRAN\_SUBR <ccp4__general__f_8c.html#a19>`__ (GETELAPSED,
getelapsed,(void),(void),(void))

--------------

Detailed Description
--------------------

Fortran API to `ccp4\_general.c <ccp4__general_8c.html>`__. Created Oct.
2001 by Martyn Winn

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +-------------------------+------+---------+-----+------+----+           |
| | void ccp4f\_mem\_tidy   | (    | void    |     | )    |    |           |
| +-------------------------+------+---------+-----+------+----+           |
+--------------------------------------------------------------------------+

+-----+-----------------------------------------------------------------------------------------------------------------------------------------------------+
|     | Free all memory malloc'd from static pointers in Fortran interface. To be called before program exit. The function can be registered with atexit.   |
+-----+-----------------------------------------------------------------------------------------------------------------------------------------------------+

FORTRAN\_SUBR

( 

GETELAPSED 

 ,

getelapsed 

 ,

(void) 

 ,

(void) 

 ,

(void) 

 

) 

+-----+------------------------------------------------------------+
|     | Print timing information to stdout wraps ccp4ProgramTime   |
+-----+------------------------------------------------------------+

FORTRAN\_SUBR

( 

CCPDAT 

 ,

ccpdat 

 ,

(fpstr caldat, int caldat\_len) 

 ,

(fpstr caldat) 

 ,

(fpstr caldat, int caldat\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to string data       |
|                                      | function.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------+                  |
|                                      |     | *caldat*    | Date string in f |
|                                      | ormat dd/mm/yy.   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------+                  |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

UIDATE 

 ,

uidate 

 ,

(int \*imonth, int \*iday, int \*iyear) 

 ,

(int \*imonth, int \*iday, int \*iyear) 

 ,

(int \*imonth, int \*iday, int \*iyear) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to integer data      |
|                                      | function.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --+                                  |
|                                      |     | *imonth*    | Month (1-12).    |
|                                      |   |                                  |
|                                      |     +-------------+----------------- |
|                                      | --+                                  |
|                                      |     | *iday*      | Day (1-31).      |
|                                      |   |                                  |
|                                      |     +-------------+----------------- |
|                                      | --+                                  |
|                                      |     | *iyear*     | Year (4 digit).  |
|                                      |   |                                  |
|                                      |     +-------------+----------------- |
|                                      | --+                                  |
+--------------------------------------+--------------------------------------+
