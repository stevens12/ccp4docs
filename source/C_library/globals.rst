`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CINCH File Members
==================

`a <#index_a>`__ \| `c <#index_c>`__ \| `d <#index_d>`__ \| `f <#index_f>`__ \| `g <#index_g>`__ \| `h <#index_h>`__ \| `i <#index_i>`__ \| `m <#index_m>`__ \| `n <#index_n>`__ \| `o <#index_o>`__ \| `r <#index_r>`__ \| `s <#index_s>`__

Here is a list of all documented file members with links to the
documentation:

- a -
~~~~~

-  all\_factors\_le\_19() : `csymlib.h <csymlib_8h.html#a61>`__
-  ASU\_1b() : `csymlib.h <csymlib_8h.html#a14>`__
-  ASU\_2\_m() : `csymlib.h <csymlib_8h.html#a15>`__
-  ASU\_3b() : `csymlib.h <csymlib_8h.html#a19>`__
-  ASU\_3bm() : `csymlib.h <csymlib_8h.html#a20>`__
-  ASU\_3bmx() : `csymlib.h <csymlib_8h.html#a21>`__
-  ASU\_4\_m() : `csymlib.h <csymlib_8h.html#a17>`__
-  ASU\_4\_mmm() : `csymlib.h <csymlib_8h.html#a18>`__
-  ASU\_6\_m() : `csymlib.h <csymlib_8h.html#a22>`__
-  ASU\_6\_mmm() : `csymlib.h <csymlib_8h.html#a23>`__
-  ASU\_m3b() : `csymlib.h <csymlib_8h.html#a24>`__
-  ASU\_m3bm() : `csymlib.h <csymlib_8h.html#a25>`__
-  ASU\_mmm() : `csymlib.h <csymlib_8h.html#a16>`__

- c -
~~~~~

-  ccp4\_byteptr : `ccp4\_array.h <ccp4__array_8h.html#a15>`__
-  ccp4\_cmap\_close() : `cmaplib.h <cmaplib_8h.html#a7>`__
-  ccp4\_cmap\_closemode() : `cmaplib.h <cmaplib_8h.html#a8>`__
-  ccp4\_cmap\_get\_cell() : `cmaplib.h <cmaplib_8h.html#a20>`__
-  ccp4\_cmap\_get\_datamode() : `cmaplib.h <cmaplib_8h.html#a34>`__
-  ccp4\_cmap\_get\_dim() : `cmaplib.h <cmaplib_8h.html#a24>`__
-  ccp4\_cmap\_get\_grid() : `cmaplib.h <cmaplib_8h.html#a21>`__
-  ccp4\_cmap\_get\_label() : `cmaplib.h <cmaplib_8h.html#a46>`__
-  ccp4\_cmap\_get\_local\_header() :
   `cmaplib.h <cmaplib_8h.html#a36>`__
-  ccp4\_cmap\_get\_mapstats() : `cmaplib.h <cmaplib_8h.html#a26>`__
-  ccp4\_cmap\_get\_mask() : `cmaplib.h <cmaplib_8h.html#a42>`__
-  ccp4\_cmap\_get\_order() : `cmaplib.h <cmaplib_8h.html#a23>`__
-  ccp4\_cmap\_get\_origin() : `cmaplib.h <cmaplib_8h.html#a22>`__
-  ccp4\_cmap\_get\_spacegroup() : `cmaplib.h <cmaplib_8h.html#a25>`__
-  ccp4\_cmap\_get\_symop() : `cmaplib.h <cmaplib_8h.html#a40>`__
-  ccp4\_cmap\_get\_title() : `cmaplib.h <cmaplib_8h.html#a48>`__
-  ccp4\_cmap\_num\_symop() : `cmaplib.h <cmaplib_8h.html#a38>`__
-  ccp4\_cmap\_number\_label() : `cmaplib.h <cmaplib_8h.html#a44>`__
-  ccp4\_cmap\_open() : `cmaplib.h <cmaplib_8h.html#a6>`__,
   `cmap\_open.c <cmap__open_8c.html#a3>`__
-  ccp4\_cmap\_read\_data() : `cmaplib.h <cmaplib_8h.html#a14>`__
-  ccp4\_cmap\_read\_row() : `cmaplib.h <cmaplib_8h.html#a13>`__
-  ccp4\_cmap\_read\_section() : `cmaplib.h <cmaplib_8h.html#a12>`__
-  ccp4\_cmap\_read\_section\_header() :
   `cmaplib.h <cmaplib_8h.html#a18>`__
-  ccp4\_cmap\_seek\_data() : `cmaplib.h <cmaplib_8h.html#a11>`__
-  ccp4\_cmap\_seek\_row() : `cmaplib.h <cmaplib_8h.html#a10>`__
-  ccp4\_cmap\_seek\_section() : `cmaplib.h <cmaplib_8h.html#a9>`__
-  ccp4\_cmap\_seek\_symop() : `cmaplib.h <cmaplib_8h.html#a39>`__
-  ccp4\_cmap\_set\_cell() : `cmaplib.h <cmaplib_8h.html#a27>`__
-  ccp4\_cmap\_set\_datamode() : `cmaplib.h <cmaplib_8h.html#a35>`__
-  ccp4\_cmap\_set\_dim() : `cmaplib.h <cmaplib_8h.html#a31>`__
-  ccp4\_cmap\_set\_grid() : `cmaplib.h <cmaplib_8h.html#a28>`__
-  ccp4\_cmap\_set\_label() : `cmaplib.h <cmaplib_8h.html#a45>`__
-  ccp4\_cmap\_set\_local\_header() :
   `cmaplib.h <cmaplib_8h.html#a37>`__
-  ccp4\_cmap\_set\_mapstats() : `cmaplib.h <cmaplib_8h.html#a33>`__
-  ccp4\_cmap\_set\_mask() : `cmaplib.h <cmaplib_8h.html#a43>`__
-  ccp4\_cmap\_set\_order() : `cmaplib.h <cmaplib_8h.html#a30>`__
-  ccp4\_cmap\_set\_origin() : `cmaplib.h <cmaplib_8h.html#a29>`__
-  ccp4\_cmap\_set\_spacegroup() : `cmaplib.h <cmaplib_8h.html#a32>`__
-  ccp4\_cmap\_set\_symop() : `cmaplib.h <cmaplib_8h.html#a41>`__
-  ccp4\_cmap\_set\_title() : `cmaplib.h <cmaplib_8h.html#a47>`__
-  ccp4\_cmap\_write\_data() : `cmaplib.h <cmaplib_8h.html#a17>`__
-  ccp4\_cmap\_write\_row() : `cmaplib.h <cmaplib_8h.html#a16>`__
-  ccp4\_cmap\_write\_section() : `cmaplib.h <cmaplib_8h.html#a15>`__
-  ccp4\_cmap\_write\_section\_header() :
   `cmaplib.h <cmaplib_8h.html#a19>`__
-  ccp4\_constptr : `ccp4\_array.h <ccp4__array_8h.html#a14>`__
-  ccp4\_CtoFString() : `library\_f.c <library__f_8c.html#a2>`__,
   `ccp4\_fortran.h <ccp4__fortran_8h.html#a16>`__
-  ccp4\_errno : `library\_err.c <library__err_8c.html#a1>`__,
   `ccp4\_errno.h <ccp4__errno_8h.html#a17>`__
-  ccp4\_error() : `ccp4\_errno.h <ccp4__errno_8h.html#a18>`__
-  ccp4\_fatal() : `ccp4\_errno.h <ccp4__errno_8h.html#a20>`__
-  ccp4\_file\_byte() : `library\_file.c <library__file_8c.html#a30>`__
-  ccp4\_file\_clearerr() :
   `library\_file.h <library__file_8h.html#a40>`__,
   `library\_file.c <library__file_8c.html#a56>`__
-  ccp4\_file\_close() : `library\_file.h <library__file_8h.html#a6>`__,
   `library\_file.c <library__file_8c.html#a34>`__
-  ccp4\_file\_error() : `library\_file.c <library__file_8c.html#a58>`__
-  ccp4\_file\_fatal() :
   `library\_file.h <library__file_8h.html#a41>`__,
   `library\_file.c <library__file_8c.html#a57>`__
-  ccp4\_file\_feof() : `library\_file.h <library__file_8h.html#a39>`__,
   `library\_file.c <library__file_8c.html#a55>`__
-  ccp4\_file\_flush() :
   `library\_file.h <library__file_8h.html#a36>`__,
   `library\_file.c <library__file_8c.html#a59>`__
-  ccp4\_file\_is\_append() :
   `library\_file.h <library__file_8h.html#a15>`__,
   `library\_file.c <library__file_8c.html#a19>`__
-  ccp4\_file\_is\_buffered() :
   `library\_file.h <library__file_8h.html#a17>`__,
   `library\_file.c <library__file_8c.html#a21>`__
-  ccp4\_file\_is\_read() :
   `library\_file.h <library__file_8h.html#a14>`__,
   `library\_file.c <library__file_8c.html#a18>`__
-  ccp4\_file\_is\_scratch() :
   `library\_file.h <library__file_8h.html#a16>`__,
   `library\_file.c <library__file_8c.html#a20>`__
-  ccp4\_file\_is\_write() :
   `library\_file.h <library__file_8h.html#a13>`__,
   `library\_file.c <library__file_8c.html#a17>`__
-  ccp4\_file\_itemsize() :
   `library\_file.h <library__file_8h.html#a10>`__,
   `library\_file.c <library__file_8c.html#a27>`__
-  ccp4\_file\_length() :
   `library\_file.h <library__file_8h.html#a37>`__,
   `library\_file.c <library__file_8c.html#a53>`__
-  ccp4\_file\_mode() : `library\_file.h <library__file_8h.html#a7>`__,
   `library\_file.c <library__file_8c.html#a26>`__
-  ccp4\_file\_name() : `library\_file.h <library__file_8h.html#a19>`__,
   `library\_file.c <library__file_8c.html#a28>`__
-  ccp4\_file\_open() : `library\_file.h <library__file_8h.html#a1>`__,
   `library\_file.c <library__file_8c.html#a33>`__
-  ccp4\_file\_open\_fd() :
   `library\_file.h <library__file_8h.html#a3>`__,
   `library\_file.c <library__file_8c.html#a32>`__
-  ccp4\_file\_open\_file() :
   `library\_file.h <library__file_8h.html#a2>`__,
   `library\_file.c <library__file_8c.html#a31>`__
-  ccp4\_file\_print() :
   `library\_file.h <library__file_8h.html#a42>`__,
   `library\_file.c <library__file_8c.html#a60>`__
-  ccp4\_file\_rarch() : `library\_file.h <library__file_8h.html#a4>`__,
   `library\_file.c <library__file_8c.html#a35>`__
-  ccp4\_file\_raw\_read() :
   `library\_file.h <library__file_8h.html#a44>`__,
   `library\_file.c <library__file_8c.html#a10>`__
-  ccp4\_file\_raw\_seek() :
   `library\_file.h <library__file_8h.html#a43>`__,
   `library\_file.c <library__file_8c.html#a12>`__
-  ccp4\_file\_raw\_write() :
   `library\_file.h <library__file_8h.html#a45>`__,
   `library\_file.c <library__file_8c.html#a11>`__
-  ccp4\_file\_read() : `library\_file.h <library__file_8h.html#a20>`__,
   `library\_file.c <library__file_8c.html#a37>`__
-  ccp4\_file\_readchar() :
   `library\_file.h <library__file_8h.html#a26>`__,
   `library\_file.c <library__file_8c.html#a43>`__
-  ccp4\_file\_readcomp() :
   `library\_file.h <library__file_8h.html#a21>`__,
   `library\_file.c <library__file_8c.html#a38>`__
-  ccp4\_file\_readfloat() :
   `library\_file.h <library__file_8h.html#a23>`__,
   `library\_file.c <library__file_8c.html#a40>`__
-  ccp4\_file\_readint() :
   `library\_file.h <library__file_8h.html#a24>`__,
   `library\_file.c <library__file_8c.html#a41>`__
-  ccp4\_file\_readshort() :
   `library\_file.h <library__file_8h.html#a25>`__,
   `library\_file.c <library__file_8c.html#a42>`__
-  ccp4\_file\_readshortcomp() :
   `library\_file.h <library__file_8h.html#a22>`__,
   `library\_file.c <library__file_8c.html#a39>`__
-  ccp4\_file\_rewind() :
   `library\_file.h <library__file_8h.html#a35>`__,
   `library\_file.c <library__file_8c.html#a52>`__
-  ccp4\_file\_seek() : `library\_file.h <library__file_8h.html#a34>`__,
   `library\_file.c <library__file_8c.html#a51>`__
-  ccp4\_file\_setbyte() :
   `library\_file.h <library__file_8h.html#a11>`__,
   `library\_file.c <library__file_8c.html#a29>`__
-  ccp4\_file\_setmode() :
   `library\_file.h <library__file_8h.html#a8>`__,
   `library\_file.c <library__file_8c.html#a25>`__
-  ccp4\_file\_setstamp() :
   `library\_file.h <library__file_8h.html#a9>`__,
   `library\_file.c <library__file_8c.html#a24>`__
-  ccp4\_file\_status() :
   `library\_file.h <library__file_8h.html#a18>`__,
   `library\_file.c <library__file_8c.html#a22>`__
-  ccp4\_file\_tell() : `library\_file.h <library__file_8h.html#a38>`__,
   `library\_file.c <library__file_8c.html#a54>`__
-  ccp4\_file\_warch() : `library\_file.h <library__file_8h.html#a5>`__,
   `library\_file.c <library__file_8c.html#a36>`__
-  ccp4\_file\_write() :
   `library\_file.h <library__file_8h.html#a27>`__,
   `library\_file.c <library__file_8c.html#a44>`__
-  ccp4\_file\_writechar() :
   `library\_file.h <library__file_8h.html#a33>`__,
   `library\_file.c <library__file_8c.html#a50>`__
-  ccp4\_file\_writecomp() :
   `library\_file.h <library__file_8h.html#a28>`__,
   `library\_file.c <library__file_8c.html#a45>`__
-  ccp4\_file\_writefloat() :
   `library\_file.h <library__file_8h.html#a30>`__,
   `library\_file.c <library__file_8c.html#a47>`__
-  ccp4\_file\_writeint() :
   `library\_file.h <library__file_8h.html#a31>`__,
   `library\_file.c <library__file_8c.html#a48>`__
-  ccp4\_file\_writeshort() :
   `library\_file.h <library__file_8h.html#a32>`__,
   `library\_file.c <library__file_8c.html#a49>`__
-  ccp4\_file\_writeshortcomp() :
   `library\_file.h <library__file_8h.html#a29>`__,
   `library\_file.c <library__file_8c.html#a46>`__
-  ccp4\_FtoCString() : `library\_f.c <library__f_8c.html#a1>`__,
   `ccp4\_fortran.h <ccp4__fortran_8h.html#a15>`__
-  ccp4\_int\_compare() : `csymlib.h <csymlib_8h.html#a37>`__
-  ccp4\_ismnf() : `cmtzlib.h <cmtzlib_8h.html#a79>`__
-  ccp4\_keymatch() : `ccp4\_parser.h <ccp4__parser_8h.html#a12>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a22>`__
-  ccp4\_lhprt() : `cmtzlib.h <cmtzlib_8h.html#a80>`__
-  ccp4\_lhprt\_adv() : `cmtzlib.h <cmtzlib_8h.html#a81>`__
-  ccp4\_liberr\_verbosity() :
   `ccp4\_errno.h <ccp4__errno_8h.html#a21>`__
-  ccp4\_licence\_exists() :
   `ccp4\_program.h <ccp4__program_8h.html#a16>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a10>`__
-  ccp4\_lrassn() : `cmtzlib.h <cmtzlib_8h.html#a75>`__
-  ccp4\_lrbat() : `cmtzlib.h <cmtzlib_8h.html#a82>`__
-  ccp4\_lrbats() : `cmtzlib.h <cmtzlib_8h.html#a70>`__
-  ccp4\_lrcell() : `cmtzlib.h <cmtzlib_8h.html#a71>`__
-  ccp4\_lrhist() : `cmtzlib.h <cmtzlib_8h.html#a68>`__
-  ccp4\_lridx() : `cmtzlib.h <cmtzlib_8h.html#a76>`__
-  ccp4\_lrreff() : `cmtzlib.h <cmtzlib_8h.html#a78>`__
-  ccp4\_lrrefl() : `cmtzlib.h <cmtzlib_8h.html#a77>`__
-  ccp4\_lrsort() : `cmtzlib.h <cmtzlib_8h.html#a69>`__
-  ccp4\_lrsymi() : `cmtzlib.h <cmtzlib_8h.html#a72>`__
-  ccp4\_lrsymm() : `cmtzlib.h <cmtzlib_8h.html#a73>`__
-  ccp4\_lrtitl() : `cmtzlib.h <cmtzlib_8h.html#a67>`__
-  ccp4\_lwbat() : `cmtzlib.h <cmtzlib_8h.html#a91>`__
-  ccp4\_lwrefl() : `cmtzlib.h <cmtzlib_8h.html#a90>`__
-  ccp4\_lwsymm() : `cmtzlib.h <cmtzlib_8h.html#a87>`__
-  ccp4\_lwtitl() : `cmtzlib.h <cmtzlib_8h.html#a84>`__
-  ccp4\_parse\_end() : `ccp4\_parser.h <ccp4__parser_8h.html#a4>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a14>`__
-  ccp4\_parse\_start() : `ccp4\_parser.h <ccp4__parser_8h.html#a3>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a13>`__
-  ccp4\_parser() : `ccp4\_parser.h <ccp4__parser_8h.html#a11>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a21>`__
-  ccp4\_prog\_vers() : `ccp4\_program.h <ccp4__program_8h.html#a7>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a1>`__
-  ccp4\_ptr : `ccp4\_array.h <ccp4__array_8h.html#a16>`__
-  ccp4\_signal() : `ccp4\_errno.h <ccp4__errno_8h.html#a22>`__
-  ccp4\_spg\_get\_centering() : `csymlib.h <csymlib_8h.html#a12>`__
-  ccp4\_spgrp\_equal() : `csymlib.h <csymlib_8h.html#a34>`__
-  ccp4\_spgrp\_equal\_order() : `csymlib.h <csymlib_8h.html#a35>`__
-  ccp4\_spgrp\_reverse\_lookup() : `csymlib.h <csymlib_8h.html#a5>`__
-  ccp4\_strerror() : `ccp4\_errno.h <ccp4__errno_8h.html#a19>`__
-  ccp4\_symop\_code() : `csymlib.h <csymlib_8h.html#a36>`__
-  ccp4\_symop\_invert() : `csymlib.h <csymlib_8h.html#a27>`__
-  ccp4\_utils\_basename() :
   `library\_utils.c <library__utils_8c.html#a21>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a20>`__
-  ccp4\_utils\_bml() :
   `library\_utils.c <library__utils_8c.html#a12>`__
-  ccp4\_utils\_calloc() :
   `library\_utils.c <library__utils_8c.html#a19>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a17>`__
-  ccp4\_utils\_chmod() :
   `library\_utils.c <library__utils_8c.html#a16>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a14>`__
-  ccp4\_utils\_date() :
   `library\_utils.c <library__utils_8c.html#a26>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a25>`__
-  ccp4\_utils\_etime() :
   `library\_utils.c <library__utils_8c.html#a29>`__
-  ccp4\_utils\_extension() :
   `library\_utils.c <library__utils_8c.html#a23>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a22>`__
-  ccp4\_utils\_flength() :
   `library\_utils.c <library__utils_8c.html#a5>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a1>`__
-  ccp4\_utils\_hgetlimits() :
   `library\_utils.c <library__utils_8c.html#a14>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a12>`__
-  ccp4\_utils\_idate() :
   `library\_utils.c <library__utils_8c.html#a25>`__
-  ccp4\_utils\_isnan() :
   `library\_utils.c <library__utils_8c.html#a11>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a9>`__
-  ccp4\_utils\_itime() :
   `library\_utils.c <library__utils_8c.html#a27>`__
-  ccp4\_utils\_joinfilenames() :
   `library\_utils.c <library__utils_8c.html#a24>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a23>`__
-  ccp4\_utils\_malloc() :
   `library\_utils.c <library__utils_8c.html#a17>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a15>`__
-  ccp4\_utils\_mkdir() :
   `library\_utils.c <library__utils_8c.html#a15>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a13>`__
-  ccp4\_utils\_noinpbuf() :
   `library\_utils.c <library__utils_8c.html#a9>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a7>`__
-  ccp4\_utils\_outbuf() :
   `library\_utils.c <library__utils_8c.html#a8>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a6>`__
-  ccp4\_utils\_pathname() :
   `library\_utils.c <library__utils_8c.html#a22>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a21>`__
-  ccp4\_utils\_print() :
   `library\_utils.c <library__utils_8c.html#a6>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a4>`__
-  ccp4\_utils\_realloc() :
   `library\_utils.c <library__utils_8c.html#a18>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a16>`__
-  ccp4\_utils\_setenv() :
   `library\_utils.c <library__utils_8c.html#a7>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a5>`__
-  ccp4\_utils\_time() :
   `library\_utils.c <library__utils_8c.html#a28>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a27>`__
-  ccp4\_utils\_translate\_mode\_float() :
   `library\_utils.c <library__utils_8c.html#a4>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a2>`__
-  ccp4\_utils\_username() :
   `library\_utils.c <library__utils_8c.html#a20>`__,
   `ccp4\_utils.h <ccp4__utils_8h.html#a19>`__
-  ccp4\_utils\_wrg() :
   `library\_utils.c <library__utils_8c.html#a13>`__
-  ccp4array\_append : `ccp4\_array.h <ccp4__array_8h.html#a4>`__
-  ccp4array\_append\_() : `ccp4\_array.h <ccp4__array_8h.html#a22>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a5>`__
-  ccp4array\_append\_list : `ccp4\_array.h <ccp4__array_8h.html#a6>`__
-  ccp4array\_append\_list\_() :
   `ccp4\_array.h <ccp4__array_8h.html#a24>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a7>`__
-  ccp4array\_append\_n : `ccp4\_array.h <ccp4__array_8h.html#a5>`__
-  ccp4array\_append\_n\_() :
   `ccp4\_array.h <ccp4__array_8h.html#a23>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a6>`__
-  ccp4array\_base : `ccp4\_array.h <ccp4__array_8h.html#a17>`__
-  ccp4array\_delete : `ccp4\_array.h <ccp4__array_8h.html#a9>`__
-  ccp4array\_delete\_() : `ccp4\_array.h <ccp4__array_8h.html#a27>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a10>`__
-  ccp4array\_delete\_last : `ccp4\_array.h <ccp4__array_8h.html#a10>`__
-  ccp4array\_delete\_last\_() :
   `ccp4\_array.h <ccp4__array_8h.html#a28>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a11>`__
-  ccp4array\_delete\_ordered :
   `ccp4\_array.h <ccp4__array_8h.html#a8>`__
-  ccp4array\_delete\_ordered\_() :
   `ccp4\_array.h <ccp4__array_8h.html#a26>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a9>`__
-  ccp4array\_free : `ccp4\_array.h <ccp4__array_8h.html#a12>`__
-  ccp4array\_free\_() : `ccp4\_array.h <ccp4__array_8h.html#a30>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a13>`__
-  ccp4array\_insert : `ccp4\_array.h <ccp4__array_8h.html#a7>`__
-  ccp4array\_insert\_() : `ccp4\_array.h <ccp4__array_8h.html#a25>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a8>`__
-  ccp4array\_new : `ccp4\_array.h <ccp4__array_8h.html#a0>`__
-  ccp4array\_new\_() : `ccp4\_array.h <ccp4__array_8h.html#a18>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a1>`__
-  ccp4array\_new\_size : `ccp4\_array.h <ccp4__array_8h.html#a1>`__
-  ccp4array\_new\_size\_() :
   `ccp4\_array.h <ccp4__array_8h.html#a19>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a2>`__
-  ccp4array\_reserve : `ccp4\_array.h <ccp4__array_8h.html#a3>`__
-  ccp4array\_reserve\_() : `ccp4\_array.h <ccp4__array_8h.html#a21>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a4>`__
-  ccp4array\_resize : `ccp4\_array.h <ccp4__array_8h.html#a2>`__
-  ccp4array\_resize\_() : `ccp4\_array.h <ccp4__array_8h.html#a20>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a3>`__
-  ccp4array\_size : `ccp4\_array.h <ccp4__array_8h.html#a11>`__
-  ccp4array\_size\_() : `ccp4\_array.h <ccp4__array_8h.html#a29>`__,
   `ccp4\_array.c <ccp4__array_8c.html#a12>`__
-  ccp4Callback() : `ccp4\_program.h <ccp4__program_8h.html#a12>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a6>`__
-  ccp4f\_mem\_tidy() :
   `ccp4\_general\_f.c <ccp4__general__f_8c.html#a2>`__
-  CCP4File : `library\_file.h <library__file_8h.html#a0>`__
-  ccp4InvokeCallback() :
   `ccp4\_program.h <ccp4__program_8h.html#a14>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a8>`__
-  CCP4IObj : `ccp4\_diskio\_f.c <ccp4__diskio__f_8c.html#a1>`__
-  ccp4NullCallback() : `ccp4\_program.h <ccp4__program_8h.html#a15>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a9>`__
-  ccp4ProgramName() : `ccp4\_program.h <ccp4__program_8h.html#a8>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a2>`__
-  ccp4ProgramTime() : `ccp4\_program.h <ccp4__program_8h.html#a10>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a4>`__
-  ccp4RCSDate() : `ccp4\_program.h <ccp4__program_8h.html#a9>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a3>`__
-  ccp4SetCallback() : `ccp4\_program.h <ccp4__program_8h.html#a13>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a7>`__
-  ccp4spg\_centric\_phase() : `csymlib.h <csymlib_8h.html#a47>`__
-  ccp4spg\_check\_centric\_zone() : `csymlib.h <csymlib_8h.html#a46>`__
-  ccp4spg\_check\_epsilon\_zone() : `csymlib.h <csymlib_8h.html#a52>`__
-  ccp4spg\_check\_symm\_cell() : `csymlib.h <csymlib_8h.html#a63>`__
-  ccp4spg\_describe\_centric\_zone() :
   `csymlib.h <csymlib_8h.html#a49>`__
-  ccp4spg\_describe\_epsilon\_zone() :
   `csymlib.h <csymlib_8h.html#a54>`__
-  ccp4spg\_do\_chb() : `csymlib.h <csymlib_8h.html#a43>`__
-  ccp4spg\_free() : `csymlib.h <csymlib_8h.html#a9>`__
-  ccp4spg\_generate\_indices() : `csymlib.h <csymlib_8h.html#a41>`__
-  ccp4spg\_generate\_origins() : `csymlib.h <csymlib_8h.html#a56>`__
-  ccp4spg\_get\_multiplicity() : `csymlib.h <csymlib_8h.html#a51>`__
-  ccp4spg\_is\_centric() : `csymlib.h <csymlib_8h.html#a45>`__
-  ccp4spg\_is\_in\_asu() : `csymlib.h <csymlib_8h.html#a39>`__
-  ccp4spg\_is\_in\_pm\_asu() : `csymlib.h <csymlib_8h.html#a38>`__
-  ccp4spg\_is\_sysabs() : `csymlib.h <csymlib_8h.html#a55>`__
-  ccp4spg\_load\_by\_ccp4\_num() : `csymlib.h <csymlib_8h.html#a2>`__
-  ccp4spg\_load\_by\_ccp4\_spgname() :
   `csymlib.h <csymlib_8h.html#a4>`__
-  ccp4spg\_load\_by\_spgname() : `csymlib.h <csymlib_8h.html#a3>`__
-  ccp4spg\_load\_by\_standard\_num() :
   `csymlib.h <csymlib_8h.html#a1>`__
-  ccp4spg\_load\_laue() : `csymlib.h <csymlib_8h.html#a13>`__
-  ccp4spg\_load\_spacegroup() : `csymlib.h <csymlib_8h.html#a6>`__
-  ccp4spg\_mem\_tidy() : `csymlib.h <csymlib_8h.html#a7>`__
-  ccp4spg\_name\_de\_colon() : `csymlib.h <csymlib_8h.html#a31>`__
-  ccp4spg\_name\_equal() : `csymlib.h <csymlib_8h.html#a28>`__
-  ccp4spg\_name\_equal\_to\_lib() : `csymlib.h <csymlib_8h.html#a29>`__
-  ccp4spg\_norm\_trans() : `csymlib.h <csymlib_8h.html#a33>`__
-  ccp4spg\_pgname\_equal() : `csymlib.h <csymlib_8h.html#a32>`__
-  ccp4spg\_phase\_shift() : `csymlib.h <csymlib_8h.html#a42>`__
-  ccp4spg\_print\_centric\_zones() :
   `csymlib.h <csymlib_8h.html#a48>`__
-  ccp4spg\_print\_epsilon\_zones() :
   `csymlib.h <csymlib_8h.html#a53>`__
-  ccp4spg\_print\_recip\_ops() : `csymlib.h <csymlib_8h.html#a58>`__
-  ccp4spg\_print\_recip\_spgrp() : `csymlib.h <csymlib_8h.html#a57>`__
-  ccp4spg\_put\_in\_asu() : `csymlib.h <csymlib_8h.html#a40>`__
-  ccp4spg\_register\_by\_ccp4\_num() :
   `csymlib.h <csymlib_8h.html#a10>`__
-  ccp4spg\_register\_by\_symops() : `csymlib.h <csymlib_8h.html#a11>`__
-  ccp4spg\_set\_centric\_zones() : `csymlib.h <csymlib_8h.html#a44>`__
-  ccp4spg\_set\_epsilon\_zones() : `csymlib.h <csymlib_8h.html#a50>`__
-  ccp4spg\_symbol\_Hall() : `csymlib.h <csymlib_8h.html#a26>`__
-  ccp4spg\_to\_shortname() : `csymlib.h <csymlib_8h.html#a30>`__
-  ccp4uc\_calc\_cell\_volume() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a6>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a7>`__
-  ccp4uc\_calc\_rcell() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a1>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a2>`__
-  ccp4uc\_cells\_differ() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a7>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a8>`__
-  ccp4uc\_frac\_orth\_mat() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a0>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a1>`__
-  ccp4uc\_frac\_to\_orth() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a3>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a4>`__
-  ccp4uc\_fracu\_to\_orthu() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a5>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a6>`__
-  ccp4uc\_is\_hexagonal() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a9>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a10>`__
-  ccp4uc\_is\_rhombohedral() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a8>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a9>`__
-  ccp4uc\_orth\_to\_frac() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a2>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a3>`__
-  ccp4uc\_orthu\_to\_fracu() :
   `ccp4\_unitcell.h <ccp4__unitcell_8h.html#a4>`__,
   `ccp4\_unitcell.c <ccp4__unitcell_8c.html#a5>`__
-  ccp4VerbosityLevel() :
   `ccp4\_program.h <ccp4__program_8h.html#a11>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a5>`__

- d -
~~~~~

-  DEFMODE : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a4>`__
-  DFNT\_CHAR : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a34>`__
-  DFNT\_DOUBLE : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a36>`__
-  DFNT\_FLOAT : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a35>`__
-  DFNT\_INT : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a32>`__
-  DFNT\_SINT : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a31>`__
-  DFNT\_UCHAR : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a33>`__
-  DFNT\_UINT : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a30>`__
-  DFNTF\_BEIEEE : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a26>`__
-  DFNTF\_CONVEXNATIVE : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a28>`__
-  DFNTF\_LEIEEE : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a29>`__
-  DFNTF\_VAX : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a27>`__
-  DFNTI\_IBO : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a25>`__
-  DFNTI\_MBO : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a24>`__

- f -
~~~~~

-  FORTRAN\_CALL : `ccp4\_fortran.h <ccp4__fortran_8h.html#a8>`__
-  FORTRAN\_FUN : `csymlib\_f.c <csymlib__f_8c.html#a32>`__,
   `cmaplib\_f.c <cmaplib__f_8c.html#a46>`__,
   `ccp4\_fortran.h <ccp4__fortran_8h.html#a9>`__
-  FORTRAN\_SUBR : `csymlib\_f.c <csymlib__f_8c.html#a44>`__,
   `cmtzlib\_f.c <cmtzlib__f_8c.html#a85>`__,
   `cmaplib\_f.c <cmaplib__f_8c.html#a53>`__,
   `ccp4\_general\_f.c <ccp4__general__f_8c.html#a19>`__,
   `ccp4\_diskio\_f.c <ccp4__diskio__f_8c.html#a23>`__,
   `ccp4\_fortran.h <ccp4__fortran_8h.html#a7>`__

- g -
~~~~~

-  get\_grid\_sample() : `csymlib.h <csymlib_8h.html#a62>`__

- h -
~~~~~

-  html\_log\_output() :
   `ccp4\_program.h <ccp4__program_8h.html#a17>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a11>`__

- i -
~~~~~

-  init\_cmap\_read() : `cmap\_open.c <cmap__open_8c.html#a0>`__
-  init\_cmap\_write() : `cmap\_open.c <cmap__open_8c.html#a1>`__
-  is\_cmap() : `cmap\_open.c <cmap__open_8c.html#a2>`__

- m -
~~~~~

-  mat4\_to\_recip\_symop() :
   `ccp4\_parser.h <ccp4__parser_8h.html#a25>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a35>`__
-  mat4\_to\_symop() : `ccp4\_parser.h <ccp4__parser_8h.html#a24>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a34>`__
-  MAXFILES : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a3>`__
-  MAXFLEN : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a2>`__
-  MAXSPGNAMELENGTH : `mtzdata.h <mtzdata_8h.html#a5>`__
-  MCOLUMNS : `mtzdata.h <mtzdata_8h.html#a11>`__
-  MSETS : `mtzdata.h <mtzdata_8h.html#a10>`__
-  MTZ\_MAJOR\_VERSN : `mtzdata.h <mtzdata_8h.html#a1>`__
-  MTZ\_MINOR\_VERSN : `mtzdata.h <mtzdata_8h.html#a2>`__
-  MtzAddColumn() : `cmtzlib.h <cmtzlib_8h.html#a45>`__
-  MtzAddDataset() : `cmtzlib.h <cmtzlib_8h.html#a38>`__
-  MtzAddHistory() : `cmtzlib.h <cmtzlib_8h.html#a86>`__
-  MtzAddXtal() : `cmtzlib.h <cmtzlib_8h.html#a28>`__
-  MtzArrayToBatch() : `cmtzlib.h <cmtzlib_8h.html#a64>`__
-  MtzAssignColumn() : `cmtzlib.h <cmtzlib_8h.html#a47>`__
-  MtzAssignHKLtoBase() : `cmtzlib.h <cmtzlib_8h.html#a46>`__
-  MTZBAT : `mtzdata.h <mtzdata_8h.html#a12>`__
-  MtzBatchToArray() : `cmtzlib.h <cmtzlib_8h.html#a65>`__
-  MtzCallocHist() : `cmtzlib.h <cmtzlib_8h.html#a15>`__
-  MtzColLookup() : `cmtzlib.h <cmtzlib_8h.html#a56>`__
-  MtzColPath() : `cmtzlib.h <cmtzlib_8h.html#a53>`__
-  MtzColSet() : `cmtzlib.h <cmtzlib_8h.html#a49>`__
-  MtzColsInSet() : `cmtzlib.h <cmtzlib_8h.html#a43>`__
-  MtzColType() : `cmtzlib.h <cmtzlib_8h.html#a57>`__
-  MtzDebugHierarchy() : `cmtzlib.h <cmtzlib_8h.html#a58>`__
-  MtzDeleteRefl() : `cmtzlib.h <cmtzlib_8h.html#a8>`__
-  MtzFindInd() : `cmtzlib.h <cmtzlib_8h.html#a61>`__
-  MtzFree() : `cmtzlib.h <cmtzlib_8h.html#a10>`__
-  MtzFreeBatch() : `cmtzlib.h <cmtzlib_8h.html#a14>`__
-  MtzFreeCol() : `cmtzlib.h <cmtzlib_8h.html#a12>`__
-  MtzFreeHist() : `cmtzlib.h <cmtzlib_8h.html#a16>`__
-  MtzGet() : `cmtzlib.h <cmtzlib_8h.html#a1>`__
-  MtzGetUserCellTolerance() : `cmtzlib.h <cmtzlib_8h.html#a2>`__
-  MtzHklcoeffs() : `cmtzlib.h <cmtzlib_8h.html#a63>`__
-  MtzIcolInSet() : `cmtzlib.h <cmtzlib_8h.html#a44>`__
-  MtzInd2reso() : `cmtzlib.h <cmtzlib_8h.html#a62>`__
-  MtzIsetInXtal() : `cmtzlib.h <cmtzlib_8h.html#a32>`__
-  MtzIxtal() : `cmtzlib.h <cmtzlib_8h.html#a25>`__
-  MtzListColumn() : `cmtzlib.h <cmtzlib_8h.html#a59>`__
-  MtzListInputColumn() : `cmtzlib.h <cmtzlib_8h.html#a60>`__
-  MtzMalloc() : `cmtzlib.h <cmtzlib_8h.html#a9>`__
-  MtzMallocBatch() : `cmtzlib.h <cmtzlib_8h.html#a13>`__
-  MtzMallocCol() : `cmtzlib.h <cmtzlib_8h.html#a11>`__
-  MtzMemTidy() : `cmtzlib.h <cmtzlib_8h.html#a17>`__
-  MtzNbat() : `cmtzlib.h <cmtzlib_8h.html#a18>`__
-  MtzNbatchesInSet() : `cmtzlib.h <cmtzlib_8h.html#a42>`__
-  MtzNcol() : `cmtzlib.h <cmtzlib_8h.html#a50>`__
-  MtzNcolsInSet() : `cmtzlib.h <cmtzlib_8h.html#a39>`__
-  MtzNref() : `cmtzlib.h <cmtzlib_8h.html#a19>`__
-  MtzNset() : `cmtzlib.h <cmtzlib_8h.html#a33>`__
-  MtzNsetsInXtal() : `cmtzlib.h <cmtzlib_8h.html#a29>`__
-  MtzNumActiveCol() : `cmtzlib.h <cmtzlib_8h.html#a51>`__
-  MtzNumActiveColsInSet() : `cmtzlib.h <cmtzlib_8h.html#a40>`__
-  MtzNumActiveSet() : `cmtzlib.h <cmtzlib_8h.html#a34>`__
-  MtzNumActiveSetsInXtal() : `cmtzlib.h <cmtzlib_8h.html#a30>`__
-  MtzNumActiveXtal() : `cmtzlib.h <cmtzlib_8h.html#a23>`__
-  MtzNumSourceCol() : `cmtzlib.h <cmtzlib_8h.html#a52>`__
-  MtzNumSourceColsInSet() : `cmtzlib.h <cmtzlib_8h.html#a41>`__
-  MtzNxtal() : `cmtzlib.h <cmtzlib_8h.html#a22>`__
-  MtzOpenForWrite() : `cmtzlib.h <cmtzlib_8h.html#a5>`__
-  MtzParseLabin() : `cmtzlib.h <cmtzlib_8h.html#a74>`__
-  MtzPathMatch() : `cmtzlib.h <cmtzlib_8h.html#a55>`__
-  MtzPrintBatchHeader() : `cmtzlib.h <cmtzlib_8h.html#a83>`__
-  MtzPut() : `cmtzlib.h <cmtzlib_8h.html#a4>`__
-  MTZRECORDLENGTH : `mtzdata.h <mtzdata_8h.html#a4>`__
-  MtzResLimits() : `cmtzlib.h <cmtzlib_8h.html#a21>`__
-  MtzRJustPath() : `cmtzlib.h <cmtzlib_8h.html#a54>`__
-  MtzRrefl() : `cmtzlib.h <cmtzlib_8h.html#a3>`__
-  MtzSetLookup() : `cmtzlib.h <cmtzlib_8h.html#a37>`__
-  MtzSetPath() : `cmtzlib.h <cmtzlib_8h.html#a36>`__
-  MtzSetsInXtal() : `cmtzlib.h <cmtzlib_8h.html#a31>`__
-  MtzSetSortOrder() : `cmtzlib.h <cmtzlib_8h.html#a85>`__
-  MtzSetXtal() : `cmtzlib.h <cmtzlib_8h.html#a35>`__
-  MtzSpacegroupNumber() : `cmtzlib.h <cmtzlib_8h.html#a20>`__
-  MtzToggleColumn() : `cmtzlib.h <cmtzlib_8h.html#a48>`__
-  MTZVERSN : `mtzdata.h <mtzdata_8h.html#a0>`__
-  MtzWhdrLine() : `cmtzlib.h <cmtzlib_8h.html#a6>`__
-  MtzWrefl() : `cmtzlib.h <cmtzlib_8h.html#a7>`__
-  MtzXtalLookup() : `cmtzlib.h <cmtzlib_8h.html#a27>`__
-  MtzXtalPath() : `cmtzlib.h <cmtzlib_8h.html#a26>`__
-  MtzXtals() : `cmtzlib.h <cmtzlib_8h.html#a24>`__
-  MXTALS : `mtzdata.h <mtzdata_8h.html#a9>`__

- n -
~~~~~

-  NBATCHINTEGERS : `mtzdata.h <mtzdata_8h.html#a7>`__
-  NBATCHREALS : `mtzdata.h <mtzdata_8h.html#a8>`__
-  NBATCHWORDS : `mtzdata.h <mtzdata_8h.html#a6>`__

- o -
~~~~~

-  O\_APPEND : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a14>`__
-  O\_CREAT : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a15>`__
-  O\_RDONLY : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a11>`__
-  O\_RDWR : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a13>`__
-  O\_TMP : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a17>`__
-  O\_TRUNC : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a16>`__
-  O\_WRONLY : `ccp4\_sysdep.h <ccp4__sysdep_8h.html#a12>`__

- r -
~~~~~

-  range\_to\_limits() : `csymlib.h <csymlib_8h.html#a59>`__

- s -
~~~~~

-  set\_fft\_grid() : `csymlib.h <csymlib_8h.html#a60>`__
-  SIZE1 : `mtzdata.h <mtzdata_8h.html#a3>`__
-  sort\_batches() : `cmtzlib.h <cmtzlib_8h.html#a66>`__
-  strtolower() : `ccp4\_parser.h <ccp4__parser_8h.html#a14>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a24>`__
-  strtoupper() : `ccp4\_parser.h <ccp4__parser_8h.html#a13>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a23>`__
-  summary\_output() : `ccp4\_program.h <ccp4__program_8h.html#a18>`__,
   `ccp4\_program.c <ccp4__program_8c.html#a12>`__
-  symfr\_driver() : `csymlib.h <csymlib_8h.html#a8>`__
-  symop\_to\_mat4() : `ccp4\_parser.h <ccp4__parser_8h.html#a19>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a29>`__
-  symop\_to\_rotandtrn() :
   `ccp4\_parser.h <ccp4__parser_8h.html#a18>`__,
   `ccp4\_parser.c <ccp4__parser_8c.html#a28>`__
