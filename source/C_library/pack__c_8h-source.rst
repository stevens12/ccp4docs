`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

pack\_c.h
=========

.. raw:: html

   <div class="fragment">

::

    00001 /* Some general defines: */
    00002 
    00003 
    00004 #define PACKIDENTIFIER "\nCCP4 packed image, X: %04d, Y: %04d\n"
    00005 /* This string defines the start of a packed image. An image file is scanned
    00006    until this string is encountered, the size of the unpacked image is 
    00007    determined from the values of X and Y (which are written out as formatted
    00008    ascii numbers), and the packed image is expected to start immediately after
    00009    the null-character ending the string. */
    00010 
    00011 #define V2IDENTIFIER "\nCCP4 packed image V2, X: %04d, Y: %04d\n"
    00012 /* This string defines the start of a packed image. An image file is scanned
    00013    until this string is encountered, the size of the unpacked image is 
    00014    determined from the values of X and Y (which are written out as formatted
    00015    ascii numbers), and the packed image is expected to start immediately after
    00016    the null-character ending the string. */
    00017 
    00018 #define PACKBUFSIZ BUFSIZ
    00019 /* Size of internal buffer in which the packed array is stored during transit
    00020    form an unpacked image to a packed image on disk. It is set to the size
    00021    used by the buffered io-routines given in <stdio.h>, but it could be 
    00022    anything. */
    00023 
    00024 #define DIFFBUFSIZ 16384L
    00025 /* Size of the internal buffer in which the differences between neighbouring 
    00026    pixels are stored prior to compression. The image is therefore compressed 
    00027    in DIFFBUFSIZ chunks. Decompression does not need to know what DIFFBUFSIZ
    00028    was when the image was compressed. By increasing this value, the image
    00029    can be compressed into a packed image which is a few bytes smaller. Do
    00030    not decrease the value of DIFFBUFSIZ below 128L. */
    00031  
    00032 #define BYTE char
    00033 /* BYTE is a one byte integer. */
    00034 
    00035 #define WORD short int
    00036 /* WORD is a two-byte integer. */
    00037 
    00038 #define LONG int
    00039 /* LONG is a four byte integer. */
    00040 /* Dave Love 5/7/94: using `int' gets you 4 bytes on the 32-bit Unix
    00041    (and VAX) systems I know of and also on (64-bit) OSF/1 Alphas which
    00042    have 64-bit longs.  (This definition previously used `long'.) */
    00043 
    00044 
    00045 
    00046 /******************************************************************************/
    00047 
    00048 /* Some usefull macros used in the code of this sourcefile: */
    00049 
    00050 
    00051 #define max(x, y) (((x) > (y)) ? (x) : (y)) 
    00052 /* Returns maximum of x and y. */
    00053 
    00054 #define min(x, y) (((x) < (y)) ? (x) : (y)) 
    00055 /* Returns minimum of x and y. */
    00056 
    00057 #undef abs                      /* avoid complaint from DEC C, at least */
    00058 #define abs(x) (((x) < 0) ? (-(x)) : (x))
    00059 /* Returns the absolute value of x. */
    00060 
    00061 /* Used to be 'static const LONG' but const declaration gives trouble on HPs */
    00062 #ifndef SKIP_SETBITS
    00063 static LONG setbits[33] =
    00064                          {0x00000000L, 0x00000001L, 0x00000003L, 0x00000007L,
    00065                           0x0000000FL, 0x0000001FL, 0x0000003FL, 0x0000007FL,
    00066                           0x000000FFL, 0x000001FFL, 0x000003FFL, 0x000007FFL,
    00067                           0x00000FFFL, 0x00001FFFL, 0x00003FFFL, 0x00007FFFL,
    00068                           0x0000FFFFL, 0x0001FFFFL, 0x0003FFFFL, 0x0007FFFFL,
    00069                           0x000FFFFFL, 0x001FFFFFL, 0x003FFFFFL, 0x007FFFFFL,
    00070                           0x00FFFFFFL, 0x01FFFFFFL, 0x03FFFFFFL, 0x07FFFFFFL,
    00071                           0x0FFFFFFFL, 0x1FFFFFFFL, 0x3FFFFFFFL, 0x7FFFFFFFL,
    00072                           0xFFFFFFFFL};
    00073 /* This is not a macro really, but I've included it here anyway. Upon indexing,
    00074    it returns a LONG with the lower (index) number of bits set. It is equivalent
    00075    to the following macro:
    00076      #define setbits(n) (((n) == 32) : ((1L << (n)) - 1) : (-1L)) 
    00077    Indexing the const array should usually be slightly faster. */
    00078 #endif
    00079 
    00080 #define shift_left(x, n)  (((x) & setbits[32 - (n)]) << (n))
    00081 /* This macro is included because the C standard does not properly define a 
    00082    left shift: on some machines the bits which are pushed out at the left are
    00083    popped back in at the right. By masking, the macro prevents this behaviour.
    00084    If you are sure that your machine does not pops bits back in, you can speed
    00085    up the code insignificantly by taking out the masking. */
    00086 
    00087 #define shift_right(x, n) (((x) >> (n)) & setbits[32 - (n)])
    00088 /* See comment on left shift. */
    00089 
    00090 
    00091 
    00092 /******************************************************************************/
    00093 
    00094 
    00095 
    00096 
    00097 /* Functions required for packing: */
    00098 
    00099 #if defined (PROTOTYPE)
    00100 void v2pack_wordimage_c(WORD *img, int x, int y, char *filename);
    00101 /* Pack image 'img', containing 'x * y' WORD-sized pixels into 'filename'.
    00102    This function generates Version 2 images! */
    00103 
    00104 void v2pack_longimage_c(LONG *img, int x, int y, char *filename);
    00105 /* Pack image 'img', containing 'x * y' LONG-sized pixels into 'filename'. 
    00106    This function generates Version 2 images! */
    00107 
    00108 
    00109 /* Functions required for unpacking: */
    00110 
    00111 
    00112 void readpack_word_c(WORD *img, char *filename);
    00113 /* Unpacks packed image from 'filename' into the WORD-array 'img'. Scans the
    00114    file defined by 'filename' until the PACKIDENTIFIER is found, then unpacks
    00115    starting from there. */
    00116 
    00117 void readpack_long_c(LONG *img, char *filename);
    00118 /* Unpacks packed image from 'filename' into the LONG-array 'img'. Scans the
    00119    file defined by 'filename' until the PACKIDENTIFIER is found, then unpacks
    00120    starting from there. */
    00121 
    00122 void imsiz_c(char *filename, LONG *x, LONG *y);
    00123 /* Determines the size of the the packed image "filename" after unpacking. The
    00124    dimensions are returned in x and y. */
    00125 
    00126 #endif  /* (PROTOTYPE) */
    00127 

.. raw:: html

   </div>
