p842asc (CCP4: Unsupported Program)
===================================

NAME
----

**p842asc, asc2p84** - Read and write plot84 and ascii files

SYNOPSIS
--------

| **p842asc plotfile** *infile[.plt]* **datout** *outfile[.out]*
| **asc2p84 datin** *infile[.dat]* **plotfile** *outfile[.p84]*

DESCRIPTION
-----------

The programs **asc2p84** and **p842asc** are used to transport plot84
metafiles between machines with different binary data formats via an
ASCII format.

SEE ALSO
--------

`pltdev <pltdev.html>`__

AUTHOR
------

P. J. Daly
