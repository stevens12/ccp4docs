|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Start and run <qtpisa-start.html>`__ •
Configuration File

.. raw:: html

   </div>

Configuration File is essential for `running PISA in command-prompt
(non-graphical) mode <qtpisa-cmdprompt.html>`__, and may be also used
for the `configuration of QtPISA <qtpisa-configuration.html>`__. QtPISA
may be configured by either loading the configuration file in the
Configuration Settings dialog, or by running the following command in
terminal:

.. code:: code-box

    qtpisa /path/to/pisa.cfg

where pisa.cfg is the configuration file with mandatory extension
\*.cfg. The above configuration command does not launch PISA for
calculations, and only rewrites configuration parameters into local
user's settings.

Configuration File is a plain-text file with simple keyword-parameter
syntax:

-  both keywords and parameters occupy exactly one line each
-  keywords are case-sensitive
-  lines starting with hash '#' are treated as remarks

For convenience, template Configuration File may be obtained from either
PISA or QtPISA using one of the following commands:

.. code:: code-box

    qtpisa -cfg-template > pisa.cfg
    pisa   -cfg-template > pisa.cfg

Below is a sample of Configuration File, obtained in such way, which is
produced with all explanatory remarks:

.. code:: code-box

    # ------------------------------------------------------------------- #
    #                                                                     #
    #          This is configuratrion file for PISA software.             #
    #                                                                     #
    #   When used in command-prompt mode, this file must be specified     #
    #       as last argument in thecommand line, or pointed out by        #
    #              PISA_CONF_FILE environmental variable.                 #
    #                                                                     #
    #    This file may be also used to configure the QtPISA graphical     #
    #   application, by either reading it using the "Load CFG" button     #
    #  in PISA Settings Dialog, or by running QtPISA from command prompt  #
    #    with this file as the only command-line argument. QtPISA needs   #
    #           to be configure only once after installation.             #
    #                                                                     #
    # ------------------------------------------------------------------- #


    #  DATA_ROOT must point on the directory to contain session
    #  directories.
    DATA_ROOT
    /tmp/Eugene/


    #  SRS_DIR must point on the directory containing SRS files.
    SRS_DIR
    /Applications/ccp4-6.4.0/share/ccp4srs/


    #  MOLREF_DIR must point on the directory containing MolRef files.
    MOLREF_DIR
    /Applications/ccp4-6.4.0/share/pisa/


    #  PISTORE_DIR must point on the directory containing files:
    #          agents.dat
    #          asm_params.dat
    #          intfstats.dat
    #          syminfo_pisa.lib and
    #          rcsb_symops.dat
    PISTORE_DIR
    /Applications/ccp4-6.4.0/share/pisa/


    #  HELP_DIR must point on the directory containing HTML-formatted
    #  help files.
    HELP_DIR
    /Applications/ccp4-6.4.0/html/pisa


    #  RASMOL_COM must give the rasmol launch command line
    RASMOL_COM
    /sw/bin/rasmol


    #  JMOL_COM must give path to jmol.jar
    JMOL_COM
    /sw/Jmol/Jmol.jar


    #  CCP4MG_COM must give the ccp4mg launch command line
    CCP4MG_COM
    /Applications/QtMG.app


    #  SESSION_PREFIX is prefix for the names of session directories
    #  created in DATA_PATH ("pisrv_" is used by default). Be sure to
    #  have unique prefixes for each configuration file that is invoked
    #  from a different user login or apache service. Session directories
    #  are regularly removed from DATA_PATH, and SESSION_PREFIX allows
    #  one to avoid permission conflicts between different services.
    SESSION_PREFIX
    __tmp_pisa_

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
