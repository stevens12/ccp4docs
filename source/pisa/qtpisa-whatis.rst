|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • What is PISA

.. raw:: html

   </div>

PISA is a software tool for the examination of macromolecular crystals,
interfaces and assemblies. The most common use of PISA is the
identification of structure and oligomeric state of macromolecular
complexes.

Other tasks, which may be done or helped by using PISA, include

-  Calculation of surface area of macromolecules
-  Calculation of interface surface area
-  Calculation of weak chemical interactions: hydrogen bonds, salt
   bridges, disulphide bonds
-  Assessment of protein folding energy
-  Assessment of hydrophobic interactions
-  Analysis of macromolecular surfaces on residue level
   (interface/surface area and interactions)
-  Assessment of crystal packing quality
-  Concentration analysis of oligomeric states
-  Analysis of alternative oligomeric states

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
