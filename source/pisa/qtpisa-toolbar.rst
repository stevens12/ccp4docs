|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Control panel <qtpisa-controlpanel.html>`__
• Toolbar

.. raw:: html

   </div>

The Toolbar is found on top of `Control
Panel <qtpisa-controlpanel.html>`__ and looks like below:

|image2|

Toolbar buttons may be disabled (grayed) if the corresponding action is
not available. The corresponding actions are as follows:

+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Button     | Action                                                                                                                                                                                                                                                                                                                                                                                                    |
+============+===========================================================================================================================================================================================================================================================================================================================================================================================================+
| |image3|   | Opens an input file. The input file may be either a coordinate file in `PDB <qtpisa-glossary.html#pdb-format>`__ or `mmCIF <qtpisa-glossary.html#mmcif-format>`__\  format, or `\*.pisa <qtpisa-glossary.html#pisa-result-file>`__\  file, containing calculation results. Clicking on arrow from right side of the button in the Toolbar will show a list of recently used files for faster selection.   |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image4|   | Opens the download dialog, which asks user for a 4-character `PDB code <qtpisa-glossary.html#pdb-code>`__. After hitting the Download button in the dialog, PISA downloads the corresponding file from the PDB web site, and uses it as if it were read from local file system.                                                                                                                           |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image5|   | Saves results of PISA calculations in `PISA result files (\*.pisa) <qtpisa-glossary.html#pisa-result-file>`__\ . These files can be read later in order to display calculation results without repeating the calculations.                                                                                                                                                                                |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image6|   | Launches a molecular graphics viewer to visualise structure highlighted in PISA result pages. The viewer is configured in PISA `settings <qtpisa-configuration.html>`__.                                                                                                                                                                                                                                  |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image7|   | Opens PISA settings dialog, used for the specifying the location of PISA data files, as well as the molecular viewer application for displaying structures.                                                                                                                                                                                                                                               |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image8|   | Opens help page specific to the content of PISA result page displayed in `working area <qtpisa-glossary.html#working-area>`__.                                                                                                                                                                                                                                                                            |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image9|   | Exits PISA.                                                                                                                                                                                                                                                                                                                                                                                               |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
.. |image2| image:: toolbar.png
.. |image3| image:: file_open.png
   :width: 32px
   :height: 32px
.. |image4| image:: pdb_open.png
   :width: 32px
   :height: 32px
.. |image5| image:: file_save.png
   :width: 32px
   :height: 32px
.. |image6| image:: view_structure.png
   :width: 27px
   :height: 27px
.. |image7| image:: configure.png
   :width: 32px
   :height: 32px
.. |image8| image:: help.png
   :width: 32px
   :height: 32px
.. |image9| image:: exit.png
   :width: 32px
   :height: 32px
