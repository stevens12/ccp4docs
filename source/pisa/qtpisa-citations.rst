|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • Citations

.. raw:: html

   </div>

Please cite PISA for all results obtained from using it, should these
results be published in any form or otherwise presented. The following
publication should be mentioned:

-  E. Krissinel and K. Henrick (2007) *Inference of macromolecular
   assemblies from crystalline state.* J. Mol. Biol. **372**, 774-797.

The following study contains analysis of PISA results and limits of
their applicability. It should be consulted if free energy estimates
from PISA are used for making scientific conclusions:

-  E. Krissinel (2010) *Crystal contacts as nature's docking solutions.*
   J. Comput. Chem. **31(1)**, 133-143; DOI 10.1002/jcc.21303

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
