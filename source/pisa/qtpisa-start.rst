|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • Start and run

.. raw:: html

   </div>

Like many other programs, on start-up, PISA performs a set of
initialisations, which include reading and checking for consistency some
30Mb of various data. Therefore, a few-second delay is possible on slow
systems.

If initialisation fails in any part, this is due to either corrupt data
files or wrong configuration settings. Configuration settings are
accessible through View/Settings menu item (qtpisa/Preferences on Mac
OSX), or by clicking on the Settings button in the
`Toolbar <qtpisa-toolbar.html>`__, or pressing Ctrl-G (⌘-G on Mac OSX)
hot key. Consult `configuration <qtpisa-configuration.html>`__ pages for
further details.

After successful initialisation is successful, QtPISA is ready for
user's input, which is indicated by the welcome message in the `working
area <qtpisa-glossary.html#working-area>`__ of the window. The input may
be:

#. A coordinate file in `PDB <qtpisa-glossary.html#pdb-format>`__ or
   `mmCIF <qtpisa-glossary.html#mmcif-format>`__\  format. Select
   File/Open file menu item, or push the Open button in the
   `Toolbar <qtpisa-toolbar.html>`__, or press
   `Ctrl-O <qtpisa-hotkeys.html>`__ and navigate to the file in your
   file system, or drag-and-drop from your file manager (Windows
   Explorer (MS Windows), Nautilus (Linux), Finder (Mac OSX)). The file
   may be also specified as a command-line argument, when starting
   QtPISA in a termoinal window.
#. QtPISA Result File \*.pisa, containing results from previous
   calculations. The file is open in the same way as above.
#. A `PDB Entry Code <qtpisa-glossary.html#pdb-code>`__. Select
   File/Open PDB online menu item, or push the Download button in the
   `Toolbar <qtpisa-toolbar.html>`__, or press
   `Ctrl-L <qtpisa-hotkeys.html>`__, then set up PDB code in the pop-up
   dialog box, and press Download button in the dialog. The
   corresponding PDB file will be downloaded from the `Protein Data
   Bank <http://www.pdb.org>`__ and used as if it were read from local
   file system.

Once an input is done, calculations start automatically. The calculation
time may vary significantly, depending on the structure. The calculation
progress is displayed in the progress bar, found in the bottom of the
`Control Panel <qtpisa-controlpanel.html>`__. During the calculations,
QtPISA interface may be used to inspect results calculated to the
moment.

| In addition to the coordinate and crystal data (both are contained in
  the coordinate files and PDB entries), PISA may require explicit
  specification of ligands that should be excluded from the analysis.
  This is done from the `Data page <qtpisa-datapage.html>`__, which is
  displayed immediately after the input of coordinated data, please see
  further details `here <qtpisa-datapage.html>`__. After selecting the
  needed ligands, PISA calculations must be re-started. This, too, is
  done from the `Data page <qtpisa-datapage.html>`__, and can be
  performed without waiting for current calculations to finish.
|  

--------------

| ***See also***

-  `Configuration <qtpisa-configuration.html>`__
-  `Configuration file <qtpisa-cfgfile.html>`__

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
