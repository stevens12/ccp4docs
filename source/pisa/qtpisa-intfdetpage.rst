|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Result Pages <qtpisa-resultpages.html>`__ •
`List of Interfaces <qtpisa-intflistpage.html>`__ • Interface Details &
Radar

.. raw:: html

   </div>

Interface Details give extended summary of the
`interface <qtpisa-glossary.html#interface>`__, selected in the `Result
Tree <qtpisa-glossary.html#pisa-result-tree>`__. The following data is
displayed for the two interfacing
`monomers <qtpisa-glossary.html#monomeric-unit>`__:

+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ID                   | `Monomer ID <qtpisa-glossary.html#monomer-id>`__                                                                                                                                                                                                                                                                                           |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Class                | Type of `monomer <qtpisa-glossary.html#monomeric-unit>`__: Protein, RNA, DNA or Ligand.                                                                                                                                                                                                                                                    |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Symmetry operation   | Symmetry operation (in fractional space), applied to `monomers <qtpisa-glossary.html#monomeric-unit>`__. 1st interfacing monomer is always in the original position, corrsponding to the idntity operation X,Y,Z.                                                                                                                          |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Symmetry ID          | Symmetry ID, corresponding to symmetry operation in the previous row. The ID has form N\_KLM, where N is serial number of the symmetry operation in the corresponding space symmetry group (as used by the `PDB <http://www.pdb.org>`__); KLM give fractional coordinates of the unit cell relative to the original cell, placed at 555.   |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Interface atoms      | Total number of atoms in the interface area for each monomer, with the percentage of the total number of atoms in brackets.                                                                                                                                                                                                                |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Surface atoms        | Total number of surface (solvent-accessible) atoms for each monomer, with the percentage of the total number of atoms in brackets.                                                                                                                                                                                                         |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Total atoms          | Total number of atoms for each monomer (100%).                                                                                                                                                                                                                                                                                             |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Interface residues   | Total number of residues in the interface area for each monomer, with the percentage of the total number of residues in brackets.                                                                                                                                                                                                          |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Surface residues     | Total number of surface (solvent-accessible) residues for each monomer, with the percentage of the total number of residues in brackets.                                                                                                                                                                                                   |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Total residues       | Total number of residues for each monomer (100%).                                                                                                                                                                                                                                                                                          |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Buried ASA           | Surface area, `buried <qtpisa-glossary.html#bsa>`__ in the interface for each monomer (in square angstroms), with the percentage of the total `surface area <qtpisa-glossary.html#surface-area>`__ in brackets.                                                                                                                            |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Total ASA            | `Surface area <qtpisa-glossary.html#surface-area>`__ of each monomer (in square angstroms).                                                                                                                                                                                                                                                |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Solvation energy     | `Solvation energy of folding <qtpisa-glossary.html#se-folding>`__ for each monomer (not defined for ligands), in kcal/mol.                                                                                                                                                                                                                 |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SE gain              | `Solvation energy <qtpisa-glossary.html#se-folding>`__ gain, in kcal/mol, for each monomer. This reflects the change in monomer's solvation energy, when part of its surface loses contact with solvent due to the formation of the interface.                                                                                             |
+----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

| 
|  

Interaction Radar gives a quick grasp of interface interaction value.
This allows a user to spot interfaces that may have biological
significance. The tool estimates the likelihood of the interface to be a
part of biological assembly by comparing key interface properties to
statistical distributions derived from the current content of the
`PDB <http://www.pdb.org>`__. In general, the larger the are inside
radar contour, the higher chances that the interface is a part of
biological assembly, rather than an artefact of crystal packing.

The radar is based on 7 interface parameters. For each interface, these
parameters and their values are shown in table on the right from the
radar plot:

+------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| IA: Interface Area     | `Interface Area <qtpisa-glossary.html#interface-area>`__ in square angstroms                                                                                                                        |
+------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DG: Delta G            | `Solvation energy <qtpisa-glossary.html#solvation-energy>`__ gain upon interface formation, in kcal/mol                                                                                             |
+------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| BE: Binding Energy     | Total Binding Energy of the interface, defined as `solvation energy <qtpisa-glossary.html#solvation-energy>`__ gain plus effect of hydrogen bonds, salt bridges and disulphide bonds, in kcal/mol   |
+------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| PV: P-value            | `Hydrophobic P-value <qtpisa-glossary.html#hp-value>`__ of the interface. The lower P-value, the more specific, or statistically surprising, the interface.                                         |
+------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| HB: Hydrogen bonds     | Number of hydrogen bonds formed between the interfacing monomers.                                                                                                                                   |
+------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SB: Salt bridges       | Number of salt bridges formed between the interfacing monomers.                                                                                                                                     |
+------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DS: Disulphide bonds   | Number of disulphide bonds formed between the interfacing monomers.                                                                                                                                 |
+------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

| 
| Radar beams (as indicated by parameter names, e.g. IA for Interface
  Area), represent probabilities that a particular value indicates a
  biological assemblage in the PDB, with zero probabilities placed at
  the beams' origin (radar center). The representation and probabilities
  are slightly different in Simple and Detail radar modes. These modes
  are swtiched in the `Configuration Settings
  Dialog <qtpisa-configuration.html#cfg-dialog>`__.

In Simple mode, the radar displays all 7 parameters, and the
corresponding probabilities are calculated per interface type, for
example, Protein-Protein, Protein-Ligand etc.

In Detail mode, the radar displays probabilities calculated for the
Reference parameter, chosen in the displayed combobox:

|image2|

| The parameter chosen may be one of radar's parameters. Then, radar
  probabilities of other parameters are calculated over the subset of
  interfaces where reference parameter has value as in the given
  interface (within a margin). Choosing Interface Type for reference
  parameter will reproduce radar in Simple mode; Choosing Unreferenced,
  untyped will display probabilities calculated in disregard to the
  interface type.
|  

--------------

| ***See further***

-  `Chemical bonds <qtpisa-intfbondspage.html>`__
-  `Interfacing residues <qtpisa-intfrespage.html>`__

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
.. |image2| image:: ref_parameter.png

