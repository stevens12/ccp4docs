|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Result Pages <qtpisa-resultpages.html>`__ •
Input Data Page

.. raw:: html

   </div>

Input Data Page displays the summary of input data and provides controls
for re-running PISA calculations with modified parameters, which may be
needed in rare circumstances.

1. Input Data Summary
~~~~~~~~~~~~~~~~~~~~~

***1.a) General data***

+---------------+-----------------------------------------------------------------------------------------------------------------------------------+
| File          | Full path to file in local file system, or "PDB=>1xyz" if coordinate file was downloaded from the `PDB <http://www.pdb.org>`__.   |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------+
| Title         | Structure title as read from the input file                                                                                       |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------+
| Space group   | Space symmetry group                                                                                                              |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------+
| Resolution    | Structure resolution, in angstroms                                                                                                |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------+
| Cell          | `Unit cell <http://en.wikipedia.org/wiki/Crystal_structure>`__ parameters: α, β, γ, a, b, c                                       |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------+
| Cell volume   | Volume of unit cell in cubic angstroms                                                                                            |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------+

|  
| ***1.b) Contents of Asymmetric Unit***
| **Note:** in this part, the numbers are presented as N(M), where N,M
  relate to the contents of asymmetric unit (ASU) and input file,
  respectively. N and M differ in presence of non-crystallographic
  symmetry.

+------------------+------------------------------------------------------------------------------------------------------------------------------+
| Protein chains   | Total number of protein chains in asymmetric unit                                                                            |
+------------------+------------------------------------------------------------------------------------------------------------------------------+
| DNA/RNA chains   | Total number of nucleic acid chains in asymmetric unit                                                                       |
+------------------+------------------------------------------------------------------------------------------------------------------------------+
| Ligands          | Total number of ligands in asymmetric unit                                                                                   |
+------------------+------------------------------------------------------------------------------------------------------------------------------+
| NCS-mates        | Total number of `monomeric units <qtpisa-glossary.html#monomeric-unit>`__ in ASU, related by non-crystallographic symmetry   |
+------------------+------------------------------------------------------------------------------------------------------------------------------+

|  

2. Modification of calculation parameters and re-run controls
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculation parameters and re-run controls are located in a special
panel fount in the bottom of the Input Data Page. By default, the panle
looks like the following:

|image2|

The above means that ligands SO4 and GOL are excluded from PISA assembly
analysis, and ligands are processed in automatic mode.

In most instances, no modification of default actions are required.
QtPISA automatically excludes cofactors (small molecules, ligands) that
are commonly used to aid crystallisation. Such substances provide
additional (unnatural) binding between `monomeric
units <qtpisa-glossary.html#monomeric-unit>`__ in crystal, and for this
they may cause wrong conclusions on `oligomeric
states <qtpisa-glossary.html#oligomeric-state>`__ of macromolecules.

The ligand processing mode is a technical parameter, which is reserved
for expert use. Due to highly non-linear complexity of PISA algorithms,
it may slow down considerably if the number of ligands is high. There is
no ultimate threshold for the acceptable number of ligands, as it
depends also on crystal structure and composition. In order to cope with
this complexity, PISA may choose to fix all or some of ligands to
macromolecules they contact, observing that any single ligand can be
fixed only to one macromolecule. The makes the macromolecules with fixed
ligands effectively a combined monomeric unit, which decreases
computational complexity. In most cases, the only side-effect of this
procedure is in changed free energies of complex dissociation within
overall accuracy of PISA calculations. In very rare instances, the side
effect may need to be verified. For this, at user's request, PISA can
process ligands in 3 modes:

+------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Auto       | PISA decides automatically, which ligands should be fixed to macromolecules, and which ones should be considered as free entities. This is the default mode, and it is good in most cases.   |
+------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Fix all    | All ligands are mandatory fixed to macromolecules.                                                                                                                                           |
+------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Free all   | No ligands are fixed to macromolecules.                                                                                                                                                      |
+------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

|  
| If any changes to the default ligand processing are required, click
  checkbox change in the bottom panel. The panel will change its
  appearance like below:

|image3|

At this point, the user can choose which ligands to exclude from the
analysis as not belonging to the biological system, by ticking in the
corresponding checkboxes. Next, the user may choose whether to fix
ligands to macromolecules or not, or let PISA to decide on this, by
making the appropriate selection in the Ligand processing combobox.
After making changes, click button Rerun to (re-)start calculations. If
necessary, both changes and restart may be done while PISA calculations
are still running. Note that QtPISA does not have any means to terminate
on-going calculations, but, instead, it allows for working with partial
results, and it is Ok to quit QtPISA before the calculations finish.

| ***In which cases default options for ligand treatment should be
  adjusted?***

a. when PISA fails to automatically identify crystallisation agents or
   when it does it incorrectly
b. when a user wishes to study the ligand effect on protein's oligimeric
   state
c. when there are reasons to suspect that free energy change, caused by
   fixing ligands to macromolecules, is sufficient for changing the
   oligimeric state of weakly bound complex

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
.. |image2| image:: rerun-closed.png
.. |image3| image:: rerun-open.png

