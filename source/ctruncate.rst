CTRUNCATE (CCP4: Supported Program)
===================================

NAME
----

**ctruncate** - Intensity to amplitude conversion and data statistics.

SYNOPSIS
--------

| **ctruncate** **-hklin** *filename* **-hklout** *filename* **-seqin**
  *sequence file name* **-colin** *column label* **-colano** *column
  label* **-colout** *column label* **-nres** *number of residues*
  **-no-aniso** *turn off anisotopy correction* **-amplitudes** *use
  amplitudes* **-comp** *if set to **nucleic** use DNA/RNA reference
  curve* **-prior** *force use of **WILSON** or **FLAT** prior*
  **-freein** *retain freeR flag*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

Conversion of measured intensities to structure factors is complicated
by the fact that background subtraction can result in negative
intensities for weak reflections. CTRUNCATE is a new CCP4 program,
intended ultimately to replace the original TRUNCATE program, which uses
Bayesian statistics to calculate positive structure factors from
negative input intensities. Small positive intensities are also boosted
by the conversion process. The algorithm used is that of French and
Wilson.

In addition, CTRUNCATE calculates a number of statistics from the
intensity data, such as moments, cumulative intensity distributions and
the Wilson plot. When output in graphical form, these can be used to
assess data quality and to check for possible twinning.

CTRUNCATE looks for anisotropy in the data and performs anisotropy
correction. A test for translational NCS is also performed. Two methods
are used to test for twinning: Yeates' H test and Padilla and Yeates L
test. The twinning tests, as well as the moments and cumulative
intensity plots are done using data which has been corrected for
anisotropy. However the output structure factors and the Wilson plot use
uncorrected data. Should tNCS or twinning be found a flat prior based
upon the same approximations as the Wilson prior is applied.

INPUT/OUTPUT FILES
------------------

    .. rubric:: -hklin *filename*
       :name: hklin-filename

    (Compulsory) Input mtz file name. '-mtzin' is accepted as an
    alternative to '-hklin'.

    .. rubric:: -hklout *filename*
       :name: hklout-filename

    Output mtz file name. Default 'ctruncate\_out.mtz'. '-mtzout' is
    accepted as an alternative to '-hklout'.

    .. rubric:: -seqin *sequence file name*
       :name: seqin-sequence-file-name

    Input sequence file in any common format (e.g. pir, fasta), used for
    scaling.

KEYWORDED INPUT
---------------

    .. rubric:: -colin *column label*
       :name: colin-column-label

    Column names for mean intensity data. If only anomalous data is
    input, then only the anomalous data will be considered.

    .. rubric:: -colano *column label*
       :name: colano-column-label

    Column names for anomalous data. Both positive and negative names
    need to be specified e.g. '/\*/\*/[I(+),SIGI(+),I(-),SIGI(-)]'. If
    column names for anomalous data are not specified, the program will
    only consider mean data.

    .. rubric:: -colout *column label*
       :name: colout-column-label

    Identifier to be appended to output column names.

    .. rubric:: -nres *number of residues*
       :name: nres-number-of-residues

    Specifies the number of residues in the asymmetric unit for scaling
    purposes. If not used and an input sequence file is not provided,
    the number of residues will be estimated assuming 50% solvent
    content.

    .. rubric:: -no-aniso
       :name: no-aniso

    Anisotropy correction will not be performed.

    .. rubric:: -amplitudes
       :name: amplitudes

    Input data is structure factors rather than intensities.

    .. rubric:: -comp *nucleic*
       :name: comp-nucleic

    Use DNA/RNA reference curve. The reference curve derived from 65
    resonably high resolution intensities and structure factors from the
    PDB and nucleic acid data bank.

    .. rubric:: -prior *WILSON* or *FLAT*
       :name: prior-wilson-or-flat

    Force use of WILSON or FLAT prior.

    .. rubric:: -freein *colname*
       :name: freein-colname

    Retain the the freeR column. Default colname is
    '/\*/\*/[FreeR\_flag]'

Release Notes
-------------

1.13.0 - follow lead of K.Diederichs and extend tabulated range for
truncate procedure to -37 (this is were R calculation becomes unstable
for centric reflections)

1.12.5 - use range for anomalous limits rather than monatomical decrease

1.12.4 - protect output in cases were there are no twinops

1.12.3 - preserve MTZ history from input file

1.12.2 - obtain twinning fraction estimate from L-test (PY)

1.12.1 - reset anisotropy corrected data for moments

1.12.0 - reconfigure twinning tests - add ML Britton

1.11.5 - fix memory corruption on ubuntu - correct residue based cell
contents correction

1.11.4 - use weighted mean and sig\_mean=sqrt(s1\*s1+s2+s2) for I and F
- not using combined sig\_mean as this gives a large value for sigma on
Fmean and Imean

1.11.3 - output missing FMEAN and SIGFMEAN when output DANO

| 1.11.2
| - rationalise computation of means and sigmas from anomalous data
| - use I/sigI derived data for F/sigI if it is given with anomalous
  flag

| 1.11.1
| - correct anomalous measurebility plot and phil plot for anonalous
  input

| 1.11.0
| - allow use of friedal pairs, without Imean

| 1.10.2
| - use mean anisotropic eigenvalue when computing poorest resolution
  cuttoff

| 1.10.1
| - fix issue of going out of range in moments plot

| 1.10.0
| - use flat prior when request, or when have tNCS or twinning
| - implementation is based on acentric case for Wilson prior

| 1.9.1
| - correct Ice Rings statistic as we are working with merged data,
  unlike aimless

| 1.9.0
| - introduce RNA/DNA reference curve

| 1.8.9
| - bug fix to Optical Resolution routine

| 1.8.7, 1.8.8
| - quick escape in case of failed anisotropy calculation

| 1.8.6
| - update for gcc 4.7

| 1.8.5
| - allow poorer data to be used

| 1.8.4
| - use ML aniso values, corrected for Biso, to scale prior for truncate
| - this should gain some of the speed back

| 1.8.3
| - use ML aniso values, corrected for Biso, to scale data for twinning
  tests

| 1.8.2
| - restrict resolution range for aniso to tidy plots

| 1.8.1
| - simplify WilsonB class and plotting to reduce confusion

| 1.8.0
| - include completeness test for data quality. Currently hardwired at
  I/sigI > 3 and above 85%
| - more flexible Scattering class for calculation of the total
  scattering
| - Completeness, tNCS, YorgoModis, ResoCorrel classes added for
  analysis

| 1.7.1
| - make twinning test output more consistent

| 1.7
| - use anisotopy in prior for truncate procedure
| - no-aniso keyword now controls use of anisotropy in prior
| - anisotropy corrected for twinning statistics, now not optional

| 1.6
| - acentric tabulated data extended to h=10 to reduce discontinuity in
  plots
| - Phil plot added to monitor output

REFERENCES
----------

#. S. French and K. Wilson, Acta Cryst. A34, 517-525 (1978).
#. T. O. Yeates, Acta Cryst. A44, 142-144 (1980).
#. J. E. Padilla and T. O. Yeates, Acta Cryst. D59, 1124-1130 (2003).
#. P. Zwartz, CCP4 Newsletter 42.
#. Z. Dauter, Acta Cryst. D62, 867 (2006)
#. P. Zwartz, Acta Cryst. D61, 1437 (2005)
#. Schnieder, Acta Cryst. D58, 1772 (2002)
#. B.C. Wang, Methods Enzymol. 115, 90 (1985)

AUTHOR
------

Charles Ballard, Rutherford Appleton Laboratory.

Norman Stein, Daresbury Laboratory.
