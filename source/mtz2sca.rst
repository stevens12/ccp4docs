MTZ2SCA (CCP4: Unsupported Program)
===================================

NAME
----

**mtz2sca** - convert CCP4 mtz-files containing anomalous data to
Scalepack format

.. raw:: html

   <div class="main">

mtz2sca is meant to guess the correct labels from the input mtz-file.
Preference is set to I(+/-) over F(+/-) over F/DANO. These columns
should be present if you used the default settings of the CCP4 User
Interface. However, in case correct guessing fails or in case the user
prefers other columns to be extracted, these can be provided on the
command line. The following listing summarizes the options available in
mtz2sca.

.. rubric:: mtz2sca usage
   :name: usage

::

         mtz2sca [OPTIONS] <mtzin> [<scaout>]
        

where

<mtzin>
    filename of input mtz-file
<scaout>
    filename of output sca-file

If <scaout> is ommitted, it is set to <mtzin> with its mtz-suffix
replaced by sca, i.e. name.mtz to name.sca.
Possible options are:

-r name
    root of labels. The mtz-file is searched for the four labels
    name(+), SIGname(+),name(-), SIGname(-)
-p name
    Label for intensities or amplitudes for (hkl)
-P name
    Label for standard deviation of p
-n name
    Label for intensities or amplitudes for -(hkl)
-N name
    Label for standard deviation of n
-h
    print help message
-s
    silent: supress output to standard out

.. rubric:: Example
   :name: mtz2sca_example

::

    mtz2sca lyso_mos.mtz lyso.sca

converts a lysozyme data set lyso\_mos.mtz processed with mosflm into
the .sca-file lyso.sca.
::

    mtz2sca lyso_mos.mtz

This is the same, but the output file name will be lyso\_mos.sca.

--------------

`Tim
Gruene <mailto:tg%20at%20shelx%20dot%20uni-ac%20at%20gwdg%20dot%20de>`__
*Last modified: Thu May 06, 2010 08:12AM*

.. raw:: html

   </div>
