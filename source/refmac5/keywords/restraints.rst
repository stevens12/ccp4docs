REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac\_5.\*
------------------------------------------

Keyworded input - Restraints keywords
-------------------------------------

Anything input on a line after "!" or "#" is ignored and lines can be
continued by using a minus (-) sign. The program only checks the first 4
characters of each keyword. The order of the cards is not important
except that an END card must be last. Some keywords have various
subsidiary keywords. The available keywords in this section are:

    `**ANGLe** <#angl>`__
        Restraints on bond angles
    `**BFACtor/TEMPerature** <#bfac>`__
        Restraints on B values
    `**CHIRal\_volumes** <#chir>`__
        Restraints on chiral volumes
    `**DISTance** <#dist>`__
        Restraints on bond distances
    `**HOLD** <#hold>`__
        Restraints against excessive shifts
    `**MAKE\_restraints** <#makecif>`__
        *Controls making restraints and checking coordinates against
        dictionary*
    `**NCSR/NONX** <#ncsr>`__
        Restraints on non-crystallographic symmetry
    `**PLANe** <#plan>`__
        Restraints on planarity
    `**RBONd** <#rbon>`__
        Rigid bond restraints on the anisotropic B values of bonded
        atoms
    `**SPHEricity** <#sphe>`__
        Sphericity restraints on the anisotropic B values
    `**TORSION** <#tors>`__
        Restraints on the torsion angles
    `**VDWR/VAND** <#vdwr>`__
        Restraints on VDW repulsions

These keywords are used to create restraints and to set the weighting of
the restraints.

MAKE\_restraints [HYDR Y\|N\|A] [HOUT Y\|N] [CHECk Y\|N] [BUIL Y\|N] [FORM F\|U] [PEPT Y\|N] [LINK Y\|N] [SUGAr Y\|N\|D]\\ [CONNectivity Y\|N\|D] [SYMM Y\|N] [CISP Y\|N] [SS Y\|N\|D] [CHAIn Y\|N] [NEWLigand Exit\|Noexit] [VALUe COOR\|ENER] [EXIT Y\|N]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For full description including algorithms look
`MAKECIF"s <http://www.ysbl.york.ac.uk/~alexei/makecif.html>`__
documentation

This keyword controls the level of automatic restraint creation. REFMAC
uses predefined dictionary entries. Any additional entry could be added
using LIB\_IN <file name> which contains a user's local ligand
descriptions (see `How to make new ligand
dictionary? <../dictionary/makecif-newentry.html>`__). On the basis of
these files, the program makes intelligent suggestions on potential
extra restraints. If additional user input is needed, the program stops,
outputting information that should help the user to decide what to do
next (*e.g.* a coarse picture of a ligand as the program 'understands'
it, for the user to help in setting up any extra restraints).

Defaults:

    ::

        MAKE HYDR All
        MAKE HOUT No
        MAKE CHEC No
        MAKE BUIL No
        MAKE FORM Unformatted
        MAKE PEPT No
        MAKE LINK No
        MAKE SUGA Yes
        MAKE SS   Yes
        MAKE CISP Yes
        MAKE SYMM No
        MAKE CONN No
        MAKE CHAIn Yes
        MAKE NEWLigand Exit
        MAKE VALUe ENERgy
        MAKE EXIT No

Subkeywords:

HYDR [ Yes \| No \| All ] (Default HYDRogens All)
    How to deal with the hydrogen atoms.
    Y - if hydrogens are present in the input file, use them,
    N - ignore hydrogens even if they are present in the input
    coordinate file,
    A - add all hydrogens in their riding positions and use them for
    geometry and structure factor calculations. They also contribute to
    the gradient and second derivative matrix of geometry residual.
    For low resolution and early stages it is better to use MAKE HYDR N,
    *i.e.* do not use hydrogens even if they are present in the input
    file.

HOUT [ Yes \| No ] (Default HOUT N)
    Whether to write hydrogens to the output coordinate file or not.
    Y - write hydrogens to the output file,
    N - do not write hydrogens to the output file.

CHECk [ ALL \| LIGAnd \| NONE ] (Default CHECk LIGAnd)
    Whether to check amino acids in the input coordinate file against
    dictionary descriptions.
    ALL - check all monomers against dictionary descriptions,
    LIGAnd - do not check amino acids, DNA/RNA and standard sugars. In
    general monomers which are part of chain will not be checked. It is
    safe option to use.
    NONE - do not check all monomers. In this case program relies on
    correctness of monomer and atom names in monomers. User has to be
    very careful in using this option.

BUILd [ Yes \| No ] (Default BUILd N)
    Whether to build absent atoms or not. For example, if you have ASN
    in input coordinates and OD1 and ND2 are absent, the program can try
    to find their position. Usually rebuilt atoms have occupancy 0.00,
    so they do not affect refinement.
    Y - build absent atoms,
    N - do not build absent atoms.

FORM [ Formatted \| Unformatted ] (Default FORMat Unformatted)
    Whether to use restraints file in Formatted or Unformatted form. If
    you want to examine the list of restraints then you could use:

        ``MAKE FORM F MAKE EXIT Y``

    In other cases unformatted form seems to be faster to handle.

PEPT [ Yes \| No ] (Default PEPTide N)
    Check and/or use presence of L or D peptides.
    Y - check if peptide is L or D and use as they are,
    N - check if there are D peptides but use standard L peptide
    descriptions.

LINK [ Yes \| No \| Define ] (Default LINK N)
    Check and/or use links between different residues that are not
    involved in standard peptide links (*i.e.* connectivity that is not
    implied by the primary structure. Atoms involved in bonds between
    HET groups, or between a HET group and standard residues, as well as
    reduced peptide bonds can be described with this keyword).
    Y - check and, if specified by record LINK in the input coordinate
    file, use links between residues. Links which have not been
    specified in the input coordinates but are found by the program,
    will be added to the list of links specified in the input coordinate
    file.
    N - check links between residues and give information about them in
    the log file, but do not use them UNLESS they are specified by
    record LINK in the input coordinate file.
    D - ignore links in the input coordinate file specified by record
    LINK. Check if current coordinates suggest links, and ONLY use
    those; *i.e.* let the program *define* what is a link and what is
    not.
    In the early stages it is better to use LINK N. In later stages the
    user can use the following keywords to check if there are any links
    between residues which should be taken into account:

        ::

            MAKE FORM F
            MAKE LINK D
            MAKE EXIT Y
            MONItor MANY

    After creation of restraints and checking all links, the program
    will stop. Then the user needs to examine the .log file, restraint
    file and the new library suggested by the program, to see if there
    is anything worthy of more attention.

    Examples of a LINK record in a coordinate file:

        ::

                     1         2         3         4         5         6         7
            123456789012345678901234567890123456789012345678901234567890123456789012
            LINK         O1  DDA     1                 C3  DDL     2
            LINK        MN    MN   391                 OE2 GLU   217            2565

SUGAr [ Yes \| No \| Define ] (Default SUGAr Y)
    Check and/or use links between sugars and sugar-protein links. These
    links can be specified through record LINK in coordinate files.
    Y - check and, if specified by record LINK in the input coordinate
    file, use links between sugars and sugar-protein links. If sugar
    links are present in the input coordinate file they will have high
    priority.
    N - check links between sugars and sugar-protein links and give
    information about them in the log file, but do not use them UNLESS
    they are specified by record LINK in the input coordinate file.
    D - ignore sugar links in the input coordinate file specified by
    record LINK. Check if current coordinates suggest links, and ONLY
    use those; *i.e.* let the program *define* what is a suitable sugar
    link and what is not.

SSbridge [ Yes \| No ] (Default SS Y)
    Check and/or use disulphide bridges automatically.
    Y - the program will find disulphide bridges and use restraints for
    them.
    N - the program will check presence of disulphide bridges but ONLY
    use disulphide bridges if they are specified in the input coordinate
    file through record SSBOND.

CISP [ Yes \| No ] (Default CISPeptide Y)
    Check and/or use *cis* peptides automatically.
    Y - find and use *cis* peptides automatically,
    N - check but do not use *cis* peptides UNLESS they have been given
    specifically in the input coordinate file, through record CISPEP.

SYMM [ Yes \| No ] (Default SYMM N)
    Check and/or use links between symmetry related atoms.
    Y - check the existence of links between symmetry related atoms and
    use if described explicitly,
    N - do not use links between symmetry related atoms.

CONN [ Yes \| No \| Define ] (Default CONNectivity N)
    Check connectivity between consecutive residues (*e.g.* standard
    peptide links and connectivity within a HET group).
    Y - check and, if specified by record CONECT in the input coordinate
    file, use connectivity in polypeptides and DNA/RNA. Connectivity
    which is not specified in the input coordinates but is found by the
    program, will be added to the list of connectivity specified in the
    input coordinate file.
    N - check connectivity between consecutive residues of polypeptide
    chain and DNA/RNA, but do not use them UNLESS they are specified by
    record CONECT in the input coordinate file. If consecutive residue
    numbers differ by more than two, it is assumed there is no link
    between them.
    D - ignore connectivity in the input coordinate file specified by
    record CONECT. Check if coordinates suggest connectivity, and ONLY
    use those; *i.e.* let the program *define* what is connected and
    what is not.

CHAIn [ Yes \| No ] (Default CHAIn N)
    Check and/or use chain definition automatically.
    Y - check chain definitions for peptide/DNA/RNA/sugar, cyclic
    peptide *etc*., and use if they are suggested by the coordinate
    file.
    N - check but do not use chain definitions.

NEWLigand [ Exit \| Noexit ] (Default NEWLigand Exit)
    If the program encounters new ligand, it will stop and tell what to
    do next. If

    ::

        MAKE NEWLigand Noexit

    has been specified then the program will continue to work relying on
    the dictionary description it has created using coordinates and/or
    minimum description of this ligand. At the moment it is highly
    recommended to check entries created by the program and make sure
    that these correspond to the ligands the user wants.

VALUe [ COORdinates \| ENERgy ] (Default VALUe ENERgy)
    It tells to the program where to take the ideal values. If 'ENERgy'
    has been specified then ideal values will be taken from the
    energetic library. If 'COORdinates' has been specified then the
    program will take ideal values from the coordinates. If you want to
    take ideal values from the coordinates then make sure that
    coordinates have been derived using good energetic minimisers or
    they have been refined against very high resolution data.

EXIT [ Yes \| No ] (Default EXIT N)
    It tells the program to exit or not after creating restraints. It is
    useful when a new entry is added. Then the user could use following
    keywords:

        ::

            MAKE FORM F
            MAKE EXIT Y

    Then look at the restraints file and check validity and values of
    the restraints.

DISTance <wdskal>
~~~~~~~~~~~~~~~~~

[Default 1.0]

<WDSKAL> is a multiplier used in calculating the weights for the
distance restraints. The weights are of the form: Weight = (WDSKAL/
SIGD)\ :sup:`2`. Sigmas for bond distances come from the dictionary
file.

ANGLe <angle\_scale>
~~~~~~~~~~~~~~~~~~~~

[Default 1.0]

<angle\_scale> is multiplier used in calculating weights for the bond
angle restraints. The weights have the form: Weight =
(angle\_scale/SIG\_angle):sup:`2`. SIG\_angle comes from the dictionary
file.

PLANe <wpskal>
~~~~~~~~~~~~~~

[Default 1.0]

<WPSKAL> is a multiplier for the distance restraints defining planes.
The weight used for the planes is (WPSKAL / SIGPlane)\ :sup:`2`.
SIGPlane comes from the dictionary file.

CHIRal <wcskal>
~~~~~~~~~~~~~~~

[Default 1.0 0.15]

<WCSKAL> is used in weighting Chiral groups restraints. The weight used
is (WCSKAL /SIGChiral):sup:`2`. SIGChiral comes from the dictionary
file.

TEMPerature \| BFAC <wbskal> <sigb1> <sigb2> <sigb3> <sigb4>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

or

TEMPerature \| BFAC SET <B\_value>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For example:

    ::

        BFAC 1.0 1.5 2.0 3.0 4.5
        BFACtor SET 20.0

[Default 1.0 1.5 2.0 3.0 4.5 ]

<WBSKAL> is used in calculating the weight for the temperature factor
restraint parameters based on the types of bonding in which the atoms
are involved. Weight calculated for B\_value restraints is
(WBSKAL/<sigbi>)\*\*2.

<sigb1>
    sigma for bonded atoms of the main chain
<sigb2>
    for angle related atoms of the main chain
<sigb3>
    for bonded atoms of the side chain
<sigb4>
    for angle related atoms of side chain

SET <B\_value> - tells the program to set all B values to <B\_value>

SPHEricity <sigsph>
~~~~~~~~~~~~~~~~~~~

[Default 2.0]

For example:

    ::

        SPHEricity 5.0

<sigsph> is used to restrain atomic anisotropic tensor to be spherical.
Used weight is calculated as 1.0/<sigsph>\*\*2.

RBONd <sigrbond>
~~~~~~~~~~~~~~~~

[Default 2.0 ]

For example:

    ::

        RBONd 3.0

<sigrbond> is used to make minimum of projections of anisotropic tensors
along bond for the bonded atoms. Used weight is calculated as
1.0/<sigrbond>\*\*2.

NCSR [NCHAIN <nchains>] [CHAIns <chain\_id\_1>.....<chain\_id\_nchains>] [NSPAns <nspans> <residue\_first\_1> <residue\_last\_1> <ncode\_1>..... <residue\_first\_nspans> <residue\_last\_nspans> <ncode\_nspans>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To define non-crystallographic symmetry. Previously part of PROTIN. Now
moved to REFMAC.

For example:

    ::

        NCSRestraints NCHAins 5 CHAIns A B C D E NSPANS 2 1 100 1 101 150 2

or

NCSR <wsscal> <sgsp1> <sigsp2> <sigsp3> <sigsb1> <sigsb2> <sigsb3>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parameters for weighting of the contribution of the ncs restraints.

For example (they are default values):

    ::

        NCSR 1.0 0.05 0.5 5.0 0.5 2.0 10.0

This keyword controls restraints on non-crystallographically related
atoms. If the first value after NCSR is NCHAin, then the first option,
*i.e.* definition of NCS restraints, is assumed. In this case, the
following subkeywords are available:

NCHAIns <nchains>
    Number of chains involved in this NCS restraint
CHAIns <chain\_id\_1>.....<chain\_id\_nchains>
    Identifier of the chains involved in this NCS restraint. Number of
    chain identifiers should be equal to <nchains>.
NSPAns <nspans> <residue\_first\_1> <residue\_last\_1> <ncode\_1>.....<residue\_first\_nspans> <residue\_last\_nspans> <ncode\_nspans>
    Number of pieces of chains which should be treated with different
    weighting schemes.
    <nspans> is number of spans,
    <residue\_first\_i><residue\_last\_i> are first, last residue
    numbers involved in ``i``'th span, with ``i``\ =1,nspans.
    <ncode\_i> is weighting to be used for ``i``'th span, with
    ``i``\ =1,nspans.
    Codes for restraints are as follows:

        ::

            ncode    Main_Chain       Side_Chain

            1    tight restraint   tight restraint
            2    tight restraint   medium restraint
            3    tight restraint   loose restraint
            4    medium restraint  medium restraint
            5    medium restraint  loose restraint
            6    loose restraint   loose restraint

If NCSR is followed only by numbers, these values are used for weighting
of contribution of NCS restraints.

[Default 1.0 0.05 0.5 5.0 0.5 2.0 10.0]

<wsscal> is used in weighting restraints involving non-crystallographic
symmetry. The weight is given by (<wsscal> / <sigs>)\*\*2.

<sigsp1>, <sigsp2>, <sigsp3> are values associated with
non-crystallographic positional restraints and are for tight, medium and
loose restraints respectively.

<sigsb1>, <sigsb2>, <sigsb3> are values associated with
non-crystallographic thermal restraints and are for tight, medium and
loose restraints respectively.

TORSION <wtskal>
~~~~~~~~~~~~~~~~

[Default 1.0]

For example:

    ::

        TORSion 2.0

<wtskal> is a multiplier used in calculating the weights for the
torsional angle restraints. The weight is of the form Weight = (<wtskal>
/ <SIGT>)\*\*2, SIGT comes from dictionary files.

VANDerwaals (or VDWR or NONBonding) <wvskal>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

or

VANDerwaal (or VDWR or NONBonding) [OVERall <wcskal>] [SIGMA VDW <vsigma > \| HBONd <hsigma> \| METAl <msigma> \| TORSion <tsigma> DUMMy <dsigma> ] [INCRement TORSion <tincrement> \| ADHB <adincrement> \| AHHB <ahincrement> DUMMy <dincrement>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[Default:

    ::

        NONBonding OVERall 1.0
        NONBonding SIGMa VDW 0.3
        NONBonding SIGMa HBOND 0.5
        NONBonding SIGMa METAl 0.5
        NONBonding SIGMa TORSion 0.5
        NONBonding SIGMa DUMMy   0.3
        NONBonding INCRement TORSion -0.75
        NONBonding INCRement ADHB    -0.2
        NONBonding INCRement AHHB     0.2 
        NONBonding INCRement DUMMy   -0.7 ]

This keywords controls parameters and weights of restraints on
nonbonding interactions. REFMAC deals only with the repulsive part of
nonbonding interactions, *i.e.* if separation of the atoms is larger
than "*ideal*" then they are not considered to be interacting atoms.

The program excludes all atom pairs related by one covalent bond, angle,
or if they are in the same plane. Atoms related by one torsion angle are
treated separately.

OVERall <wvskal>
    Controls overall weights on all nonbonding interactions.
SIGMa VDW <vsigma> \| HBONd <hsigma> \| METAl <msigma> \| TORSion
<tsigma> DUMMy <dsigma>
    Controls sigmas for each type of nonbonding interactions.

    VDW <vsigma>
        sigma of atom pair involved in vdw repulsion
    HBONd <hsigma>
        sigma of atom pair potentially involved in hydrogen bonding
        interactions
    METAl <msigma>
        sigma of atom pair potentially involved in metal-ion
        interactions
    TORSion <tsigma>
        sigma of atom pair involved in vdw interactions and related by
        one torsion angle.
    DUMMy <dsigma>
        sigma of atom pair involved dummy and nondummy atoms.

INCRement TORSion <tincrement> \| ADHB <adincrement> \| AHHB
<ahincrement>
    Provides increment factors for different types of nonbonding
    interactions.

    TORSion <tincrement>
        If atoms are related by one torsion angle and they are closer
        than vdw\ :sub:`1` + vdw\ :sub:`2` + tincrement, then they are
        considered to be interacting atoms. Here vdw\ :sub:`1` and
        vdw\ :sub:`1` are vdw radii of the interacting atoms.
    ADHB <adincrement>
        If atoms can make hydrogen bond, *i.e.* one of them is a
        potential acceptor and the other is a potential donor and they
        are closer than vdw\ :sub:`1` + vdw\ :sub:`2` + adincrement,
        then they are considered to be interacting atoms. Here
        vdw\ :sub:`1` and vdw\ :sub:`2` are vdw radii of donor and
        acceptor.
    AHHB <ahincrement>
        If one of atoms is an acceptor and the other one a hydrogen from
        a donor and they are closer than vdw\ :sub:`1` + ahincrement,
        then they are considered to be interacting atoms. Here
        vdw\ :sub:`1` is vdw radius of the acceptor.
    DUMMy <dincrement>
        If on of atoms is dummy and another is nondummy and they are
        closer than vdw\ :sub:`1` + vdw\ :sub:`2` + dincrement, then
        they are considered as interacting atoms. Here vdw\ :sub:`1` and
        vdw\ :sub:`2` are vdw radii of atoms involved in the
        interaction.

HOLD <PBEL> <BDEL> <QDEL>
~~~~~~~~~~~~~~~~~~~~~~~~~

[Default 0.3 3.0 0.2]

Restraint to current values <PDEL> is a restraint on the magnitude of
positional shifts. This goes into the matrix as (a/<PDEL>)\*\*2,
(b/<PDEL>)\*\*2, (c/<PDEL>)\*\*2 where a, b, c are the unit cell
parameters.

<BDEL> is a shifts magnitude restraint on individual thermal parameters
and goes into the matrix as (1/<BDEL>)\*\*2. It is not used if ITEMP =
0.

<QDEL> is the shifts magnitude restraint on variable occupancy factors
(not used if NOCC=0). This goes into the matrix as (1/<QDEL>)\*\*2.

[See also keyword `MONItor <xray-general.html#moni>`__]
