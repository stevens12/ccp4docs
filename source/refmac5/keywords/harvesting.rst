REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac\_5.\*
------------------------------------------

Keyworded input - Harvesting keywords
-------------------------------------

Anything input on a line after "!" or "#" is ignored and lines can be
continued by using a minus (-) sign. The program only checks the first 4
characters of each keyword. The order of the cards is not important
except that an END card must be last. Some keywords have various
subsidiary keywords. The available keywords in this section are:

    `**DNAME** <#dname>`__, `**NOHARVEST** <#noharvest>`__,
    `**PNAME** <#pname>`__, `**PRIVATE** <#private>`__,
    `**RSIZE** <#rsize>`__, `**USECWD** <#usecwd>`__,

Provided a Project Name and a Dataset Name are specified (either
explicitly or from the MTZ file) and provided the
`NOHARVEST <#noharvest>`__ keyword is not given, the program will
automatically produce a data harvesting file. This file will be written
to:

``$HARVESTHOME``/``DepositFiles``/*<projectname>*/*<datasetname>.refmac*

The environment variable ``$HARVESTHOME`` defaults to the user's home
directory, but could be changed, for example, to a group project
directory. When running the program through the CCP4 interface, the
$HARVESTHOME variable defaults to the 'PROJECT' directory.

PNAME <project\_name>
~~~~~~~~~~~~~~~~~~~~~

Project Name. In most cases, this will be inherited from the MTZ file.

DNAME <dataset\_name>
~~~~~~~~~~~~~~~~~~~~~

Dataset Name. In most cases, this will be inherited from the MTZ file.

PRIVATE
~~~~~~~

Set the directory permissions to '700', *i.e.* read/write/execute for
the user only (default '755').

USECWD
~~~~~~

Write the deposit file to the current directory, rather than a
subdirectory of $HARVESTHOME. This can be used to send deposit files
from speculative runs to the local directory rather than the official
project directory, or can be used when the program is being run on a
machine without access to the directory ``$HARVESTHOME``.

RSIZE <row\_length>
~~~~~~~~~~~~~~~~~~~

Maximum width of a row in the deposit file (default 80). <row\_length>
should be between 80 and 132 characters.

NOHARVEST
~~~~~~~~~

Do not write out a deposit file; default is to do so provided Project
and Dataset names are available.
