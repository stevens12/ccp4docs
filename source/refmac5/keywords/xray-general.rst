REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac\_5.\*
------------------------------------------

Keyworded input - Other Xray keywords
-------------------------------------

Anything input on a line after "!" or "#" is ignored and lines can be
continued by using a minus (-) sign. The program only checks the first 4
characters of each keyword. The order of the cards is not important
except that an END card must be last. Some keywords have various
subsidiary keywords. The available keywords in this section are:

    `**BINS/RANGE** <#bins>`__
        Number of the resolution bins
    `**BLIM** <#blim>`__
        Limits of allowed B value range
    `**CELL** <#cell>`__
        Cell parameters
    `**DAMP** <#damp>`__
        Factors to scale down shifts at every cycle
    `**END/GO** <#end>`__
        End of keywords
    `**FREE** <#free>`__
        Flag of the reflections excluded from the refinement
    `**LABO** <#labout>`__
        Output MTZ labels. Useful for map calculation
    `**MODE** <#mode>`__
        Refinement mode
    `**MONI** <#moni>`__
        Level of monitoring statistics during refinement
    `**PHAS** <#phas>`__
        Parameters for the phased refinement
    `**RIGI** <#rigi>`__
        Parameters of the rigid body refinement
    `**SCPA** <#scpa>`__
        For scaling of the external partial structure factors
    `**SHAN** <#shan>`__
        Shannon factor to control grid spacings
    `**SYMM** <#symm>`__
        Symmetry
    `**TLSC** <#tlsc>`__
        Number of TLS cycles

BINS \| RANGE <nbins> \| <range>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Number of resolution bins <nbins>. Default 20, maximum allowed 100. For
least-square refinements it is useful only for monitoring statistics on
resolution ranges. For maximum likelhood it is used for normalisation to
convert Fs to Es (*i.e.* normalisation coefficients are calculated in
these resolution bins).

Or <range>. If the value after BINS/RANGE is less than 1.0 this is
assumed to define the bin width in units of 4\*sin\*\*2/Lambda\*\*2.

BLIM <Bmin> <Bmax>
~~~~~~~~~~~~~~~~~~

| [Default 2.0 500.0]
| Bmin and Bmax are minimum and maximum B-factors allowed. The lower
  limit is required by the program, but is set by default.

CELL <a> <b> <c> [ <alpha> <beta> <gamma> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Defines parameters of the cell. If this keyword is specified then cell
parameters from MTZ and coordinate files will be overridden. This
keyword could be important when cell dimensions are suspect and the user
wants to change them. This keyword is generally not recommended.

DAMP <Pdamp> <Bdamp>
~~~~~~~~~~~~~~~~~~~~

| [Default 1.0 1.0 for resolution > 2.5Å, 0.5 0.5 for resolutions <
  2.5Å]
| <Pdamp> <Bdamp> scales shifts after each cycle of refinement. If there
  is limited data, or you are using unrestrained refinement, it is
  sensible to scale down the shifts.

FREE <nfree\_exclud>
~~~~~~~~~~~~~~~~~~~~

| [Default 0]
| The default value for exclusion for the FreeR is zero. However there
  is an opportunity to reset this exclusion flag here. See the
  description of the program `FREERFLAG <../../freerflag.html>`__.

LABOUT <program label>=<file label> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keyword tells the program that in output MTZ file calculated
structure factors, their phases, coefficients for map calculations
should have given labels.

For example:

::

          #  Output labels for calculated structure factors and
          #  coefficients for weighted "difference" and "2Fo-Fc"
          #  maps. Corresponding phases will have labels PHWT, DELFWT
          #  figure of merit of phases will be FOM
          #
          LABOUT FC=FC PHIC=PHIC  FWT=2FOFCWT DELFWT=FOFCWT

          #  Another example when all labels are given explicitly
          #
          LABOUT FC=FC PHIC=PHIC  FWT=2FOFCWT PHWT=PH2FOFCWT DELFWT=FOFCW -
                 PHDELWT=PH_fofc FOM=FOM_refmac

FP SIGFP (and FREE if available) plus the following additional columns
are written to the output MTZ file. Labels can be assigned for:

    ``FP SIGFP FREE   FC  PHIC   FWT PHWT   DELFWT PHDELWT   FOM   [PHCOMB]``

FP SIGFP
    By default, FP and SIGFP are scaled to match FC, *i.e.* in output
    file FP and SIGFP meant to be in the "absolute" scale. The overall
    isotropic B correction is applied to the FC terms. At the same time
    individual atomic B values are incremented by overall B value.
    Overall anisotropic B values are estimated in the first cycle and
    fixed for further refinement cycles.
FREE (if available)
    Copied from input.
FC PHIC
    FC PHIC are the structure factor contributions from the input atoms
    only, *without* any FPARTi terms added.
FWT PHWT
    FWT and PHWT are amplitude and phase for weighted "2Fo-Fc" map
    (2mFo-DFcalc).
DELFWT PHDELWT
    DELFWT and PHDELWT are amplitude and phase for weighted 'difference'
    map (mFo-DFCalc).
PHCOMB
    PHCOMB is the combined phase to use with FP. It is output only if
    "experimental " or *prior* phase information were input.
    If prior phase probability distribution is available (either through
    Hendrickson and Lattman coefficients or through centroid phases and
    their figure of merit) then all output Fourier coefficients except
    PHIC correspond to the combined phases.
FOM
    FOM = <m> - The "figure of merit" for this reflection.

| For the FWT and DELFWT terms, FP is scaled to be at the same scale as
  Fcalc,
| FCalc= vector sum of {Dc FC + D1 FPART1 + D2 FPART2 + .. },
| Di = <cos (2 pi s Delta x)> are the resolution dependent coefficient
  which reflects errors in the atomic parameters, and m is the figure of
  merit for this reflection. (PHDELWT and PHWT will be "combined" phases
  if HLA *etc.* or PHIB FOM have been assigned.) Otherwise they are
  equal to either PHIC or PHIC+180.

Missing Data: For those reflections where the FP are missing, mFo is set
equal to dFc. Hence the terms become FWT=dFC and DELFWT=0.0. The m and D
are based on the program's estimate of SigmaA.

Rebuilding into these 2mFo-DFcalc and mFo-DFcalc maps seems to be easier
than using classic nFO-(n-1)FC and difference maps, consistent with the
established technique for SigmaA style maps. One advantage here is that
since the m and D values are based on the Free set of reflections they
are less biased than the values obtained by the CCP4 version of SIGMAA
after refinement.

MODE [HKRF \| RIGID \| TLSR]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[Default HKRF]

Sub-keywords:

HKRF
    This invokes standard refinement of individual atomic parameters.
RIGID
    This invokes RIGID body refinement. The description of domains is
    given under keyword `RIGID <#rigi>`__.
TLSR
    This invokes TLS refinement. Definition of rigid bodies is taken
    from TLSIN input file (refer to input output files).

MONI <subkeyword 1> [<subkeyword i> <value>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keyword controls level of monitoring statistics during refinement.

Default is:

    ::

        MONItor MEDIim
        MONItor DISTances 10.0
        MONItor TORSions  10.0
        MONItor ANGLes    10.0
        MONItor CHIRals   10.0
        MONItor PLANes    10.0
        MONItor BFACtors  10.0
        MONItor BSHPere   10.0
        MONItor VDWrest    3.0
        MONItor RBONd     10.0
        MONItor NCSR      10.0

<subkeyword 1> can be one of:

NONE
    means no monitoring. Program will work with very little output info.
FEW
    means only overall statistics will be monitored.
MEDI
    means MEDIum number of statistics will be monitored, *i.e.* at the
    first and last cycles overall statistics and outliers will be
    output. In all other cycles only minimum information (R value, free
    R value, figure of merit and first two geometric statistics). The
    user can choose the quantity of outliers by choosing a number.
MANY
    means overall statistics and outliers will be monitored at every
    cycle.

The subsequent <subkeyword i> are:

TORSION <badtor>
    Print restrained torsion angles differing by more than badtor \*
    torsig from the ideal value. Default is 10.
DISTance <baddis>
    Print restrained bond distances differing by more than baddis \*
    dissig from the ideal value. Default is 10.
ANGLe <bad\_angle>
    Print restrained bond angles differing by more than bad\_angle
    \*angle\_sigma. angle\_sigma comes from dictionary file. Default is
    bad\_angle = 10.0.
PLANE < badpln>
    Print restrained planes differing by more than badpln \* plnsig from
    the ideal value. Default is 10.
VANderwaals (or VDWRestraint) <badvdw>
    Print vdw and ionic contact distances that are closer than
    badvdw\*vdw\_sigma. Default is 3.0.
CHIRAL <badchi>
    Print restrained chiral volumes differing by more than BADCHI \*
    chisig from the ideal value. Default is 10.0.
BFACtor <bad\_b>
    Print out bonded atoms if difference between B values is more than
    bad\_b \*B\_sigma. Default is 10.0.
BSPHere <bad\_sphere>
    Print out atoms if their anisotropy deviate from a sphere by
    bad\_sphere \* sphere\_sigma. Default is 10.0.
RBONd <bad\_rbond>
    Print out atoms if rigid body restraint deviates from ideal by
    bad\_rbond\*rbond\_sigma.
NCSR <bad\_ncsr>
    Print out atoms if either poistional parameters or B values deviate
    from the average value over ncs related atoms is more than
    bad\_ncs\*sigma. Default 10.0.

NCYC <ncycref>
~~~~~~~~~~~~~~

| [Default 5]
| This keyword defines number of cycles of refinement.

PHASe SCBL <scblur> \| BBLUr <bblur>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keyword tells the program that probability distribution of given
phase information should be altered (this can also be done through `REFI
PHAS <xray-principal.html#refi_phas>`__). For example:

    ::

        PHASe SCBLur 0.7 BBLUr 20.0

Program will apply blurring as follows:

    ::

        HLAnew = HLA*scblur*exp(-(sin(theta)/lambda)**2*bblur)
        HLBnew = HLB*scblur*exp(-(sin(theta)/lambda)**2*bblur)
        HLCnew = HLC*scblur*exp(-(sin(theta)/lambda)**2*bblur)
        HLDnew = HLD*scblur*exp(-(sin(theta)/lambda)**2*bblur)

or if PHASE and FOM are given: the program first generates HLA and HLB
using the formula:

    ::

        HLA = Func(FOM)*COS(DEGTOR*PHASE),
        HLB = Func(FOM)*SIN(DEGTOR*PHASE),
        HLC = HLD = 0.

*i.e.* the Phase probability distribution is unimodal.

RIGIDbody GROUP \| PRINT \| NCYCLE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keyword controls parameters of the rigid body refinement.

For example:

    ::

        RIGIDbody NCYCle 10
        RIGIDbody GROUp 1 FROM 1   A TO 100 A
        RIGIDbody GROUp 1 FROM 200 A TO 300 A
        RIGIDbody GROUp 2 FROM 101 A TO 199 A
        RIGIDbody GROUp 3 FROM 1   B TO 500 B

Subkeywords:

GROUp <ngroup> FROM <resnum> <chnid> TO <resnum> <chnid> \| EXCLude \|
TRANS \| EULErangles

<ngroup>
    Domain number. Maximum number of domains in the current version of
    program is 100. Each domain can consist of 100 different pieces. For
    example:

        ::

            RIGIDbody GROUP 1 FROM 10  A TO 100 A  #  A domain containing most of the A chain, and 
            RIGIDbody GROUP 1 FROM 108 A TO 176 A  #  an embedded bit of a B chain, consisting of 
            RIGIDbody GROUP 1 FROM 200 B TO 220 B  #  residues 200-220. 
            ..
            RIGIDbody GROUP 2 FROM 10  B TO 100 B  # for example the equivalent domain across a NCS axis
            RIGIDbody GROUP 2 FROM 108 B TO 176 B  
            RIGIDbody GROUP 2 FROM 200 A TO 220 A

<resnum> <chnid>
    Spans are specified by the residue number, and the chain ID. See
    `examples <../usage/refmac-examples.html>`__. Default is: all atoms
    belong to one domain.
EXCLude SCHAin \| MCHAin \| NONE
    Exclude main chain, or side chain atoms from refinement. Rotation
    and translation are determined for the defined subset. Rotation and
    translation are applied for all atoms also. Default is NO EXCLUSION.
TRANS <Tx> <Ty> <Tz>
    To add translation for this domain before starting refinement.
    Default is 0.0 0.0 0.0.
EULErangles <alpha> <beta> <gamma>
    Rotate domain by these angles around its center of the mass (in
    degrees) before starting refinement Default is 0.0 0.0 0.0.
PRINT EULEr MATRix POLAr
    Print rotation angles (EULErangles) and rotation MATRix. Default is
    EULErangles, Rotation matrix and polar angles.
NCYCle <ncycle>
    Number of cycles for rigid body refinement. Default is 40 cycles.

SCPArt < nsc1> <nsc2> <nsc3> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If NSCi are set, the FPARTnsci is scaled relative of the FC FC\_tot =
FC\_calc(PHIcalc) + sc1\*FPART1(PHIP1) + sc2\*FPART2(PHI2) + ....

If NSCi set - that partial structure factor will be scaled

For example:

    ::

        #
        #   Scale only partial 1st, 3rd and 4th structure factors.
        #   All others should be added to Fcalc without scaling
        #
        SCPArt 1 3 4

SYMM <symmetry>
~~~~~~~~~~~~~~~

Defines space group symmetry name or number. If MTZ or coordinate files
have symmetry then they will be used. It is good idea to have symmetry
in the MTZ and coordinate files. This keyword is not recommended.

TLSC <ncycle>
~~~~~~~~~~~~~

It defines number of TLS refinement cycles. If this keyword is specified
then program will do TLS refinement only. It is useful if one is
interested in TLS parameters only. If TLS refinement is considered as
precursor of individual atomic refinement then

    ::

        REFI TLSC

should be used. In any case it seems to be better if B values of all
atoms are set to some predefined value before tls refinement using

    ::

        BFACtor SET <value>

NCYCle <ncycle>
~~~~~~~~~~~~~~~

Default:

    ::

        NCYCle 5

Number of cycles for idealisation or restrained or unrestrained
refinement.

SHANnon\_factor <shannon\_factor>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keywords tells the program to change grid spacing by a given
factor.

Default:

    ::

        SHANnon_factor 1.5

If this keyword is given then grid spacing for structure factor,
gradient and second derivative calculation will change accordingly.
According to Niquist for a given resolution if grid spacing is equal to
grid\_spacing\_min=0.5 d\_max, where d\_max is maximum resolution in
angstroms, then a discrete Fourier transform will not lose any
information. It is true when structure factors are calculated from the
map and vice versa. When maps are calculated from atomic model (or
gradients and second derivatives are calculated using convolution of
derivatives of atom with difference and "Hessian" maps), then finer grid
might be needed. The reason is for example if grid spacing is coarse and
atoms have small B values, then values of electron density at the grid
points may not approximate atoms correctly. Default (1.5) is good
compromise. But if desired it could be changed. Shannon factor tells the
program that actual grid spacing should be equal to
grid\_spacing\_min/shannon\_factor. If <shannon\_factor> is increased
then calculation will require larger memory and more time. If it is too
small then approximation will not be correct and the program might
become unstable.

END \| GO \| QUIT \| STOP
~~~~~~~~~~~~~~~~~~~~~~~~~

End of keywords. Time to do work.
