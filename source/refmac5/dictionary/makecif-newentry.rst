REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac version 5.0.\*
---------------------------------------------------

How to make new ligand dictionary?
----------------------------------

Contents
~~~~~~~~

#. 

   `**Description of the dictionary
   files** <http://www.ysbl.york.ac.uk/~alexei/dictionary.html>`__
       Description of organisation of the ligand dictionary used by the
       program REFMAC

#. 

   `**Dictionary entry from coordinate file(s)** <#coord_dict>`__
       How to make dictionary entry if there is coordinate file.
       Coordinate file formats recognised are pdb, small molecular cif
       (for example when coordinates are form Cambridge small molecular
       database - CSD) or mmCIF

#. 

   `**Dictionary entry from connectivity list** <#connect_dict>`__
       If ligand under investigation is available in the pdb list of
       ligands as connectivity list.

#. 

   `**Dictionary entry from minimum description** <#minimum_dict>`__
       How to make dictionary entry by describing only connectivity list
       and element names?

#. 

   `**Interactive sketcher to make dictionary
   entry** <http://www.ysbl.york.ac.uk/~lizp/sketcher.html>`__
       Using interactive sketcher which is part of CCP4 graphical user
       interface to make dictionary entry.

#. 

   `**How to avoid three letter residue name
   problem** <../files/coordinates.html#pdb_modres>`__
       PDB residue names are restricted to three letter. This section
       describes how to avoid this problem and not to break pdb rule

#. 

   `**Retrieve coordinates from dictionary** <retrieve.html>`__
       If ligand is in the dictionary file supplied by us how retrieve
       coordinates to start to build using graphics

#. 

   `**List of standard monomer descriptions** <list-of-ligands.html>`__
       List of the ligand description distributed with the program
       REFMAC

#. 

   `**Links to non-commercial edtitors to derive coordinates from
   chemical desctiption** <#link_to_getcoords>`__
       Links to various non-commercial web sites which could help to
       derive coordinates from chemical descriptions. And these
       coordinates could be used to derive dictionary entry as well as
       model (re)building.

How to make a dictionary entry from the coordinate file?
--------------------------------------------------------

Coordinate file formats recognised are pdb, small molecular cif and
mmCIF.

If there is a pdb coordinate file then REFMAC can be used to create a
dictionary entry. In this case following script should be used:

::

    refmac XYZIN &ltnew_ligand.pdb> XYZOUT <new_ligand1.pdb> \
    LIB_IN <my_lib_if_there_is_any.lib< LIB_OUT <my_new_lib.lib> 
    << eor
    #
    #
    MODE NEWEntry
    MAKE EXIT Yes
    #
    END
    eor

If ligand contained in the coordinate file is not in the list of `the
standard entries <list-of-ligands.html>`__ then the program will create
a description for it and add them to the user supplied entry lists
specified by LIB\_IN. Output file will be written to the file specified
by LIB\_OUT (default is new.lib) in the command line. If there is no
user supplied entry list (i.e. LIB\_IN has not been specified) then
created file will contain new entry only. Then this file if desired
could be merged with old entry lists using conventional editors (not
reccommended).

It is recommended that new entry description to be checked prior to
using in refinement. Especially atom types should be checked. If there
are hydrogens the program creates dictionary files usually correctly. If
there is no hydrogens in many cases program creates dictionary entries
correctly. If coordinates are not good then atom types may not be
correct. In this case atom types in new entry list could be corrected
and program should be rerun with minimum description.

If description of the ligand is not as desired by the user then he could
use sketcher to correct the coordinates and the description for it.

After having new entries and checking them carefully then normal refmac
could be run with new ligand supplied like:

::

    refmac ... LIB_IN <my_new_ligands> ... << eor

    Command lines to use refmac

    eor

Another way of making new entries from coordinates is using
`libcheck <http://www.ysbl.york.ac.uk/~alexei/libcheck.html>`__.

Dictioanary entries from the pdb style connectivity list
--------------------------------------------------------

One way of deriving dictionary entry is using pdb style connectivity
list as shown in Text 1. Extreme care should be taken when using pdb
style connectivity list as it is formatted file and all atoms including
hydrogens must be present with all their connectivity. If at least one
atom (for example hydrogen) is absent then whole geometry of the ligand
may become incorrect as the program will interpret bonds incorrectly.
When connectivity list is ready
`libcheck <http://www.ysbl.york.ac.uk/~alexei/libcheck.html>`__ could be
used to derive `minimum description <#minimum_dict>`__:

::

    libcheck << rol

    _file_dic my_pdb_connectivity.dic
    eol 
    eol

The the program libcheck will create minimum description as in Text 2
and it will be written in new\_mon\_lib.cif. Then this file should be
copied to another file and checked carefully. After checking and editing
technique described in `the next section <#minimum_dict>`__ should be
used to derive dictionary entry and coordinates to use by the graphics
program

After editting minimum description to user's satisfaction it could be
used to derive new entry list and coordinates using
`libcheck <http://www.ysbl.york.ac.uk/~alexei/libcheck.html>`__.

Dictionary enrries from minimum description of the ligand
---------------------------------------------------------

When information about connectivity is available i.e. which atom is bond
to what and contents of ligand i.e. which element occupies which
position then minimum description could be used to derive dictionary
entry for the ligand. It is good idea first to draw ligand on a piece of
paper like:

::



                           
              OH            
              |           
        HO    C2     O1-P1(O3)
          \   /\   /
           \ /  \ /
            C3   C1
            |    |
            |    |
            C4   C6
           : \  / :
          :   \/   :
         O4    C5    OH
         |     |
       P4(O3)  O5
               |
               P5(O3)

         Figure 1

Where dotted lines show that atom is below the picture and dashed lines
show above the picture.

If this pictire is converted to the minimum description it will have the
form as in `Text 1 <Text1.html>`__

When making minimum description one should be careful with the chiral
volumes present in the ligand.

Then `libcheck <http://www.ysbl.york.ac.uk/~alexei/libcheck.html>`__
could be run like:

::

    libcheck << eol
    N
    _file_l <minimum_description>
    _mon MON
    eol

Where <minimum\_description> is the file containing a minimim
description and MON is the name of the ligand.

After libcheck there will be complete dictionary description and
coordinate files in mmCIF and pdb format with all hydrogens present.

Links to non-commercial edtitors to derive coordinates from chemical desctiption
================================================================================

#. 

   `Corina <http://www2.ccc.uni-erlangen.de/services/3d.html>`__
       This site gives very good coordinates if you supply smile
       strings. It can recognise simple or stereo smile strings. Best
       way of accessing to this site is to draw molecule using
       `CACTVS <http://www2.ccc.uni-erlangen.de/software/cactvs/tools.html>`__
       and using smile derived from cactvs to send to CORINA to derive
       coordinates.

#. 

    `Monosachiride data
   base <http://www.cermav.cnrs.fr/databank/mono/index2.html>`__
       It contains various monosacharides. They could be taken in a pdb
       format and used as a building block for ligands containing
       sugars. PDB file may hahe to be edited to replace charges with
       B-values.

#. 

    `Disaccharide data
   base <http://www.cermav.cnrs.fr/databank/disacch/index.html>`__
       It contains various discharides. Various conformation could be
       chosen using picture provided by them. Output will be pdb file.
       This pdb file could be used to create ligand description or for
       building blocks for ligand under investigation

#. 

   `Enhanced NCI database
   browser <http://www2.ccc.uni-erlangen.de/ncidb/frame.html>`__
       Using this site one can search for structure with a given
       substructure.

#. 

   `sweet2 <http://www.dkfz-heidelberg.de/spec/sweet2/doc/index.html>`__
       This sites can give you polysacharides.
