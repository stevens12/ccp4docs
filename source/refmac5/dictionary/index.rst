REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac\_5.\*
------------------------------------------

DICTIONARY
==========

`**Full Description of the dictionary files** <../../mon_lib.html>`__
    Description of organisation of the ligand dictionary used by the
    program REFMAC
`**Dictionary entry from coordinate file(s)** <coord-dict.html>`__
    How to make a dictionary entry if there is a coordinate file.
    Coordinate file formats recognised are PDB, small molecule CIF (for
    example when coordinates are from the Cambridge small molecule
    database - CSD) or mmCIF
`**Dictionary entry from minimum description** <minimum-dict.html>`__
    How to make a dictionary entry by describing only a connectivity
    list and element names
`**Interactive sketcher to make dictionary
entry** <../../../ccp4i/help/modules/sketcher.html>`__
    Using interactive sketcher which is part of CCP4 graphical user
    interface to make dictionary entry
`**How to avoid the three letter residue name
problem** <../files/coordinates.html#pdb_modres>`__
    PDB residue names are restricted to three letters. This document
    describes how to avoid this problem and not to break the PDB rule
`**Retrieve coordinates from dictionary** <../../libcheck.html>`__
    If the ligand is in the dictionary file supplied by us: how to
    retrieve coordinates to start to build using graphics
`**List of standard monomer descriptions** <list-of-ligands.html>`__
    List of ligand descriptions distributed with the program REFMAC
`**Links to non-commercial editors to derive coordinates from chemical
description** <editor-links.html>`__
    Links to various non-commercial web sites which can help to derive
    coordinates from chemical descriptions. And these coordinates can be
    used to derive a dictionary entry as well as for model (re)building
