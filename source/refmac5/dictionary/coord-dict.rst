REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac version 5.\*
-------------------------------------------------

DICTIONARY
==========

How to make a dictionary entry from the coordinate file
-------------------------------------------------------

Coordinate file formats recognised are PDB, small molecule CIF and
mmCIF.

If there is a PDB coordinate file, then REFMAC can be used to create a
dictionary entry. In this case the following script should be used:

    ::

        refmac XYZIN &ltnew_ligand.pdb> XYZOUT <new_ligand1.pdb> \
        LIB_IN <my_lib_if_there_is_any.lib< LIB_OUT <my_new_lib.lib>
        << eor
        #
        #
        MODE NEWEntry
        MAKE EXIT Yes
        #
        END
        eor

If the ligand contained in the coordinate file is not in the list of
`the standard entries <list-of-ligands.html>`__, the program will create
a description for it and add this to the user supplied entry lists
specified by LIBIN. An output file will be written to the file specified
by LIBOUT (default is *new.lib*) in the command line. If there is no
user supplied entry list (*i.e.* LIBIN has not been specified) then the
created file will contain the new entry only. If desired, this file can
be merged with old entry lists using conventional editors (not
reccommended).

It is recommended that a new entry description be checked prior to using
it in refinement. Especially atom types should be checked. If there are
hydrogens, the program usually creates dictionary files correctly. If
there are no hydrogens, in many cases the program creates dictionary
entries correctly. If coordinates are not good then atom types may not
be correct. In this case atom types in the new entry list should be
corrected and the program rerun with minimum description.

If the description of a ligand is not as desired by the user, the
sketcher can be used to correct the coordinates and the description for
it.

After creating new entries and checking them carefully, REFMAC can be
run as normal, with new ligand supplied as follows:

    ::

        refmac ... LIB_IN <my_new_ligands> ... << eor

        Command lines to use refmac

        eor

Another way of making new entries from coordinates is using
`LIBCHECK <../../libcheck.html>`__.
