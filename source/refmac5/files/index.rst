REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac\_5.\*
------------------------------------------

INPUT AND OUTPUT FILES
----------------------

`Input script <input-script.html>`__
`Log file <log.html>`__
`Input and output coordinate files <coordinates.html>`__
`Input and output reflection files <../../mtzformat.html>`__
`Input and output TLS parameter files <tls.html>`__
`Dictionary of ligands <../../mon_lib.html>`__

