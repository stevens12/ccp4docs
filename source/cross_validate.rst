CROSS\_VALIDATE (CCP4: Supported Program)
=========================================

NAME
----

**cross\_validate** - Program for validating harvest files before
deposition

| 

SYNOPSIS
--------

::

    cross_validate file1 [file2 file3 file4 file5]
    (Key-worded Input)

DESCRIPTION
-----------

The program reads various files produced during data harvesting and
checks for correct `mmCIF format. <mmcifformat.html>`__ Full details of
the mmCIF format can be found on the `IUCr mmCIF
Page. <http://www.iucr.ac.uk/iucr-top/cif/mm/index.html>`__

KEYWORDED INPUT
---------------

**cross**

When the 'cross' keyword is used, the program checks that the data
between the two files is consistent. There are certain mmCIF categories
that are produced in all harvest files. The program checks to see if the
data in these categories are consistent. This includes space group, cell
dimensions, and Project and Dataset names. This is to ensure that the
data is consistent between different harvest files belonging to the same
Project and Dataset for purposes of deposition.

EXAMPLES
--------

Runnable example
~~~~~~~~~~~~~~~~

`cross\_validate.exam <../examples/unix/runnable/cross_validate.exam>`__

Example 1
~~~~~~~~~

This program checks that harvest files are in correct mmCIF format:

::

    cross_validate $CEXAM/data/red_aucn.scala
    end

Example 2
~~~~~~~~~

::

    cross_validate $CEXAM/data/red_aucn.scala $CEXAM/data/red_aucn.truncate 
    cross
    end

AUTHORS
-------

Pryank Patel (p.patel@ccp4.ac.uk)
