FFJOIN (CCP4: Supported Program)
================================

NAME
----

**ffjoin** - Fffear Fragment JOINing, release 1.0.0, 06/06/00

SYNOPSIS
--------

| **ffjoin XYZIN** *foo.pdb* [**XYZIN1** *foo.pdb*] [**XYZIN2**
  *foo.pdb*] **XYZOUT** *bar.pdb*
| [`Keyworded input <#keywords>`__]

REFERENCE
---------

-  K. Cowtan (1998), Acta Cryst. D54, 750-756. Modified phased
   translation functions and their application to molecular fragment
   location.

DESCRIPTION
-----------

\`ffjoin' takes files of fragments output from \`fffear' and merges
linked fragments to create longer fragments (with greater confidence
values). Fragments running in opposite directions may optionally be
merged. Overlapping fragments which cannot be merged may optionally be
identified and the fragment with the lower confidence value removed.
Since fragments of opposite direction may be merged, the output file
will only contain alpha Carbons.

\`ffjoin' does not know about crystal symmetry or repeat, so all input
fragments should be calculated in \`fffear' using the same \`CENTRE'
keyword.

\`ffjoin' Recepies
~~~~~~~~~~~~~~~~~~

::

    ffjoin xyzin alpha-helix-10-rot.pdb xyzout alpha-helix-10-rot-join.pdb << eof

    eof

INPUT/OUTPUT FILES
------------------

XYZIN
~~~~~

Input pdb file. This should contain a list of fragments output from
\`fffear'.

XYZIN1
~~~~~~

Input pdb file. Additional fragments to be merged, e.g. from a different
search model.

XYZIN2
~~~~~~

Input pdb file. Additional fragments to be merged, e.g. from a different
search model.

XYZOUT
~~~~~~

Output pdb file. This contains the CA coordinates for the merged
fragments. The B-factors will contain updated confidence values which
will be lower for those fragments which appear many times.

KEYWORDS
--------

Input is keyworded. Available keywords are: `JOIN <#join>`__,
`BUMP <#bump>`__.

BASIC KEYWORDS
--------------

(All keywords are optional.)

.. raw:: html

   <div align="center">

+--------------------------------------------------------------------------+
| .. rubric:: JOIN [RADIUS <joinrad>] [OVERLAP <overlap>] [NOREVERSE]      |
|    :name: join-radius-joinrad-overlap-overlap-noreverse                  |
|                                                                          |
| <joinrad>                                                                |
|     Maximum allowed CA separation for fragments to be considered         |
|     mergeable. If any pair of corresponding CA atoms in a pair of        |
|     fragments are beyond this spacing then the fragments will not be     |
|     merged. Default=2.0A                                                 |
| <overlap>                                                                |
|     Minimum number of overlapping CA atoms for fragments to be           |
|     considered mergeable. Reduce this to 2 when trying to join unlike    |
|     fragments, e.g. helices to strands. Default=3 CA atoms.              |
| NOREVERSE                                                                |
|     Do not merge fragments running in opposite directions.               |
|                                                                          |
| .. rubric:: BUMP [RADIUS <bumprad>] [NOBUMP]                             |
|    :name: bump-radius-bumprad-nobump                                     |
|                                                                          |
| <bumprad>                                                                |
| Maximum CA separation for fragments which are considered as clashing. If |
| any pair of corresponding CA atoms in a pair of fragments are within     |
| this spacing after all other merging is complete, then the fragment with |
| the lower confidence value will be removed. Default=3.0A                 |
| NOBUMP                                                                   |
| Do not perform bump checking and fragment removal.                       |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

OUTPUT
------

New fragments are assembled using an internal numbering scheme from the
input fragments which are described by their chain name and segid for
the first input file only. Merged fragments are then eliminated by
bumping according to their internal numbers.

AUTHOR
------

| Kevin D. Cowtan, Department of Chemistry, University of York
| email: cowtan@ysbl.york.ac.uk

REFERENCES
----------

#. K. Cowtan (1998), Acta Cryst. D54, 750-756. Modified phased
   translation functions and their application to molecular fragment
   location.
#. Kleywegt G. J., Jones T. A. (1997) Acta Cryst., D53, 179-185.
   Template convolution to enhance or detect structural features in
   macromolecular electron-density maps.

SEE ALSO
--------

`fffear <fffear.html>`__
