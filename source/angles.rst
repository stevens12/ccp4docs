ANGLES (CCP4: Unsupported Program)
==================================

NAME
----

**angles** - bond lengths, bond angles and dihedral angles from
coordinate files

SYNOPSIS
--------

| **angles XYZIN** *foo\_in.pdb* **ANGDAT** *foo\_in.prt* **ANGOUT**
  *foo\_out.ang* **PLOT** *foo\_out.plt*
| [Input cards]

DESCRIPTION
-----------

This program computes bond lengths, bond angles and most usefully
dihedral angles for proteins or other molecules. A Ramachandran plot may
also be produced.

Warning! At present, the program will give incorrect angles across
breaks in the chain.

INPUT AND OUTPUT FILES
----------------------

Input
~~~~~

 XYZIN
    Input coordinates, Brookhaven PDB format.
 ANGDAT
    Library file (defaults to standard protein version
    $CLIBD/angldat.prt ).

Output
~~~~~~

 ANGOUT
    Angles, only written if keyword DIHEdral is present.
 PLOT
    Ramachandran plot file, default extension .PLT

KEYWORDED INPUT
---------------

All commands are introduced by keywords, with free format numbers. Only
the first 4 characters of the keyword are used, and all are optional.

TITLe <title>
~~~~~~~~~~~~~

Title for plot.

NODI
~~~~

Do not calculate dihedral angles (default: do).

DIHEdral
~~~~~~~~

Write calculated dihedral angles to a file with logical name ANGOUT
(default: write only to log file).

ALLOed [NEW\|OLD]
~~~~~~~~~~~~~~~~~

Either the new or old allowed regions of the Ramachandran plot will be
marked. The default is the new region.

NOPLot
~~~~~~

Do not draw Ramachandran plot (default do).

LABEL <subkeywords>
~~~~~~~~~~~~~~~~~~~

Labelling options for Ramachandran plot. Possible subkeywords:

NOGLycine
    Label all residues with positive phi which are not Gly (default).
POSItive
    Label all positive phi.
ALL
    Label all residues.
NONE
    Label none.
NUMBer
    Number all residues.

NOTItle
~~~~~~~

No labels or text on plot. This may useful for publication plots.

CHAIn <IDCH>
~~~~~~~~~~~~

Select all residues in chain with chain label IDCH (1 character).
Default, select all residues or see RESIdue commmand.

RESIdue <R1> <R2>
~~~~~~~~~~~~~~~~~

Select all residues between residue R1 and R2 inclusive. Default, select
all residues or see CHAIn commmand. <R1>, <R2> are residue names which
may be numbers, or a 3-digit number + 1-character chain identifier.

BOND <ERRL>
~~~~~~~~~~~

Compute bond lengths (default if absent, do not compute bond lengths).
<ERRL> list bonds whose fractional error is greater than <ERRL> (if
ideal bond lengths are given).

ANGLe <ERRA>
~~~~~~~~~~~~

Compute bond angles (default if absent, do not compute bond angles).
<ERRA> list angles whose fractional error is greater than <ERRA> (if
ideal bond angles are given).

Library file (ANGDAT)
---------------------

This file contains the definition of lengths and angles to be
calculated. It consists of four parts.

#. Three lines defining the three atoms from one residue to be carried
   over to the next for calculation of main-chain angles (for proteins
   these three would be N,CA,C). Each line consists of the name of the
   atom followed by a name representing that atom in the earlier residue
   in format 2A4, e.g. "N .N " means that atom ".N " represents atom "N
   " in the previous residue. The following specifications can use any
   atom names that may appear in the coordinate file, plus these three
   extra names.
#. Number of bond lengths, followed on separate lines by pairs of atom
   names (2A4) defining bond lengths, with an optional ideal distance
   (F10.5).
#. Number of bond angles, followed on separate lines by groups of three
   atom names (3A4) defining bond angles, with an optional ideal angle
   (F10.5).
#. Number of dihedral angles, followed on separate lines by groups of
   four atom names defining dihedral angles together with the angle name
   (5A4). For proteins, the first two should be "PHI " and "PSI " for
   the Ramachandran plot.

EXAMPLES
--------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`angles.exam <../examples/unix/runnable/angles.exam>`__

SEE ALSO
--------

`DISTANG <distang.html>`__, PROCHECK
